﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class ContentViewController : UIViewController
    {
        public int pageIndex = 0;
		public string titleText;
		public string imageFile;

		public ContentViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			label.Text = titleText;
			label.Font = UIFont.FromName("Avenir-Book", 25f);
            label.AdjustsFontSizeToFitWidth = true;
			ellipseImageView.ContentMode = UIViewContentMode.ScaleToFill;
			ellipseImageView.Image = ellipseImageView.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			ellipseImageView.TintColor =  UIColor.FromRGB(1, 132, 207);

		}

		public override void ViewWillAppear(bool animated)
		{
			SetAvatar();
			base.ViewWillAppear(animated);
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
		}

		void SetAvatar()
		{
			AMMain.SetAvatarLinkCards(imageView, imageFile, "Airmiles_Placeholder@3x.png", (UIScreen.MainScreen.Bounds.Width - 200));
		}
    }
}