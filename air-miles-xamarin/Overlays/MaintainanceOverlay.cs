﻿using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	/// <summary>
	/// A simple modal overlay that shows an activity spinner and maintainance message. To use, 
	/// instantiate with a CGRect frame, and add to super view. When finished, call Hide().
	/// </summary>
	public class MaintainanceOverlay : UIView
	{
		// control declarations
		UILabel MaintainanceLabel;

		public MaintainanceOverlay(CGRect frame) : base(frame)
		{
			// configurable bits
			BackgroundColor = UIColor.FromRGB(0, 125, 195);
			AutoresizingMask = UIViewAutoresizing.All;

			nfloat labelHeight = frame.Height - 144;
			nfloat labelWidth = Frame.Width - 30;

			// derive the center x and y
			nfloat centerX = Frame.Width / 2;
			nfloat centerY = Frame.Height / 2;

			// create and configure the "Loading Data" label
			MaintainanceLabel = new UILabel(new CGRect(
				15,
				0,
				labelWidth,
				labelHeight
			));
			MaintainanceLabel.Font = UIFont.FromName("Avenir-Black", 18f);
			MaintainanceLabel.BackgroundColor = UIColor.Clear;
			MaintainanceLabel.Lines = 0;
			MaintainanceLabel.TextColor = UIColor.White;
			MaintainanceLabel.Text = AMStrings.MaintainanceMessage;
			MaintainanceLabel.TextAlignment = UITextAlignment.Center;
			MaintainanceLabel.AutoresizingMask = UIViewAutoresizing.All;

			UIImageView backgroundImage = new UIImageView(new CGRect(0, 0, frame.Width, frame.Height));
			backgroundImage.Image = UIImage.FromFile("background.png");
			backgroundImage.ContentMode = UIViewContentMode.ScaleAspectFill;
			backgroundImage.Layer.MasksToBounds = true;
			AddSubview(backgroundImage);
			AddSubview(MaintainanceLabel);

			this.BackgroundColor = UIColor.FromRGB(0, 125, 195);

		}

		/// <summary>
		/// Fades out the control and then removes it from the super view
		/// </summary>
		public void Hide()
		{
			UIView.Animate(
				0.3, // duration
				() => { Alpha = 0; },
				() => { RemoveFromSuperview(); }
			);
		}
	}
}

