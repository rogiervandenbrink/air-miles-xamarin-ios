﻿using System;
using CoreGraphics;
using UIKit;

namespace airmilesxamarin
{
	/// <summary>
	/// A simple modal overlay that shows an activity spinner and loading message. To use, 
	/// instantiate with a CGRect frame, and add to super view. When finished, call Hide().
	/// </summary>
	public class LoadingOverlay : UIView
	{
		// control declarations
		UIActivityIndicatorView activitySpinner;
		UIActivityIndicatorView activitySpinner2;
		UILabel loadingLabel;

		public LoadingOverlay(CGRect frame, string text) : base(frame)
		{
			// configurable bits
			BackgroundColor = UIColor.White;
			Alpha = 1f;
			AutoresizingMask = UIViewAutoresizing.All;

			nfloat labelHeight = 22;
			nfloat labelWidth = Frame.Width - 20;

			// derive the center x and y
			nfloat centerX = UIScreen.MainScreen.Bounds.Width / 2;
			nfloat centerY = (Frame.Height - UIScreen.MainScreen.Bounds.Height) + (UIScreen.MainScreen.Bounds.Height / 2);

			// derive the center x and y
			nfloat centerX2 = Frame.Width / 2;
			nfloat centerY2 = centerY + 600;

			// create the activity spinner, center it horizontall and put it 5 points above center x
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activitySpinner.Frame = new CGRect(
				centerX - (activitySpinner.Frame.Width / 2),
				centerY - activitySpinner.Frame.Height - 20,
				activitySpinner.Frame.Width,
				activitySpinner.Frame.Height);
			activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(activitySpinner);
			activitySpinner.StartAnimating();

			// create the activity spinner, center it horizontall and put it 5 points above center x
			activitySpinner2 = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activitySpinner2.Frame = new CGRect(
				centerX2 - (activitySpinner2.Frame.Width / 2),
				centerY2 - activitySpinner2.Frame.Height + 20,
				activitySpinner2.Frame.Width,
				activitySpinner2.Frame.Height);
			activitySpinner2.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(activitySpinner2);
			activitySpinner2.StartAnimating();

			// create and configure the "Loading Data" label
			loadingLabel = new UILabel(new CGRect(
				centerX - (labelWidth / 2),
				centerY + 20,
				labelWidth,
				labelHeight
				));
			loadingLabel.Font = UIFont.FromName("Avenir-Medium", 15f);
			loadingLabel.BackgroundColor = UIColor.Clear;
			loadingLabel.TextColor = UIColor.LightGray;
			loadingLabel.Text = text;
			loadingLabel.TextAlignment = UITextAlignment.Center;
			loadingLabel.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(loadingLabel);

		}


		/// <summary>
		/// Allow you to update the label in the loading view
		/// </summary>
		public void UpdateLabel(string text)
		{
			loadingLabel.Text = text;
		}

		/// <summary>
		/// Fades out the control and then removes it from the super view
		/// </summary>
		public void Hide()
		{
			UIView.Animate(
				0.5, // duration
				() => { Alpha = 0; },
				() => { RemoveFromSuperview(); }
			);
		}
	}
}
