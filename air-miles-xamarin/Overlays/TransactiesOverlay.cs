﻿using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	/// <summary>
	/// A simple modal overlay that shows a error message. To use, 
	/// instantiate with a CGRect frame, and add to super view. When finished, call Hide().
	/// </summary>
	public class TransactiesOverlay : UIView
	{
		// control declarations
		UILabel loadingLabel;

		public TransactiesOverlay(CGRect frame, string text) : base(frame)
		{
			// configurable bits
			BackgroundColor = UIColor.FromRGB(208, 231, 243);
			AutoresizingMask = UIViewAutoresizing.All;

			nfloat labelHeight = frame.Height - 144;
			nfloat labelWidth = Frame.Width - 30;

			// derive the center x and y
			nfloat centerX = Frame.Width / 2;
			nfloat centerY = Frame.Height / 2;

			// create and configure the "Loading Data" label
			loadingLabel = new UILabel(new CGRect(
				15,
				0,
				labelWidth,
				labelHeight
			));
			loadingLabel.Font = UIFont.FromName("Avenir-Book", 18f);
			loadingLabel.TextColor = UIColor.FromRGB(0, 125, 195);
			loadingLabel.BackgroundColor = UIColor.Clear;
			loadingLabel.Lines = 0;
			loadingLabel.Text = text;
			loadingLabel.TextAlignment = UITextAlignment.Center;
			loadingLabel.AutoresizingMask = UIViewAutoresizing.All;

			UIImageView backgroundImage = new UIImageView(new CGRect(0, 0, frame.Width, frame.Height));
			backgroundImage.Image = UIImage.FromFile("background.png");
			backgroundImage.ContentMode = UIViewContentMode.ScaleAspectFill;
			backgroundImage.Layer.MasksToBounds = true;
			//AddSubview(backgroundImage);
			AddSubview(loadingLabel);

			this.BackgroundColor = UIColor.FromRGB(208, 231, 243);

		}

		/// <summary>
		/// Fades out the control and then removes it from the super view
		/// </summary>
		public void Hide()
		{
			UIView.Animate(
				0.3, // duration
				() => { Alpha = 0; },
				() => { RemoveFromSuperview(); }
			);
		}
	}
}
/*
 * // configurable bits
			BackgroundColor = UIColor.FromRGB(244, 244, 244);

			nfloat labelHeight = 26;
			nfloat labelWidth = Frame.Width - 20;

			// derive the center x and y
			nfloat centerX = Frame.Width / 2;
			nfloat centerY = Frame.Height / 2;


			// create and configure the "Loading Data" label
			loadingLabel = new UILabel(new CGRect(
				centerX - (labelWidth / 2),
				centerY - (labelHeight / 2) - 50,
				labelWidth,
				labelHeight
			));
			loadingLabel.Font = UIFont.FromName("Avenir-Medium", 22f);
			loadingLabel.BackgroundColor = UIColor.Clear;
			loadingLabel.TextColor = UIColor.DarkGray;
			loadingLabel.Text = AMStrings.InternetNoConnectionTitle;
			loadingLabel.TextAlignment = UITextAlignment.Center;
			loadingLabel.AutoresizingMask = UIViewAutoresizing.All;
			AddSubview(loadingLabel);
 */ 
