﻿using Foundation;
using System;
using UIKit;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
    public partial class BarcodeDetailButtonCell : UICollectionViewCell
    {
        public BarcodeDetailButtonCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(VoucherTableRow item, UIViewController parent, NSIndexPath rowPath)
		{

			//Description.TranslatesAutoresizingMaskIntoConstraints = true;

			Console.WriteLine("vouchercell geopend");

			//if (item.Description != null)
			//{
			//	Description.Text = item.Description;

			//	Console.WriteLine("description voucher " + item.Description);
			//}
			//if (item.Barcode != null && item.BarcodeType != null)
			//{
			//	SetBarcode(item.Barcode, item.BarcodeType);
			//	Console.WriteLine("set barcode");
			//}
			//if (item.Barcode != null)
			//{
			//	SetEanLabel(item.Barcode);
			//	Console.WriteLine("set eanlabel");
			//}
		}

		private void SetBarcode(String barcode, string format)
		{
			// Create the barcode

			if (format == "EAN13")
			{
				var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
				{

					Format = ZXing.BarcodeFormat.EAN_13,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 300,
						Height = 70,
						Margin = 0
					}
				};

				Barcode.Image = BarcodeWriter.Write(SharedMain.GenerateEan(barcode));
			}
			if (format == "EAN8")
			{
				var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
				{

					Format = ZXing.BarcodeFormat.EAN_8,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 300,
						Height = 70,
						Margin = 0
					}
				};

				Barcode.Image = BarcodeWriter.Write(SharedMain.GenerateEan(barcode));
			}
			if (format == "CODE128")
			{
				var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
				{

					Format = ZXing.BarcodeFormat.CODE_128,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 300,
						Height = 70,
						Margin = 0
					}
				};

				Barcode.Image = BarcodeWriter.Write(SharedMain.GenerateEan(barcode));
			}
		}

		private void SetEanLabel(String barcode)
		{
			EANLabel.Text = " " + SharedMain.GenerateEan(barcode);

			var EANStyle = new NSAttributedString(EANLabel.Text,
				new UIStringAttributes()
				{
					KerningAdjustment = 15
				});

			EANLabel.AttributedText = EANStyle;
		}
    }
}