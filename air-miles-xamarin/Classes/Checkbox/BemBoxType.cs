﻿using System;
namespace airmilesxamarin
{
	internal enum BemBoxType
	{
		/// Circled box.
		BemBoxTypeCircle,

		/// Squared box.
		BemBoxTypeSquare
	}
}
