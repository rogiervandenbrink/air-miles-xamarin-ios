﻿using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class TokenRoot
	{
		[JsonProperty(PropertyName = "Respons_Token")]
		public ResponseToken TokenApp { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }

		[JsonProperty(PropertyName = "Respons_TokenCheck")]
		public ResponseTokenCheck ResponseTokenCheck { get; set; }
	}

	public class ResponseToken
	{
		[JsonProperty(PropertyName = "TokenApp")]
		public string TokenApp{ get; set; }

	}

	public class ResponseTokenCheck
	{
		[JsonProperty(PropertyName = "ShowPopUp")]
		public bool ShowPopUp { get; set; }

		[JsonProperty(PropertyName = "PopUpMessage")]
		public string PopUpMessage { get; set; }

		[JsonProperty(PropertyName = "AddressBouncePresent")]
		public bool AddressBouncePresent { get; set; }

		[JsonProperty(PropertyName = "AddressBounceMessage")]
		public string AddressBounceMessage { get; set; }

		[JsonProperty(PropertyName = "EmailBouncePresent")]
		public bool EmailBouncePresent { get; set; }

		[JsonProperty(PropertyName = "EmailBounceMessage")]
		public string EmailBounceMessage { get; set; }

		[JsonProperty(PropertyName = "MobileNrBouncePresent")]
		public bool MobileNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "MobileNrBounceMessage")]
		public string MobileNrBounceMessage { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBouncePresent")]
		public bool PrivateNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBounceMessage")]
		public string PrivateNrBounceMessage { get; set; }

		[JsonProperty(PropertyName = "CheckTokenResponse_LinkCardsMessage")]
		public MobileLoginInfo_LinkCardsMessage CheckTokenResponse_LinkCardsMessage { get; set; }

		[JsonProperty(PropertyName = "CardNumber")]
		public string CardNumber { get; set; }

		[JsonProperty(PropertyName = "DigitalCard")]
		public bool DigitalCard { get; set; }

	}
}

