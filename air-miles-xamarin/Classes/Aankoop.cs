﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class AankopenRoot
	{
		[JsonProperty(PropertyName = "Respons_MijnAankopen")]
		public Aankoop[] Aankopen { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }
	}

	public class Aankoop
	{
		[JsonProperty(PropertyName = "ValidityDate")]
		public long Date { get; set; }

		[JsonProperty(PropertyName = "PartnerName")]
		public string Title { get; set; }

		[JsonProperty(PropertyName = "TotalString")]
		public string TotalString { get; set; }

		[JsonProperty(PropertyName = "BarcodeType")]
		public string BarcodeType { get; set; }

		[JsonProperty(PropertyName = "Description")]
		public string Description { get; set; }

		[JsonProperty(PropertyName = "ProductType")]
		public string ProductType { get; set; }

		[JsonProperty(PropertyName = "Validity")]
		public string ValidityDate { get; set; }

		[JsonProperty(PropertyName = "ProductID")]
		public string ProductID { get; set; }

		[JsonProperty(PropertyName = "VoucherNumber")]
		public string VoucherNumber { get; set; }

	}
}