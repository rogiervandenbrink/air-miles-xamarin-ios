﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class ValidityRoot 	{ 		[JsonProperty(PropertyName = "Respons_HoudbaarheidAirmiles")] 		public Validity[] Response { get; set; }  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Validity 	{ 		[JsonProperty(PropertyName = "NumberOfAirmiles")] 		public string NumberOfAirmiles { get; set; }

		[JsonProperty(PropertyName = "ExpirationDatePerMonth")]
		public long ExpirationDatePerMonth { get; set; } 	} }
