﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class SignoutRoot 	{ 		[JsonProperty(PropertyName = "Respons_Uitschrijven")] 		public Signout Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Signout 	{ 		[JsonProperty(PropertyName = "Message")] 		public string PopupMessage; 	} }
