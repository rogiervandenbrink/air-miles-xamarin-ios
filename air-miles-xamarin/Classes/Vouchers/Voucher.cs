﻿using System;

namespace airmilesxamarin
{
	public class Voucher
	{
		public string Title { get; set; }
		public string Detail { get; set; }
		public string EndDate { get; set; }
		public string Barcode { get; set; }
	}
}

