﻿using System;
using UIKit;

namespace airmilesxamarin
{
	public class AMSizes
	{
		public static nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
		public static nfloat ScreenPadding = 20;
		public static nfloat PromotionTitleHeight = 100;
		public static double PromotionHeightAspectRatio = 1.0135;

		public static double PromotionHeight = ((ScreenWidth - (ScreenPadding * 2)) * PromotionHeightAspectRatio) + PromotionTitleHeight;
		public static double PromotionWidth = (ScreenWidth - (ScreenPadding * 2));

	}
}

