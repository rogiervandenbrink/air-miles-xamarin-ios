﻿using System;
using Foundation;
using UIKit;

namespace airmilesxamarin
{
	public class AMImage
	{
		// get image from url
		public static UIImage FromUrl(string uri)
		{
			using (var url = new NSUrl(uri))
			using (var data = NSData.FromUrl(url))
				return UIImage.LoadFromData(data);
		}
	}
}

