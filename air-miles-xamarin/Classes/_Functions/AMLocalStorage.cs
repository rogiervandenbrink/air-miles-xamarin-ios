﻿using System;
namespace airmilesxamarin
{
	public class AMLocalStorage
	{
		public static string SecureTokenApp = "SecureTokenApp"; // returns string
		public static string SecureMemberId = "SecureMemberId"; 
		public static string SecureZMemberId = "SecureZMemberId";// returns string
		public static string SecureDeviceId = "SecureDeviceId";// returns string
		public static string SecurePincode = "SecurePincode"; // returns string

		public static string PinAttempts = "PinAttempts"; // returns int

		public static string HasLogin = "HasLogin"; // returns bool
		public static string HasCard = "HasCard";

		public static string Session = "Session";
		public static string SessionDateTimeClose = "SessionDateTimeClose";
		public static string NewSession = "NewSession";
		public static string NewSessionAanbod = "NewSessionAanbod";

		public static string AppVersion = "AppVersion";
		public static string OSVersion = "OSVersion";
		public static string OSType = "OSType";
		public static string DeviceType = "DeviceType";
		public static string DeviceID = "DeviceID";

		public static string AlertTouchID = "AlertTouchID";
		public static string AlertPinID = "AlertPinID";

		public static string FirstAlertTouchID = "FirstAlertTouchID"; // returns bool
		public static string FirstAlertPinID = "FirstAlertPinID"; // returns bool

		public static string FirstKaartAanvragen = "FirstKaartAanvragen"; // returns bool
		public static string FirstLogin = "FirstLogin"; // returns bool
        public static string FirstNotifications = "FirstNotifications";

		public static string Username = "Username"; 
		public static string UserNameIR025 = "UserNameIR025"; // returns string

		public static string Avatar = "Avatar";

		public static string CardNumber = "CardNumber";
		public static string ExternalCardNumber = "ExternalCardNumber";
		public static string Balance = "Balance";
		public static string Collectors = "Collectors";
        public static string ExpirationDate = "ExpirationDate";
        public static string ExpirationAmount = "ExpirationAmount";

		public static string BalanceDate = "BalanceDate";
		public static string OptIn = "OptIn";
		public static string PopUpMessage = "PopUpMessage";
		public static string RedirectToDigitalAccount = "RedirectToDigitalAccount";
		public static string LinkcardsMessage = "LinkcardsMessage";
		public static string DigitalAccount = "DigitalAccount";
        public static string CardBlockedMessage = "CardBlockedMessage";

		public static string UserFirstName = "UserFirstName";
		public static string UserFullName = "UserFullName";
		public static string UserPrefix = "UserPrefix";
		public static string UserLastName = "UserLastName";
		public static string UserGender = "UserGender";
		public static string UserEmailBounceReason = "UserEmailBounceReason";
		public static string UserHouseNumber = "UserHouseNumber";
		public static string UserTelephoneNrPrivate = "UserTelephoneNrPrivate";
		public static string UserTelephoneNrMobile = "UserTelephoneNrMobile";
		public static string UserShowPopUp = "UserShowPopUp";
		public static string UserPostalCode = "UserPostalCode";
		public static string UserStreet = "UserStreet";
		public static string UserMobileNrBouncePresent = "UserMobileNrBouncePresent";
		public static string MobilePhoneBounceReason = "MobilePhoneBounceReason";
		public static string UserStatus = "UserStatus";
		public static string EmailBouncePresent = "EmailBouncePresent";
		public static string PrivateNrBouncePresent = "PrivateNrBouncePresent";
		public static string AddressBouncePresent = "AddressBouncePresent";
		public static string PrivatePhoneBounceReason = "PrivatePhoneBounceReason";
		public static string AmountBounceIndicators = "AmountBounceIndicators";
		public static string UserCity = "UserCity";
		public static string UserCountry = "UserCountry";
		public static string UserSalutation = "UserSalutation";

		public static string UserRefreshToken = "RefreshToken";

		public static string UserInitials = "UserInitials";
		public static string UserBirthday= "UserBirthday";
		public static string UserEmail = "UserEmail";
		public static string UserMobile = "UserMobile";
		public static string UserBirthdayFloat = "UserBirthdayFloat";
		public static string UserHouseNumberSupplement = "UserHouseNumberSupplement";

		public static string Newsletter = "Newsletter";
		public static string Notifications = "Notifications";
		public static string EmailBounceReason = "EmailBounceReason";
		public static string MobileNrBouncePresent = "MobileNrBouncePresent";
		public static string AddressBounceReason = "AddressBounceReason";
        public static string Profiling = "Profiling";

		public static string SavedAankopen = "SavedAankopen";
		public static string SavedTransacties = "SavedTransacties";

		public static string JsonPartners = "JsonPartners";
		public static string JsonAanbod = "JsonAanbod";
		public static string JsonPromotie = "JsonPromotie";
		public static string JsonGifts = "JsonGifts";
		public static string JsonGift = "JsonGift";
		public static string JsonHippoGifts = "JsonHippoGifts";
		public static string JsonShellGifts = "JsonShellGifts";
		public static string JsonPersonalizedPromotions = "JsonPersonalizedPromotions";

		public static string CurrentPromotie = "CurrentPromotie";

		public static string SeenGifts = "SeenGifts";

		public static string NoFirstPinTouch = "NoFirstPinTouch";

		public static string UnseenGifts = "UnseenGifts";

		public static string ExternalLink = "ExternalLink";
		public static string DateTimeLastCall = "DateTimeLastCall";

		public static string KaartService = "KaartService";
		public static string AanbodService = "AanbodService";
		public static string PromotieService = "PromotieService";
		public static string PartnerService = "PartnerService";
		public static string PartnerDetailService = "PartnerDetailService";
		public static string AanbodRefresh = "AanbodRefresh";

        public static string FromSaldoViewController = "FromSaldoViewController";
        public static string ToProfileViewController = "ToProfileViewController";
		public static string TruncatedPan = "TruncatedPan";

		public static string IncorrectPersonalInformation = "IncorrectPersonalInformation";
		public static string IncorrectPersonalInformationMessage = "IncorrectPersonalInformationMessage";
        public static string TimestampLogfile = "TimestampLogfile";
	}
}

