﻿using System;
using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace airmilesxamarin
{
	public class AMStyle
	{
		public static void RoundedButton(UIButton Button)
		{
			Button.Layer.CornerRadius = Button.Bounds.Height / 2;
		}

		public static void BorderedTextfield(UITextField Textfield)
		{
			Textfield.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
			Textfield.Layer.BorderWidth = 2f;
			Textfield.Layer.CornerRadius = Textfield.Bounds.Height / 2;
			Textfield.Layer.MasksToBounds = true;
		}

		public static void ButtonPurpleDisabled(UIButton Button, string title)
		{
			Button.Layer.BorderColor = UIColor.FromRGB(221, 15, 150).CGColor;
			Button.Layer.BorderWidth = 2f;
			Button.BackgroundColor = UIColor.FromRGB(255, 255, 255);
			Button.SetTitle(title, UIControlState.Disabled);
			Button.SetTitleColor(UIColor.Red, UIControlState.Disabled);
		}

		public static void DropShadow(UIView view)
		{
			// Cell styling
			view.Layer.CornerRadius = 4;
			view.Layer.MasksToBounds = false;
			view.Layer.ShadowColor = new CGColor(0, 0, 0);
			view.Layer.ShadowRadius = 1;
			view.Layer.ShadowOpacity = 0.1f;
			view.Layer.ShadowOffset = new CGSize(0, 1);
		}

		public static void BorderedButton(UIButton Button)
		{
			Button.Layer.BorderColor = UIColor.White.CGColor;
			Button.Layer.BorderWidth = 2f;
			Button.Layer.CornerRadius = Button.Bounds.Height / 2;
			Button.Layer.MasksToBounds = true;
		}

		public static void BorderedBlueButton(UIButton Button)
		{
			Button.Layer.BorderColor = UIColor.White.CGColor;
			Button.Layer.BorderWidth = 2f;
			Button.Layer.BackgroundColor = UIColor.FromRGB(0, 125, 195).CGColor;
			Button.SetTitleColor(UIColor.White, UIControlState.Normal);
			Button.Layer.CornerRadius = Button.Bounds.Height / 2;
			Button.Layer.MasksToBounds = true;
		}

		public static void BorderedBlueSegmentedControl(UISegmentedControl SGControl)
		{
			SGControl.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
			SGControl.Layer.BorderWidth = 2f;
			SGControl.Layer.BackgroundColor = UIColor.White.CGColor;
			SGControl.Layer.CornerRadius = SGControl.Bounds.Height / 2;
			SGControl.Layer.MasksToBounds = true;
            SGControl.TintColor = UIColor.FromRGB(0, 125, 195);
		}

		public static void RoundedCornersButton(UIButton Button)
		{
			Button.Layer.CornerRadius = 5;
		}

		public static void BorderedGreyLabel(UILabel Label)
		{
			Label.Layer.BorderColor = UIColor.LightGray.CGColor;
			Label.Layer.BackgroundColor = UIColor.LightGray.CGColor;
			Label.Layer.BorderWidth = 2f;
			Label.Layer.CornerRadius = Label.Bounds.Height / 2;
			Label.Layer.MasksToBounds = true;
			Label.TextColor = UIColor.DarkGray;


			//Label.TextRectForBounds(new CGRect(50, 0, Label.Bounds.Width, Label.Bounds.Height),0);
		}

		public static void BorderedButtonBlue(UIButton Button)
		{
			Button.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
			Button.Layer.BorderWidth = 2f;
			Button.Layer.CornerRadius = Button.Bounds.Height / 2;
			Button.Layer.MasksToBounds = true;
		}

		public static void RoundedView(UIView view)
		{
			view.Layer.CornerRadius = view.Bounds.Height / 2;
			view.Layer.MasksToBounds = true;
		}

		public static void BorderedView(UIView View)
		{
			View.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
			View.Layer.BorderWidth = 2f;
			View.Layer.CornerRadius = View.Bounds.Height / 2;
			View.Layer.MasksToBounds = true;
		}

		public static void BlueTextColorOnEdit(UITextField Textfield)
		{
			Textfield.EditingDidBegin += delegate {
				Textfield.TextColor = UIColor.Blue;
			};
			Textfield.EditingDidEnd += delegate {
				Textfield.TextColor = UIColor.Black;
				Textfield.ResignFirstResponder();
			};
		}

		public static void BlackTextColor(UITextField Textfield)
		{
			
			Textfield.TextColor = UIColor.Black;

		}

		public static void GreyTextColor(UITextField Textfield)
		{

			Textfield.TextColor = UIColor.FromRGB(128, 128, 128);

		}

		public static void SlideHorizontaly(UIView view, bool isIn, double delay, bool fromLeft, double duration = 0.3, Action onFinished = null)
		{
			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;
			var minTransform = CGAffineTransform.MakeTranslation((fromLeft ? -1 : 1) * view.Bounds.Width, 0);
			var maxTransform = CGAffineTransform.MakeIdentity();

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, delay, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Alpha = isIn ? maxAlpha : minAlpha;
					view.Transform = isIn ? maxTransform : minTransform;
				},
				onFinished
			);
		}

		public static void Fade(UIView view, bool isIn, double delay, double duration = 0.3, Action onFinished = null)
		{
			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Transform = CGAffineTransform.MakeIdentity();
			UIView.Animate(duration, delay, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Alpha = isIn ? maxAlpha : minAlpha;
				},
				onFinished
			);
		}

		public static void FlipHorizontaly(UIView view, bool isIn, double duration = 0.3, Action onFinished = null)
		{
			var m34 = (nfloat)(-1 * 0.001);

			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;

			view.Alpha = (nfloat)1.0;

			var minTransform = CATransform3D.Identity;
			minTransform.m34 = m34;
			minTransform = minTransform.Rotate((nfloat)((isIn ? 1 : -1) * Math.PI * 0.5), (nfloat)0.0f, (nfloat)1.0f, (nfloat)0.0f);
			var maxTransform = CATransform3D.Identity;
			maxTransform.m34 = m34;

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Layer.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Layer.AnchorPoint = new CGPoint((nfloat)0.5, (nfloat)0.5f);
					view.Layer.Transform = isIn ? maxTransform : minTransform;
					view.Alpha = isIn ? maxAlpha : minAlpha;
				},
				onFinished
			);
		}

		public static void FlipVerticaly(UIView view, bool isIn, double duration = 0.3, Action onFinished = null)
		{
			var m34 = (nfloat)(-1 * 0.001);

			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;

			var minTransform = CATransform3D.Identity;
			minTransform.m34 = m34;
			minTransform = minTransform.Rotate((nfloat)((isIn ? 1 : -1) * Math.PI * 0.5), (nfloat)1.0f, (nfloat)0.0f, (nfloat)0.0f);
			var maxTransform = CATransform3D.Identity;
			maxTransform.m34 = m34;

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Layer.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Layer.AnchorPoint = new CGPoint((nfloat)0.5, (nfloat)0.5f);
					view.Layer.Transform = isIn ? maxTransform : minTransform;
					view.Alpha = isIn ? maxAlpha : minAlpha;
				},
				onFinished
			);
		}

		public static void Rotate(UIView view, bool isIn, bool fromLeft = true, double duration = 0.3, Action onFinished = null)
		{
			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;
			var minTransform = CGAffineTransform.MakeRotation((nfloat)((fromLeft ? -1 : 1) * 720));
			var maxTransform = CGAffineTransform.MakeRotation((nfloat)0.0);

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Alpha = isIn ? maxAlpha : minAlpha;
					view.Transform = isIn ? maxTransform : minTransform;
				},
				onFinished
			);
		}

		public static void Scale(UIView view, bool isIn, double duration = 0.3, Action onFinished = null)
		{
			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;
			var minTransform = CGAffineTransform.MakeScale((nfloat)0.1, (nfloat)0.1);
			var maxTransform = CGAffineTransform.MakeScale((nfloat)1, (nfloat)1);

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Alpha = isIn ? maxAlpha : minAlpha;
					view.Transform = isIn ? maxTransform : minTransform;
				},
				onFinished
			);
		}

		public static void SlideVerticaly(UIView view, bool isIn, bool fromTop, double duration = 0.3, Action onFinished = null)
		{
			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;
			var minTransform = CGAffineTransform.MakeTranslation(0, (fromTop ? -1 : 1) * view.Bounds.Height);
			var maxTransform = CGAffineTransform.MakeIdentity();

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Alpha = isIn ? maxAlpha : minAlpha;
					view.Transform = isIn ? maxTransform : minTransform;
				},
				onFinished
			);
		}

		public static void Zoom(UIView view, bool isIn, double duration = 0.3, Action onFinished = null)
		{
			var minAlpha = (nfloat)0.0f;
			var maxAlpha = (nfloat)1.0f;
			var minTransform = CGAffineTransform.MakeScale((nfloat)2.0, (nfloat)2.0);
			var maxTransform = CGAffineTransform.MakeScale((nfloat)1, (nfloat)1);

			view.Alpha = isIn ? minAlpha : maxAlpha;
			view.Transform = isIn ? minTransform : maxTransform;
			UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
				() =>
				{
					view.Alpha = isIn ? maxAlpha : minAlpha;
					view.Transform = isIn ? maxTransform : minTransform;
				},
				onFinished
			);
		}
	
	}
}

