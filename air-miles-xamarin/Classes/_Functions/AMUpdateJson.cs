﻿using System;
using System.Collections.Generic;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class AMUpdateJson
	{
		public AMUpdateJson()
		{
		}


		public static async void UpdateGiftsFromPartner()
		{
			List<string> giftPartners = new List<string>();
			List<string> giftProducts = new List<string>();

			// Set promoties
			string JsonGift = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);

			if (!string.IsNullOrEmpty(JsonGift))
			{
				GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(JsonGift);

				if (Gifts.Response.Length > 0)
				{
					for (int i = 0; i < Gifts.Response.Length; i++)
					{
						// Rempve tab badge
						AMMain.ClearAanbodTabBadge();

						bool productNotChosen = true;
						GiftsOffer offer = Gifts.Response[i].Offer[0];

						// if gift is activated
						if (offer.Activated == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);
									}
									// change bool
									productNotChosen = false;
								}
							}

						}
						else if (productNotChosen && !giftPartners.Contains(offer.Partner))
						{
							Console.WriteLine("add " + offer.Partner + " to list");

							// add the partnername to the list
							giftPartners.Add(offer.Partner);
						}
					}
				}
			}

			// create a dictionary where the hippo-returns are stored. based on their partnername
			var GiftOfferPartnerDictionary = new Dictionary<string, object>();
			var GiftsDictionary = new Dictionary<string, GiftRoot>();
			string productIds = null;

			// create another dictionay where the counters for the images are stored
			var CounterDictionary = new Dictionary<string, int>();

			Console.WriteLine("found partners " + giftPartners.Count.ToString());

			if (giftPartners.Count > 0)
			{
				// Set partner gifts
				foreach (string partner in giftPartners)
				{
					GiftsOfferPartner PartnerGifts = await GiftsService.GetGiftsFromPartner(partner);

					if (PartnerGifts != null)
					{
						// save the data in the dictionary
						GiftOfferPartnerDictionary.Add(partner, PartnerGifts);
						// set counter at 0
						CounterDictionary.Add(partner, 0);
					}
					else {
						Console.WriteLine($"no partner git found in {partner}");
					}
				}
			}
		}
	}
}

