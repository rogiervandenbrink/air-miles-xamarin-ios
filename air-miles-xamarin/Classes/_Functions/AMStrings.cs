﻿using System;
namespace airmilesxamarin
{
	public class AMStrings
	{
		public static string ErrorCode = "UNAUTHORIZED";
		public static string ErrorCodeTokenInvalid = "Tokenapp is empty or invalid.";

		public static string Cancel = "Annuleren";
		public static string Confirm = "OK";
		public static string Delete = "Verwijderen";

		public static string LoadingLogin = "Inloggen...";
		public static string LoadingUnlock = "Ontgrendelen...";
		public static string LoadingCardData = "Kaartgegevens ophalen...";
		public static string LoadingCreateCard = "Kaart aanmaken...";
		public static string LoadingCreateAccount = "Account aanmaken...";
		public static string LoadingTransacties = "Transacties ophalen...";
		public static string LoadingAankopen = "Aankopen ophalen...";
		public static string LoadingVoucher = "Voucher ophalen...";
		public static string LoadingToken = "Sessie verversen...";
		public static string LoadingAvatar = "Profielgegevens ophalen...";
		public static string LoadingSaveProfile = "Profielgegevens opslaan...";
		public static string LoadingGifts = "Aanbod ophalen...";
		public static string LoadingTransfer = "Air Miles overboeken...";
		public static string LoadingDonation = "Air Miles doneren...";
		public static string LoadingBlockcard = "Kaart blokkeren...";
		public static string LoadingReplacecard = "Kaart vervangen...";
		public static string LoadingLinkCards = "Kaart koppelen gegevens ophalen...";
		public static string LoadingQuitLinkCards = "Kaart koppelen opzeggen...";
		public static string LoadingInviteLinkCards = "Uitnodiging versturen...";
		public static string LoadingInvite = "Uitnodiging laden...";
		public static string LoadingSignout = "Uitschrijven...";
		public static string LoadingAvatarPhoto = "Profielfoto opslaan...";
		public static string LoadingValidity = "Geldigheid ophalen...";
		public static string LoadingUrl = "Pagina laden...";
        public static string LoadingWalletCard = "Air Miles kaart wordt opgehaald...";
        public static string LoadingWalletVoucher = "Voucher wordt opgehaald...";
		public static string ErrorTitle = "Let op!";

		public static string TooltipText = "Zorg dat je wachtwoord hieraan voldoet: minimaal 8 karakters, minimaal 1 hoofdletter (A-Z),  minimaal 1 kleine letter (a-z). Er mogen geen pipes (|) en backslashes ( \\ ) worden gebruikt. Je wachtwoord mag niet gelijk zijn aan je e-mailadres, gebruikersnaam of kaartnummer.";
		public static string TooltipTextUsername = "Zorg dat je gebruikersnaam hieraan voldoet: minimaal 4 en maximaal 20 karakters, alleen letters, cijfers en de volgende speciale karakters: ‘-’ ‘.’ ‘_’. Je gebruikersnaam mag niet gelijk zijn aan je e-mailadres of kaartnummer.";
		public static string TooltipUserNameTitle = "Gebruikersnaam vereisten";
		public static string TooltipValidityTitle = "Geldigheid";
		public static string TooltipValidityText = "Gespaarde Air Miles hebben een geldigheidsduur van 5 jaar. We zorgen ervoor dat de Air Miles die als eerste komen te vervallen als eerste ingewisseld worden.";

		public static string LogoutTitle = "Succesvol uitgelogd";
		public static string LogoutMessage = "Je bent uitgelogd. Let op! Je kaartnummer is bewaard op het scherm kaart zodat je gemakkelijk gebruik kunt blijven maken van de kaartfunctionaliteit/saldo check.";

		public static string PopUpMessageTitle = "Gegevens";
		public static string PopUpLinkcardsTitle = "Kaart koppelen";
        public static string PopUpRedirectTitle = "Account aanmaken?";

		public static string PopUpWalletTitle = "Toevoegen aan iOS Wallet";
		public static string PopUpWalletMessage = "Wil je je Air Miles kaart toevoegen aan iOS Wallet?";
        public static string PopUpWalletVoucherMessage = "Wil je deze voucher toevoegen aan iOS Wallet?";
        public static string PopUpWalletCardAlertMessage = "Om je Air Miles kaart toe te voegen aan iOS Wallet dien je ingelogd te zijn.";
        public static string PopUpWalletVoucherAlertMessage = "Om Air Miles vouchers toe te voegen aan iOS Wallet dien je je emailadres op te geven.";

        public static string SignoutExplenation = "Uitschrijven kan door op onderstaande button ‘uitschrijving bevestigen’ te klikken. Zodra je je akkoord geeft, verwijderen wij al jouw gegevens en kun je niet meer sparen en inwisselen.";
        public static string SignoutMessage = "Wat jammer dat je wilt stoppen met Air Miles. Weet je het echt zeker? Zodra je je akkoord geeft, verwijderen wij al jouw gegevens en kun je niet meer sparen en inwisselen. ";
		
        public static string LogoutAskTitle = "Uitloggen";
		public static string LogoutAskMessage = "Weet je zeker dat je wilt uitloggen? Bij een volgend bezoek moet je dan opnieuw inloggen met gebruikersnaam en wachtwoord. Voor het gemak blijft je kaartnummer bewaard. Als je niet uitlogt, wordt de app na een paar minuten automatisch vergrendeld.";
		public static string LogoutAskMessageDismiss = "Weet je zeker dat je wilt uitloggen? Bij een volgend bezoek moet je dan opnieuw inloggen met gebruikersnaam en wachtwoord. Voor het gemak blijft je kaartnummer bewaard.";

		public static string HeaderMessageOutOfApp = "Het is nog niet mogelijk om onderstaande zaken via de app te regelen. Als je op onderstaande items klikt word je doorgestuurd naar airmiles.nl.";

		public static string ErrorLoginTitle = "Inloggen mislukt";
		public static string ErrorLoginEmptyTextfields = "Vul een geldig gebruikersnaam en wachtwoord in om in te loggen.";
		public static string ErrorLoginEmptyUsername = "Vul een geldig gebruikersnaam in om in te loggen.";
		public static string ErrorLoginEmptyPassword = "Vul een geldig wachtwoord in om in te loggen.";
		public static string ErrorLoginMessage = "De combinatie gebruikersnaam en wachtwoord is onbekend. Probeer het nogmaals.";
		public static string ErrorLoginP02 = "De combinatie gebruikersnaam en wachtwoord is onbekend. Probeer het nogmaals.";
		public static string ErrorLoginP04 = "Je online mijn Air Miles account is nog niet ge(her)activeerd. Klik op de activeringslink in de e-mail of neem contact op met Air Miles via het contactformulier.";
		public static string ErrorLoginP05 = "Je Air Miles account is tijdelijk geblokkeerd. Probeer het over een uur nog een keer of vraag een nieuw wachtwoord aan.";
		public static string ErrorLoginP06 = "Je Air Miles account heeft de status 'geblokkeerd'. Neem contact op met Air Miles via het contactformulier.";
		public static string ErrorLoginNoConnection = "Vanwege een technische storing is het niet mogelijk om in te loggen. We werken hard aan een oplossing. Excuses voor dit ongemak!";
        public static string ErrorLoginIncorrectTitle = "Inloggegevens ongeldig";
        public static string ErrorLoginIncorrectMessage = "De inloggegevens komen niet overeen met de ingelogde gebruiker. Probeer nogmaals de app te ontgrendelen of log uit om het in z’n geheel opnieuw te proberen.";

		public static string ErrorDigitalCardNoConnection = "Vanwege een technische storing is het niet mogelijk om een kaart aan te maken. We werken hard aan een oplossing. Excuses voor dit ongemak!";

		public static string ErrorCardInvalidTitle = "Ongeldig kaartnummer";
		public static string ErrorCard = "Let op!";
		public static string ErrorCardInvalidMessage = "Let op! Je hebt geen geldig kaartnummer ingevoerd.";
		public static string ErrorCardNotFoundTitle = "Kaart niet gevonden";
		public static string ErrorCardNotFoundMessage = "Helaas! Deze Air Miles kaart kan niet worden gevonden.";
		public static string ErrorCardNoConnectionMessage = "Helaas! De Air Miles app kan geen verbinding maken met de database en daardoor het saldo niet ophalen. Probeer het later nogmaals.";
		public static string ErrorCardBlockedTitle = "Kaart geblokkeerd";
		public static string ErrorCardBlockedMessage = "Let op! Je Air Miles kaart is geblokkeerd. Neem contact op met onze klantenservice.";
        public static string ErrorMasterCard = "Let op! Het ingevoerde Mastercard kaartnummer is ongeldig. Je Mastercard kaartnummer staat op de voorkant van je Mastercard en bestaat uit 16 cijfers. Je kunt geen zakelijke Mastercard koppelen. Probeer het nogmaals of neem contact op met onze klantenservice.";

        public static string ErrorMailTitle = "Geen mailclient";
        public static string ErrorMailMessage = "Er is geen mailclient beschikbaar. Het verzenden van feedback is daardoor niet mogelijk.";
        public static string CallTitle = "Air Miles bellen";
        public static string CallMessage = "Weet je zeker dat je wilt bellen met Air Miles?";
        public static string FBUrl = "fb://page/?id=355249514506725";
        public static string FBWebUrl = "https://www.facebook.com/airmiles.nederland/";
        public static string TwitterUrl = "https://twitter.com/airmiles_nl";

		public static string ErrorNoGiftsTitle = "Geen aanbod gevonden";
		public static string ErrorNoGiftsMessage = "Helaas! We kunnen voor deze actie geen aanbiedingen ophalen. Probeer het later nogmaals.";

		public static string ErrorSessionExpiredTitle = "";
		public static string ErrorSessionExpiredMessage = "Helaas! Je huidige sessie is verlopen. Log opnieuw in om een nieuwe sessie te starten.";

		public static string AlertDigitalAccountTitle = "Account aanmaken";
		public static string AlertDigitalAccountMessage = "Met een Air Miles account krijg je veel extra's. Wat denk je van speciale aanbiedingen, snelle login en digitaal voucher beheer!";

		public static string DeleteCardTitle = "Air Miles kaart verwijderen";
		public static string DeleteCardMessage = "Weet je zeker dat je deze Air Miles kaart wilt verwijderen?";

		public static string InternetNoConnectionTitle = "Geen internetverbinding";
		public static string InternetNoConnectionMessage = "Helaas! Je hebt momenteel geen verbinding met het internet. Probeer het bij goed bereik nogmaals.";

		public static string MaintainanceMessage = "Vanwege een technische storing is dit onderdeel tijdelijk niet te gebruiken. We werken hard aan een oplossing. Onze excuses voor dit ongemak!";
		public static string ErrorNoConnectionMessage = "Helaas! De Air Miles app kan geen verbinding maken met de database. Probeer het later nogmaals.";

		public static string TitlePasswordTip = "Wachtwoord vereisten";
		public static string MessagePasswordTip = "Zorg dat je wachtwoord hieraan voldoet: minimaal 8 karakters, minimaal 1 hoofdletter (A-Z),  minimaal 1 kleine letter (a-z). Je wachtwoord mag niet gelijk zijn aan je e-mailadres of gebruikersnaam. Er mogen geen pipes (|) en backslashes ( \\ ) in het wachtwoord worden gebruikt.";

		public static string ContactFormTitle = "Contactformulier";

		public static string QuitApplicationMessage = "Deze actie zal de applicatie afsluiten en een webbrowser starten. Wil je verder gaan?";

		public static string InvitationViewTitle = "Je hebt een uitnodiging ontvangen om je kaart te koppelen met een andere spaarder! Klik om te accepteren of te weigeren.";

		public static string TitleEasterEggMatthijs = "Bedankt!";
		public static string TextEasterEggMatthijs = "Hi Matthijs! Bedankt voor het overboeken van 100 Air Miles naar Mike, Martijn en Rogier!";

		public static string TitleNewCardButton = "Ik heb nog geen kaartnummer. \n Kaartnummer aanmaken.";

		public static string QuitLinkcardsTitle = "Stoppen kaart koppelen";
		public static string QuitLinkcardsMessage = "Weet je het zeker? Je eerder gespaarde Air Miles kun je niet meenemen. In overleg met de andere spaarder(s) is het wel mogelijk Air Miles over te boeken. Je transacties zijn na het stoppen met koppelen ook niet meer zichtbaar.";

        public static string ReplaceCardMessage = "Weet je het zeker? Je wordt doorgestuurd naar de site van Air Miles en dient daar eerst in te loggen om je kaart te kunnen vervangen.";
        public static string BlockCardPhysicalMessage = "Weet je het zeker? Om je kaart te blokkeren en een fysieke kaart aan te vragen word je doorgestuurd naar de site van Air Miles en die jet daar eerst in te loggen.";
        public static string BlockCardExplanation = "Ben je je kaart verloren of is deze gestolen? Blokkeer dan hier en voorkom misbruik.Na blokkeren wordt er direct een gratis nieuwe Air Miles kaart met nieuw kaartnummer voor je aangevraagd. Wij zorgen er dan voor dat de gegevens en het saldo van je geblokkeerde kaart worden overgezet en dat je weer volledig van Air Miles gebruik kunt maken.";

        public static string ReplaceCardExtraMessage = "Het vervangen van je fysieke Air Miles kaart kost 250 Air Miles. Deze worden meteen afgeschreven van je saldo.";
        public static string ReplaceCardNotEnoughMessage = "Het Air Miles saldo is ontoereikend om een vervangende kaart aan te vragen. Ga naar de Air Miles website voor meer informatie. Op de site dien je echter opnieuw in te loggen.";
        public static string BlockCardExtraMessage = "Het blokkeren en vervangen van je fysieke Air Miles kaart kost 250 Air Miles. Deze worden meteen afgeschreven van je saldo.";

	}
}