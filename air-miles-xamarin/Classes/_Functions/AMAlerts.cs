﻿using System;
using Air_Miles_Xamarin;
using Foundation;
using UIKit;

namespace airmilesxamarin
{
	public class AMAlerts
	{
		public static void DefaultAlert(UIViewController Parent, string Title, string Message)
		{
			//Create Alert
			UIAlertController AlertController = UIAlertController.Create(Title, Message, UIAlertControllerStyle.Alert);

			//Add Action
			AlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

			// Present Alert
			Parent.PresentViewController(AlertController, true, null);
		}

		public static void P002Alert(UIViewController Parent)
		{
			//Create Alert
			UIAlertController AlertController = UIAlertController.Create(AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginP02, UIAlertControllerStyle.Alert);

			//Add Actions
			AlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

			//Present Alert
			Parent.PresentViewController(AlertController, true, null);
		}

		public static void P004Alert(UIViewController Parent)
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create(AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginP04, UIAlertControllerStyle.Alert);

			//Add Actions
			okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));
			okCancelAlertController.AddAction(UIAlertAction.Create("Contact", UIAlertActionStyle.Default, alert => UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLContactForm))));

			//Present Alert
			Parent.PresentViewController(okCancelAlertController, true, null);
		}

		public static void P005Alert(UIViewController Parent)
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create(AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginP05, UIAlertControllerStyle.Alert);

			//Add Actions
			okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));
			okCancelAlertController.AddAction(UIAlertAction.Create("Wachtwoord aanvragen", UIAlertActionStyle.Default, alert => UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset))));

			//Present Alert
			Parent.PresentViewController(okCancelAlertController, true, null);
		}

		public static void P006Alert(UIViewController Parent)
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create(AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginP06, UIAlertControllerStyle.Alert);

			//Add Actions
			okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));
			okCancelAlertController.AddAction(UIAlertAction.Create("Contact", UIAlertActionStyle.Default, alert => UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLContactForm))));

			//Present Alert
			Parent.PresentViewController(okCancelAlertController, true, null);
		}

		public static void PromotionAlert(UIViewController Parent, string title, string url)
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create(title, AMStrings.QuitApplicationMessage, UIAlertControllerStyle.Alert);

			//Add Actions
			okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => UIApplication.SharedApplication.OpenUrl(new NSUrl(url))));
			okCancelAlertController.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));

			//Present Alert
			Parent.PresentViewController(okCancelAlertController, true, null);
		}

	}
}

