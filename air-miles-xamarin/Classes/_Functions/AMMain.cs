﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using CoreGraphics;
using Foundation;
using Google.Analytics;
using LocalAuthentication;
using Newtonsoft.Json;
using PassKit;
using SafariServices;
using UIKit;

namespace airmilesxamarin
{
    public class AMMain
	{

		public AMMain()
		{
		}

		public static CultureInfo Cultureinfo = new CultureInfo("nl-NL");

		/// <summary>
		/// Dates to time.
		/// </summary>
		/// <returns>The to time.</returns>
		/// <param name="datum">Datum.</param>
		public static DateTime DateToTime(float datum)
		{
			DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return epoch.AddMilliseconds(datum);
		}

		/// <summary>
		/// Sets the balance format.
		/// </summary>
		/// <returns>The balance format.</returns>
		/// <param name="balance">Balance.</param>
		public static string SetBalanceFormat(string balance)
		{
			string correctBalance;

			// remove minus 
			if(balance.Substring(0, 1) == "-"){
				correctBalance = balance.Replace("-", "");

			}
			else {
				correctBalance = balance;
			}

			if (correctBalance != null)
			{
				if (correctBalance.Length > 3)
				{
					int balanceLength = balance.Length;
					int numberOfDots = Convert.ToInt16(Math.Floor((balanceLength - 1) / 3.0));
					if (numberOfDots == 0)
					{
						return balance;
					}

					string outputString = "";
					int startPos = 0;

					for (int i = 0; i < numberOfDots + 1; i++)
					{
						int length = (i == 0 ? balanceLength % 3 : 3);
						if (length == 0)
						{
							length = 3;
						}

						outputString += balance.Substring(startPos, length) + ".";

						startPos = startPos + length;
					}
					outputString = outputString.Remove(outputString.Length - 1);

					return outputString;
				}
				else {
					return balance;
				}
			}

			return "0";
		}

		/// <summary>
		/// Sets the expired session.
		/// </summary>
		/// <returns>The expired session.</returns>
		/// <param name="NavigationController">Navigation controller.</param>
		public static void SetExpiredSession(UINavigationController NavigationController, string routing) 		{
            try
            {
	            // 5 minutes
	            int SessionSeconds = 300;  				Console.WriteLine("Set Expired session \n Login = " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) + "\n Session = " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session));  				// Session is invalid and the user has been inactive for longer than SessionSeconds 				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session) == false /*&& Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SessionDateTimeClose)) < DateTime.Now.AddSeconds(-SessionSeconds)*/) 				{ 					Console.WriteLine("Session duration is "+DateTime.Now.AddSeconds(-SessionSeconds)); 					Console.WriteLine("Session datetime close is "+Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SessionDateTimeClose))); 					// Check the device supports Touch ID   					EnterPinTouchNavigationViewController Redirect = App.Storyboard.InstantiateViewController("EnterPinTouchNavigationViewController") as EnterPinTouchNavigationViewController;  					if (!string.IsNullOrEmpty(routing)) 					{ 						Console.WriteLine("on success we will be routing to: " + routing); 						Redirect.Routing = routing; 					} 					else { 						Console.WriteLine("there is no routing!"); 					}  					NavigationController.PresentModalViewController(Redirect, false); 					//NavigationController.PushViewController(Redirect, false); 				} 				else { 					 					if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session)) 					{
						Console.WriteLine("Session is true"); 						NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.Session); 					}
				 				}
			}
			catch (Exception ex)
			{
                Console.WriteLine("excepetion set expired "+ex);
            } 		}

		public static bool RedirectToCard()
		{
            try
            {
	            int SessionSeconds = 300;

				if (Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SessionDateTimeClose)) < DateTime.Now.AddSeconds(-SessionSeconds))
				{
					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
                Console.WriteLine("exception redirect to card "+ex);
                return false;
			}
		}

		public static bool DoWebService(string service)
		{
			int SessionSeconds = 3600;

			try
			{
				if (Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.DateTimeLastCall + service)) > DateTime.Now.AddSeconds(-SessionSeconds))
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				return true;
			}
		}

		public static bool NewSession { get; set; }

		/// <summary>
		/// Generates the ean.
		/// </summary>
		/// <returns>The ean.</returns>
		/// <param name="cardNumber">Card number.</param>
		public static string GenerateEan(string cardNumber)
		{
			int even = 0;
			int oneven = 0;

			string result = "260" + cardNumber;

			for (int i = 0; i < result.Length; i++)
			{
				if (i % 2 == 0)
				{
					oneven += Int32.Parse(result[i].ToString());
				}
				else {
					even += Int32.Parse(result[i].ToString());
				}
			}
			int controleGetal;
			int total = ((even * 3) + oneven) % 10;

			if (total != 0)
			{
				controleGetal = (10 - total);
			}
			else {
				controleGetal = 0;
			}

			return result + controleGetal;
		}

		/// <summary>
		/// Removes the HTML tags.
		/// </summary>
		/// <returns>The HTML tags.</returns>
		/// <param name="input">Input.</param>
		public static string RemoveHTMLTags(string input)
		{
			if (input != null)
			{
				string regexString = input;
				Regex regex = new Regex(@"<(.*?)>");
				int length = regex.Match(regexString).Length;
				while (length > 0)
				{
					regexString = regexString.Remove(regexString.IndexOf('<'), length);
					regex = new Regex(@"<(.*?)>");
					length = regex.Match(regexString).Length;
				}

				return WebUtility.HtmlDecode(regexString);
			}

			return null;
			//@"<(.*?)>"
		}

		public static string ReplaceBreakpoints(String input)
		{
			input = input.Replace("<br />", " ");
			input = input.Replace("<br>", " ");
			input = input.Replace("  ", " ");

			return input;
		}


		/// <summary>
		/// Add toolbar with Done button
		/// </summary>
		public static void AddDoneButton(UITextField textfield)
		{
			UIToolbar toolbar = new UIToolbar(new CGRect(0.0f, 0.0f, 50.0f, 44.0f));

			UIButton button = new UIButton(UIButtonType.Custom);
			button.SetTitle("GEREED", UIControlState.Normal);
			button.Font = UIFont.FromName("Avenir-Black", 12f);
			button.SizeToFit();
			button.SetTitleColor(UIColor.FromRGB(0, 125, 195), UIControlState.Normal);

			button.TouchUpInside += delegate
			{
				textfield.ResignFirstResponder();
			};

			UIBarButtonItem barButtonItem = new UIBarButtonItem(button);

			toolbar.Items = new UIBarButtonItem[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				barButtonItem
			};

			textfield.InputAccessoryView = toolbar;
		}


		public static void AddNextButton(UITextField textfield, UITextField nextTextfield)
		{
			UIToolbar toolbar = new UIToolbar(new CGRect(0.0f, 0.0f, 50.0f, 44.0f));

			UIButton button = new UIButton(UIButtonType.Custom);
			button.SetTitle("VOLGENDE", UIControlState.Normal);
			button.Font = UIFont.FromName("Avenir-Black", 12f);
			button.SizeToFit();
			button.SetTitleColor(UIColor.FromRGB(0, 125, 195), UIControlState.Normal);

			button.TouchUpInside += delegate
			{
				textfield.ResignFirstResponder();
				nextTextfield.BecomeFirstResponder();
			};

			UIBarButtonItem barButtonItem = new UIBarButtonItem(button);

			toolbar.Items = new UIBarButtonItem[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				barButtonItem
			};

			textfield.InputAccessoryView = toolbar;
		}

		public static void AnimateBorder(UIView view, nfloat x, nfloat y)
		{
			UIView.Animate(
				duration: 0.1,
				delay: 0,
				options: UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.CurveEaseInOut,
				animation: () =>
				{
					view.Center =
						new CGPoint(x, y);
				},
				completion: () =>
				{
					view.Center =
						new CGPoint(x, y);
				}
			);
		}

		/// <summary>
		/// Shows the first pin touc alert.
		/// </summary>
		public static void ShowFirstPinToucAlert(UIViewController parent)
		{
			LAContext Context = new LAContext();
			NSError Error;
			UINavigationController navctrlr;

			if (parent == null)
			{
				navctrlr = new UINavigationController();
				parent = navctrlr;
			}

			// Check if user has Touch ID
			if (Context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out Error))
			{
				Console.WriteLine("TOUCH IS LOCALSTORE = " + NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertTouchID));

				if (!NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertTouchID))
				{
					Console.WriteLine("navigation controller parent firstpintouch" + parent);
					// Redirect to detail page
					TouchIdNavigationViewController TouchIdModal = App.Storyboard.InstantiateViewController("TouchIdNavigationViewController") as TouchIdNavigationViewController;
					parent.PresentModalViewController(TouchIdModal, true);
					// parent.PushViewC...
					return;
				}
			}
			else {

				Console.WriteLine("PINCODE IS LOCALSTORE = " + NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertTouchID));

				if (!NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertPinID))
				{
					Console.WriteLine("navigation controller parent firstpintouch" + parent);

					Console.WriteLine("bool firstalertpin "+NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertPinID));
					// Redirect to detail page
					PincodeNavigationViewController PinIdModal = App.Storyboard.InstantiateViewController("PincodeNavigationViewController") as PincodeNavigationViewController;
					parent.PresentModalViewController(PinIdModal, true);

					return;
				}
			}
		}


		public static void ShowFirstKaartAanmaken(UIViewController parent)
		{

			StartKaartAanvragenViewController FirstKaartModal = App.Storyboard.InstantiateViewController("StartKaartAanvragenViewController") as StartKaartAanvragenViewController;
			parent.PresentModalViewController(FirstKaartModal, true);
				
		}

		public static UIImageView UpdateAvatar(UIImageView image, string placeholder)
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar))));
			}
			else {
				image.Image = UIImage.FromBundle(placeholder);
			}

			return image;
		}


		public static UIImageView SetAvatar(UIImageView image, string placeholder)
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar))));
			}
			else {
				image.Image = UIImage.FromBundle(placeholder);
			}

			image.Layer.CornerRadius = image.Bounds.Height / 2;
			image.Layer.MasksToBounds = true;

			return image;
		}

		public static UIImageView SetAvatarLinkCards (UIImageView image, string imageString, string placeholder, nfloat height)
		{
			image.Frame = new CGRect(100, 165, height, height);
			if (!string.IsNullOrEmpty(imageString))
			{
				image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(imageString)));
		/*public static UIImageView SetAvatarAvatarPicker(UIImageView image, string placeholder)
		{
			image.Frame = new CGRect(100, 150, UIScreen.MainScreen.Bounds.Width - 200, UIScreen.MainScreen.Bounds.Width - 200);

			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar))));
			*/
			}
			else {
				image.Image = UIImage.FromBundle(placeholder);
			}

			image.Layer.CornerRadius = image.Bounds.Height / 2;
			image.Layer.MasksToBounds = true;

			return image;
		}

		public static UIImageView SetAvatarAvatarPicker(UIImageView image, string placeholder)
		{
			image.Frame = new CGRect(100, 150, UIScreen.MainScreen.Bounds.Width - 200, UIScreen.MainScreen.Bounds.Width - 200);

			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				image.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar))));
			
			}
			else {
				image.Image = UIImage.FromBundle(placeholder);
			}

			image.Layer.CornerRadius = image.Bounds.Height / 2;
			image.Layer.MasksToBounds = true;

			return image;
		}

		public static string AvatarImageToBase64(NSUrl imagepath)
		{
			if (imagepath!=null)
			{
				FileStream fs = new FileStream(imagepath.Path, FileMode.Open, FileAccess.Read);
				byte[] ImageData = new byte[fs.Length];
				fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
				fs.Close();
				string _base64String = Convert.ToBase64String(ImageData);

				return _base64String;
			}
			return null;
		}

		public static UIButton SetAvatarButton (UIButton imagebutton, string placeholder)
		{
			imagebutton = UIButton.FromType(UIButtonType.Custom);
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				imagebutton.SetImage(UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))), UIControlState.Normal);
			}
			else {
				imagebutton.SetImage(UIImage.FromBundle(placeholder), UIControlState.Normal);
			}

			imagebutton.Layer.CornerRadius = imagebutton.Bounds.Height / 2;
			imagebutton.Layer.MasksToBounds = true;

			return imagebutton;
		}

		public static UIButton UpdateAvatarButton(UIButton imagebutton, string placeholder)
		{
			imagebutton = UIButton.FromType(UIButtonType.Custom);
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				imagebutton.SetImage(UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))), UIControlState.Normal);
			}
			else {
				imagebutton.SetImage(UIImage.FromBundle(placeholder), UIControlState.Normal);
			}

			return imagebutton;
		}

		public static void ClearAanbodTabBadge()
		{
			try {
			 // Remove tabbaritem
				if (App.TabBar.ViewControllers[2].TabBarItem != null)
					{
						Console.WriteLine("clear tab badge");
						App.TabBar.ViewControllers[2].TabBarItem.BadgeValue = null;
					}
				App.Notification.ApplicationIconBadgeNumber = 0;
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
				NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.UnseenGifts);
				NSUserDefaults.StandardUserDefaults.Synchronize();
				}
				catch (Exception ex)
				{
				Console.WriteLine("exception clearaanbodtabbadge "+ ex);

				}
		}

		public static void SetAanbodTabBadge(nint label)
		{
			// Remove tabbaritem
			if (label == 0)
			{
				ClearAanbodTabBadge();
			}
			else {
				App.TabBar.ViewControllers[2].TabBarItem.BadgeValue = label.ToString();
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = label;
			}
		}

		public static async Task<int> GetGiftsForBadge(UIViewController Controller)
		{
			int seenCounter = 0;
			int UnseenGifts = 0;
			int PersonalGifts = 0;
			string SeenGifts = "";
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				NetworkStatus Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					try
					{
						// Localstorage for seen gift
						if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.SeenGifts)))
						{
							SeenGifts = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.SeenGifts);
						}
						string[] gifts = new string[] { };

						if (!string.IsNullOrEmpty(SeenGifts))
						{
							gifts = SeenGifts.Split(',');
						};


						// GET GIFTS
						GiftsService GiftService = new GiftsService();
						GiftsRoot Gifts = await GiftService.GetGiftData();

						if (Gifts.Response != null && Gifts.Errors[0].Code == "E00")
						{
							if (Gifts.Response.Length > 0)
							{
								if (!string.IsNullOrEmpty(SeenGifts))
								{
									Console.WriteLine("{0} seen gifts", gifts.Length);

									for (int i = 0; i < Gifts.Response.Length; i++)
									{
										if (SeenGifts.Contains(Gifts.Response[i].Offer[0].OfferNr))
										{
											seenCounter++;
										}
									}
								}
							}
						}
						if (Gifts.Response != null && Gifts.Errors[0].Code == "E23")
						{
							if (Controller != null && Controller.GetType() == typeof(AanbodViewController))
							{
								Console.WriteLine("E23 Sessie verlopen");

								// Logout
								LocalStorage.Logout(Controller.TabBarController);

								Controller.NavigationController.PopToRootViewController(false);

								// Redirect to login page
								AMAlerts.DefaultAlert(Controller, AMStrings.ErrorSessionExpiredTitle, AMStrings.ErrorSessionExpiredMessage);
							}
						}


						// GET PERSONALIZED PROMOTIONS

						PersonalizedPromotionsService PersonalizedPromotionsService = new PersonalizedPromotionsService();
						MendixPersonalizedPromotionRoot PersonalPromotions = await PersonalizedPromotionsService.GetPersonalizedPromotions();

						if (PersonalPromotions.Response != null && PersonalPromotions.Errors[0].Code == "E00")
						{
							if (PersonalPromotions.Response.Length > 0)
							{
								if (!string.IsNullOrEmpty(SeenGifts))
								{
									for (int i = 0; i < PersonalPromotions.Response.Length; i++)
									{
										string productId = "";

										// check if type is campain or product
										if (!string.IsNullOrEmpty(PersonalPromotions.Response[i].CampaignID))
										{
											productId = PersonalPromotions.Response[i].CampaignID;
										}
										else if (!string.IsNullOrEmpty(PersonalPromotions.Response[i].ProductID))
										{
											productId = PersonalPromotions.Response[i].ProductID;
										}

										// see if product id is already in seengifts
										if (SeenGifts.Contains(productId))
										{
											seenCounter++;
										}
									}
								}
							}
						}

						if (PersonalPromotions.Response != null && Gifts.Response != null)
						{
							UnseenGifts = (Gifts.Response.Length + PersonalPromotions.Response.Length) - seenCounter;
							PersonalGifts = Gifts.Response.Length + PersonalPromotions.Response.Length;
							Console.WriteLine("seen gifts (" + gifts.Length + ") = " + SeenGifts + ", counter = " + seenCounter + ", total + " + UnseenGifts);

							if (UnseenGifts > 0)
							{
								SetAanbodTabBadge(UnseenGifts);
								NSUserDefaults.StandardUserDefaults.SetInt(UnseenGifts, AMLocalStorage.UnseenGifts);
							}
							else
							{
								ClearAanbodTabBadge();
								NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.UnseenGifts);
							}
						}
						else
						{
							ClearAanbodTabBadge();
							NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.UnseenGifts);
						}
					}
					catch (Exception e)
					{
						Console.WriteLine("getgiftsforbadge " + e);
					}

				}
			}
			return PersonalGifts;
		}

		public static List<string> GetPersonalGiftsPartners()
		{
			// create list where hippo-calls are stored
			List<string> GiftsPartners = new List<string>();

			string OfferGiftJson = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);

			if (!string.IsNullOrEmpty(OfferGiftJson))
			{
				GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(OfferGiftJson);

				if (Gifts.Response.Length > 0)
				{
					GiftsOffer[] GiftsOffers = Gifts.Response[0].Offer;

					int GiftsOffersAmount = GiftsOffers.Length;

					// loop over all offers and collect all the different partnernames
					for (int i = 0; i < GiftsOffers.Length; i++)
					{
						string partnerName = GiftsOffers[i].Partner;
						if (!GiftsPartners.Contains(partnerName))
						{
							Console.WriteLine($"add {partnerName} to list");

							// add the partnername to the list
							GiftsPartners.Add(partnerName);
						}
					}

					Console.WriteLine("amount of partners " + GiftsPartners.Count);

					return GiftsPartners;
				}

				return null;
			}

			return null;
		}

		/// <summary>
		/// Sets the seen gifts.
		/// </summary>
		/// <param name="giftId">Gift identifier.</param>
		public static void SetSeenGifts(string giftId)
		{

			Console.WriteLine("Set this gift as seen: " + giftId);
			// set gifts in localstorage
			string SeenGifts = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.SeenGifts);

			if (!string.IsNullOrEmpty(SeenGifts))
			{
				// if more than 1 seen gifts
				if (SeenGifts.Contains(","))
				{
					if (!SeenGifts.Contains("," + giftId))
					{
						SeenGifts += "," + giftId;
					}
				}
				else {
					if (!string.IsNullOrEmpty(giftId))
					{
						if (!SeenGifts.Contains(giftId))
						{
							SeenGifts += "," + giftId;
						}
					}
				}
			}
			else {
				SeenGifts = giftId;
			}
			if (!string.IsNullOrEmpty(SeenGifts))
			{
				NSUserDefaults.StandardUserDefaults.SetString(SeenGifts, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.SeenGifts);
			}
		}

		public static void UpdateTokenApp(string token)
		{
			if (!string.IsNullOrEmpty(token))
			{
				NSUserDefaults.StandardUserDefaults.SetString(token, AMLocalStorage.SecureTokenApp);

				//KeychainHelpers.SetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), token, AMLocalStorage.SecureTokenApp, Security.SecAccessible.WhenUnlockedThisDeviceOnly, true);

			}
		}

		public static void UpdateBounceIndicators(ResponseTokenCheck response)
		{
			List<string> bounceIndicators = new List<string>();
			if (!string.IsNullOrEmpty(response.EmailBounceMessage))
			{
				NSUserDefaults.StandardUserDefaults.SetString(response.EmailBounceMessage, AMLocalStorage.EmailBounceReason);
				if (!string.IsNullOrEmpty(response.EmailBounceMessage))
				{
					bounceIndicators.Add(response.EmailBounceMessage);
				}
			}
			else if (string.IsNullOrEmpty(response.EmailBounceMessage))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.EmailBounceReason);
				}
			}
			if (!string.IsNullOrEmpty(response.MobileNrBounceMessage))
			{
				NSUserDefaults.StandardUserDefaults.SetString(response.MobileNrBounceMessage, AMLocalStorage.MobilePhoneBounceReason);
				if (!string.IsNullOrEmpty(response.MobileNrBounceMessage))
				{
					bounceIndicators.Add(response.MobileNrBounceMessage);
				}
			}
			else if (string.IsNullOrEmpty(response.MobileNrBounceMessage))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.MobilePhoneBounceReason);
				}
			}
			if (!string.IsNullOrEmpty(response.PrivateNrBounceMessage))
			{
				NSUserDefaults.StandardUserDefaults.SetString(response.PrivateNrBounceMessage, AMLocalStorage.PrivatePhoneBounceReason);
				if (!string.IsNullOrEmpty(response.PrivateNrBounceMessage))
				{
					bounceIndicators.Add(response.PrivateNrBounceMessage);
				}
			}
			else if (string.IsNullOrEmpty(response.PrivateNrBounceMessage))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.PrivatePhoneBounceReason);
				}
			}
			if (!string.IsNullOrEmpty(response.AddressBounceMessage))
			{
				NSUserDefaults.StandardUserDefaults.SetString(response.AddressBounceMessage, AMLocalStorage.AddressBounceReason);
				if (!string.IsNullOrEmpty(response.AddressBounceMessage))
				{
					//bounceIndicators.Add(response.AddressBounceMessage);
				}
			}
			else if (string.IsNullOrEmpty(response.AddressBounceMessage))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.AddressBounceReason);
				}
			}
            string combindedBounceIndicators = string.Join(Environment.NewLine, bounceIndicators);
            // Google analaytics
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Bounceindicator", combindedBounceIndicators, null).Build();
            Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
            Gai.SharedInstance.Dispatch();

			NSUserDefaults.StandardUserDefaults.SetBool(response.AddressBouncePresent, AMLocalStorage.AddressBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(response.EmailBouncePresent, AMLocalStorage.EmailBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(response.PrivateNrBouncePresent, AMLocalStorage.PrivateNrBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(response.MobileNrBouncePresent, AMLocalStorage.MobileNrBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetInt(bounceIndicators.Count, AMLocalStorage.AmountBounceIndicators);
			NSUserDefaults.StandardUserDefaults.SetBool(response.DigitalCard, AMLocalStorage.DigitalAccount);

			if (!string.IsNullOrEmpty(response.CardNumber))
			{
				NSUserDefaults.StandardUserDefaults.SetString(response.CardNumber, AMLocalStorage.CardNumber);
				NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
				shared.SetString(response.CardNumber, AMLocalStorage.CardNumber);
				shared.Synchronize();
			}

			NSUserDefaults.StandardUserDefaults.Synchronize();
		}


		/// <summary>
		/// check if the task is canceled by the user then return false
		/// </summary>
		/// <returns><c>true</c>, if normal exception <c>false</c> if taskcanceledexeption.</returns>
		public static bool CanShowError()
		{
			Exception e = SharedWebServices.ErrorException;
			SharedWebServices.ErrorException = null;

			CancellationTokenSource errorToken = SharedWebServices.LastCancelToken;
			SharedWebServices.LastCancelToken = null;

			if (e != null && errorToken != null)
			{
				if (e is System.Threading.Tasks.TaskCanceledException)
				{
					if (errorToken.IsCancellationRequested)
						return false;
					else
						Console.WriteLine("cancelation not requested");
				}
			}

			return true;
		}

		public static void ShowFiles()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Documents folder
			var libRary = Path.Combine(documentsPath, "..", "Library");
			var directories = Directory.EnumerateDirectories("./");
			foreach (var directory in directories)
			{
				Console.WriteLine(directory);
			}
			var files = Directory.EnumerateFiles("./");
			foreach (var directory in files)
			{
				Console.WriteLine("file "+directory);
			}
		}

		public static void ShowPass(string filename)
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Documents folder
			var libRary = Path.Combine(documentsPath, "..", "Library");
			var passPath = Path.Combine(libRary, filename);
			Console.WriteLine("pass	"+File.ReadAllBytes(passPath).Length);

		}

        public static void OpenURLInApp(UIViewController Parent, string url){
			// Create an instance of the Safari Services View Controller
			var sfvc = new SFSafariViewController(new NSUrl(url), true);
            sfvc.PreferredBarTintColor = UIColor.FromRGB(0, 125, 195);

			Parent.PresentViewController(sfvc, true, null);
        }

		public static void OpenPassbookViewController(UINavigationController NavigationController, string filepath, string filename)
		{
			if (PKPassLibrary.IsAvailable)
			{
				PKPassLibrary library = new PKPassLibrary();
				var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
				var libRary = Path.Combine(documentsPath, "..", "Library");
				var newFilePath = Path.Combine(libRary, filename);
				var builtInPassPath = Path.Combine(System.Environment.CurrentDirectory, filename);
				if (!System.IO.File.Exists(filepath))
					System.IO.File.Copy(builtInPassPath, newFilePath);

				NSData nsdata;
				using (FileStream oStream = File.Open(filepath, FileMode.Open))
				{
					nsdata = NSData.FromStream(oStream);
				}

				var err = new NSError(new NSString("42"), -42);
				var newPass = new PKPass(nsdata, out err);


				var pkapvc = new PKAddPassesViewController(newPass);
				NavigationController.PresentModalViewController(pkapvc, true);
				
			}
		}

        public static string getLatitudePartner(string partnername)
        {
            string latitude = "";
            switch (partnername)
            {
                case "Wildlands Adventure Zoo Emmen":
                    latitude = "52.7826141";
                    break;
				case "Efteling":
					latitude = "51.649528";
					break;
				case "Tikibad":
					latitude = "52.146196";
					break;
				case "Duinrell":
					latitude = "52.146019";
					break;
				case "Dolfinarium":
					latitude = "52.354366";
					break;
				case "Beekse Bergen":
					latitude = "51.5230477";
					break;
				//case "Vue Cinemas":
					//latitude = "52.0672031";
					//break;
				default:
                    latitude = "0";
                    break;
            }

            return latitude;
        }

		public static string getLongitudePartner(string partnername)
		{
			string longitude = "";
            switch (partnername)
			{
                case "Wildlands Adventure Zoo Emmen":
					longitude = "6.8886209";
					break;
				case "Efteling":
					longitude = "5.043712";
					break;
				case "Tikibad":
					longitude = "4.380884";
					break;
				case "Duinrell":
					longitude = "4.387302";
					break;
				case "Dolfinarium":
					longitude = "5.618987";
					break;
				case "Beekse Bergen":
					longitude = "5.1131812";
					break;
				//case "Vue Cinemas":
					//longitude = "5.0845735";
					//break;
				default:
					longitude = "0";
					break;
			}

			return longitude;
		}
	}

}