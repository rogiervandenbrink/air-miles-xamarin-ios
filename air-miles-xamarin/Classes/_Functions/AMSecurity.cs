﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace airmilesxamarin
{
	public class AMSecurity
	{
		public static SecureRequest GenerateSecureRequest(string text)
		{
			SecureRequest Request = new SecureRequest
			{
				Inputstring = text
			};

			return Request;
		}

		public static string GenerateSHA1(string Hash)
		{
			SHA1 SH1 = SHA1.Create();
			//UTF8Encoding encoding = new UTF8Encoding();
			byte[] Bytes = Encoding.UTF8.GetBytes(Hash);
			byte[] Result = SH1.ComputeHash(Bytes);

			var sb = new StringBuilder();
			foreach (byte b in Result)
			{
				var hex = b.ToString("x2");
				sb.Append(hex);
			}
			return sb.ToString();
		}

	}
}
