﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class AvatarRoot 	{ 		[JsonProperty(PropertyName = "Respons_ProfielAfbeelding")] 		public Avatar[] Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Avatar 	{ 		[JsonProperty(PropertyName = "Image")] 		public string Image; 	} }
