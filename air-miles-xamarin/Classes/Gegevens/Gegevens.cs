﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class Gegevens
	{
		public static string Aanhef { get; set; }
		public static string Initialen { get; set; }
		public static string Voornaam { get; set; }
		public static string Tussenvoegsel { get; set; }
		public static string Achternaam { get; set; }
		public static nint Geboortedatum { get; set; }
		public static long GeboortedatumFloat { get; set; }
		public static string Email { get; set; }
		public static string Telefoon { get; set; }
		public static string Mobiel { get; set; }
		public static string Straat { get; set; }
		public static string Huisnummer { get; set; }
		public static string HuisnummerToevoeging { get; set; }
		public static string Plaats { get; set; }
		public static string Postcode { get; set; }
		public static string Land { get; set; }
		public static bool Newsletter { get; set; }
		public static bool AppNotifications { get; set; }

	}

	public class GegevensRoot
	{
		[JsonProperty(PropertyName = "Respons_Inloggen")]
		public Gegevens[] Gegevens { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }

		[JsonProperty(PropertyName = "Respons_Profielwijziging")]
		public Gegevenswijziging[] Wijziging { get; set; }
	}

	public class Gegevenswijziging
	{
		[JsonProperty(PropertyName = "CardNumber")]
		public string Cardnumber { get; set; }

		[JsonProperty(PropertyName = "HouseNumber")]
		public string HouseNumber { get; set; }

		[JsonProperty(PropertyName = "Email")]
		public string Email { get; set; }

		[JsonProperty(PropertyName = "TelephoneNrPrivate")]
		public string TelephoneNrPrivate { get; set; }

		[JsonProperty(PropertyName = "FirstName")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "TelephoneNrMobile")]
		public string TelephoneNrMobile { get; set; }

		[JsonProperty(PropertyName = "PostalCode")]
		public string PostalCode { get; set; }

		[JsonProperty(PropertyName = "City")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "Prefix")]
		public string Prefix { get; set; }

		[JsonProperty(PropertyName = "Initials")]
		public string Initials { get; set; }

		[JsonProperty(PropertyName = "Salutation")]
		public string Salutation { get; set; }

		[JsonProperty(PropertyName = "HouseNumberSuppliment")]
		public string HouseNumberSuppliment { get; set; }

		[JsonProperty(PropertyName = "OptIn")]
		public bool OptIn { get; set; }

        [JsonProperty(PropertyName = "Profiling")]
        public bool Profiling { get; set; }

		[JsonProperty(PropertyName = "FullName")]
		public string FullName { get; set; }

		[JsonProperty(PropertyName = "Street")]
		public string Street { get; set; }

		[JsonProperty(PropertyName = "Country")]
		public string Country { get; set; }

		[JsonProperty(PropertyName = "LastName")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "BirthDate")]
		public string BirthDate { get; set; }

		[JsonProperty(PropertyName = "EmailBounceMessage")]
		public string EmailBounceMessage { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBounceMessage")]
		public string PrivateNrBounceMessage { get; set; }

		[JsonProperty(PropertyName = "AddressBounceMessage")]
		public string AddressBounceMessage { get; set; }

		[JsonProperty(PropertyName = "MobileNrBounceMessage")]
		public string MobileNrBounceMessage { get; set; }

		[JsonProperty(PropertyName = "MobileNrBouncePresent")]
		public bool MobileNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "EmailBouncePresent")]
		public bool EmailBouncePresent { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBouncePresent")]
		public bool PrivateNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "AddressBouncePresent")]
		public bool AddressBouncePresent { get; set; }


	}
}

