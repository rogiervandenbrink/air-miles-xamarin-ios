﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class GingerKeyRoot 	{ 		[JsonProperty(PropertyName = "Respons_SleutelGP")] 		public GingerKey Response { get; set; }  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }
 	}  	public class GingerKey 	{ 		[JsonProperty(PropertyName = "Key")] 		public string Key { get; set; } 	} }
