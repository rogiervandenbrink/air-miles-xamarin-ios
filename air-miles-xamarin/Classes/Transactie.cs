﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class TransactiesRoot
	{
		[JsonProperty(PropertyName = "Respons_Transacties")]
		public TransactiesResponse[] Response { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }

		[JsonProperty(PropertyName = "Respons_Token")]
		public ResponseToken TokenApp { get; set; }
	}

	public class TransactiesResponse
	{
		[JsonProperty(PropertyName = "Transactions")]
		public Transactie[] Transacties { get; set; }
	}

	public class Transactie
	{
		[JsonProperty(PropertyName = "PostingDate")]
		public long Date { get; set; }

		[JsonProperty(PropertyName = "TransactionDescription")]
		public string Title { get; set; }

		[JsonProperty(PropertyName = "Points")]
		public string Points { get; set; }

		[JsonProperty(PropertyName = "NameOrganisation")]
		public string Organisation { get; set; }

		[JsonProperty(PropertyName = "NameCharity")]
		public string NameCharity { get; set; }

		[JsonProperty(PropertyName = "Initials")]
		public string Initials { get; set; }

		[JsonProperty(PropertyName = "Prefix")]
		public string Prefix { get; set; }

		[JsonProperty(PropertyName = "LastNameMember")]
		public string LastNameMember { get; set; }

		[JsonProperty(PropertyName = "ReferenceMemberID")]
		public string ReferenceMemberID { get; set; }

		[JsonProperty(PropertyName = "NameAccount")]
		public string NameAccount { get; set; }
	}
}