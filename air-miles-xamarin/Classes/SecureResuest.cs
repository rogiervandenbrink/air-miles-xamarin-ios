﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class SecureRequest
	{
		[JsonProperty(PropertyName = "Inputstring")]
		public string Inputstring { get; set; }
	}
}

