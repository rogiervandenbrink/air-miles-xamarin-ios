﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public enum PromotieType
	{
		Offer,
		Gift,
		PersonalizedPromotion,
		MarketingPromotion,
	}

	public class AanbodPromoties
	{
		[JsonProperty(PropertyName = "Promotions")]
		public Promotie[] Promoties { get; set; }
	}

	public class Promotie
	{

		public PromotieType PromotionType { get; set; }

		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }

		[JsonProperty(PropertyName = "previewImage")]
		public string Image { get; set; }

		[JsonProperty(PropertyName = "logo")]
		public string Logo { get; set; }

		[JsonProperty(PropertyName = "endDate")]
		public string EndDate { get; set; }

		[JsonProperty(PropertyName = "registrationDateTo")]
		public string RegistrationDate { get; set; }

		[JsonProperty(PropertyName = "EnrolledMember")]
		public bool EnrolledMember { get; set; }

		[JsonProperty(PropertyName = "EndDateCampaign")]
		public string EndDateCampaign { get; set; }

		[JsonProperty(PropertyName = "webview")]
		public string Url { get; set; }

		[JsonProperty(PropertyName = "priceTitle")]
		public string PriceTitle { get; set; }

		[JsonProperty(PropertyName = "priceTile")]
		public string PriceTite { get; set; }

		[JsonProperty(PropertyName = "priceDetail")]
		public string PriceDetail { get; set; }

		[JsonProperty(PropertyName = "propositionTitle")]
		public string PropositionTitle { get; set; }

		[JsonProperty(PropertyName = "propositionDetail")]
		public string PropositionDetail { get; set; }

		[JsonProperty(PropertyName = "milesOnly")]
		public bool MilesOnly { get; set; }

        [JsonProperty(PropertyName = "Used")]
        public string Used { get; set; }

		[JsonProperty(PropertyName = "showLogo")]
		public bool ShowLogo { get; set; }

		[JsonProperty(PropertyName = "CancelButton")]
		public string CancelButton { get; set; }

		[JsonProperty(PropertyName = "CancelButtonMobile")]
		public string CancelButtonMobile { get; set; }

		[JsonProperty(PropertyName = "ConfirmButton")]
		public string ConfirmButton { get; set; }

		[JsonProperty(PropertyName = "productText")]
		public string ProductText { get; set; }

		[JsonProperty(PropertyName = "usedOfferTitle")]
		public string UsedOfferTitle { get; set; }

		[JsonProperty(PropertyName = "usedOfferText")]
		public string UsedOfferText { get; set; }

		[JsonProperty(PropertyName = "notActivated")]
		public bool NotActivated { get; set; }

		[JsonProperty(PropertyName = "OfferUsed")]
		public bool OfferUsed { get; set; }

		[JsonProperty(PropertyName = "conditionsText")]
		public string ConditionsText { get; set; }

		[JsonProperty(PropertyName = "PartnerName")]
		public string PartnerName { get; set; }

		[JsonProperty(PropertyName = "productTileTitle")]
		public string ProductTileTitle { get; set; }

		[JsonProperty(PropertyName = "rectangleImage")]
		public string RectangleImage { get; set; }

		[JsonProperty(PropertyName = "generalConditionsValue")]
		public string GeneralConditionsValue { get; set; }

		[JsonProperty(PropertyName = "generalConditions")]
		public string GeneralConditions { get; set; }

		[JsonProperty(PropertyName = "conditionsLabel")]
		public string ConditionsLabel { get; set; }

		[JsonProperty(PropertyName = "conditionsTitle")]
		public string ConditionsTitle { get; set; }

		[JsonProperty(PropertyName = "ConditionsButton")]
		public string ConditionsButton { get; set; }

		[JsonProperty(PropertyName = "checkConditionsLabel")]
		public string CheckConditionsLabel { get; set; }

		[JsonProperty(PropertyName = "checkConditionsButton")]
		public string CheckConditionsButton { get; set; }

		[JsonProperty(PropertyName = "activatedMessage")]
		public string ActivatedMessage { get; set; }

		[JsonProperty(PropertyName = "activatedMessageTitle")]
		public string ActivatedMessageTitle { get; set; }

		[JsonProperty(PropertyName = "keywords")]
		public string[] Keywords { get; set; }

		[JsonProperty(PropertyName = "partnerProgramLogo")]
		public string PartnerprogramLogo { get; set; }

		[JsonProperty(PropertyName = "chooseGifts")]
		public string ChooseGifts { get; set; }

		[JsonProperty(PropertyName = "giftValidUntil")]
		public string GiftValidUntil { get; set; }

		[JsonProperty(PropertyName = "periodValidUntil")]
		public string PeriodValidUntil { get; set; }

		[JsonProperty(PropertyName = "validity")]
		public string Validity { get; set; }

		[JsonProperty(PropertyName = "ItemNr")]
		public string ItemNr { get; set; }

		[JsonProperty(PropertyName = "OfferNr")]
		public string OfferNr { get; set; }

		[JsonProperty(PropertyName = "openDetails")]
		public string OpenDetails { get; set; }

		[JsonProperty(PropertyName = "paymentTitle")]
		public string PaymentTitle { get; set; }

		[JsonProperty(PropertyName = "paymentImage")]
		public string PaymentImage { get; set; }

		[JsonProperty(PropertyName = "paymentMethod")]
		public string PaymentMethod { get; set; }

		[JsonProperty(PropertyName = "productType")]
		public string ProductType { get; set; }

		[JsonProperty(PropertyName = "productId")]
		public string ProductId { get; set; }

		[JsonProperty(PropertyName = "linkPartner")]
		public string LinkPartner { get; set; }

		[JsonProperty(PropertyName = "conditions")]
		public string Conditions { get; set; }

		[JsonProperty(PropertyName = "buttonTitle")]
		public string ButtonTitle { get; set; }

		[JsonProperty(PropertyName = "deliverTime")]
		public string DeliverTime { get; set; }

		[JsonProperty(PropertyName = "startDate")]
		public string StartDate { get; set; }

		[JsonProperty(PropertyName = "paymentColor")]
		public string PaymentColor { get; set; }

		[JsonProperty(PropertyName = "storeLocator")]
		public string StoreLocator { get; set; }

		[JsonProperty(PropertyName = "openCoupon")]
		public string OpenCoupon { get; set; }

		[JsonProperty(PropertyName = "usedCouponTitle")]
		public string UsedCouponTitle { get; set; }

		[JsonProperty(PropertyName = "printButtonTitle")]
		public string PrintButtonTitle { get; set; }

		[JsonProperty(PropertyName = "previewMessageActivated")]
		public string PreviewMessageActivated { get; set; }

		[JsonProperty(PropertyName = "changeGift")]
		public string ChangeGift { get; set; }

		//[JsonProperty(PropertyName = "subCategory")]
		//public string SubCategory { get; set; }

		[JsonProperty(PropertyName = "propositionValue")]
		public string PropositionValue { get; set; }

		[JsonProperty(PropertyName = "propositionLabel")]
		public string PropositionLabel { get; set; }

		[JsonProperty(PropertyName = "priceValue")]
		public string PriceValue { get; set; }

		[JsonProperty(PropertyName = "priceLabel")]
		public string PriceLabel { get; set; }

		[JsonProperty(PropertyName = "logoUrl")]
		public string LogoUrl { get; set; }

		[JsonProperty(PropertyName = "optimizedLogoUrl")]
		public string OptimizedLogoUrl { get; set; }

		[JsonProperty(PropertyName = "ticketSecondaryImageUrl")]
		public string TicketSecondaryImageUrl { get; set; }

		[JsonProperty(PropertyName = "ticketImageUrl")]
		public string TicketImageUrl { get; set; }

		[JsonProperty(PropertyName = "productSubType")]
		public string productSubType { get; set; }

		[JsonProperty(PropertyName = "conditionsValue")]
		public string ConditionsValue { get; set; }

		[JsonProperty(PropertyName = "voucherType")]
		public string VoucherType { get; set; }

		[JsonProperty(PropertyName = "partnerImageUrl")]
		public string PartnerImageUrl { get; set; }

		[JsonProperty(PropertyName = "ticketDetailsHeaderText")]
		public string TicketDetailsHeaderText { get; set; }

		[JsonProperty(PropertyName = "orderedByText")]
		public string OrderedByText { get; set; }

		[JsonProperty(PropertyName = "pricePerTicketText")]
		public string PricePerTicketText { get; set; }

		[JsonProperty(PropertyName = "addressText")]
		public string AddressText { get; set; }

		[JsonProperty(PropertyName = "validityText")]
		public string ValidityText { get; set; }

		[JsonProperty(PropertyName = "shoppingBagsImageUrl")]
		public string ShoppingBagsImageUrl { get; set; }

		[JsonProperty(PropertyName = "logoImageUrl")]
		public string LogoImageUrl { get; set; }

		[JsonProperty(PropertyName = "shortDescription")]
		public string ShortDescription { get; set; }

		[JsonProperty(PropertyName = "summary")]
		public string Summary { get; set; }

		[JsonProperty(PropertyName = "category")]
		public string Category { get; set; }

		[JsonProperty(PropertyName = "address")]
		public string Address { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }


		// custom
		public GiftsOffer GiftsOffer { get; set; }
		public Gift[] Gifts { get; set; }
		public string OfferJson { get; set; }
		public string OfferGiftJson { get; set; }
		public string GiftJson { get; set; }
		public string PersonalizedPromotionJson { get; set; }

		public string ProductIds { get; set; }
		public string GiftId { get; set; }
		public string GiftValidTo { get; set; }
		public string GiftActivated { get; set; }
		public string GiftOfferused { get; set; }

		public string ChoiceCounter { get; set; }


	}
}