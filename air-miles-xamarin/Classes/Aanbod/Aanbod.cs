﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class AanbodCategorien
	{
		[JsonProperty(PropertyName = "categories")]
		public Aanbod[] Categorien { get; set; }
	}

	public class Aanbod
	{
		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }

		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "icon")]
		public string Icon { get; set; }

		[JsonProperty(PropertyName = "svgIcon")]
		public string SvgIcon { get; set; }

		[JsonProperty(PropertyName = "sizePromotions")]
		public string SizePromotions { get; set; }

		[JsonProperty(PropertyName = "image")]
		public string Image { get; set; }

		[JsonProperty(PropertyName = "link")]
		public string Url { get; set; }
	}
}