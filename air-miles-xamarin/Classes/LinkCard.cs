﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class LinkCardsRoot 	{ 		[JsonProperty(PropertyName = "Respons_KaartKoppelenInfo")] 		public LinkCardsInfo[] ResponseInfo { get; set; } 
		[JsonProperty(PropertyName = "Respons_KaartKoppelenActie")]
		public LinkCardsAction ResponseAction { get; set; }
 		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; }
 	}  	public class LinkCardsInfo 	{ 		[JsonProperty(PropertyName = "MainOwner")]
		public bool MainOwner { get; set; }

		[JsonProperty(PropertyName = "Picture")]
		public string Picture { get; set; }

		[JsonProperty(PropertyName = "FirstName")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "FullName")]
		public string FullName { get; set; }

		[JsonProperty(PropertyName = "Color")]
		public string Color { get; set; }

		[JsonProperty(PropertyName = "LastName")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "Prefix")]
		public string Prefix { get; set; }

		[JsonProperty(PropertyName = "Initials")]
		public string Initials { get; set; }

		[JsonProperty(PropertyName = "EmailAddress")]
		public string EmailAddress { get; set; }
 	}

	public class LinkCardsAction
	{
		[JsonProperty(PropertyName = "MainOwner")]
		public bool MainOwner { get; set; }

		[JsonProperty(PropertyName = "LinkCardsActionResponse_ErrorMessage")]
		public LinkCardsActionResponse_ErrorMessage LinkCardsActionResponse_ErrorMessage { get; set; }


		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }
	}

	public class LinkCardsActionResponse_ErrorMessage
	{
		[JsonProperty(PropertyName = "Place")]
		public string Place { get; set; }

		[JsonProperty(PropertyName = "Code")]
		public string Code { get; set; }

		[JsonProperty(PropertyName = "Message")]
		public string Message{ get; set; }
	} }
