﻿using System;
namespace airmilesxamarin
{
	public class LinkCardTableItems
	{
		public static TableSection[] Overview()
		{
			TableSection[] OverviewTableItems = new[] {
				new TableSection {
					Title = "",
					Items = new [] {
						new TableRow {
							Title = "Met andere spaarder(s)",
							Icon = "circle_koppelen",
							Segue = "SamenSparenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorBlueTableViewCell
						},
						new TableRow {
							Title = "Mastercard",
							Icon = "circle_mastercard",
							Segue = "MastercardSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorBlueTableViewCell
						},
						new TableRow {
							Title = "Albert Heijn",
							Icon = "circle_ah",
							Segue = "AHSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorBlueTableViewCell
						},
						new TableRow {
							Title = "Etos",
							Icon = "circle_etos.png",
							Segue = "EtosSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorBlueTableViewCell
						},
						new TableRow {
							Title = "Praxis",
							Icon = "circle_praxix",
							Segue = "PraxisSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorBlueTableViewCell
						},
					}
				},

			};

			return OverviewTableItems;
		}
	}
}

