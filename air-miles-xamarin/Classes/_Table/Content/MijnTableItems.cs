﻿using System;
namespace airmilesxamarin
{
	
	public class MijnTableItems
	{
		public static TableSection[] LoggedIn()
		{
			TableSection[] MijnTableItems = new[] {
			new TableSection {
				Title = "",
				Items = new [] {
					new TableRow {
						Title = "Transacties",
						Icon = "IconTransacties",
						Segue = "MijnTransactiesSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Aankopen",
						Icon = "IconBill",
						Segue = "MijnAankopenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Speciaal voor jou!",
						Icon = "IconGift",
						Segue = "PersoonlijkAanbodSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Geldigheid Air Miles",
						Icon = "IconCalendar",
						Segue = "GeldigheidSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Gegevens",
						Icon = "IconUser",
						Segue = "GegevensSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Inloggegevens",
						Icon = "IconSettings",
						Segue = "InstellingenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Overboeken",
						Icon = "IconTransferNew",
						Segue = "OverboekenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Doneren",
						Icon = "IconHart",
						Segue = "DonerenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
                        Title = "(Ont)koppelen",
						Icon = "IconSamenSparen",
						Segue = "SamenSparenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Blokkeren",
						Icon = "IconKaartBlokkeren",
						Segue = "KaartBlokkerenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Vervangen",
						Icon = "replacecardicon",
						Segue = "KaartVervangenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Uitschrijven",
						Icon = "IconUitschrijven",
						Segue = "UitschrijvenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
                    new TableRow {
                        Title = "Uitloggen",
                        Icon = "IconLogout",
                        Segue = "UitloggenSegue",
                        CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
                    },
				}
			},
		};

			return MijnTableItems;
		}

		public static TableSection[] DigitalCard()
		{
			TableSection[] MijnTableItems = new[] {
			new TableSection {
				Title = "",
				Items = new [] {
					new TableRow {
						Title = "Transacties",
						Icon = "IconTransacties",
						Segue = "MijnTransactiesSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Aankopen",
						Icon = "IconBill",
						Segue = "MijnAankopenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Speciaal voor jou!",
						Icon = "IconGift",
						Segue = "PersoonlijkAanbodSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Geldigheid Air Miles",
						Icon = "IconCalendar",
						Segue = "GeldigheidSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Gegevens",
						Icon = "IconUser",
						Segue = "GegevensSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Inloggegevens",
						Icon = "IconSettings",
						Segue = "InstellingenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
                    /*new TableRow {
                        Title = "Digitale kaart aanmaken",
                        Icon = "IconCard",
                        Segue = "DigitalAccountSegue",
                        CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
                    },*/
                    new TableRow {
						Title = "Overboeken",
						Icon = "IconTransferNew",
						Segue = "OverboekenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Doneren",
						Icon = "IconHart",
						Segue = "DonerenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
                        Title = "(Ont)koppelen",
						Icon = "IconSamenSparen",
						Segue = "SamenSparenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Blokkeren",
						Icon = "IconKaartBlokkeren",
						Segue = "KaartBlokkerenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					//new TableRow {
					//	Title = "Vervangen",
					//	Icon = "replacecardicon",
					//	Segue = "KaartVervangenSegue",
					//	CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					//},
					new TableRow {
						Title = "Uitschrijven",
						Icon = "IconUitschrijven",
						Segue = "UitschrijvenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
                    new TableRow {
                        Title = "Uitloggen",
                        Icon = "IconLogout",
                        Segue = "UitloggenSegue",
                        CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
                    },
				}
			},
		};

			return MijnTableItems;
		}

		public static TableSection[] NoPersonalOffers()
		{
			TableSection[] MijnTableItems = new[] {
			new TableSection {
				Title = "Account",
				Items = new [] {
					new TableRow {
						Title = "Gegevens",
						Icon = "IconUser",
						Segue = "GegevensSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Inloggegevens",
						Icon = "IconSettings",
						Segue = "InstellingenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Uitloggen",
						Icon = "IconLogout",
						Segue = "UitloggenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
				}
			},
			new TableSection {
				Title = "Mijn",
				Items = new [] {
					/*new TableRow {
						Title = "Geldigheid Air Miles",
						Icon = "IconCalendar",
						Segue = "GeldigheidSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},*/
					new TableRow {
						Title = "Transacties",
						Icon = "IconTransacties",
						Segue = "MijnTransactiesSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Aankopen",
						Icon = "IconBill",
						Segue = "MijnAankopenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Speciaal voor jou!",
						Icon = "IconGift",
						Segue = "PersoonlijkAanbodSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCellDisabled
					},
				}
			},
			new TableSection {
				Title = "Zelf regelen",
				Items = new [] {
					/*new TableRow {
						Title = "Digitale kaart aanmaken",
						Icon = "IconCard",
						Segue = "DigitalekaartAanmakenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},*/
					new TableRow {
						Title = "Koppelen",
						Icon = "IconSamenSparen",
						Segue = "SamenSparenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Overboeken",
						Icon = "IconTransferNew",
						Segue = "OverboekenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Doneren",
						Icon = "IconHart",
						Segue = "DonerenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Blokkeren",
						Icon = "IconKaartBlokkeren",
						Segue = "KaartBlokkerenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Vervangen",
						Icon = "replacecardicon",
						Segue = "KaartVervangenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
					new TableRow {
						Title = "Uitschrijven",
						Icon = "IconUitschrijven",
						Segue = "UitschrijvenSegue",
						CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
					},
				}
			},
		};

			return MijnTableItems;
		}


		public static TableSection[] LoggedOut()
		{
			TableSection[] MijnTableItems = new[] {
			new TableSection
			{
				Title = "Account",
				Items = new[] {
						/*new TableRow {
							Title = "Mijn gegevens",
							Icon = "IconUser",
							Segue = "GegevensSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Mijn aankopen",
							Icon = "IconBill",
							Segue = "AankopenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},*/
						new TableRow {
							Title = "Inloggegevens",
							Icon = "IconSettingsDisabled",
							Segue = "InstellingenSegueDisabled",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCellDisabled
						},
						new TableRow {
							Title = "Uitloggen",
							Icon = "IconLogoutDisabled",
							Segue = "UitloggenSegueDisabled",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCellDisabled
						},
					}
			},

			/*new TableSection {
				Title = "Notificaties",
				Items = new [] {
					new TableRow {
						Title = "Email nieuwsbrief",
						Icon = "IconMail",
						CellIdentifier = CellIdentifier.IconSwitchTableViewCell
					},
					new TableRow {
						Title = "App notificaties",
						Icon = "IconNotifications",
						CellIdentifier = CellIdentifier.IconSwitchTableViewCell
					},
				}
			},*/
		};

			return MijnTableItems;
		}

	}
}

