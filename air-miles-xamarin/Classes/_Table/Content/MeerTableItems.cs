﻿using System;
namespace airmilesxamarin
{
	public class MeerTableItems
	{
		public static TableSection[] LoggedIn()
		{
			TableSection[] MeerTableItems = new[] {
				new TableSection {
					Title = "Account",
					Items = new [] {
						new TableRow {
							Title = "Gegevens",
							Icon = "IconUser",
							Segue = "GegevensSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						/*new TableRow {
							Title = "Geldigheid Air Miles",
							Icon = "IconCalendar",
							Segue = "GeldigheidSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},*/
						new TableRow {
							Title = "Mijn aankopen",
							Icon = "IconBill",
							Segue = "MijnAankopenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Inloggegevens",
							Icon = "IconSettings",
							Segue = "InstellingenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Uitloggen",
							Icon = "IconLogout",
							Segue = "UitloggenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
					}
				},
				new TableSection {
					Title = "Partners",
					Items = new [] {
						new TableRow {
							Title = "Sparen",
							Icon = "IconSave",
							Segue = "PartnersSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Inwisselen",
							Icon = "IconCard",
							Segue = "PartnersSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						}
					}
				},
				new TableSection {
					Title = "Klantenservice",
					Items = new [] {
						new TableRow {
							Title = "Veelgestelde vragen",
							Icon = "IconHelp",
							Segue = "VeelgesteldeVragenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Telefoon",
							Detail = "085 – 0022209",
							Subdetail = /*"(€1 per gesprek)"*/"",
							Icon = "IconPhone",
							CellIdentifier = CellIdentifier.IconSubdetailRightTableViewCell,
							Segue = "TelefoonSegue",
						},
						new TableRow {
							Title = "Chat",
							Icon = "IconChat",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "ChatSegue",
						},
						new TableRow {
							Title = "Contactformulier",
							Icon = "IconPen",
							Segue = "ContactFormulierSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
					}
				},
				/*new TableSection {
					Title = "Instellingen",
					Items = new [] {
						new TableRow {
							Title = "Notificaties",
							Icon = "IconNotifications",
							CellIdentifier = CellIdentifier.IconSwitchTableViewCell
						}
					}
				},*/
				new TableSection {
					Title = "Zelf regelen",
					Items = new [] {
						new TableRow {
							Title = "Samen sparen",
							Icon = "IconSamenSparen",
							Segue = "SamenSparenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Overboeken",
							Icon = "IconOverboeken",
							Segue = "OverboekenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Doneren",
							Icon = "IconHart",
							Segue = "DonerenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Kaart blokkeren",
							Icon = "IconKaartBlokkeren",
							Segue = "KaartBlokkerenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Uitschrijven",
							Icon = "IconUitschrijven",
							Segue = "UitschrijvenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
					}
				},
				new TableSection {
					Title = "Volg ons op",
					Items = new [] {
						new TableRow {
							Title = "Facebook",
							Icon = "IconFacebook",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "FacebookSegue"
						},
						new TableRow {
							Title = "Twitter",
							Icon = "IconTwitter",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "TwitterSegue"
						},
					}
				},
				new TableSection {
					Title = "Feedback",
					Items = new [] {
						new TableRow {
							Title = "Feedback over de app",
							Icon = "IconIdea",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "FeedbackSegue"
						},
					}
				}
			};

			return MeerTableItems;
		}

		public static TableSection[] LoggedOut()
		{
			TableSection[] MeerTableItems = new[] {
				new TableSection {
					Title = "Partners",
					Items = new [] {
						new TableRow {
							Title = "Sparen",
							Icon = "IconSave",
							Segue = "PartnersSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Inwisselen",
							Icon = "IconCard",
							Segue = "PartnersSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						}
					}
				},
				new TableSection {
					Title = "Klantenservice",
					Items = new [] {
						
						new TableRow {
							Title = "Veelgestelde vragen",
							Icon = "IconHelp",
							Segue = "VeelgesteldeVragenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Telefoon",
							Detail = "085 – 0022209",
							Subdetail = /*"(€1 per gesprek)"*/"",
							Icon = "IconPhone",
							CellIdentifier = CellIdentifier.IconSubdetailRightTableViewCell,
							Segue = "TelefoonSegue",
						},
						new TableRow {
							Title = "Chat",
							Icon = "IconChat",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "ChatSegue",
						},
						new TableRow {
							Title = "Contactformulier",
							Icon = "IconPen",
							Segue = "ContactFormulierSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
					}
				},
                /*new TableSection {
                    Title = "Instellingen",
                    Items = new [] {
                        new TableRow {
                            Title = "Notificaties",
                            Icon = "IconNotifications",
                            CellIdentifier = CellIdentifier.IconSwitchTableViewCell
                        }
                    }
                },*/
				new TableSection {
					Title = "Volg ons op",
					Items = new [] {
						new TableRow {
							Title = "Facebook",
							Icon = "IconFacebook",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "FacebookSegue"
						},
						new TableRow {
							Title = "Twitter",
							Icon = "IconTwitter",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "TwitterSegue"
						},
					}
				},
				new TableSection {
					Title = "Meedoen",
					Items = new [] {
						new TableRow {
							Title = "Kaartnummer aanmaken",
							Icon = "IconNewcard",
							Segue = "",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCellDisabled
						},
					}
				},
				new TableSection {
					Title = "Feedback",
					Items = new [] {
						new TableRow {
							Title = "Feedback over de app",
							Icon = "IconIdea",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "FeedbackSegue"
						},
					}
				}
			};

			return MeerTableItems;
		}


		public static TableSection[] NoCard()
		{
			TableSection[] MeerTableItems = new[] {
				new TableSection {
					Title = "Partners",
					Items = new [] {
						new TableRow {
							Title = "Sparen",
							Icon = "IconSave",
							Segue = "PartnersSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Inwisselen",
							Icon = "IconCard",
							Segue = "PartnersSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						}
					}
				},
				new TableSection {
					Title = "Klantenservice",
					Items = new [] {
						
						new TableRow {
							Title = "Veelgestelde vragen",
							Icon = "IconHelp",
							Segue = "VeelgesteldeVragenSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
						new TableRow {
							Title = "Telefoon",
							Detail = "085 – 0022209",
							Subdetail = /*"(€1 per gesprek)"*/"",
							Icon = "IconPhone",
							CellIdentifier = CellIdentifier.IconSubdetailRightTableViewCell,
							Segue = "TelefoonSegue",
						},
						new TableRow {
							Title = "Chat",
							Icon = "IconChat",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "ChatSegue",
						},
						new TableRow {
							Title = "Contactformulier",
							Icon = "IconPen",
							Segue = "ContactFormulierSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
					}
				},
                /*new TableSection {
                    Title = "Instellingen",
                    Items = new [] {
                        new TableRow {
                            Title = "Notificaties",
                            Icon = "IconNotifications",
                            CellIdentifier = CellIdentifier.IconSwitchTableViewCell
                        }
                    }
                },*/
				new TableSection {
					Title = "Volg ons op",
					Items = new [] {
						new TableRow {
							Title = "Facebook",
							Icon = "IconFacebook",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "FacebookSegue"
						},
						new TableRow {
							Title = "Twitter",
							Icon = "IconTwitter",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "TwitterSegue"
						},
					}
				},
				new TableSection {
					Title = "Meedoen",
					Items = new [] {
						new TableRow {
							Title = "Kaartnummer aanmaken",
							Icon = "IconNewcard",
							Segue = "NewCardSegue",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell
						},
					}
				},
				new TableSection {
					Title = "Feedback",
					Items = new [] {
						new TableRow {
							Title = "Feedback over de app",
							Icon = "IconIdea",
							CellIdentifier = CellIdentifier.IconDisclosureIndicatorTableViewCell,
							Segue = "FeedbackSegue"
						},
					}
				}
			};

			return MeerTableItems;
		}
	}
}

