﻿using System;

namespace airmilesxamarin
{
	public class TableSection {
		public string Title { get; set; }
		public string Detail { get; set; }
		public TableRow[] Items { get; set; }
		public string FooterId { get; set; }
	}
}

