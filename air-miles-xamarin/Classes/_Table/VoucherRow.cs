﻿using System;

namespace airmilesxamarin
{
	public class VoucherTableRow
	{
		public int SectionIndex { get; set; }
		public int Index { get; set; }
		public string Barcode { get; set; }
		public string BarcodeType { get; set; }
		public string Description { get; set; }
		public string TotalString { get; set; }
		public string ProductType { get; set; }
		public string Validity { get; set; }
		public string ValidityDate { get; set; }
		public string Button { get; set; }
		public string Segue { get; set; }
		public string Url { get; set; }
		public string PartnerName { get; set; }
		public string VoucherNumber { get; set; }                             
		public CellIdentifier CellIdentifier { get; set; }
	}

}

