﻿using System;
namespace airmilesxamarin
{
	public enum BarcodeFormat
	{
		EAN13,
		EAN8,
		CODE128,
		ALL1D,
		AZTEC,
		CODABAR,
		CODE39,
		CODE93,
		DATAMATRIX,
		IMB,
		ITF,
		MAXICODE,
		MSI,
		PDF417,
		PLESSEY,
		QRCODE,
		RSS14,
		RSSEXPANDED,
		UPCA,
		UPCE,
		UPCEANEXTENSION
	}
}
