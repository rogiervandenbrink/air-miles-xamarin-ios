﻿using System;
namespace airmilesxamarin
{
	public class PartnerDetailRow
	{
		public int Index { get; set; }
		public string Text { get; set; }
		public string Webview { get; set; }
		public string CellIdentifier { get; set; }
	}
}

