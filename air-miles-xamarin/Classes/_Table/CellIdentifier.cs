﻿using System;

namespace airmilesxamarin
{
	public enum CellIdentifier {
		DetailButtonTableViewCell,
		DetailAmountTableViewCell,
		DisclosureIndicatorTableViewCell,
		IconDetailCollectionViewCell,
		IconDetailRightTableViewCell,
		IconDetailRightTableViewCellDisabled,
		IconSubdetailRightTableViewCell,
		IconDisclosureIndicatorTableViewCell,
		IconDisclosureIndicatorBlueTableViewCell,
		IconDisclosureIndicatorTableViewCellDisabled,
		IconSwitchTableViewCell,
		IconTableViewCell,
		ImageTableViewCell,
		PromotionCollectionViewCell,
		IconTextRightTableViewCell,
		GreyIconDetailRightTableViewCell,
		DatePickerTableViewCell,
		GenderPickersTableViewCell,
		CountryPickerTableViewCell,
		BarcodeDetailButtonCell,
		VoucherCollectionViewCell,
		StaticIconSwitchTableViewCell,
		CheckboxLabelButtonTableViewCell,
		CheckboxTitleButtonTableViewCell,
		CheckboxTitleTableViewCell,
		SaldoTopTableViewCell,
		TitleAmountTableViewCell
	}
}

