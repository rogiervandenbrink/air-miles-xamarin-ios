﻿using System;

namespace airmilesxamarin
{
	public class TableRow
	{
		public int SectionIndex { get; set; }
		public int Index { get; set; }
		public string Id { get; set; }
		public string Title { get; set; }
		public string Detail { get; set; }
		public string Description { get; set; }
		public string Subdetail { get; set; }
		public string Hiddeninfo { get; set; }
		public string Icon { get; set; }
		public string Button { get; set; }
		public string Segue { get; set; }
		public string Url { get; set; }
		public string InputText { get; set; }
		public string ButtonAnnuleer { get; set; }
		public string ButtonWijzig { get; set; }  
		public bool Switch { get; set; }
		public BemCheckBox Checkbox { get; set;}
		public CellIdentifier CellIdentifier { get; set; }
	}

}

