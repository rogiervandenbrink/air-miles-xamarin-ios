﻿using System;
namespace airmilesxamarin
{
	public class TableSectionGegevens
	{
		public string Title { get; set; }
		public string ErrorOne { get; set; }
		public string ErrorTwo { get; set; }
		public string ErrorThree { get; set; }
		public TableRow[] Items { get; set; }
		public string FooterId { get; set; }
	}
}
