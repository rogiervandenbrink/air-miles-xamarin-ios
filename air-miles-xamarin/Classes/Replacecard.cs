﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class ReplacecardRoot 	{ 		[JsonProperty(PropertyName = "Respons_KaartVervangen")] 		public Replacecard Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Replacecard 	{ 		[JsonProperty(PropertyName = "NewCardNumber")] 		public string NewCardNumber;

		[JsonProperty(PropertyName = "Message")]
		public string Message; 	} }
