﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class PartnersRoot 
	{
		[JsonProperty(PropertyName = "partners")]
		public Partner[] Partners { get; set; }		
	}

	public class Partner
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "title")]
		public string Title { get; set; }

		[JsonProperty(PropertyName = "summary")]
		public string Summary { get; set; }

		[JsonProperty(PropertyName = "image")]
		public string Image { get; set; }

		[JsonProperty(PropertyName = "partnerLogoImage")]
		public string Logo { get; set; }

		[JsonProperty(PropertyName = "collectAirmiles")]
		public bool Sparen { get; set; }

		[JsonProperty(PropertyName = "thisPartnerCantExchangeAirmiles")]
		public bool Inwisselen { get; set; }
	}

	public class PartnerDetail
	{
		[JsonProperty(PropertyName = "partnerPromotions")]
		public PartnerPromotie Promotions { get; set; }

		[JsonProperty(PropertyName = "tabsWebview")]
		public string Webview { get; set; }
	}

	public class PartnerPromotie
	{
		[JsonProperty(PropertyName = "Promotions")]
		public Promotie[] Promotions { get; set; }
	}

    public enum PartnerEnum{
        Efteling,
        Duinrell,
        Tikibad,
        Wildlands,
        Dolfinarium
    }
}

