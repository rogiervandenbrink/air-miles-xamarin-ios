﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class SecureResponse
	{
		[JsonProperty(PropertyName = "OutputString")]
		public string OutputString { get; set; }
	}
}

