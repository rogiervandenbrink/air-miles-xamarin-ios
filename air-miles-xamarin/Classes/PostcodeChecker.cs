﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class PostcodeCheckerDataRoot
	{
		[JsonProperty(PropertyName = "Respons_PostCode")]
		public PostcodeCheckerData Response { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Errors { get; set; }

	}

	public class PostcodeCheckerData
	{
		[JsonProperty (PropertyName = "HouseNumber")]
		public string HouseNumber { get; set; }

		[JsonProperty(PropertyName = "Country")]
		public string Country { get; set; }

		[JsonProperty(PropertyName = "Street")]
		public string Street { get; set; }

		[JsonProperty(PropertyName = "City")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "Postcode")]
		public string Postcode { get; set; }
	}

	public class Address
	{
		public static string Street { get; set; }
		public static string HouseNumber { get; set; }
		public static string Postcode { get; set; }
		public static string City { get; set; }
		public static string Country { get; set; }
	}
}
