﻿using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class UserRoot 	{ 		[JsonProperty(PropertyName = "Respons_Inloggen")] 		public User[] User { get; set; }  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class User 	{
		
		[JsonProperty(PropertyName = "Token")]
		public string Token { get; set; }

        [JsonProperty(PropertyName = "ZMemberID")]
		public string ZMemberID { get; set; }

		[JsonProperty(PropertyName = "MemberID")]
		public string MemberId { get; set; }

		[JsonProperty(PropertyName = "MembershipStatus")]
		public string MembershipStatus { get; set; }

		[JsonProperty(PropertyName = "EmailBounceMessage")]
		public string EmailBounceReason { get; set; }

		[JsonProperty(PropertyName = "HouseNumber")]
		public string HouseNumber { get; set; }

		[JsonProperty(PropertyName = "TelephoneNrPrivate")]
		public string TelephoneNrPrivate { get; set; }

		[JsonProperty(PropertyName = "TelephoneNrMobile")]
		public string TelephoneNrMobile { get; set; }

		[JsonProperty(PropertyName = "ShowPopUp")]
		public bool ShowPopUp { get; set; }

		[JsonProperty(PropertyName = "PostalCode")]
		public string PostalCode { get; set; }

		[JsonProperty(PropertyName = "MobileNrBouncePresent")]
		public bool MobileNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "RefreshToken")]
		public string RefreshToken { get; set; }

		[JsonProperty(PropertyName = "ExternalCardNumber")]
		public string ExternalCardNumber { get; set; }

		[JsonProperty(PropertyName = "OptIn")]
		public bool OptIn { get; set; }

        [JsonProperty(PropertyName = "Profiling")]
        public bool Profiling { get; set; }

		[JsonProperty(PropertyName = "PopUpMessage")]
		public string PopUpMessage { get; set; }

		[JsonProperty(PropertyName = "MobileNrBounceMessage")]
		public string MobilePhoneBounceReason { get; set; }

		[JsonProperty(PropertyName = "EmailBouncePresent")]
		public bool EmailBouncePresent { get; set; }

		[JsonProperty(PropertyName = "UserNameIR025")]
		public string UserNameIR025 { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBouncePresent")]
		public bool PrivateNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "DigitalCard")]
		public bool DigitalCard { get; set; }

		[JsonProperty(PropertyName = "Error")]
		public string Error { get; set; }

		[JsonProperty(PropertyName = "AddressBouncePresent")]
		public bool AddressBouncePresent { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBounceMessage")]
		public string PrivatePhoneBounceReason { get; set; }

		[JsonProperty(PropertyName = "City")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "Street")]
		public string Street { get; set; }

		[JsonProperty(PropertyName = "Country")]
		public string Country { get; set; }

		[JsonProperty(PropertyName = "HouseNumberSupplement")]
		public string HouseNumberSupplement { get; set; }

		[JsonProperty(PropertyName = "AddressBounceMessage")]
		public string AddressBounceReason { get; set; }

		[JsonProperty(PropertyName = "Initials")]
		public string Initials { get; set; }

		[JsonProperty(PropertyName = "FirstName")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "FullName")]
		public string FullName { get; set; }

		[JsonProperty(PropertyName = "Prefix")]
		public string Prefix { get; set; }

		[JsonProperty(PropertyName = "LastName")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "Salutation")]
		public string Salutation { get; set; }

		[JsonProperty(PropertyName = "Email")]
		public string Email { get; set; }

		[JsonProperty(PropertyName = "BirthDate")]
		public string BirthDate { get; set; }

		[JsonProperty(PropertyName = "CCL4")]
		public string CCL4 { get; set; }

		[JsonProperty(PropertyName = "UserStatus")]
		public string UserStatus { get; set; }

		[JsonProperty(PropertyName = "CardNumber")]
		public string CardNumber { get; set; }

		[JsonProperty(PropertyName = "Balance")]
		public long Balance { get; set; }

		[JsonProperty(PropertyName = "errorCode")]
		public string ErrorCode { get; set; }

		[JsonProperty(PropertyName = "errorMessage")]
		public string ErrorMessage { get; set; }

		[JsonProperty(PropertyName = "MobileLoginInfo_LinkCardsMessage")]
		public MobileLoginInfo_LinkCardsMessage MobileLoginInfo_LinkCardsMessage { get; set; }
	}

	public class MobileLoginInfo_LinkCardsMessage
	{
		[JsonProperty(PropertyName = "Message")]
		public string Message { get; set; }
	}
}
