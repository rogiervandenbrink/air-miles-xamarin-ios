﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class KaartData
	{
		[JsonProperty(PropertyName = "Notifications")]
		public KaartNotifications[] Notifications { get; set; }

		[JsonProperty(PropertyName = "BalanceCheckerResponse_ErrorMessage")]
		public KaartError[] KaartError { get; set; }

		[JsonProperty(PropertyName = "Collectors")]
		public int Collectors { get; set; }

		[JsonProperty(PropertyName = "Balance")]
		public int Balance { get; set; }

		[JsonProperty(PropertyName = "IncorrectPersonalInformation")]
		public string IncorrectPersonalInformation { get; set; }

		[JsonProperty(PropertyName = "Error")]
		public bool Error { get; set; }

		[JsonProperty(PropertyName = "NeedRedirect")]
		public bool Redirect { get; set; }

		[JsonProperty(PropertyName = "CheckBirthday")]
		public bool CheckBirthday { get; set; }

        [JsonProperty(PropertyName = "CardBlockedMessage")]
        public String CardBlockedMessage { get; set; }

        [JsonProperty(PropertyName = "Expiration")]
        public Validity[] KaartValidity { get; set; }

	}

	public class KaartRoot
	{
		[JsonProperty(PropertyName = "Respons_BalansCheckerV2")]
		public KaartData Kaart { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }

	}

	public class KaartNotifications 
	{
		[JsonProperty(PropertyName = "NotificationType")]
		public string NotificationType { get; set; }

		[JsonProperty(PropertyName = "Code")]
		public string Code { get; set; }
	}

	public class KaartError
	{
		[JsonProperty(PropertyName = "Message")]
		public string Message { get; set; }

		[JsonProperty(PropertyName = "Code")]
		public string Code { get; set; }
	}

	public class KaartGegevens
	{

		[JsonProperty(PropertyName = "Collectors")]
		public int Collectors { get; set; }

		[JsonProperty(PropertyName = "Balance")]
		public int Balance { get; set; }

		[JsonProperty(PropertyName = "Error")]
		public bool Error { get; set; }

		[JsonProperty(PropertyName = "NeedRedirect")]
		public bool Redirect { get; set; }

		[JsonProperty(PropertyName = "CheckBirthday")]
		public bool CheckBirthday { get; set; }

	}
}