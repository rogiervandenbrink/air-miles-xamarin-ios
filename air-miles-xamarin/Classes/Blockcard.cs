﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class BlockcardRoot 	{ 		[JsonProperty(PropertyName = "Respons_KaartBlokkeren")] 		public Blockcard Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Blockcard 	{ 		[JsonProperty(PropertyName = "NewCardNumber")] 		public string NewCardNumber; 	} }
