﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class LightAccountRoot
	{
		[JsonProperty(PropertyName = "Respons_InschrijvenAccount")]
		public LightAccount[] LichtAccount { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }
	}

	public class LightAccount
	{
		[JsonProperty(PropertyName = "TokenApp")]
		public string TokenApp { get; set; }

		[JsonProperty(PropertyName = "Token")]
		public string Token { get; set; }

		[JsonProperty(PropertyName = "MemberID")]
		public string MemberId { get; set; }

		[JsonProperty(PropertyName = "ZMemberID")]
		public string ZMemberId { get; set; }

		[JsonProperty(PropertyName = "DeviceID")]
		public string DeviceId { get; set; }

		[JsonProperty(PropertyName = "MembershipStatus")]
		public string MembershipStatus { get; set; }

		[JsonProperty(PropertyName = "Initials")]
		public string Initials { get; set; }

		[JsonProperty(PropertyName = "FirstName")]
		public string FirstName { get; set; }

		[JsonProperty(PropertyName = "Prefix")]
		public string Prefix { get; set; }

		[JsonProperty(PropertyName = "LastName")]
		public string LastName { get; set; }

		[JsonProperty(PropertyName = "Email")]
		public string Email { get; set; }

		[JsonProperty(PropertyName = "Street")]
		public string Street { get; set; }

		[JsonProperty(PropertyName = "HouseNumber")]
		public string HouseNumber { get; set; }

		[JsonProperty(PropertyName = "HouseNumberSupplement")]
		public string HouseNumberSupplement { get; set; }

		[JsonProperty(PropertyName = "City")]
		public string City { get; set; }

		[JsonProperty(PropertyName = "PostalCode")]
		public string PostalCode { get; set; }

		[JsonProperty(PropertyName = "Country")]
		public string Country { get; set; }

		[JsonProperty(PropertyName = "TelephoneNrPrivate")]
		public string Phone { get; set; }

		[JsonProperty(PropertyName = "TelephoneNrMobile")]
		public string Mobile { get; set; }

		[JsonProperty(PropertyName = "BirthDate")]
		public long BirthDate { get; set; }

		[JsonProperty(PropertyName = "Salutation")]
		public string Gender { get; set; }

		[JsonProperty(PropertyName = "UserStatus")]
		public string UserStatus { get; set; }

		[JsonProperty(PropertyName = "CardNumber")]
		public string CardNumber { get; set; }

		[JsonProperty(PropertyName = "Balance")]
		public long Balance { get; set; }

		[JsonProperty(PropertyName = "Partner")]
		public string Partner { get; set; }

		[JsonProperty(PropertyName = "OptIN")]
		public bool OptIN { get; set; }

		[JsonProperty(PropertyName = "errorCode")]
		public string ErrorCode { get; set; }

		[JsonProperty(PropertyName = "errorMessage")]
		public string ErrorMessage { get; set; }

		[JsonProperty(PropertyName = "EmailBouncePresent")]
		public bool EmailBouncePresent { get; set; }

		[JsonProperty(PropertyName = "EmailBounceReason")]
		public string EmailBounceReason { get; set; }

		[JsonProperty(PropertyName = "MobileNrBouncePresent")]
		public bool MobileNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "MobilePhoneBounceReason")]
		public string MobilePhoneBounceReason { get; set; }

		[JsonProperty(PropertyName = "PrivateNrBouncePresent")]
		public bool PrivateNrBouncePresent { get; set; }

		[JsonProperty(PropertyName = "PrivatePhoneBounceReason")]
		public string PrivatePhoneBounceReason { get; set; }

		[JsonProperty(PropertyName = "AddressBouncePresent")]
		public bool AddressBouncePresent { get; set; }

		[JsonProperty(PropertyName = "AddressBounceReason")]
		public string AddressBounceReason { get; set; }

	}
}