﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class TransferRoot 	{ 		[JsonProperty(PropertyName = "Respons_Overboeken")] 		public Transfer Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Transfer 	{ 		[JsonProperty(PropertyName = "PopUpMessage")] 		public string PopupMessage; 	} }
