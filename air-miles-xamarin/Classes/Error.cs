﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class ResponseError
	{
		[JsonProperty(PropertyName = "Place")]
		public string Place;

		[JsonProperty(PropertyName = "Code")]
		public string Code;

		[JsonProperty(PropertyName = "Message")]
		public string Message;
	}
}

