﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class CharityRoot 	{ 		[JsonProperty(PropertyName = "Respons_NaamGoedeDoelen")] 		public CharityNames[] Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class CharityNames 	{ 		[JsonProperty(PropertyName = "CharityName")] 		public string CharityName; 	} }
