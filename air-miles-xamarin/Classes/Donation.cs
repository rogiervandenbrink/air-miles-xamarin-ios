﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class DonationRoot 	{ 		[JsonProperty(PropertyName = "Respons_Doneren")] 		public Donation Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }  		[JsonProperty(PropertyName = "Respons_Token")] 		public ResponseToken TokenApp { get; set; } 	}  	public class Donation 	{ 		[JsonProperty(PropertyName = "PopUpMessage")] 		public string PopupMessage; 	} }
