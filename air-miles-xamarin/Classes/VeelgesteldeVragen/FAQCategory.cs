﻿using System;

namespace airmilesxamarin
{
	public class FAQCategory
	{
		public string Title { get; set; }
		public string ShortTitle { get; set; }
		public FAQ[] Items { get; set; }
	}
}