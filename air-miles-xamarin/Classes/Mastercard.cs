﻿using System;
using Newtonsoft.Json;

namespace airmilesxamarin
{

	public class MastercardRoot 	{ 		[JsonProperty(PropertyName = "Respons_Mastercard")] 		public Mastercard Response;  		[JsonProperty(PropertyName = "Respons_Foutmelding")] 		public ResponseError[] Error { get; set; }

		[JsonProperty(PropertyName = "truncated_pan")]
		public string TruncatedPan { get; set; }  		[JsonProperty(PropertyName = "token")] 		public string Token { get; set; } 	}  	public class Mastercard 	{ 		[JsonProperty(PropertyName = "PopUpMessage")] 		public string PopupMessage; 	}

	public class StoreTokenRoot
	{
		[JsonProperty(PropertyName = "Respons_OpslaanToken")]
		public TruncatedPan Response { get; set; }

		[JsonProperty(PropertyName = "Respons_Foutmelding")]
		public ResponseError[] Error { get; set; }

		[JsonProperty(PropertyName = "truncated_pan")]
		public string TruncatedPan { get; set; }

		[JsonProperty(PropertyName = "token")]
		public string Token { get; set; }
	}

	public class TruncatedPan
	{
		[JsonProperty(PropertyName = "Truncated_PAN")]
		public string Truncated_PAN { get; set; }
	} }
