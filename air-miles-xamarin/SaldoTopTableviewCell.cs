﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class SaldoTopTableviewCell : UITableViewCell
    {
        public SaldoTopTableviewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell()
		{
			SetCollectorsLabel();
			SetBalanceLabel();
			SetDatetimeLabel();
		}

		private void SetCollectorsLabel()
		{
			if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.Collectors) > 0)
			{
				CollectorsTableLabel.Text = "Momenteel hebben jullie";
			}
			else {
				CollectorsTableLabel.Text = "Momenteel heb je";
			}
		}

		private void SetBalanceLabel()
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance)))
			{
				BalanceTableLabel.Text = AMMain.SetBalanceFormat(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance));
			}
			else
			{
				BalanceTableLabel.Text = "0";
			}
		}

		private void SetDatetimeLabel()
		{
			//Console.WriteLine("Hoi " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.BalanceDate).ToUpper());
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.BalanceDate)))
			{
				DatetimeTableLabel.Text = "BIJGEWERKT T/M " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.BalanceDate).ToUpper();
			}
			else
			{
				DatetimeTableLabel.Text = "BIJGEWERKT T/M: ONBEKEND";
			}
		}
    }
}