﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class PageViewController : UIPageViewController
    {
        public PageViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			var pageControl = UIPageControl.Appearance;
			pageControl.CurrentPageIndicatorTintColor = UIColor.FromRGB(222, 0, 150);
			base.ViewDidLoad();
		}
    }
}