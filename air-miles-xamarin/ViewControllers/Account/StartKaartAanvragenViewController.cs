﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class StartKaartAanvragenViewController : UIViewController
    {
        public StartKaartAanvragenViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			AirMilesImageView.Image = UIImage.FromBundle("AirMilesAvatar");
			BackgroundView.BackgroundColor = UIColor.FromRGB(1, 125, 195);
			KaartAanvragenButton.TouchUpInside += KaartAanvragenButton_TouchUpInside;
			SkipKaartAanvragenButton.TouchUpInside += SkipKaartButton_TouchUpInside;

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: kaart - vraag");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(KaartAanvragenButton);
			AMStyle.BorderedButton(SkipKaartAanvragenButton);
		}

		private void KaartAanvragenButton_TouchUpInside(object sender, EventArgs e)
		{
			PerformSegue("KaartAanvragenSegue", this);
		}

		private void SkipKaartButton_TouchUpInside(object sender, EventArgs e)
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
			DismissModalViewController(true);
		}
    }
}