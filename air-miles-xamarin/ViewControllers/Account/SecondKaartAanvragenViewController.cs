﻿using Foundation;
using System;
using UIKit;
using System.Text.RegularExpressions;
using CoreGraphics;
using System.Drawing;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
    public partial class SecondKaartAanvragenViewController : UIViewController
    {
		BemCheckBox checkboxPolicy;
		BemCheckBox checkboxNewsletter;

		LoadingOverlay LoadingView;

		UIViewController Controller;


		public SecondKaartAanvragenViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			SecondKaartAanvragenButton.TouchUpInside += SecondKaartAanvragenButton_TouchUpInside;
			SkipSecondKaartAanvragenButton.TouchUpInside += SkipSecondKaartAanvragenButton_TouchUpInside;
			VoorwaardenButton.TouchUpInside += delegate {
				AlgemeneVoorwaardenURL();
			};


            checkboxNewsletter = new BemCheckBox(new CGRect(30, (UIScreen.MainScreen.Bounds.Height - KaartAanvragenWhiteView.Bounds.Height) + NewsletterLabel.Frame.Top + (NewsletterLabel.Frame.Height / 4), 20, 20), new BemCheckBoxDelegate());
			View.AddSubview(checkboxNewsletter);
            Console.WriteLine("Y coordinate NesletterLAbel "+ NewsletterLabel.Frame.Y);

			checkboxPolicy = new BemCheckBox(new CGRect(30, (UIScreen.MainScreen.Bounds.Height - KaartAanvragenWhiteView.Bounds.Height) + AkkoordLabel.Frame.Top + (AkkoordLabel.Frame.Height / 4), 20, 20), new BemCheckBoxDelegate());
			View.AddSubview(checkboxPolicy);
			Console.WriteLine("Y coordinate AkkoordLAbel " + AkkoordLabel.Frame.Y);

			KaartEmailTextField.KeyboardType = UIKeyboardType.EmailAddress;
			AMMain.AddDoneButton(KaartEmailTextField);
			KaartEmailTextField.ShouldReturn += (textField) =>
			{
				KaartEmailTextField.ResignFirstResponder();
				return true;
			};
		}


		private void Initialize()
		{
			AMStyle.RoundedButton(SecondKaartAanvragenButton);
			AMStyle.BorderedTextfield(KaartEmailTextField);
            NewsletterLabel.Font = UIFont.FromName("Avenir-Book", 11f);
            NewsletterLabel.AdjustsFontSizeToFitWidth = true;
		}



		void SecondKaartAanvragenButton_TouchUpInside (object sender, EventArgs e)
		{
			if (Regex.Match(KaartEmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success && checkboxPolicy.On)
			{
				CreateLightAccount(KaartEmailTextField.Text, checkboxNewsletter.On);

			}
			if (Regex.Match(KaartEmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success &&!checkboxPolicy.On)
			{
				AMAlerts.DefaultAlert(this, "Algemene voorwaarden", "Om verder te gaan, dien je akkoord te gaan met de algemene voorwaarden.");

			}
			if (!Regex.Match(KaartEmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
			{
				AMAlerts.DefaultAlert(this, "Ongeldig E-mailadres", "Je hebt geen geldig e-mailadres ingevuld");

			}
		}

		void SkipSecondKaartAanvragenButton_TouchUpInside(object sender, EventArgs e)
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
			PerformSegue("TochGeenKaartSegue", this);
		}

		private async void CreateLightAccount(string email, bool optin)
		{
			SetLoader(AMStrings.LoadingCreateCard);
			CreateLightAccountService LightAccountService = new CreateLightAccountService();
			LightAccountRoot CurrentLightAccount = await LightAccountService.CreateLightAccount(email, optin);

			if (CurrentLightAccount.Error != null && CurrentLightAccount.Error[0].Code == "E00")
			{
				LocalStorage.SaveLightAccount(CurrentLightAccount.LichtAccount[0].CardNumber,email);
                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);

				PerformSegue("KaartBevestigingSegue", this);
				Console.WriteLine("kaartnummer lightaccount " + CurrentLightAccount.LichtAccount[0].CardNumber.ToString());
				LoadingView.Hide();
			}
			if (CurrentLightAccount.Error[0].Code != "E00")
			{
				LoadingView.Hide();
				AMAlerts.DefaultAlert(this, "Fout bij aanmaken kaart", CurrentLightAccount.Error[0].Message);
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void AlgemeneVoorwaardenURL()
		{
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLAlgemeneVoorwaarden));
            //AMMain.OpenURLInApp(this, AMClient.URLAlgemeneVoorwaarden);
		}


	}
}