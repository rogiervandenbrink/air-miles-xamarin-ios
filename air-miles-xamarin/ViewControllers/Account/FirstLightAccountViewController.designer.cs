﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("FirstLightAccountViewController")]
    partial class FirstLightAccountViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView DigitalAccountView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton FirstLightAccountButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SkipLightAccountButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DigitalAccountView != null) {
                DigitalAccountView.Dispose ();
                DigitalAccountView = null;
            }

            if (FirstLightAccountButton != null) {
                FirstLightAccountButton.Dispose ();
                FirstLightAccountButton = null;
            }

            if (SkipLightAccountButton != null) {
                SkipLightAccountButton.Dispose ();
                SkipLightAccountButton = null;
            }
        }
    }
}