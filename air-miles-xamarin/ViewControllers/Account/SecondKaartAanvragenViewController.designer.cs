﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("SecondKaartAanvragenViewController")]
    partial class SecondKaartAanvragenViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AkkoordLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel KaartAanvragenLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView KaartAanvragenWhiteView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField KaartEmailTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NewsletterLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SecondKaartAanvragenButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SkipSecondKaartAanvragenButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton VoorwaardenButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AkkoordLabel != null) {
                AkkoordLabel.Dispose ();
                AkkoordLabel = null;
            }

            if (KaartAanvragenLabel != null) {
                KaartAanvragenLabel.Dispose ();
                KaartAanvragenLabel = null;
            }

            if (KaartAanvragenWhiteView != null) {
                KaartAanvragenWhiteView.Dispose ();
                KaartAanvragenWhiteView = null;
            }

            if (KaartEmailTextField != null) {
                KaartEmailTextField.Dispose ();
                KaartEmailTextField = null;
            }

            if (NewsletterLabel != null) {
                NewsletterLabel.Dispose ();
                NewsletterLabel = null;
            }

            if (SecondKaartAanvragenButton != null) {
                SecondKaartAanvragenButton.Dispose ();
                SecondKaartAanvragenButton = null;
            }

            if (SkipSecondKaartAanvragenButton != null) {
                SkipSecondKaartAanvragenButton.Dispose ();
                SkipSecondKaartAanvragenButton = null;
            }

            if (VoorwaardenButton != null) {
                VoorwaardenButton.Dispose ();
                VoorwaardenButton = null;
            }
        }
    }
}