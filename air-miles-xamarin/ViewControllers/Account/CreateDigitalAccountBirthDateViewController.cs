﻿using Foundation;
using System;
using UIKit;
using System.Threading;
using CoreGraphics;
using Google.Analytics;
using System.Text.RegularExpressions;
using Xamarin;
using Air_Miles_Xamarin;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class CreateDigitalAccountBirthDateViewController : UITableViewController
    {

		bool PasswordHidden = true;
		bool PasswordRepeatHidden = true;
		LoadingOverlay LoadingView;
		string errorList;
		public bool CheckCard { get; set; }
		public bool CheckBirthdate { get; set; }
		public bool CheckEmail { get; set; }
		public bool NoCardNumber { get; set; }
		string birthdate;
		long BirthDateChanged;
		long birthDate;
		bool birthDateHasChanged;

		long birthDayLong = 0;

		BemCheckBox checkboxPolicy;
		BemCheckBox checkboxNewsletter;

		UILabel VoorwaardenText;

		public bool NewsletterBool;
		public bool PolicyBool;



		// Network status
		NetworkStatus Internet;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		public string Routing { get; set; }

        public CreateDigitalAccountBirthDateViewController (IntPtr handle) : base (handle)
        {
        }
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();
			CreateDigitalAccountButton.TouchUpInside += delegate
			{
				if (checkboxPolicy.On)
				{
					HideKeyboard();
					SaveLightAccount();
				}else
				{
					AMAlerts.DefaultAlert(this, "Let op!", "Om verder te gaan, dien je akkoord te gaan met de algemene voorwaarden en het privacy- en cookiebeleid.");
				}
				//Console.WriteLine("button clicked");
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: account - gegevens", "Accounts aanmaken", "Maak account aan", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			};


			Console.WriteLine("Cardnumber in localstorage " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
			CardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
			CardnumberTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
			CardnumberTextfield.KeyboardType = UIKeyboardType.NumberPad;
            ConfirmEmailTextField.KeyboardType = UIKeyboardType.EmailAddress;
			//EmailTextField.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
			PasswordTextField.SecureTextEntry = true;
			PasswordRepeatTextField.SecureTextEntry = true;

			BirthdateTextfield.ShouldBeginEditing += OnTextFieldShouldBeginEditing;

			UIButton eyeButton = new UIButton(UIButtonType.Custom);
			eyeButton.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
			eyeButton.TintColor = UIColor.LightGray;
			eyeButton.Frame = new CGRect(0, 0, 25, 15);
			ViewEyeGrey.AddSubview(eyeButton);

			eyeButton.TouchUpInside += delegate
			{
				PasswordHidden = !PasswordHidden;
				PasswordTextField.SecureTextEntry = PasswordHidden;
				if (!PasswordHidden)
				{
					eyeButton.SetImage(UIImage.FromBundle("IconEyeBlue"), UIControlState.Normal);
				}
				else
				{
					eyeButton.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);

				}
			};


			NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
			UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
			{
				if (NavigationController != null)
				{
					NavigationController.PopViewController(false);
				}
				else
				{
					dismissModalStack();

				}

			}), true);

			SkipCreateAccountButtons.TouchUpInside += SkipCreateAccountButton_TouchUpInside;
			SkipCreateAccountButtons.SetTitleColor(UIColor.FromRGB(199, 199, 199), UIControlState.Normal);

			IconButton.SetImage(UIImage.FromBundle("IconInfo"), UIControlState.Normal);
			IconButton.TouchUpInside += delegate
			{
				AMAlerts.DefaultAlert(this, AMStrings.TitlePasswordTip, AMStrings.MessagePasswordTip);
			};

			IconUserNameButton.SetImage(UIImage.FromBundle("IconInfo"), UIControlState.Normal);
			IconUserNameButton.TouchUpInside += delegate
			{
				AMAlerts.DefaultAlert(this, AMStrings.TooltipUserNameTitle, AMStrings.TooltipTextUsername);
			};

			UIButton eyeButtonTwo = new UIButton(UIButtonType.Custom);
			eyeButtonTwo.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
			eyeButtonTwo.Frame = new CGRect(0, 0, 25, 15);
			ViewBlueEye.AddSubview(eyeButtonTwo);

			eyeButtonTwo.TouchUpInside += delegate
			{
				PasswordRepeatHidden = !PasswordRepeatHidden;
				PasswordRepeatTextField.SecureTextEntry = PasswordRepeatHidden;
				if (!PasswordRepeatHidden)
				{
					eyeButtonTwo.SetImage(UIImage.FromBundle("IconEyeBlue"), UIControlState.Normal);
				}
				else
				{
					eyeButtonTwo.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
				}
			};

			string cardNumber = "";


			CardnumberButton.TouchUpInside+= delegate {

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: account - gegevens", "Accounts aanmaken", "Bevestig kaartnummer", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();


				//if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
				//{
				if (CardnumberTextfield.Text.Length == 9)
				{
					cardNumber = CardnumberTextfield.Text;
					NSUserDefaults.StandardUserDefaults.SetString(CardnumberTextfield.Text, AMLocalStorage.CardNumber);

					NoCardNumber = false;
					//CheckCard = false;
					//CheckBirthdate = true;
					//TableView.ReloadData();
					CheckIfShouldAskForBirthdate(cardNumber);

				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorCardInvalidMessage);
				}
				//}
				//else
				//{
				//	cardNumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
				//	//CheckCard = false;
				//	//CheckBirthdate = true;
				//	//TableView.ReloadData();
				//	CheckIfShouldAskForBirthdate(cardNumber);
				//}

			};
			ConfirmEmailButton.TouchUpInside += delegate
			{

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: account - gegevens", "Accounts aanmaken", "Bevestig email", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				if (!string.IsNullOrEmpty(CardnumberTextfield.Text))
				{
					cardNumber = CardnumberTextfield.Text;
				}
				else
				{
					cardNumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
				}

				if (string.IsNullOrEmpty(ConfirmEmailTextField.Text) || !Regex.Match(ConfirmEmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, " Geen geldig e-mailadres ingevuld.");
				}
				else
				{
					if(!string.IsNullOrEmpty(cardNumber))
					{
						DoCheckEmail(cardNumber, ConfirmEmailTextField.Text);
					}
				}

			};
			BirthdateButton.TouchUpInside+= delegate {

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: account - gegevens", "Accounts aanmaken", "Bevestig geboortedatum", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				DateTime birthdateTime;
				bool parsed = DateTime.TryParseExact(BirthdateTextfield.Text, "dd-MM-yyyy", AMMain.Cultureinfo, System.Globalization.DateTimeStyles.None, out birthdateTime);
				if (parsed)
				{
					var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Amsterdam");
					var localTime = TimeZoneInfo.ConvertTimeFromUtc(birthdateTime, localTimeZone);
					birthdate = SharedDatetime.ConvertToUnixTimestampLong(localTime).ToString();
					if (!string.IsNullOrEmpty(cardNumber))
					{
						DoCheckBirthdate(cardNumber, birthdate);
					}
				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Je hebt nog geen geboortedatum ter verificatie ingevuld.");
				}
			};

			var titleVoorwaarden = new NSAttributedString(
				"- de algemene voorwaarden",
				font: UIFont.FromName("Avenir-Book", 14.0f),
				foregroundColor: UIColor.Blue
			//underlineStyle: NSUnderlineStyle.Single
			);
			var titlePrivacy = new NSAttributedString(
				"- het privacy- en cookiebeleid",
				font: UIFont.FromName("Avenir-Book", 14.0f),
				foregroundColor: UIColor.Blue
			//underlineStyle: NSUnderlineStyle.Single
			);

			if (UIScreen.MainScreen.Bounds.Width > 320)
			{
				VoorwaardenLabel.Font = UIFont.FromName("Avenir-Book", 12f);
				NewsletterLabel.Font = UIFont.FromName("Avenir-Book", 12f);
			}
			else
			{
				VoorwaardenLabel.Font = UIFont.FromName("Avenir-Book", 10f);
				NewsletterLabel.Font = UIFont.FromName("Avenir-Book", 10f);
			}

			ConditionsButton.SetAttributedTitle(titleVoorwaarden, UIControlState.Normal);
			PrivacyButton.SetAttributedTitle(titlePrivacy, UIControlState.Normal);

			ConditionsButton.TouchUpInside += delegate
			{
                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLAlgemeneVoorwaarden));
			};
			PrivacyButton.TouchUpInside += delegate
			{
                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLPrivacyCookieBeleid));
			};

			checkboxNewsletter = new BemCheckBox(new CGRect(0, 0, 20, 20), new BemCheckBoxDelegate());
			ViewCheckboxNewsletter.AddSubview(checkboxNewsletter);

			checkboxPolicy = new BemCheckBox(new CGRect(0, 0, 20, 20), new BemCheckBoxDelegate());
			ViewCheckboxPolicy.AddSubview(checkboxPolicy);

			NewCardnumberButton.Font = UIFont.FromName("Avenir-Black", 12f);
			NewCardnumberButton.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
			NewCardnumberButton.TitleLabel.TextAlignment = UITextAlignment.Center;
			NewCardnumberButton.TitleLabel.Lines = 0;
			NewCardnumberButton.SetTitle(AMStrings.TitleNewCardButton, UIControlState.Normal);
			NewCardnumberButton.SetTitleColor(UIColor.FromRGB(1, 125, 195), UIControlState.Normal);
			NewCardnumberButton.TouchUpInside += delegate
			{
				Console.WriteLine("kaart aanmaken segue");
				DigitalCardViewController FirstKaartModal = App.Storyboard.InstantiateViewController("DigitalCardViewController") as DigitalCardViewController;
				FirstKaartModal.Routing = "Meer";
				NavigationController.PushViewController(FirstKaartModal, false);
			};

			AddCardImageView.Image = UIImage.FromBundle("IconNewcard").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			AddCardImageView.TintColor = UIColor.FromRGB(1, 125, 195);
			AddCardImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();

		}

		async void CheckIfShouldAskForBirthdate(string cardnumber)
		{
			SetLoaderSmall(AMStrings.LoadingCreateAccount);

				try
				{

					AccountSignUpService SignUpService = new AccountSignUpService();
					LightAccountRoot CurrentLightAccount = await SignUpService.CheckIfBirthdateIsNeeded(cardnumber);

                    //check for birthdate needed
					if (CurrentLightAccount.Error[0].Code == "E128")
					{
						CheckCard = false;
						CheckBirthdate = true;
						CheckEmail = false;

						CardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
						CardnumberTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
						CardNumberLabel.Hidden = false;
						CardnumberTextfield.Hidden = true;
						TableView.ReloadData();

						// Analytics Screenname
						Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: account - geboortedatum");
						Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
					}
					//email needs to be checked
					else if (CurrentLightAccount.Error[0].Code == "E146" /*|| CurrentLightAccount.Error[0].Code == "E145"*/)
					{
						if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
						{
							CardNumberLabel.Hidden = true;
							CardnumberTextfield.Hidden = false;

						}
						else
						{
							CardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
							CardnumberTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
							CardNumberLabel.Hidden = false;
							CardnumberTextfield.Hidden = true;
						}
					CheckBirthdate = false;
					CheckCard = false;
					CheckEmail = true;
					TableView.ReloadData();
					}
					else if (CurrentLightAccount.Error[0].Code == "E47" /*|| CurrentLightAccount.Error[0].Code == "E145"*/)
					{
						if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
						{
							CardNumberLabel.Hidden = true;
							CardnumberTextfield.Hidden = false;

						}
						else
						{
							CardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
							CardnumberTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
							CardNumberLabel.Hidden = false;
							CardnumberTextfield.Hidden = true;
						}
						CheckBirthdate = false;
						CheckCard = false;
						CheckEmail = false;
						TableView.ReloadData();
					}
					else
					{
						AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentLightAccount.Error[0].Message);
						
					}

				}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Digitaal account aanmaken: geboortedatum check", "Exception: "+ ex}
                    });
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
					Console.WriteLine("exception is birthdate needed "+ex);
    				if (LoadingView != null)
    				{
    					LoadingView.Hide();
    				}
				}

            if (LoadingView != null)
            {
                LoadingView.Hide();
            }
		}

		async void DoCheckBirthdate(string cardnumber, string birthDate)
		{
			SetLoaderSmall(AMStrings.LoadingCreateAccount);
			//if (Internet != NetworkStatus.NotReachable)
			//{
				try
				{

					AccountSignUpService SignUpService = new AccountSignUpService();
					LightAccountRoot CurrentLightAccount = await SignUpService.CheckBirthdate(cardnumber, birthDate);

					if (CurrentLightAccount.Error[0].Code == "E143")
					{
						//BirthdateButton is good
						CheckBirthdate = false;
						CheckCard = false;
						CheckEmail = false;
						TableView.ReloadData();
					}
					else
					{
						AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentLightAccount.Error[0].Message);

					}

				}
				catch (Exception ex)
				{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Digitaal account aanmaken: geboortedatum check", "Exception: "+ ex}
                });
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
					Console.WriteLine("exception is docheckbirthdate " + ex);
				}

			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		async void DoCheckEmail(string cardnumber, string email)
		{
			SetLoaderSmall(AMStrings.LoadingCreateAccount);
			//if (Internet != NetworkStatus.NotReachable)
			//{
			try
			{

				AccountSignUpService SignUpService = new AccountSignUpService();
				LightAccountRoot CurrentLightAccount = await SignUpService.CheckEmail(cardnumber, email);

				if (CurrentLightAccount.Error[0].Code == "E143")
				{
					//BirthdateButton is good
					CheckBirthdate = false;
					CheckCard = false;
					CheckEmail = false;
					EmailTextField.Text = email;
					TableView.ReloadData();
				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentLightAccount.Error[0].Message);

				}

			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Digitaal account aanmaken: geboortedatum check", "Exception: "+ ex}
                });

				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				Console.WriteLine("exception is docheckemail " + ex);
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
			}

			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 40;

			if (indexPath.Row == 0)
			{

				height = 120.0f;
			}
			if (indexPath.Row == 15)
			{
				height = 0.0f;
			}
			if (indexPath.Row == 17)
			{

				height = 60.0f;
			}
			if (indexPath.Row > 18)
			{

				height = 0.0f;
			}
			if (indexPath.Row == 16)
			{

				height = 80.0f;
			}
			if (CheckCard)
			{
				if (indexPath.Row > 2 && indexPath.Row < 22)
				{
					height = 0.0f;
				}
				if (indexPath.Row == 22)
				{
					height = 60f;
				}
				if (indexPath.Row == 0)
				{
					height = 120f;
				}
			}
			if (CheckBirthdate)
			{
				if (indexPath.Row > 0 && indexPath.Row < 19)
				{
					height = 0.0f;
				}
				if (indexPath.Row == 22)
				{
					height = 0.0f;
				}
				if (indexPath.Row > 18 && indexPath.Row < 22)
				{
					height = 60.0f;
				}
				if (indexPath.Row == 0)
				{
					height = 120f;
				}
			}
			if (CheckEmail)
			{
				if (indexPath.Row > 0 && indexPath.Row < 25)
				{
					height = 0.0f;
				}
				if (indexPath.Row > 24)
				{
					height = 60.0f;
				}
				if (indexPath.Row == 0)
				{
					height = 120f;
				}
			}
			if (NoCardNumber)
			{
				if (indexPath.Row == 23)
				{
					height = 15.0f;
				}
				if (indexPath.Row == 24)
				{
					height = 60.0f;
				}
			}
			if (!NoCardNumber)
			{
				if (indexPath.Row == 23)
				{
					height = 0.0f;
				}
				if (indexPath.Row == 24)
				{
					height = 0.0f;
				}
			}

			return height;
		}

		public override void RowHighlighted(UITableView tableView, NSIndexPath rowIndexPath)
		{
			var Cell = tableView.CellAt(rowIndexPath);
			Cell.BackgroundColor = UIColor.White;
		}

		private void Initialize()
		{
			CheckBirthdate = false;
			CheckCard = true;
			Title = "Account aanmaken";

			AMStyle.RoundedButton(CreateDigitalAccountButton);
			AMStyle.RoundedButton(CardnumberButton);
			AMStyle.RoundedButton(BirthdateButton);
			AMStyle.BorderedGreyLabel(CardNumberLabel);
			AMStyle.BorderedTextfield(UsernameTextField);
			AMStyle.BorderedTextfield(EmailTextField);
			AMStyle.BorderedTextfield(CardnumberTextfield);
			AMStyle.BorderedTextfield(BirthdateTextfield);
			AMStyle.BorderedView(PasswordBackground);
			AMStyle.BorderedView(PasswordConfirmBackground);
			AMStyle.BorderedView(ConfirmEmailTextField);
			AMStyle.RoundedButton(ConfirmEmailButton);

			AMMain.AddDoneButton(UsernameTextField);
			AMMain.AddDoneButton(EmailTextField);
			AMMain.AddDoneButton(PasswordTextField);
			AMMain.AddDoneButton(PasswordRepeatTextField);

			ToptextaccountLabel.TextColor = UIColor.White;

			HideBackButton();

			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
			{
				NoCardNumber = true;
				CardNumberLabel.Hidden = true;
				CardnumberTextfield.Hidden = false;

			}else
			{
				NoCardNumber = false;
				CardNumberLabel.Hidden = true;
				CardnumberTextfield.Hidden = false;
			}


			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: account - gegevens");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		void SkipCreateAccountButton_TouchUpInside(object sender, EventArgs e)
		{
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: account - gegevens", "Accounts aanmaken", "Annuleren", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			if (Routing == "Meer")
			{
				Console.WriteLine("Routing is Meer");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				NavigationController.PopToRootViewController(false);
			}
			if (Routing == "Login")
			{
				Console.WriteLine("Routing is Meer");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				NavigationController.PopToRootViewController(false);
			}
			else
			{
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				PerformSegue("SkipAccountSegue", this);
			}
		}

		void SaveLightAccount()
		{
			errorList = "";
			string Email = "";
			if (!string.IsNullOrEmpty(EmailTextField.Text))
			{
				Email = EmailTextField.Text;
			}
		
			string Username = UsernameTextField.Text;
			string Password = PasswordTextField.Text;
			string PasswordConfirm = PasswordRepeatTextField.Text;

			if (!string.IsNullOrEmpty(Email) && Regex.Match(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success && !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(PasswordConfirm) && Password == PasswordConfirm)
			{
				CreateAccount(Email, Username, Password, PasswordConfirm, birthdate);
			}
			else
			{
				if (string.IsNullOrEmpty(Email) || !Regex.Match(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
				{
					errorList += "\n- Geen geldig e-mailadres ingevuld.";
				}
				if (string.IsNullOrEmpty(Username))
				{
					errorList += "\n- De gebruikersnaam is niet ingevuld.";
				}
				if (string.IsNullOrEmpty(Password))
				{
					errorList += "\n- Het wachtwoord is niet ingevuld.";
				}
				if (PasswordConfirm != Password)
				{
					errorList += "\n- De wachtwoorden komen niet overeen.";
				}
				Console.WriteLine("errorlist "+errorList);
				AMAlerts.DefaultAlert(this, "Gegevens niet correct", errorList);
			}

		}


		private async void CreateAccount(string email, string username, string password, string passwordconfirm, string birthDate)
		{
			SetLoader(AMStrings.LoadingCreateAccount);
			try
			{


				AccountSignUpService SignUpService = new AccountSignUpService();
				LightAccountRoot CurrentLightAccount = await SignUpService.CreateAccount(username, password, passwordconfirm, birthDate, email);

				if (CurrentLightAccount.Error != null && CurrentLightAccount.Error[0].Code == "E00")
				{
					Console.WriteLine("username digitalaccount saved: " + username);
					LocalStorage.SaveDigitalAccount(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), email, username);

					//delay 2 sec
					await Task.Delay(2000);
					Login(username, password);

					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
                    NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
					PerformSegue("ConfirmAccountSegue", this);

                    // Analytics event
                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: account - gegevens", "Account aanmaken", "Account aanmaken", null).Build();
                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                    Gai.SharedInstance.Dispatch();

					if (LoadingView != null)
					{
						LoadingView.Hide();
					}
				}
				if (CurrentLightAccount.Error[0].Code == "E21")
				{
					Console.WriteLine("username digitalaccount saved: " + username);
					LocalStorage.SaveDigitalAccount(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), email, username);


					//delay 2 sec
					await Task.Delay(2000);
					Login(username, password);

					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
					PerformSegue("ConfirmAccountSegue", this);
					if (LoadingView != null)
					{
						LoadingView.Hide();
					}
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentLightAccount.Error[0].Message);
				}
				if (CurrentLightAccount.Error[0].Code != "E00")
				{
					if (LoadingView != null)
					{
						LoadingView.Hide();
					}
					AMAlerts.DefaultAlert(this, "Fout bij aanmaken account", CurrentLightAccount.Error[0].Message);

					//voor test nu even wel gegevens opslaan
					//LocalStorage.SaveDigitalAccount(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), email, username);
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Digitaal account aanmaken", "Exception: "+ ex}
                });
				Console.WriteLine("exception aanmaken account: "+ex);
				AMAlerts.DefaultAlert(this, "Fout bij aanmaken account", AMStrings.ErrorDigitalCardNoConnection);
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}

			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 900);

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void SetLoaderSmall(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private async void Login(string username, string password)
		{

			LoginService LoginService = new LoginService();
			UserRoot UserResponse = await LoginService.Login(username, password);
			try
			{
				Console.WriteLine("UserResponse " + UserResponse.Error);

				if (UserResponse.Error.Length > 0)
				{
					//{
					/// <summary>
					/// Login request is succesfull without errors
					/// </summary>
					if (UserResponse.Error[0].Code == "E00")
					{
						SuccesLogin(UserResponse.User[0], UserResponse.TokenApp, username, password);
					}
					/// <summary>
					/// Login request is succesfull, but has errors (wrong password SAP CODES: P02 AND P03)
					/// </summary>
					if (UserResponse.Error[0].Code == "E34" || UserResponse.Error[0].Code == "E35")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

					}
					/// <summary>
					/// Login request is succesfull, but has errors (user not activated SAP CODE: P04)
					/// </summary>
					if (UserResponse.Error[0].Code == "E36")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);


					}
					if (UserResponse.Error[0].Code == "E24")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						//AMAlerts.P002Alert(this);


					}
					/// <summary>
					/// Login request is succesfull, but has errors (temporary blocked SAP CODE: P05)
					/// </summary>
					if (UserResponse.Error[0].Code == "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Naar de website");
						alert.AddButton("Annuleren");
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();


					}
					if (UserResponse.Error[0].Code == "E86")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Inloggegevens wijzigen");
						alert.AddButton("Annuleren");
						//alert.Message = "De combinatie van gebruikersnaam en wachtwoord blijft onbekend. Je account is geblokkeerd. Deblokkeren? Klik op inloggevens wijzigen en pas je inloggegevens aan."
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();


					}
					/// <summary>
					/// Login request is succesfull, but does not have any of the above errors
					/// </summary>
					if (UserResponse.Error[0].Code != "E00" && UserResponse.Error[0].Code != "24" && UserResponse.Error[0].Code != "E34" && UserResponse.Error[0].Code != "E35" && UserResponse.Error[0].Code != "E36" && UserResponse.Error[0].Code != "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

					}
				}
				else
				{
					Console.WriteLine("Niet ingelogd");
					NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);


				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Digitaal account aanmaken: login", "Exception: "+ ex}
                });

                Console.WriteLine("Niet ingelogd");
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

				AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);


			}
		}


		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
                // OK button
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset));
			}
			else
			{

				// Cancel button

			}
		}

		private async void SuccesLogin(User user, ResponseToken token, string username, string password)
		{
			// Set user
			LocalStorage.LocalLogin(user, token, username);
			// Set user card
			string cardnumber = "";

			if (!string.IsNullOrEmpty(user.CardNumber))
			{
				cardnumber = user.CardNumber;
			}
			else
			{
				cardnumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
			}

			if (cardnumber.Length == 9)
			{
				LoadingView.UpdateLabel(AMStrings.LoadingCardData);

				try
				{
					KaartService KaartService = new KaartService();
					//KaartData CurrentKaart = await KaartService.GetKaartData(user.CardNumber);
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(user.CardNumber, cancelToken);

					Console.WriteLine("currentkaart " + CurrentKaart + " , kaartnummer" + user.CardNumber);

					if (CurrentKaart.Kaart.Error != true)
					{
						LocalStorage.SetCard(CurrentKaart.Kaart, user.CardNumber);

						NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);
						LoadingView.UpdateLabel(AMStrings.LoadingAvatar);

					}
				}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Digitaal account aanmaken: kaartservice", "Exception: "+ ex}
                    });
					if (AMMain.CanShowError())
					{
						AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.ErrorNoConnectionMessage);
					}
				}
			}

			LoadingView.UpdateLabel(AMStrings.LoadingAvatar);

			AvatarService AvatarService = new AvatarService();
			AvatarRoot Avatar = await AvatarService.GetAvatar(token.TokenApp);

			//if (!string.IsNullOrEmpty(Avatar.TokenApp.TokenApp))
			//{
			//	AMMain.UpdateTokenApp(Avatar.TokenApp.TokenApp);
			//}

			if (Avatar != null && Avatar.Response != null)
			{
				if (Avatar.Response.Length > 0 && Avatar.Response[0].Image != null)
				{
					NSUserDefaults.StandardUserDefaults.SetString(Avatar.Response[0].Image, username + "-" + AMLocalStorage.Avatar);
				}
				else
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(username + "-" + AMLocalStorage.Avatar);
				}
			}

			LoadingView.UpdateLabel(AMStrings.LoadingGifts);

			AMMain.GetGiftsForBadge(this);

		}

		void dismissModalStack()
		{
			UIViewController vc = PresentingViewController;
			while (vc.PresentingViewController != null)
			{
				vc = vc.PresentingViewController;
			}
			vc.DismissViewController(false, null);
		}

		bool OnTextFieldShouldBeginEditing(UITextField textField)
		{

			var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Selecteer de datum", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};

			modalPicker.DatePicker.Mode = UIDatePickerMode.Date;
			modalPicker.DatePicker.IsAccessibilityElement = true;
			modalPicker.DatePicker.AccessibilityLabel = "BirthdatePicker";

			modalPicker.OnModalPickerDismissed += (s, ea) =>
			{
				var dateFormatter = new NSDateFormatter()
				{
					DateFormat = "dd-MM-yyyy"
				};

				if (modalPicker.DatePicker.Date != null)
				{
					DateTime date = DateTime.ParseExact(dateFormatter.ToString(modalPicker.DatePicker.Date), "dd-MM-yyyy", AMMain.Cultureinfo);
					var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Amsterdam");
					var localTime = TimeZoneInfo.ConvertTimeFromUtc(date, localTimeZone);
					long epochDate = SharedDatetime.ConvertToUnixTimestampLong(localTime);
					Console.WriteLine("epoch is " + epochDate);

					textField.Text = dateFormatter.ToString(modalPicker.DatePicker.Date);
					BirthDateChanged = epochDate;
					birthDateHasChanged = true;

				}
				Console.WriteLine("gegevens geboortedatum " + BirthDateChanged + " " + textField.Text);
				Console.WriteLine("geboortedatum  aangepast" + birthDateHasChanged);
			};

			this.PresentViewController(modalPicker, true, null);

			return false;
		}

		private void HideKeyboard()
		{
			UsernameTextField.ResignFirstResponder();
			CardnumberTextfield.ResignFirstResponder();
			EmailTextField.ResignFirstResponder();
			PasswordTextField.ResignFirstResponder();
			PasswordRepeatTextField.ResignFirstResponder();
		}

	}
}