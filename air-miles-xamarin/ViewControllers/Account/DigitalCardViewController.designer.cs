﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("DigitalCardViewController")]
    partial class DigitalCardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ArimilesLogoImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell BlueLogoCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView DigitalCardTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DigitlCardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DisclaimerLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmailTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NewsletterLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PrivacyButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PrivacyLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SkipCardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewCheckboxNewsletter { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewCheckboxPolicy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel VoorwaardenLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ArimilesLogoImageView != null) {
                ArimilesLogoImageView.Dispose ();
                ArimilesLogoImageView = null;
            }

            if (BlueLogoCell != null) {
                BlueLogoCell.Dispose ();
                BlueLogoCell = null;
            }

            if (Button != null) {
                Button.Dispose ();
                Button = null;
            }

            if (DigitalCardTableView != null) {
                DigitalCardTableView.Dispose ();
                DigitalCardTableView = null;
            }

            if (DigitlCardButton != null) {
                DigitlCardButton.Dispose ();
                DigitlCardButton = null;
            }

            if (DisclaimerLabel != null) {
                DisclaimerLabel.Dispose ();
                DisclaimerLabel = null;
            }

            if (EmailTextField != null) {
                EmailTextField.Dispose ();
                EmailTextField = null;
            }

            if (NewsletterLabel != null) {
                NewsletterLabel.Dispose ();
                NewsletterLabel = null;
            }

            if (PrivacyButton != null) {
                PrivacyButton.Dispose ();
                PrivacyButton = null;
            }

            if (PrivacyLabel != null) {
                PrivacyLabel.Dispose ();
                PrivacyLabel = null;
            }

            if (SkipCardButton != null) {
                SkipCardButton.Dispose ();
                SkipCardButton = null;
            }

            if (TitleLabel != null) {
                TitleLabel.Dispose ();
                TitleLabel = null;
            }

            if (ViewCheckboxNewsletter != null) {
                ViewCheckboxNewsletter.Dispose ();
                ViewCheckboxNewsletter = null;
            }

            if (ViewCheckboxPolicy != null) {
                ViewCheckboxPolicy.Dispose ();
                ViewCheckboxPolicy = null;
            }

            if (VoorwaardenLabel != null) {
                VoorwaardenLabel.Dispose ();
                VoorwaardenLabel = null;
            }
        }
    }
}