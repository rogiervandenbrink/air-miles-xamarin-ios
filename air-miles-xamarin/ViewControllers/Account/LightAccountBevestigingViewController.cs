﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class LightAccountBevestigingViewController : UIViewController
    {
		UIViewController Controller;
		UINavigationController NavController;
		LoadingOverlay LoadingView;
		NetworkStatus Internet;

        public LightAccountBevestigingViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();
			LightAccountBevestigingStartDeAppButton.TouchUpInside += delegate
			{
				PerformSegue("LightAccountToCardSegue", this);
				//TabController Redirect = App.Storyboard.InstantiateViewController("TabController") as TabController;
				//PresentModalViewController(Redirect, true);)
				//dismissModalStack();
			};
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(LightAccountBevestigingStartDeAppButton);
			NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.NoFirstPinTouch);

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: account - klaar");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			if (NavigationController != null)
			{
				NavigationController.SetNavigationBarHidden(true, false);
			}
		}

		void dismissModalStack() 
		{
    		UIViewController vc = PresentingViewController;
    		while (vc.PresentingViewController!=null) 
			{
        	vc = vc.PresentingViewController;
    		}
			vc.DismissViewController(false, null);
		}


    }
}