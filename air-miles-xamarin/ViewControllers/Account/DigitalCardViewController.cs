﻿using Foundation;
using System;
using UIKit;
using System.Text.RegularExpressions;
using CoreGraphics;
using Google.Analytics;
using Xamarin;
using Air_Miles_Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class DigitalCardViewController : UITableViewController
    {
     
		BemCheckBox checkboxPolicy;
		BemCheckBox checkboxNewsletter;

		LoadingOverlay LoadingView;

		UIViewController Controller;

		UILabel VoorwaardenText;

		public bool NewsletterBool;
		public bool PolicyBool;

		public string Routing { get; set; }

		public DigitalCardViewController (IntPtr handle) : base (handle)
        {
        }


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			//this.NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(3, 124, 194);
			var titleVoorwaarden = new NSAttributedString(
				"- de algemene voorwaarden",
				font: UIFont.FromName("Avenir-Book", 14.0f),
				foregroundColor: UIColor.Blue
				//underlineStyle: NSUnderlineStyle.Single
			);
			var titlePrivacy = new NSAttributedString(
				"- het privacy- en cookiebeleid",
				font: UIFont.FromName("Avenir-Book", 14.0f),
				foregroundColor: UIColor.Blue
				//underlineStyle: NSUnderlineStyle.Single
			);

			if (UIScreen.MainScreen.Bounds.Width > 320)
			{
				VoorwaardenLabel.Font = UIFont.FromName("Avenir-Book", 11f);
				NewsletterLabel.Font = UIFont.FromName("Avenir-Book", 11f);
			}else
			{
				VoorwaardenLabel.Font = UIFont.FromName("Avenir-Book", 9f);
				NewsletterLabel.Font = UIFont.FromName("Avenir-Book", 9f);
			}
			TitleLabel.AdjustsFontSizeToFitWidth = true;

			DigitlCardButton.TouchUpInside += SecondKaartAanvragenButton_TouchUpInside;
			SkipCardButton.TouchUpInside += SkipSecondKaartAanvragenButton_TouchUpInside;
			SkipCardButton.SetTitle("IK WIL TOCH GEEN KAARTNUMMER AANMAKEN", UIControlState.Normal);
			Button.SetAttributedTitle(titleVoorwaarden, UIControlState.Normal);
			PrivacyButton.SetAttributedTitle(titlePrivacy, UIControlState.Normal);

            DisclaimerLabel.Font = UIFont.FromName("Avenir-Book", 9f);
            DisclaimerLabel.SizeToFit();
            var atts = new UIStringAttributes();
            atts.ForegroundColor = UIColor.Black;
            atts.Font = UIFont.FromName("avenir-book", 9f);
            var secondAttributes = new UIStringAttributes
            {
                UnderlineStyle = NSUnderlineStyle.Single,
                Font = UIFont.FromName("avenir-book", 9f)
            };
            var txt = "Lees dan onze privacy- en cookieverklaring.";
            var attributedString = new NSMutableAttributedString(txt, atts);
            attributedString.SetAttributes(secondAttributes.Dictionary, new NSRange(attributedString.Length - 29, 29));
            PrivacyLabel.AttributedText = attributedString;

            UITapGestureRecognizer labelTap = new UITapGestureRecognizer(() => {
                // open privacy statement
                UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLPrivacyCookieBeleid));
            });

            PrivacyLabel.UserInteractionEnabled = true;
            PrivacyLabel.AddGestureRecognizer(labelTap);

			Button.TouchUpInside += delegate {
                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLAlgemeneVoorwaarden));
			};
			PrivacyButton.TouchUpInside += delegate {
                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLPrivacyCookieBeleid));
			};

			ArimilesLogoImageView.Image = UIImage.FromBundle("AirMilesAvatar");
			BlueLogoCell.BackgroundColor = UIColor.FromRGB(3, 124, 194);

			AMMain.AddDoneButton(EmailTextField);

			EmailTextField.KeyboardType = UIKeyboardType.EmailAddress;

			checkboxNewsletter = new BemCheckBox(new CGRect(0, 0, 20, 20), new BemCheckBoxDelegate());
			ViewCheckboxNewsletter.AddSubview(checkboxNewsletter);

			checkboxPolicy = new BemCheckBox(new CGRect(0, 0, 20, 20), new BemCheckBoxDelegate());
			ViewCheckboxPolicy.AddSubview(checkboxPolicy);

			NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
			UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
			{
				if (NavigationController != null)
				{
					NavigationController.PopViewController(false);
				}
				else
				{
					dismissModalStack();

				}

			}), true);
		
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			SetAnalytics();
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(DigitlCardButton);
			AMStyle.BorderedTextfield(EmailTextField);
			Title = "Kaartnummer aanmaken";
		}

		void SecondKaartAanvragenButton_TouchUpInside(object sender, EventArgs e)
		{
			if (Regex.Match(EmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success && checkboxPolicy.On)
			{
				CreateLightAccount(EmailTextField.Text, checkboxNewsletter.On);

			}
			if (Regex.Match(EmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success && !checkboxPolicy.On)
			{
				AMAlerts.DefaultAlert(this, "Waarschuwing", "Om verder te gaan, dien je akkoord te gaan met de algemene voorwaarden en het privacy- en cookiebeleid.");

			}
			if (!Regex.Match(EmailTextField.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
			{
				AMAlerts.DefaultAlert(this, "Waarschuwing", "Je hebt geen geldig e-mailadres ingevuld.");

			}
		}

		void SkipSecondKaartAanvragenButton_TouchUpInside(object sender, EventArgs e)
		{
			if (Routing == "Meer")
			{
				Console.WriteLine("skip button Routing Meer");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				NavigationController.PopToRootViewController(false);
				
			}
			else
			{
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				PerformSegue("TochGeenKaartSegue", this);


			}

		}

		private async void CreateLightAccount(string email, bool optin)
		{
			SetLoader(AMStrings.LoadingCreateCard);
			try
			{


			CreateLightAccountService LightAccountService = new CreateLightAccountService();
			LightAccountRoot CurrentLightAccount = await LightAccountService.CreateLightAccount(email, optin);

			if (CurrentLightAccount.Error != null && CurrentLightAccount.Error[0].Code == "E00")
			{
				LocalStorage.SaveLightAccount(CurrentLightAccount.LichtAccount[0].CardNumber, email);
					if (Routing == "Meer")
					{
						Console.WriteLine("Routing is Meer");
						NavigationController.PopViewController(false);
					}
				PerformSegue("KaartBevestigingSegue", this);

				Console.WriteLine("kaartnummer lightaccount " + CurrentLightAccount.LichtAccount[0].CardNumber.ToString());

                // Analytics event
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanmelden: kaart - gegevens", "Kaart aanmaken", "Kaart aanmaken", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();
				LoadingView.Hide();
				
			}
			if (CurrentLightAccount.Error[0].Code != "E00")
			{
				LoadingView.Hide();
				AMAlerts.DefaultAlert(this, "Fout bij aanmaken kaart", CurrentLightAccount.Error[0].Message);
			}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Light account aanmaken", "Exception: "+ ex}
                });

				AMAlerts.DefaultAlert(this, "Fout bij aanmaken kaart", AMStrings.ErrorDigitalCardNoConnection);
				LoadingView.Hide();
				Console.WriteLine("exception kaartservice: " + ex);


			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

			if (segue.Identifier == "KaartBevestigingSegue")
			{
				var destination = segue.DestinationViewController as KaartBevestigingViewController;
				if (Routing == "Meer")
				{
					destination.Routing = "Meer";
				}
			}
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: kaart - gegevens");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		void dismissModalStack()
		{
			UIViewController vc = PresentingViewController;
			while (vc.PresentingViewController != null)
			{
				vc = vc.PresentingViewController;
			}
			vc.DismissViewController(false, null);
		}
	}
}