﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("CreateDigitalAccountViewController")]
    partial class CreateDigitalAccountViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AccountAanmakenLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardNumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CreateDigitalAccountButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell EmailinputTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmailTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell EmailtextTableViewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton IconButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton IconUserNameButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell IntrotextCreateAccountTableviewcell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PasswordBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PasswordConfirmBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordRepeatTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SkipCreateAccountButtons { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ToptextaccountLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField UsernameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewBlueEye { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewEyeGrey { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AccountAanmakenLabel != null) {
                AccountAanmakenLabel.Dispose ();
                AccountAanmakenLabel = null;
            }

            if (CardNumberLabel != null) {
                CardNumberLabel.Dispose ();
                CardNumberLabel = null;
            }

            if (CreateDigitalAccountButton != null) {
                CreateDigitalAccountButton.Dispose ();
                CreateDigitalAccountButton = null;
            }

            if (EmailinputTableviewCell != null) {
                EmailinputTableviewCell.Dispose ();
                EmailinputTableviewCell = null;
            }

            if (EmailTextField != null) {
                EmailTextField.Dispose ();
                EmailTextField = null;
            }

            if (EmailtextTableViewCell != null) {
                EmailtextTableViewCell.Dispose ();
                EmailtextTableViewCell = null;
            }

            if (IconButton != null) {
                IconButton.Dispose ();
                IconButton = null;
            }

            if (IconUserNameButton != null) {
                IconUserNameButton.Dispose ();
                IconUserNameButton = null;
            }

            if (IntrotextCreateAccountTableviewcell != null) {
                IntrotextCreateAccountTableviewcell.Dispose ();
                IntrotextCreateAccountTableviewcell = null;
            }

            if (PasswordBackground != null) {
                PasswordBackground.Dispose ();
                PasswordBackground = null;
            }

            if (PasswordConfirmBackground != null) {
                PasswordConfirmBackground.Dispose ();
                PasswordConfirmBackground = null;
            }

            if (PasswordRepeatTextField != null) {
                PasswordRepeatTextField.Dispose ();
                PasswordRepeatTextField = null;
            }

            if (PasswordTextField != null) {
                PasswordTextField.Dispose ();
                PasswordTextField = null;
            }

            if (SkipCreateAccountButtons != null) {
                SkipCreateAccountButtons.Dispose ();
                SkipCreateAccountButtons = null;
            }

            if (ToptextaccountLabel != null) {
                ToptextaccountLabel.Dispose ();
                ToptextaccountLabel = null;
            }

            if (UsernameTextField != null) {
                UsernameTextField.Dispose ();
                UsernameTextField = null;
            }

            if (ViewBlueEye != null) {
                ViewBlueEye.Dispose ();
                ViewBlueEye = null;
            }

            if (ViewEyeGrey != null) {
                ViewEyeGrey.Dispose ();
                ViewEyeGrey = null;
            }
        }
    }
}