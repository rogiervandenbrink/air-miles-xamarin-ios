﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class FirstLightAccountViewController : UIViewController
    {

		public string Routing { get; set; }

        public FirstLightAccountViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			if (NavigationController != null)
			{

				if (Routing == "Login")
				{
					//NavigationController.SetNavigationBarHidden(true, false);
				}
				else
				{

					NavigationController.SetNavigationBarHidden(false, false);
				}
			}

			SkipLightAccountButton.SetTitleColor(UIColor.FromRGB(199, 199, 199), UIControlState.Normal);

			SkipLightAccountButton.TouchUpInside += delegate
			{
				if (Routing == "Meer")
				{
					Console.WriteLine("Routing is Meer");
					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
					NavigationController.PopToRootViewController(true);
				}
				if (Routing == "Login")
				{
					Console.WriteLine("Routing is Meer");
					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
					NavigationController.PopToRootViewController(true);
				}
				else {
					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
                    NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
					PerformSegue("SkipLightAccountSegue", this);
                }
			};
			FirstLightAccountButton.TouchUpInside += delegate {
				
				PerformSegue("CreateLightAccountSegue", this);
			};
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			SetAnalytics();
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(FirstLightAccountButton);
			DigitalAccountView.BackgroundColor = UIColor.FromRGB(1, 125, 195);

		}

		void dismissModalStack()
		{
			UIViewController vc = PresentingViewController;
			while (vc.PresentingViewController != null)
			{
				vc = vc.PresentingViewController;
			}
			vc.DismissViewController(false, null);
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

			if (segue.Identifier == "CreateLightAccountSegue")
			{
				var destination = segue.DestinationViewController as CreateDigitalAccountViewController;
				if (Routing == "Meer")
				{
					destination.Routing = "Meer";
				}
			}
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: kaart - vraag");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}
    }
}