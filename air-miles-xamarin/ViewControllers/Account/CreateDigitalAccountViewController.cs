﻿using Foundation;
using System;
using UIKit;
using System.Drawing;
using CoreGraphics;
using System.Text.RegularExpressions;
using Google.Analytics;
using System.Threading;
using Xamarin;
using Air_Miles_Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class CreateDigitalAccountViewController : UITableViewController
    {
		bool PasswordHidden = true;
		bool PasswordRepeatHidden = true;
		LoadingOverlay LoadingView;
		string errorList;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		public string Routing { get; set; }

        public CreateDigitalAccountViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();
			CreateDigitalAccountButton.TouchUpInside += delegate
			{
				SaveLightAccount();
			};


			Console.WriteLine("Email in localstorage "+NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
			CardNumberLabel.Text = "   "+ NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
            			PasswordTextField.SecureTextEntry = true;
			PasswordRepeatTextField.SecureTextEntry = true;

			UIButton eyeButton = new UIButton(UIButtonType.Custom);
			eyeButton.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
			eyeButton.TintColor = UIColor.LightGray;
			eyeButton.Frame = new CGRect(0, 0, 25, 15);
			ViewEyeGrey.AddSubview(eyeButton);

			eyeButton.TouchUpInside += delegate {
				PasswordHidden = !PasswordHidden;
				PasswordTextField.SecureTextEntry = PasswordHidden;
				if (!PasswordHidden)
				{
					eyeButton.SetImage(UIImage.FromBundle("IconEyeBlue"), UIControlState.Normal);
				}
				else
				{
					eyeButton.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);

				}
			};


			this.NavigationItem.SetRightBarButtonItem(
				new UIBarButtonItem(UIBarButtonSystemItem.Stop, (sender, args) =>
				{
					
					if (NavigationController != null)
					{
						NavigationController.PopToRootViewController(false);
					}else
					{
						dismissModalStack();

					}
					
				})
				, true);

			SkipCreateAccountButtons.TouchUpInside += SkipCreateAccountButton_TouchUpInside;
			SkipCreateAccountButtons.SetTitleColor(UIColor.FromRGB(199, 199, 199), UIControlState.Normal);

			IconButton.SetImage(UIImage.FromBundle("IconInfo"), UIControlState.Normal);
			IconButton.TouchUpInside += delegate {
				AMAlerts.DefaultAlert(this, AMStrings.TitlePasswordTip, AMStrings.MessagePasswordTip);	
			};

			IconUserNameButton.SetImage(UIImage.FromBundle("IconInfo"), UIControlState.Normal);
			IconUserNameButton.TouchUpInside += delegate
			{
				AMAlerts.DefaultAlert(this, AMStrings.TooltipUserNameTitle, AMStrings.TooltipTextUsername);
			};

			UIButton eyeButtonTwo = new UIButton(UIButtonType.Custom);
			eyeButtonTwo.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
			eyeButtonTwo.Frame = new CGRect(0, 0, 25, 15);
			ViewBlueEye.AddSubview(eyeButtonTwo);

			eyeButtonTwo.TouchUpInside += delegate {
				PasswordRepeatHidden = !PasswordRepeatHidden;
				PasswordRepeatTextField.SecureTextEntry = PasswordRepeatHidden;
				if (!PasswordRepeatHidden)
				{
					eyeButtonTwo.SetImage(UIImage.FromBundle("IconEyeBlue"), UIControlState.Normal);
				}
				else {
					eyeButtonTwo.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
				}
			};
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();

		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 40;

			if (indexPath.Row == 0)
			{

				height = 170.0f;
			}

			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail)))
			{
				if (indexPath.Row == 14)
				{

					height = 55.0f;
				}
				if (indexPath.Row == 6)
				{
					
					height = 0.0f;
				}
				if (indexPath.Row == 7)
				{

					height = 0.0f;
				}

			}

			return height;
		}

		public override void RowHighlighted(UITableView tableView, NSIndexPath rowIndexPath)
		{
			var Cell = tableView.CellAt(rowIndexPath);
			Cell.BackgroundColor = UIColor.White;
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(CreateDigitalAccountButton);
			AMStyle.BorderedGreyLabel(CardNumberLabel);
			AMStyle.BorderedTextfield(UsernameTextField);
			AMStyle.BorderedTextfield(EmailTextField);
			AMStyle.BorderedView(PasswordBackground);
			AMStyle.BorderedView(PasswordConfirmBackground);

			AMMain.AddDoneButton(UsernameTextField);
			AMMain.AddDoneButton(EmailTextField);
			AMMain.AddDoneButton(PasswordTextField);
			AMMain.AddDoneButton(PasswordRepeatTextField);

			AccountAanmakenLabel.TextColor = UIColor.White;
			ToptextaccountLabel.TextColor = UIColor.White;

			HideBackButton();

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: account - gegevens");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		void SkipCreateAccountButton_TouchUpInside (object sender, EventArgs e)
		{
			if (Routing == "Meer")
			{
				Console.WriteLine("Routing is Meer");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				NavigationController.PopToRootViewController(false);
			}
			if (Routing == "Login")
			{
				Console.WriteLine("Routing is Meer");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				NavigationController.PopToRootViewController(false);
			}
			else {
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
				PerformSegue("SkipAccountSegue", this);
			}
		}

		void SaveLightAccount()
		{
			errorList = "";
			string Email = "";
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail)))
			{
				Email = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
			}
			else
			{
				Email = EmailTextField.Text;

			}

			string Username = UsernameTextField.Text;
			string Password = PasswordTextField.Text;
			string PasswordConfirm = PasswordRepeatTextField.Text;

			if (!string.IsNullOrEmpty(Email) && Regex.Match(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success && !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(PasswordConfirm) && Password == PasswordConfirm)
			{
				CreateAccount(Email, Username, Password, PasswordConfirm);
			}
			else
			{
				if (string.IsNullOrEmpty(Email) && !Regex.Match(Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
				{
					errorList += "\n- Geen geldig e-mailadres ingevuld.";
				}
				if (string.IsNullOrEmpty(Username))
				{
					errorList += "\n- De gebruikersnaam is niet ingevuld.";
				}
				if (string.IsNullOrEmpty(Password))
				{
					errorList += "\n- Het wachtwoord is niet ingevuld.";
				}
				if (PasswordConfirm != Password)
				{
					errorList += "\n- De wachtwoorden komen niet overeen.";
				}
				AMAlerts.DefaultAlert(this, "Gegevens niet correct", errorList);
			}

		}


		private async void CreateAccount(string email, string username, string password, string passwordconfirm)
		{
			SetLoader(AMStrings.LoadingCreateAccount);
			try
			{


				AccountSignUpService SignUpService = new AccountSignUpService();
				LightAccountRoot CurrentLightAccount = await SignUpService.CreateAccount(username, password, passwordconfirm, null, email);

				if (CurrentLightAccount.Error != null && CurrentLightAccount.Error[0].Code == "E00")
				{
					Console.WriteLine("username digitalaccount saved: "+ username);
					LocalStorage.SaveDigitalAccount(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), email, username);

					Login(username, password);

					NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
                    NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
					PerformSegue("ConfirmAccountSegue", this);
					Console.WriteLine("kaartnummer digital account " + CurrentLightAccount.LichtAccount[0].CardNumber.ToString());
					LoadingView.Hide();
				}
				if (CurrentLightAccount.Error[0].Code != "E00")
				{
					LoadingView.Hide();
					AMAlerts.DefaultAlert(this, "Fout bij aanmaken account", CurrentLightAccount.Error[0].Message);

				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Digitaal account aanmaken", "Exception: "+ ex}
                });

				AMAlerts.DefaultAlert(this, "Fout bij aanmaken account", AMStrings.ErrorDigitalCardNoConnection);
				LoadingView.Hide();

			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 900);

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private async void Login(string username, string password)
		{

			LoginService LoginService = new LoginService();
			UserRoot UserResponse = await LoginService.Login(username, password);
			try
			{
				Console.WriteLine("UserResponse " + UserResponse.Error);

				if (UserResponse.Error.Length > 0)
				{
					//if (UserResponse.Error[0].Code != AMStrings.ErrorCode)
					//{
						/// <summary>
						/// Login request is succesfull without errors
						/// </summary>
						if (UserResponse.Error[0].Code == "E00")
						{
							SuccesLogin(UserResponse.User[0], UserResponse.TokenApp, username, password);
						}
						/// <summary>
						/// Login request is succesfull, but has errors (wrong password SAP CODES: P02 AND P03)
						/// </summary>
						if (UserResponse.Error[0].Code == "E34" || UserResponse.Error[0].Code == "E35")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);


							AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						}
						/// <summary>
						/// Login request is succesfull, but has errors (user not activated SAP CODE: P04)
						/// </summary>
						if (UserResponse.Error[0].Code == "E36")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

							AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

							
						}
						if (UserResponse.Error[0].Code == "E24")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

							AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
							
						}
						/// <summary>
						/// Login request is succesfull, but has errors (temporary blocked SAP CODE: P05)
						/// </summary>
						if (UserResponse.Error[0].Code == "E37")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

							AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

                            UIAlertView alert = new UIAlertView();
							alert.Title = AMStrings.ErrorLoginTitle;
							alert.AddButton("Naar de website");
							alert.AddButton("Annuleren");
							alert.Message = UserResponse.Error[0].Message;
							alert.AlertViewStyle = UIAlertViewStyle.Default;
							alert.Clicked += ButtonClicked;
							alert.Show();

							
						}
						if (UserResponse.Error[0].Code == "E86")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

							//AMAlerts.P005Alert(this);
							UIAlertView alert = new UIAlertView();
							alert.Title = AMStrings.ErrorLoginTitle;
							alert.AddButton("Inloggegevens wijzigen");
							alert.AddButton("Annuleren");
                        alert.Message = UserResponse.Error[0].Message;
							alert.AlertViewStyle = UIAlertViewStyle.Default;
							alert.Clicked += ButtonClicked;
							alert.Show();

							
						}
						/// <summary>
						/// Login request is succesfull, but does not have any of the above errors
						/// </summary>
						if (UserResponse.Error[0].Code != "E00" && UserResponse.Error[0].Code != "24" && UserResponse.Error[0].Code != "E34" && UserResponse.Error[0].Code != "E35" && UserResponse.Error[0].Code != "E36" && UserResponse.Error[0].Code != "E37")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

							AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
							
						}
				}
				else {
					Console.WriteLine("Niet ingelogd");
					NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);

					
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Digitaal account aanmaken: login", "Exception: "+ ex}
                });

                Console.WriteLine("Niet ingelogd");
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

				AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);

				
			}
		}


		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
                // OK button
                UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset));
			}
			else {

				// Cancel button

			}
		}

		private async void SuccesLogin(User user, ResponseToken token, string username, string password)
		{
			// Set user
			LocalStorage.LocalLogin(user, token, username);
			// Set user card
			string cardnumber = "";

			if (!string.IsNullOrEmpty(user.CardNumber))
			{
				cardnumber = user.CardNumber;
			}
			else {
				cardnumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
			}

			if (cardnumber.Length == 9)
			{
				LoadingView.UpdateLabel(AMStrings.LoadingCardData);

				try
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(user.CardNumber, cancelToken);

					Console.WriteLine("currentkaart " + CurrentKaart + " , kaartnummer" + user.CardNumber);


					if (CurrentKaart.Kaart.Error != true)
					{
						LocalStorage.SetCard(CurrentKaart.Kaart, user.CardNumber);

						NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);
						LoadingView.UpdateLabel(AMStrings.LoadingAvatar);


					}
				}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Digitaal account aanmaken: kaartservice", "Exception: "+ ex}
                    });
					if (AMMain.CanShowError())
					{
						AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.ErrorNoConnectionMessage);
					}
                }
			}

			LoadingView.UpdateLabel(AMStrings.LoadingAvatar);

			AvatarService AvatarService = new AvatarService();
			AvatarRoot Avatar = await AvatarService.GetAvatar(token.TokenApp);

			if (Avatar != null && Avatar.Response != null)
			{
				if (Avatar.Response.Length > 0 && Avatar.Response[0].Image != null)
				{
					NSUserDefaults.StandardUserDefaults.SetString(Avatar.Response[0].Image, username + "-" + AMLocalStorage.Avatar);
				}
				else {
					NSUserDefaults.StandardUserDefaults.RemoveObject(username + "-" + AMLocalStorage.Avatar);
				}
			}

			LoadingView.UpdateLabel(AMStrings.LoadingGifts);

			AMMain.GetGiftsForBadge(this);
			
		}

		void dismissModalStack()
		{
			UIViewController vc = PresentingViewController;
			while (vc.PresentingViewController != null)
			{
				vc = vc.PresentingViewController;
			}
			vc.DismissViewController(false, null);
		}

    }
}