﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("CreateDigitalAccountBirthDateViewController")]
    partial class CreateDigitalAccountBirthDateViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView AddCardImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton BirthdateButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField BirthdateTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CardnumberButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardNumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CardnumberTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ConditionsButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ConfirmEmailButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ConfirmEmailTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CreateDigitalAccountButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell EmailinputTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmailTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell EmailtextTableViewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel GeboortedatumLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton IconButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton IconUserNameButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell IntrotextCreateAccountTableviewcell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton NewCardnumberButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NewsletterLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PasswordBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PasswordConfirmBackground { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordRepeatTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PrivacyButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SkipCreateAccountButtons { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ToptextaccountLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField UsernameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewBlueEye { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewCheckboxNewsletter { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewCheckboxPolicy { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewEyeGrey { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel VoorwaardenLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AddCardImageView != null) {
                AddCardImageView.Dispose ();
                AddCardImageView = null;
            }

            if (BirthdateButton != null) {
                BirthdateButton.Dispose ();
                BirthdateButton = null;
            }

            if (BirthdateTextfield != null) {
                BirthdateTextfield.Dispose ();
                BirthdateTextfield = null;
            }

            if (CardnumberButton != null) {
                CardnumberButton.Dispose ();
                CardnumberButton = null;
            }

            if (CardNumberLabel != null) {
                CardNumberLabel.Dispose ();
                CardNumberLabel = null;
            }

            if (CardnumberTextfield != null) {
                CardnumberTextfield.Dispose ();
                CardnumberTextfield = null;
            }

            if (ConditionsButton != null) {
                ConditionsButton.Dispose ();
                ConditionsButton = null;
            }

            if (ConfirmEmailButton != null) {
                ConfirmEmailButton.Dispose ();
                ConfirmEmailButton = null;
            }

            if (ConfirmEmailTextField != null) {
                ConfirmEmailTextField.Dispose ();
                ConfirmEmailTextField = null;
            }

            if (CreateDigitalAccountButton != null) {
                CreateDigitalAccountButton.Dispose ();
                CreateDigitalAccountButton = null;
            }

            if (EmailinputTableviewCell != null) {
                EmailinputTableviewCell.Dispose ();
                EmailinputTableviewCell = null;
            }

            if (EmailTextField != null) {
                EmailTextField.Dispose ();
                EmailTextField = null;
            }

            if (EmailtextTableViewCell != null) {
                EmailtextTableViewCell.Dispose ();
                EmailtextTableViewCell = null;
            }

            if (GeboortedatumLabel != null) {
                GeboortedatumLabel.Dispose ();
                GeboortedatumLabel = null;
            }

            if (IconButton != null) {
                IconButton.Dispose ();
                IconButton = null;
            }

            if (IconUserNameButton != null) {
                IconUserNameButton.Dispose ();
                IconUserNameButton = null;
            }

            if (IntrotextCreateAccountTableviewcell != null) {
                IntrotextCreateAccountTableviewcell.Dispose ();
                IntrotextCreateAccountTableviewcell = null;
            }

            if (NewCardnumberButton != null) {
                NewCardnumberButton.Dispose ();
                NewCardnumberButton = null;
            }

            if (NewsletterLabel != null) {
                NewsletterLabel.Dispose ();
                NewsletterLabel = null;
            }

            if (PasswordBackground != null) {
                PasswordBackground.Dispose ();
                PasswordBackground = null;
            }

            if (PasswordConfirmBackground != null) {
                PasswordConfirmBackground.Dispose ();
                PasswordConfirmBackground = null;
            }

            if (PasswordRepeatTextField != null) {
                PasswordRepeatTextField.Dispose ();
                PasswordRepeatTextField = null;
            }

            if (PasswordTextField != null) {
                PasswordTextField.Dispose ();
                PasswordTextField = null;
            }

            if (PrivacyButton != null) {
                PrivacyButton.Dispose ();
                PrivacyButton = null;
            }

            if (SkipCreateAccountButtons != null) {
                SkipCreateAccountButtons.Dispose ();
                SkipCreateAccountButtons = null;
            }

            if (ToptextaccountLabel != null) {
                ToptextaccountLabel.Dispose ();
                ToptextaccountLabel = null;
            }

            if (UsernameTextField != null) {
                UsernameTextField.Dispose ();
                UsernameTextField = null;
            }

            if (ViewBlueEye != null) {
                ViewBlueEye.Dispose ();
                ViewBlueEye = null;
            }

            if (ViewCheckboxNewsletter != null) {
                ViewCheckboxNewsletter.Dispose ();
                ViewCheckboxNewsletter = null;
            }

            if (ViewCheckboxPolicy != null) {
                ViewCheckboxPolicy.Dispose ();
                ViewCheckboxPolicy = null;
            }

            if (ViewEyeGrey != null) {
                ViewEyeGrey.Dispose ();
                ViewEyeGrey = null;
            }

            if (VoorwaardenLabel != null) {
                VoorwaardenLabel.Dispose ();
                VoorwaardenLabel = null;
            }
        }
    }
}