﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("StartKaartAanvragenViewController")]
    partial class StartKaartAanvragenViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView AirMilesImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BackgroundView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton KaartAanvragenButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SkipKaartAanvragenButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AirMilesImageView != null) {
                AirMilesImageView.Dispose ();
                AirMilesImageView = null;
            }

            if (BackgroundView != null) {
                BackgroundView.Dispose ();
                BackgroundView = null;
            }

            if (KaartAanvragenButton != null) {
                KaartAanvragenButton.Dispose ();
                KaartAanvragenButton = null;
            }

            if (SkipKaartAanvragenButton != null) {
                SkipKaartAanvragenButton.Dispose ();
                SkipKaartAanvragenButton = null;
            }
        }
    }
}