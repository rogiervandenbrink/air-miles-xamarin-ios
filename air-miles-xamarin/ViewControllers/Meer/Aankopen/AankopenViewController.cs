﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Linq;
using System.Collections.Generic;

namespace airmilesxamarin
{
	partial class AankopenViewController : UITableViewController
	{
		static readonly Voucher[] Aankopen = new[] {
			new Voucher {  
				Title = "Efteling",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/06/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Ariane Inden",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/06/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Voorbeeld van een lange titel",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/07/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Efteling",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/07/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Efteling",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/07/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Efteling",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/08/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Ariane Inden",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/09/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Efteling",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/10/2016 00:00:00",
				Barcode = "1234"
			},
			new Voucher {  
				Title = "Ariane Inden",
				Detail = "1.200 Air Miles + € 10.-",
				EndDate = "01/10/2016 00:00:00",
				Barcode = "1234"
			},
		};

		// Segue is defined in storyboard
		string VoucherSegue = "VoucherSegue";

		public AankopenViewController (IntPtr handle) : base (handle)
		{
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			TableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems());
			TableView.RowHeight = 60f;

		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier == VoucherSegue) {
				var navctlr = segue.DestinationViewController as VoucherViewController;
				if (navctlr != null) {
					//navctlr.SetCurrentVoucher (Aankopen[0]);
				}
			}
		}

		private TableSection[] GetTableItems(){

			// Define lists for the Sections and Rows
			List<TableSection> Sections = new List<TableSection>();
			List<TableRow>[] Rows = new List<TableRow>[Aankopen.Length];

			// Section id starts with 0
			int SectionId = 0;

			for (int i = 0; i < Aankopen.Length; i++)
			{
				// if Section Rows don't exist, create them
				if(Rows[SectionId] == null){
					Rows[SectionId] = new List<TableRow>(); 
				}

				// Add the Table Rows
				Rows[SectionId].Add(new TableRow(){
					Title = Aankopen[i].Title,
					Detail = Aankopen[i].Detail,
					Segue = VoucherSegue,
					CellIdentifier = CellIdentifier.DetailButtonTableViewCell
				});

				// Always add a section if this is the first item
				if (SectionId == 0) {

					// Add the Section
					Sections.Add (new TableSection() {  
						Title = "Geldig tot",
						Detail = Aankopen[i].EndDate,
						Items = Rows[SectionId].ToArray ()
					});

					// Increase section id
					SectionId++;
		
				} 
				else {
					// If the EndDate is not the same as the Endate of the previous Table Row, add a new section
					if(Aankopen[i].EndDate != Aankopen[i - 1].EndDate){

						// Add the Section
						Sections.Add (new TableSection() {
							Title = "Geldig tot",
							Detail = Aankopen[i].EndDate,
							Items = Rows[SectionId].ToArray ()
						});

						// Increase section id
						SectionId++;

					}
				}
			}

			// Return the Sections as Rows as an array
			return Sections.ToArray ();

		}
	}
}
