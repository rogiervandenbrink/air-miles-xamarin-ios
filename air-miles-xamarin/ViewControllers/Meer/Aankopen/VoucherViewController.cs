﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;

namespace airmilesxamarin
{
	partial class VoucherViewController : UIViewController
	{
		Aankoop CurrentVoucher;
        airmilesxamarin.Aankoop[] firstAankopenBatch;
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;

		public VoucherViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (CurrentVoucher != null)
			{
				Title = CurrentVoucher.Title + " voucher";
			}

			RetrieveAankopen();
		}

		public void SetCurrentVoucher (Aankoop item) {
			CurrentVoucher = item;
		}

		private VoucherTableRow[] GetCollectionViewItems(Aankoop[] aankopen)
		{

			// Define lists for the Row
			List<VoucherTableRow> Rows = new List<VoucherTableRow>();

			if (aankopen != null)
			{
				for (int i = 0; i < aankopen.Length; i++)
				{
					// if Rows don't exist, create them
					if (Rows == null)
					{
						Rows = new List<VoucherTableRow>();
					}
						// Add the Table Row
					Rows.Add(new VoucherTableRow()
						{
							Description = aankopen[i].TotalString,
							Barcode = aankopen[i].VoucherNumber,
							BarcodeType = aankopen[i].BarcodeType,
							CellIdentifier = CellIdentifier.BarcodeDetailButtonCell
							//Segue = "PromotieDetailSegue",
						});

				}

				// Return the Rows as an arra
				return Rows.ToArray();
			}

			return Rows.ToArray();
		}



		public async void RetrieveAankopen()
		{
			//InitLogin();

			SetLoader(AMStrings.LoadingAankopen);
			try
			{

			AankopenService AankopenService = new AankopenService();

			if (firstAankopenBatch != null)
			{
				AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();


				if (CurrentAankopen.Error != null)
				{
					// if there is a response
					if (CurrentAankopen.Error[0].Code == "E00")
					{
						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
						Console.WriteLine("current aankopen aantal: "+CurrentAankopen.Aankopen.Length);

						VoucherCollectionView.ReloadData();

					}
					else {

						if (CurrentAankopen.Error[0].Code == "E23")
						{
							Console.WriteLine("E23 ");

							// Logout
							LocalStorage.Logout(TabBarController);

							// Redirect to login page
							LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
							Redirect.Routing = "TransactiesViewController";
							NavigationController.PushViewController(Redirect, false);
							//NavigationController.ViewControllers = new[] { Redirect };

							AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentAankopen.Error[0].Message);
						}
					}
				}
				else {
					Console.WriteLine("error");
					SetMaintanaceView();
				}


				LoadingView.Hide();
			}

			else {
				AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();


				if (CurrentAankopen.Error != null)
				{
					// if there is a respons
					if (CurrentAankopen.Error[0].Code == "E00")
					{
						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));

						VoucherCollectionView.ReloadData();
					}
					else {

						if (CurrentAankopen.Error[0].Code == "E23")
						{
							Console.WriteLine("E23 ");

							// Logou
							LocalStorage.Logout(TabBarController);

							// Redirect to login pag
							LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
							Redirect.Routing = "TransactieAankoopViewController";
							NavigationController.PushViewController(Redirect, false);
							//NavigationController.ViewControllers = new[] { Redirect };

							AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentAankopen.Error[0].Message);
						}
					}
				}
				else {
					Console.WriteLine("error");
					SetMaintanaceView();
				}
					if (LoadingView != null)
					{
						LoadingView.Hide();
					}

			}
				}
			catch (Exception ex)
			{
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
				SetMaintanaceView();
				Console.WriteLine("error aankopenservice "+ex);
			}
		}

		public async void UpdateAankopen()
		{
			//InitLogin();
			try
			{
			AankopenService AankopenService = new AankopenService();
			AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();

			// if there is a respons
			if (CurrentAankopen.Error != null)
			{
				if (CurrentAankopen.Error[0].Code == "E00")
				{
					Console.WriteLine("updating transacties");

					if (firstAankopenBatch != null)
					{
						Aankoop[] combined = new Aankoop[firstAankopenBatch.Length + CurrentAankopen.Aankopen.Length];
						Array.Copy(firstAankopenBatch, combined, firstAankopenBatch.Length);
						Array.Copy(CurrentAankopen.Aankopen, 0, combined, firstAankopenBatch.Length, CurrentAankopen.Aankopen.Length);
						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(combined));
						VoucherCollectionView.ReloadData();
						firstAankopenBatch = combined;

					}
					else {
						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
						firstAankopenBatch = CurrentAankopen.Aankopen;
						VoucherCollectionView.ReloadData();
					}
				}
			}
			else {
				SetMaintanaceView();
			}
				}
			catch (Exception ex)
			{
				
				SetMaintanaceView();
				Console.WriteLine("error update aankopenservice " + ex);
			}
		}


		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			VoucherCollectionView.ScrollEnabled = false;
			View.Add(MaintainanceView);
		}

		void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			//CGRect overlay = new CGRect(0, NavigationController.NavigationBar.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height + ButtonTabView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - (NavigationController.NavigationBar.Bounds.Height + ButtonTabView.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height));
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

	}
}
