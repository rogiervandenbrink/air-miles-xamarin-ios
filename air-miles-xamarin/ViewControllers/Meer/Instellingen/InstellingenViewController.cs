﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Linq;
using LocalAuthentication;
using System.Collections;
using System.Collections.Generic;
using Google.Analytics;

namespace airmilesxamarin
{
	partial class InstellingenViewController : UITableViewController
	{

		LAContext Context = new LAContext();
		NSError Error;
		UIViewController Controller;
		NSObject observer;

		public List<TableSection> InstellingenSections = new List<TableSection>();
		public List<TableRow> LoginGegevensTableItems = new List<TableRow>();
		public List<TableRow> BeveiligingTableItems = new List<TableRow>();
		public List<TableRow> IngelogdBlijvenTableItems = new List<TableRow>();

		public InstellingenViewController (IntPtr handle) : base (handle)
		{
			// Login Gegevens
			LoginGegevensTableItems.Add(new TableRow
			{
				Title = "Gebruikersnaam",
				Detail = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username),
				CellIdentifier = CellIdentifier.IconDetailRightTableViewCellDisabled
			});
			LoginGegevensTableItems.Add(new TableRow
			{
				Title = "Wachtwoord",
				Detail = "*******",
				CellIdentifier = CellIdentifier.IconDetailRightTableViewCellDisabled
			});

			// Beveiliging, if User has Touch ID, add list item
			if (Context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out Error))
			{
				BeveiligingTableItems.Add(new TableRow
				{
					Title = "Touch ID",
					CellIdentifier = CellIdentifier.IconSwitchTableViewCell
				});
			}
			BeveiligingTableItems.Add(new TableRow
			{
				Title = "Pincode",
				CellIdentifier = CellIdentifier.IconSwitchTableViewCell
			});

			// Check if user doesn't have a valid Pincode
			if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true)))
			//if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID))
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID))
				{
					BeveiligingTableItems.Add(new TableRow
					{
						Title = "Wijzig pincode",
						Detail = "*****",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						Segue = "EnterPinSegue"
					});
				}
				else
				{
					BeveiligingTableItems.Add(new TableRow
					{
						Title = "Wijzig pincode",
						Detail = "*****",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCellDisabled,
						Segue = "EnterPinSegue"
					});
				}

			}

			// Ingelogd blijven
			IngelogdBlijvenTableItems.Add(new TableRow
			{
				Title = "Uitloggen na sessie",
				CellIdentifier = CellIdentifier.IconSwitchTableViewCell
			});

			// Add sections to tablesource
			InstellingenSections.Add(new TableSection { Title = "Account", Items = LoginGegevensTableItems.ToArray(), FooterId = "InloggegevensAccount" });
			InstellingenSections.Add(new TableSection { Title = "Ontgrendelen", Items = BeveiligingTableItems.ToArray() });
			//InstellingenSections.Add(new TableSection { Title = "Ingelogd blijven", Items = IngelogdBlijvenTableItems.ToArray() });



		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			observer = NSNotificationCenter.DefaultCenter.AddObserver((NSString)"NSUserDefaultsDidChangeNotification", UpdateTableView);

		}

		void UpdateTableView(NSNotification obj)
		{
			InstellingenSections.Clear();
			LoginGegevensTableItems.Clear();
			BeveiligingTableItems.Clear();
			IngelogdBlijvenTableItems.Clear();
			// Login Gegevens
			LoginGegevensTableItems.Add(new TableRow
			{
				Title = "Gebruikersnaam",
				Detail = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username),
				CellIdentifier = CellIdentifier.IconDetailRightTableViewCellDisabled
			});
			LoginGegevensTableItems.Add(new TableRow
			{
				Title = "Wachtwoord",
				Detail = "*******",
				CellIdentifier = CellIdentifier.IconDetailRightTableViewCellDisabled
			});

			// Beveiliging, if User has Touch ID, add list item
			if (Context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out Error))
			{
				BeveiligingTableItems.Add(new TableRow
				{
					Title = "Touch ID",
					CellIdentifier = CellIdentifier.IconSwitchTableViewCell
				});
			}
			BeveiligingTableItems.Add(new TableRow
			{
				Title = "Pincode",
				CellIdentifier = CellIdentifier.IconSwitchTableViewCell
			});

			// Check if user doesn't have a valid Pincode
			if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true)))
			//if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID))
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID))
				{
					Console.WriteLine("pin present + enabled");
					BeveiligingTableItems.Add(new TableRow
					{
						Title = "Wijzig pincode",
						Detail = "*****",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						Segue = "EnterPinSegue"
					});
				}
				else
				{
					Console.WriteLine("pin present + disabled");

					BeveiligingTableItems.Add(new TableRow
					{
						Title = "Wijzig pincode",
						Detail = "*****",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCellDisabled,
						Segue = "EnterPinSegue"
					});
				}
			}

			// Ingelogd blijven
			IngelogdBlijvenTableItems.Add(new TableRow
			{
				Title = "Uitloggen na sessie",
				CellIdentifier = CellIdentifier.IconSwitchTableViewCell
			});

			// Add sections to tablesource
			InstellingenSections.Add(new TableSection { Title = "Account", Items = LoginGegevensTableItems.ToArray(), FooterId = "InloggegevensAccount" });
			InstellingenSections.Add(new TableSection { Title = "Ontgrendelen", Items = BeveiligingTableItems.ToArray() });

			TableView.Source = new DefaultSectionHeaderTableViewSource(this, InstellingenSections.ToArray());
			TableView.ReloadData();		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			App.LeftOnSecure = NavigationController;

			Initialize();

			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Inloggegevens");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			if (App.LeftOnSecure == NavigationController)
			{
				App.LeftOnSecure = null;
			}
		}

		public void Initialize()
		{
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false)
			{
				NavigationController.PopViewController(true);
			}
			else {
				TableView.Source = new DefaultSectionHeaderTableViewSource(this, InstellingenSections.ToArray());
				TableView.ReloadData();
				AMMain.SetExpiredSession(NavigationController, "InstellingenViewController");
			};
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{
			if (segueIdentifier == "EnterPinSegue")
			{
				Console.WriteLine("EnterPinSegue");

				// Check the device supports Touch ID
				//SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
				//NavigationController.PushViewController(Redirect, true);
				EnterPinTouchNavigationViewController Redirect = App.Storyboard.InstantiateViewController("EnterPinTouchNavigationViewController") as EnterPinTouchNavigationViewController;
				Redirect.Routing = "SetPinTouchViewController";
                // Google analaytics
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Inloggegevens", "Wijzig pincode", "Pin wijzigen", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();
				NavigationController.PresentModalViewController(Redirect, true);

				return false;
			}

			this.PerformSegue(segueIdentifier, sender);

			return true;
		}
	}
}
