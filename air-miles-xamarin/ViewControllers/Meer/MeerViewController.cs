﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Linq;
using CoreGraphics;
using MessageUI;
using Rivets;
using Google.Analytics;
using System.Threading;
using Xamarin;
using SafariServices;
using Air_Miles_Xamarin;
using airmilesxamarin.UI;
using System.IO;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{

	partial class MeerViewController : UITableViewController
	{
		MFMailComposeViewController mailController;
		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;
		bool ViewLoad;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		public MeerViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			ViewLoad = true;
			base.ViewDidLoad ();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;

			TableView.BackgroundColor = UIColor.FromRGB(217, 236, 246);
            TableView.TableFooterView = new SectionFooterMeerViewController();
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard))
			{
				TableView.Source = new DefaultSectionHeaderTableViewSource(this, MeerTableItems.LoggedOut());
			}
			else
			{
				TableView.Source = new DefaultSectionHeaderTableViewSource(this, MeerTableItems.NoCard());
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			ViewLoad = true;

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Meer");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{
			if (segueIdentifier == "UitloggenSegue")
			{
				VraagUitloggenSegue();

				return false;	
			}
			if (segueIdentifier == "UitloggenSegueDisabled")
			{
				UitloggenSegueDisabled();

				return false;
			}
			if (segueIdentifier == "GeldigheidSegue")
			{
				GeldigheidSegue();

				return false;
			}
			if (segueIdentifier == "NewCardSegue")
			{
				Kaartaanvragen();

				return false;
			}
			if (segueIdentifier == "InstellingenSegueDisabled")
			{
				InstellingenSegueDisabled();

				return false;
			}
			if (segueIdentifier == "ContactFormulierSegue")
			{
				ContactFormulierSegue();

				return false;	
			}
			if (segueIdentifier == "FeedbackSegue")
			{
				FeedbackSegue();

				return false;
			}
			if (segueIdentifier == "TelefoonSegue")
			{
				TelefoonSegue();

				return false;
			}
			if (segueIdentifier == "ChatSegue")
			{
				ChatSegue();

				return false;
			}
			if (segueIdentifier == "VeelgesteldeVragenSegue")
			{
				VeelgesteldeVragenSegue();

				return false;
			}
			if (segueIdentifier == "SamenSparenSegue")
			{
				SamenSparenSegue();

				return false;
			}
			if (segueIdentifier == "OverboekenSegue")
			{
				OverboekenSegue();

				return false;
			}
			if (segueIdentifier == "DonerenSegue")
			{
				DonerenSegue();

				return false;
			}
			if (segueIdentifier == "KaartBlokkerenSegue")
			{
				KaartBlokkerenSegue();

				return false;
			}
			if (segueIdentifier == "UitschrijvenSegue")
			{
				UitschrijvenSegue();

				return false;
			}
			if (segueIdentifier == "TwitterSegue")
			{
				TwitterSegue();

				return false;
			}
			if (segueIdentifier == "FacebookSegue")
			{
				FacebookSegue();

				return false;
			}

			this.PerformSegue(segueIdentifier, sender);
			return true;
		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue (segue, sender);

			TableSection[] TableItems = MeerTableItems.LoggedOut();
			ViewLoad = false;

			if (segue.Identifier == "InstellingenSegue") 
			{
				var destination = segue.DestinationViewController as InstellingenViewController;
			}

			if (segue.Identifier == "MijnAankopenSegue")
			{
				var destination = segue.DestinationViewController as TransactieAankoopViewController;

				destination.Aankopen = true;
			}

			if (segue.Identifier == "PartnersSegue")
			{
				var destination = segue.DestinationViewController as PartnersViewController;
				var rowPath = TableView.IndexPathForSelectedRow;
				var item = TableItems[rowPath.Section].Items[rowPath.Row];
				Console.WriteLine("item title partner "+item.Title);
				if (item.Title == "Sparen")
				{
					destination.Sparen = true;

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Partners", "Tabkeuze", "Sparen", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
				if (item.Title == "Inwisselen")
				{
					destination.Sparen = false;

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Partners", "Tabkeuze", "Inwisselen", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
			}

		}

		void Kaartaanvragen()
		{
			Console.WriteLine("kaart aanmaken segue");
			DigitalCardViewController FirstKaartModal = App.Storyboard.InstantiateViewController("DigitalCardViewController") as DigitalCardViewController;
			FirstKaartModal.Routing = "Meer";
			NavigationController.PushViewController(FirstKaartModal, true);
		}

		private void VraagUitloggenSegue()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.LogoutAskTitle;
			alert.AddButton("Uitloggen");
			alert.AddButton("Annuleren");
			alert.Message = AMStrings.LogoutAskMessage;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonClicked;
			alert.Show(); 
		}

		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				UitloggenSegue();
			}
			else {

				// Cancel button

			}
		}

		private void UitloggenSegue()
		{

			TableView.Source = new DefaultSectionHeaderTableViewSource(this, MeerTableItems.LoggedOut());
			TableView.ReloadData();

			LocalStorage.Logout(TabBarController);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Uitloggen", "Uitloggen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		private void InstellingenSegueDisabled()
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create("Je bent niet ingelogd", "Log in om je inloggegevens te bekijken.", UIAlertControllerStyle.Alert);

			//Add Actions
			okCancelAlertController.AddAction(UIAlertAction.Create("Inloggen", UIAlertActionStyle.Default, alert => LoginFromUitloggenSegue()));
			okCancelAlertController.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));

			//Present Alert
			PresentViewController(okCancelAlertController, true, null);
		}

		private void UitloggenSegueDisabled()
		{
			AMAlerts.DefaultAlert(this, "Je bent niet ingelogd", "Je bent niet ingelogd. Uitloggen is daardoor niet mogelijk.");
		}

		private void LoginFromUitloggenSegue()
		{
			LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
			Redirect.Routing = "InstellingenViewController";
			NavigationController.PushViewController(Redirect, false);
		}

		private void VeelgesteldeVragenSegue()
		{
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open website", "Veelgestelde vragen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLVeelgesteldeVragen + "?utm_campaign=meer&utm_medium=ios&utm_source=app"));
		}

		private void GeldigheidSegue()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "GeldigheidAirmilesURL");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void SamenSparenSegue()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "SamenSparenURL");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void OverboekenSegue()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "OverboekenURL");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void DonerenSegue()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "DonerenURL");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void KaartBlokkerenSegue()
        {
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "KaartBlokkerenURL");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void UitschrijvenSegue()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "UitschrijvenURL");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void ContactFormulierSegue()
		{
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open website", "Contactformulier", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLContactForm + "?utm_campaign=meer&utm_medium=ios&utm_source=app"));
		}

		private void FeedbackSegue()
		{
			if (MFMailComposeViewController.CanSendMail)
			{
				// do mail operations here
				mailController = new MFMailComposeViewController();
				mailController.SetToRecipients(new string[] { "tipsairmilesapp@airmiles.nl" });
				mailController.SetSubject("Suggestie(s) Air Miles app");
				mailController.SetMessageBody("", false);

                var textAttr = new UITextAttributes()
                {
                    TextColor = UIColor.Green
                };


                //create deviceinfo file
                string version_accp = "Air Miles versie " + NSBundle.MainBundle.InfoDictionary[new NSString("CFBundleVersion")].ToString() + " acceptatie";
                string version_prod = "Air Miles versie " + NSBundle.MainBundle.InfoDictionary[new NSString("CFBundleVersion")].ToString();
                string version = version_accp;
                if (!SharedWebUrls.ISACCP)
                {
                    version = version_prod;
                }
                string Now = DateTime.UtcNow.ToString("d");
                DateTime TimestampLog = DateTime.UtcNow;
                var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                var filename = Path.Combine(documents, "logiOS.txt");
                if (System.IO.File.Exists(filename))
                {
                    if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TimestampLogfile)))
                    {
                        DateTime dt = Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TimestampLogfile));
                        if (dt.AddDays(10) > TimestampLog)
                        {
                            var text = File.ReadAllText(filename);
                            if (!text.Contains("Devicename"))
                            {
                                File.AppendAllText(filename, "Devicename: " + UIDevice.CurrentDevice.Model + "\nOS: " + UIDevice.CurrentDevice.SystemVersion + "\nTimestamp: " + Now + "\nAppversion: " + version);
                            }
                        }
                        else
                        {
                            NSUserDefaults.StandardUserDefaults.SetString(TimestampLog.ToString(), AMLocalStorage.TimestampLogfile);
                            File.WriteAllText(filename, "Devicename: " + UIDevice.CurrentDevice.Model + "\nOS: " + UIDevice.CurrentDevice.SystemVersion + "\nTimestamp: " + Now + "\nAppversion: " + version);
                        }
                    }
                    else
                    {
                        NSUserDefaults.StandardUserDefaults.SetString(TimestampLog.ToString(), AMLocalStorage.TimestampLogfile);
                        File.WriteAllText(filename, "Devicename: " + UIDevice.CurrentDevice.Model + "\nOS: " + UIDevice.CurrentDevice.SystemVersion + "\nTimestamp: " + Now + "\nAppversion: " + version);
                    }
                }
                else
                {
                    NSUserDefaults.StandardUserDefaults.SetString(TimestampLog.ToString(), AMLocalStorage.TimestampLogfile);
                    File.WriteAllText(filename, "Devicename: " + UIDevice.CurrentDevice.Model + "\nOS: " + UIDevice.CurrentDevice.SystemVersion + "\nTimestamp: " + Now + "\nAppversion: " + version);
                }

                NSData nsdata;
                using (FileStream oStream = File.Open(filename, FileMode.Open))
                {
                    nsdata = NSData.FromStream(oStream);
                }

                var err = new NSError(new NSString("42"), -42);

                var MailBarButtonTextAttributes = new UIStringAttributes()
                {
                    Font = UIFont.FromName("Avenir-Heavy", 17),
                    ForegroundColor = UIColor.White
                };
                mailController.NavigationBar.TitleTextAttributes = MailBarButtonTextAttributes;
                mailController.NavigationBar.BarStyle = UIBarStyle.Black;
                //add attachment
                mailController.AddAttachmentData(nsdata, "text/plain", "logiOS.txt");

                mailController.Finished += (object s, MFComposeResultEventArgs args) =>
				{
					Console.WriteLine(args.Result.ToString());
					args.Controller.DismissViewController(true, null);
				};
                this.PresentViewController(mailController, true, null);
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Stuur email", "Feedback", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

			}
			else {
				AMAlerts.DefaultAlert(this, AMStrings.ErrorMailTitle, AMStrings.ErrorMailMessage);
			}
		}

		private void TelefoonSegue()
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create(AMStrings.CallTitle, AMStrings.CallMessage, UIAlertControllerStyle.Alert);

			// Url
			var url = new NSUrl("tel:0850022209");

			if (!UIApplication.SharedApplication.CanOpenUrl(url))
			{
				var AlertController = new UIAlertView("Bellen niet mogelijk", "Er is geen telefoonverbinding beschikbaar. Het bellen van de klantenservice is daardoor niet mogelijk.", null, AMStrings.Confirm, null);
				AlertController.Show();
			}
			else {
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Bel nummer", "Klantenservice", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				okCancelAlertController.AddAction(UIAlertAction.Create("Bellen", UIAlertActionStyle.Default, alert => UIApplication.SharedApplication.OpenUrl(url)));
				okCancelAlertController.AddAction(UIAlertAction.Create(AMStrings.Cancel, UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));

				//Present Alert
				this.PresentViewController(okCancelAlertController, true, null);
			}
		}

		private void ChatSegue()
		{
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open website", "Chat", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLChatForm));
		}

		private async void FacebookSegue()
		{
			string url = AMStrings.FBUrl;
			string weburl = AMStrings.FBWebUrl;

			SetLoader(AMStrings.LoadingUrl);
			try
			{
			NSUrl faceBookUrl = new NSUrl(url);
				if (UIApplication.SharedApplication.CanOpenUrl(faceBookUrl))
				{
					UIApplication.SharedApplication.OpenUrl(faceBookUrl);
                    // Google analaytics
                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open app", "Facebook", null).Build();
                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                    Gai.SharedInstance.Dispatch();
				}
				else
				{
					UIApplication.SharedApplication.OpenUrl(new NSUrl(weburl));
                    // Google analaytics
                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open website", "Facebook", null).Build();
                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                    Gai.SharedInstance.Dispatch();
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Meer menu", "Exception: "+ ex}
                });
				Console.WriteLine("error opening facebook url: "+ex);

				UIApplication.SharedApplication.OpenUrl(new NSUrl(weburl));
                // Google analaytics
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open website", "Facebook", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();
			}

			LoadingView.Hide();
		}

		private async void TwitterSegue()
		{

			SetLoader(AMStrings.LoadingUrl);

			string url = AMStrings.TwitterUrl;

			try
			{
				var resolver = new HttpClientAppLinkResolver();
				var appLinks = await resolver.ResolveAppLinks(url);

				var navigator = new AppLinkNavigator();
				var status = await navigator.Navigate(appLinks);

				if (status == NavigationResult.App)
				{
					Console.WriteLine("open twitter app");
					// Success! We navigated to another app
					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open app", "Twitter", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
				else if (status == NavigationResult.Web)
				{
					// No app installed or no App found in AppLinks,
					//  so we should fall back to URL instead:
					UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                    // Google analaytics
                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open website", "Twitter", null).Build();
                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                    Gai.SharedInstance.Dispatch();
				}
				else {
					// Some other error occurred
					UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("open twitter web error "+ex);
				// Some other error occurred
				Console.WriteLine("error opening twitter url: " + ex);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
			}
			LoadingView.Hide();
		}

		void SetLoader(string text)
		{
			CGRect bounds = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 900);

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			TableView.ScrollEnabled = false;
			View.Add(MaintainanceView);
		}

		public void PrepareCellsForAnimation()
		{
			for (int z = 0; z < TableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < TableView.NumberOfRowsInSection(z); i++)
				{
					IconDisclosureIndicatorTableViewCell Cell = TableView.CellAt(NSIndexPath.FromItemSection(i, z)) as IconDisclosureIndicatorTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						Cell.Alpha = 0.0f;
					}
				}
			}
		}

		public void AnimateCells()
		{
			int count = 0;
			for (int z = 0; z < TableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < TableView.NumberOfRowsInSection(z); i++)
				{
					IconDisclosureIndicatorTableViewCell Cell = TableView.CellAt(NSIndexPath.FromItemSection(i, z)) as IconDisclosureIndicatorTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						count++;
						Console.WriteLine("count is "+count);
						Cell.Alpha = 1.0f;
						AMStyle.SlideHorizontaly(Cell, true, count * 0.1, true);
					}
				}
			}
		}

		[Foundation.Export("safariViewControllerDidFinish:")]
		public void DidFinish(SFSafariViewController controller)
		{
			DismissViewController(true, null);
		}
	}

}