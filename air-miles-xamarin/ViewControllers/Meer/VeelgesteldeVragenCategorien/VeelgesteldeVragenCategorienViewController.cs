﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Linq;

namespace airmilesxamarin
{
	partial class VeelgesteldeVragenCategorienViewController : UITableViewController
	{
		static readonly FAQCategory[] VeelgesteldeVragenTableItems = new[] {
			new FAQCategory {  
				Title = "Lees onze meest gestelde vragen",
				ShortTitle = "Meest gestelde vragen",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				},
			},
			new FAQCategory {  
				Title = "Heb je een vraag over inloggen?",
				ShortTitle = "Inloggen",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				}
			},
			new FAQCategory {  
				Title = "Heb je een vraag over je Air Miles kaart?",
				ShortTitle = "Air Miles kaart",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				}
			},
			new FAQCategory {  
				Title = "Inschrijven en wijzigen van je persoonsgegevens",
				ShortTitle = "Persoonsgegevens",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				}
			},
			new FAQCategory {  
				Title = "E-tickets",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				}
			},
			new FAQCategory {  
				Title = "Geldigheid gespaarde Air Miles",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				}
			},
			new FAQCategory {  
				Title = "Goede doelen",
				Items = new [] {
					new FAQ {
						Title = "Title",
						Text = "Text"
					},
				}
			}
		};

		public VeelgesteldeVragenCategorienViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			TableView.Source = new VeelgesteldeVragenCategorienTableViewSource(this, VeelgesteldeVragenTableItems.ToArray());
			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 44f;
		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier == "VeelgesteldeVragenSegue") {
				var navctlr = segue.DestinationViewController as VeelgesteldeVragenViewController;
				if (navctlr != null) {
					var source = TableView.Source as VeelgesteldeVragenCategorienTableViewSource;
					var rowPath = TableView.IndexPathForSelectedRow;
					var item = source.GetVeelgesteldeVragen(rowPath.Row);
					navctlr.SetVeelgesteldeVragenCategorie (item);
				}
			}
		}
	}
}
