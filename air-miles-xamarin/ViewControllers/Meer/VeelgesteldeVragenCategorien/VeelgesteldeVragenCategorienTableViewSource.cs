﻿using System;
using UIKit;
using Foundation;

namespace airmilesxamarin
{
	public class VeelgesteldeVragenCategorienTableViewSource : UITableViewSource {

		UIViewController Controller;
		FAQCategory[] Items;

		public VeelgesteldeVragenCategorienTableViewSource (UIViewController parent, FAQCategory[] items)
		{
			Controller = parent;
			Items = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Items.Length;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			Controller.PerformSegue ("VeelgesteldeVragenSegue", this);
		}
			
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			String CellIdentifier = "DisclosureIndicatorTableViewCell";

			TableRow Item = new TableRow();
			Item.Title = Items [indexPath.Row].Title;

			DisclosureIndicatorTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as DisclosureIndicatorTableViewCell;
			Cell.UpdateCell(Item);

			return Cell;
		}

		public FAQCategory GetVeelgesteldeVragen(nint id) {
			return Items[id];
		}
	}
}

