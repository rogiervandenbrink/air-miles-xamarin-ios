﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
	partial class VeelgesteldeVragenViewController : UITableViewController
	{

		FAQCategory Categorie;

		public VeelgesteldeVragenViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Set title for page
			if (Categorie.ShortTitle != null) {
				Title = Categorie.ShortTitle;
			} else {
				Title = Categorie.Title;
			}
		}

		public void SetVeelgesteldeVragenCategorie (FAQCategory item) {
			Categorie = item;
		}
	}
}
