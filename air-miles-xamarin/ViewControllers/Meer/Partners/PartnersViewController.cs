﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Google.Analytics;
using Newtonsoft.Json;
using System.Threading;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class PartnersViewController : UIViewController
    {
		// Network status
		NetworkStatus Internet;

		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;

		UIView SelectedBorder;
		nfloat SelectedBorderX;

		PartnersRoot PartnerRoot;

		//refreshlayout
		UIRefreshControl RefreshControl;
		UIRefreshControl InwisselenRefreshControl;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		public bool Sparen { get; set; }

		bool ViewLoad;

        public PartnersViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
			nfloat SelectedBorderWidth = ScreenWidth / 2;

			ViewLoad = true;

			if (Sparen == true)
			{
				SelectedBorderX = 0;

			}
			else {
				SelectedBorderX = SelectedBorderWidth;

			}

			SelectedBorder = new UIView(new CGRect(SelectedBorderX, 0, SelectedBorderWidth, 4))
			{
				BackgroundColor = UIColor.FromRGB(0, 125, 195)
			};

			Internet = Reachability.InternetConnectionStatus();
			Reachability.ReachabilityChanged += InternetChanged;

			if (AMMain.DoWebService(AMLocalStorage.PartnerService))
			{
				RetrievePartners();
			}
			else
			{
				PartnerRoot = JsonConvert.DeserializeObject<PartnersRoot>(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonPartners));
				if (PartnerRoot.Partners != null)
				{
					SparenTableView.Source = new PartnerTableViewSource(this, GetPartnersSparen(PartnerRoot.Partners.ToArray()));
					SparenTableView.ReloadData();

					InwisselenTableView.Source = new PartnerTableViewSource(this, GetPartnersInwisselen(PartnerRoot.Partners.ToArray()));
					InwisselenTableView.ReloadData();
				}
			}
			SetState(Sparen);

			SparenButton.TouchUpInside += delegate
			{
				var GAIbarcode = DictionaryBuilder.CreateEvent("Partners", "Tab keuze", "Sparen", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAIbarcode);
				Gai.SharedInstance.Dispatch();

				Sparen = true;
				SetAnalytics();
				SetState(Sparen);
				AMMain.AnimateBorder(SelectedBorder, SparenButton.Center.X, SelectedBorder.Center.Y);
			};

			InwisselenButton.TouchUpInside += delegate
			{
				var GAIbarcode = DictionaryBuilder.CreateEvent("Partners", "Tab keuze", "Inwisselen", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAIbarcode);
				Gai.SharedInstance.Dispatch();

				Sparen = false;
				SetAnalytics();
				SetState(Sparen);
				AMMain.AnimateBorder(SelectedBorder, InwisselenButton.Center.X, SelectedBorder.Center.Y);
			};

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				SparenTableView.AddSubview(RefreshControl);
			}
			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				InwisselenRefreshControl = new UIRefreshControl();
				InwisselenRefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				InwisselenRefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				InwisselenTableView.AddSubview(InwisselenRefreshControl);
			}
			BorderView.AddSubview(SelectedBorder);

            Title = "Partners";

		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			if (ViewLoad)
			{
				//AnimateSparenCells();
				//AnimateInwisselenCells();
			}
			ViewLoad = true;
			SetAnalytics();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if (ViewLoad)
			{
				//PrepareInwisselenCellsForAnimation();
				//PrepareSparenCellsForAnimation();
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
		}

		void SetAnalytics()
		{
			if (Sparen == true)
			{
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Partners: sparen");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
			}
			else {
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Partners: inwisselen");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
			}
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			ViewLoad = false;
			var navctlr = segue.DestinationViewController as PartnerDetailViewController;

			if (segue.Identifier == "PartnerSparenDetailSegue")
			{
				Console.WriteLine("Sparen partner geselecteerd");

				var source = SparenTableView.Source as PartnerTableViewSource;
				var rowPath = SparenTableView.IndexPathForSelectedRow;
				SparenTableView.DeselectRow(rowPath, true);

				navctlr.SetPartner(PartnerRoot.Partners[source.GetRealIndex(GetPartnersSparen(PartnerRoot.Partners.ToArray()), rowPath.Section,rowPath.Row)]);
			}

			if (segue.Identifier == "PartnerInwisselenDetailSegue")
			{
				Console.WriteLine("Inwisselen partner geselecteerd");
				var source = SparenTableView.Source as PartnerTableViewSource;
				var rowPath = InwisselenTableView.IndexPathForSelectedRow;
				InwisselenTableView.DeselectRow(rowPath, true);

				navctlr.SetPartner(PartnerRoot.Partners[source.GetRealIndex(GetPartnersInwisselen(PartnerRoot.Partners.ToArray()), rowPath.Section, rowPath.Row)]);
			}
		}

		private TableRow[] GetPartnersSparen(Partner[] Partner)
		{

			// Define lists for the Rows
			List<TableRow> Rows = new List<TableRow>();

			for (int i = 0; i < Partner.Length; i++)
			{
				// if Rows don't exist, create them
				if (Rows == null)
				{
					Rows = new List<TableRow>();
				}

				if (Partner[i].Sparen == true)
				{
					// Add the Table Rows
					Rows.Add(new TableRow()
					{
						Index = i,
						Title = Partner[i].Title,
						Icon = Partner[i].Logo,
						Segue = "PartnerSparenDetailSegue",
						CellIdentifier = CellIdentifier.IconTableViewCell
					});
				}
			}

			// Return the Rows as an array
			return Rows.ToArray();
		}

		private TableRow[] GetPartnersInwisselen(Partner[] Partner)
		{

			// Define lists for the Rows
			List<TableRow> Rows = new List<TableRow>();

			for (int i = 0; i < Partner.Length; i++)
			{
				// if Rows don't exist, create them
				if (Rows == null)
				{
					Rows = new List<TableRow>();
				}

				if (Partner[i].Inwisselen != true)
				{

					// Add the Table Rows
					Rows.Add(new TableRow()
					{
						Index = i,
						Title = Partner[i].Title,
						Icon = Partner[i].Logo,
						Segue = "PartnerInwisselenDetailSegue",
						CellIdentifier = CellIdentifier.IconTableViewCell
					});
				}
			}

			// Return the Rows as an array
			return Rows.ToArray();
		}

		private async void RetrievePartners()
		{

			if (NetworkView != null)
			{
				NetworkView.Hide();
			}
			Internet = Reachability.InternetConnectionStatus();

			try
			{

			
			if (Internet != NetworkStatus.NotReachable)
			{
				SetLoader("Partners laden...");
				PartnersService PartnersService = new PartnersService();
				PartnerRoot = await PartnersService.GetPartners();

				if (PartnerRoot.Partners != null)
				{
					SparenTableView.Source = new PartnerTableViewSource(this, GetPartnersSparen(PartnerRoot.Partners.ToArray()));
					SparenTableView.ReloadData();

					InwisselenTableView.Source = new PartnerTableViewSource(this, GetPartnersInwisselen(PartnerRoot.Partners.ToArray()));
					InwisselenTableView.ReloadData();

					string service = AMLocalStorage.PartnerService;
					NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
				}
				else {
						if (RefreshControl != null)
						{
							RefreshControl.EndRefreshing();
						}
						if (InwisselenRefreshControl != null)
						{
							InwisselenRefreshControl.EndRefreshing();
						}
					SetMaintanaceView();
				}
					if (RefreshControl != null)
					{
						RefreshControl.EndRefreshing();
					}
					if (InwisselenRefreshControl != null)
					{
						InwisselenRefreshControl.EndRefreshing();
					}
					LoadingView.Hide();
			}
			else {
				// if localstorage
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonPartners)))
				{
					PartnerRoot = JsonConvert.DeserializeObject<PartnersRoot>(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonPartners));

					SparenTableView.Source = new PartnerTableViewSource(this, GetPartnersSparen(PartnerRoot.Partners.ToArray()));
					SparenTableView.ReloadData();

					InwisselenTableView.Source = new PartnerTableViewSource(this, GetPartnersInwisselen(PartnerRoot.Partners.ToArray()));
					InwisselenTableView.ReloadData();
					if (RefreshControl != null)
					{
						RefreshControl.EndRefreshing();
					}
					if (InwisselenRefreshControl != null)
					{
						InwisselenRefreshControl.EndRefreshing();
					}
				}
				else {
					if (RefreshControl != null)
					{
						RefreshControl.EndRefreshing();
					}
					if (InwisselenRefreshControl != null)
					{
						InwisselenRefreshControl.EndRefreshing();
					}
					SetNetworkView();
				}

			}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Partners", "Exception: "+ ex}
                });
				if (RefreshControl != null)
				{
					RefreshControl.EndRefreshing();
				}
				if (InwisselenRefreshControl != null)
				{
					InwisselenRefreshControl.EndRefreshing();
				}
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				Console.WriteLine("exception partnerservice "+ex);
				SetMaintanaceView();
			}
			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
			if (InwisselenRefreshControl != null)
			{
				InwisselenRefreshControl.EndRefreshing();
			}
		}

		void InternetChanged(object sender, EventArgs ea)
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable)
			{
				if (NetworkView != null)
				{
					NetworkView.RemoveFromSuperview();
				}

				if (PartnerRoot == null)
				{
					RetrievePartners();
				}
			}
			else
			{
				SetNetworkView();
			}
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void SetState(bool SparenState) 
		{
			if (SparenState == false)
			{
				// Show and hide tableview
				SparenTableView.Hidden = true;
				InwisselenTableView.Hidden = false;
				
				// Enable and disable buttons
				SparenButton.Enabled = true;
				InwisselenButton.Enabled = false;
			}

			if (SparenState == true)
			{
				// Show and hide tableview
				SparenTableView.Hidden = false;
				InwisselenTableView.Hidden = true;

				// Enable and disable buttons
				SparenButton.Enabled = false;
				InwisselenButton.Enabled = true;
			}
		}


		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			View.Add(MaintainanceView);
		}

		void Refresh()
		{
			var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Partners", "Pull to refresh", "Pull to refresh", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
			Gai.SharedInstance.Dispatch();
			RetrievePartners();

			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
			if (InwisselenRefreshControl != null)
			{
				InwisselenRefreshControl.EndRefreshing();
			}
		}

		public void PrepareInwisselenCellsForAnimation()
		{
			for (int z = 0; z < InwisselenTableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < InwisselenTableView.NumberOfRowsInSection(z); i++)
				{
					IconTableViewCell Cell = InwisselenTableView.CellAt(NSIndexPath.FromItemSection(i, z)) as IconTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						Cell.Alpha = 0.0f;
					}
				}
			}
		}

		public void AnimateInwisselenCells()
		{
			int count = 0;
			for (int z = 0; z < InwisselenTableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < InwisselenTableView.NumberOfRowsInSection(z); i++)
				{
					IconTableViewCell Cell = InwisselenTableView.CellAt(NSIndexPath.FromItemSection(i, z)) as IconTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						count++;
						Console.WriteLine("count is " + count);
						Cell.Alpha = 1.0f;
						AMStyle.SlideHorizontaly(Cell, true, count * 0.1, false);
					}
				}
			}
		}

		public void PrepareSparenCellsForAnimation()
		{
			for (int z = 0; z < SparenTableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < SparenTableView.NumberOfRowsInSection(z); i++)
				{
					IconTableViewCell Cell = SparenTableView.CellAt(NSIndexPath.FromItemSection(i, z)) as IconTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						Cell.Alpha = 0.0f;
					}
				}
			}
		}

		public void AnimateSparenCells()
		{
			int count = 0;
			for (int z = 0; z < SparenTableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < SparenTableView.NumberOfRowsInSection(z); i++)
				{
					IconTableViewCell Cell = SparenTableView.CellAt(NSIndexPath.FromItemSection(i, z)) as IconTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						count++;
						Console.WriteLine("count is " + count);
						Cell.Alpha = 1.0f;
						AMStyle.SlideHorizontaly(Cell, true, count * 0.1, true);
					}
				}
			}
		}
	}
}