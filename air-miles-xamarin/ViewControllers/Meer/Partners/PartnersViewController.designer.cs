﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("PartnersViewController")]
    partial class PartnersViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BorderView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton InwisselenButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView InwisselenTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SparenButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView SparenTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BorderView != null) {
                BorderView.Dispose ();
                BorderView = null;
            }

            if (InwisselenButton != null) {
                InwisselenButton.Dispose ();
                InwisselenButton = null;
            }

            if (InwisselenTableView != null) {
                InwisselenTableView.Dispose ();
                InwisselenTableView = null;
            }

            if (SparenButton != null) {
                SparenButton.Dispose ();
                SparenButton = null;
            }

            if (SparenTableView != null) {
                SparenTableView.Dispose ();
                SparenTableView = null;
            }
        }
    }
}