﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class PartnerInfoNavigationViewController : UINavigationController
    {
		public string InfoUrl { get; set; }
		public string PartnerTitle { get; set; }

        public PartnerInfoNavigationViewController (IntPtr handle) : base (handle)
        {
        }
    }
}