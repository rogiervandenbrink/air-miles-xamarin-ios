﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class PartnerInfoViewController : UIViewController
    {
		public string InfoUrl { get; set; }
		public string PartnerTitle { get; set; }

		public PartnerInfoViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			this.NavigationItem.SetRightBarButtonItem(
				new UIBarButtonItem(UIBarButtonSystemItem.Stop, (sender, args) =>
				{
					// button was clicked
					NavigationController.DismissViewController(true, null);
				})
			, true);
			NavigationItem.RightBarButtonItem.TintColor = UIColor.White;

			WebView.ShouldStartLoad = HandleShouldStartLoad;

			var NavCtrl = NavigationController as PartnerInfoNavigationViewController;

			WebView.LoadRequest(new NSUrlRequest(new NSUrl(NavCtrl.InfoUrl)));
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			SetAnalytics();
		}

		void SetAnalytics()
		{
			var NavCtrl = NavigationController as PartnerInfoNavigationViewController;

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Partner informatie: " + NavCtrl.PartnerTitle);
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		bool HandleShouldStartLoad(UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
		{
			// Filter out clicked links
			if (navigationType == UIWebViewNavigationType.LinkClicked)
			{
				if (UIApplication.SharedApplication.CanOpenUrl(request.Url))
				{
					// Open in Safari instea
					UIApplication.SharedApplication.OpenUrl(request.Url);
                    //AMMain.OpenURLInApp(this, request.Url.AbsoluteString);
					return false;
				}
			}

			return true;
		}
    }
}