﻿using Foundation;
using System;
using UIKit;
using System.Text.RegularExpressions;

namespace airmilesxamarin
{
	public partial class PartnerDetailCell : UITableViewCell
	{
		
		public PartnerDetailCell(IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(PartnerDetailViewController parent, Partner partner)
		{
			TitleLabel.Text = AMMain.RemoveHTMLTags(partner.Title);

			if (!AMMain.RemoveHTMLTags(partner.Summary).Contains(";}"))
			{
				SummaryLabel.Text = AMMain.RemoveHTMLTags(partner.Summary);
			}
			else
			{
				SummaryLabel.Text = "";	
			}

			AMStyle.RoundedButton(MoreInfoButton);
		}
	}
}