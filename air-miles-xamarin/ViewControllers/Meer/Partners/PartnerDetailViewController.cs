﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using SDWebImage;
using System.Collections.Generic;
using System.Net;
using Google.Analytics;
using Newtonsoft.Json.Linq;
using System.Json;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
	public partial class PartnerDetailViewController : UIViewController, IUITableViewDelegate
    {
		// Network status
		NetworkStatus Internet;

		bool LoadedDetails;

		bool findable;

		Partner CurrentPartner;

		public string InfoUrl { get; set; }
		public Promotie[] Promoties;

		//refreshlayout
		UIRefreshControl RefreshControl;


        public PartnerDetailViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			LoadedDetails = false;

			// Get partner details and set partner promotions
			GetPartnerDetails();

			Title = CurrentPartner.Title;

			TableView.Delegate = this;
			TableView.Source = new PartnerDetailTableViewSource(this, CurrentPartner);
			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 100f;

			PartnerImage.SetImage(
				url: new NSUrl(CurrentPartner.Image),
				placeholder: UIImage.FromBundle("placeholder.png")
			);

			PartnerImage.Layer.MasksToBounds = true;

			AMStyle.DropShadow(LogoView);

			LogoImage.SetImage(
				url: new NSUrl(CurrentPartner.Logo),
				placeholder: UIImage.FromBundle("placeholder.png")
			);

			TableView.TableHeaderView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, (UIScreen.MainScreen.Bounds.Width * 0.583) + 70));
			TableView.TableFooterView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 0));


			PromotionsCollectionView.ScrollEnabled = false;

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				TableView.AddSubview(RefreshControl);
			}
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{

			if (segueIdentifier == "PartnerInfoSegue")
			{
				if (InfoUrl == null)
				{
					UIAlertView alert = new UIAlertView()
					{
						Title = "Kan pagina niet openen",
						Message = "Om deze pagina te openen moet je met internet verbonden zijn."
					};
					alert.AddButton(AMStrings.Confirm);
					alert.Show();

					return false;
				}
			}
			return true;
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			var navctlr = segue.DestinationViewController as PartnerInfoNavigationViewController;

			if (segue.Identifier == "PartnerInfoSegue")
			{
				if (InfoUrl != null)
				{
					navctlr.PartnerTitle = CurrentPartner.Title;
					navctlr.InfoUrl = InfoUrl;

					// Google analaytics
					var GAIopenWebsite = DictionaryBuilder.CreateEvent("Partners", "Open website - meer informatie", CurrentPartner.Title + " - " + InfoUrl, null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAIopenWebsite);
					Gai.SharedInstance.Dispatch();
				}
			}
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			SetAnalytics();
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Partner detail: " + CurrentPartner.Title);
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}

		public void SetPartner(Partner partner)
		{
			CurrentPartner = partner;
		}

		public void SetScrollOffset(nfloat currentOffset)
		{
			Console.WriteLine("offset " + currentOffset);
			if (currentOffset < -64)
			{
				PartnerImage.Frame = new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, ((UIScreen.MainScreen.Bounds.Width * 0.583) + (currentOffset * -1)) - 64);
				LogoView.Frame = new CGRect(LogoView.Frame.X, ((UIScreen.MainScreen.Bounds.Width * 0.583) + 24), LogoView.Frame.Width, LogoView.Frame.Height);
				LogoView.Alpha = (nfloat)((0.001 * ((currentOffset + 64) * 40)) + 1);
			}
			else {
				PartnerImage.Frame = new CGRect(0, currentOffset * -1, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Width * 0.583);
				LogoView.Frame = new CGRect(LogoView.Frame.X, (PartnerImage.Frame.Height - 40) + (currentOffset * -1), LogoView.Frame.Width, LogoView.Frame.Height);
				LogoView.Alpha = 1f;
			}
		}

		private async void GetPartnerDetails()
		{
			Internet = Reachability.InternetConnectionStatus();
			Reachability.ReachabilityChanged += InternetChanged;

			if (Internet != NetworkStatus.NotReachable)
			{
                try
                {
                    PartnerDetailService PartnerDetailService = new PartnerDetailService();
    				PartnerDetail CurrentPartnerDetails = await PartnerDetailService.GetPartnerDetails(CurrentPartner.Id);

    				if (CurrentPartnerDetails != null)
    				{
    					LoadedDetails = true;
    					ShowPartnerDetails(CurrentPartnerDetails);
    				}
                }
                catch (Exception ex)
                {
                    RefreshControl.EndRefreshing();
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"PartnerDetails", "Exception: "+ ex}
                    });
                }
				RefreshControl.EndRefreshing();
			}
		}

		private void ShowPartnerDetails(PartnerDetail partner)
		{
			InfoUrl = partner.Webview;
			int promotionLength = partner.Promotions.Promotions.Length;


			if (promotionLength > 0)
			{
				
				Promoties = partner.Promotions.Promotions;
				Promotie[] PartnerPromoties = GetCollectionViewItems();

				if (PartnerPromoties.Length > 0)
				{
					if (Promoties != null)
					{
						UILabel HuidigeActiesLabel = new UILabel(new CGRect(20, 0, TableView.Bounds.Width - 40, 60))
						{
							Text = "Huidige acties",
							Font = UIFont.FromName("Avenir-Black", 21f),
							TextColor = UIColor.FromRGB(12, 103, 181),
						};

						PromotionsCollectionView.DataSource = new PromotieCollectionViewSource(this, PartnerPromoties);
						PromotionsCollectionView.Delegate = new PromotieFlowDelegate(this, PartnerPromoties);

						PromotionsCollectionView.Frame = new CGRect(0, 40, TableView.TableFooterView.Bounds.Width, ((AMSizes.PromotionHeight + 15) * PartnerPromoties.Length) + 20);
						PromotionsCollectionView.BackgroundColor = UIColor.White;

						TableView.TableFooterView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, ((AMSizes.PromotionHeight + 15) * PartnerPromoties.Length) + 20 + 50));

						TableView.TableFooterView.AddSubview(PromotionsCollectionView);
						TableView.TableFooterView.AddSubview(HuidigeActiesLabel);
						TableView.ReloadData();

					}
				}
			}
			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
		}

		void InternetChanged(object sender, EventArgs ea)
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable && LoadedDetails == false)
			{
				GetPartnerDetails();
			}
		}

		private class PromotieFlowDelegate : UICollectionViewDelegateFlowLayout
		{
			UIViewController Controller;
			Promotie[] Rows;

			public PromotieFlowDelegate(UIViewController parent, Promotie[] items) : base()
			{
				Controller = parent;
				Rows = items;
			}

			public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
			{
				nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;

				return new CGSize(AMSizes.PromotionWidth, AMSizes.PromotionHeight);
			}

			public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.FromRGB(250, 250, 250);
			}

			public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
			{
				// Analytics Screenname
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Partner promotie: " + Rows[indexPath.Row].Title);
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

				// Google analaytics
				var GAIopenWebsite = DictionaryBuilder.CreateEvent("Partners", "Open website - aanbod", Rows[indexPath.Row].Title + " - " + Rows[indexPath.Row].Url, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAIopenWebsite);
				Gai.SharedInstance.Dispatch();

				UIApplication.SharedApplication.OpenUrl(new NSUrl(Rows[indexPath.Row].Url));
                //AMMain.OpenURLInApp(Controller, Rows[indexPath.Row].Url);
			}
		}

		private Promotie[] GetCollectionViewItems()
		{

			// Define lists for the Row
			List<Promotie> Rows = new List<Promotie>();

			for (int i = 0; i < Promoties.Length; i++)
			{
				// if Rows don't exist, create them
				if (Rows == null)
				{
					Rows = new List<Promotie>();
				}

				if (Promoties[i].Keywords != null)
				{
					//JsonArray keywords = (JsonArray)Promoties[i].Keywords[0];
					Console.WriteLine("keywords != null");


					foreach (string key in Promoties[i].Keywords)
					{
						if (key == "onvindbaar")
						{
							findable = true;
							Console.WriteLine("key is onvindbaar " + Promoties[i].Title);
						}
						else
						{
							Console.WriteLine("key != onvindbaar" + Promoties[i].Title);
							findable = false;
						}
					}

					if (!string.IsNullOrEmpty(Promoties[i].Image) && !findable)
					{
						// Add the Table Row
						Rows.Add(new Promotie()
						{
							Title = AMMain.RemoveHTMLTags(AMMain.ReplaceBreakpoints(Promoties[i].Title.Replace("<br>", " "))),
							Image = Promoties[i].Image,
							EndDate = Promoties[i].EndDate,
							RegistrationDate = Promoties[i].RegistrationDate,
							PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
							PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
							PriceValue = AMMain.RemoveHTMLTags(Promoties[i].PriceValue),
							PriceLabel = AMMain.RemoveHTMLTags(Promoties[i].PriceLabel),
							PropositionValue = AMMain.RemoveHTMLTags(Promoties[i].PropositionValue),
							PropositionLabel = AMMain.RemoveHTMLTags(Promoties[i].PropositionLabel),
							PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
							PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
							MilesOnly = Promoties[i].MilesOnly,
							Url = Promoties[i].Url
							//Segue = "PromotieDetailSegue",
						});
						findable = false;
					}
				}
				else
				{
					if (!string.IsNullOrEmpty(Promoties[i].Image))
					{
						// Add the Table Row
						Rows.Add(new Promotie()
						{
							Title = AMMain.RemoveHTMLTags(AMMain.ReplaceBreakpoints(Promoties[i].Title.Replace("<br>", " "))),
							Image = Promoties[i].Image,
							EndDate = Promoties[i].EndDate,
							RegistrationDate = Promoties[i].RegistrationDate,
							PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
							PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
							PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
							PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
							PriceValue = AMMain.RemoveHTMLTags(Promoties[i].PriceValue),
							PriceLabel = AMMain.RemoveHTMLTags(Promoties[i].PriceLabel),
							PropositionValue = AMMain.RemoveHTMLTags(Promoties[i].PropositionValue),
							PropositionLabel = AMMain.RemoveHTMLTags(Promoties[i].PropositionLabel),
							MilesOnly = Promoties[i].MilesOnly,
							Url = Promoties[i].Url
							//Segue = "PromotieDetailSegue",
						});
					}
				}
			}

			// Return the Rows as an arra
			return Rows.ToArray();
		}

		void Refresh()
		{
			GetPartnerDetails();

		}
    }

}