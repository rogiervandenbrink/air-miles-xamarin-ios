﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;
using MessageUI;
using Rivets;
using CoreGraphics;
using Newtonsoft.Json;
using System.Threading;
using Xamarin;
using ObjCRuntime;
using System.Collections.Generic;
using Air_Miles_Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class MijnViewController : UITableViewController
    {
		// Network status
		NetworkStatus Internet;

		MFMailComposeViewController mailController;

		UIActivityIndicatorView activitySpinner;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();
		LoadingOverlay LoadingView;
		TransactiesOverlay TransactiesOverlay;
        public string Routing { get; set; }

		NSObject observer;

		CGPoint pt;

		//refreshlayout
		UIRefreshControl RefreshControl;

		bool ViewLoad;

        public MijnViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			// derive the center x and y
			nfloat centerX = UIScreen.MainScreen.Bounds.Width / 2;
			nfloat centerY = UIScreen.MainScreen.Bounds.Height / 2;

			// create the activity spinner, center it horizontall and put it 5 points above center x
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activitySpinner.Frame = new CGRect(
				centerX - (activitySpinner.Frame.Width / 2),
				centerY - activitySpinner.Frame.Height - 20,
				activitySpinner.Frame.Width,
				activitySpinner.Frame.Height);
			activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
			Add(activitySpinner);
			activitySpinner.StartAnimating();

			//PrepareCellsForAnimation();

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				TableView.AddSubview(RefreshControl);
			}
			ViewLoad = true;

            MijnNavigationViewController NavCrtl = NavigationController as MijnNavigationViewController;
            Console.WriteLine("Routing Mijn is "+NavCrtl.Routing);
            Routing = NavCrtl.Routing;

            base.ViewDidLoad();

		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(true);
			if (App.LeftOnSecure == NavigationController)
			{
				App.LeftOnSecure = null;
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

            if (!ViewLoad)
            {
                SetBackupView("");
            }

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Mijn");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			TableView.ReloadData();

			App.LeftOnSecure = NavigationController;
			Console.WriteLine("NAV CTRL SECURE = " + App.LeftOnSecure);

			if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))// && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
			{
  				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.DigitalAccount))
				{
					TableView.Source = new DefaultSectionHeaderTableViewSource(this, MijnTableItems.DigitalCard());
					//TableView.ReloadData();
					MijnHeader.Hidden = false;
				}
				else
				{
					TableView.Source = new DefaultSectionHeaderTableViewSource(this, MijnTableItems.LoggedIn());
					MijnHeader.Hidden = false;
				}
			}
            if(string.IsNullOrEmpty(Routing)){
                Routing = "MijnViewController";
            }
            AMMain.SetExpiredSession(NavigationController, Routing);

			HideBackButton();

			SetHeader();

			if (AMMain.DoWebService(AMLocalStorage.KaartService))
			{
				UpdateBalance();
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

            if(TransactiesOverlay != null){
                TransactiesOverlay.Hide();
            }

            if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
            {
                if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.DigitalAccount))
                {
                    TableView.Source = new DefaultSectionHeaderTableViewSource(this, MijnTableItems.DigitalCard());
                    TableView.ReloadData();
                    MijnHeader.Hidden = false;
                }
                else
                {
                    TableView.Source = new DefaultSectionHeaderTableViewSource(this, MijnTableItems.LoggedIn());
                    MijnHeader.Hidden = false;
                }
                if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.FromSaldoViewController))
                {
                    NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.FromSaldoViewController);
                    App.TabBar.SelectedIndex = 0;
                }
            }

            else
            {
                if (string.IsNullOrEmpty(Routing))
                {
                    Routing = "MijnViewController";
                }
                // Redirect to login pag
                LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
                Redirect.Routing = Routing;
                Console.WriteLine("redirect "+Redirect);
                NavigationController.PushViewController(Redirect, false);
            }

			Console.WriteLine("showpopup" +(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.UserShowPopUp)));

			bool PopUpProfile = false;

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.UserShowPopUp) && (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertPinID) || NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertTouchID)))
			{
				ShowPopupMEssage();
				PopUpProfile = true;

				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.UserShowPopUp);
			}
			if (PopUpProfile)
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent))
				{
					ShowPopupBounceIndicators();
				}
			}
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.ToProfileViewController))
			{
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.ToProfileViewController);
                GegevensSegue();
			}
			
			activitySpinner.RemoveFromSuperview();
			UpdateBadge();

			SetAnalytics();
		}

		async void UpdateBadge()
		{
			await AMMain.GetGiftsForBadge(this);
			TableView.ReloadData();
		}

		void ReloadTable(NSNotification obj)
		{
			TableView.ReloadData();
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Mijn");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void SetHeader()
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.LinkcardsMessage)))
			{
				MijnHeader.Frame = new CGRect(0, -2, UIScreen.MainScreen.Bounds.Width, 148f);
				MijnHeader.TranslatesAutoresizingMaskIntoConstraints = true;
				InvitationView.Hidden = false;
				InvitationView.BackgroundColor = UIColor.FromRGB(46, 123, 190);
				InvitationView.UserInteractionEnabled = true;
				UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(TapThat);
				InvitationView.AddGestureRecognizer(tapGesture);
				LeftIconImageView.Image = LeftIconImageView.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				LeftIconImageView.TintColor = UIColor.White;

				RightIconImageView.Image = RightIconImageView.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				RightIconImageView.TintColor = UIColor.White;

				InvitationTitleLabel.Text = AMStrings.InvitationViewTitle;
				InvitationTitleLabel.Lines = 0;
				InvitationTitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
				InvitationTitleLabel.AdjustsFontSizeToFitWidth = true;
				InvitationTitleLabel.Font = UIFont.FromName("Avenir-Book", 12f);
				InvitationTitleLabel.MinimumFontSize = 10f;

			}else
			{
				MijnHeader.Frame = new CGRect(0, -2, UIScreen.MainScreen.Bounds.Width, 72f);
				MijnHeader.TranslatesAutoresizingMaskIntoConstraints = true;
				InvitationView.Hidden = true;
			}

			MijnHeader.BackgroundColor = UIColor.FromRGB(221, 15, 150);

            if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
            {
                AMMain.SetAvatar(UserAvatar, "Airmiles_Placeholder.png");
                getAvatar(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp), NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username));
            }

			// Set title
			if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName) != null)
			{
				UserTitle.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName);
			}
			else {
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation)))
				{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation) == "heer" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation).ToLower() == "male")
					{
						UserTitle.Text = "Dhr.";
					}
					if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation) == "mevrouw" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation).ToLower() == "female")
					{
						UserTitle.Text = "Mevr.";
					}
					else
					{
						UserTitle.Text = "";
					}
				}
			}

			if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName) != null)
			{
				string prefix;
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix)))
				{
					prefix = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix) + " ";
				}
				else {
					prefix = "";
				}
				UserLastname.Text = prefix + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
			}

			if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance) != null)
			{
				UserBalance.Text = AMMain.SetBalanceFormat(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance));
			}
			else
			{
				UserBalance.Text = "0";

			}
			if (ViewLoad)
			{
				MijnHeader.Hidden = true;
			}
		}

		async void getAvatar(string Token, string username)
		{
			try
			{
				AvatarService AvatarService = new AvatarService();
				AvatarRoot Avatar = await AvatarService.GetAvatar(Token);


				if (Avatar != null && Avatar.Response != null)
				{
					if (Avatar.Response.Length > 0 && Avatar.Response[0].Image != null)
					{
						NSUserDefaults.StandardUserDefaults.SetString(Avatar.Response[0].Image, username + "-" + AMLocalStorage.Avatar);
						AMMain.SetAvatar(UserAvatar, "Airmiles_Placeholder.png");
					}
					else
					{
						NSUserDefaults.StandardUserDefaults.RemoveObject(username + "-" + AMLocalStorage.Avatar);
					}
				}
			}
			catch (Exception ex)
			{
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
				}
			}
		}

		void SetBackupView(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			TransactiesOverlay = new TransactiesOverlay(bounds, text);
			View.Add(TransactiesOverlay);
		}


		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{
			if (segueIdentifier == "UitloggenSegue")
			{
				VraagUitloggenSegue();

				return false;
			}
			if (segueIdentifier == "UitloggenSegueDisabled")
			{
				UitloggenSegueDisabled();

				return false;
			}
			if (segueIdentifier == "GeldigheidSegue")
			{
				GeldigheidSegue();

				return false;
			}
			if (segueIdentifier == "InstellingenSegueDisabled")
			{
				InstellingenSegueDisabled();

				return false;
			}
			if (segueIdentifier == "ContactFormulierSegue")
			{
				ContactFormulierSegue();

				return false;
			}
			if (segueIdentifier == "FeedbackSegue")
			{
				FeedbackSegue();

				return false;
			}
			if (segueIdentifier == "TelefoonSegue")
			{
				TelefoonSegue();

				return false;
			}
			if (segueIdentifier == "ChatSegue")
			{
				ChatSegue();

				return false;
			}
			if (segueIdentifier == "VeelgesteldeVragenSegue")
			{
				VeelgesteldeVragenSegue();

				return false;
			}
			if (segueIdentifier == "SamenSparenSegue")
			{
				SamenSparenSegue();

				return false;
			}
			if (segueIdentifier == "OverboekenSegue")
			{
				OverboekenSegue();

				return false;
			}
			if (segueIdentifier == "KaartVervangenSegue")
			{
				Kaartvervangen();

				return false;
			}
			if (segueIdentifier == "DonerenSegue")
			{
				DonerenSegue();

				return false;
			}
			if (segueIdentifier == "KaartBlokkerenSegue")
			{
				KaartBlokkerenSegue();

				return false;
			}
			if (segueIdentifier == "UitschrijvenSegue")
			{
				UitschrijvenSegue();

				return false;
			}
			if (segueIdentifier == "GeldigheidSegue")
			{
				GeldigheidSegue();

				return false;
			}
			if (segueIdentifier == "TwitterSegue")
			{
				TwitterSegue();

				return false;
			}
			if (segueIdentifier == "FacebookSegue")
			{
				FacebookSegue();

				return false;
			}

			if (segueIdentifier == "GegevensSegue")
			{
				GegevensSegue();

				return false;
			}


			this.PerformSegue(segueIdentifier, sender);
			return true;
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);
			ViewLoad = false;
			TableSection[] TableItems = MeerTableItems.LoggedOut();

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				TableItems = MeerTableItems.LoggedIn();
			}

			if (segue.Identifier == "InstellingenSegue")
			{
				var destination = segue.DestinationViewController as InstellingenViewController;
			}

			if (segue.Identifier == "DigitalekaartAanmakenSegue")
			{
				//var destination = segue.DestinationViewController as StartNavigationViewController;
				KaartAanmakenSegue();
			}

			if (segue.Identifier == "MijnTransactiesSegue")
			{
				var destination = segue.DestinationViewController as TransactieAankoopViewController;

				destination.Aankopen = false;
			}

			if (segue.Identifier == "PersoonlijkAanbodSegue")
			{
				Internet = Reachability.InternetConnectionStatus();
 				
				if (Internet != NetworkStatus.NotReachable)
				{
					var destination = segue.DestinationViewController as PromotiesViewController;
					destination.Title = "Speciaal voor jou!";
					destination.PersonalGifts = true;
				}
				else {
					AMAlerts.DefaultAlert(App.ViewCtrl, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}

			if (segue.Identifier == "MijnAankopenSegue")
			{
				var destination = segue.DestinationViewController as TransactieAankoopViewController;

				destination.Aankopen = true;
			}

			if (segue.Identifier == "PartnersSegue")
			{
				var destination = segue.DestinationViewController as PartnersViewController;
				var rowPath = TableView.IndexPathForSelectedRow;
				var item = TableItems[rowPath.Section].Items[rowPath.Row];

				if (item.Title == "Sparen")
				{
					destination.Sparen = true;

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Partners", "Tabkeuze", "Sparen", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
				if (item.Title == "Inwisselen")
				{
					destination.Sparen = false;

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Partners", "Tabkeuze", "Inwisselen", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
			}

		}

		private void KaartAanmakenSegue()
		{
			Console.WriteLine("kaart aanmaken segue");
			FirstLightAccountViewController FirstKaartModal = App.Storyboard.InstantiateViewController("FirstLightAccountViewController") as FirstLightAccountViewController;
			NavigationController.PushViewController(FirstKaartModal, true);
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FirstKaartAanvragen);
		}

		private void VraagUitloggenSegue()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.LogoutAskTitle;
			alert.AddButton("Uitloggen");
			alert.AddButton("Annuleren");
			alert.Message = AMStrings.LogoutAskMessage;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonClicked;
			alert.Show();
		}

		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				Console.WriteLine("navigationcontroller logout dialog "+sender);
				// OK button
				UitloggenSegue();
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Uitloggen", "Uitloggen", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else {

				// Cancel button

			}
		}

		private void ShowPopupMEssage()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpMessageTitle;
			alert.AddButton("Naar gegevens");
			alert.AddButton("Annuleren");
			alert.Message = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PopUpMessage);
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpClicked;
			alert.Show();
		}

		void ButtonPopUpClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				GegevensSegue();

			}
			else {

				// Cancel button

			}
		}

		private void ShowPopupBounceIndicators()
		{
			List<string> bounceList = new List<string>();
			bounceList.Clear();
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason)))
			{
				bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason)))
			{
				bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason)))
			{
				bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason)))
			{
				bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason));
			}
			string combindedBounceIndicators = string.Join(Environment.NewLine, bounceList);

            // Google analaytics
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Bounceindicator", combindedBounceIndicators, null).Build();
            Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
            Gai.SharedInstance.Dispatch();

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpMessageTitle;
			alert.AddButton("Naar gegevens");
			alert.AddButton("Annuleren");
			alert.Message = combindedBounceIndicators;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpBIClicked;
			alert.Show();
		}

		void ButtonPopUpBIClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				GegevensSegue();

			}
			else
			{

				// Cancel button

			}
		}

		void GegevensSegue()
		{
			ViewLoad = false;
			ProfileViewController Redirect = App.Storyboard.InstantiateViewController("ProfileViewController") as ProfileViewController;
			Redirect.Title = "Gegevens";
			NavigationController.PushViewController(Redirect, true);
		}

		private void UitloggenSegue()
		{

			TableView.Source = new DefaultSectionHeaderTableViewSource(this, MeerTableItems.LoggedOut());
			TableView.ReloadData();

			this.ViewDidDisappear(false);
			this.ViewWillAppear(false);
			Console.WriteLine("navigationcontroller uitloggensegue " + NavigationController);
			LocalStorage.Logout(TabBarController);

			// Redirect to login pag
			if (NavigationController != null)
			{
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
				Redirect.Routing = "MijnViewController";
                NavigationController.PushViewController(Redirect, false);
			}

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Uitloggen", "Uitloggen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

		}

		private void InstellingenSegueDisabled()
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create("Je bent niet ingelogd", "Log in om je inloggegevens te bekijken.", UIAlertControllerStyle.Alert);

			//Add Actions
			okCancelAlertController.AddAction(UIAlertAction.Create("Inloggen", UIAlertActionStyle.Default, alert => LoginFromUitloggenSegue()));
			okCancelAlertController.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));

			//Present Alert
			PresentViewController(okCancelAlertController, true, null);
		}

		private void UitloggenSegueDisabled()
		{
			AMAlerts.DefaultAlert(this, "Je bent niet ingelogd", "Je bent niet ingelogd. Uitloggen is daardoor niet mogelijk.");
		}

		private void LoginFromUitloggenSegue()
		{
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Inloggen", "Inloggen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
			Redirect.Routing = "MijnViewController";
			NavigationController.PushViewController(Redirect, false);
		}

		private void VeelgesteldeVragenSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Open website", "Veelgestelde vragen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLVeelgesteldeVragen));
        }

		private void GeldigheidSegue()
		{
			ValidityViewController Redirect = App.Storyboard.InstantiateViewController("ValidityViewController") as ValidityViewController;
			NavigationController.PushViewController(Redirect, false);

		}

		private void SamenSparenSegue()
		{
			LinkCardOverviewViewController Redirect = App.Storyboard.InstantiateViewController("LinkCardOverviewViewController") as LinkCardOverviewViewController;
			NavigationController.PushViewController(Redirect, false);

		}

		private void OverboekenSegue()
		{
			TransferViewController Redirect = App.Storyboard.InstantiateViewController("TransferViewController") as TransferViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		private void Kaartvervangen()
		{
			ReplaceCardViewController Redirect = App.Storyboard.InstantiateViewController("ReplaceCardViewController") as ReplaceCardViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		private void DonerenSegue()
		{
			DonateViewController Redirect = App.Storyboard.InstantiateViewController("DonateViewController") as DonateViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		private void KaartBlokkerenSegue()
		{
			BlockCardViewController Redirect = App.Storyboard.InstantiateViewController("BlockCardViewController") as BlockCardViewController;;
			NavigationController.PushViewController(Redirect, false);
		}

		private void UitschrijvenSegue()
		{
			SignoutViewController Redirect = App.Storyboard.InstantiateViewController("SignoutViewController") as SignoutViewController;
			NavigationController.PushViewController(Redirect, true);
		}

		private void ContactFormulierSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Open website", "Contactformulier", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLContactForm));
        }

		private void FeedbackSegue()
		{
			if (MFMailComposeViewController.CanSendMail)
			{
				// do mail operations here

				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);

				mailController = new MFMailComposeViewController();

				mailController.SetToRecipients(new string[] { "tipsairmilesapp@airmiles.nl" });
				mailController.SetSubject("Suggestie(s) Air Miles app");
				mailController.SetMessageBody("", false);

				mailController.Finished += (object s, MFComposeResultEventArgs args) =>
				{
					Console.WriteLine(args.Result.ToString());
					args.Controller.DismissViewController(true, null);
				};

				this.PresentViewController(mailController, true, null);
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Stuur email", "Feedback", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

			}
			else {
				AMAlerts.DefaultAlert(this, "Geen mailclient", "Er is geen mailclient beschikbaar. Het verzenden van feedback is daardoor niet mogelijk.");
			}
		}

		private void TelefoonSegue()
		{
			//Create Alert
			var okCancelAlertController = UIAlertController.Create("Air Miles bellen", "Weet je zeker dat je wilt bellen met Air Miles?", UIAlertControllerStyle.Alert);

			// Url
			var url = new NSUrl("tel:09008877");

			if (!UIApplication.SharedApplication.CanOpenUrl(url))
			{
				var AlertController = new UIAlertView("Bellen niet mogelijk", "Er is geen telefoonverbinding beschikbaar. Het bellen van de klantenservice is daardoor niet mogelijk.", null, AMStrings.Confirm, null);
				AlertController.Show();
			}
			else {
				//Add Actions
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Bel nummer", "Klantenservice", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				okCancelAlertController.AddAction(UIAlertAction.Create("Bellen", UIAlertActionStyle.Default, alert => UIApplication.SharedApplication.OpenUrl(url)));
				okCancelAlertController.AddAction(UIAlertAction.Create(AMStrings.Cancel, UIAlertActionStyle.Cancel, alert => Console.WriteLine("Annuleren")));

				//Present Alert
				this.PresentViewController(okCancelAlertController, true, null);
			}
		}

		private void ChatSegue()
		{
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Open website", "Chat", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLChatForm));
            //AMMain.OpenURLInApp(this, AMClient.URLChatForm);
		}

		private async void FacebookSegue()
		{
			string url = "https://www.facebook.com/airmiles.nederland/";
			var resolver = new HttpClientAppLinkResolver();
			var appLinks = await resolver.ResolveAppLinks(url);

			var navigator = new AppLinkNavigator();
			var status = await navigator.Navigate(appLinks);

			if (status == NavigationResult.App)
			{
				Console.WriteLine("open facebook app");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open app", "Facebook", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
				// Success! We navigated to another app
			}
			else if (status == NavigationResult.Web)
			{
				Console.WriteLine("open facebook web");
				// No app installed or no App found in AppLinks,
				//  so we should fall back to URL instead:
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				//UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                AMMain.OpenURLInApp(this, url);
			}
			else {
				Console.WriteLine("open facebook web error");
				// Some other error occurred
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                //AMMain.OpenURLInApp(this, url);
			}
		}

		private async void TwitterSegue()
		{
			string url = "https://twitter.com/airmiles_nl";
			var resolver = new HttpClientAppLinkResolver();
			var appLinks = await resolver.ResolveAppLinks(url);

			var navigator = new AppLinkNavigator();
			var status = await navigator.Navigate(appLinks);

			if (status == NavigationResult.App)
			{
				Console.WriteLine("open facebook app");
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				// Success! We navigated to another app
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Meer", "Open app", "Twitter", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else if (status == NavigationResult.Web)
			{
				Console.WriteLine("open facebook web");
				// No app installed or no App found in AppLinks,
				//  so we should fall back to URL instead:
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                //AMMain.OpenURLInApp(this, url);
			}
			else {
				Console.WriteLine("open facebook web error");
				// Some other error occurred
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
				UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
                //AMMain.OpenURLInApp(this, url);
			}
		}

		private async void UpdateBalance()
		{
			
			try
			{


				Internet = Reachability.InternetConnectionStatus();

				// If internet
				if (Internet != NetworkStatus.NotReachable)
				{
					KaartService KaartService = new KaartService();
					//KaartData kaartData = await KaartService.GetKaartData(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

					if (CurrentKaart.Error[0].Code == "E00")
					{
						Console.WriteLine("Updating Card data");
						Console.WriteLine("response balance "+JsonConvert.SerializeObject(CurrentKaart));
						string service = AMLocalStorage.KaartService;
						NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
						LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
						NSUserDefaults.StandardUserDefaults.Synchronize();
						if (UserBalance != null)
						{
							UserBalance.Text = AMMain.SetBalanceFormat(CurrentKaart.Kaart.Balance.ToString());
						}
					}
				}
				else
				{
					//SetNetworkView();
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Mijn menu", "Exception: "+ ex}
                });

				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			TableView.ReloadData();
			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
		}

		void TapThat()
		{
			ShowPopupLinkCards();
		}

		private void ShowPopupLinkCards()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpLinkcardsTitle;
			alert.AddButton("Accepteren");
			alert.AddButton("Weigeren");
			alert.Message = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.LinkcardsMessage);
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonLinkCardsClicked;
			alert.Show();

		}

		void ButtonLinkCardsClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK buttonn
				AcceptInviteLinkCards(true);
				MijnHeader.Frame = new CGRect(0, -2, UIScreen.MainScreen.Bounds.Width, 72f);
				MijnHeader.TranslatesAutoresizingMaskIntoConstraints = true;
				InvitationView.Hidden = true;
				TableView.ReloadData();
				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Koppelen uitnodiging ontvangen", "Accepteren", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else {

				// Cancel button
				AcceptInviteLinkCards(false);
				MijnHeader.Frame = new CGRect(0, -2, UIScreen.MainScreen.Bounds.Width, 72f);
				MijnHeader.TranslatesAutoresizingMaskIntoConstraints = true;
				InvitationView.Hidden = true;
				TableView.ReloadData();
				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Koppelen uitnodiging ontvangen", "Weigeren", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
		}

		async void AcceptInviteLinkCards(bool accept)
		{
			SetLoader(AMStrings.LoadingInvite);
			NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.LinkcardsMessage);
			try
			{
				LinkCardService linkcardService = new LinkCardService();
				LinkCardsRoot linkCardsRoot = await linkcardService.LinkCardsAction(accept, null);

				if (linkCardsRoot.Error[0].Code == "E00")
				{
					Console.WriteLine("accept invite "+accept);
					AMAlerts.DefaultAlert(this, "Geslaagd!", linkCardsRoot.Error[0].Message);
					UpdateBalance();

				}
                else if (linkCardsRoot.Error[0].Message.ToLower() == "ds")
                {
                    Console.WriteLine("dont show warning");
                }
                else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, linkCardsRoot.Error[0].Message);

				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Mijn menu", "Exception: "+ ex}
                });
				Console.WriteLine("exception linkcards "+ex);
				AMAlerts.DefaultAlert(this, "Technische storing", AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);

		}

		void Refresh()
		{
			UpdateBalance();
			//Google analytics
			var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Mijn", "Pull to refresh", "Pull to refresh", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
			Gai.SharedInstance.Dispatch();
		}

	}

}