﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("DonateViewController")]
    partial class DonateViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CharityAmountTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CharityTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DonateButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DonateExplenationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DonateTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel WarningLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CharityAmountTextfield != null) {
                CharityAmountTextfield.Dispose ();
                CharityAmountTextfield = null;
            }

            if (CharityTextfield != null) {
                CharityTextfield.Dispose ();
                CharityTextfield = null;
            }

            if (DonateButton != null) {
                DonateButton.Dispose ();
                DonateButton = null;
            }

            if (DonateExplenationLabel != null) {
                DonateExplenationLabel.Dispose ();
                DonateExplenationLabel = null;
            }

            if (DonateTitleLabel != null) {
                DonateTitleLabel.Dispose ();
                DonateTitleLabel = null;
            }

            if (WarningLabel != null) {
                WarningLabel.Dispose ();
                WarningLabel = null;
            }
        }
    }
}