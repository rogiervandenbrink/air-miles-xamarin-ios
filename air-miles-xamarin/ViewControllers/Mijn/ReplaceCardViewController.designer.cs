﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("ReplaceCardViewController")]
    partial class ReplaceCardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ReplacecardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ReplacecardExplenationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ReplacecardLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ReplacecardButton != null) {
                ReplacecardButton.Dispose ();
                ReplacecardButton = null;
            }

            if (ReplacecardExplenationLabel != null) {
                ReplacecardExplenationLabel.Dispose ();
                ReplacecardExplenationLabel = null;
            }

            if (ReplacecardLabel != null) {
                ReplacecardLabel.Dispose ();
                ReplacecardLabel = null;
            }
        }
    }
}