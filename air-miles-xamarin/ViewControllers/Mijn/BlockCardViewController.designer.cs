﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("BlockCardViewController")]
    partial class BlockCardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton BlockcardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BlockcardExplenationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BlockcardNumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PhysicalCardLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl SegmentedControl { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BlockcardButton != null) {
                BlockcardButton.Dispose ();
                BlockcardButton = null;
            }

            if (BlockcardExplenationLabel != null) {
                BlockcardExplenationLabel.Dispose ();
                BlockcardExplenationLabel = null;
            }

            if (BlockcardNumberLabel != null) {
                BlockcardNumberLabel.Dispose ();
                BlockcardNumberLabel = null;
            }

            if (PhysicalCardLabel != null) {
                PhysicalCardLabel.Dispose ();
                PhysicalCardLabel = null;
            }

            if (SegmentedControl != null) {
                SegmentedControl.Dispose ();
                SegmentedControl = null;
            }
        }
    }
}