﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class MijnNavigationViewController : UINavigationController
    {
		public string Routing { get; set; }

        public MijnNavigationViewController (IntPtr handle) : base (handle)
        {
        }
    }
}