﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("ValidityViewController")]
    partial class ValidityViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AirmilesLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView HeaderView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton IconInfo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ValidTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AirmilesLabel != null) {
                AirmilesLabel.Dispose ();
                AirmilesLabel = null;
            }

            if (HeaderView != null) {
                HeaderView.Dispose ();
                HeaderView = null;
            }

            if (IconInfo != null) {
                IconInfo.Dispose ();
                IconInfo = null;
            }

            if (ValidTitle != null) {
                ValidTitle.Dispose ();
                ValidTitle = null;
            }
        }
    }
}