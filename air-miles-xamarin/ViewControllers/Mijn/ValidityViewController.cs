﻿using Foundation;
using System;
using UIKit;
using LocalAuthentication;
using Google.Analytics;
using CoreGraphics;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Air_Miles_Xamarin;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class ValidityViewController : UITableViewController
    {

		// Network status
		NetworkStatus Internet;

		//refreshlayout
		UIRefreshControl RefreshControl;

		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;
		TransactiesOverlay TransactiesOverlay;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();


		LAContext Context = new LAContext();
		NSError Error;

        public ValidityViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			Validity();
			Title = "Geldigheid Air Miles";

			IconInfo.TouchUpInside += delegate
			{
				AMAlerts.DefaultAlert(this, AMStrings.TooltipValidityTitle, AMStrings.TooltipValidityText);
			};

			base.ViewDidLoad();
		}

		private async void Validity()
		{
			SetLoader(AMStrings.LoadingValidity);
			TableView.SetContentOffset(new CGPoint(0, 0), false);
			try
			{

			
				ValidityService ValidityService = new ValidityService();
				ValidityRoot Validity = await ValidityService.GetExperitaionAirMiles(cancelToken);

				if (Validity.Error[0].Code == "E00")
				{

					TableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems(Validity.Response));
					TableView.ReloadData();
				}
				else {

					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(Validity.Error[0].Message));

				}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Geldigheid", "Exception: "+ ex}
                });
				Console.WriteLine("exception validity "+ex);
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private TableSection[] GetTableItems(Validity[] validities)
		{
			// Define lists for the Sections and Rows
			Console.WriteLine("validities length "+validities.Length);
			List<TableSection> Sections = new List<TableSection>();
			List<TableRow>[] Rows = new List<TableRow>[validities.Length];

			// Section id starts with 0
			int SectionId = 0;

			for (int i = 0; i < validities.Length; i++)
			{
				if (i > 0)
				{
					if (AMMain.DateToTime(validities[i].ExpirationDatePerMonth).Year.ToString() != AMMain.DateToTime(validities[i - 1].ExpirationDatePerMonth).Year.ToString())
					{
						SectionId++;
					}
				}

				// if Section Rows don't exist, create them
				if (Rows[SectionId] == null)
				{
					Rows[SectionId] = new List<TableRow>();
				}

				Console.WriteLine("Title validity: "+AMMain.DateToTime(validities[i].ExpirationDatePerMonth).ToString("dd-MM-yyyy"));
				Console.WriteLine("Validity other date "+SharedDatetime.ConvertFromUnixTimestamp(validities[i].ExpirationDatePerMonth).ToString("D", AMMain.Cultureinfo));
				Console.WriteLine("Detail validity: vali"+validities[i].NumberOfAirmiles);
				// Add the Table Rows
				string detail = validities[i].NumberOfAirmiles;

				Rows[SectionId].Add(new TableRow()
				{
					Title = SharedDatetime.ConvertFromUnixTimestamp(validities[i].ExpirationDatePerMonth).ToString("d", AMMain.Cultureinfo),
					Detail = AMMain.SetBalanceFormat(detail),
					Hiddeninfo = AMMain.DateToTime(validities[i].ExpirationDatePerMonth).Year.ToString(),
					CellIdentifier = CellIdentifier.TitleAmountTableViewCell
				});
			}

			if (SectionId > 0)
			{
				for (int i = 0; i < SectionId + 1; i++)
				{
					Console.WriteLine("sections added" + Rows[i].Count);
					Sections.Add(new TableSection()
					{
						Title = Rows[i][0].Hiddeninfo,
						Items = Rows[i].ToArray()
					});
				}
			}
			else {
				if (Rows.Length > 0)
				{
					Sections.Add(new TableSection()
					{
						Title = Rows[0][0].Hiddeninfo,
						Items = Rows[0].ToArray()
					});
				}
			}
			// Return the Sections as Rows as an array
			return Sections.ToArray();
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

        public override void ViewDidAppear(bool animated)
        {
            // Analytics Screenname
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Geldigheid");
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
            base.ViewDidAppear(animated);
        }
	}
}