﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using System.Threading;
using Google.Analytics;
using Xamarin;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Air_Miles_Xamarin;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class SignoutViewController : UITableViewController
    {
        public SignoutViewController (IntPtr handle) : base (handle)
        {
        }

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		LoadingOverlay LoadingView;

		// Network status
		NetworkStatus Internet;
        public bool HasNoAirmiles { get; set; }

		public override void ViewDidLoad()
		{

			Initialize();

			SignoutButton.TouchUpInside += delegate
			{
				ShowPopupMEssage();
			};

			TransferSignoutButton.TouchUpInside += delegate {

				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Uitschrijven", "Overige Air Miles overboeken", null, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				TransferViewController Redirect = App.Storyboard.InstantiateViewController("TransferViewController") as TransferViewController;
				NavigationController.PushViewController(Redirect, false);
				
			};
			DonateSignoutButton.TouchUpInside += delegate
			{
				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Uitschrijven", "Overige Air Miles doneren", null, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				DonateViewController Redirect = App.Storyboard.InstantiateViewController("DonateViewController") as DonateViewController;
				NavigationController.PushViewController(Redirect, false);

			};

			base.ViewDidLoad();

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Uitschrijven");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			//UpdateBalance();
			string balance = "";
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance)))
			{
				balance = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance);
                HasNoAirmiles = false;
                TransferSignoutButton.Hidden = false;
                DonateSignoutButton.Hidden = false;
                ConditionsSignoutLabel.Hidden = false;

                if(balance == "0"){
                    HasNoAirmiles = true;
                    TransferSignoutButton.Hidden = true;
                    DonateSignoutButton.Hidden = true;
                    ConditionsSignoutLabel.Hidden = true;
                }
                TableView.ReloadData();
			}
			else
			{
				balance = "0";
                HasNoAirmiles = true;
				TransferSignoutButton.Hidden = true;
				DonateSignoutButton.Hidden = true;
				ConditionsSignoutLabel.Hidden = true;
                TableView.ReloadData();
			}

			WarningLabel.Text = "Let op! Je hebt nog " + AMMain.SetBalanceFormat(balance) + " Air Miles";
		}

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            nfloat height = 45;

            if(indexPath.Row == 0){
                height = 160;
            }
            if (indexPath.Row == 7)
            {
                height = 80;
            }
            if (!HasNoAirmiles)
            {
                if (indexPath.Row > 3 && indexPath.Row < 7)
                {
                    height = 60;

                }
            }else{
                if (indexPath.Row == 1)
                {
                    height = 0;

                }
                if (indexPath.Row > 3 && indexPath.Row < 7){
                    height = 0;
                }
            }
			
            return height;
        }

		public void Initialize()
		{
			AMStyle.RoundedButton(SignoutButton);

			SignoutExplenationLabel.Font = UIFont.FromName("avenir-book", 16f);
            SignoutExplenationLabel.Text = AMStrings.SignoutExplenation;
            SignoutExplenationLabel.SizeToFit();

            var atts = new UIStringAttributes();
            atts.ForegroundColor = UIColor.White;
            atts.Font = UIFont.FromName("avenir-book", 16f);
            var secondAttributes = new UIStringAttributes
            {
                UnderlineStyle = NSUnderlineStyle.Single,
                Font = UIFont.FromName("avenir-book", 16f)
            };
            var txt = "Meer info over uitschrijven? Klik hier.";
            var attributedString = new NSMutableAttributedString(txt, atts);
            attributedString.SetAttributes(secondAttributes.Dictionary, new NSRange(attributedString.Length - 10, 9));
            PrivacyLabel.AttributedText = attributedString;

            UITapGestureRecognizer labelTap = new UITapGestureRecognizer(() => {
                // open privacy statement
                AlgemeneVoorwaardenURL();
            });

            PrivacyLabel.UserInteractionEnabled = true;
            PrivacyLabel.AddGestureRecognizer(labelTap);

            AMStyle.BorderedGreyLabel(CardLabel);
            CardLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
            CardLabel.Font = UIFont.FromName("avenir-book", 17f);

			AMStyle.BorderedButtonBlue(TransferSignoutButton);
            TransferSignoutButton.Font = UIFont.FromName("Avenir-Black", 11f);
			AMStyle.BorderedButtonBlue(DonateSignoutButton);
            DonateSignoutButton.Font = UIFont.FromName("Avenir-Black", 11f);
			ConditionsSignoutLabel.Font = UIFont.FromName("avenir-book", 11f);
			ConditionsSignoutLabel.TextColor = UIColor.FromRGB(2200, 200, 200);
			string balance = "";
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance)))
			{
				balance = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance);
			}
			else
			{
				balance = "0";
			}

			WarningLabel.Text = "Let op! Je hebt nog " + AMMain.SetBalanceFormat(balance) + " Air Miles";

		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
            if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard))
            {
                RemoveFromParentViewController();
            }
			// cancel the webservice via tok
			//if (cancelToken.Token.CanBeCanceled)
			//	cancelToken.Cancel();

		}

		private async void Signout()
		{
			SetLoader(AMStrings.LoadingSignout);
			TableView.SetContentOffset(new CGPoint(0, 0), false);

			// Analytics event
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Uitschrijven", "Uitschrijving aanvragen", "Uitschrijving aanvragen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
			try
			{

			SignoutService SignoutService = new SignoutService();
			SignoutRoot Signout = await SignoutService.Signout(cancelToken);

			if (Signout.Error[0].Code == "E00")
			{
				LocalStorage.Logout(this.TabBarController);
				LocalStorage.RemoveCard();
                AskSignout(AMMain.RemoveHTMLTags(Signout.Response.PopupMessage));

			}
			else {

				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(Signout.Error[0].Message));

			}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Uitschrijven", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private void AskSignout(string message)
		{
			
			UIAlertView alert = new UIAlertView();
			alert.Title = "Uitschrijving geslaagd";
			alert.AddButton("OK");
			alert.Message = message;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonClicked;
			alert.Show();
		}


		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
                if (this.TabBarController != null)
                {
                    this.TabBarController.SelectedIndex = 0;
                }
			}
			else
			{
				// Cancel button

			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private async void UpdateBalance()
		{

			try
			{
				Internet = Reachability.InternetConnectionStatus();

				// If internet
				if (Internet != NetworkStatus.NotReachable)
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

					if (CurrentKaart.Error[0].Code == "E00")
					{
						Console.WriteLine("Updating Card data");
						Console.WriteLine("response balance " + JsonConvert.SerializeObject(CurrentKaart));
						string service = AMLocalStorage.KaartService;
						NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
						LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
						NSUserDefaults.StandardUserDefaults.Synchronize();

						WarningLabel.Text = "Let op! Je hebt nog " + AMMain.SetBalanceFormat(CurrentKaart.Kaart.Balance.ToString()) + " Air Miles";

					}
				}
				else
				{
					//SetNetworkView();
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Uitschrijven", "Exception: "+ ex}
                });

				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			TableView.ReloadData();
		}

        private void AlgemeneVoorwaardenURL()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLPrivacyCookieBeleid));
        }

        private void ShowPopupMEssage()
        {

            UIAlertView alert = new UIAlertView();
            alert.Title = "Uitschrijving bevestigen";
            alert.Message = AMStrings.SignoutMessage;
            alert.AddButton("Uitschrijven");
            alert.AddButton("Annuleren");
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonPopUpClicked;
            alert.Show();
        }

        void ButtonPopUpClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                // OK button
                Signout();
            }
            else
            {
                // Cancel button
            }
        }
    }
}