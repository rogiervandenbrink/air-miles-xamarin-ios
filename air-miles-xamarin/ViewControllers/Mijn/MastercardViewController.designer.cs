﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("MastercardViewController")]
    partial class MastercardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardNumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton MastercardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MastercardExplenationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField MastercardnumberTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MastercardTitleLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CardNumberLabel != null) {
                CardNumberLabel.Dispose ();
                CardNumberLabel = null;
            }

            if (MastercardButton != null) {
                MastercardButton.Dispose ();
                MastercardButton = null;
            }

            if (MastercardExplenationLabel != null) {
                MastercardExplenationLabel.Dispose ();
                MastercardExplenationLabel = null;
            }

            if (MastercardnumberTextfield != null) {
                MastercardnumberTextfield.Dispose ();
                MastercardnumberTextfield = null;
            }

            if (MastercardTitleLabel != null) {
                MastercardTitleLabel.Dispose ();
                MastercardTitleLabel = null;
            }
        }
    }
}