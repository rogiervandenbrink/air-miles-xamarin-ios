﻿using Foundation;
using System;
using UIKit;
using System.Threading;
using CoreGraphics;
using Newtonsoft.Json;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class InviteLinkCardViewController : UITableViewController
    {

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();
		LoadingOverlay LoadingView;

		public bool NotLinked { get; set; }

        public InviteLinkCardViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			Initialize();

			NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
			UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
			{
				NavigationController.PopViewController(true);
			}), true);

			InviteButton.TouchUpInside += delegate {
				if (CardnumberTextfield.Text.Length == 9)
				{
					InviteLinkCards();

				}
				else {
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Ongeldig kaartnummer.");
				}
				CardnumberTextfield.ResignFirstResponder();
			};

            //this.NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(37, 102, 177);

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Koppelen uitnodigen");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			base.ViewDidLoad();
		}

		public override void ViewWillAppear(bool animated)
		{
			Console.WriteLine("bool NotLinked "+NotLinked);
			base.ViewWillAppear(animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			Console.WriteLine("notlinked = "+NotLinked+" navigationcontroller "+NavigationController);
            if (NotLinked && NavigationController != null)
			{
				NavigationController.PopViewController(false);
				NotLinked = false;
			}
			base.ViewWillDisappear(animated);
		}


		public void Initialize()
		{
			AMStyle.RoundedButton(InviteButton);
			AMStyle.BorderedTextfield(CardnumberTextfield);
			CardnumberTextfield.KeyboardType = UIKeyboardType.NumberPad;
			CardnumberTextfield.Font = UIFont.FromName("Avenir-Black", 12f);
			AMMain.AddDoneButton(CardnumberTextfield);
			InviteLinkcardTitleLabel.TextColor = UIColor.White;
			InviteLinkcardTitleLabel.AdjustsFontSizeToFitWidth = true;
			LinkcardTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
		}

		async void InviteLinkCards()
		{
			SetLoader(AMStrings.LoadingInviteLinkCards);

			// Analytics event
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen uitnodigen", "Uitnodiging versturen", "Uitnodiging versturen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
			try
			{
				LinkCardService linkcardService = new LinkCardService();
				LinkCardsRoot linkCardsRoot = await linkcardService.LinkCards(CardnumberTextfield.Text, false, cancelToken);

				if (linkCardsRoot.Error[0].Code == "E00")
				{
					AlertInviteLinkcards(AMMain.RemoveHTMLTags(linkCardsRoot.Error[0].Message));
					NotLinked = false;
					CardnumberTextfield.Text = string.Empty;

				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(linkCardsRoot.Error[0].Message));

				}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart kopppelen: uitnodigen", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, "Technische storing", AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void AlertInviteLinkcards(String message)
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = "Koppelen";
			alert.AddButton("OK");
			alert.Message = message;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonClickedIt;
			alert.Show();
		}

		void ButtonClickedIt(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				if (NavigationController != null)
				{
					NavigationController.PopViewController(false);
				}
			}
			else
			{

				// Cancel button

			}
		}
    }
}