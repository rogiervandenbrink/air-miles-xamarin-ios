﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CoreGraphics;
using Foundation;
using Google.Analytics;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using UIKit;
using Xamarin;

namespace airmilesxamarin
{
    public partial class KaartKoppelenViewController : UIViewController
    {
        
		private UIPageViewController pageViewController;
		private List<string> _pageTitles;
		private List<string> _images;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();
		LoadingOverlay LoadingView;


		public KaartKoppelenViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = "Gekoppeld";
            //this.NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(3, 124, 194);
			//_pageTitles = new List<string> { "Explore", "Widget", "Navigation", "Random" };
			//_images = new List<string> { "Airmiles_Placeholder@3x", "Airmiles_Placeholder@2x", "Airmiles_Placeholder", "Airmiles_Placeholder@3x" };
			AMStyle.BorderedBlueButton(StopLickCardsButton);
			AMStyle.RoundedButton(InviteLinkcardButton);

			StopLickCardsButton.TouchUpInside += delegate {
				//QuitLinkCards();	
				QuestionQuitLinkcards();
			};

			/*this.NavigationItem.SetRightBarButtonItem(
					new UIBarButtonItem(UIBarButtonSystemItem.Add, (sender, args) =>
				{
					// button was clicked
					InviteLinkCardViewController Redirect = App.Storyboard.InstantiateViewController("InviteLinkCardViewController") as InviteLinkCardViewController;
					//Redirect.NotLinked = true;
					NavigationController.PushViewController(Redirect, false);

				})
				, true);*/

			InviteLinkcardButton.TouchUpInside += delegate {
				// button was clicked
					InviteLinkCardViewController Redirect = App.Storyboard.InstantiateViewController("InviteLinkCardViewController") as InviteLinkCardViewController;
				//Redirect.NotLinked = true;
				NavigationController.PushViewController(Redirect, false);
			};
		}

		public override void ViewWillAppear(bool animated)
		{
			SetLoader(AMStrings.LoadingLinkCards);
			GetLinkCardsInfo();
			base.ViewWillAppear(animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
			base.ViewWillDisappear(animated);
		}

		async void GetLinkCardsInfo()
		{

			try
			{
				LinkCardService linkcardService = new LinkCardService();
				LinkCardsRoot linkCardsRoot = await linkcardService.LinkCardsInfo(cancelToken);

				if (linkCardsRoot.Error[0].Code == "E00")
				{
					Console.WriteLine("ResponseInfo "+JsonConvert.SerializeObject(linkCardsRoot.ResponseInfo));
					Console.WriteLine("ResponseInfo length " + linkCardsRoot.ResponseInfo.Length);
					if (linkCardsRoot.ResponseInfo.Length < 1)
					{
						InviteLinkCardViewController Redirect = App.Storyboard.InstantiateViewController("InviteLinkCardViewController") as InviteLinkCardViewController;
						Redirect.NotLinked = true;
						NavigationController.PushViewController(Redirect, false);
					}
					else {
						InviteLinkCardViewController Redirect = App.Storyboard.InstantiateViewController("InviteLinkCardViewController") as InviteLinkCardViewController;
						Redirect.NotLinked = false;


						_pageTitles = new List<string>();
						_images = new List<string>();

						for (int i = 0; i < linkCardsRoot.ResponseInfo.Length; i++)
						{
							_pageTitles.Add(linkCardsRoot.ResponseInfo[i].FullName);
							_images.Add(linkCardsRoot.ResponseInfo[i].Picture);
							Console.WriteLine("firstname: " + linkCardsRoot.ResponseInfo[i].FullName);

						}
						if (LoadingView != null)
						{
							LoadingView.Hide();
						}
						startViewControllers();
					}
				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(linkCardsRoot.Error[0].Message));

				}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart koppelen", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Koppelen spaarders overzicht");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public void startViewControllers()
		{
			var startVC = this.ViewControllerAtIndex(0) as ContentViewController;
			var viewControllers = new UIViewController[] { startVC };

			pageViewController = this.Storyboard.InstantiateViewController("PageViewController") as UIPageViewController;
			pageViewController.DataSource = new PageViewControllerDataSource(this, _pageTitles);


			pageViewController.SetViewControllers(viewControllers, UIPageViewControllerNavigationDirection.Forward, false, null);
			pageViewController.View.Frame = new CGRect(0, 0, this.View.Frame.Width, this.View.Frame.Size.Height - 160);
			AddChildViewController(this.pageViewController);
			View.AddSubview(this.pageViewController.View);
			pageViewController.DidMoveToParentViewController(this);

		}

		public UIViewController ViewControllerAtIndex(int index)
		{
			var vc = this.Storyboard.InstantiateViewController("ContentViewController") as ContentViewController;
			vc.titleText = _pageTitles.ElementAt(index);
			vc.imageFile = _images.ElementAt(index);
			vc.pageIndex = index;
			return vc;
		}

		private class PageViewControllerDataSource : UIPageViewControllerDataSource
		{
			private KaartKoppelenViewController _parentViewController;
			private List<string> _pageTitles;

			public PageViewControllerDataSource(UIViewController parentViewController, List<string> pageTitles)
			{
				_parentViewController = parentViewController as KaartKoppelenViewController;
				_pageTitles = pageTitles;
			}

			public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
			{
				var vc = referenceViewController as ContentViewController;
				var index = vc.pageIndex;
				if (index == 0)
				{
					return null;
				}
				else {
					index--;
					return _parentViewController.ViewControllerAtIndex(index);
				}
			}

			public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
			{
				var vc = referenceViewController as ContentViewController;
				var index = vc.pageIndex;

				index++;
				if (index == _pageTitles.Count)
				{
					return null;
				}
				else {
					return _parentViewController.ViewControllerAtIndex(index);
				}
			}

			public override nint GetPresentationCount(UIPageViewController pageViewController)
			{
				return _pageTitles.Count;
			}

			public override nint GetPresentationIndex(UIPageViewController pageViewController)
			{
				return 0;
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		async void QuitLinkCards()
		{
			SetLoader(AMStrings.LoadingQuitLinkCards);

			try
			{
				LinkCardService linkcardService = new LinkCardService();
				LinkCardsRoot linkCardsRoot = await linkcardService.LinkCards(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), true, cancelToken);

				if (linkCardsRoot.Error[0].Code == "E00")
				{
					AlertQuitLinkcards(AMMain.RemoveHTMLTags(linkCardsRoot.Error[0].Message));

				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(linkCardsRoot.Error[0].Message));

				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart koppelen", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private void QuestionQuitLinkcards()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.QuitLinkcardsTitle;
			alert.AddButton("Stoppen");
			alert.AddButton("Annuleren");
			alert.Message = AMStrings.QuitLinkcardsMessage;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonClicked;
			alert.Show();
		}

		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				Console.WriteLine("navigationcontroller quit linkcards dialog " + sender);
				// OK button
				QuitLinkCards();
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Kaartkoppelen", "Stoppen", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else
			{

				// Cancel button

			}
		}

		private void AlertQuitLinkcards(String message)
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.QuitLinkcardsTitle;
			alert.AddButton("OK");
			alert.Message = message;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonClickedIt;
			alert.Show();
		}

		void ButtonClickedIt(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				if (NavigationController != null)
				{
					NavigationController.PopViewController(false);
				}
			}
			else
			{

				// Cancel button

			}
		}
	}
}
