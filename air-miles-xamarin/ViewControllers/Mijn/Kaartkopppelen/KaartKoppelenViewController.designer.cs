﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("KaartKoppelenViewController")]
    partial class KaartKoppelenViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton InviteLinkcardButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton StopLickCardsButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (InviteLinkcardButton != null) {
                InviteLinkcardButton.Dispose ();
                InviteLinkcardButton = null;
            }

            if (StopLickCardsButton != null) {
                StopLickCardsButton.Dispose ();
                StopLickCardsButton = null;
            }
        }
    }
}