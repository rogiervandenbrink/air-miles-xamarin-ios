﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using Google.Analytics;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
    public partial class LinkCardOverviewViewController : UITableViewController
    {
        public LinkCardOverviewViewController (IntPtr handle) : base (handle)
        {
        }

		UIRefreshControl RefreshControl;

		public override void ViewDidLoad()
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				TableView.AddSubview(RefreshControl);
			}
            TableView.TableFooterView = new UIView();
			base.ViewDidLoad();
            //this.NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(3, 124, 194);
		}

		public override void ViewWillAppear(bool animated)
		{
			TableView.Source = new DefaultSectionHeaderTableViewSource(this, LinkCardTableItems.Overview());
			base.ViewWillAppear(animated);

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Koppelen menu");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{
			if (segueIdentifier == "SamenSparenSegue")
			{
				SamenSparenSegue();

				return false;
			}
			if (segueIdentifier == "MastercardSegue")
			{
				MastercardSegue();

				return false;
			}
			if (segueIdentifier == "AHSegue")
			{
				AHSegue();

				return false;
			}
			if (segueIdentifier == "EtosSegue")
			{
				EtosSegue();

				return false;
			}
			if (segueIdentifier == "PraxisSegue")
			{
				PraxisSegue();

				return false;
			}

			this.PerformSegue(segueIdentifier, sender);
			return true;
		}

		void Refresh()
		{
			//do refresh
			RefreshControl.EndRefreshing();
		}

		void SamenSparenSegue()
		{
			//UIApplication.SharedApplication.OpenUrl(new NSUrl(AMClient.URLSamenSparen + "?utm_campaign=meer&utm_medium=ios&utm_source=app"));
			// Analytics event
			//var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen menu", "Open website", "Spaarders", null).Build();
			//Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			//Gai.SharedInstance.Dispatch();
			KaartKoppelenViewController Redirect = App.Storyboard.InstantiateViewController("KaartKoppelenViewController") as KaartKoppelenViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		void MastercardSegue()
		{
			MastercardViewController Redirect = App.Storyboard.InstantiateViewController("MastercardViewController") as MastercardViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		void AHSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLAHLinkCard));
            //AMMain.OpenURLInApp(this, AMClient.URLAHLinkCard);

			// Analytics event
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen menu", "Open website", "AH", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		void EtosSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLEtosLinkCard));
			//AMMain.OpenURLInApp(this, AMClient.URLEtosLinkCard);

			// Analytics event
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen menu", "Open website", "Etos", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		void PraxisSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLPraxisLinkCard));
			//AMMain.OpenURLInApp(this, AMClient.URLPraxisLinkCard);

			// Analytics event
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen menu", "Open website", "Praxis", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}
    }
}