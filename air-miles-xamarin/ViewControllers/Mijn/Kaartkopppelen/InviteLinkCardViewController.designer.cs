﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("InviteLinkCardViewController")]
    partial class InviteLinkCardViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CardnumberTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton InviteButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel InviteLinkcardTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LinkcardTextLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CardnumberTextfield != null) {
                CardnumberTextfield.Dispose ();
                CardnumberTextfield = null;
            }

            if (InviteButton != null) {
                InviteButton.Dispose ();
                InviteButton = null;
            }

            if (InviteLinkcardTitleLabel != null) {
                InviteLinkcardTitleLabel.Dispose ();
                InviteLinkcardTitleLabel = null;
            }

            if (LinkcardTextLabel != null) {
                LinkcardTextLabel.Dispose ();
                LinkcardTextLabel = null;
            }
        }
    }
}