﻿using Foundation;
using System;
using UIKit;
using System.IO;
using CoreGraphics;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class AvatarPickerViewController : UIViewController
    {
        bool firstOpened;
        protected UIImagePickerController imagePicker;         protected PickerDelegate pickerDelegate;
        LoadingOverlay LoadingView;
        public static bool ShowConfirmButton;

        public AvatarPickerViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            Console.WriteLine("avatarpickervc didload");
            base.ViewDidLoad();
            Initialize();

            NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
            UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
            {
                NavigationController.PopViewController(true);

            }), true);

            PickAvatarButton.TouchUpInside += delegate
            {
                if (TempAvatarFile.ChosenAvatar != null)
                {
                    UploadAvatar(CenterCrop(TempAvatarFile.ChosenAvatar));
                }
            };
            GalleryButton.TouchUpInside += (s, e) =>
            {
                // create a new picker controller
                imagePicker = new UIImagePickerController();

                // set our source to the photo library
                imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;

                // set what media types
                imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);

                // As with most controls, you can either handle the events directly, or
                // wire up a delegate. in this first one, i wire up the events. in the other two,
                // i use a delegate
                imagePicker.FinishedPickingMedia += Handle_imagePickerhandleFinishedPickingMedia;
                imagePicker.Canceled += (sender, evt) =>
                {
                    Console.WriteLine("picker cancelled");
                    // HACK: NavigationController.DismissModalViewControllerAnimated to NavigationController.DismissModalViewController
                    imagePicker.DismissModalViewController(true);
                };

                // Google analaytics
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Profielafbeelding", "Afbeelding verkrijgen", "Galerij", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();

                // show the picker
                NavigationController.PresentModalViewController(imagePicker, true);
            };

            CameraButton.TouchUpInside += (s, e) =>
            {

                try
                {
                    // create a new picker controller
                    imagePicker = new UIImagePickerController();

                    // set our source to the camera
                    imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;

                    // set what media types
                    imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.Camera);

                    // show the camera controls
                    imagePicker.ShowsCameraControls = true;

                    // attach the delegate
                    pickerDelegate = new AvatarPickerViewController.PickerDelegate();
                    imagePicker.Delegate = pickerDelegate;

                    // Google analaytics
                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Profielafbeelding", "Afbeelding verkrijgen", "Camera", null).Build();
                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                    Gai.SharedInstance.Dispatch();

                    // show the picker
                    NavigationController.PresentModalViewController(imagePicker, true);

                }
                catch
                {
                    UIAlertView alert = new UIAlertView("No Camera", "No Camera Detected!", null, "OK", null);
                    alert.Show();
                }
            };

        }

        public override void ViewDidDisappear(bool animated)
        {
            TempAvatarFile.ChosenAvatar = null;
            ShowConfirmButton = false;
            base.ViewDidDisappear(animated);
        }

        private void Initialize()
        {
            // Analytics Screenname
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Profielafbeelding");
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

            Title = "Profielfoto aanpassen";

            CircleBackgroundImage.Image = CircleBackgroundImage.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
            CircleBackgroundImage.TintColor = UIColor.FromRGB(1, 132, 220);

            AMStyle.BorderedBlueButton(GalleryButton);
            AMStyle.BorderedBlueButton(CameraButton);
            AMStyle.RoundedButton(PickAvatarButton);
            PickAvatarButton.Hidden = true;
            AvatarPickerImage.Layer.CornerRadius = AvatarPickerImage.Bounds.Height / 2;
            AvatarPickerImage.Layer.MasksToBounds = true;

            if (!firstOpened)
            {
                SetAvatar();
                firstOpened = true;
            }
        }

        private void SetAvatar()
        {
            if (TempAvatarFile.ChosenAvatar==null)
            {
                AMMain.SetAvatarAvatarPicker(AvatarPickerImage, "Airmiles_Placeholder");
            } else
            {

                AvatarPickerImage.Image = CenterCrop(TempAvatarFile.ChosenAvatar);

            }


        }

        public override void ViewWillAppear(bool animated)
        {
            SetAvatar();
            if (ShowConfirmButton)
            {
                PickAvatarButton.Hidden = false;
            }
            Console.WriteLine("avatarpickervc will apear, setavatar");
            base.ViewWillAppear(animated);
        }


        protected void Handle_imagePickerhandleFinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs e)
        {
            // determine what was selected, video or image
            bool isImage = false;
            switch (e.Info[UIImagePickerController.MediaType].ToString())
            {
                case "public.image":
                    Console.WriteLine("Image selected");
                    isImage = true;
                    Console.WriteLine("Video selected");
                    break;
                case "public.video":
                    break;
            }

            Console.Write("Reference URL: [" + UIImagePickerController.ReferenceUrl + "]");

            // get common info (shared between images and video)
            //NSUrl referenceURL = info[UIImagePickerController.ReferenceUrl] as NSUrl;
            NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;
            if (referenceURL != null)
                Console.WriteLine(referenceURL.ToString());

            // if it was an image, get the other image info
            if (isImage)
            {

                // get the original image
                UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
                if (originalImage != null)
                {
                    // do something with the image
                    TempAvatarFile.ChosenAvatar = originalImage;
                    Console.WriteLine("reference url " + referenceURL);
                    ShowConfirmButton = true;
                    //SetAvatarImage(referenceURL);;
                    Console.WriteLine("got the original image");
                }

                // get the edited image
                UIImage editedImage = e.Info[UIImagePickerController.EditedImage] as UIImage;
                if (editedImage != null)
                {
                    // do something with the image
                    TempAvatarFile.ChosenAvatar = originalImage;
                    ShowConfirmButton = true;
                    Console.WriteLine("reference url " + referenceURL);
                    //SetAvatarImage(referenceURL);
                    Console.WriteLine("got the edited image");
                }

                //- get the image metadata
                NSDictionary imageMetadata = e.Info[UIImagePickerController.MediaMetadata] as NSDictionary;
                if (imageMetadata != null)
                {
                    // do something with the metadata
                    Console.WriteLine("got image metadata");
                }

            }
            // if it's a video
            else {
                // get video url
                NSUrl mediaURL = e.Info[UIImagePickerController.MediaURL] as NSUrl;
                if (mediaURL != null)
                {
                    //
                    Console.WriteLine(mediaURL.ToString());
                }
            }

            // dismiss the picker
            // HACK: NavigationController.DismissModalViewControllerAnimated to NavigationController.DismissModalViewController
            imagePicker.DismissModalViewController(true);
        }


        // Our custom picker delegate. The events haven't been exposed so we have to use a
        // delegate.
        public class PickerDelegate : UIImagePickerControllerDelegate
        {
            public override void Canceled(UIImagePickerController picker)
            {
                Console.WriteLine("picker cancelled");
                // HACK: NavigationController.DismissModalViewControllerAnimated to NavigationController.DismissModalViewController
                picker.DismissModalViewController(true);
            }

            public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
            {
                // determine what was selected, video or image
                bool isImage = false;
                switch (info[UIImagePickerController.MediaType].ToString())
                {
                    case "public.image":
                        Console.WriteLine("Image selected");
                        isImage = true;
                        Console.WriteLine("Video selected");
                        break;
                    case "public.video":
                        break;
                }

                Console.Write("Reference URL: [" + UIImagePickerController.ReferenceUrl + "]");

                // get common info (shared between images and video)
                //NSUrl referenceURL = info[UIImagePickerController.ReferenceUrl] as NSUrl;
                NSUrl referenceURL = info[new NSString("UIImagePickerControllerReferenceUrl")] as NSUrl;
                if (referenceURL != null)
                {
                    //
                    Console.WriteLine(referenceURL.ToString());
                }

                // if it was an image, get the other image info
                if (isImage)
                {
                    // get the original image
                    UIImage originalImage = info[UIImagePickerController.OriginalImage] as UIImage;
                    if (originalImage != null)
                    {
                        // do something with the image
                        Console.WriteLine("reference url "+referenceURL);
                        TempAvatarFile.ChosenAvatar = originalImage;
                        ShowConfirmButton = true;
                        Console.WriteLine("got the original image");

                    }

                    // get the edited image
                    UIImage editedImage = info[UIImagePickerController.EditedImage] as UIImage;
                    if (editedImage != null)
                    {
                        //AMMain.AvatarImageToBase64(referenceURL);

                        // do something with the image
                        Console.WriteLine("reference url " + referenceURL);
                        TempAvatarFile.ChosenAvatar = originalImage;;
                        ShowConfirmButton = true;
                        Console.WriteLine("got the edited image");

                    }

                    //- get the image metadata
                    NSDictionary imageMetadata = info[UIImagePickerController.MediaMetadata] as NSDictionary;
                    if (imageMetadata != null)
                    {
                        // do something with the metadata
                        Console.WriteLine("got image metadata");
                    }

                }
                // if it's a video
                else {
                    // get video url
                    NSUrl mediaURL = info[UIImagePickerController.MediaURL] as NSUrl;
                    if (mediaURL != null)
                    {
                        //
                        Console.WriteLine(mediaURL.ToString());
                    }

                }

                // dismiss the picker
                // HACK: NavigationController.DismissModalViewControllerAnimated to NavigationController.DismissModalViewController
                picker.DismissModalViewController(true);
            }
        }

        public async void UploadAvatar(UIImage image)
        {
            SetLoader(AMStrings.LoadingAvatarPhoto);
            NSData imageData = image.AsJPEG(0.5f);

            string encodedImage = imageData.GetBase64EncodedData(NSDataBase64EncodingOptions.None).ToString();
            try
            {
            AvatarService AvatarService = new AvatarService();
            AvatarRoot Avatar = await AvatarService.SaveAvatar(encodedImage, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + image);

            if (Avatar.Error[0].Code!=null && Avatar.Error[0].Code == "E00")
            {
                // Google analaytics
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Profielafbeelding", "Opslaan", "Opslaan", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();

                NSUserDefaults.StandardUserDefaults.SetString(encodedImage, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar);
                HideAvatarPicker(Avatar.Error[0].Message);
                
                TempAvatarFile.DatastringAvatar = null;
                    LoadingView.Hide();

            }
                else
                {
                    AMAlerts.DefaultAlert(this, "Profielfoto", Avatar.Error[0].Message);

                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Avatar kiezen", "Exception: "+ ex}
                });
                Console.WriteLine("exception upload avatar "+ex);
                LoadingView.Hide();
            }
            LoadingView.Hide();

        }

        void HideAvatarPicker(string Message)
        {
            UIAlertController AlertController = UIAlertController.Create(Title, Message, UIAlertControllerStyle.Alert);

            //Add Action
            AlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default,HandleAction));

            // Present Alert
            PresentViewController(AlertController, true, null);
            
        }

        void HandleAction (UIAlertAction obj)
        {
            NavigationController.PopViewController(true);
        }

        public static UIImage CenterCrop(UIImage originalImage)
        {
            
            double squareLength = Math.Min(originalImage.Size.Width, originalImage.Size.Height);

            nfloat x, y;
            x = (nfloat)((originalImage.Size.Width - squareLength) / 2.0);
            y = (nfloat)((originalImage.Size.Height - squareLength) / 2.0);

            //This Rect defines the coordinates to be used for the crop
            CGRect croppedRect = CGRect.FromLTRB(x, y, x + (nfloat)squareLength, y + (nfloat)squareLength);

            // Center-Crop the image
            UIGraphics.BeginImageContextWithOptions(croppedRect.Size, false, originalImage.CurrentScale);
            originalImage.Draw(new CGPoint(-croppedRect.X, -croppedRect.Y));
            UIImage croppedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return croppedImage;
        }

        void SetLoader(string text)
        {
            CGRect bounds = UIScreen.MainScreen.Bounds;
            //CGRect overlay = new CGRect(0, NavigationController.NavigationBar.Bounds.Height+UIApplication.SharedApplication.StatusBarFrame.Height + ButtonTabView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - (NavigationController.NavigationBar.Bounds.Height + ButtonTabView.Bounds.Height+ UIApplication.SharedApplication.StatusBarFrame.Height));
            LoadingView = new LoadingOverlay(bounds, text);
            View.Add(LoadingView);
        }


        public class TempAvatarFile
        {
            public static string DatastringAvatar { get; set; }
            public static UIImage ChosenAvatar { get; set;}
        }
    }
}