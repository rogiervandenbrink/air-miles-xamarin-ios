﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("ProfileViewController")]
    partial class ProfileViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AvatarButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BirthdateCommentLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView BirthdateIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell BirthdateTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField BirthdateTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BirthdateTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardnumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell CardnumberTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView CityIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell CityTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CityTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CityTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView CountryIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell CountryTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CountryTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CountryTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView EmailIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell EmailTableviewcell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmailTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EmailTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell FirstnameCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView FirstnameIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField FirstnameTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel FirstnameTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView HousenrIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell HousenrsupplementTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField HousenrsupplementTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel HousenrsupplementTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView HousenumberIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell HousenumberTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField HousenumberTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel HousenumberTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView InitialsIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell InitialsTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField InitialsTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel InitialsTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView LastnameIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell LastnameTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField LastnameTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LastnameTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView MobileIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell MobileTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField MobileTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MobileTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView NewsletterIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NewsletterLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch NewsletterSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell NewsletterTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView PhoneIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell PhoneTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PhoneTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PhoneTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView PostalcodeIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell PostalcodeTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PostalcodeTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PostalTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView PrefixIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell PrefixTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PrefixTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PrefixTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ProfileAvatar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ProfilingIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ProfilingLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch ProfilingSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell ProfilingTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView SalutationIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell SalutationTableViewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField SalutationTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel SalutationTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView StreetIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableViewCell StreetTableviewCell { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField StreetTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel StreetTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Title { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AvatarButton != null) {
                AvatarButton.Dispose ();
                AvatarButton = null;
            }

            if (BirthdateCommentLabel != null) {
                BirthdateCommentLabel.Dispose ();
                BirthdateCommentLabel = null;
            }

            if (BirthdateIcon != null) {
                BirthdateIcon.Dispose ();
                BirthdateIcon = null;
            }

            if (BirthdateTableviewCell != null) {
                BirthdateTableviewCell.Dispose ();
                BirthdateTableviewCell = null;
            }

            if (BirthdateTextfield != null) {
                BirthdateTextfield.Dispose ();
                BirthdateTextfield = null;
            }

            if (BirthdateTitle != null) {
                BirthdateTitle.Dispose ();
                BirthdateTitle = null;
            }

            if (CardnumberLabel != null) {
                CardnumberLabel.Dispose ();
                CardnumberLabel = null;
            }

            if (CardnumberTableviewCell != null) {
                CardnumberTableviewCell.Dispose ();
                CardnumberTableviewCell = null;
            }

            if (CityIcon != null) {
                CityIcon.Dispose ();
                CityIcon = null;
            }

            if (CityTableviewCell != null) {
                CityTableviewCell.Dispose ();
                CityTableviewCell = null;
            }

            if (CityTextfield != null) {
                CityTextfield.Dispose ();
                CityTextfield = null;
            }

            if (CityTitle != null) {
                CityTitle.Dispose ();
                CityTitle = null;
            }

            if (CountryIcon != null) {
                CountryIcon.Dispose ();
                CountryIcon = null;
            }

            if (CountryTableviewCell != null) {
                CountryTableviewCell.Dispose ();
                CountryTableviewCell = null;
            }

            if (CountryTextfield != null) {
                CountryTextfield.Dispose ();
                CountryTextfield = null;
            }

            if (CountryTitle != null) {
                CountryTitle.Dispose ();
                CountryTitle = null;
            }

            if (EmailIcon != null) {
                EmailIcon.Dispose ();
                EmailIcon = null;
            }

            if (EmailTableviewcell != null) {
                EmailTableviewcell.Dispose ();
                EmailTableviewcell = null;
            }

            if (EmailTextfield != null) {
                EmailTextfield.Dispose ();
                EmailTextfield = null;
            }

            if (EmailTitle != null) {
                EmailTitle.Dispose ();
                EmailTitle = null;
            }

            if (FirstnameCell != null) {
                FirstnameCell.Dispose ();
                FirstnameCell = null;
            }

            if (FirstnameIcon != null) {
                FirstnameIcon.Dispose ();
                FirstnameIcon = null;
            }

            if (FirstnameTextfield != null) {
                FirstnameTextfield.Dispose ();
                FirstnameTextfield = null;
            }

            if (FirstnameTitle != null) {
                FirstnameTitle.Dispose ();
                FirstnameTitle = null;
            }

            if (HousenrIcon != null) {
                HousenrIcon.Dispose ();
                HousenrIcon = null;
            }

            if (HousenrsupplementTableviewCell != null) {
                HousenrsupplementTableviewCell.Dispose ();
                HousenrsupplementTableviewCell = null;
            }

            if (HousenrsupplementTextfield != null) {
                HousenrsupplementTextfield.Dispose ();
                HousenrsupplementTextfield = null;
            }

            if (HousenrsupplementTitle != null) {
                HousenrsupplementTitle.Dispose ();
                HousenrsupplementTitle = null;
            }

            if (HousenumberIcon != null) {
                HousenumberIcon.Dispose ();
                HousenumberIcon = null;
            }

            if (HousenumberTableviewCell != null) {
                HousenumberTableviewCell.Dispose ();
                HousenumberTableviewCell = null;
            }

            if (HousenumberTextfield != null) {
                HousenumberTextfield.Dispose ();
                HousenumberTextfield = null;
            }

            if (HousenumberTitle != null) {
                HousenumberTitle.Dispose ();
                HousenumberTitle = null;
            }

            if (InitialsIcon != null) {
                InitialsIcon.Dispose ();
                InitialsIcon = null;
            }

            if (InitialsTableviewCell != null) {
                InitialsTableviewCell.Dispose ();
                InitialsTableviewCell = null;
            }

            if (InitialsTextfield != null) {
                InitialsTextfield.Dispose ();
                InitialsTextfield = null;
            }

            if (InitialsTitle != null) {
                InitialsTitle.Dispose ();
                InitialsTitle = null;
            }

            if (LastnameIcon != null) {
                LastnameIcon.Dispose ();
                LastnameIcon = null;
            }

            if (LastnameTableviewCell != null) {
                LastnameTableviewCell.Dispose ();
                LastnameTableviewCell = null;
            }

            if (LastnameTextfield != null) {
                LastnameTextfield.Dispose ();
                LastnameTextfield = null;
            }

            if (LastnameTitle != null) {
                LastnameTitle.Dispose ();
                LastnameTitle = null;
            }

            if (MobileIcon != null) {
                MobileIcon.Dispose ();
                MobileIcon = null;
            }

            if (MobileTableviewCell != null) {
                MobileTableviewCell.Dispose ();
                MobileTableviewCell = null;
            }

            if (MobileTextfield != null) {
                MobileTextfield.Dispose ();
                MobileTextfield = null;
            }

            if (MobileTitle != null) {
                MobileTitle.Dispose ();
                MobileTitle = null;
            }

            if (NewsletterIcon != null) {
                NewsletterIcon.Dispose ();
                NewsletterIcon = null;
            }

            if (NewsletterLabel != null) {
                NewsletterLabel.Dispose ();
                NewsletterLabel = null;
            }

            if (NewsletterSwitch != null) {
                NewsletterSwitch.Dispose ();
                NewsletterSwitch = null;
            }

            if (NewsletterTableviewCell != null) {
                NewsletterTableviewCell.Dispose ();
                NewsletterTableviewCell = null;
            }

            if (PhoneIcon != null) {
                PhoneIcon.Dispose ();
                PhoneIcon = null;
            }

            if (PhoneTableviewCell != null) {
                PhoneTableviewCell.Dispose ();
                PhoneTableviewCell = null;
            }

            if (PhoneTextfield != null) {
                PhoneTextfield.Dispose ();
                PhoneTextfield = null;
            }

            if (PhoneTitle != null) {
                PhoneTitle.Dispose ();
                PhoneTitle = null;
            }

            if (PostalcodeIcon != null) {
                PostalcodeIcon.Dispose ();
                PostalcodeIcon = null;
            }

            if (PostalcodeTableviewCell != null) {
                PostalcodeTableviewCell.Dispose ();
                PostalcodeTableviewCell = null;
            }

            if (PostalcodeTextfield != null) {
                PostalcodeTextfield.Dispose ();
                PostalcodeTextfield = null;
            }

            if (PostalTitle != null) {
                PostalTitle.Dispose ();
                PostalTitle = null;
            }

            if (PrefixIcon != null) {
                PrefixIcon.Dispose ();
                PrefixIcon = null;
            }

            if (PrefixTableviewCell != null) {
                PrefixTableviewCell.Dispose ();
                PrefixTableviewCell = null;
            }

            if (PrefixTextfield != null) {
                PrefixTextfield.Dispose ();
                PrefixTextfield = null;
            }

            if (PrefixTitle != null) {
                PrefixTitle.Dispose ();
                PrefixTitle = null;
            }

            if (ProfileAvatar != null) {
                ProfileAvatar.Dispose ();
                ProfileAvatar = null;
            }

            if (ProfilingIcon != null) {
                ProfilingIcon.Dispose ();
                ProfilingIcon = null;
            }

            if (ProfilingLabel != null) {
                ProfilingLabel.Dispose ();
                ProfilingLabel = null;
            }

            if (ProfilingSwitch != null) {
                ProfilingSwitch.Dispose ();
                ProfilingSwitch = null;
            }

            if (ProfilingTableviewCell != null) {
                ProfilingTableviewCell.Dispose ();
                ProfilingTableviewCell = null;
            }

            if (SalutationIcon != null) {
                SalutationIcon.Dispose ();
                SalutationIcon = null;
            }

            if (SalutationTableViewCell != null) {
                SalutationTableViewCell.Dispose ();
                SalutationTableViewCell = null;
            }

            if (SalutationTextfield != null) {
                SalutationTextfield.Dispose ();
                SalutationTextfield = null;
            }

            if (SalutationTitle != null) {
                SalutationTitle.Dispose ();
                SalutationTitle = null;
            }

            if (StreetIcon != null) {
                StreetIcon.Dispose ();
                StreetIcon = null;
            }

            if (StreetTableviewCell != null) {
                StreetTableviewCell.Dispose ();
                StreetTableviewCell = null;
            }

            if (StreetTextfield != null) {
                StreetTextfield.Dispose ();
                StreetTextfield = null;
            }

            if (StreetTitle != null) {
                StreetTitle.Dispose ();
                StreetTitle = null;
            }

            if (Title != null) {
                Title.Dispose ();
                Title = null;
            }
        }
    }
}