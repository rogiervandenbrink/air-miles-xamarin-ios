﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using Air_Miles_Xamarin;
using CoreGraphics;
using System.Text.RegularExpressions;
using System.Threading;
using Google.Analytics;
using Newtonsoft.Json;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class ProfileViewController : UITableViewController
    {
		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		LoadingOverlay LoadingView;

		public bool Opslaan { get; set;}
		public bool Invalid { get; set; }
		public bool FromCard { get; set; }
		public bool IsNotNederland;
		bool postal;

		UIActivityIndicatorView activitySpinner;

		UIDatePicker datePickerView;

		string errorone;
		string errortwo;
		string errorthree;
		string errorfour;

		long BirthDateChanged;
		long birthDate;
		bool birthDateHasChanged;

		long birthDayLong = 0;

		string errorList;
		List<string> bounceList;

        List<String> customPrefixList = new List<string>();

		//refreshlayout
		UIRefreshControl RefreshControl;

		NSObject observer;

		string[] Sections = new string[] { "kaartgegevens", "profiel", "contactgegevens", "adres", "updates", "* Dit zijn verplichte velden om in te vullen" };

        public ProfileViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			SetButton(false);
			TextcolorToGrey();

			Initialize();

			AMMain.AddDoneButton(SalutationTextfield);
			AMMain.AddDoneButton(FirstnameTextfield);
			AMMain.AddDoneButton(InitialsTextfield);
			AMMain.AddDoneButton(PrefixTextfield);
			AMMain.AddDoneButton(LastnameTextfield);
			AMMain.AddDoneButton(BirthdateTextfield);

			AMMain.AddDoneButton(EmailTextfield);
			AMMain.AddDoneButton(PhoneTextfield);
			AMMain.AddDoneButton(MobileTextfield);

			AMMain.AddDoneButton(CountryTextfield);
			AMMain.AddDoneButton(PostalcodeTextfield);
			AMMain.AddDoneButton(HousenumberTextfield);
			AMMain.AddDoneButton(HousenrsupplementTextfield);
			AMMain.AddDoneButton(StreetTextfield);
			AMMain.AddDoneButton(CityTextfield);

			AMStyle.BlueTextColorOnEdit(SalutationTextfield);
			AMStyle.BlueTextColorOnEdit(FirstnameTextfield);
			AMStyle.BlueTextColorOnEdit(InitialsTextfield);
			AMStyle.BlueTextColorOnEdit(PrefixTextfield);
			AMStyle.BlueTextColorOnEdit(LastnameTextfield);
			AMStyle.BlueTextColorOnEdit(BirthdateTextfield);

			AMStyle.BlueTextColorOnEdit(EmailTextfield);
			AMStyle.BlueTextColorOnEdit(PhoneTextfield);
			AMStyle.BlueTextColorOnEdit(MobileTextfield);

			AMStyle.BlueTextColorOnEdit(CountryTextfield);
			AMStyle.BlueTextColorOnEdit(PostalcodeTextfield);
			AMStyle.BlueTextColorOnEdit(HousenumberTextfield);
			AMStyle.BlueTextColorOnEdit(HousenrsupplementTextfield);
			AMStyle.BlueTextColorOnEdit(StreetTextfield);
			AMStyle.BlueTextColorOnEdit(CityTextfield);


			SalutationTextfield.ShouldBeginEditing += OnGenderPickerShouldBeginEditing;
			BirthdateTextfield.ShouldBeginEditing += OnTextFieldShouldBeginEditing;
			CountryTextfield.ShouldBeginEditing += OnCountryPickerShouldBeginEditing;
			PrefixTextfield.ShouldBeginEditing += OnPrefixPickerShouldBeginEditing;

			EmailTextfield.KeyboardType = UIKeyboardType.EmailAddress;
			PhoneTextfield.KeyboardType = UIKeyboardType.NumberPad;
			MobileTextfield.KeyboardType = UIKeyboardType.NumberPad;
			HousenumberTextfield.KeyboardType = UIKeyboardType.NumberPad;

			NewsletterLabel.Lines = 0;
			NewsletterLabel.Font = UIFont.FromName("Avenir-Book", 12f);
			NewsletterLabel.LineBreakMode = UILineBreakMode.WordWrap;
			NewsletterLabel.AdjustsFontSizeToFitWidth = true;
			NewsletterLabel.MinimumFontSize = 5f;

            ProfilingLabel.Lines = 0;
            ProfilingLabel.Font = UIFont.FromName("Avenir-Book", 12f);
            ProfilingLabel.LineBreakMode = UILineBreakMode.WordWrap;
            ProfilingLabel.AdjustsFontSizeToFitWidth = true;
            ProfilingLabel.MinimumFontSize = 5f;

            BirthdateCommentLabel.Font = UIFont.FromName("Avenir-Book", 8f);
            BirthdateCommentLabel.TextColor = UIColor.FromRGB(128, 128, 128);
            BirthdateCommentLabel.AdjustsFontSizeToFitWidth = true;

			PostalcodeTextfield.EditingDidEnd += delegate
			{
				if (CountryTextfield.Text == "Nederland")
				{
					CheckZip(PostalcodeTextfield);

					ValidCell(PostalcodeTextfield, PostalTitle, PostalcodeIcon);
					PostcodeCheckerPostcode(PostalcodeTextfield.Text);
				}
				if (!string.IsNullOrEmpty(StreetTextfield.Text))
				{
					ValidCell(StreetTextfield, StreetTitle, StreetIcon);
				}
				if (!string.IsNullOrEmpty(CityTextfield.Text))
				{
					ValidCell(CityTextfield, CityTitle, CityIcon);
				}
			};
			HousenumberTextfield.EditingDidEnd += delegate
			{
				if (CountryTextfield.Text == "Nederland")
				{
					ValidCell(HousenumberTextfield, HousenumberTitle, HousenumberIcon);
					PostcodeCheckerHuisnummer(HousenumberTextfield.Text);
				}
				if (!string.IsNullOrEmpty(StreetTextfield.Text))
				{
					ValidCell(StreetTextfield, StreetTitle, StreetIcon);
				}
				if (!string.IsNullOrEmpty(CityTextfield.Text))
				{
					ValidCell(CityTextfield, CityTitle, CityIcon);
				}
			};

			AvatarButton.TouchUpInside += delegate
			{
				Console.WriteLine("avatarbutton clicked");
				AvatarPickerViewController Redirect = App.Storyboard.InstantiateViewController("AvatarPickerViewController") as AvatarPickerViewController;
				NavigationController.PushViewController(Redirect, true);

			};

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				TableView.AddSubview(RefreshControl);
			}

			//create prefix list
			customPrefixList.Add("");
			customPrefixList.Add("a");
			customPrefixList.Add("a.");
			customPrefixList.Add("aan");
			customPrefixList.Add("aan de");
			customPrefixList.Add("aan den");
			customPrefixList.Add("aan der");
			customPrefixList.Add("aan het");
			customPrefixList.Add("aan 't");
			customPrefixList.Add("achter de");
			customPrefixList.Add("achter het");
			customPrefixList.Add("a d");
			customPrefixList.Add("af");
			customPrefixList.Add("a h");
			customPrefixList.Add("al");
			customPrefixList.Add("am");
			customPrefixList.Add("am de");
			customPrefixList.Add("an der");
			customPrefixList.Add("a t");
			customPrefixList.Add("a.t.");
			customPrefixList.Add("auf");
			customPrefixList.Add("auf das");
			customPrefixList.Add("auf dem");
			customPrefixList.Add("auf den");
			customPrefixList.Add("auf der");
			customPrefixList.Add("aus dem");
			customPrefixList.Add("aus den");
			customPrefixList.Add("b d");
			customPrefixList.Add("b.h.");
			customPrefixList.Add("bij de");
			customPrefixList.Add("bij 't");
			customPrefixList.Add("bin");
			customPrefixList.Add("d.");
			customPrefixList.Add("d'");
			customPrefixList.Add("da");
			customPrefixList.Add("dal");
			customPrefixList.Add("das");
			customPrefixList.Add("de");
			customPrefixList.Add("del");
			customPrefixList.Add("de l'");
			customPrefixList.Add("de la");
			customPrefixList.Add("de le");
			customPrefixList.Add("della");
			customPrefixList.Add("de los");
			customPrefixList.Add("dem");
			customPrefixList.Add("den");
			customPrefixList.Add("der");
			customPrefixList.Add("des");
			customPrefixList.Add("de van der");
			customPrefixList.Add("di");
			customPrefixList.Add("do");
			customPrefixList.Add("don");
			customPrefixList.Add("dos");
			customPrefixList.Add("du");
			customPrefixList.Add("el");
			customPrefixList.Add("et");
			customPrefixList.Add("et de l'");
			customPrefixList.Add("het");
			customPrefixList.Add("i");
			customPrefixList.Add("in");
			customPrefixList.Add("in de");
			customPrefixList.Add("in het");
			customPrefixList.Add("in 't");
			customPrefixList.Add("l'");
			customPrefixList.Add("la");
			customPrefixList.Add("le");
			customPrefixList.Add("lo");
			customPrefixList.Add("met de");
			customPrefixList.Add("o.d.");
			customPrefixList.Add("of");
			customPrefixList.Add("onder");
			customPrefixList.Add("onder de");
			customPrefixList.Add("onder den");
			customPrefixList.Add("onder 't");
			customPrefixList.Add("op");
			customPrefixList.Add("op de");
			customPrefixList.Add("op den");
			customPrefixList.Add("op der");
			customPrefixList.Add("op het");
			customPrefixList.Add("op 't");
			customPrefixList.Add("op ten");
			customPrefixList.Add("over");
			customPrefixList.Add("over de");
			customPrefixList.Add("'s");
			customPrefixList.Add("'t");
			customPrefixList.Add("t.");
			customPrefixList.Add("te");
			customPrefixList.Add("ten");
			customPrefixList.Add("ter");
			customPrefixList.Add("the");
			customPrefixList.Add("thoe");
			customPrefixList.Add("t t");
			customPrefixList.Add("uijt de");
			customPrefixList.Add("uit");
			customPrefixList.Add("uit de");
			customPrefixList.Add("uit den");
			customPrefixList.Add("uit het");
			customPrefixList.Add("under dem");
			customPrefixList.Add("uyt de");
			customPrefixList.Add("uyt den");
			customPrefixList.Add("v.");
			customPrefixList.Add("van");
			customPrefixList.Add("van de");
			customPrefixList.Add("van den");
			customPrefixList.Add("van der");
			customPrefixList.Add("van het");
			customPrefixList.Add("van 't");
			customPrefixList.Add("van ter");
			customPrefixList.Add("v d");
			customPrefixList.Add("v.d.");
			customPrefixList.Add("ver");
			customPrefixList.Add("von");
			customPrefixList.Add("vom");
			customPrefixList.Add("von dem");
			customPrefixList.Add("von den");
			customPrefixList.Add("von der");
			customPrefixList.Add("voor");
			customPrefixList.Add("voor de");
			customPrefixList.Add("voor den");
			customPrefixList.Add("voor in 't");
			customPrefixList.Add("voor 't");
			customPrefixList.Add("v t");
			customPrefixList.Add("v.t.");
			customPrefixList.Add("y");
			customPrefixList.Add("zum");
			customPrefixList.Add("zur");
		}

		public override void ViewWillAppear(bool animated)
		{
			
			if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
			{

				if (AMMain.DoWebService(AMLocalStorage.AanbodService) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSession))
				{
					UpdateUserInfo(false);
				}
				SetButton(false);
				Initialize();
				SetHeader();
				TableView.ReloadData();
				SetBounceIndicators();
			}

			else
			{
				// Redirect to login pag
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
				Redirect.Routing = "GegevensViewController";
				NavigationController.PushViewController(Redirect, false);
            
            }
			base.ViewWillAppear(animated);
		}

		public override void ViewDidAppear(bool animated)
		{
			SetAvatar();
			SetAnalytics();

            Console.WriteLine("Telephone private "+NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrPrivate));
            base.ViewDidAppear(animated);
		}

		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			if (section == 2)
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
				{
					SectionHeaderThreeDetails Header = new SectionHeaderThreeDetails("contactgegevens", errorone, section);
					return Header;
				}else
				{
					SectionHeaderProfile Header = new SectionHeaderProfile(Sections[section], section);
					return Header;
				}
			}
			if (section == 3)
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent))
				{
					SectionHeaderTwoDetails Header = new SectionHeaderTwoDetails("adresgegevens", errortwo, section);
					return Header;
				}
				else
				{
					SectionHeaderProfile Header = new SectionHeaderProfile(Sections[section], section);
					return Header;
				}
			}
			else
			{
				SectionHeaderProfile Header = new SectionHeaderProfile(Sections[section], section);
				return Header;

			}


		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			if (section == 0)
			{
				return 55;
			}
			if (section == 2)
			{
				if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.AmountBounceIndicators) == 1)
				{
					return 80;
				}
				if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.AmountBounceIndicators) == 2)
				{
					return 120;
				}
				if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.AmountBounceIndicators) == 3)
				{
					return 160;
				}
				//if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
				//{
				//	return 160;
				//}
				else
				{

					return 35;
				}
			}
			if (section == 3)
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent))
				{
					return 80;
				}else
				{
					return 35;
				}
			}
			else {
				return 35;
			}

		}

		void Initialize()
		{
			if (FromCard)
			{
				HideBackButton();
				if (NavigationController != null)
				{
					NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(

					UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
					{
						NavigationController.PopToRootViewController(false);

					}), true);
				}
				FromCard = false;
			}

			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserBirthday)))
			{
				birthDate = Int64.Parse(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserBirthday));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
			{
			CardnumberLabel.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
			CardnumberLabel.TextColor = UIColor.FromRGB(128, 128, 128);
			}
			SalutationTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation);
			FirstnameTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName);
			InitialsTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserInitials);
			PrefixTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix);
			LastnameTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
            if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserBirthday)))
            {
                BirthdateTextfield.Text = SharedDatetime.ConvertFromUnixTimestamp(birthDate).ToString("d", AMMain.Cultureinfo);
            }
            EmailTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
			PhoneTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrPrivate);
			MobileTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrMobile);
			CountryTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCountry);
			PostalcodeTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode);
			HousenumberTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
			HousenrsupplementTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement);
			StreetTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet);
			CityTextfield.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);

            List<string> bounceIndicators = new List<string>();
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason)))
			{
				bounceIndicators.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason)))
			{
				bounceIndicators.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason)))
			{
				bounceIndicators.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason));
			}
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason)))
			{
				bounceIndicators.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason));
			}

			NewsletterSwitch.On = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.OptIn);
			NewsletterIcon.Image = UIImage.FromBundle("IconNewsletter");
            ProfilingSwitch.On = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Profiling);
            ProfilingIcon.Image = UIImage.FromBundle("IconGift");

			StreetTableviewCell.BackgroundColor = UIColor.White;
			CityTableviewCell.BackgroundColor = UIColor.White;

		}

		void SetHeader()
		{
			SetAvatar();

			ProfileAvatar.Layer.CornerRadius = ProfileAvatar.Layer.Bounds.Height / 2;
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		private void SetAvatar()
		{
			AMMain.SetAvatar(ProfileAvatar, "Airmiles_Placeholder@3x.png");

			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				AvatarButton.SetImage(UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))), UIControlState.Normal);

			}
			else {
				AvatarButton.SetImage(UIImage.FromBundle("Airmiles_Placeholder@3x.png"), UIControlState.Normal);
				AvatarButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Fill;
				AvatarButton.VerticalAlignment = UIControlContentVerticalAlignment.Fill;
			}

			AvatarButton.Layer.CornerRadius = AvatarButton.Bounds.Height / 2;
			AvatarButton.Layer.MasksToBounds = true;
		}

		void DisableStreetCity(bool enable)
		{
			if (!enable)
			{
				StreetTableviewCell.UserInteractionEnabled = false;
				StreetTableviewCell.BackgroundColor = UIColor.FromRGB(230, 230, 230);
				CityTableviewCell.UserInteractionEnabled = false;
				CityTableviewCell.BackgroundColor = UIColor.FromRGB(230, 230, 230);
			}
			else
			{
				StreetTableviewCell.UserInteractionEnabled = true;
				StreetTableviewCell.BackgroundColor = UIColor.White;
				CityTableviewCell.UserInteractionEnabled = true;
				CityTableviewCell.BackgroundColor = UIColor.White;

			}

		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Persoonlijke gegevens");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		void SetSpinner()
		{
			// derive the center x and y
			nfloat centerX = UIScreen.MainScreen.Bounds.Width / 2;
			nfloat centerY = UIScreen.MainScreen.Bounds.Height / 2;

			// create the activity spinner, center it horizontall and put it 5 points above center x
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activitySpinner.Frame = new CGRect(
				centerX - (activitySpinner.Frame.Width / 2),
				centerY - activitySpinner.Frame.Height - 20,
				activitySpinner.Frame.Width,
				activitySpinner.Frame.Height);
			activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
			Add(activitySpinner);
			activitySpinner.StartAnimating();
		}

		void SetLoader(string text)
		{
			//CGRect bounds = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 1250);
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);


		}

		void DisableCells(bool enabled)
		{
			SalutationTableViewCell.UserInteractionEnabled = enabled;
			FirstnameCell.UserInteractionEnabled = enabled;
			InitialsTableviewCell.UserInteractionEnabled = enabled;
			PrefixTableviewCell.UserInteractionEnabled = enabled;
			LastnameTableviewCell.UserInteractionEnabled = enabled;
			BirthdateTableviewCell.UserInteractionEnabled = enabled;

			EmailTableviewcell.UserInteractionEnabled = enabled;
			PhoneTableviewCell.UserInteractionEnabled = enabled;
			MobileTableviewCell.UserInteractionEnabled = enabled;

			CountryTableviewCell.UserInteractionEnabled = enabled;
			PostalcodeTableviewCell.UserInteractionEnabled = enabled;
			HousenumberTableviewCell.UserInteractionEnabled = enabled;
			HousenrsupplementTableviewCell.UserInteractionEnabled = enabled;
			StreetTableviewCell.UserInteractionEnabled = enabled;
			CityTableviewCell.UserInteractionEnabled = enabled;

			NewsletterTableviewCell.UserInteractionEnabled = enabled;
			NewsletterSwitch.Enabled = enabled;
            ProfilingTableviewCell.UserInteractionEnabled = enabled;
            ProfilingSwitch.Enabled = enabled;
		}

		void TextcolorToBlack()
		{
			AMStyle.BlackTextColor(SalutationTextfield);
			AMStyle.BlackTextColor(FirstnameTextfield);
			AMStyle.BlackTextColor(InitialsTextfield);
			AMStyle.BlackTextColor(PrefixTextfield);
			AMStyle.BlackTextColor(LastnameTextfield);
			AMStyle.BlackTextColor(BirthdateTextfield);

			AMStyle.BlackTextColor(EmailTextfield);
			AMStyle.BlackTextColor(PhoneTextfield);
			AMStyle.BlackTextColor(MobileTextfield);

			AMStyle.BlackTextColor(CountryTextfield);
			AMStyle.BlackTextColor(PostalcodeTextfield);
			AMStyle.BlackTextColor(HousenumberTextfield);
			AMStyle.BlackTextColor(HousenrsupplementTextfield);
			AMStyle.BlackTextColor(StreetTextfield);
			AMStyle.BlackTextColor(CityTextfield);
		}

		void TextcolorToGrey()
		{
			AMStyle.GreyTextColor(SalutationTextfield);
			AMStyle.GreyTextColor(FirstnameTextfield);
			AMStyle.GreyTextColor(InitialsTextfield);
			AMStyle.GreyTextColor(PrefixTextfield);
			AMStyle.GreyTextColor(LastnameTextfield);
			AMStyle.GreyTextColor(BirthdateTextfield);

			AMStyle.GreyTextColor(EmailTextfield);
			AMStyle.GreyTextColor(PhoneTextfield);
			AMStyle.GreyTextColor(MobileTextfield);

			AMStyle.GreyTextColor(CountryTextfield);
			AMStyle.GreyTextColor(PostalcodeTextfield);
			AMStyle.GreyTextColor(HousenumberTextfield);
			AMStyle.GreyTextColor(HousenrsupplementTextfield);
			AMStyle.GreyTextColor(StreetTextfield);
			AMStyle.GreyTextColor(CityTextfield);
		}

		void ShowAlert()
		{
			AMAlerts.DefaultAlert(this, "Weet je het zeker?", "Wijzigen gaan verloren.");
		}

		public void SetButton(bool opslaan)
		{
			if (opslaan)
			{
				TextcolorToBlack();
				DisableCells(true);
				if (CountryTextfield.Text == "Nederland")
				{
					DisableStreetCity(false);
				}
				if (CountryTextfield.Text != "Nederland")
				{
					DisableStreetCity(true);
				}
				ProfileAvatar.Hidden = true;
				AvatarButton.Hidden = false;
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
				{
					AvatarButton.SetImage(UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))), UIControlState.Normal);

				}
				else {
					AvatarButton.SetImage(UIImage.FromBundle("Airmiles_Placeholder@3x.png"), UIControlState.Normal);
					AvatarButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Fill;
					AvatarButton.VerticalAlignment = UIControlContentVerticalAlignment.Fill;
				}

				AvatarButton.Layer.CornerRadius = AvatarButton.Bounds.Height / 2;
				AvatarButton.Layer.MasksToBounds = true;

				UITextAttributes btnFontAttribute = new UITextAttributes();

				btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
				btnFontAttribute.TextColor = UIColor.White;
				UIBarButtonItem btn = new UIBarButtonItem();
				btn.Title = "Opslaan";
				btn.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
				btn.Clicked += (sendero, e) =>
				{
					DismissKeyboard();
					Console.WriteLine("brithdate "+ BirthdateTextfield.Text);
					ValidateInput();

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Persoonlijke gegevens", "Opslaan", "Opslaan", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();

					if (!Invalid)
					{
						TableView.ScrollRectToVisible(new CGRect(0, 0, 1, 1), false);
						string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureMemberId, true);

                        string Salutation = "empty";
                        if (!string.IsNullOrEmpty(SalutationTextfield.Text)) {
                            Salutation = SalutationTextfield.Text;
                        }

						ChangeProfile(MemberId, Salutation, FirstnameTextfield.Text, LastnameTextfield.Text, PrefixTextfield.Text, birthDayLong, EmailTextfield.Text, EmailTextfield.Text, StreetTextfield.Text, HousenumberTextfield.Text, HousenrsupplementTextfield.Text, CityTextfield.Text, PostalcodeTextfield.Text, PhoneTextfield.Text, MobileTextfield.Text, CountryTextfield.Text, InitialsTextfield.Text, NewsletterSwitch.On, ProfilingSwitch.On);

					}
					else
					{
						//AMAlerts.DefaultAlert(this, "Je gegevens zijn nog niet compleet", errorList);

						//Create Alert
						UIAlertController AlertController = UIAlertController.Create("Je gegevens zijn nog niet compleet", errorList, UIAlertControllerStyle.Alert);

						//Add Action
						AlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, delegate {
							errorList = null;	
						}));

						// Present Alert
						this.PresentViewController(AlertController, true, null);
					}


				};
				this.NavigationItem.RightBarButtonItem = btn;

				UITextAttributes btnLeftFontAttribute = new UITextAttributes();

				btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
				btnFontAttribute.TextColor = UIColor.White;
				UIBarButtonItem btnleft = new UIBarButtonItem();
				btnleft.Title = "Annuleren";
				btnleft.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
				btnleft.Clicked += (sendero, e) =>
				{

					CancelProfileAlert("Weet je het zeker?", "Wijzigingen gaan verloren.");

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Persoonlijke gegevens", "Annuleren","Annuleren", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();

				};
				this.NavigationItem.LeftBarButtonItem = btnleft;

			}
			else {
				TextcolorToGrey();
				DisableCells(false);
				StreetTableviewCell.BackgroundColor = UIColor.White;
				CityTableviewCell.BackgroundColor = UIColor.White;

				ProfileAvatar.Hidden = true;
				AvatarButton.Hidden = false;
				UITextAttributes btnFontAttribute = new UITextAttributes();
				btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
				btnFontAttribute.TextColor = UIColor.White;
				UIBarButtonItem btn = new UIBarButtonItem();
				btn.Title = "Wijzig";
				btn.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
				btn.Clicked += (sendero, e) =>
				{
					SetButton(true);
					//RefreshTableAanpassenItems();
					Opslaan = true;

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Persoonlijke gegevens", "Wijzig gegevens", "Wijzig gegevens", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();

				};
				this.NavigationItem.RightBarButtonItem = btn;
				this.NavigationItem.LeftBarButtonItem = null;
			};
		}

		bool OnGenderPickerShouldBeginEditing(UITextField textField)
		{

			// Create a new Alert Controller
			UIAlertController actionSheetAlert = UIAlertController.Create(null, "Selecteer aanhef", UIAlertControllerStyle.ActionSheet);

			// Add Actions
            actionSheetAlert.AddAction(UIAlertAction.Create("", UIAlertActionStyle.Default, (action) => {
                textField.Text = "";
                Gegevens.Aanhef = "";
            }));

            actionSheetAlert.AddAction(UIAlertAction.Create("Heer", UIAlertActionStyle.Default, (action) => {
				textField.Text = "heer";
				Gegevens.Aanhef = "heer";
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("Mevrouw", UIAlertActionStyle.Default, (action) =>
			{
				textField.Text = "mevrouw";
				Gegevens.Aanhef = "mevrouw";
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

			// Required for iPad - You must specify a source for the Action Sheet since it is
			// displayed as a popover
			UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
			if (presentationPopover != null)
			{
				presentationPopover.SourceView = this.View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			// Display the alert
			this.PresentViewController(actionSheetAlert, true, null);

			return false;
		}

		bool OnTextFieldShouldBeginEditing(UITextField textField)
		{


            var viewDatePicker = new UIView();
            viewDatePicker.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 200);
            viewDatePicker.BackgroundColor = UIColor.Clear;

            var datePicker = new UIDatePicker();
            datePicker.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 200);
            datePicker.Mode = UIDatePickerMode.Date;
            datePicker.Locale = new NSLocale("nl-NL");
			var dateFormatter = new NSDateFormatter()
			{
				DateFormat = "dd-MM-yyyy"
			};
            if(!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserBirthday))){
                birthDate = Int64.Parse(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserBirthday));
                var date = SharedDatetime.ConvertFromUnixTimestamp(birthDate);
                datePicker.SetDate((NSDate)date, true);
            }
            else
            {
                datePicker.SetDate((NSDate)DateTime.UtcNow, true);
            }
            datePicker.ValueChanged += (sender, e) =>
            {
                

                DateTime date = DateTime.ParseExact(dateFormatter.ToString((sender as UIDatePicker).Date), "dd-MM-yyyy", AMMain.Cultureinfo);
                var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Amsterdam");
                var localTime = TimeZoneInfo.ConvertTimeFromUtc(date, localTimeZone);
                long epochDate = SharedDatetime.ConvertToUnixTimestampLong(localTime);
                Console.WriteLine("epoch is " + epochDate);

                textField.Text = dateFormatter.ToString((sender as UIDatePicker).Date);
                BirthDateChanged = epochDate;
                birthDateHasChanged = true;
                //textField.Text = (sender as UIDatePicker).Date.ToString();
            };

            viewDatePicker.AddSubview(datePicker);

            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{

                var alertController = UIAlertController.Create(null, "\n\n\n\n\n\n\n\n\n\n", UIAlertControllerStyle.ActionSheet);
                alertController.View.AddSubview(viewDatePicker);

				//Add Actions
				var cancelAction = UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, alertAction => Console.WriteLine("Cancel was Pressed"));
				var okayAction = UIAlertAction.Create("Bevestig datum", UIAlertActionStyle.Default, alertAction => Console.WriteLine("The user entered ok"));

                alertController.AddAction(cancelAction);
                alertController.AddAction(okayAction);


                this.PresentViewController(alertController, true, null);

			
				}

			
			else
			{
				var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Selecteer de datum", this)
				{
				  HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
				  HeaderTextColor = UIColor.White,
				  TransitioningDelegate = new ModalPickerTransitionDelegate(),
				  ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPicker.DatePicker.Mode = UIDatePickerMode.Date;
				modalPicker.DatePicker.IsAccessibilityElement = true;
				modalPicker.DatePicker.AccessibilityLabel = "BirthdatePicker";

				modalPicker.OnModalPickerDismissed += (s, ea) =>
				{
				 
				  if (modalPicker.DatePicker.Date != null)
				  {
				      DateTime date = DateTime.ParseExact(dateFormatter.ToString(modalPicker.DatePicker.Date), "dd-MM-yyyy", AMMain.Cultureinfo);
				      var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Amsterdam");
				      var localTime = TimeZoneInfo.ConvertTimeFromUtc(date, localTimeZone);
				      long epochDate = SharedDatetime.ConvertToUnixTimestampLong(localTime);
				      Console.WriteLine("epoch is " + epochDate);

				      textField.Text = dateFormatter.ToString(modalPicker.DatePicker.Date);
				      BirthDateChanged = epochDate;
				      birthDateHasChanged = true;

				  }
				  Console.WriteLine("gegevens geboortedatum " + BirthDateChanged +" "+ textField.Text);
				  Console.WriteLine("geboortedatum  aangepast" + birthDateHasChanged);
				};

				this.PresentViewController(modalPicker, true, null);

			}

			return false;
		}

		bool OnCountryPickerShouldBeginEditing(UITextField textField)
		{


			// Create a new Alert Controller
			UIAlertController actionSheetAlert = UIAlertController.Create(null, "Selecteer land", UIAlertControllerStyle.ActionSheet);

			// Add Actions
			actionSheetAlert.AddAction(UIAlertAction.Create("Nederland", UIAlertActionStyle.Default, (action) =>
			{
				textField.Text = "Nederland";
				Gegevens.Aanhef = "Nederland";
				IsNotNederland = false;
				DisableStreetCity(false);
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("België", UIAlertActionStyle.Default, (action) =>
			{
				textField.Text = "België";
				Gegevens.Aanhef = "België";
				IsNotNederland = true;
				DisableStreetCity(true);
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("Duitsland", UIAlertActionStyle.Default, (action) =>
			{
				textField.Text = "Duitsland";
				Gegevens.Aanhef = "Duitsland";
				IsNotNederland = true;
				DisableStreetCity(true);
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("Frankrijk", UIAlertActionStyle.Default, (action) =>
			{
				textField.Text = "Frankrijk";
				Gegevens.Aanhef = "Frankrijk";
				IsNotNederland = true;
				DisableStreetCity(true);
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("Luxemburg", UIAlertActionStyle.Default, (action) =>
			{
				textField.Text = "Luxemburg";
				Gegevens.Aanhef = "Luxemburg";
				IsNotNederland = true;
				DisableStreetCity(true);
			}));

			actionSheetAlert.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

			// Required for iPad - You must specify a source for the Action Sheet since it is
			// displayed as a popover
			UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
			if (presentationPopover != null)
			{
				presentationPopover.SourceView = this.View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			Gegevens.Land = textField.Text;
			  
			// Display the alert
			this.PresentViewController(actionSheetAlert, true, null);

			return false;
		}

		bool OnPrefixPickerShouldBeginEditing(UITextField textField)
		{
            SetLoader("Tussenvoegsels laden...");
			// Create a new Alert Controller
			UIAlertController actionSheetAlert = UIAlertController.Create(null, "Selecteer tussenvoegsel", UIAlertControllerStyle.ActionSheet);

            // Add Actions

			for (int i = 0; i < customPrefixList.Count; i++)
			{
                actionSheetAlert.AddAction(UIAlertAction.Create(customPrefixList[i], UIAlertActionStyle.Default, (UIAlertAction obj) => {
                    var index = obj.Title;
                    textField.Text = obj.Title;
                    Console.WriteLine("pressed prefix "+(obj.Title));

                }));
			}

			actionSheetAlert.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

			// Required for iPad - You must specify a source for the Action Sheet since it is
			// displayed as a popover
			UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
			if (presentationPopover != null)
			{
				presentationPopover.SourceView = this.View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			// Display the alert
			this.PresentViewController(actionSheetAlert, true, null);
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}

			return false;
		}


		public void SetBounceIndicators()
		{
			errorone = "";
			errortwo = "";
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
			{

				Console.WriteLine("Bounce indicator present");
				bounceList = new List<string>();
				bounceList.Clear();
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason)))
				{
					bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason));
				}
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason)))
				{
					bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason));
				}
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason)))
				{
					bounceList.Add(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason));
				}
				string combindedBounceIndicators = string.Join(Environment.NewLine, bounceList);
				Console.WriteLine("combined bi "+combindedBounceIndicators);
				errorone = combindedBounceIndicators;
			}
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent))
				Console.WriteLine("address bounce present "+NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent));
			{
				errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason);
			}
			TableView.ReloadData();
			RefreshControl.EndRefreshing();

		}

		private async void ChangeProfile(string memberid, string saluation, string firstname, string lastname, string prefix, long birthdate, string email, string emailconfirm, string street, string housenumber, string housenumbersupplement, string city, string postalcode, string phone, string mobile, string country, string initials, bool newsletter, bool profiling)
		{
			SetLoader("Gegevens aanpassen...");
			TableView.ScrollEnabled = false;
			try
			{
			ChangeProfileService ChangeProfileService = new ChangeProfileService();
			GegevensRoot GegevensResponse = await ChangeProfileService.ChangeProfile(memberid, saluation, firstname, lastname, prefix, birthdate, email, emailconfirm, street, housenumber, housenumbersupplement, city, postalcode, phone, mobile, country, initials, newsletter, profiling);

			Console.WriteLine("input aanpassen gegevens: " + memberid + saluation + firstname + lastname + prefix + birthdate + email + emailconfirm + street + housenumber + housenumbersupplement + city + postalcode + phone + mobile + country + initials);

				if (GegevensResponse.Error.Length > 0)
				{
					Console.WriteLine("Response melding changeprofile: " + GegevensResponse.Error[0].Code);
					Console.WriteLine("Response message changeprofile: " + GegevensResponse.Error[0].Message);
					if (GegevensResponse.Error[0].Code != AMStrings.ErrorCode)
					{
						if (GegevensResponse.Error[0].Code == "E00")
						{
							NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.AmountBounceIndicators);
							List<string> bounceIndicators = new List<string>();
							bounceIndicators.Clear();

							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].HouseNumber))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].HouseNumber, AMLocalStorage.UserHouseNumber);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserHouseNumber);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Email))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Email, AMLocalStorage.UserEmail);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserEmail);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].TelephoneNrPrivate))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].TelephoneNrPrivate, AMLocalStorage.UserTelephoneNrPrivate);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserTelephoneNrPrivate);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].FirstName))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].FirstName, AMLocalStorage.UserFirstName);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserFirstName);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].TelephoneNrMobile))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].TelephoneNrMobile, AMLocalStorage.UserTelephoneNrMobile);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserTelephoneNrMobile);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].PostalCode))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].PostalCode, AMLocalStorage.UserPostalCode);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserPostalCode);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].City))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].City, AMLocalStorage.UserCity);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserCity);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Prefix))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Prefix, AMLocalStorage.UserPrefix);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserPrefix);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Initials))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Initials, AMLocalStorage.UserInitials);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserInitials);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Salutation))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Salutation, AMLocalStorage.UserSalutation);
							}
                            else{
                                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserSalutation);
                            }
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].HouseNumberSuppliment))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].HouseNumberSuppliment, AMLocalStorage.UserHouseNumberSupplement);
							}else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserHouseNumberSupplement);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].FullName))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].FullName, AMLocalStorage.UserFullName);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserFullName);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Street))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Street, AMLocalStorage.UserStreet);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserStreet);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Country))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Country, AMLocalStorage.UserCountry);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserCountry);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].LastName))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].LastName, AMLocalStorage.UserLastName);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserLastName);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].BirthDate))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].BirthDate, AMLocalStorage.UserBirthday);
							}
							else
							{
								NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserBirthday);
							} 
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].MobileNrBounceMessage))
							{
								bounceIndicators.Add(GegevensResponse.Wijziging[0].MobileNrBounceMessage);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].PrivateNrBounceMessage))
							{
								bounceIndicators.Add(GegevensResponse.Wijziging[0].PrivateNrBounceMessage);
							}
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].PrivateNrBounceMessage, AMLocalStorage.PrivatePhoneBounceReason);
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].MobileNrBounceMessage, AMLocalStorage.MobilePhoneBounceReason);
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].EmailBounceMessage, AMLocalStorage.EmailBounceReason);

							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].EmailBounceMessage))
							{
								bounceIndicators.Add(GegevensResponse.Wijziging[0].EmailBounceMessage);
							}
							//if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].AddressBounceMessage))
							//{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].AddressBounceMessage, AMLocalStorage.AddressBounceReason);
								//bounceIndicators.Add(GegevensResponse.Wijziging[0].AddressBounceMessage);
							//}
							NSUserDefaults.StandardUserDefaults.SetInt(bounceIndicators.Count, AMLocalStorage.AmountBounceIndicators);
							Console.WriteLine("amount bounce indicators "+bounceIndicators.Count);
							Console.WriteLine("response bi mobile bool "+GegevensResponse.Wijziging[0].MobileNrBouncePresent);
							Console.WriteLine("response bi email bool " + GegevensResponse.Wijziging[0].EmailBouncePresent);
							Console.WriteLine("response bi telefoon bool " + GegevensResponse.Wijziging[0].PrivateNrBouncePresent);
							Console.WriteLine("response bi address bool " + GegevensResponse.Wijziging[0].AddressBouncePresent);

							Console.WriteLine("amount bounce indicators " + bounceIndicators.Count);
							Console.WriteLine("response bi mobile reason " + GegevensResponse.Wijziging[0].MobileNrBounceMessage);
							Console.WriteLine("response bi email reason " + GegevensResponse.Wijziging[0].EmailBounceMessage);
							Console.WriteLine("response bi telefoon reason " + GegevensResponse.Wijziging[0].PrivateNrBounceMessage);
							Console.WriteLine("response bi address reason " + GegevensResponse.Wijziging[0].AddressBounceMessage);

							NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].MobileNrBouncePresent, AMLocalStorage.MobileNrBouncePresent);
							NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].PrivateNrBouncePresent, AMLocalStorage.PrivateNrBouncePresent);
							NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].EmailBouncePresent, AMLocalStorage.EmailBouncePresent);
							NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].AddressBouncePresent, AMLocalStorage.AddressBouncePresent);

                            //updates newsletter and profiling
							NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].OptIn, AMLocalStorage.OptIn);
                            NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].Profiling, AMLocalStorage.Profiling);
							NSUserDefaults.StandardUserDefaults.Synchronize();

							Console.WriteLine("local bi mobile bool " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent));
							Console.WriteLine("local bi email bool " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent));
							Console.WriteLine("local bi telefoon bool " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent));
							Console.WriteLine("local bi address bool " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent));

							Console.WriteLine("local bi mobile reason " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason));
							Console.WriteLine("local bi email reason " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason));
							Console.WriteLine("local bi telefoon reason " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason));
							Console.WriteLine("local bi address reason " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason));

							SetAllValid();
							observer = NSNotificationCenter.DefaultCenter.AddObserver((NSString)"NSUserDefaultsDidChangeNotification", ReloadTable);
							if (LoadingView != null)
							{
								LoadingView.Hide();
							}
							TableView.ScrollEnabled = true;
							AMAlerts.DefaultAlert(this, "Opgeslagen", GegevensResponse.Error[0].Message);
							SetButton(false);
						}
						else
						{
							string place = "";
							if (!string.IsNullOrEmpty(GegevensResponse.Error[0].Place))
							{
								place = GegevensResponse.Error[0].Place;
							}

							switch (place)
							{
								case "P02":
									ErrorCell(FirstnameTextfield, FirstnameTitle, FirstnameIcon);
									break;
								case "P03":
									ErrorCell(LastnameTextfield, LastnameTitle, LastnameIcon);
									break;
								case "P04":
									ErrorCell(InitialsTextfield, InitialsTitle, InitialsIcon);
									break;
								case "P05":
									ErrorCell(MobileTextfield, MobileTitle, MobileIcon);
									break;
								case "P06":
									ErrorCell(PhoneTextfield, PhoneTitle, PhoneIcon);
									break;
								case "P07":
									ErrorCell(StreetTextfield, StreetTitle, StreetIcon);
									break;
								case "P08":
									ErrorCell(HousenumberTextfield, HousenumberTitle, HousenumberIcon);
									break;
								case "P09":
									ErrorCell(HousenrsupplementTextfield, HousenrsupplementTitle, HousenrIcon);
									break;
								case "P10":
									ErrorCell(PostalcodeTextfield, PostalTitle, PostalcodeIcon);
									break;
								case "P11":
									ErrorCell(CityTextfield, CityTitle, CityIcon);
									break;
								case "P12":
									ErrorCell(CountryTextfield, CountryTitle, CountryIcon);
									break;
								case "P13":
									ErrorCell(BirthdateTextfield, BirthdateTitle, BirthdateIcon);
									break;
								case "P14":
									ErrorCell(SalutationTextfield, SalutationTitle, SalutationIcon);
									break;
								case "P15":
									ErrorCell(EmailTextfield, EmailTitle, EmailIcon);
									break;
								case "P16":
									ErrorCell(PrefixTextfield, PrefixTitle, PrefixIcon);
									break;
							}

							TableView.ReloadData();
							if (LoadingView != null)
							{
								LoadingView.Hide();
							}
							TableView.ScrollEnabled = true;
							AMAlerts.DefaultAlert(this, "Je gegevens zijn nog niet aangepast", GegevensResponse.Error[0].Message);
						}

					}
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Gegevens: Gegevens wijzigen", "Exception: "+ ex}
                });
				Console.WriteLine("exception opslaan " + ex);
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
				TableView.ScrollEnabled = true;
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Je gegevens zijn nog niet aangepast");
			}
			UpdateUserInfo(false);
		}

		void ReloadTable(NSNotification obj)
		{
			Initialize();
			SetBounceIndicators();
			TableView.ReloadData();
		}

		public void CancelProfileAlert(string Title, string Message)
		{
			//Create Alert
			UIAlertController AlertController = UIAlertController.Create(Title, Message, UIAlertControllerStyle.Alert);

			//Add Action
			AlertController.AddAction(UIAlertAction.Create("Ja", UIAlertActionStyle.Default, (UIAlertAction obj) => DismissChanges()));
			AlertController.AddAction(UIAlertAction.Create("Nee", UIAlertActionStyle.Default, null));

			// Present Alert
			this.PresentViewController(AlertController, true, null);
		}

		public void DismissChanges()
		{
			DismissKeyboard();
			SetAllValid();
			SetButton(false);
			Initialize();
			TableView.ReloadData();
		}

		public void DismissKeyboard()
		{
			CardnumberLabel.ResignFirstResponder();

			SalutationTextfield.ResignFirstResponder();
			FirstnameTextfield.ResignFirstResponder();
			InitialsTextfield.ResignFirstResponder();
			PrefixTextfield.ResignFirstResponder();
			LastnameTextfield.ResignFirstResponder();
			BirthdateTextfield.ResignFirstResponder();

			EmailTextfield.ResignFirstResponder();
			PhoneTextfield.ResignFirstResponder();
			MobileTextfield.ResignFirstResponder();

			CountryTextfield.ResignFirstResponder();
			PostalcodeTextfield.ResignFirstResponder();
			HousenumberTextfield.ResignFirstResponder();
			HousenrsupplementTextfield.ResignFirstResponder();
			StreetTextfield.ResignFirstResponder();
			CityTextfield.ResignFirstResponder();
		}


		public void ValidateInput()
		{
			bool firstname;
			bool initials;
			bool lastname;
			bool email;
			bool housenumber;
			bool street;
			bool city;
			bool birthday;
			bool salutation;

			Console.WriteLine("country textfield "+CountryTextfield.Text);

			if (birthDateHasChanged)
			{
				birthDayLong = BirthDateChanged;
				Console.WriteLine("birthdate changed: " + birthDayLong);
				birthDateHasChanged = false;
			}
			else
			{
				if (birthDate != 0)
				{
					birthDayLong = birthDate;
					Console.WriteLine("birthdate not changed: " + birthDayLong);
				}
				else
				{
					birthDayLong = BirthDateChanged;
					Console.WriteLine("birthdate not changed text: " + birthDayLong);
				}

			}

			if (!string.IsNullOrEmpty(SalutationTextfield.Text))
			{
				salutation = true;
				ValidCell(SalutationTextfield, SalutationTitle, SalutationIcon);
			}
			else
			{
				salutation = true;
                ValidCell(SalutationTextfield, SalutationTitle, SalutationIcon);

			}
			if (!string.IsNullOrEmpty(FirstnameTextfield.Text))
			{
				firstname = true;
				ValidCell(FirstnameTextfield, FirstnameTitle, FirstnameIcon);
			}
			else
			{
				firstname = false;
				ErrorCell(FirstnameTextfield, FirstnameTitle, FirstnameIcon);
				errorList += "\n- Voornaam is niet ingevuld.";

			}
			if (!string.IsNullOrEmpty(InitialsTextfield.Text))
			{
				initials = true;
				ValidCell(InitialsTextfield, InitialsTitle, InitialsIcon);
			}
			else
			{
				initials = false;
				ErrorCell(InitialsTextfield, InitialsTitle, InitialsIcon);
				errorList+= "\n- Voorletters zijn niet ingevuld.";

			}
			if (!string.IsNullOrEmpty(LastnameTextfield.Text))
			{
				lastname = true;
				ValidCell(LastnameTextfield, LastnameTitle, LastnameIcon);
			}
			else
			{
				lastname = false;
				ErrorCell(LastnameTextfield, LastnameTitle, LastnameIcon);
				errorList+= "\n- Achternaam is niet ingevuld.";

			}
			if (Regex.Match(EmailTextfield.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
			{
				email = true;
				ValidCell(EmailTextfield, EmailTitle, EmailIcon);
			}
			else
			{
				email = false;
				ErrorCell(EmailTextfield, EmailTitle, EmailIcon);
				errorList+= "\n- Email is niet ingevuld.";

			}
			if (!string.IsNullOrEmpty(PhoneTextfield.Text))
			{
				if (PhoneTextfield.Text.Length == 10)
				{
					ValidCell(PhoneTextfield, PhoneTitle, PhoneIcon);
				}

				else
				{
					ErrorCell(PhoneTextfield, PhoneTitle, PhoneIcon);

				}
			}
			if (!string.IsNullOrEmpty(MobileTextfield.Text))
			{
				if (MobileTextfield.Text.Length == 10)
				{
					ValidCell(MobileTextfield, MobileTitle, MobileIcon);
				}

				else
				{
					ErrorCell(MobileTextfield, MobileTitle, MobileIcon);

				}
			}
			if (Regex.Match(PostalcodeTextfield.Text, "^[1-9][\\d]{3}\\s?(?!(sa|sd|ss|SA|SD|SS))([a-eghj-opr-tv-xzA-EGHJ-OPR-TV-XZ]{2})?$").Success)
			{
					postal = true;
					ValidCell(PostalcodeTextfield, PostalTitle, PostalcodeIcon);
				}
				else
				{
					postal = false;
					ErrorCell(PostalcodeTextfield, PostalTitle, PostalcodeIcon);
					errorList += "\n- Postcode is niet ingevuld.";

			}
			if (!string.IsNullOrEmpty(HousenumberTextfield.Text))
			{
				housenumber = true;
				ValidCell(HousenumberTextfield, HousenumberTitle, HousenumberIcon);
			}
			else
			{
				housenumber = false;
				ErrorCell(HousenumberTextfield, HousenumberTitle, HousenumberIcon);
				errorList+= "\n- Huisnummer is niet ingevuld.";

			}
			if (!string.IsNullOrEmpty(StreetTextfield.Text))
			{
				street = true;
				ValidCell(StreetTextfield, StreetTitle, StreetIcon);
			}
			else
			{
				street = false;
				ErrorCell(StreetTextfield, StreetTitle, StreetIcon);
				errorList+= "\n- Straat is niet ingevuld.";

			}
			Console.WriteLine("birthdaylong "+birthDayLong);
			if (birthDayLong != 0)
			{
				birthday = true;
				ValidCell(BirthdateTextfield, BirthdateTitle, BirthdateIcon);
			}
			else
			{
				birthday = false;
				ErrorCell(BirthdateTextfield, BirthdateTitle, BirthdateIcon);
				errorList += "\n- Geboortedatum is niet ingevuld.";

			}
			if (!string.IsNullOrEmpty(CityTextfield.Text))
			{
				city = true;
				ValidCell(CityTextfield, CityTitle, CityIcon);
			}
			else
			{
				city = false;
				ErrorCell(CityTextfield, CityTitle, CityIcon);
				errorList+= "\n- Stad is niet ingevuld.";

			}

			if (salutation && firstname && initials && lastname && email && postal && housenumber && street && city && birthday)
			{
				Invalid = false;
			}
			else
			{
				Invalid = true;

			}
		}

		public async void PostcodeCheckerPostcode(string postcode)
		{
			
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber)))
			{
				try
				{
					string houseNumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
					PostcodeCheckerService PostcodeService = new PostcodeCheckerService();
					PostcodeCheckerDataRoot CurrentAddress = await PostcodeService.GetPostcodeCheckerData(postcode, houseNumber);

					if (CurrentAddress.Errors[0] != null && CurrentAddress.Errors[0].Code == "E00")
					{
						StreetTextfield.Text = CurrentAddress.Response.Street;
						CityTextfield.Text = CurrentAddress.Response.City;

						Console.WriteLine("response getaddress straat "+CurrentAddress.Response.Street +" plaats "+ CurrentAddress.Response.City);
						Console.WriteLine("postcode checker postcode huisnummer storage " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber));
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("exception postcodeservice " + ex);
				}

			}

		}

		public async void PostcodeCheckerHuisnummer(string huisnummer)
		{


			if (!string.IsNullOrEmpty(PostalcodeTextfield.Text))
			{
				try
				{
					PostcodeCheckerService PostcodeService = new PostcodeCheckerService();
					PostcodeCheckerDataRoot CurrentAddress = await PostcodeService.GetPostcodeCheckerData(PostalcodeTextfield.Text, huisnummer);

					if (CurrentAddress.Errors[0] != null && CurrentAddress.Errors[0].Code == "E00")
					{
						Console.WriteLine("response postcode " + JsonConvert.SerializeObject(CurrentAddress.Response));
						StreetTextfield.Text = CurrentAddress.Response.Street;
						CityTextfield.Text = CurrentAddress.Response.City;

						Console.WriteLine("address gegevens " + StreetTextfield.Text + " " + CityTextfield.Text);
					}
				}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Gegevens: Postcodeservice", "Exception: "+ ex}
                    });
					Console.WriteLine("exception postcodeservice " + ex);
				}
			}
		}

		public void ErrorCell(UITextField InputText, UILabel Title, UIImageView Icon)
		{
			Title.Frame = new CGRect(50, Title.Frame.Y, Title.Frame.Width, Title.Frame.Height);
			Title.TranslatesAutoresizingMaskIntoConstraints = true;
			Title.TextColor = UIColor.FromRGB(242, 42, 94);
			Icon.Image = UIImage.FromBundle("warning_icon_air");
			Icon.TintColor = UIColor.FromRGB(242, 42, 94);
			Icon.Hidden = false;
			InputText.TextColor = UIColor.FromRGB(242, 42, 94);

			Console.WriteLine("error cell");
		}

		public void ValidCell(UITextField InputText, UILabel Title, UIImageView Icon)
		{
			Title.Frame = new CGRect(15, Title.Frame.Y, Title.Frame.Width, Title.Frame.Height);
			Title.TranslatesAutoresizingMaskIntoConstraints = true;
			Title.TextColor = UIColor.Black;
			Icon.Hidden = true;
			InputText.TextColor = UIColor.Black;
		}

		static void CheckZip(UITextField zip)
		{
			string replace = zip.Text.ToUpper();
			Regex rgx = new Regex("[^0-9A-Z]");
			replace = rgx.Replace(replace, "");
			if (replace.Length > 6)
				replace = replace.Substring(0, 6);
			if (replace.Length >= 6)
				replace = replace.Substring(0, 4) + " " + replace.Substring(4, replace.Length - 4);
			zip.Text = replace;
		}

		void SetAllValid()
		{
			ValidCell(FirstnameTextfield, FirstnameTitle, FirstnameIcon);
			ValidCell(SalutationTextfield, SalutationTitle, SalutationIcon);
			ValidCell(BirthdateTextfield, BirthdateTitle, BirthdateIcon);
			ValidCell(InitialsTextfield, InitialsTitle, InitialsIcon);
			ValidCell(LastnameTextfield, LastnameTitle, LastnameIcon);
			ValidCell(EmailTextfield, EmailTitle, EmailIcon);
			ValidCell(PhoneTextfield, PhoneTitle, PhoneIcon);
			ValidCell(MobileTextfield, MobileTitle, MobileIcon);
			ValidCell(PhoneTextfield, PhoneTitle, PhoneIcon);
			ValidCell(PostalcodeTextfield, PostalTitle, PostalcodeIcon);
			ValidCell(HousenumberTextfield, HousenumberTitle, HousenumberIcon);
			ValidCell(StreetTextfield, StreetTitle, StreetIcon);
			ValidCell(CityTextfield, CityTitle, CityIcon);
		}

		void StartAnimateAvatar()
		{
			// AnimateNotify ( duration, delay, options, animations, completion )
			UIView.AnimateNotify(0.25, 0, UIViewAnimationOptions.CurveLinear, () =>
		   {
			   
			   CGAffineTransform transform = CGAffineTransform.MakeScale(0.75f, 0.75f);
			   AvatarButton.Transform = transform;
		   },
			(finished) =>
			{
				
				AnimateAvatar();
			});
		}

		void AnimateAvatar()
		{
			// AnimateNotify ( duration, delay, spring damping, spring velocity, options, animations, completion )
			UIView.AnimateNotify(0.25, 0, 1, 50, 0, () =>
		   {
			   CGAffineTransform transform = CGAffineTransform.MakeScale(1f, 1f);
			   AvatarButton.Transform = transform;
		   }, null);

		}


		public void PrepareCellsForAnimation()
		{
			for (int z = 0; z < TableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < TableView.NumberOfRowsInSection(z); i++)
				{
					UITableViewCell Cell = TableView.CellAt(NSIndexPath.FromItemSection(i, z)) as UITableViewCell;   
					Console.WriteLine("cell numer " + 1);
					Console.WriteLine("cell " + Cell);
					if (Cell != null)
					{
						Cell.Alpha = 0.0f;
					}
				}
			}
		}

		public void AnimateCells()
		{
			int count = 0;
			for (int z = 0; z < TableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < TableView.NumberOfRowsInSection(z); i++)
				{
					UITableViewCell Cell = TableView.CellAt(NSIndexPath.FromItemSection(i, z)) as UITableViewCell;            
					if (Cell != null)
					{
						count++;
						Cell.Alpha = 1.0f;
						AMStyle.SlideHorizontaly(Cell, true, count * 0.1, true);
					}
				}
			}
		}

		void Refresh()
		{
			var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Persoonlijke gegevens", "Pull to refresh", "Pull to refresh", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
			Gai.SharedInstance.Dispatch();

			UpdateUserInfo(true);
			SetButton(false);
			Initialize();
			SetHeader();
			TableView.ReloadData();
			SetBounceIndicators();
		}

		async void UpdateUserInfo(bool update)
		{
			if (!update)
			{
				SetSpinner();
			}
			try
			{
				LoginService LoginService = new LoginService();
				UserRoot UserResponse = await LoginService.LoginToken(cancelToken);

				//AMMain.UpdateTokenApp(UserResponse.TokenApp.TokenApp);
				Console.WriteLine("UserResponse " + JsonConvert.SerializeObject(UserResponse));

				if (UserResponse.Error.Length > 0)
				{
					
					if (UserResponse.Error[0].Code == "E00")
					{
                        try
						{
							LocalStorage.LocalLogin(UserResponse.User[0], UserResponse.TokenApp, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username));
                            getAvatar(UserResponse.TokenApp, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username));
                            Initialize();
						}
						catch (Exception ex)
						{
							Console.WriteLine("exception local login: " + ex);
						}
					}
					/// <summary>
					/// Login request is succesfull, but has errors (wrong password SAP CODES: P02 AND P03)
					/// </summary>
					else if (UserResponse.Error[0].Code == "E34" || UserResponse.Error[0].Code == "E35")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);


						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

						// Set the routing is user is succesfully loggedin
						Redirect.Routing = "MijnViewController";
						NavigationController.PushViewController(Redirect, false);

						RemoveFromParentViewController();
					}
					/// <summary>
					/// Login request is succesfull, but has errors (user not activated SAP CODE: P04)
					/// </summary>
					else if (UserResponse.Error[0].Code == "E36")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						//AMAlerts.P004Alert(this);

						LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

						// Set the routing is user is succesfully loggedin
						Redirect.Routing = "MijnViewController";
						NavigationController.PushViewController(Redirect, false);

						RemoveFromParentViewController();

					}
					else if (UserResponse.Error[0].Code == "E24")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

						// Set the routing is user is succesfully loggedin
						Redirect.Routing = "MijnViewController";
						NavigationController.PushViewController(Redirect, false);

						RemoveFromParentViewController();
					}

					else if (UserResponse.Error[0].Code == "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Naar de website");
						alert.AddButton("Annuleren");
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();

					}
					else if (UserResponse.Error[0].Code == "E86")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Inloggegevens wijzigen");
						alert.AddButton("Annuleren");
						//alert.Message = "De combinatie van gebruikersnaam en wachtwoord blijft onbekend. Je account is geblokkeerd. Deblokkeren? Klik op inloggevens wijzigen en pas je inloggegevens aan."
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();

					}
					/// <summary>
					/// Login request is succesfull, but does not have any of the above errors
					/// </summary>
					else if (UserResponse.Error[0].Code != "E00" && UserResponse.Error[0].Code != "24" && UserResponse.Error[0].Code != "E34" && UserResponse.Error[0].Code != "E35" && UserResponse.Error[0].Code != "E36" && UserResponse.Error[0].Code != "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						//AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
						LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

						// Set the routing is user is succesfully loggedin
						Redirect.Routing = "MijnViewController";
						NavigationController.PushViewController(Redirect, false);

						RemoveFromParentViewController();
					}

				}
				else
				{
					Console.WriteLine("Niet ingelogd");
					NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

				}
				if (activitySpinner != null)
				{
					activitySpinner.RemoveFromSuperview();
				}
			}
			catch (Exception ex)
			{
				//Console.WriteLine("UserResponse " + UserResponse.Error);
				Console.WriteLine("Niet ingelogd " + ex);
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);
				// Logout
				LocalStorage.Logout(TabBarController);

				if (activitySpinner != null)
				{
					activitySpinner.RemoveFromSuperview();
				}

				AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);

			}
			if (activitySpinner != null)
			{
				activitySpinner.RemoveFromSuperview();
			}
		}

		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset));
                //AMMain.OpenURLInApp(this, AMClient.URLloginReset);
			}
			else
			{

				// Cancel button

			}
		}

        async void getAvatar(ResponseToken Token, string username){
			try
			{
				AvatarService AvatarService = new AvatarService();
				AvatarRoot Avatar = await AvatarService.GetAvatar(Token.TokenApp);


				if (Avatar != null && Avatar.Response != null)
				{
					if (Avatar.Response.Length > 0 && Avatar.Response[0].Image != null)
					{
						NSUserDefaults.StandardUserDefaults.SetString(Avatar.Response[0].Image, username + "-" + AMLocalStorage.Avatar);
                        SetAvatar();
					}
					else
					{
						NSUserDefaults.StandardUserDefaults.RemoveObject(username + "-" + AMLocalStorage.Avatar);
					}
				}
			}
			catch (Exception ex)
			{
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
				}
			}
        }
	}
}