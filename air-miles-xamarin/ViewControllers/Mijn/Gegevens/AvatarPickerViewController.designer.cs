﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("AvatarPickerViewController")]
    partial class AvatarPickerViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView AvatarPickerImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CameraButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView CircleBackgroundImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton GalleryButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PickAvatarButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AvatarPickerImage != null) {
                AvatarPickerImage.Dispose ();
                AvatarPickerImage = null;
            }

            if (CameraButton != null) {
                CameraButton.Dispose ();
                CameraButton = null;
            }

            if (CircleBackgroundImage != null) {
                CircleBackgroundImage.Dispose ();
                CircleBackgroundImage = null;
            }

            if (GalleryButton != null) {
                GalleryButton.Dispose ();
                GalleryButton = null;
            }

            if (PickAvatarButton != null) {
                PickAvatarButton.Dispose ();
                PickAvatarButton = null;
            }
        }
    }
}