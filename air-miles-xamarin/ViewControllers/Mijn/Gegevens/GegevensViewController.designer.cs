﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("GegevensViewController")]
    partial class GegevensViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AvatarButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ProfileAvatar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AvatarButton != null) {
                AvatarButton.Dispose ();
                AvatarButton = null;
            }

            if (ProfileAvatar != null) {
                ProfileAvatar.Dispose ();
                ProfileAvatar = null;
            }
        }
    }
}