﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Linq;
using CoreGraphics;
using System.Threading.Tasks;
using System.Drawing;
using System.Globalization;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
	partial class GegevensViewController : UITableViewController
	{

		LoadingOverlay LoadingView;

		public bool Opslaan = false;
		public bool Invalid = false;
		public bool IsNotNederland;

		UIDatePicker datePickerView;

		public static string Aanhef;
		public static string GeboortedatumString = AMMain.DateToTime(NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UserBirthday)).ToString("d");
		public static string Voornaam = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName);
		public static string Initialen = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserInitials);
		public static string Tussenvoegsel = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix);
		public static string Achternaam = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
		public static nint Geboortedatum = NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UserBirthday);
		public static string Email = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
		public static string Telefoon = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrPrivate);
		public static string Mobiel = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserMobile);
		public static string Straat = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet);
		public static string Huisnummer = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
		public static string HuisnummerToevoeging = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement);
		public static string Plaats = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);
		public static string Postcode = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode);
		public static string Land = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCountry);
		public static bool Newsletter = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Newsletter);
		public static bool AppNotifications = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Notifications);

		                                                                                 
		public string aanhef { get { return Aanhef; } set { Aanhef = value; } }
		public string initialen { get { return Initialen; } set { Initialen = value; } }
		public string voornaam { get { return Voornaam;} set { Voornaam = value; } }
		public string tussenvoegsel { get { return Tussenvoegsel;} set { Tussenvoegsel = value; } }
		public string achternaam { get { return Achternaam;} set { Achternaam = value; } }
		public nint geboortedatum { get { return Geboortedatum;} set { Geboortedatum = value; } }
		public string email { get { return Email;} set { Email = value; } }
		public string telefoon { get { return Telefoon;} set { Telefoon = value; } }
		public string mobiel { get { return Mobiel;} set { Mobiel = value; } }
		public string straat { get { return Straat;} set { Straat = value; } }
		public string huisnummer { get { return Huisnummer;} set { Huisnummer = value; } }
		public string huisnummertoevoeging { get { return HuisnummerToevoeging; } set { HuisnummerToevoeging = value; } }
		public string plaats { get { return Plaats;} set { Plaats = value; } }
		public string postcode { get { return Postcode;} set { Postcode = value; } }
		public string land { get { return Land; } set { Land = value; } }
		public bool newsletter { get { return Newsletter; } set { Newsletter = value; } }


		string errorone;
		string errortwo;
		string errorthree;


		static readonly TableSection[] GegevensTableItems = new[] {
			new TableSection {
				Title = "Profiel",
				Items = new [] {
					new TableRow {
						Title = "Voornaam",
						Detail = "Anton",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Tussenvoegsel",
						Detail = "van der",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Achternaam",
						Detail = "Janssen",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Geboortedatum",
						Detail = "21/04/1989",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			},
			new TableSection {
				Title = "Contactgegevens",
				Items = new [] {
					new TableRow {
						Title = "E-mail",
						Detail = "Anton",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Telefoon",
						Detail = "0612345678",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Mobiel",
						Detail = "0612345678",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					}
				}

			},
			new TableSection {
				Title = "Adres",
				Items = new [] {
					new TableRow {
						Title = "Straat",
						Detail = "Polarisavenue",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer",
						Detail = "28",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Plaats",
						Detail = "Hoofddorp",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Postcode",
						Detail = "2132 JH",
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			}
		};


		public GegevensViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false)
			{
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

				// Set the routing is user is succesfully loggedin
				Redirect.Routing = "MijnViewController";
				NavigationController.PushViewController(Redirect, false);

				RemoveFromParentViewController();


			}
			Initialize();
			RefreshTableItems();
			SetPersonalInfo();

			Console.WriteLine("initiealen "+NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserInitials));

			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				AvatarButton.SetImage(UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))), UIControlState.Normal);

			}
			else {
				AvatarButton.SetImage(UIImage.FromBundle("Airmiles_Placeholder@3x.png"), UIControlState.Normal);
			}

		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetPersonalInfo();

		}

		private void Initialize()
		{
			SetAvatar();

			SetPrefix();

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Persoonlijke gegevens");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			ProfileAvatar.Layer.CornerRadius = ProfileAvatar.Layer.Bounds.Height / 2;

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false)
			{
				TableView.Source = new DefaultSectionHeaderTableViewSource(this, GegevensTableItems.ToArray());

			}
			else
			{
				SetButton(false);
			}


		}


		private void SetAvatar()
		{
			AMMain.SetAvatar(ProfileAvatar, "Airmiles_Placeholder@3x.png");


		}

		public void CreateDatePicker()
		{
			datePickerView = new UIDatePicker(RectangleF.Empty)
			{
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
				Mode = UIDatePickerMode.Date,
				//Hidden = true
			};
			datePickerView.Frame = PickerFrameWithSize((System.Drawing.SizeF)datePickerView.SizeThatFits(SizeF.Empty));
			datePickerView.BackgroundColor = UIColor.White;
			NavigationController.View.AddSubview(datePickerView);
		}

		RectangleF PickerFrameWithSize(SizeF size)
		{
			var screenRect = UIScreen.MainScreen.ApplicationFrame;
			return new RectangleF(0f, (float)(UIScreen.MainScreen.Bounds.Height - size.Height), (float)UIScreen.MainScreen.Bounds.Width, size.Height);
		}



		private void SetPrefix()
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender)))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "male" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "heer")
				{
					Aanhef = "Dhr.";
				}
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "female" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "mevrouw")
				{
					Aanhef = "Mevr.";
				}
			}
		}

		void SetLoader(string text)
		{
			CGRect bounds = new CGRect(0,0, UIScreen.MainScreen.Bounds.Width, 900);

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		void SetPersonalInfo()
		{

			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName)))
			{
				Voornaam = "";
			}
			else
			{
				Voornaam = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix)))
			{
				Tussenvoegsel = "";
			}
			else
			{
				Tussenvoegsel = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName)))
			{
				Achternaam = "";
			}
			else
			{
				Achternaam = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
			}
			if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UserBirthday) == 0)
			{
				GeboortedatumString = "";
			}
			else
			{
				GeboortedatumString = AMMain.DateToTime(NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UserBirthday)).ToString("d");
				Console.WriteLine("geboortedatum string " + GeboortedatumString);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail)))
			{
				Email = "";
			}
			else
			{
				Email = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrPrivate)))
			{
				Telefoon = "";
			}
			else
			{
				Telefoon = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrPrivate);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserMobile)))
			{
				Mobiel = "";
			}
			else
			{
				Mobiel = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserMobile);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet)))
			{
				Straat = "";
			}
			else
			{
				Straat = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber)))
			{
				Huisnummer = "";
			}
			else
			{
				Huisnummer = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement)))
			{
				HuisnummerToevoeging = "";
			}
			else
			{
				HuisnummerToevoeging = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity)))
			{
				Plaats = "";
			}
			else
			{
				Plaats = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);
			}
			if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode)))
			{
				Postcode = "";
			}
			else
			{
				Postcode = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode);
			}

		}


		void ShowAlert()
		{
			AMAlerts.DefaultAlert(this, "Weet je het zeker?", "Wijzigen gaan verloren.");
		}

		public void SetButton(bool opslaan)
		{
			if (opslaan)
			{
				RefreshTableAanpassenItems();
				ProfileAvatar.Hidden = true;
				AvatarButton.Hidden = false;
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
				{
					AvatarButton.SetImage(UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))), UIControlState.Normal);

				}
				else {
					AvatarButton.SetImage(UIImage.FromBundle("Airmiles_Placeholder@3x.png"), UIControlState.Normal);
					AvatarButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Fill;
					AvatarButton.VerticalAlignment = UIControlContentVerticalAlignment.Fill;
				}

				AvatarButton.Layer.CornerRadius = AvatarButton.Bounds.Height / 2;
				AvatarButton.Layer.MasksToBounds = true;

				AvatarButton.TouchUpInside += delegate
				{
					Console.WriteLine("avatarbutton clicked");
					AvatarPickerViewController Redirect = App.Storyboard.InstantiateViewController("AvatarPickerViewController") as AvatarPickerViewController;
					NavigationController.PushViewController(Redirect, true);

				};

				UITextAttributes btnFontAttribute = new UITextAttributes();

				btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
				btnFontAttribute.TextColor = UIColor.White;
				UIBarButtonItem btn = new UIBarButtonItem();
				btn.Title = "Opslaan";
				btn.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
				btn.Clicked += (sendero, e) =>
				{

					if (!Invalid)
					{
						SaveEditProfile();

					}
					else
					{
						AMAlerts.DefaultAlert(this, "Je gegevens zijn nog niet compleet", "De velden met een uitroepteken zijn nog niet correct ingevuld.");
					}


				};
				this.NavigationItem.RightBarButtonItem = btn;

				UITextAttributes btnLeftFontAttribute = new UITextAttributes();

				btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
				btnFontAttribute.TextColor = UIColor.White;
				UIBarButtonItem btnleft = new UIBarButtonItem();
				btnleft.Title = "Annuleren";
				btnleft.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
				btnleft.Clicked += (sendero, e) =>
				{

					CancelProfileAlert("Weet je het zeker?", "Wijzigingen gaan verloren.");


				};
				this.NavigationItem.LeftBarButtonItem = btnleft;

			}
			else {
				RefreshTableItems();
				ProfileAvatar.Hidden = false;
				AvatarButton.Hidden = true;
				UITextAttributes btnFontAttribute = new UITextAttributes();
				btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
				btnFontAttribute.TextColor = UIColor.White;
				UIBarButtonItem btn = new UIBarButtonItem();
				btn.Title = "Wijzig";
				btn.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
				btn.Clicked += (sendero, e) =>
				{
					SetButton(true);
					RefreshTableAanpassenItems();
					Opslaan = true;


				};
				this.NavigationItem.RightBarButtonItem = btn;
				this.NavigationItem.LeftBarButtonItem = null;
			};
		}

		public void RefreshTableItems()
		{

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
			{

				Console.WriteLine("Bounce indicator present");
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
					errorthree = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				SetAvatar();

				TableSectionGegevens[] IngelogdGegevensTableItems = new[] {

				new TableSectionGegevens {
					Title = "Profiel",
					Items = new [] {
						new TableRow {
							Title = "Aanhef",
							Detail = Aanhef,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						},
						new TableRow {
							Title = "Voornaam",
							Detail = Voornaam,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						},
						new TableRow {
							Title = "Voorletters",
							Detail = Initialen,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						},
						new TableRow {
							Title = "Tussenvoegsel",
							Detail = Tussenvoegsel,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						},
						new TableRow {
							Title = "Achternaam",
							Detail = Achternaam,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
						},
						new TableRow {
							Title = "Geboortedatum",
							Detail = GeboortedatumString,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
					}
				},
				new TableSectionGegevens {
					Title = "Contactgegevens",
					ErrorOne = errorone,
					ErrorTwo = errortwo,
					ErrorThree = errorthree,
					Items = new [] {
						new TableRow {
							Title = "E-mail",
							Detail = Email,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Telefoon",
							Detail = Telefoon,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Mobiel",
							Detail = Mobiel,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						}
					}

				},
				new TableSectionGegevens {
					Title = "Adres",
					ErrorOne = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason),
					Items = new [] {
						new TableRow {
							Title = "Land",
							Detail = Land,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Postcode",
							Detail = Postcode,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Huisnummer",
							Detail = Huisnummer,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Huisnummer toevoeging",
							Detail = HuisnummerToevoeging,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Straat",
							Detail = Straat,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},
						new TableRow {
							Title = "Plaats",
							Detail = Plaats,
							CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
						},

					}
				},
					new TableSectionGegevens {
					Title = "Notificaties",
					Items = new [] {
						new TableRow {
							Title = "E-mail nieuwsbrief",
							Icon  = "IconMail",
							Switch = Newsletter,
							CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
						},
						/*new TableRow {
							Title = "App notificaties",
							Icon = "IconNotifications",
							Switch = AppNotifications,
							CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
						}*/
					}
				}


			};

				TableView.Source = new GegevensSectionHeaderTableViewSource(this, IngelogdGegevensTableItems.ToArray());
				TableView.ReloadData();
			}
			else
			{

				Console.WriteLine("Email bouncereason2: " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason));

				TableSection[] IngelogdGegevensTableItems = new[] {

			new TableSection {
				Title = "Profiel",
				Items = new [] {
					new TableRow {
						Title = "Aanhef",
						Detail = Aanhef,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Voornaam",
						Detail = Voornaam,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Voorletters",
						Detail = Initialen,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Tussenvoegsel",
						Detail = Tussenvoegsel,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Achternaam",
						Detail = Achternaam,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Geboortedatum",
						Detail = GeboortedatumString,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			},
			new TableSection {
				Title = "Contactgegevens",
				Items = new [] {
					new TableRow {
						Title = "E-mail",
						Detail = Email,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Telefoon",
						Detail = Telefoon,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Mobiel",
						Detail = Mobiel,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					}
				}

			},
			new TableSection {
				Title = "Adres",
				Items = new [] {
					new TableRow {
						Title = "Land",
						Detail = Land,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Postcode",
						Detail = Postcode,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer",
						Detail = Huisnummer,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer toevoeging",
						Detail = HuisnummerToevoeging,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Straat",
						Detail = Straat,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Plaats",
						Detail = Plaats,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			},
				new TableSection {
				Title = "Notificaties",
				Items = new [] {
					new TableRow {
						Title = "E-mail nieuwsbrief",
						Icon  = "IconMail",
						Switch = Newsletter,
						CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
					},
					/*new TableRow {
						Title = "App notificaties",
						Icon = "IconNotifications",
						Switch = AppNotifications,
						CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
					}*/
				}
			}


		};

				TableView.Source = new DefaultSectionHeaderTableViewSource(this, IngelogdGegevensTableItems.ToArray());
				TableView.ReloadData();

			}
		}

		public void RefreshTableAanpassenItems()
		{
			if (IsNotNederland)
			{
				SetAvatar();
				TableSection[] AanpassenGegevensTableItems = new[] {

				new TableSection {
					Title = "Profiel",
					Items = new [] {
						new TableRow {
							Title = "Aanhef",
							InputText = Aanhef,
							CellIdentifier = CellIdentifier.GenderPickersTableViewCell,
						},
						new TableRow {
							Title = "Voornaam",
							InputText = Voornaam,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
						},
						new TableRow {
							Title = "Voorletters",
							InputText = Initialen,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
						},
						new TableRow {
							Title = "Tussenvoegsel",
							InputText = Tussenvoegsel,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
						},
						new TableRow {
							Title = "Achternaam",
							InputText = Achternaam,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
						},
						new TableRow {
							Title = "Geboortedatum",
							InputText = GeboortedatumString,
							CellIdentifier = CellIdentifier.DatePickerTableViewCell
						},
					}
				},
				new TableSection {
					Title = "Contactgegevens",
					Items = new [] {
						new TableRow {
							Title = "E-mail",
							InputText = Email,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
						new TableRow {
							Title = "Telefoon",
							InputText = Telefoon,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
						new TableRow {
							Title = "Mobiel",
							InputText = Mobiel,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						}
					}

				},
				new TableSection {
					Title = "Adres",
					Items = new [] {
						new TableRow {
							Title = "Land",
							InputText = Land,
							CellIdentifier = CellIdentifier.CountryPickerTableViewCell
						},
						new TableRow {
							Title = "Postcode",
							InputText = Postcode,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
						new TableRow {
							Title = "Huisnummer",
							InputText = Huisnummer,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
						new TableRow {
							Title = "Huisnummer toevoeging",
							InputText = HuisnummerToevoeging,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
						new TableRow {
							Title = "Straat",
							InputText = Straat,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
						new TableRow {
							Title = "Plaats",
							InputText = Plaats,
							CellIdentifier = CellIdentifier.IconTextRightTableViewCell
						},
					}
				},
					new TableSection {
					Title = "Notificaties",
					Items = new [] {
						new TableRow {
							Title = "E-mail nieuwsbrief",
							Switch = Newsletter,
							Icon = "IconMail",
							CellIdentifier = CellIdentifier.IconSwitchTableViewCell
						},
						/*new TableRow {
							Title = "App notificaties",
							Icon = "IconNotifications",
							Switch = AppNotifications,
							CellIdentifier = CellIdentifier.IconSwitchTableViewCell
						}*/
					}
				}
			};

				TableView.Source = new DefaultSectionHeaderTableViewSource(this, AanpassenGegevensTableItems.ToArray());
				TableView.ReloadData();
				SetButton(false);
				Opslaan = false;
			}
			else
			{
				SetAvatar();
				TableSection[] AanpassenGegevensTableItems = new[] {

			new TableSection {
				Title = "Profiel",
				Items = new [] {
					new TableRow {
						Title = "Aanhef",
						InputText = Aanhef,
						CellIdentifier = CellIdentifier.GenderPickersTableViewCell,
					},
					new TableRow {
						Title = "Voornaam",
						InputText = Voornaam,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
					},
					new TableRow {
						Title = "Voorletters",
						InputText = Initialen,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
					},
					new TableRow {
						Title = "Tussenvoegsel",
						InputText = Tussenvoegsel,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
					},
					new TableRow {
						Title = "Achternaam",
						InputText = Achternaam,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell,
					},
					new TableRow {
						Title = "Geboortedatum",
						InputText = GeboortedatumString,
						CellIdentifier = CellIdentifier.DatePickerTableViewCell
					},
				}
			},
			new TableSection {
				Title = "Contactgegevens",
				Items = new [] {
					new TableRow {
						Title = "E-mail",
						InputText = Email,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell
					},
					new TableRow {
						Title = "Telefoon",
						InputText = Telefoon,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell
					},
					new TableRow {
						Title = "Mobiel",
						InputText = Mobiel,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell
					}
				}

			},
			new TableSection {
				Title = "Adres",
				Items = new [] {
					new TableRow {
						Title = "Land",
						InputText = Land,
						CellIdentifier = CellIdentifier.CountryPickerTableViewCell
					},
					new TableRow {
						Title = "Postcode",
						InputText = Postcode,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer",
						InputText = Huisnummer,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer toevoeging",
						InputText = HuisnummerToevoeging,
						CellIdentifier = CellIdentifier.IconTextRightTableViewCell
					},
					new TableRow {
						Title = "Straat",
						Detail = Straat,
						CellIdentifier = CellIdentifier.GreyIconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Plaats",
						Detail = Plaats,
						CellIdentifier = CellIdentifier.GreyIconDetailRightTableViewCell
					},
				}
			},
				new TableSection {
				Title = "Notificaties",
				Items = new [] {
					new TableRow {
						Title = "E-mail nieuwsbrief",
						Switch = Newsletter,
						Icon = "IconMail",
						CellIdentifier = CellIdentifier.IconSwitchTableViewCell
					},
					/*new TableRow {
						Title = "App notificaties",
						Icon = "IconNotifications",
						Switch = AppNotifications,
						CellIdentifier = CellIdentifier.IconSwitchTableViewCell
					}*/
				}
			}
		};

				TableView.Source = new DefaultSectionHeaderTableViewSource(this, AanpassenGegevensTableItems.ToArray());
				TableView.ReloadData();


			}
		}

		public void SaveEditProfile()
		{
			SetLoader(AMStrings.LoadingSaveProfile);
			if (Gegevens.Voornaam != null)
			{
				Voornaam = Gegevens.Voornaam;
			}
			else{
				Voornaam = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName);
			}

			if (Gegevens.Tussenvoegsel != null)
			{
				Tussenvoegsel = Gegevens.Tussenvoegsel;
			}
			else{
				Tussenvoegsel = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix);
			}

			if (Gegevens.Achternaam != null)
			{
				Achternaam = Gegevens.Achternaam;
			}
			else{
				Achternaam = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
			}

			if (Gegevens.Geboortedatum > 0)
			{
				Geboortedatum = Gegevens.Geboortedatum;
			}
			else{
				Geboortedatum = NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UserBirthday);
			}

			if (Gegevens.Email != null)
			{
				Email = Gegevens.Email;
			}
			else{
				Email = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
			}

			if (Gegevens.Telefoon != null)
			{
				Telefoon = Gegevens.Telefoon;
			}
			else{
				Telefoon = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserTelephoneNrPrivate);
			}

			if (Gegevens.Mobiel != null)
			{
				Mobiel = Gegevens.Mobiel;
			}
			else{
				Mobiel = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserMobile);
			}	

			if (Gegevens.Straat != null)
			{
				Straat = Gegevens.Straat;
			}
			else{
				Straat = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet);
			}	

			if (Gegevens.Huisnummer != null)
			{
				Huisnummer = Gegevens.Huisnummer;
			}
			else{
				Huisnummer = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
			}

			if (Gegevens.HuisnummerToevoeging != null)
			{
				HuisnummerToevoeging = Gegevens.HuisnummerToevoeging;
			}
			else{
				HuisnummerToevoeging = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement);
			}

			if (Gegevens.Plaats != null)
			{
				Plaats = Gegevens.Plaats;
			}
			else{
				Plaats = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);
			}

			if (Gegevens.Postcode != null)
			{
				Postcode = Gegevens.Postcode;
			}
			else{
				Postcode = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode);
			}	                                                         

			if (Gegevens.Land != null)
			{
				Land = Gegevens.Land;
			} else {
				Land = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCountry);
			}

			if (Gegevens.Initialen != null)
			{
				Initialen = Gegevens.Initialen;
			}
			else{
				Initialen = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserInitials);
			}
			if (Gegevens.Aanhef != null)
			{
				Aanhef = Gegevens.Aanhef;
			}
			else {
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender)))
				{
					if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "male" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "heer")
					{
						Aanhef = "Heer";
					}
					if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "female" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "mevrouw")
					{
						Aanhef = "Mevrouw";

					}
				}
			}
			Newsletter = Gegevens.Newsletter;
			AppNotifications = Gegevens.AppNotifications;
			SetAvatar();
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureMemberId, true);
			string UserName = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username);


			ChangeProfile(MemberId, Aanhef, Voornaam, Achternaam, Tussenvoegsel, Geboortedatum, Email, Email, Straat, Huisnummer, HuisnummerToevoeging, Plaats, Postcode, Telefoon, Mobiel, Land, Initialen, Newsletter, AppNotifications);
					
		}

		/*private void SaveProfileLocalStorage()
		{
			if (Gegevens.Voornaam != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Voornaam, AMLocalStorage.UserFirstName);
				Console.WriteLine("opgeslagen voornaam = " + Gegevens.Voornaam);
			}
			if (Gegevens.Tussenvoegsel != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Tussenvoegsel, AMLocalStorage.UserPrefix);
			}
			if (Gegevens.Achternaam != null)
			{
			NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Achternaam, AMLocalStorage.UserLastName);
			}
			if (Gegevens.Geboortedatum != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Geboortedatum, AMLocalStorage.UserBirthday);
			}
			if (Gegevens.GeboortedatumFloat > 0)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.GeboortedatumFloat.ToString(), AMLocalStorage.UserBirthdayFloat);

			}
			if (Gegevens.Email != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Email, AMLocalStorage.UserEmail);
			}
			if (Gegevens.Telefoon != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Telefoon, AMLocalStorage.UserTelephoneNrPrivate);
			}
			if (Gegevens.Mobiel != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Mobiel, AMLocalStorage.UserMobile);
			}
			if (Gegevens.Straat != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Straat, AMLocalStorage.UserStreet);
			}
			if (Gegevens.Huisnummer != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Huisnummer, AMLocalStorage.UserHouseNumber);
			}
			if (Gegevens.HuisnummerToevoeging != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.HuisnummerToevoeging, AMLocalStorage.UserHouseNumberSupplement);
			}
			if (Gegevens.Plaats != null){
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Plaats, AMLocalStorage.UserCity);
			}
			if (Gegevens.Postcode != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Postcode, AMLocalStorage.UserPostalCode);
			}
			if (Gegevens.Land != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Land, AMLocalStorage.UserCountry);
			}
			if (Gegevens.Initialen != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(Gegevens.Initialen, AMLocalStorage.UserInitials);
			}
			if (Gegevens.Aanhef != null)
			{
				if (Gegevens.Aanhef == "Heer")
				{
					NSUserDefaults.StandardUserDefaults.SetString("Male", AMLocalStorage.UserGender);
					NSUserDefaults.StandardUserDefaults.SetString("heer", AMLocalStorage.UserSalutation);
				}
				if (Gegevens.Aanhef == "Mevrouw")
				{
					NSUserDefaults.StandardUserDefaults.SetString("Female", AMLocalStorage.UserGender);
					NSUserDefaults.StandardUserDefaults.SetString("mevrouw", AMLocalStorage.UserSalutation);
				}
			}
			NSUserDefaults.StandardUserDefaults.SetBool(Gegevens.Newsletter, AMLocalStorage.Newsletter);
			NSUserDefaults.StandardUserDefaults.SetBool(Gegevens.AppNotifications, AMLocalStorage.Notificattions);
			//AMAlerts.DefaultAlert(this, "Opgeslagen", "Wijzigingen opgeslagen.");
			RefreshTableItems();
			SetButton(false);
			Opslaan = false;

		}*/

		public void CancelProfileAlert(string Title, string Message)
		{
			//Create Alert
			UIAlertController AlertController = UIAlertController.Create(Title, Message, UIAlertControllerStyle.Alert);

			//Add Action
			AlertController.AddAction(UIAlertAction.Create("Ja", UIAlertActionStyle.Default, (UIAlertAction obj) => RefreshTableItemsAndSetBool()));
			AlertController.AddAction(UIAlertAction.Create("Nee", UIAlertActionStyle.Default, null));

			// Present Alert
			this.PresentViewController(AlertController, true, null);
		}

		private void RefreshTableItemsAndSetBool()
		{
			SetButton(false);
			Opslaan = false;
			Invalid = false;

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
			{
				

				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
					errorthree = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
					errortwo = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
				}
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
				{
					errorone = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
				}

				TableSectionGegevens[] IngelogdGegevensTableItems = new[] {

			new TableSectionGegevens {
				Title = "Profiel",
				Items = new [] {
					new TableRow {
						Title = "Aanhef",
						Detail = Aanhef,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Voornaam",
						Detail = Voornaam,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Voorletters",
						Detail = Initialen,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Tussenvoegsel",
						Detail = Tussenvoegsel,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Achternaam",
						Detail = Achternaam,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Geboortedatum",
						Detail = GeboortedatumString,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			},
			new TableSectionGegevens {
				Title = "Contactgegevens",
				ErrorOne = errorone,
				ErrorTwo = errortwo,
				ErrorThree = errorthree,
				Items = new [] {
					new TableRow {
						Title = "E-mail",
						Detail = Email,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Telefoon",
						Detail = Telefoon,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Mobiel",
						Detail = Mobiel,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					}
				}

			},
			new TableSectionGegevens {
				Title = "Adres",
				ErrorOne = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason),
				Items = new [] {
					new TableRow {
						Title = "Land",
						Detail = Land,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},		
					new TableRow {
						Title = "Postcode",
						Detail = Postcode,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer",
						Detail = Huisnummer,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer toevoeging",
						Detail = HuisnummerToevoeging,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Straat",
						Detail = Straat,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Plaats",
						Detail = Plaats,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					
				}
			},
				new TableSectionGegevens {
				Title = "Notificaties",
				Items = new [] {
					new TableRow {
						Title = "E-mail nieuwsbrief",
						Icon  = "IconMail",
						Switch = Newsletter,
						CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
					},
					/*new TableRow {
						Title = "App notificaties",
						Icon = "IconNotifications",
						Switch = AppNotifications,
						CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
					}*/
				}
			}


		};

				TableView.Source = new GegevensSectionHeaderTableViewSource(this, IngelogdGegevensTableItems.ToArray());
				TableView.ReloadData();
			}
			else
			{


			TableSection[] IngelogdGegevensTableItems = new[] {

			new TableSection {
				Title = "Profiel",
				Items = new [] {
					new TableRow {
						Title = "Aanhef",
						Detail = Aanhef,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Voornaam",
						Detail = Voornaam,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Voorletters",
						Detail = Initialen,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Tussenvoegsel",
						Detail = Tussenvoegsel,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Achternaam",
						Detail = Achternaam,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell,
					},
					new TableRow {
						Title = "Geboortedatum",
						Detail = GeboortedatumString,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			},
			new TableSection {
				Title = "Contactgegevens",
				Items = new [] {
					new TableRow {
						Title = "E-mail",
						Detail = Email,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Telefoon",
						Detail = Telefoon,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Mobiel",
						Detail = Mobiel,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					}
				}

			},
			new TableSection {
				Title = "Adres",
				Items = new [] {
					new TableRow {
						Title = "Land",
						Detail = Land,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Postcode",
						Detail = Postcode,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer",
						Detail = Huisnummer,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Huisnummer toevoeging",
						Detail = HuisnummerToevoeging,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Straat",
						Detail = Straat,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
					new TableRow {
						Title = "Plaats",
						Detail = Plaats,
						CellIdentifier = CellIdentifier.IconDetailRightTableViewCell
					},
				}
			},
				new TableSection {
				Title = "Notificaties",
				Items = new [] {
					new TableRow {
						Title = "E-mail nieuwsbrief",
						Icon  = "IconMail",
						Switch = Newsletter,
						CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
					},
					/*new TableRow {
						Title = "App notificaties",
						Icon = "IconNotifications",
						Switch = AppNotifications,
						CellIdentifier = CellIdentifier.StaticIconSwitchTableViewCell
					}*/
				}
			}


		};
				SetAvatar();
				TableView.Source = new DefaultSectionHeaderTableViewSource(this, IngelogdGegevensTableItems.ToArray());
				TableView.ReloadData();
			}
		}


		private async void ChangeProfile(string memberid, string saluation, string firstname, string lastname, string prefix, nint birthdate, string email, string emailconfirm, string street, string housenumber, string housenumbersupplement, string city, string postalcode, string phone, string mobile, string country, string initials, bool newsletter, bool profiling)
		{
			ChangeProfileService ChangeProfileService = new ChangeProfileService();
			GegevensRoot GegevensResponse = await ChangeProfileService.ChangeProfile(memberid, saluation, firstname, lastname, prefix, (int)birthdate, email, emailconfirm, street, housenumber, housenumbersupplement, city, postalcode, phone, mobile, country, initials, newsletter, profiling);

			Console.WriteLine("input aanpassen gegevens: "+ memberid+saluation+firstname+lastname+prefix+birthdate+email+emailconfirm+street+housenumber+housenumbersupplement+city+postalcode+phone+mobile+country+initials);
			try
			{
			if (GegevensResponse.Error.Length > 0)
			{
				Console.WriteLine("Response melding changeprofile: "+ GegevensResponse.Error[0].Code);
				if (GegevensResponse.Error[0].Code != AMStrings.ErrorCode)
				{
					if (GegevensResponse.Error[0].Code == "E00")
					{
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].HouseNumber))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].HouseNumber, AMLocalStorage.UserHouseNumber);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Email))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Email, AMLocalStorage.UserEmail);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].TelephoneNrPrivate))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].TelephoneNrPrivate, AMLocalStorage.UserTelephoneNrPrivate);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].FirstName))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].FirstName, AMLocalStorage.UserFirstName);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].TelephoneNrMobile))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].TelephoneNrMobile, AMLocalStorage.UserTelephoneNrMobile);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].PostalCode))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].PostalCode, AMLocalStorage.UserPostalCode);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].City))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].City, AMLocalStorage.UserCity);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Prefix))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Prefix, AMLocalStorage.UserPrefix);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Initials))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Initials, AMLocalStorage.UserInitials);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Salutation))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Salutation, AMLocalStorage.UserSalutation);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].HouseNumberSuppliment))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].HouseNumberSuppliment, AMLocalStorage.UserHouseNumberSupplement);
							}

							NSUserDefaults.StandardUserDefaults.SetBool(GegevensResponse.Wijziging[0].OptIn, AMLocalStorage.OptIn);

							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].FullName))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].FullName, AMLocalStorage.UserFullName);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Street))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Street, AMLocalStorage.UserStreet);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].Country))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].Country, AMLocalStorage.UserCountry);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].LastName))
							{
							NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].LastName, AMLocalStorage.UserLastName);
							}
							if (!string.IsNullOrEmpty(GegevensResponse.Wijziging[0].BirthDate))
							{
								NSUserDefaults.StandardUserDefaults.SetString(GegevensResponse.Wijziging[0].BirthDate, AMLocalStorage.UserBirthday);
							}

							RefreshTableItemsAndSetBool();
						LoadingView.Hide();
						AMAlerts.DefaultAlert(this, "Opgeslagen", GegevensResponse.Error[0].Message);
					}
					else
					{
						LoadingView.Hide();
						AMAlerts.DefaultAlert(this, "Je gegevens zijn nog niet aangepast", GegevensResponse.Error[0].Message);
						RefreshTableAanpassenItems();
					}

				}
			}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Gegevens: profiel aanpassen", "Exception: "+ ex}
                });
				Console.WriteLine("exception opslaan "+ex);

				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Je gegevens zijn nog niet aangepast");
			}
		}
}
}
