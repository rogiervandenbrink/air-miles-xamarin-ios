﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("TransferViewController")]
    partial class TransferViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TransferAmountLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TransferAmountTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton TransferButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField TransferCerdnumberTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TransferExplenationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TransferTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel WarningLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (TransferAmountLabel != null) {
                TransferAmountLabel.Dispose ();
                TransferAmountLabel = null;
            }

            if (TransferAmountTextfield != null) {
                TransferAmountTextfield.Dispose ();
                TransferAmountTextfield = null;
            }

            if (TransferButton != null) {
                TransferButton.Dispose ();
                TransferButton = null;
            }

            if (TransferCerdnumberTextfield != null) {
                TransferCerdnumberTextfield.Dispose ();
                TransferCerdnumberTextfield = null;
            }

            if (TransferExplenationLabel != null) {
                TransferExplenationLabel.Dispose ();
                TransferExplenationLabel = null;
            }

            if (TransferTitleLabel != null) {
                TransferTitleLabel.Dispose ();
                TransferTitleLabel = null;
            }

            if (WarningLabel != null) {
                WarningLabel.Dispose ();
                WarningLabel = null;
            }
        }
    }
}