﻿using Foundation;
using System;
using UIKit;
using Air_Miles_Xamarin;
using System.Threading;
using CoreGraphics;
using Newtonsoft.Json.Linq;
using Google.Analytics;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class MastercardViewController : UITableViewController
    {
        public MastercardViewController (IntPtr handle) : base (handle)
        {
        }

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		// Network status
		NetworkStatus Internet;

		LoadingOverlay LoadingView;

		//refreshlayout
		UIRefreshControl RefreshControl;
		UIActivityIndicatorView activitySpinner;

		public override void ViewDidLoad()
		{
			Initialize();

			MastercardButton.TouchUpInside += delegate
			{
                MastercardnumberTextfield.ResignFirstResponder();

				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TruncatedPan)))
				{
					DisconnectMastercard();
				}
				else
				{
					string cardNumber = MastercardnumberTextfield.Text;
					cardNumber = Regex.Replace(cardNumber, @"[^\d]", string.Empty);

                    if (string.IsNullOrEmpty(cardNumber) || cardNumber.Length != 16)
                    {
                        AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorMasterCard);
                    }
                    else if (!SharedMain.PersonalMastercard(cardNumber)) 
                    {
                        AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorMasterCard);
                    }
					else
					{
						GetSetupToken(cardNumber);
					}
				}
			};

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Koppelen Mastercard");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(0, 125, 195);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
			}
			else
			{
				// old style refresh button
				NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Refresh), false);
				NavigationItem.RightBarButtonItem.Clicked += (sender, e) => { Refresh(); };
			}
			TableView.AddSubview(RefreshControl);

			base.ViewDidLoad();

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			
            SetButton();
		}

		public void Initialize()
		{
			AMStyle.RoundedButton(MastercardButton);
			AMStyle.BorderedTextfield(MastercardnumberTextfield);
			AMStyle.BorderedGreyLabel(CardNumberLabel);
			MastercardExplenationLabel.Font = UIFont.FromName("avenir-book", 14f);
			MastercardnumberTextfield.KeyboardType = UIKeyboardType.NumberPad;
			MastercardnumberTextfield.Font = UIFont.FromName("Avenir-Black", 12f);
			AMMain.AddDoneButton(MastercardnumberTextfield);
			MastercardTitleLabel.TextColor = UIColor.White;
			MastercardTitleLabel.AdjustsFontSizeToFitWidth = true;

		}

        private void HideBackButton()
        {
            NavigationItem.SetHidesBackButton(true, false);
        }

        void SetButton () {
            if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TruncatedPan)))
            {
                CardNumberLabel.Hidden = false;
                MastercardnumberTextfield.Hidden = true;
                CardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TruncatedPan);
                MastercardButton.SetTitle("MASTERCARD ONTKOPPELEN", UIControlState.Normal);
            }
            else
            {
                CardNumberLabel.Hidden = true;
                MastercardnumberTextfield.Hidden = false;
                MastercardButton.SetTitle("MASTERCARD KOPPELEN", UIControlState.Normal);
            }
        }

		async void GetSetupToken(string cardNumber)
		{
			SetLoader("Mastercard koppelen...");

			Internet = Reachability.InternetConnectionStatus();


			// If internet
			if (Internet != NetworkStatus.NotReachable)
			{
				try
				{

					MastercardService MastercardService = new MastercardService();
					GingerKeyRoot GingerKey = await MastercardService.GetGingerKey(cancelToken);

					//string response = await SharedWebServices.GingerSetupToken(SharedMain.DecodeFromBase64(GingerKey.Response.Key), cancelToken);
					//Console.WriteLine("ginger key "+GingerKey.Response.Key);
					//string response = await SharedWebServices.GingerSetupToken("NGU0ZGEyN2EzZTIyNDM4M2FlOWQzNGIyY2UxMTRlN2U6", cancelToken);
					Console.WriteLine(JsonConvert.SerializeObject(GingerKey));
					//todo : if errorcode E00
					if (GingerKey.Error[0].Code == "E00")
					{
						PostCardNumber(GingerKey.Response.Key, cardNumber);
						return;
					}
					else
					{
						AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);

					}
					}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Mastercard", "Exception: "+ ex}
                    });
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				}
			}
			// if not show a message and get the localstorage
			else
			{
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorLoginNoConnection);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		async void PostCardNumber(string setupToken, string cardNumber)
		{
			Internet = Reachability.InternetConnectionStatus();


			// Analytics event
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen Mastercard", "Opslaan Mastercard nummer", "Opslaan Mastercard nummer", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			// If internet
			//if (Internet != NetworkStatus.NotReachable)
			//{
				try
				{
					MastercardService MastercardService = new MastercardService();
					MastercardRoot Mastercard = await MastercardService.PostMastercardNumber(setupToken, cardNumber, cancelToken);

					if (Mastercard == null)
					{
						AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
					}
					else
					{
						string panToken = Mastercard.Token;
						string truncatedPan = Mastercard.TruncatedPan;

						StoreTokenRoot StoreToken = await MastercardService.StoreToken(truncatedPan, panToken, cancelToken);

						//AMAlerts.DefaultAlert(this, "Foutmelding", StoreToken.Error[0].Code);
						if (StoreToken.Error[0].Code == "E00")
						{
						
							Console.WriteLine("StoreToken.Response.Truncated_PAN " + StoreToken.Response.Truncated_PAN);
							if (!string.IsNullOrEmpty(StoreToken.Response.Truncated_PAN))
							{
								CardNumberLabel.Text = "   " + StoreToken.Response.Truncated_PAN;
							}
							//MastercardnumberTextfield.Text = StoreToken.Response.Truncated_PAN;
							CardNumberLabel.Hidden = false;
							MastercardnumberTextfield.Hidden = true;
							MastercardButton.SetTitle("MASTERCARD ONTKOPPELEN", UIControlState.Normal);
							NSUserDefaults.StandardUserDefaults.SetString(StoreToken.Response.Truncated_PAN, AMLocalStorage.TruncatedPan);
							AMAlerts.DefaultAlert(this, "Mastercard", "Je Mastercard kaartnummer is nu gekoppeld!");
						}
						else
						{
							AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, StoreToken.Error[0].Message);
						}
						

					}
				}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Mastercard", "Exception: "+ ex}
                    });
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorMasterCard);

				}
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
			//}
			// if not show a message and get the localstorage
			//else
			//{
			//	AMAlerts.DefaultAlert(this, "Foutmelding", AMStrings.InternetNoConnectionMessage);
			//}

			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		async void DisconnectMastercard()
		{
			SetLoader("Mastercard ontkoppelen...");
			Internet = Reachability.InternetConnectionStatus();


			// Analytics event
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Koppelen Mastercard", "Ontkoppelen Mastercard nummer", "Ontkoppelen Mastercard nummer", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			// If internet
			//if (Internet != NetworkStatus.NotReachable)
			//{
			try
			{

				MastercardService MastercardService = new MastercardService();
				StoreTokenRoot StoreToken = await MastercardService.StoreToken(string.Empty, "E7Oi45ZTSm", cancelToken);

				//AMAlerts.DefaultAlert(this, "Foutmelding", StoreToken.Error[0].Code);
				if (StoreToken.Error[0].Code == "E00")
				{
					
					MastercardnumberTextfield.Text = string.Empty;
					MastercardnumberTextfield.Hidden = false;
					CardNumberLabel.Hidden = true;
					MastercardButton.SetTitle("MASTERCARD KOPPELEN", UIControlState.Normal);
					NSUserDefaults.StandardUserDefaults.SetString(string.Empty, AMLocalStorage.TruncatedPan);
					AMAlerts.DefaultAlert(this, "Mastercard", "Je Mastercard kaartnummer is nu ontkoppeld!");
				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, StoreToken.Error[0].Message);
				}



			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Mastercard", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorMasterCard);

			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
			//}
			// if not show a message and get the localstorage
			//else
			//{
			//	AMAlerts.DefaultAlert(this, "Foutmelding", AMStrings.InternetNoConnectionMessage);
			//}

			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		async void Refresh()
		{
			
			try
			{
				LoginService LoginService = new LoginService();
				UserRoot UserResponse = await LoginService.LoginToken(cancelToken);

				//AMMain.UpdateTokenApp(UserResponse.TokenApp.TokenApp);
				Console.WriteLine("UserResponse " + JsonConvert.SerializeObject(UserResponse));

				if (UserResponse.Error.Length > 0)
				{

					if (UserResponse.Error[0].Code == "E00")
					{
						try
						{
							LocalStorage.LocalLogin(UserResponse.User[0], UserResponse.TokenApp, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username));
                            CardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TruncatedPan);
                            SetButton();

						}
						catch (Exception ex)
						{
							Console.WriteLine("exception local login: " + ex);
						}
					}
					/// <summary>
					/// Login request is succesfull, but has errors (wrong password SAP CODES: P02 AND P03)
					/// </summary>
					if (UserResponse.Error[0].Code == "E34" || UserResponse.Error[0].Code == "E35")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);


						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);


					}
					/// <summary>
					/// Login request is succesfull, but has errors (user not activated SAP CODE: P04)
					/// </summary>
					if (UserResponse.Error[0].Code == "E36")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						//AMAlerts.P004Alert(this);

					}
					if (UserResponse.Error[0].Code == "E24")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

					}

					if (UserResponse.Error[0].Code == "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Naar de website");
						alert.AddButton("Annuleren");
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();

					}
					if (UserResponse.Error[0].Code == "E86")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Inloggegevens wijzigen");
						alert.AddButton("Annuleren");
						//alert.Message = "De combinatie van gebruikersnaam en wachtwoord blijft onbekend. Je account is geblokkeerd. Deblokkeren? Klik op inloggevens wijzigen en pas je inloggegevens aan."
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();

					}
					/// <summary>
					/// Login request is succesfull, but does not have any of the above errors
					/// </summary>
					if (UserResponse.Error[0].Code != "E00" && UserResponse.Error[0].Code != "24" && UserResponse.Error[0].Code != "E34" && UserResponse.Error[0].Code != "E35" && UserResponse.Error[0].Code != "E36" && UserResponse.Error[0].Code != "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						//AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);

					}

				}
				else
				{
					Console.WriteLine("Niet ingelogd");
					NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

				}

			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Mastercard", "Exception: "+ ex}
                });
				Console.WriteLine("Niet ingelogd " + ex);
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);
				// Logout
				LocalStorage.Logout(TabBarController);

				AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);

			}
			RefreshControl.EndRefreshing();
		}

		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset));
                //AMMain.OpenURLInApp(this, AMClient.URLloginReset);
			}
			else
			{

				// Cancel button

			}
		}

		void SetSpinner()
		{
			// derive the center x and y
			nfloat centerX = UIScreen.MainScreen.Bounds.Width / 2;
			nfloat centerY = UIScreen.MainScreen.Bounds.Height / 2;

			// create the activity spinner, center it horizontall and put it 5 points above center x
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activitySpinner.Frame = new CGRect(
				centerX - (activitySpinner.Frame.Width / 2),
				centerY - activitySpinner.Frame.Height - 20,
				activitySpinner.Frame.Width,
				activitySpinner.Frame.Height);
			activitySpinner.AutoresizingMask = UIViewAutoresizing.All;
			Add(activitySpinner);
			activitySpinner.StartAnimating();
		}
    }
}