﻿using Foundation;
using System;
using UIKit;
using System.Threading;
using CoreGraphics;
using Google.Analytics;
using Air_Miles_Xamarin;
using Newtonsoft.Json;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class BlockCardViewController : UITableViewController
    {
        public BlockCardViewController (IntPtr handle) : base (handle)
        {
        }

        readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

        LoadingOverlay LoadingView;
        public bool PhysicalCard { get; set; }
        public bool DigitalCard { get; set; }

        // Network status
        NetworkStatus Internet;

        public override void ViewDidLoad()
        {
            Initialize();

            BlockcardButton.TouchUpInside += delegate
            {
                if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.DigitalAccount))
                {
                    AskReplacecardDigital();
                }
                else
                {
                    //kaart betalen
                    //ShowExtraCheckMessage();
                    AskReplacecardNormal();
                }
            };

            base.ViewDidLoad();

            // Analytics Screenname
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Kaart blokkeren");
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
        }

        public void Initialize()
        {
            AMStyle.RoundedButton(BlockcardButton);
            AMStyle.BorderedGreyLabel(BlockcardNumberLabel);
            BlockcardExplenationLabel.Font = UIFont.FromName("Avenir-Book", 14f);
            //old text for release 2.3
            BlockcardExplenationLabel.Text = AMStrings.BlockCardExplanation;

            BlockcardNumberLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
            BlockcardNumberLabel.Font = UIFont.FromName("Avenir-Book", 17f);
            PhysicalCardLabel.Font = UIFont.FromName("Avenir-Book", 14f);
            AMStyle.BorderedBlueSegmentedControl(SegmentedControl);
            SegmentedControl.SetWidth((UIScreen.MainScreen.Bounds.Width - 30) / 2, 1);
            SegmentedControl.SetWidth((UIScreen.MainScreen.Bounds.Width - 30) / 2, 0);
            SegmentedControl.SelectedSegment = -1;
            SegmentedControl.ValueChanged += (sender, e) =>
            {
                var selectedSegmentId = (sender as UISegmentedControl).SelectedSegment;
                // do something with selectedSegmentId
                if(selectedSegmentId == 0 ){
                    DigitalCard = true;
                    PhysicalCard = false;
                    BlockcardButton.SetTitle("BLOKKERING BEVESTIGEN", UIControlState.Normal);
                    TableView.ReloadData();

                    Console.WriteLine("digital");
                }else{
                    PhysicalCard = true;
                    DigitalCard = false;
                    BlockcardButton.SetTitle("OPEN WEBSITE", UIControlState.Normal);
                    TableView.ReloadData();

                    Console.WriteLine("physical");
                }
            };
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            // cancel the webservice via tok
            if (cancelToken.Token.CanBeCanceled)
                cancelToken.Cancel();

        }

        private void ShowExtraCheckMessage()
        {
            UIAlertView alert = new UIAlertView();
            alert.Title = "Weet je het zeker?";
            alert.AddButton("Blokkeren");
            alert.AddButton("Annuleren");
            alert.Message = AMStrings.BlockCardExtraMessage;
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonToContinueClicked;
            alert.Show();
        }

        private void CheckAMBalance()
        {
            string Balance = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance);
            int AMBalance;
            if (Int32.TryParse(Balance, out AMBalance))
            {
                Console.WriteLine("AMBalance is " + AMBalance);
                if (AMBalance < 250)
                {
                    UIAlertView alert = new UIAlertView();
                    alert.Title = "Kaart blokkeren";
                    alert.AddButton("Naar site");
                    alert.AddButton("Annuleren");
                    alert.Message = AMStrings.ReplaceCardNotEnoughMessage;
                    alert.AlertViewStyle = UIAlertViewStyle.Default;
                    alert.Clicked += ButtonToSiteClicked;
                    alert.Show();
                }
                else
                {
                    AskReplacecardNormal();
                }
            }
            else
            {
                Console.WriteLine("AMBalance could not be parsed");
            }

        }

        private async void Replacecard()
        {
            SetLoader(AMStrings.LoadingBlockcard);
            TableView.SetContentOffset(new CGPoint(0, 0), false);

            // Analytics event
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart blokkeren", "Blokkering aanvragen", "Blokkering aanvragen", null).Build();
            Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
            Gai.SharedInstance.Dispatch();

            try
            {

                BlockcardService BlockcardService = new BlockcardService();
                BlockcardRoot Blockcard = await BlockcardService.BlockCard(cancelToken);

                if (Blockcard.Error[0].Code == "E00")
                {
                    AMAlerts.DefaultAlert(this, "Blokkeren", AMMain.RemoveHTMLTags(Blockcard.Error[0].Message) + "Je nieuwe kaartnummer is: "+Blockcard.Response.NewCardNumber);
                    NSUserDefaults.StandardUserDefaults.SetString(Blockcard.Response.NewCardNumber, AMLocalStorage.CardNumber);
                    NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
                    shared.SetString(Blockcard.Response.NewCardNumber, AMLocalStorage.CardNumber);
                    shared.Synchronize();
                    BlockcardNumberLabel.Text = "   " + Blockcard.Response.NewCardNumber;
                }
                else
                {
                    AMAlerts.DefaultAlert(this, "Blokkeren", AMMain.RemoveHTMLTags(Blockcard.Error[0].Message));
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart blokkeren", "Exception: "+ ex}
                });
                Console.WriteLine("exception block card: "+ex);
                AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
            }
            if (LoadingView != null)
            {
                LoadingView.Hide();
            }
        }

        private void AskReplacecardNormal()
        {
            string address = "";
            if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement)))
            {
                address = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement) + ", " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);
            }
            else
            {
                address = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber) + ", " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);

            }
            UIAlertView alert = new UIAlertView();
            alert.Title = "Kaart blokkeren";
            alert.AddButton("Blokkeren");
            alert.AddButton("Annuleren");
            alert.Message = "Je Air Miles kaart wordt geblokkeerd en er wordt een nieuwe kaart verzonden naar: " + address + " . Klopt dit? Zo niet, klik op annuleren en wijzig je adres via ‘Gegevens’. Je nieuwe kaartnummer is ook direct beschikbaar in deze app.";
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonClicked;
            alert.Show();
        }

        void ButtonClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                Replacecard();
            }
            else
            {
                // Cancel button
            }
        }

        private void AskReplacecardPhysical()
        {
        
            UIAlertView alert = new UIAlertView();
            alert.Title = "Kaart blokkeren";
            alert.AddButton("Blokkeren");
            alert.AddButton("Annuleren");
            alert.Message = AMStrings.BlockCardPhysicalMessage;
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonClickedPhysical;
            alert.Show();
        }

        void ButtonClickedPhysical(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                //ReplaceCard();
                //AMMain.OpenURLInApp(this, AMClient.URLKaartBlokkeren);
                UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLKaartBlokkeren));

                //Analytics Screenname
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Open website", "Kaart blokkeren", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();
            }
            else
            {
                // Cancel button
            }
        }

        void ButtonToSiteClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLReplaceCard));

                //Analytics Screenname
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Open website", "Kaart vervangen", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();
            }
            else
            {
                // Cancel button
            }
        }

        void ButtonToContinueClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                CheckAMBalance();
            }
            else
            {
                // Cancel button
            }
        }

        private void AskReplacecardDigital()
        {
            UIAlertView alert = new UIAlertView();
            alert.Title = "Kaart blokkeren";
            alert.AddButton("Blokkeren");
            alert.AddButton("Annuleren");
            alert.Message = "Je Air Miles kaart wordt geblokkeerd en er wordt een nieuw kaartnummer voor je aangemaakt. Dit kaartnummer is direct beschikbaar in deze app en krijg je ook toegestuurd per e-mail.";
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonDigitalClicked;
            alert.Show();
        }

        void ButtonDigitalClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                Replacecard();
            }
            else
            {
                // Cancel button
            }
        }

        private async void UpdateBalance()
        {

            try
            {

                Internet = Reachability.InternetConnectionStatus();

                // If internet
                if (Internet != NetworkStatus.NotReachable)
                {
                    KaartService KaartService = new KaartService();
                    KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

                    if (CurrentKaart.Error[0].Code == "E00")
                    {
                        Console.WriteLine("Updating Card data");
                        Console.WriteLine("response balance " + JsonConvert.SerializeObject(CurrentKaart));
                        string service = AMLocalStorage.KaartService;
                        NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
                        LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
                        NSUserDefaults.StandardUserDefaults.Synchronize();
                    }
                }
                else
                {
                    //SetNetworkView();
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart blokkeren getbalance", "Exception: "+ ex}
                });
                //SetNetworkView()
                if (AMMain.CanShowError())
                {
                    AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
                }
            }
            TableView.ReloadData();
        }

        private void SetLoader(string text)
        {
            CGRect bounds = UIScreen.MainScreen.Bounds;
            LoadingView = new LoadingOverlay(bounds, text);
            View.Add(LoadingView);
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            nfloat height = 45;

            if (indexPath.Row == 0)
            {

                height = 180.0f;
            }
            //for choice betwee digital and physical
            if (indexPath.Row == 3)
            {
                height = 0.0f;
            }
            if (indexPath.Row == 4)
            {
                height = 0.0f;
            }
            if (indexPath.Row == 5)
            {
                height = 0.0f;
            }
            if (indexPath.Row == 7)
            {
                height = 15.0f;
            }
            if(PhysicalCard){
                if (indexPath.Row == 5)
                {
                    height = 0.0f;
                }
                if (indexPath.Row == 6)
                {
                    height = 45.0f;
                }
            }
            if (DigitalCard)
            {
                if (indexPath.Row == 5)
                {
                    height = 0.0f;
                }
                if (indexPath.Row == 6)
                {
                    height = 0.0f;
                }
            }
            if(!DigitalCard && !PhysicalCard)
            {
                if (indexPath.Row == 5)
                {
                    height = 0.0f;
                }
                if (indexPath.Row == 6)
                {
                    height = 0.0f;
                }
            }

            return height;
        }
    }
}