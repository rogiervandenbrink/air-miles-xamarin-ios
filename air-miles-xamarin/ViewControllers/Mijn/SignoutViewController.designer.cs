﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("SignoutViewController")]
    partial class SignoutViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ConditionsSignoutLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton DonateSignoutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PrivacyLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SignoutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel SignoutExplenationLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton TransferSignoutButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel WarningLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CardLabel != null) {
                CardLabel.Dispose ();
                CardLabel = null;
            }

            if (CardTitleLabel != null) {
                CardTitleLabel.Dispose ();
                CardTitleLabel = null;
            }

            if (ConditionsSignoutLabel != null) {
                ConditionsSignoutLabel.Dispose ();
                ConditionsSignoutLabel = null;
            }

            if (DonateSignoutButton != null) {
                DonateSignoutButton.Dispose ();
                DonateSignoutButton = null;
            }

            if (PrivacyLabel != null) {
                PrivacyLabel.Dispose ();
                PrivacyLabel = null;
            }

            if (SignoutButton != null) {
                SignoutButton.Dispose ();
                SignoutButton = null;
            }

            if (SignoutExplenationLabel != null) {
                SignoutExplenationLabel.Dispose ();
                SignoutExplenationLabel = null;
            }

            if (TransferSignoutButton != null) {
                TransferSignoutButton.Dispose ();
                TransferSignoutButton = null;
            }

            if (WarningLabel != null) {
                WarningLabel.Dispose ();
                WarningLabel = null;
            }
        }
    }
}