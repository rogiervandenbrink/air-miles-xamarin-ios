﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using System.Threading;
using Google.Analytics;
using Xamarin;
using Air_Miles_Xamarin;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class ReplaceCardViewController : UITableViewController
    {
        public ReplaceCardViewController (IntPtr handle) : base (handle)
        {
        }

        readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

        LoadingOverlay LoadingView;

        // Network status
        NetworkStatus Internet;

        public override void ViewDidLoad()
        {
            Initialize();

            ReplacecardButton.TouchUpInside += delegate
            {
                if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.DigitalAccount))
                {
                    AskReplacecardDigital();
                }else
                {
                    //tes
                    //ShowExtraCheckMessage();
                    AskReplacecardNormal();
                }
            };

            base.ViewDidLoad();

            // Analytics Screenname
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Kaart vervangen");
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
        }

        public void Initialize()
        {
            AMStyle.RoundedButton(ReplacecardButton);
            AMStyle.BorderedGreyLabel(ReplacecardLabel);
            ReplacecardLabel.Text = "   " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
            ReplacecardLabel.Font = UIFont.FromName("avenir-book", 17f);
            ReplacecardExplenationLabel.Font = UIFont.FromName("avenir-book", 14f);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            // cancel the webservice via tok
            if (cancelToken.Token.CanBeCanceled)
                cancelToken.Cancel();

        }

        private async void ReplaceCard()
        {
            SetLoader(AMStrings.LoadingReplacecard);
            TableView.SetContentOffset(new CGPoint(0, 0), false);
            // Analytics event
            var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart vervangen", "Vervanging aanvragen", "Vervanging aanvragen", null).Build();
            Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
            Gai.SharedInstance.Dispatch();

            try
            {

                ReplacecardService ReplacecardService = new ReplacecardService();
                ReplacecardRoot Replacecard = await ReplacecardService.ReplaceCard(cancelToken);

                if (Replacecard.Error[0].Code == "E00")
                {
                    AMAlerts.DefaultAlert(this, "Vervangen", AMMain.RemoveHTMLTags(Replacecard.Response.Message));
                    NSUserDefaults.StandardUserDefaults.SetString(Replacecard.Response.NewCardNumber, AMLocalStorage.CardNumber);
                    NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
                    shared.SetString(Replacecard.Response.NewCardNumber, AMLocalStorage.CardNumber);
                    shared.Synchronize();
                    UpdateBalance();
                }
                else {

                    AMAlerts.DefaultAlert(this, "Vervangen", AMMain.RemoveHTMLTags(Replacecard.Error[0].Message));

                }
                }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart vervangen", "Exception: "+ ex}
                });
            }
            if (LoadingView != null)
            {
                LoadingView.Hide();
            }
        }

        private void ShowExtraCheckMessage()
        {
            UIAlertView alert = new UIAlertView();
            alert.Title = "Weet je het zeker?";
            alert.AddButton("Vervangen");
            alert.AddButton("Annuleren");
            alert.Message = AMStrings.ReplaceCardExtraMessage;
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonToContinueClicked;
            alert.Show();
        }

        private void CheckAMBalance() {
            string Balance = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance);
            int AMBalance;
            if (Int32.TryParse(Balance, out AMBalance))
            {
                Console.WriteLine("AMBalance is " + AMBalance);
                if (AMBalance < 250)
                {
                    UIAlertView alert = new UIAlertView();
                    alert.Title = "Kaart vervangen";
                    alert.AddButton("Naar site");
                    alert.AddButton("Annuleren");
                    alert.Message = AMStrings.ReplaceCardNotEnoughMessage;
                    alert.AlertViewStyle = UIAlertViewStyle.Default;
                    alert.Clicked += ButtonToSiteClicked;
                    alert.Show();
                }
                else
                {
                    AskReplacecardNormal();
                }
            }
            else
            {
                Console.WriteLine("AMBalance could not be parsed");
            }

        }

        private void AskReplacecardNormal()
        {
            string address = "";
            if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement)))
            {
                address = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement) +", "+ NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode) + " "+ NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);
            }else
            {
                address = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber) + ", " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode) + " " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);

            }
            UIAlertView alert = new UIAlertView();
            alert.Title = "Kaart vervangen";
            alert.AddButton("Vervangen");
            alert.AddButton("Annuleren");
            alert.Message = "Er wordt een nieuwe kaart verzonden naar: "+ address +  " . Klopt dit? Zo niet, klik op annuleren en wijzig je adres via ‘Gegevens’.";
            //alert.Message = AMStrings.ReplaceCardMessage;
            alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += ButtonClicked;
            alert.Show();
        }

        void ButtonClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                ReplaceCard();
               
            }
            else
            {

                // Cancel button

            }
        }

        void ButtonToSiteClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLReplaceCard));

                //Analytics Screenname
                var GAItabkeuze = DictionaryBuilder.CreateEvent("Mijn", "Open website", "Kaart vervangen", null).Build();
                Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                Gai.SharedInstance.Dispatch();
            }
            else
            {

                // Cancel button

            }
        }

        void ButtonToContinueClicked(object sender, UIButtonEventArgs e)
        {
            UIAlertView parent_alert = (UIAlertView)sender;

            if (e.ButtonIndex == 0)
            {
                CheckAMBalance();

            }
            else
            {

                // Cancel button

            }
        }

        private void AskReplacecardDigital()
        {

            AMAlerts.DefaultAlert(this, "Vervangen", "Je hebt een digitaal account, vervangen is daarom niet mogelijk. Heb je vragen? Neem contact op met onze klantenservice.");
        }


        private void SetLoader(string text)
        {
            CGRect bounds = UIScreen.MainScreen.Bounds;
            LoadingView = new LoadingOverlay(bounds, text);
            View.Add(LoadingView);
        }

        private async void UpdateBalance()
        {

            try
            {

                Internet = Reachability.InternetConnectionStatus();

                // If internet
                if (Internet != NetworkStatus.NotReachable)
                {
                    KaartService KaartService = new KaartService();
                    KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

                    if (CurrentKaart.Error[0].Code == "E00")
                    {
                        Console.WriteLine("Updating Card data");
                        Console.WriteLine("response balance " + JsonConvert.SerializeObject(CurrentKaart));
                        string service = AMLocalStorage.KaartService;
                        NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
                        LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
                        NSUserDefaults.StandardUserDefaults.Synchronize();
                    }
                }
                else
                {
                    //SetNetworkView();
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart vervangen", "Exception: "+ ex}
                });

                if (AMMain.CanShowError())
                {
                    AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
                }
            }
            TableView.ReloadData();
        }
    }
}