﻿using Foundation;
using System;
using UIKit;
using System.Threading;
using CoreGraphics;
using Google.Analytics;
using Xamarin;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class TransferViewController : UITableViewController
    {
        public TransferViewController (IntPtr handle) : base (handle)
        {
        }

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		LoadingOverlay LoadingView;

		// Network status
		NetworkStatus Internet;


		public override void ViewDidLoad()
		{
			Initialize();

			TransferButton.TouchUpInside += delegate {
				if (string.IsNullOrEmpty(TransferCerdnumberTextfield.Text) || TransferCerdnumberTextfield.Text.Length != 9)
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Geen geldig kaartnummer ingevuld.");
				}
				if (string.IsNullOrEmpty(TransferAmountTextfield.Text))
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Het aantal Air Miles is niet ingevuld.");
				}
				else
				{
					Transfer();
					TransferAmountTextfield.ResignFirstResponder();
				}
			};

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Overboeken");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			base.ViewDidLoad();

		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 44.0f;
			if (indexPath.Row == 1)
			{
				if (UIScreen.MainScreen.Bounds.Width < 320)
				{
			
					return 160;
				}
				else
				{
					return 120;
				}
			}
			if (indexPath.Row == 7)
			{
				return 15;
			}
			else
			{
				return height;
			}

		}

		public void Initialize()
		{
			AMStyle.RoundedButton(TransferButton);
			AMStyle.BorderedTextfield(TransferCerdnumberTextfield);
			AMStyle.BorderedTextfield(TransferAmountTextfield);
			TransferExplenationLabel.Font = UIFont.FromName("avenir-book", 14f);
			TransferCerdnumberTextfield.KeyboardType = UIKeyboardType.NumberPad;
			TransferAmountTextfield.KeyboardType = UIKeyboardType.NumberPad;
			TransferCerdnumberTextfield.Font = UIFont.FromName("Avenir-Black", 12f);
			TransferAmountTextfield.Font = UIFont.FromName("Avenir-Black", 12f);
			AMMain.AddNextButton(TransferCerdnumberTextfield, TransferAmountTextfield);
			AMMain.AddDoneButton(TransferAmountTextfield);
			TransferTitleLabel.TextColor = UIColor.White;
          	TransferTitleLabel.AdjustsFontSizeToFitWidth = true;
			TransferAmountLabel.AdjustsFontSizeToFitWidth = true;

			string balance = "";
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance)))
			{
				balance = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance);
			}
			else
			{
				balance = "0";
			}
			WarningLabel.Text = "Je hebt " + AMMain.SetBalanceFormat(balance) + " Air Miles";

			Console.WriteLine("Tag textfield amount "+TransferAmountTextfield.Tag);
			Console.WriteLine("View with tag 2 "+ this.View.ViewWithTag(2));
		}

		private async void Transfer()
		{
			SetLoader(AMStrings.LoadingTransfer);
			TableView.SetContentOffset(new CGPoint(0,0), false);
			string receiverCardNr = TransferCerdnumberTextfield.Text;
			string amount = TransferAmountTextfield.Text;

			// Analytics event
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Overboeken", "Overboekeing overmaken", TransferAmountTextfield.Text, null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			try
			{
				TransferService TransferService = new TransferService();
				TransferRoot Transfer = await TransferService.Transfer(receiverCardNr, amount, cancelToken);

    				if (Transfer.Error[0].Code == "E00")
    				{
    					AMAlerts.DefaultAlert(this, "Geslaagd!", AMMain.RemoveHTMLTags(Transfer.Response.PopupMessage));
    					UpdateBalance();
    					TransferCerdnumberTextfield.Text = string.Empty;
    					TransferAmountTextfield.Text = string.Empty;

    				}
    				else
    				{
    					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(Transfer.Error[0].Message));

    				}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Overboeken", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

       	public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			//if (cancelToken.Token.CanBeCanceled)
			//	cancelToken.Cancel();

		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private async void UpdateBalance()
		{

			try
			{
				
				Internet = Reachability.InternetConnectionStatus();

				// If internet
				if (Internet != NetworkStatus.NotReachable)
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

					if (CurrentKaart.Error[0].Code == "E00")
					{
						Console.WriteLine("Updating Card data");
						Console.WriteLine("response balance " + JsonConvert.SerializeObject(CurrentKaart));
						string service = AMLocalStorage.KaartService;
						NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
						LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
						NSUserDefaults.StandardUserDefaults.Synchronize();

						WarningLabel.Text = "Je hebt " + AMMain.SetBalanceFormat(CurrentKaart.Kaart.Balance.ToString()) + " Air Miles";

					}
				}
				else
				{
					//SetNetworkView();
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Overboeken", "Exception: "+ ex}
                });
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			TableView.ReloadData();
		}
    }
}