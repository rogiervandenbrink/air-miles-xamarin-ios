﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using System.Threading;
using Google.Analytics;
using Newtonsoft.Json;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class DonateViewController : UITableViewController
    {
		// Network status
		NetworkStatus Internet;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		LoadingOverlay LoadingView;
		List<string> customCharityList;


        public DonateViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			Initialize();

			base.ViewDidLoad();

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Doneren");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			nfloat height = 44.0f;
			if (indexPath.Row == 1)
			{
				if (UIScreen.MainScreen.Bounds.Width < 320)
				{
					return 110;
				}
				else
				{
					return 80;
				}
			}
			if (indexPath.Row == 7)
			{
				return 15;
			}
			else
			{
				return height;
			}

		}

		public void Initialize()
		{
			AMStyle.RoundedButton(DonateButton);
			AMStyle.BorderedTextfield(CharityTextfield);
			AMStyle.BorderedTextfield(CharityAmountTextfield);
			CharityAmountTextfield.KeyboardType = UIKeyboardType.NumberPad;
			CharityTextfield.Font = UIFont.FromName("Avenir-Black", 12f);
			CharityAmountTextfield.Font = UIFont.FromName("Avenir-Black", 12f);
			DonateExplenationLabel.Font = UIFont.FromName("avenir-book", 14f);
			AMMain.AddDoneButton(CharityAmountTextfield);
			DonateTitleLabel.TextColor = UIColor.White;
			DonateTitleLabel.AdjustsFontSizeToFitWidth = true;

			string balance = "";
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance)))
			{
				balance = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance);
			}
			else
			{
				balance = "0";
			}
			WarningLabel.Text = "Je hebt " + AMMain.SetBalanceFormat(balance) + " Air Miles";

			CharityTextfield.ShouldBeginEditing += OnCharityPickerShouldBeginEditing;

			GetCharityNames();

			DonateButton.TouchUpInside += delegate {
				if (string.IsNullOrEmpty(CharityTextfield.Text))
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Geen goed doel ingevuld.");
				}
				if (string.IsNullOrEmpty(CharityAmountTextfield.Text))
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, "Het aantal Air Miles is niet ingevuld.");
				}
				else
				{
					Donate();
                    CharityAmountTextfield.ResignFirstResponder();
				}

			};
		}

		bool OnCharityPickerShouldBeginEditing(UITextField textField)
		{
			//customCharityList.Add("Nationaal Fonds Kinderhulp");
			//customCharityList.Add("Solidaridad");
			//customCharityList.Add("Stichting AAP");
			//customCharityList.Add("War Child");
			// Create a new Alert Controller
			UIAlertController actionSheetAlert = UIAlertController.Create(null, "Goed doel", UIAlertControllerStyle.ActionSheet);

			// Add Actions

			for (int i = 0; i < customCharityList.Count; i++)
			{
				actionSheetAlert.AddAction(UIAlertAction.Create(customCharityList[i], UIAlertActionStyle.Default, (UIAlertAction obj) =>
				{
					var index = obj.Title;
					textField.Text = obj.Title;
					Console.WriteLine("pressed charity " + (obj.Title));

				}));
			}

			actionSheetAlert.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

			// Required for iPad - You must specify a source for the Action Sheet since it is
			// displayed as a popover
			UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
			if (presentationPopover != null)
			{
				presentationPopover.SourceView = this.View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			// Display the alert
			this.PresentViewController(actionSheetAlert, true, null);

			return false;
		}

		private async void Donate()
		{
			SetLoader(AMStrings.LoadingDonation);
			TableView.SetContentOffset(new CGPoint(0, 0), false);
			string charityName = CharityTextfield.Text;
			string amount = CharityAmountTextfield.Text;

			// Analytics event
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Donatie", "Donatie overmaken", CharityAmountTextfield.Text, null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			try
			{
				DonateService DonationService = new DonateService();
				DonationRoot Donation = await DonationService.Donate(charityName, amount, cancelToken);

				if (Donation.Error[0].Code == "E00")
				{
					AMAlerts.DefaultAlert(this, "Geslaagd!", AMMain.RemoveHTMLTags(Donation.Response.PopupMessage));
					UpdateBalance();
					CharityTextfield.Text = string.Empty;
					CharityAmountTextfield.Text = string.Empty;
				}
				else{

					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMMain.RemoveHTMLTags(Donation.Error[0].Message));

				}
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Doneren", "Exception: "+ ex}
                });
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via toke
			//if (cancelToken.Token.CanBeCanceled)
			//	cancelToken.Cancel();

		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		async void GetCharityNames()
		{
			SetLoader(AMStrings.LoadingDonation);
			TableView.SetContentOffset(new CGPoint(0, 0), false);
			string charityName = CharityTextfield.Text;
			string amount = CharityAmountTextfield.Text;
			DonateService DonationService = new DonateService();
			CharityRoot CharityRoot = await DonationService.GetCharityNames(cancelToken);

			if (CharityRoot.Error[0].Code == "E00")
			{
				customCharityList = new List<string>();
				for (int i = 0; i < CharityRoot.Response.Length; i++)
				{
					customCharityList.Add(CharityRoot.Response[i].CharityName);

				}

			}
			else {

				AMAlerts.DefaultAlert(this, "Foutmelding", AMMain.RemoveHTMLTags(CharityRoot.Error[0].Message));

			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		private async void UpdateBalance()
		{

			try
			{


				Internet = Reachability.InternetConnectionStatus();

				// If internet
				if (Internet != NetworkStatus.NotReachable)
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

					if (CurrentKaart.Error[0].Code == "E00")
					{
						Console.WriteLine("Updating Card data");
						Console.WriteLine("response balance " + JsonConvert.SerializeObject(CurrentKaart));
						string service = AMLocalStorage.KaartService;
						NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
						LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
						NSUserDefaults.StandardUserDefaults.Synchronize();

						WarningLabel.Text = "Je hebt " + AMMain.SetBalanceFormat(CurrentKaart.Kaart.Balance.ToString()) + " Air Miles";

					}
				}
				else
				{
					//SetNetworkView();
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Doneren: kaartservice", "Exception: "+ ex}
                });

				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			TableView.ReloadData();
		}
    }
}