﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("MijnViewController")]
    partial class MijnViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel InvitationTitleLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView InvitationView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView LeftIconImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView MijnHeader { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView RightIconImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView UserAvatar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel UserBalance { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel UserLastname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel UserTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (InvitationTitleLabel != null) {
                InvitationTitleLabel.Dispose ();
                InvitationTitleLabel = null;
            }

            if (InvitationView != null) {
                InvitationView.Dispose ();
                InvitationView = null;
            }

            if (LeftIconImageView != null) {
                LeftIconImageView.Dispose ();
                LeftIconImageView = null;
            }

            if (MijnHeader != null) {
                MijnHeader.Dispose ();
                MijnHeader = null;
            }

            if (RightIconImageView != null) {
                RightIconImageView.Dispose ();
                RightIconImageView = null;
            }

            if (UserAvatar != null) {
                UserAvatar.Dispose ();
                UserAvatar = null;
            }

            if (UserBalance != null) {
                UserBalance.Dispose ();
                UserBalance = null;
            }

            if (UserLastname != null) {
                UserLastname.Dispose ();
                UserLastname = null;
            }

            if (UserTitle != null) {
                UserTitle.Dispose ();
                UserTitle = null;
            }
        }
    }
}