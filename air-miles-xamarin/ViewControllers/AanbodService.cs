﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class AanbodService
	{
		public AanbodService()
		{
		}

		public async Task<AanbodCategorien> GetAanbod()
		{
			Console.WriteLine("Start loading aanbod");

			HttpClient Client = new HttpClient();

			Client.BaseAddress = new Uri(AMClient.HippoBaseURL);
			Client.Timeout = new TimeSpan(0, 0, 0, 10);

			HttpResponseMessage response = null;

			try
			{
				response = await Client.GetAsync(AMClient.HipppAanbodUrl);

				if (response.IsSuccessStatusCode)
				{
					if (response.StatusCode.ToString() != "NoContent")
					{
						Console.WriteLine("Aanbod Response:  (" + response.StatusCode + ")");
						Console.WriteLine(response.Content.ReadAsStringAsync().Result);

						// set in localstorage
						NSUserDefaults.StandardUserDefaults.SetString(response.Content.ReadAsStringAsync().Result, AMLocalStorage.JsonAanbod);

						return JsonConvert.DeserializeObject<AanbodCategorien>(response.Content.ReadAsStringAsync().Result);
					}
				}

				Console.WriteLine("Aanbod Response: No Succes (" + response.StatusCode + ")");
				return JsonConvert.DeserializeObject<AanbodCategorien>("{}");

			}
			catch (Exception e)
			{
				Console.WriteLine("Aanbod Response: Error (" + e + ")");
				return JsonConvert.DeserializeObject<AanbodCategorien>("{}");
			}
		}

	}
}

