﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using LocalAuthentication;
using Google.Analytics;
using Xamarin;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading;
using Air_Miles_Xamarin;
using System.IO;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
	public partial class LoginViewController : UITableViewController
	{

		public string Routing { get; set; }

		LoadingOverlay LoadingView;
		NetworkStatus Internet;
		UIViewController Controller;

		LAContext Context = new LAContext();
		NSError Error;
		UIViewController controller;
		UINavigationController NavCtrl;

		bool UserHasForgotPincode = false;

		bool PasswordHidden = true;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();


		public LoginViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Console.WriteLine("routing initialize is " + Routing);
			Console.WriteLine("HasLogin is " + (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin)));
			Console.WriteLine("NavigationController is "+NavigationController);

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Login: Ontgrendelen");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			// Show touch id if user is logged in and has toch id
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				if (NavigationController != null && (NavigationController.VisibleViewController.GetType() == typeof(LoginViewController)))
				{
					Console.WriteLine("Login navigation!=null");
					UITextAttributes btnFontAttribute = new UITextAttributes();

					btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
					btnFontAttribute.TextColor = UIColor.White;
					UIBarButtonItem btn = new UIBarButtonItem();
					btn.Title = "Uitloggen";
					btn.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
					btn.Clicked += (sendero, e) =>
					{
    					// Logout user
    					VraagUitloggenSegue();
					};
					this.NavigationItem.RightBarButtonItem = btn;
				}

                if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
				{
					var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;
					if (NavCtrl.ForgotPincode == true)
					{
						UserHasForgotPincode = true;
					}
				}

				if (!UserHasForgotPincode)
				{
					
					ShowTouchId();
				}
			}

			// Login Click
			LoginButton.TouchUpInside += LoginTouchUpInside;

			// Buttons Click
			ForgotLoginButton.TouchUpInside += ForgotLoginTouchUpInside;
			NoLoginButton.TouchUpInside += NoLoginTouchUpInside;

			UIButton eyeButton = new UIButton(UIButtonType.Custom);
			eyeButton.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);
			eyeButton.TintColor = UIColor.LightGray;
			eyeButton.Frame = new CGRect(0, 0, 25, 15);
			ViewEyeGrey.AddSubview(eyeButton);

			eyeButton.TouchUpInside += delegate
			{
				PasswordHidden = !PasswordHidden;
				PasswordTextfield.SecureTextEntry = PasswordHidden;
				if (!PasswordHidden)
				{
					eyeButton.SetImage(UIImage.FromBundle("IconEyeBlue"), UIControlState.Normal);
				}
				else
				{
					eyeButton.SetImage(UIImage.FromBundle("IconEyeGrey"), UIControlState.Normal);

				}
			};

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			Initialize();
			SetButton();
			TableView.ReloadData();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == true)
			{
				RemoveFromParentViewController();
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			SetAnalytics();

		}

		public override void ViewDidDisappear(bool animated)
		{
			Console.WriteLine("diddisappear login Routing "+Routing);
			if (Routing != null)
			{
				if (Routing == "AanbodViewController")
				{
					if (NavigationController != null)
					{
						NavigationController.PopToRootViewController(false);
					}
				}
			}

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
			
			base.ViewDidDisappear(animated);

		}

		void SetButton()
		{
			// Show touch id if user is logged in and has toch id
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				Console.WriteLine("navigationcontroller login 1 "+NavigationController);
				if (NavigationController != null)
				{
					if (Routing != "AanbodViewController")
					{
						Console.WriteLine("Login navigation!=null");
						UITextAttributes btnFontAttribute = new UITextAttributes();

						btnFontAttribute.Font = UIFont.FromName("Avenir-Heavy", 14);
						btnFontAttribute.TextColor = UIColor.White;
						UIBarButtonItem btn = new UIBarButtonItem();
						btn.Title = "Uitloggen";
						btn.SetTitleTextAttributes(btnFontAttribute, UIControlState.Normal);
						btn.Clicked += (sendero, e) =>
						{
    						// Logout user
    						VraagUitloggenSegue();

						};
						this.NavigationItem.RightBarButtonItem = btn;
					}
					else
					{
						this.NavigationItem.RightBarButtonItem = null;
					}
				}
			}
			else
			{
				this.NavigationItem.RightBarButtonItem = null;
			}
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Login");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void VraagUitloggenSegue()
		{
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Login: Ontgrendelen", "Uitloggen", "Uitloggen", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				UIAlertController AlertController = UIAlertController.Create("Uitloggen", AMStrings.LogoutAskMessageDismiss, UIAlertControllerStyle.Alert);
				AlertController.AddAction(UIAlertAction.Create("Uitloggen", UIAlertActionStyle.Default, alert => UitloggenSegue()));
				AlertController.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Default, alert => DismissViewController(true, null)));
				PresentViewController(AlertController, true, null);
			}else
			{
				UIAlertController AlertController = UIAlertController.Create("Uitloggen", AMStrings.LogoutAskMessage, UIAlertControllerStyle.Alert);
				AlertController.AddAction(UIAlertAction.Create("Uitloggen", UIAlertActionStyle.Default, alert => UitloggenSegue()));
				AlertController.AddAction(UIAlertAction.Create("Annuleren", UIAlertActionStyle.Default, alert => DismissViewController(true, null)));
				PresentViewController(AlertController, true, null);
			}

		}

		private void UitloggenSegue()
		{
			
			LocalStorage.Logout(TabBarController);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Login", "Uitloggen", "Uitloggen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
			this.ViewDidDisappear(false);
			this.ViewWillAppear(false);
			DismissModalViewController(false);
		}

		void LoginTouchUpInside(object sender, EventArgs ea)
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable)
			{
				// if user is logged in
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					// if current logged in user is loggin in
					if (UsernameTextfield.Text.ToLower() == NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username).ToLower())
					{
						ValidateLogin(UsernameTextfield.Text.ToLower(), PasswordTextfield.Text);

					}
					else {
						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginIncorrectTitle, AMStrings.ErrorLoginIncorrectMessage);
					}
				}
				// if user is not loggedin
				else {
					ValidateLogin(UsernameTextfield.Text.ToLower(), PasswordTextfield.Text);
				}
			}
			else {
				AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
			}
		}

		void ForgotLoginTouchUpInside(object sender, EventArgs ea)
		{
			UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset));
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Login: Ontgrendelen", "Open website", "Inloggegevens vergeten", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		void NoLoginTouchUpInside(object sender, EventArgs ea)
		{
		
			CreateDigitalAccountBirthDateViewController FirstKaartModal = App.Storyboard.InstantiateViewController("CreateDigitalAccountBirthDateViewController") as CreateDigitalAccountBirthDateViewController;
			FirstKaartModal.Routing = "Mijn";
			NavigationController.PushViewController(FirstKaartModal, true);
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Login: Ontgrendelen", "Account aanmaken", "Nog geen inloggegevens", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		private void Initialize()
		{
			HideBackButton();

			if (NavigationController != null)
			{
				NavigationController.NavigationBar.TintColor = UIColor.White;
			}
			if (NavigationController != null && Routing == "Dismiss")
			{
				LoginDetailLabel.Text = "Log in om de app te ontgrendelen.";
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					LoginButton.SetTitle("ONTGRENDELEN",UIControlState.Normal);
					NoLoginButton.Hidden = true;
					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Login", "Ontgrendelen", "Ontgrendelen", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
				else
				{
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}
			if (NavigationController != null && Routing == "TransactieAankoopViewController")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Ontgrendelen";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Mijn";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Inloggen";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Mijn";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}
			if (NavigationController != null && Routing == "InstellingenViewController")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Meer";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Meer";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Meer";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Meer";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}
			if (NavigationController != null && Routing == "MijnViewController")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Mijn";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Mijn";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Mijn";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Mijn";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}

			if (NavigationController != null && Routing == "GegevensViewController")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Mijn";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Mijn";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Mijn";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Mijn";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}

			if (NavigationController != null && Routing == "AanbodViewController")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Aanbod";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Aanbod";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Aanbod";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					NavigationController.Title = "Aanbod";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}

			if (NavigationController != null && Routing == "SetPinTouchViewController")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Ontgrendelen";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Inloggen";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}
			if (NavigationController != null && Routing == "ResetBlockedPin")
			{
				if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
				{
					Title = "Ontgrendelen";
					LoginHeaderLabel.Text = "Ontgrendelen";
					LoginDetailLabel.Text = "Ontgrendel om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					LoginButton.SetTitle("ONTGRENDELEN", UIControlState.Normal);
					NoLoginButton.Hidden = true;
				}
				else
				{
					Title = "Inloggen";
					LoginHeaderLabel.Text = "Inloggen";
					LoginDetailLabel.Text = "Log in om je persoonlijke gegevens, transacties, aanbiedingen en/of aankopen te bekijken.";
					LoginButton.SetTitle("INLOGGEN", UIControlState.Normal);
					NoLoginButton.Hidden = false;
				}
			}

			AMMain.AddDoneButton(UsernameTextfield);
			AMMain.AddDoneButton(PasswordTextfield);
			AMStyle.BorderedTextfield(UsernameTextfield);
			AMStyle.BorderedTextfield(PasswordTextfield);
			AMStyle.RoundedButton(LoginButton);
		}


		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		private void ValidateLogin(string username, string password)
		{
			if (username != "" && password != "")
			{
				Login(username, password);
			}
			else {
				int error = 0;

				// Validate username
				if (username == "" && password != "")
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginEmptyUsername);
					error++;
				}

				// Validate password
				if (username != "" && password == "")
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginEmptyPassword);
					error++;
				}

				// Validate textfields
				if (username == "" && password == "")
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginEmptyTextfields);
					error++;
				}
			}
		}

		private async void Login(string username, string password)
		{
			InitLogin();

			try
			{
			LoginService LoginService = new LoginService();
			UserRoot UserResponse = await LoginService.Login(username, password);

			//AMMain.UpdateTokenApp(UserResponse.TokenApp.TokenApp);
			Console.WriteLine("UserResponse "+UserResponse.Error);
		
				if (UserResponse.Error.Length > 0)
			{
					//clear old balance
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.Balance);
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.CardNumber);

                    //create deviceinfo file
                    string Now = DateTime.UtcNow.ToString("d");
                    DateTime TimestampLog = DateTime.UtcNow;
                    var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    var filename = Path.Combine(documents, "logiOS.txt");
                    if (System.IO.File.Exists(filename))
                    {
                        if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TimestampLogfile)))
                        {
                            DateTime dt = Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.TimestampLogfile));
                            if (dt.AddDays(10) > TimestampLog){
                                File.AppendAllText(filename, "\nErrorcode login: " + UserResponse.Error[0].Code + "\nErrormessage: " + UserResponse.Error[0].Message + "\nTimestamp: " + Now);
                            } else {
                                NSUserDefaults.StandardUserDefaults.SetString(TimestampLog.ToString(), AMLocalStorage.TimestampLogfile);
                                File.WriteAllText(filename, "\nErrorcode login: " + UserResponse.Error[0].Code + "\nErrormessage: " + UserResponse.Error[0].Message + "\nTimestamp: " + Now);
                            }
                        } else {
                            NSUserDefaults.StandardUserDefaults.SetString(TimestampLog.ToString(), AMLocalStorage.TimestampLogfile);
                            File.WriteAllText(filename, "\nErrorcode login: " + UserResponse.Error[0].Code + "\nErrormessage: " + UserResponse.Error[0].Message + "\nTimestamp: " + Now);
                        }
                    }
                    else
                    {
                        NSUserDefaults.StandardUserDefaults.SetString(TimestampLog.ToString(), AMLocalStorage.TimestampLogfile);
                        File.WriteAllText(filename, "Errorcode login: " + UserResponse.Error[0].Code + "\nErrormessage: " + UserResponse.Error[0].Message + "\nTimestamp: " + Now);
                    }
				//if (UserResponse.Error[0].Code != AMStrings.ErrorCode)
				//{
					/// <summary>
					/// Login request is succesfull without errors
					/// </summary>
					if (UserResponse.Error[0].Code == "E00")
					{
                        LocalStorage.RemoveCard();
						SuccesLogin(UserResponse.User[0],UserResponse.TokenApp, username, password);
					}
					/// <summary>
					/// Login request is succesfull, but has errors (wrong password SAP CODES: P02 AND P03)
					/// </summary>
					if (UserResponse.Error[0].Code == "E34" || UserResponse.Error[0].Code == "E35")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						
						//AMAlerts.P002Alert(this);

						DisposeLogin();
					}
					/// <summary>
					/// Login request is succesfull, but has errors (user not activated SAP CODE: P04)
					/// </summary>
					if (UserResponse.Error[0].Code == "E36")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
						//AMAlerts.P004Alert(this);

						DisposeLogin();
					}
					if (UserResponse.Error[0].Code == "E24")
						{
							Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
							NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

							AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
							//AMAlerts.P002Alert(this);

							DisposeLogin();
						}
					/// <summary>
					/// Login request is succesfull, but has errors (temporary blocked SAP CODE: P05)
					/// </summary>
					if (UserResponse.Error[0].Code == "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Naar de website");
						alert.AddButton("Annuleren");
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();

						DisposeLogin();
					}
					if (UserResponse.Error[0].Code == "E86")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						//AMAlerts.P005Alert(this);
						UIAlertView alert = new UIAlertView();
						alert.Title = AMStrings.ErrorLoginTitle;
						alert.AddButton("Inloggegevens wijzigen");
						alert.AddButton("Annuleren");
						//alert.Message = "De combinatie van gebruikersnaam en wachtwoord blijft onbekend. Je account is geblokkeerd. Deblokkeren? Klik op inloggevens wijzigen en pas je inloggegevens aan."
						alert.Message = UserResponse.Error[0].Message;
						alert.AlertViewStyle = UIAlertViewStyle.Default;
						alert.Clicked += ButtonClicked;
						alert.Show();

						DisposeLogin();
						}
					/// <summary>
					/// Login request is succesfull, but does not have any of the above errors
					/// </summary>
					if (UserResponse.Error[0].Code != "E00" && UserResponse.Error[0].Code != "24" && UserResponse.Error[0].Code != "E34" && UserResponse.Error[0].Code != "E35" && UserResponse.Error[0].Code != "E36" && UserResponse.Error[0].Code != "E37")
					{
						Console.WriteLine("Niet ingelogd " + UserResponse.Error[0].Code);
						NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);
                        //AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
                        Initialize();
						DisposeLogin();
					}
				//}
				//else {
				//	Console.WriteLine("Niet ingelogd"+ UserResponse.Error[0].Code);
				//	NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

				//	AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

				//	DisposeLogin();
				//}
			}
			else {
				Console.WriteLine("Niet ingelogd");
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);

				AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, UserResponse.Error[0].Message);

				DisposeLogin();
			}
				}
			catch (Exception ex)
			{
				//Console.WriteLine("UserResponse " + UserResponse.Error);
				Console.WriteLine("Niet ingelogd "+ex);
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);
				// Logout
				LocalStorage.Logout(TabBarController);

				AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);

				DisposeLogin();
			}
		}


		void ButtonClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLloginReset));
			}
			else {

				// Cancel button
			}
		}

		private async void SuccesLogin(User user, ResponseToken token, string username, string password)
		{
			
			Console.WriteLine("cardnumber "+NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
			// Set user
			try
			{
				LocalStorage.LocalLogin(user, token, username);
			}
			catch (Exception ex)
			{
				Console.WriteLine("exception local login: "+ex);
			}

			// Set user card
			string cardnumber = "";

			if (!string.IsNullOrEmpty(user.CardNumber))
			{
				cardnumber = user.CardNumber;
			}

			if (cardnumber.Length == 9)
			{
				LoadingView.UpdateLabel(AMStrings.LoadingCardData);

				try
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(user.CardNumber, cancelToken);

					Console.WriteLine("currentkaart "+CurrentKaart+" , kaartnummer"+ user.CardNumber);

					if (CurrentKaart.Kaart.Error != true)
					{
						LocalStorage.SetCard(CurrentKaart.Kaart, user.CardNumber);

						NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);
						NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);

						LoadingView.UpdateLabel(AMStrings.LoadingAvatar);
					}
				}
				catch (Exception ex)
				{
					if (LoadingView != null)
					{
						LoadingView.Hide();
					}
					if (AMMain.CanShowError())
					{
						AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
					}
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Login: kaartservice", "Exception: "+ ex}
                    });
				}
			}

			LoadingView.UpdateLabel(AMStrings.LoadingAvatar);

			try
			{
    			AvatarService AvatarService = new AvatarService();
    			AvatarRoot Avatar = await AvatarService.GetAvatar(token.TokenApp);

    			if (Avatar != null && Avatar.Response != null)
    			{
    				if (Avatar.Response.Length > 0 && Avatar.Response[0].Image != null)
    				{
    					NSUserDefaults.StandardUserDefaults.SetString(Avatar.Response[0].Image, username + "-" + AMLocalStorage.Avatar);
    				}
    				else {
    					NSUserDefaults.StandardUserDefaults.RemoveObject(username + "-" + AMLocalStorage.Avatar);
    				}
    			}
			}
			catch (Exception ex)
			{
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
				}
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Login: avatarservice", "Exception: "+ ex}
                });
			}

			LoadingView.UpdateLabel(AMStrings.LoadingGifts);

			DisposeLogin();

			RedirectToRoute();
		}

		protected void ShowTouchId()
		{
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID) == true && !UserHasForgotPincode)
			{
				//Lets double check the device supports Touch ID
				if (Context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out Error))
				{
					var replyHandler = new LAContextReplyHandler((success, error) =>
						{
							InvokeOnMainThread(() =>
								{
									if (success)
									{
										UpdateToken();

										
									}
								});
						});
					Context.EvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, "Ontgrendel met je vingerafdruk.", replyHandler);
				}
			}
		}

		private void RedirectToRoute()
		{
			
			Console.WriteLine("navigationcontroller firstpintouch " + NavigationController);
			
			AMMain.ShowFirstPinToucAlert(NavigationController);

			if (UserHasForgotPincode)
			{
				Console.WriteLine("first reset the pincode before routing");

				SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
				NavigationController.PushViewController(Redirect, false);

				UserHasForgotPincode = false;
			}
			else {
				Console.WriteLine("THE FINAL ROUTING TO REDIRECT" + Routing);

				if (Routing == "Dismiss")
				{
					DismissViewController(true, null);
				}
				if (Routing == "ResetBlockedPin")
				{
					LocalStorage.DeletePin();
					DismissViewController(true, null);
				}
				if (Routing == "TransactieAankoopViewController")
				{
					if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
					{
						DismissViewController(true, null);
					}
					else {
						DismissViewController(true, null);
					}
				}
				if (Routing == "MijnViewController")
				{
					if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
					{
						Console.WriteLine("here we are navigation = enterpintouch + dissmiss");

						DismissViewController(true, null);
					}
					else {
						Console.WriteLine("here we are navigation = other + pushview");

						MijnViewController Redirect = App.Storyboard.InstantiateViewController("MijnViewController") as MijnViewController;
						NavigationController.PushViewController(Redirect, false);
					}
				}
			
                if (Routing == "GegevensViewController")
				{
                    
                    this.NavigationItem.RightBarButtonItem = null;

                    DismissViewController(true, null);

				}
				if (Routing == "AanbodViewController")
				{
					
					Console.WriteLine("here we are navigation "+ Routing);
					PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;
					Redirect.Title = "Speciaal voor jou!";
					Redirect.PersonalGifts = true;
					NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.UnseenGifts);
					NSUserDefaults.StandardUserDefaults.Synchronize();
					AMMain.ClearAanbodTabBadge();
					NavigationController.PushViewController(Redirect, true);

				}
				if (Routing == "SetPinTouchViewController")
				{
					SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
					NavigationController.PushViewController(Redirect, false);
				}
				if (Routing == "InstellingenViewController")
				{
					Console.WriteLine("here we are");
					if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
					{
						DismissViewController(true, null);
					}
					else {
						InstellingenViewController Redirect = App.Storyboard.InstantiateViewController("InstellingenViewController") as InstellingenViewController;
						NavigationController.PushViewController(Redirect, false);
					}
				}
				else
				{
					Console.WriteLine("routing is empty");
					DismissViewController(true, null);
				}
			}
		}

		private void InitLogin()
		{
			TableView.ScrollEnabled = false;
			SetLoader(AMStrings.LoadingLogin);
			EnableLogin(false);
		}

		private void DisposeLogin()
		{
			TableView.ScrollEnabled = true;
            TableView.ReloadData();
			LoadingView.Hide();
			EnableLogin(true);
		}

		private void InitLoading()
		{
			TableView.ScrollEnabled = false;
			SetLoader(AMStrings.LoadingLogin);
			EnableLogin(false);
		}

		private void EnableLogin(bool enable)
		{
			UsernameTextfield.Enabled = enable;
			PasswordTextfield.Enabled = enable;
			LoginButton.Enabled = enable;
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		async void UpdateToken()
		{

			SetLoader(AMStrings.LoadingUnlock);
			try{
				CheckTokenService CheckTokenService = new CheckTokenService();
				TokenRoot response = await CheckTokenService.GetTokenData();

				if (response.Error[0].Code == "E00")
				{
					if (!string.IsNullOrEmpty(response.TokenApp.TokenApp))
					{
						AMMain.UpdateTokenApp(response.TokenApp.TokenApp);
						if (response.ResponseTokenCheck.CheckTokenResponse_LinkCardsMessage != null)
						{
							NSUserDefaults.StandardUserDefaults.SetString(response.ResponseTokenCheck.CheckTokenResponse_LinkCardsMessage.Message, AMLocalStorage.LinkcardsMessage);
						}
						Console.WriteLine("update token: " + response.TokenApp.TokenApp);
						AMMain.UpdateBounceIndicators(response.ResponseTokenCheck);
						NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
						NSUserDefaults.StandardUserDefaults.Synchronize();
						Redirect();
					}
				}
				if (response.Error[0].Code == "E23")
				{
					Console.WriteLine("E23 ");

					// Logout
					LocalStorage.Logout(TabBarController);

					Console.WriteLine("navigation controller is: "+NavigationController);
					// Redirect to login page
					LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
					Redirect.Routing = "SetPinTouchViewController";
					NavigationController.PushViewController(Redirect, false);
				if (NavigationController != null)
				{
					NavigationController.DismissViewController(false, null);
				}
					AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, response.Error[0].Message);
				}
				else if (response.ResponseTokenCheck.ShowPopUp)
				{
					AMAlerts.DefaultAlert(this, "Maak je profiel compleet", response.ResponseTokenCheck.PopUpMessage);
				}
			}
			catch (Exception ex)
			{

				Console.WriteLine("Exception CheckToken " + ex);
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Login", "Exception: "+ ex}
                });

				// Logout
				LocalStorage.Logout(TabBarController);

				// Redirect to login page
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
				Redirect.Routing = "SetPinTouchViewController";
				NavigationController.PushViewController(Redirect, false);
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, AMStrings.ErrorSessionExpiredMessage);
				}

				if (LoadingView != null)
				{
					LoadingView.Hide();
				}


			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

        void ResetHasLogin() 
        {
            NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);    
        }

		public void Redirect()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.Session);

			Console.WriteLine("Validated touch, redirect to " + Routing);

			if (Routing == "Dismiss")
			{
				DismissViewController(true, null);
				Console.WriteLine("Login is dismissed");
			}
			if (Routing == "ResetBlockedPin")
			{
				LocalStorage.DeletePin();
				DismissViewController(true, null);
			}
			if (Routing == "TransactieAankoopViewController")
			{
				DismissViewController(true, null);
			}
			if (Routing == "AanbodViewController")
			{
				Console.WriteLine("here we are navigation " + Routing);
				PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;
				Redirect.Title = "Speciaal voor jou!";
				Redirect.PersonalGifts = true;
				NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.UnseenGifts);
				NSUserDefaults.StandardUserDefaults.Synchronize();
				AMMain.ClearAanbodTabBadge();
				NavigationController.PushViewController(Redirect, true);
			}
			if (Routing == "SetPinTouchViewController")
			{
				SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
				NavigationController.PushViewController(Redirect, false);
			}
			if (Routing == "InstellingenViewController")
			{
				Console.WriteLine("here we are");
				if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
				{
					DismissViewController(true, null);
				}
				else {
					InstellingenViewController Redirect = App.Storyboard.InstantiateViewController("InstellingenViewController") as InstellingenViewController;
					NavigationController.PushViewController(Redirect, false);
				}

			}
			if (Routing == "MijnViewController")
			{
				if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
				{
					DismissViewController(true, null);
				}
				else {
					MijnViewController Redirect = App.Storyboard.InstantiateViewController("MijnViewController") as MijnViewController;
					NavigationController.PushViewController(Redirect, false);
				}
			}
            if (Routing == "GegevensViewController")
            {
                Console.WriteLine("navigationcontroller "+NavigationController.GetType());

                this.NavigationItem.RightBarButtonItem = null;
                DismissViewController(true, delegate
                {
                    this.NavigationItem.RightBarButtonItem = null;
                    ProfileViewController redirect = App.Storyboard.InstantiateViewController("ProfileViewController") as ProfileViewController;
                    redirect.Title = "Gegevens";
                    NavigationController.PushViewController(redirect, true);
                });
            }
			
		}


    }
}