﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("LoginViewController")]
    partial class LoginViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ForgotLoginButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton LoginButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LoginDetailLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LoginHeaderLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton NoLoginButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PasswordTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField UsernameTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ViewEyeGrey { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ForgotLoginButton != null) {
                ForgotLoginButton.Dispose ();
                ForgotLoginButton = null;
            }

            if (LoginButton != null) {
                LoginButton.Dispose ();
                LoginButton = null;
            }

            if (LoginDetailLabel != null) {
                LoginDetailLabel.Dispose ();
                LoginDetailLabel = null;
            }

            if (LoginHeaderLabel != null) {
                LoginHeaderLabel.Dispose ();
                LoginHeaderLabel = null;
            }

            if (NoLoginButton != null) {
                NoLoginButton.Dispose ();
                NoLoginButton = null;
            }

            if (PasswordTextfield != null) {
                PasswordTextfield.Dispose ();
                PasswordTextfield = null;
            }

            if (UsernameTextfield != null) {
                UsernameTextfield.Dispose ();
                UsernameTextfield = null;
            }

            if (ViewEyeGrey != null) {
                ViewEyeGrey.Dispose ();
                ViewEyeGrey = null;
            }
        }
    }
}