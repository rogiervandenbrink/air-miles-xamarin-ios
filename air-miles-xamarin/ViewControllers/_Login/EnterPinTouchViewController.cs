﻿using Foundation;
using System;
using UIKit;
using LocalAuthentication;
using CoreGraphics;
using CoreAnimation;
using System.Threading.Tasks;
using System.Threading;
using SDWebImage;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class EnterPinTouchViewController : UIViewController
    {
		LAContext Context = new LAContext();
		NSError Error;

		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;

		bool SessionIsTrue;

        public EnterPinTouchViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Console.WriteLine("Secure validation");

			UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(OnTapped);
			PincodeView.AddGestureRecognizer(tapGesture);

			// If user does not have Pin enabled, go to login
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID) != true)
			{
				Console.WriteLine("User does not have pin");
				RedirectToLoginOnRouting();
				//this.Title = "Ontgrendelen";
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Inloggegevens", "Gebruik pin", "Nee", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else {
				Console.WriteLine("User has pin");

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Inloggegevens", "Gebruik pin", "Ja", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();


				var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

				if (NavCtrl.Routing != "SetPinTouchViewController")
				{
					ShowTouchId();
				}
				Initialize();

				ForgotPinButton.TouchUpInside += ForgotPinTouchUpInside;
				LoginButton.TouchUpInside += BlockedPinLogin;


				Textfield.EditingChanged += (object sender, EventArgs e) =>
				{
					if (Textfield.IsEditing)
					{
						StylePincodesOnChange(Textfield.Text);
					}
				};
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			if (!Textfield.IsFirstResponder)
			{
				Textfield.BecomeFirstResponder();
			}
			nint CurrentPinAttempts = NSUserDefaults.StandardUserDefaults.IntForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);

			if (CurrentPinAttempts >= 3)
			{
				Textfield.ResignFirstResponder();
			}

			SessionIsTrue = false;
			base.ViewDidAppear(animated);
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (Textfield.IsFirstResponder)
			{
				Textfield.ResignFirstResponder();
			}

			if (SessionIsTrue) 			{ 				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.Session); 			} 			else 			{ 				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.Session); 			}

		}

		void OnTapped()
		{
			if (Textfield.IsFirstResponder)
			{
				Textfield.ResignFirstResponder();
			}
			else
			{
				Textfield.BecomeFirstResponder();
			}
		}

		private void Initialize()
		{
			Textfield.Hidden = true;
			ErrorMessageLabel.Hidden = true;
			AMStyle.RoundedButton(LoginButton);

			//DetailMessageLabel.Text = "Voer je pincode in.";

			//LoginButton.Hidden = true;
			//ForgotPinButton.Hidden = false;
			//DetailMessageLabel.Hidden = false;
			//Pincodes.Hidden = false;

			ResetPincodes();

			AMMain.SetAvatar(UserAvatar, "placeholder.png");

			UserAvatar.UserInteractionEnabled = true;

			//UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(ShowKeyBoard);
			//UserAvatar.AddGestureRecognizer(tapGesture);
				
			nint CurrentPinAttempts = NSUserDefaults.StandardUserDefaults.IntForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);

			if (CurrentPinAttempts >= 3)
			{
				Console.WriteLine("pin attempts onthouden");
				BlockPin();
			}

			var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

			if (NavCtrl.Routing == "SetPinTouchViewController")
			{
				// Hide back button
				NavigationItem.SetHidesBackButton(true, false);
				DetailMessageLabel.Text = "Voer je huidige pincode in.";

				// Set close icon
				this.NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Stop, (sender, args) =>
				{
					Textfield.ResignFirstResponder();
					SessionIsTrue = true;
					DismissModalViewController(true);
				})
				, true);
			}
		}



		void ShowKeyBoard(UITapGestureRecognizer tap)
		{
			Textfield.BecomeFirstResponder();
		}

		protected void ValidatePincode() 
		{
			if (Textfield.Text == KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true))
			{
				StyleSuccesPincodes();
			}
			else {
				StyleIncorrectPincodes();
			}
		}

		void ForgotPinTouchUpInside(object sender, EventArgs ea)
		{
			var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

			NavCtrl.ForgotPincode = true;
			RedirectToLoginOnRouting();
		}

		void BlockedPinLogin(object sender, EventArgs ea)
		{
			RedirectToLoginOnRouting();
		}

		void RedirectToLogin(string route)
		{
			Textfield.ResignFirstResponder();

			LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
			Redirect.Routing = route;
			NavigationController.PushViewController(Redirect, false);
		}

		void RedirectToLoginOnRouting()
		{
			var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

			Console.WriteLine("No pin, login routing will be " + NavCtrl.Routing);
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.FromSaldoViewController))
			{
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.FromSaldoViewController);
				RedirectToLogin("SaldoTableViewController");
			}
            else if (NavCtrl.Routing == "SetPinTouchViewController")
			{
				RedirectToLogin("SetPinTouchViewController");
			}
			else if (NavCtrl.Routing == "TransactiesViewController" || NavCtrl.Routing == "InstellingenViewController" || NavCtrl.Routing == "MijnViewController")
			{
				RedirectToLogin("Dismiss");
			}
			else {
				RedirectToLogin("ResetBlockedPin");
			}
			//NavCtrl.DismissModalViewController(false);
		}

		protected void ResetPinAndLogout()
		{
			Textfield.ResignFirstResponder();
			LocalStorage.DeleteTouchPin(); // always executue this function before logout !very important! otherwise values won't be deleted
			LocalStorage.Logout(TabBarController);
			DismissViewController(false, null);
		}

		protected void ShowTouchId()
		{

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID) == true)
			{
				//Lets double check the device supports Touch ID
				if (Context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out Error))
				{
					// Analytics Screenname
					Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Login: Vingerafdruk");
					Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());


					var replyHandler = new LAContextReplyHandler((success, error) =>
						{
							InvokeOnMainThread(() =>
								{
									if (success)
									{
										StyleSuccesPincodes();
									}
									else
									{
										if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID) == true)
										{
											if (NSUserDefaults.StandardUserDefaults.IntForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts) < 3)
											{
												Textfield.BecomeFirstResponder();
											}
										}
										else {
											RedirectToLoginOnRouting();
										}
									}
								});
						});
					Context.EvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, "Ontgrendel met je vingerafdruk.", replyHandler);

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Inloggegevens", "Gebruik vingerafdruk", "Ja", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();
				}
				else
				{
					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Inloggegevens", "Gebruik pin", "Nee", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();

					Console.WriteLine("No Touch supported");
					CheckPinIDSettings();
				}
			}
			else
			{
				Console.WriteLine("No Touch detected");
				CheckPinIDSettings();
			}
		}

		void CheckPinIDSettings()
		{
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID) == true)
			{
				// Analytics Screenname
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Login: Pincode");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

				Textfield.BecomeFirstResponder();
			}
			else {
				RedirectToLoginOnRouting();
			}
		}

		private void StyleDefaultBackground()
		{
			View.BackgroundColor = UIColor.FromRGB(217, 236, 246);
		}

		private void StyleEmptyPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.Clear;
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(172, 209, 229).CGColor;
		}

		private void StyleFilledPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.FromRGB(0, 125, 195);
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
		}

		private void StyleCorrectPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.FromRGB(159, 211, 61);
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(159, 211, 61).CGColor;
		}

		private void StyleIncorrectPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.FromRGB(242, 42, 94);
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(242, 42, 94).CGColor;
		}

		private async void StyleSuccesPincodes() 
		{
			// Set session so pincode won't be shown again during this session
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.Session);

			SessionIsTrue = true;

			//NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
			// Reset pin attempt to 0
			LocalStorage.ResetPinAttempts();

			View.BackgroundColor = UIColor.FromRGB(238, 244, 225);

			if (Textfield.CanResignFirstResponder)
			{
				Textfield.ResignFirstResponder();
			}

			StyleCorrectPincode(Pincode1);
			StyleCorrectPincode(Pincode2);
			StyleCorrectPincode(Pincode3);
			StyleCorrectPincode(Pincode4);
			StyleCorrectPincode(Pincode5);

			await AnimateSucces(Pincodes);

			var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

			Console.WriteLine("We will be routing to: " + NavCtrl.Routing);
			try
			{
			CheckTokenService CheckTokenService = new CheckTokenService();
			TokenRoot response = await CheckTokenService.GetTokenData();
			//Console.WriteLine("new token "+response.TokenApp.TokenApp);


				Console.WriteLine("error code checktoken: " + response.Error[0].Code);
				Console.WriteLine("bool showpopup token "+ response.ResponseTokenCheck.ShowPopUp);
			
			if (response.Error[0].Code == "E00")
			{
					
				if (!string.IsNullOrEmpty(response.TokenApp.TokenApp))
				{
						AMMain.UpdateTokenApp(response.TokenApp.TokenApp);
						AMMain.UpdateBounceIndicators(response.ResponseTokenCheck);
						Console.WriteLine("update token: "+response.TokenApp.TokenApp);
						App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;
						//DismissModalViewController(false);
						NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
					
						if (string.IsNullOrEmpty(NavCtrl.Routing))
						{
							DismissModalViewController(true);
						}
						else
						{
							if (NavCtrl.Routing == "Dismiss")
							{
								DismissModalViewController(true);
							}
							if (NavCtrl.Routing == "TransactieAankoopViewController" || NavCtrl.Routing == "InstellingenViewController" || NavCtrl.Routing == "MijnViewController")
							{
								DismissModalViewController(true);
							}
							if (NavCtrl.Routing == "SetPinTouchViewController")
							{
								SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
								NavigationController.PushViewController(Redirect, false);
							}
						}

				}
			}
			if (response.Error[0].Code == "E23")
			{
				Console.WriteLine("E23 ");

				// Logout
				LocalStorage.Logout(TabBarController);

				// Redirect to login page
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
				Redirect.Routing = "SetPinTouchViewController";
				NavigationController.PushViewController(Redirect, false);
				//NavigationController.ViewControllers = new[] { Redirect };

				AMAlerts.DefaultAlert(this, AMStrings.ErrorSessionExpiredTitle, response.Error[0].Message);
				//AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, AMStrings.ErrorSessionExpiredMessage);
			}
			if (response.ResponseTokenCheck.ShowPopUp)
			{
				AMAlerts.DefaultAlert(this, "Maak je profiel compleet", response.ResponseTokenCheck.PopUpMessage);
			}
				}
			catch (Exception ex)
			{
				SetNetworkView();

				//KaartViewController Redirect = App.Storyboard.InstantiateViewController("KaartViewController") as KaartViewController;

				//NavigationController.ViewControllers = new[] { Redirect };
				DismissViewController(false, null);

				Console.WriteLine("Exception CheckToken "+ex);

                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Pin-touch: checktoken", "Exception: "+ ex}
                });

				if (string.IsNullOrEmpty(NavCtrl.Routing))
				{
					DismissModalViewController(true);
				}
				else {
					if (NavCtrl.Routing == "Dismiss")
					{
						DismissModalViewController(true);
					}
					if (NavCtrl.Routing == "TransactieAankoopViewController" || NavCtrl.Routing == "InstellingenViewController" || NavCtrl.Routing == "MijnViewController")
					{
						DismissModalViewController(true);
					}
					if (NavCtrl.Routing == "SetPinTouchViewController")
					{
						SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
						NavigationController.PushViewController(Redirect, false);
					}
				}

			}

		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		private void WrongPinAttemptLabel()
		{
			LocalStorage.IncreareWrongPinAttempt();
			ErrorMessageLabel.Hidden = false;

			nint CurrentPinAttempts = NSUserDefaults.StandardUserDefaults.IntForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);
			nint RemainingAttempts = 3 - CurrentPinAttempts;
			ErrorMessageLabel.Text = "PINCODE IS NIET CORRECT";
			ErrorMessageLabel.Hidden = false;

			if (RemainingAttempts == 0)
			{
				BlockPin();
			}
			if (RemainingAttempts == 1)
			{
				DetailMessageLabel.Text = RemainingAttempts + " poging resterend.";
			}
			if (RemainingAttempts > 1)
			{
				DetailMessageLabel.Text = RemainingAttempts + " pogingen resterend.";
			}
		}

		private async void StyleIncorrectPincodes()
		{
			nint CurrentPinAttempts = NSUserDefaults.StandardUserDefaults.IntForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);

			View.BackgroundColor = UIColor.FromRGB(255, 234, 239);
			WrongPinAttemptLabel();

			StyleIncorrectPincode(Pincode1);
			StyleIncorrectPincode(Pincode2);
			StyleIncorrectPincode(Pincode3);
			StyleIncorrectPincode(Pincode4);
			StyleIncorrectPincode(Pincode5);


			if (CurrentPinAttempts >= 3)
			{
				BlockPin();
			}

			await AnimateError(Pincodes);

			ErrorMessageLabel.Hidden = true;

			ResetPincodes();

		}

		void BlockPin()
		{
			Textfield.ResignFirstResponder();

			DetailMessageLabel.Text = "Pincode geblokkeerd.";

			LoginButton.Hidden = false;
			ForgotPinButton.Hidden = true;
			DetailMessageLabel.Hidden = false;
			Pincodes.Hidden = true;


		}

		private void ResetPincodes()
		{
			Textfield.Text = "";
			StyleEmptyPincode(Pincode1);
			StyleEmptyPincode(Pincode2);
			StyleEmptyPincode(Pincode3);
			StyleEmptyPincode(Pincode4);
			StyleEmptyPincode(Pincode5);
			StyleDefaultBackground();
		}

		private static async Task AnimateError(UIView view)
		{
			await AnimateHorizontalMovementAsync(view, -10);
			await AnimateHorizontalMovementAsync(view, 20);
			await AnimateHorizontalMovementAsync(view, -20);
			await AnimateHorizontalMovementAsync(view, 20);
			await AnimateHorizontalMovementAsync(view, -15);
			await AnimateHorizontalMovementAsync(view, 10);
			await AnimateHorizontalMovementAsync(view, -5);
			Thread.Sleep(750);
		}

		private static async Task AnimateSucces(UIView view)
		{
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			Thread.Sleep(750);
		}

		private static async Task AnimateHorizontalMovementAsync(UIView view, float horizontalOffset)
		{
			await UIView.AnimateAsync(0.085, () => view.Frame =
              new CGRect(new CGPoint(view.Frame.Left + horizontalOffset, view.Frame.Top),
							   view.Frame.Size));
		}

		void StylePincodesOnChange(string text)
		{
			if (text.Length == 0)
			{
				StyleEmptyPincode(Pincode1);
				StyleEmptyPincode(Pincode2);
				StyleEmptyPincode(Pincode3);
				StyleEmptyPincode(Pincode4);
				StyleEmptyPincode(Pincode5);
				StyleDefaultBackground();
			}
			if (text.Length == 1)
			{
				StyleFilledPincode(Pincode1);
				StyleEmptyPincode(Pincode2);
				StyleEmptyPincode(Pincode3);
				StyleEmptyPincode(Pincode4);
				StyleEmptyPincode(Pincode5);
				StyleDefaultBackground();
			}
			if (text.Length == 2)
			{
				StyleFilledPincode(Pincode1);
				StyleFilledPincode(Pincode2);
				StyleEmptyPincode(Pincode3);
				StyleEmptyPincode(Pincode4);
				StyleEmptyPincode(Pincode5);
				StyleDefaultBackground();
			}
			if (text.Length == 3)
			{
				StyleFilledPincode(Pincode1);
				StyleFilledPincode(Pincode2);
				StyleFilledPincode(Pincode3);
				StyleEmptyPincode(Pincode4);
				StyleEmptyPincode(Pincode5);
				StyleDefaultBackground();
			}
			if (text.Length == 4)
			{
				StyleFilledPincode(Pincode1);
				StyleFilledPincode(Pincode2);
				StyleFilledPincode(Pincode3);
				StyleFilledPincode(Pincode4);
				StyleEmptyPincode(Pincode5);
				StyleDefaultBackground();
			}
			if (text.Length == 5)
			{
				ValidatePincode();
			}
			if (text.Length > 5)
			{
				Textfield.Text = text.Substring(0, 5);
			}
			Console.WriteLine(text);
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}
    }
}