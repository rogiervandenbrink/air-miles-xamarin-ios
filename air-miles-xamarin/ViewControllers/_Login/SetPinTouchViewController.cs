﻿using Foundation;
using System;
using UIKit;
using System.Threading.Tasks;
using System.Threading;
using CoreGraphics;
using SDWebImage;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class SetPinTouchViewController : UIViewController
    {
		string FirstPincode { get; set; }

        public SetPinTouchViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			Textfield.EditingChanged += (object sender, EventArgs e) =>
			{
				if (Textfield.Text.Length == 0)
				{
					StyleEmptyPincode(Pincode1);
					StyleEmptyPincode(Pincode2);
					StyleEmptyPincode(Pincode3);
					StyleEmptyPincode(Pincode4);
					StyleEmptyPincode(Pincode5);
					StyleDefaultBackground();
				}
				if (Textfield.Text.Length == 1)
				{
					StyleFilledPincode(Pincode1);
					StyleEmptyPincode(Pincode2);
					StyleEmptyPincode(Pincode3);
					StyleEmptyPincode(Pincode4);
					StyleEmptyPincode(Pincode5);
					StyleDefaultBackground();
				}
				if (Textfield.Text.Length == 2)
				{
					StyleFilledPincode(Pincode1);
					StyleFilledPincode(Pincode2);
					StyleEmptyPincode(Pincode3);
					StyleEmptyPincode(Pincode4);
					StyleEmptyPincode(Pincode5);
					StyleDefaultBackground();
				}
				if (Textfield.Text.Length == 3)
				{
					StyleFilledPincode(Pincode1);
					StyleFilledPincode(Pincode2);
					StyleFilledPincode(Pincode3);
					StyleEmptyPincode(Pincode4);
					StyleEmptyPincode(Pincode5);
					StyleDefaultBackground();
				}
				if (Textfield.Text.Length == 4)
				{
					StyleFilledPincode(Pincode1);
					StyleFilledPincode(Pincode2);
					StyleFilledPincode(Pincode3);
					StyleFilledPincode(Pincode4);
					StyleEmptyPincode(Pincode5);
					StyleDefaultBackground();
				}
				if (Textfield.Text.Length == 5)
				{
					SetPincode();
				}
				if (Textfield.Text.Length > 5)
				{
					Textfield.Text = Textfield.Text.Substring(0, 5);
				}
				Console.WriteLine(Textfield.Text);
			};
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Initialize();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
		}

		private void Initialize()
		{
			//NavigationItem.SetHidesBackButton(true, false);

			//NavigationItem.TitleView.Hidden = true;

			var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

			if (NavCtrl != null)
			{
				if (!string.IsNullOrEmpty(NavCtrl.Routing))
				{
					// Hide back button
					NavigationItem.SetHidesBackButton(true, false);

					// Set close icon
					this.NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Stop, (sender, args) =>
					{
						Textfield.ResignFirstResponder();
						DismissViewController(true, null);
					})
					, true);
				}
				else {
					NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
					UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
					{
						NavigationController.PopViewController(true);
					}), true);
				}
			}
			else {
				NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
				UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
				{
					NavigationController.PopViewController(true);
				}), true);
			}


			Textfield.BecomeFirstResponder();
			Textfield.Hidden = true;

			ErrorMessageLabel.Hidden = true;
			ResetPincodes();

			AMMain.SetAvatar(UserAvatar, "placeholder.png");
		}

		protected void SetPincode()
		{
			if (string.IsNullOrEmpty(FirstPincode))
			{
				FirstPincode = Textfield.Text;
				StyleRepeatPincodes();
			}
			else {
				if (FirstPincode == Textfield.Text)
				{
					StyleSuccesPincodes();;
				}
				else {
					StyleIncorrectPincodes();
				}
			}
		}

		// TODO: MAKE PUBLIC CLASSES
		private void StyleDefaultBackground()
		{
			View.BackgroundColor = UIColor.FromRGB(217, 236, 246);
		}

		private void StyleEmptyPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.Clear;
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(172, 209, 229).CGColor;
		}

		private void StyleFilledPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.FromRGB(0, 125, 195);
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
		}

		private void StyleCorrectPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.FromRGB(159, 211, 61);
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(159, 211, 61).CGColor;
		}

		private void StyleIncorrectPincode(UIView pincode)
		{
			pincode.BackgroundColor = UIColor.FromRGB(242, 42, 94);
			pincode.Layer.CornerRadius = pincode.Bounds.Height / 2;
			pincode.Layer.BorderWidth = 2f;
			pincode.Layer.BorderColor = UIColor.FromRGB(242, 42, 94).CGColor;
		}

		private async void StyleRepeatPincodes()
		{
			View.BackgroundColor = UIColor.FromRGB(238, 244, 225);

			DetailMessageLabel.Text = "Herhaal de zojuist ingestelde pincode.";

			StyleCorrectPincode(Pincode1);
			StyleCorrectPincode(Pincode2);
			StyleCorrectPincode(Pincode3);
			StyleCorrectPincode(Pincode4);
			StyleCorrectPincode(Pincode5);

			await AnimateSucces(Pincodes);
			ResetPincodes();
		}

		private async void StyleSuccesPincodes()
		{
			// Set Pincode
			KeychainHelpers.SetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), Textfield.Text, AMLocalStorage.SecurePincode, Security.SecAccessible.WhenUnlockedThisDeviceOnly, true);
			NSUserDefaults.StandardUserDefaults.SetBool(true, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID);
			LocalStorage.ResetPinAttempts();

			Console.WriteLine("BELANGRIJK: " + KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true));

			View.BackgroundColor = UIColor.FromRGB(238, 244, 225);

			DetailMessageLabel.Text = "Je pincode is opgeslagen.";

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Inloggegevens", "Gebruik pin", "Ja", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			if (Textfield.CanResignFirstResponder)
			{
				Textfield.ResignFirstResponder();
			}

			StyleCorrectPincode(Pincode1);
			StyleCorrectPincode(Pincode2);
			StyleCorrectPincode(Pincode3);
			StyleCorrectPincode(Pincode4);
			StyleCorrectPincode(Pincode5);

			await AnimateSucces(Pincodes);

			RedirectToRoute();
		}

		private void RedirectToRoute()
		{
			//NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
			App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;
			if (NavigationController != null && NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
			{
				var NavCtrl = NavigationController as EnterPinTouchNavigationViewController;

				string Routing = NavCtrl.Routing;

				if (string.IsNullOrEmpty(Routing))
				{
					if (Routing == "Dismiss")
					{
						DismissViewController(true, null);
					}
					if (Routing == "ResetBlockedPin")
					{
						LocalStorage.DeletePin();
						DismissViewController(true, null);
					}
					if (Routing == "TransactiesViewController")
					{
						if (NavigationController.GetType() == typeof(EnterPinTouchNavigationViewController))
						{
							DismissViewController(true, null);
						}
						else {
							TransactiesViewController Redirect = App.Storyboard.InstantiateViewController("TransactiesViewController") as TransactiesViewController;
							NavigationController.PushViewController(Redirect, false);
						}
					}
					if (Routing == "SetPinTouchViewController")
					{
						SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
						NavigationController.PushViewController(Redirect, false);
					}
					if (Routing == "InstellingenViewController")
					{
						InstellingenViewController Redirect = App.Storyboard.InstantiateViewController("InstellingenViewController") as InstellingenViewController;
						NavigationController.PushViewController(Redirect, false);
					}
				}
				else {
					DismissViewController(true, null);
				}
			}
			else {
				DismissViewController(true, null);
				//NavigationController.PopViewController(false);
			}
		}

		private async void StyleIncorrectPincodes()
		{
			DetailMessageLabel.Text = "Je pincode komt niet overeen.";
			View.BackgroundColor = UIColor.FromRGB(255, 234, 239);

			StyleIncorrectPincode(Pincode1);
			StyleIncorrectPincode(Pincode2);
			StyleIncorrectPincode(Pincode3);
			StyleIncorrectPincode(Pincode4);
			StyleIncorrectPincode(Pincode5);

			await AnimateError(Pincodes);
			DetailMessageLabel.Text = "Herhaal pincode.";

			ResetPincodes();
		}

		private void ResetPincodes()
		{
			Textfield.Text = "";
			StyleEmptyPincode(Pincode1);
			StyleEmptyPincode(Pincode2);
			StyleEmptyPincode(Pincode3);
			StyleEmptyPincode(Pincode4);
			StyleEmptyPincode(Pincode5);
			StyleDefaultBackground();
		}

		private static async Task AnimateError(UIView view)
		{
			await AnimateHorizontalMovementAsync(view, -10);
			await AnimateHorizontalMovementAsync(view, 20);
			await AnimateHorizontalMovementAsync(view, -20);
			await AnimateHorizontalMovementAsync(view, 20);
			await AnimateHorizontalMovementAsync(view, -15);
			await AnimateHorizontalMovementAsync(view, 10);
			await AnimateHorizontalMovementAsync(view, -5);
			Thread.Sleep(1250);
		}

		private static async Task AnimateSucces(UIView view)
		{
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			await AnimateHorizontalMovementAsync(view, 0);
			Thread.Sleep(1250);
		}

		private static async Task AnimateHorizontalMovementAsync(UIView view, float horizontalOffset)
		{
			await UIView.AnimateAsync(0.085, () => view.Frame =
			  new CGRect(new CGPoint(view.Frame.Left + horizontalOffset, view.Frame.Top),
							   view.Frame.Size));
		}
    }
}