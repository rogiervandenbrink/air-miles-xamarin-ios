﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("EnterPinTouchViewController")]
    partial class EnterPinTouchViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DetailMessageLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ErrorMessageLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ForgotPinButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton LoginButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Pincode1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Pincode2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Pincode3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Pincode4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Pincode5 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Pincodes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PincodeView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Textfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView UserAvatar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DetailMessageLabel != null) {
                DetailMessageLabel.Dispose ();
                DetailMessageLabel = null;
            }

            if (ErrorMessageLabel != null) {
                ErrorMessageLabel.Dispose ();
                ErrorMessageLabel = null;
            }

            if (ForgotPinButton != null) {
                ForgotPinButton.Dispose ();
                ForgotPinButton = null;
            }

            if (LoginButton != null) {
                LoginButton.Dispose ();
                LoginButton = null;
            }

            if (Pincode1 != null) {
                Pincode1.Dispose ();
                Pincode1 = null;
            }

            if (Pincode2 != null) {
                Pincode2.Dispose ();
                Pincode2 = null;
            }

            if (Pincode3 != null) {
                Pincode3.Dispose ();
                Pincode3 = null;
            }

            if (Pincode4 != null) {
                Pincode4.Dispose ();
                Pincode4 = null;
            }

            if (Pincode5 != null) {
                Pincode5.Dispose ();
                Pincode5 = null;
            }

            if (Pincodes != null) {
                Pincodes.Dispose ();
                Pincodes = null;
            }

            if (PincodeView != null) {
                PincodeView.Dispose ();
                PincodeView = null;
            }

            if (Textfield != null) {
                Textfield.Dispose ();
                Textfield = null;
            }

            if (UserAvatar != null) {
                UserAvatar.Dispose ();
                UserAvatar = null;
            }
        }
    }
}