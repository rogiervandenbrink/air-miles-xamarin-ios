﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class EnterPinTouchNavigationViewController : UINavigationController
    {
		public string Routing { get; set; }
		public bool ForgotPincode { get; set; }

        public EnterPinTouchNavigationViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Console.WriteLine("We will be routing the page to " + Routing);
			RedirectToDetailPage(NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID));
		}

		private void RedirectToDetailPage(bool pin){
			if (pin == true)
			{
				// redirect to page
				EnterPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("EnterPinTouchViewController") as EnterPinTouchViewController;
				PushViewController(Redirect, false);
			}

			if (pin == false)
			{
				// redirect to login page
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
				Redirect.Routing = Routing;
				PushViewController(Redirect, false);
			}
		}
    }
}