﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class TouchIdViewController : UIViewController
    {
        public TouchIdViewController (IntPtr handle) : base (handle)
        {
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			NSUserDefaults.StandardUserDefaults.SetBool(true, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertTouchID);

			TouchIdButton.TouchUpInside += TouchIdButtonTouchUpInside;
			SkipButton.TouchUpInside += SkipButtonTouchUpInside;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			NavigationController.NavigationBarHidden = true;
			SetAnalytics();
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(TouchIdButton);
		}

		void TouchIdButtonTouchUpInside(object sender, EventArgs ea)
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Eerste inlog", "Gebruik vingerafdruk", "Ja", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			DismissViewController(true, null);
		}

		void SkipButtonTouchUpInside(object sender, EventArgs ea)
		{
			NSUserDefaults.StandardUserDefaults.SetBool(false, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID);

			PincodeViewController Redirect = App.Storyboard.InstantiateViewController("PincodeViewController") as PincodeViewController;

			//Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Eerste inlog", "Gebruik vingerafdruk", "Nee", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();

			NavigationController.PushViewController(Redirect, false);

			//DismissViewController(true, null);
		}

		void SetAnalytics()
		{
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Eerste inlog: Vingerafdruk");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}
    }
}