﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class PincodeViewController : UIViewController
    {

        public PincodeViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();

			NSUserDefaults.StandardUserDefaults.SetBool(true, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.FirstAlertPinID);
			Console.WriteLine("IS SET " + NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Username + "-" + AMLocalStorage.FirstAlertPinID));

			PinButton.TouchUpInside += PinButtonTouchUpInside;
			SkipButton.TouchUpInside += SkipButtonTouchUpInside;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			NavigationController.NavigationBarHidden = true;
			SetAnalytics();
			if (NSUserDefaults.StandardUserDefaults.BoolForKey("InstellingenViewcontroller"))
			{
				SkipButton.Hidden = true;
				NSUserDefaults.StandardUserDefaults.SetBool(false, "InstellingenViewcontroller");
			}
			else
			{
				SkipButton.Hidden = false;
			}
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(PinButton);
		}

		void PinButtonTouchUpInside(object sender, EventArgs ea)
		{
			NavigationController.NavigationBarHidden = false;

			// Redirect to detail page
			SetPinTouchViewController SetPinModal = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
			ShowViewController(SetPinModal, this);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Eerste inlog", "Gebruik pin", "Ja", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		void SkipButtonTouchUpInside(object sender, EventArgs ea)
		{
			DismissViewController(true, null);
			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Eerste inlog", "Gebruik pin", "Nee", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		void SetAnalytics()
		{
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Eerste inlog: Pincode");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}
    }
}