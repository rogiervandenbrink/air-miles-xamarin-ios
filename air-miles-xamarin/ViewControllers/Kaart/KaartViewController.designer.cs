﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("KaartViewController")]
    partial class KaartViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ButtonView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField KaartnummerTextfield { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton SaldoButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonView != null) {
                ButtonView.Dispose ();
                ButtonView = null;
            }

            if (KaartnummerTextfield != null) {
                KaartnummerTextfield.Dispose ();
                KaartnummerTextfield = null;
            }

            if (SaldoButton != null) {
                SaldoButton.Dispose ();
                SaldoButton = null;
            }
        }
    }
}