﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;
using CoreGraphics;
using Xamarin;
using Air_Miles_Xamarin;
using System.Threading;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class ScanmodusViewController : UIViewController
	{
		
		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		public bool BarcodeState { get; set; }
		NetworkStatus Internet;

		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;

		UIView SelectedBorder;
		nfloat SelectedBorderX;

		nfloat startbrightness;

		private NSObject NotificationHandle;


		public ScanmodusViewController(IntPtr handle) : base(handle)
		{
		}

			public override void ViewDidLoad()
			{
				base.ViewDidLoad();
	        	nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
				nfloat SelectedBorderWidth = ScreenWidth / 2;

				if (!BarcodeState)
				{
					SelectedBorderX = 0;
				}
				else {
					SelectedBorderX = SelectedBorderWidth;
				}

				SelectedBorder = new UIView(new CGRect(SelectedBorderX, 0, SelectedBorderWidth, 4))
				{
					BackgroundColor = UIColor.FromRGB(0, 125, 195)
				};
			
				QRCodeButton.TouchUpInside += delegate
				{
					var GAIbarcode = DictionaryBuilder.CreateEvent("Scanmodus", "Tab keuze", "QR-Code", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAIbarcode);
					Gai.SharedInstance.Dispatch();

					BarcodeState = true;
					SetState(BarcodeState);
					AMMain.AnimateBorder(SelectedBorder, QRCodeButton.Center.X, SelectedBorder.Center.Y);
				};

				StreepjescodeButton.TouchUpInside += delegate
				{
					var GAIbarcode = DictionaryBuilder.CreateEvent("Scanmodus", "Tab keuze", "Barcode", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAIbarcode);
					Gai.SharedInstance.Dispatch();

					BarcodeState = false;
					SetState(BarcodeState);
					AMMain.AnimateBorder(SelectedBorder, StreepjescodeButton.Center.X, SelectedBorder.Center.Y);
				};

				this.Title = "";
				StreepjescodeButton.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width / 2, 40);
				QRCodeButton.Frame = new CGRect(UIScreen.MainScreen.Bounds.Width / 2, 0, UIScreen.MainScreen.Bounds.Width / 2, 40);


			Initialize();

			BorderView.AddSubview(SelectedBorder);

			UpdateBalance(true);

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			NotificationHandle = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.WillEnterForegroundNotification, HandleAppWillEnterForeground);
			UpdateBalance(false);
			KaartnummerLabel.Hidden =true;

			startbrightness = UIScreen.MainScreen.Brightness;
			UIScreen.MainScreen.Brightness = 1.0f;
		}


		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Scanmodus");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void HandleAppWillEnterForeground(NSNotification notification)
		{
			UpdateBalance(false);
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			SetAnalytics();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard) == false)
			{
				RemoveFromParentViewController();
			}

			UIScreen.MainScreen.Brightness = startbrightness;
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
		}

		private async void UpdateBalance(bool load)
		{
			UpdateLabels();

			try
			{
				Internet = Reachability.InternetConnectionStatus();

				// If internet
				if (Internet != NetworkStatus.NotReachable)
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

					if (CurrentKaart.Kaart.Error != true)
					{
						Console.WriteLine("Updating Card data");
						LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
						NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);

						if (load)
						{
							if (CurrentKaart.Kaart.Notifications != null && CurrentKaart.Kaart.Notifications.Length > 0)
							{
								if (CurrentKaart.Kaart.Notifications[0].Code == "AccountBlocked")
								{
									AMAlerts.DefaultAlert(this, AMStrings.ErrorCardBlockedTitle, AMStrings.ErrorCardBlockedMessage);
								}
							}
						}
					}
				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart scannen", "Exception: "+ ex}
                });
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}

			UpdateLabels();
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		private void Initialize()
		{
			// Set Barcode state
			SetState(BarcodeState);

			// If card data
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == true || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard) == true)
			{
				UpdateLabels();
			}
		}

		// Set all dynamic labels
		private void UpdateLabels()
		{
			SetKaartnummerLabel();
			SetQRCode();
			SetBarcode();
			SetEanLabel();
		}

		private void SetQRCode()
		{
			try
			{
				// Create the qrcode
				var QRCodeWriter = new ZXing.Mobile.BarcodeWriter
				{
					Format = ZXing.BarcodeFormat.QR_CODE,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 100,
						Height = 100,
						Margin = 0
					}
				};

				QRCode.Image = QRCodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart scannen", "Exception: "+ ex}
                });
			}
		}

		private void SetBarcode()
		{
			try
			{

				// Create the barcode
				var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
				{
					Format = ZXing.BarcodeFormat.EAN_13,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 300,
						Height = 70,
						Margin = 0
					}
				};

				Barcode.Image = BarcodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart scannen", "Exception: "+ ex}
                });
			}
		}

		private void SetEanLabel()
		{
			EANLabel.Text = " " + SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));

			var EANStyle = new NSAttributedString(EANLabel.Text,
				new UIStringAttributes()
				{
					KerningAdjustment = 15
				});

			EANLabel.AttributedText = EANStyle;
		}

		private void SetKaartnummerLabel()
		{
			KaartnummerLabel.Text = "KAARTNUMMER: " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
		}


		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		private void SetState(bool BarcodeState)
		{
			if (BarcodeState)
			{
				// Show and hide code

				// Enable and disable buttons
				StreepjescodeView.Hidden = true;
				QRCode.Hidden = false;
				StreepjescodeButton.Enabled = true;
				QRCodeButton.Enabled = false;

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Tabkeuze", "QR-Code", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}

			if (!BarcodeState)
			{
				// Show and hide code

				// Enable and disable buttons
				StreepjescodeView.Hidden = false;
				QRCode.Hidden = true;
				StreepjescodeButton.Enabled = false;
				QRCodeButton.Enabled = true;

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Tabkeuze", "Barcode", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
		}
	}
}