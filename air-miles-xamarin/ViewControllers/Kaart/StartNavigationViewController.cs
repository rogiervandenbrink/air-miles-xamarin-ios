﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class StartNavigationViewController : UINavigationController
    {
        public StartNavigationViewController (IntPtr handle) : base (handle)
        {
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			RedirectToDetailPage();
		}

		protected void RedirectToDetailPage()
		{
			StartKaartAanvragenViewController Redirect = App.Storyboard.InstantiateViewController("StartKaartAanvragenViewController") as StartKaartAanvragenViewController;
			PushViewController(Redirect, false);

		}
    }
}