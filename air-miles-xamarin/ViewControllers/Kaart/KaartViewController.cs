﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using Google.Analytics;
using Xamarin;
using Air_Miles_Xamarin;
using System.Threading;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class KaartViewController : UITableViewController
    {
		LoadingOverlay LoadingView;
		NetworkStatus Internet;
		NetworkOverlay NetworkView;

		CancellationTokenSource cancelToken;

		public KaartViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Initialize();
			cancelToken = new CancellationTokenSource();

			SaldoButton.TouchUpInside += SaldoButtonTouchUpInside;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			RedirectToDetailPage(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin));
			SetAnalytics();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == true || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard) == true)
			{
				RemoveFromParentViewController();
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
		}

		private void Initialize()
		{
            //show create card?
			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.FirstKaartAanvragen) && !NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard))
			{
				AMMain.ShowFirstKaartAanmaken(NavigationController);

			}

			HideBackButton();
			StyleButton();

            //do webservice for balance?
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
			{
				if (AMMain.DoWebService(AMLocalStorage.KaartService))
				{
					GetBalance(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
				}
			}
			AMMain.AddDoneButton(KaartnummerTextfield);

			// Nav Button
			this.NavigationItem.SetRightBarButtonItem(
				new UIBarButtonItem(UIImage.FromBundle("IconAddCard")
				, UIBarButtonItemStyle.Plain
				, (sender, args) =>
				{
					Internet = Reachability.InternetConnectionStatus();

					if (Internet != NetworkStatus.NotReachable)
					{
						BarcodeScan();
					}
					else {
						new UIAlertView("Geen internetverbinding", "Helaas! Je hebt momenteel geen verbinding met het internet. Probeer het bij goed bereik nogmaals.", null, "OK", null).Show();
					}
				})
			, true);
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		/// <summary>
		/// Style the 'Check Saldo' button
		/// </summary>
		private void StyleButton()
		{
			ButtonView.Layer.BorderColor = UIColor.FromRGB(0, 125, 195).CGColor;
			ButtonView.Layer.BorderWidth = 1f;
			ButtonView.Layer.CornerRadius = ButtonView.Bounds.Height / 2;
			ButtonView.Layer.MasksToBounds = true;
		}

		private void ValidateBalance(string kaartnummer)
		{
			KaartnummerTextfield.ResignFirstResponder();

			if (kaartnummer.Length != 9)
			{
				Console.WriteLine("Ongeldig kaartnummer");
				AMAlerts.DefaultAlert(this, AMStrings.ErrorCardInvalidTitle, AMStrings.ErrorCardInvalidMessage);
			}
			else {
				GetBalance(kaartnummer);
			}
		}

		public async void GetBalance(string kaartnummer)
		{
			if (NetworkView != null)
			{
				NetworkView.Hide();
			}
			SetLoader(AMStrings.LoadingCardData);

			try
			{
				KaartService KaartService = new KaartService();
				KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(kaartnummer, cancelToken);

				if (CurrentKaart.Error[0].Code == "E00")
				{
					LocalStorage.SetCard(CurrentKaart.Kaart, kaartnummer);

					string service = AMLocalStorage.KaartService;
					NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
                    NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);

					if (!string.IsNullOrEmpty(CurrentKaart.Kaart.IncorrectPersonalInformation))
					{
						NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.IncorrectPersonalInformation);
						NSUserDefaults.StandardUserDefaults.SetString(CurrentKaart.Kaart.IncorrectPersonalInformation, AMLocalStorage.IncorrectPersonalInformationMessage);
					}
					if (!string.IsNullOrEmpty(CurrentKaart.Kaart.CardBlockedMessage)) 
					{
						NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.CardBlockedMessage);
						NSUserDefaults.StandardUserDefaults.SetString(CurrentKaart.Kaart.CardBlockedMessage, AMLocalStorage.CardBlockedMessage);
                        NSUserDefaults.StandardUserDefaults.Synchronize();
                        ShowDetailPage(true);
					}else
                    {
                        ShowDetailPage(false);
                    }

					NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);

					LoadingView.Hide();
				}
				else {
					LoadingView.Hide();

					AMAlerts.DefaultAlert(this, AMStrings.ErrorCard, CurrentKaart.Error[0].Message);

					if (CurrentKaart.Kaart.Notifications != null && CurrentKaart.Kaart.Notifications.Length > 0)
					{
						DislayNotificationError(CurrentKaart.Kaart.Notifications[0].Code);
					}
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart", "Exception: "+ ex}
                });
				if (LoadingView != null)
				{
					Console.WriteLine("hide loadingview");
					LoadingView.Hide();
				}
				Console.WriteLine("exception kaartservice: "+ex);
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				}

			}
		}

		protected void DislayNotificationError(string error)
		{
			if (error == "AccountBlocked")
			{
				// do nothing
			}
			else if (error == "CardNumberUnknown")
			{
				AMAlerts.DefaultAlert(this, AMStrings.ErrorCardNotFoundTitle, AMStrings.ErrorCardNotFoundMessage);
			}
			else if (error == "MainError")
			{
				AMAlerts.DefaultAlert(this, AMStrings.ErrorCardNotFoundTitle, AMStrings.ErrorCardNoConnectionMessage);
			}
			else {
				AMAlerts.DefaultAlert(this, AMStrings.ErrorCardNotFoundTitle, AMStrings.ErrorCardNoConnectionMessage);
			}
		}

		protected void RedirectToDetailPage(bool login)
		{
			if (login == true && NavigationController != null)
			{
				if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
				{
					// Code that uses features from Xamarin.iOS 10.0 and later
					SaldoTableViewController Redirect = App.Storyboard.InstantiateViewController("SaldoTableViewController") as SaldoTableViewController;
					NavigationController.PushViewController(Redirect, false);
				}
				else
				{
					// Code to support earlier iOS versions
					SaldoViewController Redirect = App.Storyboard.InstantiateViewController("SaldoViewController") as SaldoViewController;
					NavigationController.PushViewController(Redirect, false);
				}

			}
		}

		private void ShowDetailPage(bool cardblocked)
		{
			if (NavigationController != null)
			{
				if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
				{
					// Code that uses features from Xamarin.iOS 10.0 and later
					SaldoTableViewController Redirect = App.Storyboard.InstantiateViewController("SaldoTableViewController") as SaldoTableViewController;
                    Redirect.ShowCardBlocked = cardblocked;
					NavigationController.PushViewController(Redirect, false);
				}
				else
				{
					// Code to support earlier iOS versions
					SaldoViewController Redirect = App.Storyboard.InstantiateViewController("SaldoViewController") as SaldoViewController;
                    Redirect.ShowCardBlocked = cardblocked;
					NavigationController.PushViewController(Redirect, false);
				}
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		void SaldoButtonTouchUpInside(object sender, EventArgs ea)
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable)
			{
				// Google Analytics
                var GAIaddCardNr = DictionaryBuilder.CreateEvent("Kaart uitgelogd", "Kaartnummer toevoegen", "Kaartnummer toevoegen", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAIaddCardNr);
				Gai.SharedInstance.Dispatch();

				ValidateBalance(KaartnummerTextfield.Text);
			}
			else {
				AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
			}
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Kaart uitgelogd");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private async void BarcodeScan()
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable)
			{
				try
				{
    				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Barcode scanner");
    				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

    				var scanner = new ZXing.Mobile.MobileBarcodeScanner();
    				scanner.TopText = "Scan je streepjescode";
    				scanner.CancelButtonText = "Annuleren";
    				scanner.FlashButtonText = "Flits";
    				var result = await scanner.Scan();

    					if (result != null)
    					{
    						// Check if string length is equal to 13 characters (Air Miles barcode length)
    						if (result.Text.Length == 13)
    						{
    							ValidateBalance(result.Text.Substring(3, 9));
    						}
    						else {
    							new UIAlertView("Ongeldig kaartnummer", "Let op! Je hebt geen geldig Air Miles kaartnummer ingevoerd.", null, "OK", null).Show();
    						}

    						// GA
                            var GAIcameraScan = DictionaryBuilder.CreateEvent("Barcode scanner", "Barcode gevonden", "Barcode gevonden", null).Build();
    						Gai.SharedInstance.DefaultTracker.Send(GAIcameraScan);
    						Gai.SharedInstance.Dispatch();
    					}
				}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Kaart", "Exception: "+ ex}
                    });

				}
			}
			else {
				AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
			}
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		private void ShowPopupMEssage(string message)
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpMessageTitle;
			alert.AddButton("Naar gegevens");
			alert.AddButton("Annuleren");
			alert.Message = message;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpClicked;
			alert.Show();
		}

		void ButtonPopUpClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				GegevensSegue();

			}
			else
			{
				// Cancel button

			}
		}

		void GegevensSegue()
		{
			{
                if (NavigationController != null)
                {
                    ProfileViewController Redirect = App.Storyboard.InstantiateViewController("ProfileViewController") as ProfileViewController;
                    Redirect.Title = "Gegevens";
                    NavigationController.PushViewController(Redirect, true);
                }
			}
		}
    }
}