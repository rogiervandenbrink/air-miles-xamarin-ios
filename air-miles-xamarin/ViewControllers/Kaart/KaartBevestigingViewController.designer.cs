﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("KaartBevestigingViewController")]
    partial class KaartBevestigingViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Backgroundview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton KaartBevestigingOkButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Backgroundview != null) {
                Backgroundview.Dispose ();
                Backgroundview = null;
            }

            if (KaartBevestigingOkButton != null) {
                KaartBevestigingOkButton.Dispose ();
                KaartBevestigingOkButton = null;
            }
        }
    }
}