﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("ScanmodusViewController")]
    partial class ScanmodusViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Barcode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BorderView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EANLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel KaartnummerLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView QRCode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton QRCodeButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView QRCodeView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton StreepjescodeButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView StreepjescodeView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Barcode != null) {
                Barcode.Dispose ();
                Barcode = null;
            }

            if (BorderView != null) {
                BorderView.Dispose ();
                BorderView = null;
            }

            if (EANLabel != null) {
                EANLabel.Dispose ();
                EANLabel = null;
            }

            if (KaartnummerLabel != null) {
                KaartnummerLabel.Dispose ();
                KaartnummerLabel = null;
            }

            if (QRCode != null) {
                QRCode.Dispose ();
                QRCode = null;
            }

            if (QRCodeButton != null) {
                QRCodeButton.Dispose ();
                QRCodeButton = null;
            }

            if (QRCodeView != null) {
                QRCodeView.Dispose ();
                QRCodeView = null;
            }

            if (StreepjescodeButton != null) {
                StreepjescodeButton.Dispose ();
                StreepjescodeButton = null;
            }

            if (StreepjescodeView != null) {
                StreepjescodeView.Dispose ();
                StreepjescodeView = null;
            }
        }
    }
}