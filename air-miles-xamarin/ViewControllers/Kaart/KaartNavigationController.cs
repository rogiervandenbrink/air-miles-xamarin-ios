﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class KaartNavigationController : UINavigationController
    {

        public KaartNavigationController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
  		}

        public override void ViewWillAppear(bool animated){

            RedirectToDetailPage();
            base.ViewWillAppear(animated);
        }

		protected void RedirectToDetailPage()
		{
			Console.WriteLine("user has login = "+NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin)+" user has card "+NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard));
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard))
			{
				if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
				{
					// Code that uses features from Xamarin.iOS 10.0 and later
					SaldoTableViewController Redirect = App.Storyboard.InstantiateViewController("SaldoTableViewController") as SaldoTableViewController;
					PushViewController(Redirect, false);
				}
				else
				{
					// Code to support earlier iOS versions
					SaldoViewController Redirect = App.Storyboard.InstantiateViewController("SaldoViewController") as SaldoViewController;
					PushViewController(Redirect, false);
				}

			}

			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) && !NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard))
			{
				KaartViewController Redirect = App.Storyboard.InstantiateViewController("KaartViewController") as KaartViewController;
				PushViewController(Redirect, false);

			}
		}
    }
}