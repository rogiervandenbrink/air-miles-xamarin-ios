﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using Google.Analytics;
using System.Threading;

namespace airmilesxamarin
{
    public partial class KaartBevestigingViewController : UIViewController
    {

		UIViewController Controller;
		LoadingOverlay LoadingView;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		public string Routing { get; set; }

        public KaartBevestigingViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Initialize();
			Backgroundview.BackgroundColor = UIColor.FromRGB(1,125,195);

			if (NavigationController != null)
			{
				if (Routing == "Meer")
				{
					//NavigationController.SetNavigationBarHidden(true, false);
				}
				else
				{

					NavigationController.SetNavigationBarHidden(false, false);
				}
			}

			KaartBevestigingOkButton.TouchUpInside += KaartBevestigingOkButton_TouchUpInside;
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();

		}

		private void Initialize()
		{
            //style button
			AMStyle.RoundedButton(KaartBevestigingOkButton);

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanmelden: kaart - klaar");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}
		async void GetBalance(string cardnumber)
		{
			SetLoader(AMStrings.LoadingCardData);
			try
			{
				KaartService KaartService = new KaartService();
				KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(cardnumber, cancelToken);

				if (CurrentKaart.Kaart.Error != true)
				{
					LocalStorage.SetCard(CurrentKaart.Kaart, cardnumber);

					NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);

					if (LoadingView != null)
					{
						LoadingView.Hide();
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("exception kaartservice: " + ex);
				if (AMMain.CanShowError())
				{
					AMAlerts.DefaultAlert(this, "Fout bij opvragen kaartgegevens", AMStrings.ErrorDigitalCardNoConnection);
				}
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
			}
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

			// do first a control on the Identifier for your segue
			if (segue.Identifier.Equals("BevestigingNaarKaartSegue"))
			{
				if (Routing == "Meer")
				{
					Routing = "Meer";
				}
                //set nofirstpintouch to true
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NoFirstPinTouch);
			}
		}

		void KaartBevestigingOkButton_TouchUpInside (object sender, EventArgs e)
		{
			// OK button
			PerformSegue("CardtoDigitalAccountSegue", this);
			NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.RedirectToDigitalAccount);
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}
	}
}