﻿using Foundation;
using System;
using UIKit;
using Google.Analytics;
using System.Threading;
using CoreGraphics;
using Xamarin;
using Air_Miles_Xamarin;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PassKit;
using System.IO;
using NotificationCenter;
using ObjCRuntime;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;
using System.Globalization;

namespace airmilesxamarin
{
    public partial class SaldoTableViewController : UITableViewController
    {

		public bool BarcodeState { get; set; }
		public bool ShowPopUp { get; set; }
        public bool ShowCardBlocked { get; set; }
        public bool ShowValidity = false;
		public string PopUpMessage { get; set; }
		NetworkStatus Internet;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		// Overlays
		LoadingOverlay LoadingView;
		NetworkOverlay NetworkView;

		//refreshlayout
		UIRefreshControl RefreshControl;

		UIView SelectedBorder;
		nfloat SelectedBorderX;

		private NSObject NotificationHandle;

		PKPassLibrary library;

		public SaldoTableViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
			nfloat SelectedBorderWidth = ScreenWidth / 2;

			this.Title = "Kaart";

			if (!BarcodeState)
			{
				SelectedBorderX = 0;
			}
			else {
				SelectedBorderX = SelectedBorderWidth;
			}

			SelectedBorder = new UIView(new CGRect(SelectedBorderX, 0, SelectedBorderWidth, 4))
			{
				BackgroundColor = UIColor.FromRGB(0, 125, 195)
			};

			QRCodeButton.TouchUpInside += delegate
			{
                var GAIbarcode = DictionaryBuilder.CreateEvent("Kaart", "Tab setting", "QR-Code", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAIbarcode);
				Gai.SharedInstance.Dispatch();

				BarcodeState = true;
				SetState(BarcodeState);
				AMMain.AnimateBorder(SelectedBorder, QRCodeButton.Center.X, SelectedBorder.Center.Y);
			};

			StreepjescodeButton.TouchUpInside += delegate
			{
				var GAIbarcode = DictionaryBuilder.CreateEvent("Kaart", "Tab setting", "Barcode", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAIbarcode);
				Gai.SharedInstance.Dispatch();

				BarcodeState = false;
				SetState(BarcodeState);
				AMMain.AnimateBorder(SelectedBorder, StreepjescodeButton.Center.X, SelectedBorder.Center.Y);
			};
			AMStyle.BorderedBlueButton(ButtonQRCodeButton);

            ValidityCardLabel.AdjustsFontSizeToFitWidth = true;

			ButtonQRCodeButton.TouchUpInside += delegate
				{					
					if (BarcodeState)
					{
						ScanmodusViewController Redirect = App.Storyboard.InstantiateViewController("ScanmodusViewController") as ScanmodusViewController;
						Redirect.BarcodeState = true;
						NavigationController.PushViewController(Redirect, true);

					}
					else
					{
						ScanmodusViewController Redirect = App.Storyboard.InstantiateViewController("ScanmodusViewController") as ScanmodusViewController;
						Redirect.BarcodeState = false;
						NavigationController.PushViewController(Redirect, true);
					}
				};

			DeleteCard.TouchUpInside += delegate
			{
				DeleteCardAlert();
			};

            WalletButton.TouchUpInside += delegate {
                bool passwordforusername = !string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true));
                bool session = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session);
                bool useremail = !string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail));

                if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail)) && !string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
                {
                    ShowPopupWallet();
                }else
                {
                    ShowWalletAlertMessage();
                }
            };

			Barcode.UserInteractionEnabled = true;
			QRCode.UserInteractionEnabled = true;

			UITapGestureRecognizer tapGesture = new UITapGestureRecognizer(OnTapped);
			tapGesture.NumberOfTapsRequired = 2;
			Barcode.AddGestureRecognizer(tapGesture);
			QRCode.AddGestureRecognizer(tapGesture);

			Initialize();

			BorderView.AddSubview(SelectedBorder);

			// STYLE THE NAVIGATION BAR
			this.NavigationController.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
			this.NavigationController.NavigationBar.ShadowImage = new UIImage();

			this.NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes()
			{
				ForegroundColor = UIColor.White
			};

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				TableView.AddSubview(RefreshControl);
			}
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;

			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NoFirstPinTouch) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				AMMain.ShowFirstPinToucAlert(NavigationController);
			}

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				NavigationItem.RightBarButtonItem = null;
				DeleteCard.Hidden = true;
			}
			else {
				this.NavigationItem.SetRightBarButtonItem(
				new UIBarButtonItem(UIBarButtonSystemItem.Trash, (sender, args) =>
				{
					// button was clicked
					DeleteCardAlert();

				})
				, true);
				DeleteCard.Hidden = true;

			}

			var controller = NCWidgetController.GetWidgetController();
			controller.SetHasContent(true, "nl.airmiles.app.air_miles_ios.airmileswidget");

			NotificationHandle = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.WillEnterForegroundNotification, HandleAppWillEnterForeground);

			if (AMMain.DoWebService(AMLocalStorage.KaartService) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSession))
			{
				UpdateBalance(true);
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.NewSession);

			}

		}


		void ButtonClicked(object sender, UIButtonEventArgs e)

		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{

			}
			else {

				// Cancel button
			}
		}



		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Kaart ingelogd");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void HandleAppWillEnterForeground(NSNotification notification)
		{
			//UpdateBalance(false);
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			RedirectToRoot(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin));
			SetAnalytics();
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.IncorrectPersonalInformation))
			{
				ShowPopupMEssage(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.IncorrectPersonalInformationMessage));
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.IncorrectPersonalInformation);
			}
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.CardBlockedMessage) || ShowCardBlocked)
			{
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardBlockedMessage));
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.CardBlockedMessage);
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard) == false)
			{
				RemoveFromParentViewController();
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();
		}

		private async void UpdateBalance(bool load)
		{
			try
			{
				Internet = Reachability.InternetConnectionStatus();

				// If internet
				if (Internet != NetworkStatus.NotReachable)
				{
					KaartService KaartService = new KaartService();
					KaartRoot CurrentKaart = await KaartService.GetKaartDataV2(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), cancelToken);

					if (CurrentKaart.Error[0].Code == "E00")
					{
						Console.WriteLine("Updating Card data " + JsonConvert.SerializeObject(CurrentKaart.Kaart));
						LocalStorage.SetCard(CurrentKaart.Kaart, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
						string service = AMLocalStorage.KaartService;
						NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
						
						NSUserDefaults.StandardUserDefaults.SetBool(CurrentKaart.Kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);

                        UpdateLabels();
						
                        //update widget
                        if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)))
						{
							NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
							shared.SetString(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), AMLocalStorage.CardNumber);
							shared.Synchronize();
						}
						if (!string.IsNullOrEmpty(CurrentKaart.Kaart.CardBlockedMessage))
						{
							AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentKaart.Kaart.CardBlockedMessage);
							// Google analaytics
							var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart", "Kaart ingelogd", "Kaart geblokkeerd", null).Build();
							Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
							Gai.SharedInstance.Dispatch();
						}
						else if (CurrentKaart.Kaart.Redirect)
						{
							ShowPopupRedirect("Bij je kaartnummer is nog geen account aangemaakt. Wil je deze nu aanmaken?");
							// Google analaytics
							var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart", "Kaart ingelogd", "Account aanmaken getoond", null).Build();
							Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
							Gai.SharedInstance.Dispatch();
						}

						else if (!string.IsNullOrEmpty(CurrentKaart.Kaart.IncorrectPersonalInformation))
						{
							ShowPopupMEssage(CurrentKaart.Kaart.IncorrectPersonalInformation);
							// Google analaytics
							var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart", "Kaart ingelogd", "Incorrecte persoonsgegevens", null).Build();
							Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
							Gai.SharedInstance.Dispatch();
						}
					}
                    else
                    {
						if (load)
						{
							if (CurrentKaart.Error[0].Code != "E00")
							{
								if (CurrentKaart.Error[0].Code == "E94")
								{
									AMAlerts.DefaultAlert(this, AMStrings.ErrorCardBlockedTitle, CurrentKaart.Error[0].Message);
								}
								if (CurrentKaart.Error[0].Code == "E71" || CurrentKaart.Error[0].Code == "E91")
								{
									// Logout
									LocalStorage.Logout(TabBarController);
									DeleteBalance();
									AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentKaart.Error[0].Message);
								}
								else
								{
									AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, CurrentKaart.Error[0].Message);
								}
							}
						}
					}
				}
				else
				{
					AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart", "Exception: "+ ex}
                });
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
			}

			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		private void Initialize()
		{
			// Hide the back button from navigation controller
			HideBackButton();

			// Set Barcode state
			SetState(BarcodeState);

			// Style button
			AMStyle.RoundedButton(DeleteCard);

			// If card data
			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == true || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard) == true)
			{
				UpdateLabels();
			}
		}

		// Set all dynamic labels
		private void UpdateLabels()
		{
			SetCollectorsLabel();
			SetBalanceLabel();
			SetDatetimeLabel();
			SetKaartnummerLabel();
			SetQRCode();
			SetBarcode();
			SetEanLabel();
            SetTextValidity();
            TableView.ReloadData();
		}

		private void SetQRCode()
		{
			try
			{
				// Create the qrcode
				var QRCodeWriter = new ZXing.Mobile.BarcodeWriter
				{
					Format = ZXing.BarcodeFormat.QR_CODE,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 100,
						Height = 100,
						Margin = 0
					}
				};
				QRCode.Image = QRCodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart", "Exception: "+ ex}
                });
			}
		}

		private void SetBarcode()
		{	
			try
			{
				// Create the barcode
				var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
				{
					Format = ZXing.BarcodeFormat.EAN_13,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 300,
						Height = 70,
						Margin = 0
					}
				};

				Barcode.Image = BarcodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
				}
			catch (Exception ex)
			{
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Kaart", "Exception: "+ ex}
                });
			}
		}

		private void SetEanLabel()
		{
			EANLabel.Text = " " + SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));

			var EANStyle = new NSAttributedString(EANLabel.Text,
				new UIStringAttributes()
				{
					KerningAdjustment = 15
				});

			EANLabel.AttributedText = EANStyle;
		}



		private void SetCollectorsLabel()
		{
			if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.Collectors) > 0)
			{
				CollectorsLabel.Text = "Momenteel hebben jullie";
			}
			else {
				CollectorsLabel.Text = "Momenteel heb je";
			}
		}

		private void SetKaartnummerLabel()
		{
			KaartnummerLabel.Text = "KAARTNUMMER: " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
		}

		private void SetBalanceLabel()
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance)))
			{
				BalanceLabel.Text = AMMain.SetBalanceFormat(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance));
			}
			else
			{
				BalanceLabel.Text = "0";
			}
		}

		private void SetDatetimeLabel()
		{
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.BalanceDate)))
			{
				DatetimeLabel.Text = "BIJGEWERKT T/M " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.BalanceDate).ToUpper();
			}
			else
			{
				DatetimeLabel.Text = "BIJGEWERKT T/M: ONBEKEND";
			}
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		void OnTapped(UITapGestureRecognizer tap)
		{
			Console.WriteLine("tapped");
			if (BarcodeState)
			{
				ScanmodusViewController Redirect = App.Storyboard.InstantiateViewController("ScanmodusViewController") as ScanmodusViewController;
				Redirect.BarcodeState = true;
				NavigationController.PushViewController(Redirect, true);
			}
			else
			{
				ScanmodusViewController Redirect = App.Storyboard.InstantiateViewController("ScanmodusViewController") as ScanmodusViewController;
				Redirect.BarcodeState = false;
				NavigationController.PushViewController(Redirect, true);
			}
		}

		protected void RedirectToRoot(bool login)
		{
			if (!login && !NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard) && NavigationController != null)
			{
				KaartViewController Redirect = App.Storyboard.InstantiateViewController("KaartViewController") as KaartViewController;
				NavigationController.PushViewController(Redirect, false);
			}
		}

		private void SetState(bool BarcodeState)
		{
			if (BarcodeState)
			{
				// Show and hide code

				// Enable and disable buttons
				StreepjescodeView.Hidden = true;
				QRCode.Hidden = false;
				StreepjescodeButton.Enabled = true;
				QRCodeButton.Enabled = false;

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Tabkeuze", "QR-Code", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}

			if (!BarcodeState)
			{
				// Show and hide code

				// Enable and disable buttons
				StreepjescodeView.Hidden = false;
				QRCode.Hidden = true;
				StreepjescodeButton.Enabled = false;
				QRCodeButton.Enabled = true;

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Tabkeuze", "Barcode", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
		}

		void Refresh()
		{
			UpdateBalance(true);
			var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Kaart", "Pull to refresh", "Pull to refresh", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
			Gai.SharedInstance.Dispatch();
		}

		private void DeleteCardAlert()
		{
			// Create a new Alert Controller
			UIAlertController actionSheetAlert = UIAlertController.Create(AMStrings.DeleteCardTitle, AMStrings.DeleteCardMessage, UIAlertControllerStyle.ActionSheet);

			// Add Actions
			actionSheetAlert.AddAction(UIAlertAction.Create(AMStrings.Delete, UIAlertActionStyle.Destructive, (action) => DeleteBalance()));

			actionSheetAlert.AddAction(UIAlertAction.Create(AMStrings.Cancel, UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

			// Required for iPad - You must specify a source for the Action Sheet since it is
			// displayed as a popover
			UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
			if (presentationPopover != null)
			{
				presentationPopover.SourceView = this.View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			// Display the alert
			this.PresentViewController(actionSheetAlert, true, null);

		}

		private void DeleteBalance()
		{
			// Google analaytics
            var GAIremoveCardNr = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Kaartnummer verwijderen", "Kaartnummer verwijderen", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAIremoveCardNr);
			Gai.SharedInstance.Dispatch();

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasCard))
			{
				LocalStorage.RemoveCard();
				NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasCard);
			}

			RedirectToRoot(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin));
		}

		private void ShowPopupMEssage(string message)
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpMessageTitle;
			alert.AddButton("Naar gegevens");
			alert.AddButton("Annuleren");
			alert.Message = message;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpClicked;
			alert.Show();
		}

		void ButtonPopUpClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ToProfileViewController);
				GegevensSegue();

			}
			else
			{
				// Cancel button
			}
		}

		void GegevensSegue()
		{
            if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
            {
                ProfileViewController redirect = App.Storyboard.InstantiateViewController("ProfileViewController") as ProfileViewController;
                redirect.Title = "Gegevens";
                UINavigationController NavCtrl = this.TabBarController.ViewControllers[2] as UINavigationController;
                NavCtrl.PushViewController(redirect, true);
            }
            this.TabBarController.SelectedIndex = 2;
		}

		private void ShowPopupRedirect(string message)
		{
			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpRedirectTitle;
			alert.AddButton("Account aanmaken");
			alert.AddButton("Annuleren");
			alert.Message = message;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpRedirectClicked;
			alert.Show();
		}

		void ButtonPopUpRedirectClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				CreateDigitalAccountBirthDateViewController FirstKaartModal = App.Storyboard.InstantiateViewController("CreateDigitalAccountBirthDateViewController") as CreateDigitalAccountBirthDateViewController;
				FirstKaartModal.Routing = "Mijn";
				NavigationController.PushViewController(FirstKaartModal, true);
				
                // Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart", "Kaart ingelogd", "Account aanmaken geaccepteerd", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

			}
			else
			{
				// Cancel button
			}
		}

		void StartAnimateBalance()
		{
			// AnimateNotify ( duration, delay, options, animations, completion )
			UIView.AnimateNotify(0.25, 0, UIViewAnimationOptions.CurveLinear, () =>
		   {
     
			   CGAffineTransform transform = CGAffineTransform.MakeScale(0.75f, 0.75f);
			   BalanceLabel.Transform = transform;
		   },
			(finished) =>
			{

				AnimateBalance();
				StartAnimateScanbutton();
			});
		}

		void AnimateBalance()
		{
			// AnimateNotify ( duration, delay, spring damping, spring velocity, options, animations, completion )
			UIView.AnimateNotify(0.25, 0, 1, 50, 0, () =>
		   {
			   CGAffineTransform transform = CGAffineTransform.MakeScale(1f, 1f);
			   BalanceLabel.Transform = transform;
		   }, null);

			// AddKeyframeWithRelativeStartTime ( start time, duration, animations )
		    // start time and duration 0-1 as percentage of wrapping AnimateKeyframes duration
		    UIView.AnimateKeyframes(2, 0, UIViewKeyframeAnimationOptions.CalculationModeLinear, () =>
		   {
			   UIView.AddKeyframeWithRelativeStartTime(0, 0.125, () =>
			   {
				   Barcode.Alpha = 1;
				   KaartnummerLabel.Alpha = 1;
				   EANLabel.Alpha = 1;

			   });
			},(finished) =>
			{
				
				
			});

		}

		void StartAnimateScanbutton()
		{
			// AnimateNotify ( duration, delay, options, animations, completion 
			UIView.AnimateNotify(0.25, 0, UIViewAnimationOptions.CurveLinear, () =>
		   {

			
		   },
			(finished) =>
			{

				AnimateScanbutton();
			});
		}

		void AnimateScanbutton()
		{
			AMStyle.FlipVerticaly(ButtonQRCodeButton, true);

		}

		void EasterEggMatthijs()
		{
			AMAlerts.DefaultAlert(this, AMStrings.TitleEasterEggMatthijs, AMStrings.TextEasterEggMatthijs);

			int j;
			if (Int32.TryParse(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance), out j))
			{
				int balans = j - 300;
				string balance = balans.ToString();
				BalanceLabel.Text = balance;
				Console.WriteLine(j);
			}
			else
			{
				Console.WriteLine("String could not be parsed.");
			}
		}


		private void ShowPopupWallet()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpWalletTitle;
			alert.AddButton("OK");
			alert.AddButton("Annuleren");
			alert.Message = AMStrings.PopUpWalletMessage;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonWalletClicked;
			alert.Show();

		}

		void ButtonWalletClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
                // OK buttonn
                AddToWallet();
				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Toevoegen aan Wallet", "Accepteren", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else
			{
				// Cancel button
				
				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Kaart ingelogd", "Toevoegen aan Wallet", "Weigeren", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
		}

        async void AddToWallet()
        {
            SetLoader(AMStrings.LoadingWalletCard);
            WalletService walletService = new WalletService();

            string passType = "storeCard";
            string barcode = SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));
            string barcodeType = "EAN13";
            string barcodeEncodingMessage = "iso-8859-1";
            string foregroundColor = "rgb(0, 125, 195)";
            string backgroundColor = "rgb(255, 255, 255)";
            string primaryKey = "voucher";
            string auxiliaryKey = "";
            string dateStyle = "PKDateStyleShort";
            string cardnumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
            var resp = await walletService.addWallet(passType, barcode, barcodeType, barcodeEncodingMessage, "0" , "0", "0", null, "Air Miles", "Klantenkaart", "Air Miles", foregroundColor, backgroundColor, primaryKey, "Air Miles kaartnummer", cardnumber, null, null, null, null, false, dateStyle);
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string localFilename = barcode + ".pkpass";
            string localPath = Path.Combine(documentsPath, localFilename);
            if (resp != null) 
            {
                try
                {
	                File.WriteAllBytes(localPath, resp);
	                Console.WriteLine("pass " + File.ReadAllBytes(localPath).Length);
	                Console.WriteLine("pass filepath "+ localPath);
	                if(LoadingView != null){
	                    LoadingView.RemoveFromSuperview();
	                }
	                AMMain.OpenPassbookViewController(NavigationController, localPath,localFilename);
				}
				catch (Exception ex)
				{
                    Console.WriteLine("exception add to wallet "+ex);

                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Kaart", "Exception: "+ ex}
                    });
                    if (LoadingView != null)
					{
						LoadingView.RemoveFromSuperview();
					}
				}
            }
			if (LoadingView != null)
			{
				LoadingView.RemoveFromSuperview();
			}

		}

		private void ShowWalletAlertMessage()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpWalletTitle;
			alert.AddButton("Naar inloggen");
			alert.AddButton("Annuleren");
			alert.Message = AMStrings.PopUpWalletCardAlertMessage;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpWalletClicked;
			alert.Show();
		}

		void ButtonPopUpWalletClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.FromSaldoViewController);
				GegevensSegue();

			}
			else
			{
				// Cancel button
			}
		}

		void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
  			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

        void SetTextValidity()
        {
            string NumberOfAirmiles = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.ExpirationAmount);
            long ExpirationDate = NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.ExpirationDate);

            Console.WriteLine("numberofairmiles "+NumberOfAirmiles);

            if (!string.IsNullOrEmpty(NumberOfAirmiles))
            {
                ShowValidity = true;
                //parse date
                string dateString = "";

                try
                {
                    long date = Convert.ToInt64(ExpirationDate);
                    var validityDate = SharedDatetime.ConvertFromUnixTimestamp(date);
                    CultureInfo arSA = AMMain.Cultureinfo;
                    dateString = validityDate.ToString("dd\\/MM\\/yyyy", arSA).Replace("/", "-");

                }
                catch (FormatException e)
                {
                    Crashes.TrackError(e);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                            {"Kaart: geldigheid datum", "Exception: "+ e}
                        });
                    Console.WriteLine(e.Message);
                }
                ValidityCardLabel.Text = "Per " + dateString + " vervallen er voor jou " + NumberOfAirmiles + " Air Miles";

                ValidityTableViewCell.Hidden = false;
                TableView.ReloadData();
            } 
            else
            {
                ShowValidity = false;
                ValidityTableViewCell.Hidden = true;
                TableView.ReloadData();
            }


        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Row == 1)
            {
                if (ShowValidity)
                {
                    return 45;
                } else {
                    
                    return 0;
                }
            }
            else
            {
                return GetCell(tableView, indexPath).Frame.Size.Height;
            }
        }
	}
}