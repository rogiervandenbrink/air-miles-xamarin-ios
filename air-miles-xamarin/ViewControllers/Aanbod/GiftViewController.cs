﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using SDWebImage;
using System.Collections.Generic;
using Newtonsoft.Json;
using Air_Miles_Xamarin;
using System.Globalization;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{

	public enum GiftRowType
	{
		GiftIntroCell,
		GiftSmallIntroCell,
		GiftRowCell,
		GiftFooterCell,
		GiftFooterEmptyCell,
		GiftPrintSmallIntroCell
	}

	public enum ButtonSegue
	{
		ActivationSegue,
		ActivationPersonalizedPromotionsSegue,
		EanCompoundSegue,
	}

	public class GiftRow
	{
		public GiftRowType Type { get; set; }
		public string Title { get; set; }
		public string Subtitle { get; set; }
		public string Description { get; set; }
		public ButtonSegue SegueButton { get; set; }
	}

	public partial class GiftViewController : UIViewController
	{
		public Promotie Promotie { get; set; }
		public static Gift Gift;
		public static GiftsOffer Offer;
		public static GiftsOfferGift OfferGift;
		public static PersonalizedPromotion PersonalizedPromotion;

		public bool RemovePriceView { get; set; }
		public bool RemovePropositionView { get; set; }

		public bool InitActivated { get; set; }

		public UIViewController Parent { get; set; }
		private static ButtonSegue SegueForButton { get; set; }

		LoadingOverlay LoadingView;
		UIView GiftActivationView;

		DateTime enDate;
		DateTime registrationDate;

		bool IsActivated;

		double ImageHeight;

		public GiftViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Console.WriteLine("giftviewcontroller");
			Title = AMMain.RemoveHTMLTags(Promotie.Title);

			NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
			UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
			{
				NavigationController.PopViewController(true);
			}), true);

			PriceView.Layer.CornerRadius = PriceView.Bounds.Height / 2;
			PropositionView.Layer.CornerRadius = PropositionView.Bounds.Height / 2;

			// Personolized promotion
			if (Promotie.PromotionType == PromotieType.Gift || Promotie.PromotionType == PromotieType.Offer)
			{
				// Set current gift, offer and calculate the offergift
				Gift = JsonConvert.DeserializeObject<Gift>(Promotie.GiftJson);
				Offer = JsonConvert.DeserializeObject<GiftsOffer>(Promotie.OfferJson);

				foreach (GiftsOfferGift gift in Offer.Gift)
				{
					if (gift.ProductID == Gift.ProductId)
					{
						OfferGift = gift;
					}
				}

				// Set the content
				GetGift(Gift, Offer);
			}

			// Personolized promotion
			if (Promotie.PromotionType == PromotieType.PersonalizedPromotion)
			{
				// Set current personal promotion
				PersonalizedPromotion = JsonConvert.DeserializeObject<PersonalizedPromotion>(Promotie.PersonalizedPromotionJson);

				Console.WriteLine("personalized promotion giftviewcontroller "+JsonConvert.SerializeObject(PersonalizedPromotion));

				if (PersonalizedPromotion.linkCompound != null)
				{
					if (!string.IsNullOrEmpty(PersonalizedPromotion.linkCompound.Link))
					{
						NavigationController.PopViewController(false);
                        //AMMain.OpenURLInApp(this, PersonalizedPromotion.linkCompound.Link);
						UIApplication.SharedApplication.OpenUrl(new NSUrl(PersonalizedPromotion.linkCompound.Link));
				}
				}
				// Set the content
				GetPersonalizedPromotion(PersonalizedPromotion, Promotie);
			}

			ImageHeight = UIScreen.MainScreen.Bounds.Width * 1.0135;

			Initialize();

			Button.TouchUpInside += (sender, e) =>
			{
				if (SegueForButton == ButtonSegue.ActivationSegue)
				{
					if (OfferGift != null)
					{
						if (!string.IsNullOrEmpty(OfferGift.ItemNr))
						{
							ActivateOffer(this);
						}
					}

				}
				if (SegueForButton == ButtonSegue.ActivationPersonalizedPromotionsSegue)
				{
					if (PersonalizedPromotion != null)
					{
						if (!string.IsNullOrEmpty(PersonalizedPromotion.CampaignId))
						{
							ActivatePersonalizedPromotion(this);
							// Google analaytics
							var GAItabkeuze = DictionaryBuilder.CreateEvent("Overig aanbod: details", "Activeer actie "+PersonalizedPromotion.CampaignId, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), null).Build();
							Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
							Gai.SharedInstance.Dispatch();

						}
					}

				}
				else if (SegueForButton == ButtonSegue.EanCompoundSegue)
				{
					PerformSegue(ButtonSegue.EanCompoundSegue.ToString(), this);
				}
			};

			if (!string.IsNullOrEmpty(PersonalizedPromotion.ProductId) && !string.IsNullOrEmpty(PersonalizedPromotion.Title))
			{
				// Google analaytics
				var GAItabkeuzes = DictionaryBuilder.CreateEvent("Persoonlijk aanbod", "Bekijk Overige details", PersonalizedPromotion.ProductId + " " + PersonalizedPromotion.Title, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuzes);
				Gai.SharedInstance.Dispatch();
			}
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Console.WriteLine("Promotie "+JsonConvert.SerializeObject(Promotie));
			Console.WriteLine("promotie registrationdate "+Promotie.RegistrationDate);
			Console.WriteLine("promotie registrationdate convert " + JsonConvert.SerializeObject(Promotie.RegistrationDate));
			// if user is not logged in go back to root
			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				NavigationController.PopToRootViewController(true);
			}

		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
			TableView.SetContentOffset(new CGPoint(0, 0), false);
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{
			if (segueIdentifier== ButtonSegue.EanCompoundSegue.ToString())
			{
				if (PersonalizedPromotion.EanCompound != null)
				{
					return true;
				}
				else {
					return false;
				}
			}

			return true;
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

			if (segue.Identifier == ButtonSegue.EanCompoundSegue.ToString())
			{
				var navctlr = segue.DestinationViewController as CouponViewController;
				if (navctlr != null)
				{
					navctlr.SetPromotion(PersonalizedPromotion);
				}
			}
		}

		public async void ActivateOffer(GiftViewController parent)
		{
			Console.WriteLine("start");
			SetLoader("Aanbod activeren");

			ActivatedGiftRoot ActivatedResponse = await GiftsService.ActivateGift(Offer.OfferNr, OfferGift.ItemNr);

			if (ActivatedResponse != null)
			{
				if (ActivatedResponse.Errors[0].Code == "E00")
				{
					// Update gifts
					await AMMain.GetGiftsForBadge(parent);
					AMUpdateJson.UpdateGiftsFromPartner();

					PromotiesViewController Ctrl = Parent as PromotiesViewController;
					//Ctrl.RedrawGifts = true;

					// Set offer to activated
					Offer.Activated = "true";
					OfferGift.GiftChosen = "true";
					string dateString = "";
					try
					{
						long date = Convert.ToInt64(Promotie.RegistrationDate);
						registrationDate = SharedDatetime.ConvertFromUnixTimestamp(date);
						CultureInfo arSA = AMMain.Cultureinfo;
						dateString = registrationDate.ToString("dd\\/MM\\/yyyy", arSA).Replace("/", "-");

					}
					catch (FormatException e)
					{
                        Crashes.TrackError(e);
                        Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                            {"Aanbod: aanbieding activeren", "Exception: "+ e}
                        });
						Console.WriteLine(e.Message);
					}
					// Set current gift, offer and calculate the offergift
					Gift = JsonConvert.DeserializeObject<Gift>(Promotie.GiftJson);
					Offer = JsonConvert.DeserializeObject<GiftsOffer>(Promotie.OfferJson);

					foreach (GiftsOfferGift gift in Offer.Gift)
					{
						if (gift.ProductID == Gift.ProductId)
						{
							OfferGift = gift;
						}
					}

					if (!string.IsNullOrEmpty(Offer.OfferValidTo))
					{
						DrawGiftActivation(Offer.OfferValidTo, "true");
						Button.Hidden = true;
					}

					// Set the content
					GetGift(Gift, Offer);

					// Update table
					TableView.Source = new GiftTableViewSource(this, GetGiftRows(Gift, Offer, OfferGift));
					TableView.ReloadData();
					TableView.SetContentOffset(new CGPoint(0, -64), false);



					LoadingView.Hide();

					// Google analaytics
					var GAItabkeuze = DictionaryBuilder.CreateEvent("Overig aanbod: details", "Activeer actie ", NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber), null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
					Gai.SharedInstance.Dispatch();

					//GiftActivationView.Hidden = false;
				}
				else {
					LoadingView.Hide();
					AMAlerts.DefaultAlert(parent, "Activeren mislukt", "Helaas! Het activeren van deze aanbieding is mislukt. Probeer het later opnieuw.");
				}
			}
		}

		private void GetGift(Gift gift, GiftsOffer offer)
		{
			GiftsOfferGift offerGift = null;
			if (Promotie.OfferGiftJson != null)
			{
				offerGift = JsonConvert.DeserializeObject<GiftsOfferGift>(Promotie.OfferGiftJson);
			}

			TableView.Source = new GiftTableViewSource(this, GetGiftRows(gift, offer, offerGift));
			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 200f;
			TableView.ReloadData();
		}


		private void GetPersonalizedPromotion(PersonalizedPromotion promotion, Promotie promotie)
		{
			GiftsOfferGift offerGift = null;
			if (Promotie.OfferGiftJson != null)
			{
				offerGift = JsonConvert.DeserializeObject<GiftsOfferGift>(Promotie.OfferGiftJson);
			}

			TableView.Source = new GiftTableViewSource(this, GetPersonalizedPromotionGiftRows(promotion, promotie));
			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 200f;
			TableView.ReloadData();
		}

		public async void ActivatePersonalizedPromotion(GiftViewController parent)
		{
			Console.WriteLine("start");
			SetLoader("Aanbod activeren");

			ActivatedPromotionRoot ActivatedPromotionResponse = await PersonalizedPromotionsService.ActivatePersonalizedPromotion(PersonalizedPromotion.CampaignId);

			if (ActivatedPromotionResponse != null)
			{
				if (ActivatedPromotionResponse.Errors[0].Code == "E00")
				{
					// Update gifts
					await AMMain.GetGiftsForBadge(parent);
					AMUpdateJson.UpdateGiftsFromPartner();

					PromotiesViewController Ctrl = Parent as PromotiesViewController;
					//Ctrl.RedrawGifts = true;

					// Set personalized promotion to activated ??
					PersonalizedPromotion.ActivateCompound.Activated = true;
					Promotie.EnrolledMember = true;

					if (!string.IsNullOrEmpty(PersonalizedPromotion.EndDate))
					{
						DrawGiftActivation(PersonalizedPromotion.RegistrationDateTo, "true");
						Button.Hidden = true;
					}

					// Set the content
					//GetGift(Gift, Offer);

					// Update table
					TableView.Source = new GiftTableViewSource(this, GetPersonalizedPromotionGiftRows(PersonalizedPromotion, Promotie));
					TableView.ReloadData();
					TableView.SetContentOffset(new CGPoint(0, -64), false);

					LoadingView.Hide();

					//GiftActivationView.Hidden = false;
				}
				else {
					LoadingView.Hide();
					AMAlerts.DefaultAlert(parent, "Activeren mislukt", "Helaas! Het activeren van deze aanbieding is mislukt. Probeer het later opnieuw.");
				}
			}
		}

		private GiftRow[] GetGiftRows(Gift gift, GiftsOffer offer, GiftsOfferGift offerGift)
		{
			// Set page title
			Title = gift.ProductTileTitle;
			string dateString = "";
			// Gifst never have a priveview or a propositionview
			RemovePriceView = true;
			RemovePropositionView = true;
			PriceView.Hidden = RemovePriceView;
			PropositionView.Hidden = RemovePropositionView;

			List<GiftRow> Rows = new List<GiftRow>();

			// Set partner logo
			Logo.SetImage(
				url: new NSUrl(gift.PartnerImageUrl),
				placeholder: UIImage.FromBundle("placeholder.png")
			);

			Console.WriteLine("offer convert "+JsonConvert.SerializeObject(offer));
			Console.WriteLine("gift convert " + JsonConvert.SerializeObject(gift));
			Console.WriteLine("offergift convert " + JsonConvert.SerializeObject(offerGift));
			DateTime dateOfferValidTo = DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", null);

			Console.WriteLine("de datum is " + dateOfferValidTo.ToString());

			// Offer is activated
			if (offer.Activated == "true" && offerGift != null)
			{
				Console.WriteLine("Offer is activated");

				Button.Hidden = true;

				// ACTIVATED & GIFT IS CHOSEN
				if (offerGift.GiftChosen == "true" && offer.OfferUsed == "true")
				{
					Console.WriteLine("Gift is chosen");

					Button.Hidden = true;
					DrawGiftUsed();

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftSmallIntroCell, Title = AMMain.RemoveHTMLTags(gift.UsedCouponTitle), Description = AMMain.RemoveHTMLTags(gift.ProductText) });
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Geldigheid", Description = AMMain.RemoveHTMLTags(gift.GiftValidUntil) + " " + dateOfferValidTo.ToString("d").Replace("/", "-") });

					if (!string.IsNullOrEmpty(gift.Conditions)){
						Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Voorwaarden", Description = AMMain.RemoveHTMLTags(gift.Conditions) });
					}
				}
				// ACTIVATED & GIFT IS NOT CHOSEN
				else if (offerGift.GiftChosen == "true" && offer.OfferUsed == "false"){
					Console.WriteLine("Gift is chosen, guft used false");

					Button.Hidden = true;
					DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftSmallIntroCell, Title = AMMain.RemoveHTMLTags(gift.ActivatedMessageTitle), Description = AMMain.RemoveHTMLTags(gift.ProductText) });
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Geldigheid", Description = AMMain.RemoveHTMLTags(gift.GiftValidUntil) + " " + dateOfferValidTo.ToString("d").Replace("/","-") });

					if (!string.IsNullOrEmpty(gift.Conditions))
					{
						Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Voorwaarden", Description = AMMain.RemoveHTMLTags(gift.Conditions) });
					}

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftFooterEmptyCell, SegueButton = ButtonSegue.ActivationSegue, Description = gift.CancelButtonMobile });
				}
				else if (offerGift.GiftChosen == "false" && offer.OfferUsed == "false")
				{
					Console.WriteLine("Gift is not chosen");

					Button.Hidden = false;
					Button.SetTitle(gift.ConfirmButton.ToUpper(), UIControlState.Normal);
					DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftSmallIntroCell, Title = AMMain.RemoveHTMLTags(gift.ActivatedMessageTitle), Description = AMMain.RemoveHTMLTags(gift.ProductText) });
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Geldigheid", Description = AMMain.RemoveHTMLTags(gift.GiftValidUntil) + " " + dateOfferValidTo.ToString("d").Replace("/", "-") });

					if (!string.IsNullOrEmpty(gift.Conditions))
					{
						Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Voorwaarden", Description = AMMain.RemoveHTMLTags(gift.Conditions) });
					}

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftFooterCell, Title = gift.ConfirmButton, Description = gift.CancelButtonMobile });
				}
			}

			// NOT ACTIVATED
			else
			{
				Console.WriteLine("Offer is not activated");

				SegueForButton = ButtonSegue.ActivationSegue;

				Button.Hidden = false;
				Button.SetTitle(gift.ConfirmButton.ToUpper(), UIControlState.Normal);

				Rows.Add(new GiftRow() { Type = GiftRowType.GiftPrintSmallIntroCell, Title = AMMain.RemoveHTMLTags(gift.ProductTileTitle), Description = AMMain.RemoveHTMLTags(gift.GiftValidUntil) + " " + dateOfferValidTo.ToString("d").Replace("/", "-") });
				Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(gift.ProductText) });

				if (!string.IsNullOrEmpty(gift.Conditions))
				{
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Voorwaarden", Description = AMMain.RemoveHTMLTags(gift.Conditions) });
				}

				Rows.Add(new GiftRow() { Type = GiftRowType.GiftFooterCell, Title = gift.ConfirmButton, Description = gift.CancelButtonMobile, SegueButton = SegueForButton });
			}

	        return Rows.ToArray();
		}

		private GiftRow[] GetPersonalizedPromotionGiftRows(PersonalizedPromotion promotion, Promotie promotie)
		{
			// Hide or show price and proposition views
			//PriceView.Hidden = RemovePriceView;
			//PropositionView.Hidden = RemovePropositionView;

			PriceView.Hidden = true;
			PropositionView.Hidden = true;

			string dateString = "";
			string validTo = "";
			string DateDescription = "";

			Console.WriteLine("persoonlijke promotie "+JsonConvert.SerializeObject(promotie));
			Console.WriteLine("personalized promotion " + JsonConvert.SerializeObject(promotion));
            Console.WriteLine("promotion.LogoUrl "+promotion.LogoUrl);

            List<GiftRow> Rows = new List<GiftRow>();

			if (!string.IsNullOrEmpty(promotion.LogoUrl))
			{
				// Set partner logo
				Logo.SetImage(
					url: new NSUrl(promotion.LogoUrl),
					placeholder: UIImage.FromBundle("placeholder.png")
				);
			}

			PriceTitle.Text = AMMain.RemoveHTMLTags(promotion.PropositionValue);
			PriceDetail.Text = AMMain.RemoveHTMLTags(promotion.PropositionLabel);

			if (promotion.MilesOnly)
			{
				PropositionTitle.Text = "MILES";
				PropositionDetail.Text = "ONLY";
				PropositionView.BackgroundColor = UIColor.FromRGB(221, 15, 150);
			}
			else {
				PropositionTitle.Text = promotion.PriceLabel;
				PropositionDetail.Text = promotion.PriceValue;
			}

			try
			{
				long date = Convert.ToInt64(promotie.RegistrationDate);
				registrationDate = SharedDatetime.ConvertFromUnixTimestamp(date);
				CultureInfo arSA = AMMain.Cultureinfo;
				dateString = registrationDate.ToString("dd\\/MM\\/yyyy", arSA).Replace("/", "-");

			}
			catch (FormatException e)
			{
                Crashes.TrackError(e);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Persoonlijk aanbod", "Exception: "+ e}
                });
				Console.WriteLine(e.Message);
			}
			if (!string.IsNullOrEmpty(promotie.PeriodValidUntil))
			{
				validTo = promotie.PeriodValidUntil;
			}else
			{
				validTo = "Geldig tot: ";
			}
		   	Console.WriteLine("promotion enddate "+promotion.EndDate);
			if (!string.IsNullOrEmpty(dateString) && dateString != "01-01-1970")
			{
				DateDescription = validTo + " " + dateString.Substring(0, 10);
			}
			else if (!string.IsNullOrEmpty(promotion.EndDate))
			{
				DateDescription = validTo + " " + promotion.EndDate.Replace("/", "-").Substring(0, 10);
			}
			else if (!string.IsNullOrEmpty(promotie.EndDateCampaign))
			{
				DateDescription = validTo + " " + enDate.ToString("d");
			}
		   // Offer can be activated
			if (!string.IsNullOrEmpty(promotion.ActivateCompound.LinkTitle) && promotion.ActivateCompound != null)
			{
				// Offer IS activated
				//if (!IsActivated)
				//{
				//	promotion.ActivateCompound.Activated = false;
				//	IsActivated = true;
				//	Console.WriteLine("personalpromotion Isactivated = true");
				//}

				if (promotion.ActivateCompound.Activated)
				{
				//if (promotie.EnrolledMember && !string.IsNullOrEmpty(promotie.EndDateCampaign))
				//{
					try
					{
						long date = Convert.ToInt64(promotie.RegistrationDate);
						enDate = SharedDatetime.ConvertFromUnixTimestamp(date);
						DrawGiftActivationDateTime(enDate,promotie.EnrolledMember);
					}
					catch (FormatException e)
					{
						Console.WriteLine(e.Message);
					}
					if (Promotie.EnrolledMember)
					{
						Button.Hidden = true;

						Rows.Add(new GiftRow() { Type = GiftRowType.GiftSmallIntroCell, Title = AMMain.RemoveHTMLTags(promotion.Title), Description = DateDescription });

						Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(promotion.Summary) });
					}else
					{
						Button.Hidden = false;
						Button.SetTitle(promotion.ActivateCompound.LinkTitle, UIControlState.Normal);
						SegueForButton = ButtonSegue.ActivationPersonalizedPromotionsSegue;

						Rows.Add(new GiftRow() { Type = GiftRowType.GiftSmallIntroCell, Title = AMMain.RemoveHTMLTags(promotion.Title), Description = DateDescription });

						Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(promotion.Summary) });

					}
					Console.WriteLine("date description "+DateDescription);


				
				}
				else {

					Button.Hidden = true;
					Button.SetTitle(promotion.ActivateCompound.LinkTitle, UIControlState.Normal);

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftPrintSmallIntroCell, Title = AMMain.RemoveHTMLTags(promotion.Title), Description = DateDescription });
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftFooterCell, Title = promotion.ActivateCompound.LinkTitle, Description = null, SegueButton = SegueForButton });

					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(promotion.Summary) });


					/*if (!string.IsNullOrEmpty(promotion.GeneralConditionsValue))
					{
						Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Voorwaarden", Description = promotion.GeneralConditionsValue });
					}*/
				}	
			}
            Console.WriteLine("promotion.used "+promotie.Used);
            if (!string.IsNullOrEmpty(promotie.Used))
            {
                // Do something with Used (for Essent)
                DrawGiftEssentUsed();

            }

			// Coupon
			if (promotion.EanCompound != null && !string.IsNullOrEmpty(promotion.EanCompound.EanCode))
			{
				SegueForButton = ButtonSegue.EanCompoundSegue;

				Button.Hidden = false;
				string buttonTitle = "Open Coupon";

				if (!string.IsNullOrEmpty(promotion.EanCompound.PrintTitle))
				{
					buttonTitle = promotion.EanCompound.PrintTitle.ToUpper();
				}
				Console.WriteLine("registration date coupon "+dateString);
				Console.WriteLine("Enddate coupon "+ promotion.EndDate);
				Console.WriteLine("EndDate campaign coupon "+ promotie.EndDateCampaign);
				Console.WriteLine("date description " + DateDescription);
				Button.SetTitle(buttonTitle, UIControlState.Normal);

				if (!string.IsNullOrEmpty(promotion.EanCompound.TextBlock))
				{
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftPrintSmallIntroCell, Title = AMMain.RemoveHTMLTags(promotion.EanCompound.Title), Description = DateDescription });
					//Rows.Add(new GiftRow() { Type = GiftRowType.GiftFooterCell, Title = buttonTitle, Description = null, SegueButton = SegueForButton });
				
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(promotion.EanCompound.TextBlock) });

				}
				else
				{
					
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftPrintSmallIntroCell, Title = AMMain.RemoveHTMLTags(promotion.EanCompound.Title), Description = DateDescription });

				}

				/*if (!string.IsNullOrEmpty(promotion.GeneralConditionsValue))
				{
					Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "Voorwaarden", Description = promotion.GeneralConditionsValue });
				}*/
			}
			if(string.IsNullOrEmpty(promotion.ActivateCompound.LinkTitle) && string.IsNullOrEmpty(promotion.EanCompound.EanCode))
				//else
			{
				Button.Hidden = true;

				Rows.Add(new GiftRow() { Type = GiftRowType.GiftSmallIntroCell, Title = AMMain.RemoveHTMLTags(promotion.Title), Description = DateDescription });
                Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(promotion.PromotionBannerCompound.Summary) });
				Rows.Add(new GiftRow() { Type = GiftRowType.GiftRowCell, Title = "", Description = AMMain.RemoveHTMLTags(promotion.PromotionBannerCompound.TextBlock) });
			}



			return Rows.ToArray();
		}

		private void Initialize()
		{
			AMStyle.RoundedButton(Button);

			// Set button frame
			Button.TranslatesAutoresizingMaskIntoConstraints = true;
			Button.Frame = GetButtonFrame(0);

			TableHeaderView.Frame = GetTableHeaderViewFrame();

			Image.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, ImageHeight);
			Image.Layer.MasksToBounds = true;

			Image.SetImage(
				url: new NSUrl(Promotie.Image),
                placeholder: UIImage.FromBundle("placeholder.png")
			);

			if (!Promotie.ShowLogo)
			{
				//Logo.Hidden = true;
			}

			BulletImage.Layer.MasksToBounds = true;
			BulletImage.Hidden = true;
		}

		public void DrawGiftActivation(string date, string activated)
		{
			if (!string.IsNullOrEmpty(date) || !string.IsNullOrWhiteSpace(date))
			{
				DateTime To = DateTime.ParseExact(date.Replace("/", "-"), "dd-MM-yyyy HH:mm:ss", null);
				DateTime Now = DateTime.Now;

				int d;

				if (To < Now)
					d = 0;
				else
					d = (int)Math.Ceiling((To - Now).TotalDays);

				d++;

				string DaysBetween = "Geactiveerd: Nog ";
				if (d == 1)
					DaysBetween += (d + " dag");
				else
					DaysBetween += (d + " dagen");
				DaysBetween += " geldig";

				GiftActivationView = new UIView(new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, 30));
				GiftActivationView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

				if (activated == "true")
				{
					GiftActivationView.Hidden = false;
					Button.Hidden = true;
				}
				else {
					GiftActivationView.Hidden = true;
					Button.Hidden = false;
				}

				UILabel GiftActivationLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
				{
					MinimumFontSize = 12f,
					TextAlignment = UITextAlignment.Center,
					TextColor = UIColor.White,
					Font = UIFont.FromName("Avenir-Black", 14f),
					Lines = 1,
					Text = DaysBetween.ToUpper()
				};

				GiftActivationView.AddSubviews(GiftActivationLabel);
				View.Add(GiftActivationView);
			}
		}

		public void DrawGiftActivationDateTime(DateTime date, bool activated)
		{

			DateTime Now = DateTime.Now;
			Console.WriteLine("now " + Now);
			Console.WriteLine("to - now milliseconds " + (int)Math.Ceiling((date - Now).TotalMinutes));

			string DaysBetween = "";

			int d;
			int h;
			int m;

			if (date < Now)
			{
				d = 0;
			}
			else if ((int)Math.Ceiling((date - Now).TotalMinutes) < 3600 && (int)Math.Ceiling((date - Now).TotalMinutes) > 0)
			{
				if ((int)Math.Ceiling((date - Now).TotalMinutes) < 60)
				{
					m = (int)Math.Ceiling((date - Now).TotalMinutes);
					DaysBetween = "Geactiveerd: Nog ";
					if (m == 1)
						DaysBetween += (m + " minuut");
					else
						DaysBetween += (m + " minuten");
					DaysBetween += " geldig";

				}
				if ((int)Math.Ceiling((date - Now).TotalMinutes) > 60)
				{
					h = (int)Math.Ceiling((date - Now).TotalHours);
					DaysBetween = "Geactiveerd: Nog ";
					if (h == 1)
						DaysBetween += (h + " uur");
					else
						DaysBetween += (h + " uur");
					DaysBetween += " geldig";

				}
			}
			else
			{
				d = (int)Math.Ceiling((date - Now).TotalDays);
				DaysBetween = "Geactiveerd: Nog ";
				if (d == 1)
					DaysBetween += (d + " dag");
				else
					DaysBetween += (d + " dagen");
				DaysBetween += " geldig";

			}
			GiftActivationView = new UIView(new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, 30));
			GiftActivationView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

			if (activated)
			{
				GiftActivationView.Hidden = false;
			}
			else {
				GiftActivationView.Hidden = true;
			}

			UILabel GiftActivationLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
			{
				MinimumFontSize = 12f,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Black", 14f),
				Lines = 1,
				Text = DaysBetween.ToUpper()
			};

			GiftActivationView.AddSubviews(GiftActivationLabel);
			View.Add(GiftActivationView);

		}

		public void DrawGiftUsed()
		{
			GiftActivationView = new UIView(new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, 30));
			GiftActivationView.BackgroundColor = UIColor.FromRGB(0, 0, 0);

			UILabel GiftUsedLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
			{
				MinimumFontSize = 12f,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Black", 14f),
				Lines = 1,
				Text = "ACTIE INGEWISSELD"
			};
			GiftActivationView.Hidden = false;
			GiftActivationView.AddSubviews(GiftUsedLabel);
			View.Add(GiftActivationView);
		}

        public void DrawGiftEssentUsed()
        {
            if (GiftActivationView != null) {
                GiftActivationView.RemoveFromSuperview();
            }
            GiftActivationView = new UIView(new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, 30));
            GiftActivationView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

            UILabel GiftUsedLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
            {
                MinimumFontSize = 12f,
                TextAlignment = UITextAlignment.Center,
                TextColor = UIColor.White,
                Font = UIFont.FromName("Avenir-Black", 14f),
                Lines = 1,
                Text = "ONTVANGEN"
            };
            GiftActivationView.Hidden = false;
            GiftActivationView.AddSubviews(GiftUsedLabel);
            View.Add(GiftActivationView);
        }

		public CGRect GetButtonFrame(nfloat currentOffset)
		{
			if (RemovePriceView == false)
			{
				return Button.Frame = new CGRect(60, ((-currentOffset + ImageHeight) - (Button.Layer.Bounds.Height / 2) + 70), UIScreen.MainScreen.Bounds.Width - 120, Button.Layer.Bounds.Height);
			}
			else {
				return Button.Frame = new CGRect(60, (-currentOffset + ImageHeight) - (Button.Layer.Bounds.Height / 2), UIScreen.MainScreen.Bounds.Width - 120, Button.Layer.Bounds.Height);
			}
		}

		private CGRect GetTableHeaderViewFrame()
		{
			nfloat buttonHeight;
			nfloat PriceBottomHeight = 30;

			if (Button.Hidden == false && RemovePriceView == false)
			{
				buttonHeight = Button.Layer.Bounds.Height + PriceBottomHeight;
			}
			else if (Button.Hidden == false && RemovePriceView == true)
			{
				buttonHeight = PriceBottomHeight;
			}
			else if (Button.Hidden == true && RemovePriceView == false)
			{
				buttonHeight = 0;
			}
			else if (Button.Hidden == true && RemovePriceView == true)
			{
				buttonHeight = 0;
			}
			else {
				buttonHeight = 0;
			}

			return new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, ImageHeight + buttonHeight);

		}

		public void SetScrollOffset(nfloat currentOffset)
		{
			Button.Frame = GetButtonFrame(currentOffset);

			BulletImage.Frame = new CGRect(BulletImage.Frame.X, (-currentOffset + ImageHeight) - (BulletImage.Frame.Height / 2), BulletImage.Frame.Width, BulletImage.Frame.Height);

			PriceView.Frame = new CGRect(PriceView.Frame.X, (-currentOffset + ImageHeight) - ((PriceView.Frame.Height / 2) + 20), PriceView.Frame.Width, PriceView.Frame.Height);
			PropositionView.Frame = new CGRect(PropositionView.Frame.X, (-currentOffset + ImageHeight) - PriceView.Frame.Height, PropositionView.Frame.Width, PropositionView.Frame.Height);

			if (currentOffset < -64)
			{
				Logo.Alpha = (nfloat)((0.001 * ((currentOffset + 64) * 40)) + 1);
				Logo.Frame = new CGRect(UIScreen.MainScreen.Bounds.Width - 75, 84, Logo.Bounds.Width, Logo.Bounds.Height);
				Image.Frame = new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, (ImageHeight + (currentOffset * -1)) - 64);
			}
			else {
				Logo.Alpha = 1f;
				Logo.Frame = new CGRect(UIScreen.MainScreen.Bounds.Width - 75, (currentOffset * -1) + 20, Logo.Bounds.Width, Logo.Bounds.Height);
				Image.Frame = new CGRect(0, currentOffset * -1, UIScreen.MainScreen.Bounds.Width, ImageHeight);
			}
		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		public async void RedirectToGiftDetails(UINavigationController NavCtrl, bool NotActivated)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts);
			NavCtrl.VisibleViewController.Add(LoadingView);

			List<string> ProductIds = new List<string>();

			for (int i = 0; i < Offer.Gift.Length; i++)
			{
				GiftsOfferGift gift = Offer.Gift[i];

				ProductIds.Add(gift.ProductID);
			}

			string JoinedProductIds = string.Join(",", ProductIds);

			GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(JoinedProductIds);
			List<Promotie> Rows = new List<Promotie>();

			if (GiftRoot.Gifts == null)
			{
				AMAlerts.DefaultAlert(NavCtrl.VisibleViewController, AMStrings.ErrorNoGiftsTitle, AMStrings.ErrorNoGiftsMessage);
			}
			else
			{
				// of the offer has more than 1 gifts
				if (GiftRoot.Gifts.Length > 1)
				{
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						GiftsOfferGift[] gifts = Promotie.GiftsOffer.Gift;
						GiftsOfferGift selectedGift = null;
						Gift product = GiftRoot.Gifts[i];

						string title = AMMain.RemoveHTMLTags(product.ProductTileTitle);

						foreach (GiftsOfferGift gift in gifts)
						{
							if (gift.ProductID == GiftRoot.Gifts[i].ProductId && gift.GiftChosen == "true")
							{
								selectedGift = gift;
								title = AMMain.RemoveHTMLTags(product.ActivatedMessageTitle) + " \n " + AMMain.RemoveHTMLTags(product.ActivatedMessage);
							}
						}

						Console.WriteLine("Set offer " + JsonConvert.SerializeObject(Offer));

						// Add gift (the promotion is part of an offer)
						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = title,
							Image = product.Image,
							Logo = product.PartnerImageUrl,
							GiftJson = JsonConvert.SerializeObject(product),
							OfferJson = JsonConvert.SerializeObject(Offer),
	                       	OfferGiftJson = JsonConvert.SerializeObject(selectedGift)
						});
					}

					PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;

					NavCtrl.PopViewController(false);

					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = "Wijzig";
					Redirect.Promoties = Rows.ToArray();

					NavCtrl.PushViewController(Redirect, false);

					//NavCtrl.PopToViewController(Redirect, true);
					//NavCtrl.PushViewController(Redirect, NotActivated);
				}
			}
			LoadingView.Hide();
		}
	}
}