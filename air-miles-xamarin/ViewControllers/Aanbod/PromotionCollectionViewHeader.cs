﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace airmilesxamarin
{
	public partial class PromotionCollectionViewHeader : UICollectionReusableView
	{
		UILabel Title;

		public string Text
		{
			get
			{
				return Title.Text;
			}
			set
			{
				Title.Text = value;
				SetNeedsDisplay();
			}
		}

		public PromotionCollectionViewHeader(IntPtr handle) : base(handle)
		{
			Title = new UILabel()
			{
				Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 60),
				Font = UIFont.FromName("Avenir-Heavy", 17f),
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.FromRGB(143, 196, 219)
			};
			AddSubview(Title);
		}
	}
}

