﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("CouponViewController")]
    partial class CouponViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView BarcodeView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView TitleView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BarcodeView != null) {
                BarcodeView.Dispose ();
                BarcodeView = null;
            }

            if (TitleView != null) {
                TitleView.Dispose ();
                TitleView = null;
            }
        }
    }
}