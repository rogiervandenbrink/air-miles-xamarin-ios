﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("SmilesCollectionViewHeader")]
    partial class SmilesCollectionViewHeader
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel KIESJOUWLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView MilesmeImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Redline { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView RedLine { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel SecondTitleInfoLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView SmilesHeaderView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TANKBIJLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TitleInfoLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (KIESJOUWLabel != null) {
                KIESJOUWLabel.Dispose ();
                KIESJOUWLabel = null;
            }

            if (MilesmeImageView != null) {
                MilesmeImageView.Dispose ();
                MilesmeImageView = null;
            }

            if (Redline != null) {
                Redline.Dispose ();
                Redline = null;
            }

            if (RedLine != null) {
                RedLine.Dispose ();
                RedLine = null;
            }

            if (SecondTitleInfoLabel != null) {
                SecondTitleInfoLabel.Dispose ();
                SecondTitleInfoLabel = null;
            }

            if (SmilesHeaderView != null) {
                SmilesHeaderView.Dispose ();
                SmilesHeaderView = null;
            }

            if (TANKBIJLabel != null) {
                TANKBIJLabel.Dispose ();
                TANKBIJLabel = null;
            }

            if (TitleInfoLabel != null) {
                TitleInfoLabel.Dispose ();
                TitleInfoLabel = null;
            }
        }
    }
}