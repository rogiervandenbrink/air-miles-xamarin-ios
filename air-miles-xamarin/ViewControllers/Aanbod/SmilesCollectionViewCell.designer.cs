﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("SmilesCollectionViewCell")]
    partial class SmilesCollectionViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ChoiceNumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Image { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Logo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PriceDetail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PriceTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PriceView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ProductTextLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PropositionDetail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel PropositionTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView PropositionView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Title { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton VoorwaardenButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Button != null) {
                Button.Dispose ();
                Button = null;
            }

            if (ChoiceNumberLabel != null) {
                ChoiceNumberLabel.Dispose ();
                ChoiceNumberLabel = null;
            }

            if (Image != null) {
                Image.Dispose ();
                Image = null;
            }

            if (Logo != null) {
                Logo.Dispose ();
                Logo = null;
            }

            if (PriceDetail != null) {
                PriceDetail.Dispose ();
                PriceDetail = null;
            }

            if (PriceTitle != null) {
                PriceTitle.Dispose ();
                PriceTitle = null;
            }

            if (PriceView != null) {
                PriceView.Dispose ();
                PriceView = null;
            }

            if (ProductTextLabel != null) {
                ProductTextLabel.Dispose ();
                ProductTextLabel = null;
            }

            if (PropositionDetail != null) {
                PropositionDetail.Dispose ();
                PropositionDetail = null;
            }

            if (PropositionTitle != null) {
                PropositionTitle.Dispose ();
                PropositionTitle = null;
            }

            if (PropositionView != null) {
                PropositionView.Dispose ();
                PropositionView = null;
            }

            if (Title != null) {
                Title.Dispose ();
                Title = null;
            }

            if (VoorwaardenButton != null) {
                VoorwaardenButton.Dispose ();
                VoorwaardenButton = null;
            }
        }
    }
}