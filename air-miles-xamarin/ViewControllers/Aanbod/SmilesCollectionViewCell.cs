﻿using Foundation;
using System;
using UIKit;
using SDWebImage;
using System.Globalization;
using Newtonsoft.Json;
using CoreGraphics;
using System.Drawing;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
	public partial class SmilesCollectionViewCell : UICollectionViewCell
	{
		UIView OfferActivationView, GiftActivationView, GiftUsedView;
		UILabel OfferActivationLabel, GiftActivationLabel, GiftUsedLabel;
		UILabel OfferActivationValue;
		UIViewController Controller;
		Promotie Promotie;
		bool CellIsUpdated;

		// Overlays
		LoadingOverlay LoadingView;

		public bool RemovePriceView, RemovePropositionView;

		public SmilesCollectionViewCell(IntPtr handle) : base(handle)
		{
			DrawGiftActivation();
			DrawOfferActivation();
			//DrawGiftUsed();
		}

		private static IEnumerable<JToken> AllChildren(JToken json)
		{
			foreach (var c in json.Children())
			{
				yield return c;
				foreach (var cc in AllChildren(c))
				{
					yield return cc;
				}
			}
		}

		public void UpdateCell(Promotie item, UICollectionView collectionView, UIViewController parent)
		{
			Controller = parent;
			Promotie = item;

			RemovePriceView = true;
			RemovePropositionView = true;

			if (GiftActivationView != null)
			{
				GiftActivationView.Hidden = true;
			}
			if (GiftUsedView != null)
			{
				GiftUsedView.Hidden = true;
			}

			ProductTextLabel.SizeToFit();

			string ItemNr = "";
			string OfferNr = "";
			string activatedmessage = "";
			string rectangleimage = "";
			string usedoffertext = "";
			string producttext = "";
			string cancelbutton = "";
			string confirmbutton = "";
			string conditionstitle = "";
			string conditionstext = "";
			string conditionslabel= "";
			string usedoffertitle = "";
			string productid = "";

			Console.WriteLine("item shell smiles " + JsonConvert.SerializeObject(Promotie));

			string Item = JsonConvert.SerializeObject(item);
			 
			//Promotie promotieTest = JsonConvert.DeserializeObject<Promotie>(PersonalPromotionJsonEdit);
			string giftJson = JsonConvert.SerializeObject(item.OfferGiftJson);
			Console.WriteLine("giftJson " + giftJson);
			string giftJsonNoBackslash = giftJson.Replace("\\", "");
			string giftJsonNoForwardSlash = giftJsonNoBackslash.Replace("\"{", "{");
			string giftJsonHTML1 = giftJsonNoForwardSlash.Replace("=\"", "=\\\"");
			string giftJsonHTML2 = giftJsonHTML1.Replace("\">", "\\\">");
			string giftJsonEdit = giftJsonHTML2.Replace("}\"", "}");
			Console.WriteLine("offerJson cell " + parent + " json " + item.OfferJson);
			bool giftActivated = false;
			bool offerused = false;
			if (item.OfferJson == "null")
			{
			}else
			{

				offerused = JToken.Parse(Promotie.OfferJson).Value<bool?>("Activated") ?? false;

			}
			Console.WriteLine("offergiftJson cell " + parent + " json " + item.OfferGiftJson);
			if (!string.IsNullOrEmpty(item.GiftActivated))
			{
				if (item.GiftActivated == "true")
				{
					giftActivated = true;
				}
			}
			else if (!string.IsNullOrEmpty(item.OfferGiftJson))
			{

				giftActivated = JToken.Parse(item.OfferGiftJson).Value<bool?>("GiftChosen") ?? false;

			}


			Console.WriteLine("giftJson " + giftJson);
			Console.WriteLine("Item string " + JsonConvert.SerializeObject(item));

			Console.WriteLine("giftJson cell " + parent + " json " + giftJsonEdit);
			Console.WriteLine("giftJson cell " + parent + " json " + item.GiftJson);
			Console.WriteLine("offerJson cell " + parent + " json " + item.OfferJson);
			Console.WriteLine("offergiftJson celll " + parent + " json " + item.OfferGiftJson);
			Console.WriteLine("gift chosen " + " " + giftActivated);

			string productId = item.ProductId;

			Console.WriteLine("Item string " + Item);
			Console.WriteLine("giftJson cell " + parent + " json " + giftJsonEdit);
			Console.WriteLine("giftJson cell " + parent + " json " + item.GiftJson);
			Console.WriteLine("offerJson cell "+ parent + " json " + item.OfferJson);
			Console.WriteLine("offergiftJson cellll " + parent + " json " + item.OfferGiftJson);
			Console.WriteLine("gift activated "+item.GiftActivated);
			if (!string.IsNullOrEmpty(item.OfferJson))
			{
				Console.WriteLine("gift shell used " + JToken.Parse(item.OfferJson).Value<bool>("OfferUsed"));
				//Console.WriteLine("gift shell offerusedtext " + JToken.Parse(item.GiftJson).Value<string>("usedOfferText"));
				Console.WriteLine("gift shell item offerusedtext "+item.UsedOfferText);
			}

			var storeLinkButton = UIButton.FromType(UIButtonType.RoundedRect);
			VoorwaardenButton.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;

			if (Controller.GetType() == typeof(SmilesDetailViewController))
			{
				Console.WriteLine("rectangle image smilesdetailviewc " + item.RectangleImage);
				Console.WriteLine("item smiledetailv " + JsonConvert.SerializeObject(item));
				Console.WriteLine("item smilesdetailvc "+ JsonConvert.SerializeObject(item));
				Console.WriteLine("item previemessageactivated "+item.PreviewMessageActivated);
				Console.WriteLine("item generalconditions " + item.GeneralConditions);
				Console.WriteLine("item generalconditionsvalue " + item.GeneralConditionsValue);

				if (rectangleimage != null)
				{
					Image.SetImage(
						url: new NSUrl(item.RectangleImage)
					);
				}

				if (!item.OfferUsed)
				{
					ChoiceNumberLabel.Text = item.ChoiceCounter;
					ChoiceNumberLabel.TextColor = UIColor.Red;
					ChoiceNumberLabel.Font = UIFont.FromName("Avenir-Heavy", 16f);
					ChoiceNumberLabel.Hidden = false;
				}
				else
				{
					ChoiceNumberLabel.Hidden = true;
				}
				if (item.OfferUsed)
				{
					Title.Hidden = true;
				}
				else if (giftActivated && item.PreviewMessageActivated != null)
				{
					Title.Text = item.PreviewMessageActivated;
					Title.Font = UIFont.FromName("Avenir-Heavy", 48f);
					Title.Hidden = false;
				}
				else if (item.Title != null)
				{
					Title.Text = item.Title;
					Title.Font = UIFont.FromName("Avenir-Heavy", 48f);
					Title.Hidden = false;
				}
				else if (item.Title == null) {
					Title.Text = "";
				}
				if (giftActivated /*&& item.ActivatedMessage != null*/)
				{
                    ProductTextLabel.Hidden = false;
                    ProductTextLabel.Text = AMMain.RemoveHTMLTags(item.Title);
					ProductTextLabel.TextColor = UIColor.FromRGB(87, 87, 87);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.MinimumFontSize = 10f;
					ProductTextLabel.Lines = 6;
					ProductTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				}
				if (Promotie.OfferUsed /*&& !string.IsNullOrEmpty(Promotie.UsedOfferText)*/)
				{
					ProductTextLabel.Hidden = true;
					ProductTextLabel.Text = AMMain.RemoveHTMLTags(Promotie.UsedOfferText);
					ProductTextLabel.TextColor = UIColor.FromRGB(87, 87, 87);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.MinimumFontSize = 10f;
					ProductTextLabel.Lines = 6;
					ProductTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				}
				else if (!giftActivated && item.ProductText != null)
				{
					ProductTextLabel.Hidden = false;
					ProductTextLabel.Text = AMMain.RemoveHTMLTags(item.ProductText);
					ProductTextLabel.TextColor = UIColor.FromRGB(87, 87, 87);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.MinimumFontSize = 10f;
					ProductTextLabel.Lines = 6;
					ProductTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				}

				Console.WriteLine("open details " + item.OpenDetails);
                Console.WriteLine("confirm burron "+item.ConfirmButton);
                string confirmButton = JToken.Parse(item.GiftJson).Value<string>("confirmButton"); 
                Console.WriteLine("confirmmmmmmbuttton "+confirmButton);
                if (item.OfferUsed)
				{
					Button.Hidden = true;
				}
				else if (giftActivated && item.OpenDetails !=null)
				{
					Button.SetTitle(item.OpenDetails, UIControlState.Normal);
					AMStyle.RoundedCornersButton(Button);
					Button.Hidden = false;
				}
				else if (!offerused && !giftActivated && item.ConfirmButton != null)
				{
					Button.SetTitle(item.ConfirmButton, UIControlState.Normal);
					AMStyle.RoundedCornersButton(Button);
					Button.Hidden = false;
				}
				else if (!giftActivated && item.CancelButton != null)
				{
					Button.SetTitle(item.CancelButton, UIControlState.Normal);
					AMStyle.RoundedCornersButton(Button);
					Button.Hidden = false;
				}
				else if (!giftActivated && item.OpenDetails == null && item.ConfirmButton != null)
				{
					Button.SetTitle(item.ConfirmButton, UIControlState.Normal);
					AMStyle.RoundedCornersButton(Button);
					Button.Hidden = false;
				}
                else if (!string.IsNullOrEmpty(confirmButton)){
					Button.SetTitle(confirmButton, UIControlState.Normal);
					AMStyle.RoundedCornersButton(Button);
					Button.Hidden = false;
                }
			}



			if (Controller.GetType() == typeof(SmilesInfoViewController))
			{
				string ProductId = "";
				if (!string.IsNullOrEmpty(item.ProductId))
				{
					ProductId = item.ProductId;
				}
				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Persoonlijk aanbod", "Bekijk Miles&Me details", ProductId + " " + item.Title, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();

				Console.WriteLine("SmilesInfoViewController");
				Console.WriteLine("item.GiftActivated " + item.GiftActivated);
				Console.WriteLine("giiftactivated " + giftActivated);
				Console.WriteLine("Activated Image " + item.RectangleImage);
				Console.WriteLine("Voorwaarden "+item.ConditionsValue);
				Console.WriteLine("item.GeneralConditionslabel " + item.GeneralConditionsValue);
				Console.WriteLine("Promotie.OfferUsed "+Promotie.OfferUsed);
				Console.WriteLine("item Title SmilesInfo Cell "+item.Title);
				Console.WriteLine("Smiles Info Activated Message "+item.ActivatedMessageTitle);

				if (item.RectangleImage != null)
				{
					Image.SetImage(
						url: new NSUrl(item.RectangleImage),
						placeholder: UIImage.FromBundle("Airmiles_Placeholder_Promotion.png")
					);
				}
				if (giftActivated && item.ActivatedMessage != null)
				{
					Console.WriteLine("activated " + giftActivated + " acticatedmessage "+item.ActivatedMessageTitle);
					ProductTextLabel.Text = AMMain.RemoveHTMLTags(item.ActivatedMessage);
					ProductTextLabel.TextColor = UIColor.FromRGB(87, 87, 87);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.MinimumFontSize = 10f;
					ProductTextLabel.Lines = 9;
					ProductTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				}
				if (Promotie.OfferUsed && !string.IsNullOrEmpty(Promotie.UsedOfferText))
				{
					ProductTextLabel.Text = AMMain.RemoveHTMLTags(Promotie.UsedOfferText);
					ProductTextLabel.TextColor = UIColor.FromRGB(87, 87, 87);
					ProductTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.MinimumFontSize = 14f;
					ProductTextLabel.Lines = 9;
					ProductTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
					if (storeLinkButton != null)
					{
						storeLinkButton.RemoveFromSuperview();
					}
					if (Button != null)
					{
						Button.RemoveFromSuperview();
					}

				}
				else if (item.ActivatedMessage != null)
				{
					Console.WriteLine("activated " + giftActivated + " acticatedmessage " + item.ActivatedMessageTitle);
					ProductTextLabel.Text = AMMain.RemoveHTMLTags(item.ActivatedMessage);
					ProductTextLabel.TextColor = UIColor.FromRGB(87, 87, 87);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.Font = UIFont.FromName("Avenir-Book", 14f);
					ProductTextLabel.AdjustsFontSizeToFitWidth = true; // gets smaller if it doesn't fit
					ProductTextLabel.MinimumFontSize = 10f;
					ProductTextLabel.Lines = 9;
					ProductTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				}
				Console.WriteLine("storelocator "+item.StoreLocator);
				if (!string.IsNullOrEmpty(item.StoreLocator) && !Promotie.OfferUsed)
				{
					Match match = Regex.Match(item.StoreLocator, "href='(.*)'");
					if (match.Success)
					{
						string link = match.Groups[1].Value;

						string storeLocator = AMMain.RemoveHTMLTags(item.StoreLocator);

						if (!string.IsNullOrEmpty(link) && !string.IsNullOrEmpty(storeLocator))
						{
							// place storeLocator in textView and link in the onClick method
							
							storeLinkButton.SetTitle(storeLocator, UIControlState.Normal);
							storeLinkButton.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
							storeLinkButton.TitleLabel.Lines = 2;
							storeLinkButton.Font = UIFont.FromName("Avenir-Book", 14f);
							storeLinkButton.Frame = new CGRect(15, this.Frame.Height - 100, this.Frame.Width - 30, 20);
							Add(storeLinkButton);
							storeLinkButton.TouchUpInside += delegate {
								UIApplication.SharedApplication.OpenUrl(new NSUrl(link));
                                //AMMain.OpenURLInApp(Controller, link);
							};

						}
					}
				}

				AMStyle.RoundedCornersButton(Button);

				if (item.OfferUsed)
				{
					Button.Hidden = true;
				}
				else if (item.CancelButton != null)
				{
					Button.SetTitle(item.CancelButton, UIControlState.Normal);
					Console.WriteLine("cancelbutton");
				}

			}

			if (item.Logo != null && item.ShowLogo == true)
			{
				//Console.WriteLine("set logo" + item.Logo);
				//Logo.SetImage(
				//	url: new NSUrl(item.Logo),
				//	placeholder: UIImage.FromBundle("")
				//);
			}

			if (!CellIsUpdated)
			{
				if (item.OfferUsed)
				{
					VoorwaardenButton.Hidden = true;
				}
				else if (item.ConditionsButton != null)
				{
					VoorwaardenButton.Hidden = false;
					VoorwaardenButton.SetTitle(item.ConditionsButton, UIControlState.Normal);
				}

				Console.WriteLine("item conditionsvalue " + item.ConditionsValue);

				VoorwaardenButton.TouchUpInside += delegate
				{
					if (Controller.GetType() == typeof(SmilesInfoViewController))
					{
						string ProductId = "";
						if (!string.IsNullOrEmpty(item.ProductId))
						{
							ProductId = item.ProductId;
						}
						// Google analaytics
						var GAItabkeuze = DictionaryBuilder.CreateEvent("Miles&Me aanbod: details", "Bekijk voorwaarden", ProductId + " " +  item.Title, null).Build();
						Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
						Gai.SharedInstance.Dispatch();

						if (!string.IsNullOrEmpty(item.ConditionsValue))
						{
							UIAlertView alert = new UIAlertView()
							{
								Title = item.ConditionsLabel + " " + item.Title,
								Message = AMMain.RemoveHTMLTags(item.ConditionsValue)
							};
							alert.AddButton("OK");
							alert.Show();
						}
						else
						{
							UIAlertView alert = new UIAlertView()
							{
								Title = item.ConditionsLabel,
								Message = item.GeneralConditionsValue
							};
							alert.AddButton("OK");
							alert.Show();

						}
					}
					if (Controller.GetType() == typeof(SmilesDetailViewController))
					{
						string ProductId = "";
						if (!string.IsNullOrEmpty(item.ProductId))
						{
							ProductId = item.ProductId;
						}
						// Google analaytics
						var GAItabkeuze = DictionaryBuilder.CreateEvent("Miles&Me aanbod: keuzes", "Bekijk voorwaarden", ProductId + " " + item.Title, null).Build();
						Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
						Gai.SharedInstance.Dispatch();


						if (!string.IsNullOrEmpty(item.ConditionsValue))
						{
							UIAlertView alert = new UIAlertView()
							{
								Title = item.ConditionsLabel + " " + item.Title,
								Message = AMMain.RemoveHTMLTags(item.ConditionsValue)
							};
							alert.AddButton("OK");
							alert.Show();
						}
						else
						{
							UIAlertView alert = new UIAlertView()
							{
								Title = item.ConditionsLabel,
								Message = item.GeneralConditionsValue
							};
							alert.AddButton("OK");
							alert.Show();

						}


					}

				};
				
				if (Controller.GetType() == typeof(SmilesDetailViewController))
				{
					

					Button.TouchUpInside += async delegate
					{
						Console.WriteLine("item smilesdetailvc in cell " + JsonConvert.SerializeObject(item));
						Console.WriteLine("item.OfferGitJson smilesdetailvc in cell " + item.OfferGiftJson);


						if (item.OfferGiftJson.Contains("ItemNr"))
						{
							if (!string.IsNullOrEmpty(JToken.Parse(item.OfferGiftJson).Value<string>("ItemNr")))
							{

								ItemNr = JToken.Parse(item.OfferGiftJson).Value<string>("ItemNr");
								Console.WriteLine("itemnr item.OfferGiftJson " + JToken.Parse(item.OfferGiftJson).Value<string>("ItemNr"));
							}
						}
						if (!string.IsNullOrEmpty(item.OfferNr))
						{

							OfferNr = item.OfferNr;
							Console.WriteLine("offernr item.OffernNr "+item.OfferNr);
						}
						else if (!string.IsNullOrEmpty(JToken.Parse(item.OfferJson).Value<string>("OfferNr")))
						{

							OfferNr = JToken.Parse(item.OfferJson).Value<string>("OfferNr");
							Console.WriteLine("offernr Offerjson "+ JToken.Parse(item.OfferJson).Value<string>("OfferNr"));
						}
						if (string.IsNullOrEmpty(ItemNr))
						{
							ItemNr = item.ItemNr;
						}

						Console.WriteLine("itemnr " + ItemNr + " offernr " + OfferNr);


						if (!string.IsNullOrEmpty(OfferNr) && !string.IsNullOrEmpty(ItemNr))
						{
							CGRect bounds = UIScreen.MainScreen.Bounds;

							LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts);
							Controller.NavigationController.VisibleViewController.Add(LoadingView);

							Promotie promotie = JsonConvert.DeserializeObject<Promotie>(Item);
							Console.WriteLine("Item smilesdetail on click "+JsonConvert.SerializeObject(Item));
							try
							{
							ActivatedGiftRoot activatedGift = await GiftsService.ActivateGift(OfferNr, ItemNr);
							if (activatedGift.Errors.Length > 0)
							{
								if (activatedGift.Errors[0].Code == "E00")
								{
										//Get Personal Offers (giftservice.getgiftdata)
									// GET GIFTS
										GiftsService GiftService = new GiftsService();
										GiftsRoot Gifts = await GiftService.GetGiftData();

										if (Gifts.Response != null && Gifts.Errors[0].Code == "E00")
										{
											if (Gifts.Response.Length > 0)
											{
												Console.WriteLine("Gifts response tijdens activatie "+JsonConvert.SerializeObject(Gifts.Response));

												string ProductId = "";
												if (!string.IsNullOrEmpty(item.ProductId))
												{
													ProductId = item.ProductId;
												}
												// Google analaytics
												var GAItabkeuze = DictionaryBuilder.CreateEvent("Miles&Me aanbod: details", "Activeer actie " + ProductId + " " + item.Title, "", null).Build();
												Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
												Gai.SharedInstance.Dispatch();

												Console.WriteLine("gift acivated " + JsonConvert.SerializeObject(activatedGift));
												PromotiesViewController SmilesDetailViewController = Controller as PromotiesViewController;
												SmilesInfoViewController Redirect = App.Storyboard.InstantiateViewController("SmilesInfoViewController") as SmilesInfoViewController;

												Console.WriteLine("item click detailvc "+JsonConvert.SerializeObject(item));
												Redirect.Parent = Controller;
												Redirect.Promotie = item;
												Redirect.PopToPromoties = true;
												Redirect.InitActivated = false;
												Redirect.Title = item.Title;

												if (LoadingView != null)
												{
													LoadingView.Hide();
												}
												Controller.NavigationController.PushViewController(Redirect, false);	
											}
										}



								}
							}
								}
							catch (Exception except)
							{
                                Crashes.TrackError(except);
                                Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                                    {"Shell smiles", "Exception: "+ except}
                                });
								Console.WriteLine("exception activate shell smiles "+except);
								if (LoadingView != null)
								{
									LoadingView.Hide();
								}
								AMAlerts.DefaultAlert(Controller, "Niet geactiveerd", "Helaas! De aanbieding is niet geactiveerd. Probeer het later nogmaals.");
							}
						}
						else
						{
							if (LoadingView != null)
							{
								LoadingView.Hide();
							}
							AMAlerts.DefaultAlert(Controller, "Niet geactiveerd", "Helaas! De aanbieding is niet geactiveerd. Probeer het later nogmaals.");
						}

					};
				}

				if (Controller.GetType() == typeof(SmilesInfoViewController))
				{
					Button.TouchUpInside += delegate
					{
						string ProductId = "";
						if (!string.IsNullOrEmpty(item.ProductId))
						{
							ProductId = item.ProductId;
						}
						// Google analaytics
						var GAItabkeuze = DictionaryBuilder.CreateEvent("Miles&Me aanbod: details", "Verander keuze", ProductId + " "+ item.Title, null).Build();
						Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
						Gai.SharedInstance.Dispatch();

						SmilesInfoViewController ctrl = Controller as SmilesInfoViewController;

						SmilesDetailViewController.RedirectToGiftDetailsString(Controller.NavigationController, Regex.Unescape(item.OfferJson), true);
						Console.WriteLine("click item shell smiles " + JsonConvert.SerializeObject(Promotie));
						Console.WriteLine("button click Promotie " + Regex.Unescape(item.OfferJson));

					};

					}


				CellIsUpdated = true;
			}

			if (item.PriceTitle != null)
			{
				PriceTitle.Text = item.PriceTitle;
				if (item.PriceTitle != "")
				{
					PriceTitle.Text = item.PriceTitle;
					RemovePriceView = false;
				}
			}
			else {
				PriceTitle.Text = "";
			}

			if (item.PriceDetail != null)
			{
				if (item.PriceDetail != "")
				{
					PriceDetail.Text = item.PriceDetail;
					RemovePriceView = false;
				}
			}
			else {
				PriceDetail.Text = "";
			}

			if (item.MilesOnly == true)
			{
				//PropositionTitle.Text = "MILES";
				//PropositionDetail.Text = "ONLY";
				//PropositionView.BackgroundColor = UIColor.FromRGB(221, 15, 150);

				RemovePropositionView = false;
			}
			else {
				if (item.PropositionTitle != null)
				{
					if (item.PropositionTitle != "")
					{
						PropositionTitle.Text = item.PropositionTitle;
						RemovePropositionView = false;
					}
					else {
						PropositionTitle.Text = "";
					}
				}
				else {
					PropositionTitle.Text = "";
				}

				if (item.PropositionDetail != null)
				{
					if (item.PropositionDetail != "")
					{
						PropositionDetail.Text = item.PropositionDetail;
						RemovePropositionView = false;
					}
					else {
						PropositionDetail.Text = "";
					}
				}
				else {
					PropositionDetail.Text = "";
				}
				PropositionView.BackgroundColor = UIColor.FromRGB(0, 163, 255);
			}

			if (RemovePriceView == true)
			{
				PriceView.Hidden = true;
			}
			else {
				PriceView.Hidden = false;
			}

			if (RemovePropositionView == true)
			{
				PropositionView.Hidden = true;
			}
			else {
				PropositionView.Hidden = false;
			}

			if (!string.IsNullOrEmpty(item.RegistrationDate) && item.RegistrationDate != item.EndDate)
			{
				string date = item.EndDate;
				CultureInfo arSA = AMMain.Cultureinfo;
				DateTime oDate = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", arSA);
				string dateString = "Te activeren tot " + oDate.ToString("dd\\/MM\\/yyyy");

				OfferActivationValue.Text = dateString;

				OfferActivationView.Hidden = false;
			}
			else {
				OfferActivationView.Hidden = true;
			}

			//if (item.PromotionType == PromotieType.MarketingPromotion)
			//{
			//	GiftActivationView.Hidden = true;
			//	GiftUsedView.Hidden = true;
			//}
			//else if (item.PromotionType == PromotieType.Offer)
			//{
			//	UpdateOffer(item.GiftsOffer);
			//}
			//else if (item.PromotionType == PromotieType.PersonalizedPromotion)
			//{
			//	PersonalizedPromotion personalizedPromotion = JsonConvert.DeserializeObject<PersonalizedPromotion>(item.PersonalizedPromotionJson);
			//	UpdatePersonalizedPromotion(personalizedPromotion);
			//}
			//else if (item.PromotionType == PromotieType.Gift)
			//{
			//	GiftsOffer offer = JsonConvert.DeserializeObject<GiftsOffer>(item.OfferJson);
			//	GiftsOfferGift gift = JsonConvert.DeserializeObject<GiftsOfferGift>(item.OfferGiftJson);
			//	UpdateGift(offer, gift);
			//}
		}

		static string FixJson(string input)
		{
			var output = input;
			for (var x = 0; x < input.Length; x++)
			{
				if (input[x] != '\"') continue;

				for (var y = x + 1; y < input.Length; y++)
				{
					if (input[y] != '\"') continue;

					var found = false;
					for (var z = y + 1; z < input.Length; z++)
					{
						if (input[z] != '\"') continue;

						var tmp = input.Substring(y + 1, z - y - 1);
						if (tmp.Any(t => t != ' ' && t != ':' && t != ',' && t != '{' && t != '}'))
						{
							output = output.Replace("\"" + tmp + "\"", "\\\"" + tmp + "\\\"");
						}

						x = y;
						found = true;
						break;
					}

					if (found)
						break;
				}
			}

			return output;
		}

		public void StyleCell()
		{
			// Cell styling
			Layer.CornerRadius = 0;
			Layer.MasksToBounds = false;
			Layer.ShadowColor = new CGColor(0, 0, 0);
			Layer.ShadowRadius = 1;
			Layer.ShadowOpacity = 0.1f;
			Layer.ShadowOffset = new CGSize(0, 1);

			// ContentView styling
			ContentView.Layer.CornerRadius = 0;
			ContentView.Layer.MasksToBounds = true;

			// PriceView styling
			PriceView.Layer.CornerRadius = PriceView.Bounds.Height / 2;

			// PropositionView styling
			PropositionView.Layer.CornerRadius = PropositionView.Bounds.Height / 2;
		}

		public void UpdateOffer(GiftsOffer offer)
		{
			if (offer != null)
			{
				if (offer.Activated == "true" && offer.OfferUsed == "false")
				{
					SetActivationDate(DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", null));

					GiftActivationView.Hidden = false;
				}
				else {
					GiftActivationView.Hidden = true;
					GiftUsedView.Hidden = true;
				}

				if (offer.Activated == "true" && offer.OfferUsed == "true")
				{
					ContentView.Alpha = 0.3f;
					GiftUsedView.Hidden = false;
					GiftActivationView.Hidden = true;
				}
				else {
					ContentView.Alpha = 1f;
					GiftUsedView.Hidden = true;
				}
			}
			else {
				ContentView.Alpha = 1f;
				GiftActivationView.Hidden = true;
				GiftUsedView.Hidden = true;
			}
		}

		public void SetActivationDate(DateTime To)
		{
			DateTime Now = DateTime.Now;

			int d;

			if (To < Now)
				d = 0;
			else
				d = (int)Math.Ceiling((To - Now).TotalDays);

			// increase the day with 1 since day is valid t/m
			d++;

			string DaysBetween = "Geactiveerd: Nog ";
			if (d == 1)
				DaysBetween += (d + " dag");
			else
				DaysBetween += (d + " dagen");
			DaysBetween += " geldig";

			GiftActivationLabel.Text = DaysBetween.ToUpper();
		}

		public void UpdatePersonalizedPromotion(PersonalizedPromotion personalizedPromotion)
		{
			if (personalizedPromotion.ActivateCompound != null)
			{
				if (personalizedPromotion.ActivateCompound.Activated && !string.IsNullOrEmpty(personalizedPromotion.RegistrationDateTo))
				{
					SetActivationDate(DateTime.ParseExact(personalizedPromotion.RegistrationDateTo, "dd/MM/yyyy HH:mm:ss", AMMain.Cultureinfo));
					GiftActivationView.Hidden = false;
				}
				else {
					GiftActivationView.Hidden = true;
				}
			}
			else {
				GiftActivationView.Hidden = true;
			}

			ContentView.Alpha = 1f;
			GiftUsedView.Hidden = true;
		}

		public void UpdateGift(GiftsOffer offer, GiftsOfferGift gift)
		{
			// set gift
			if (gift != null)
			{
				if (offer.Activated == "true" && offer.OfferUsed == "false" && gift.GiftChosen == "true")
				{
					DateTime To = DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", AMMain.Cultureinfo);
					DateTime Now = DateTime.Now;

					int d;

					if (To < Now)
						d = 0;
					else
						d = (int)Math.Ceiling((To - Now).TotalDays);

					string DaysBetween = "Geactiveerd: Nog ";
					if (d == 1)
						DaysBetween += (d + " dag");
					else
						DaysBetween += (d + " dagen");
					DaysBetween += " geldig";

					GiftActivationLabel.Text = DaysBetween.ToUpper();
					GiftActivationView.Hidden = false;
					GiftUsedView.Hidden = true;
				}
				else {
					GiftActivationView.Hidden = true;
					GiftUsedView.Hidden = true;
				}
			}
			else {
				GiftActivationView.Hidden = true;
				GiftUsedView.Hidden = true;
			}
		}

		public void DrawOfferActivation()
		{
			OfferActivationView = new UIView(new CGRect(-10, (ContentView.Bounds.Width / 2) - 35, 260, 70));
			OfferActivationView.BackgroundColor = UIColor.FromRGBA(239, 106, 10, 200);
			OfferActivationView.Layer.CornerRadius = 5f;

			OfferActivationLabel = new UILabel(new CGRect(20, 5, UIScreen.MainScreen.Bounds.Width - 60, 35))
			{
				MinimumFontSize = 22f,
				TextAlignment = UITextAlignment.Left,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Heavy", 24f),
				Lines = 1,
				Text = "Let op!"
			};

			OfferActivationValue = new UILabel(new CGRect(20, 34, UIScreen.MainScreen.Bounds.Width - 60, 35))
			{
				MinimumFontSize = 14f,
				TextAlignment = UITextAlignment.Left,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Medium", 16f),
				Lines = 1,
				Text = "Te activeren tot "
			};

			ContentView.AddSubview(OfferActivationView);
			OfferActivationView.AddSubviews(OfferActivationLabel, OfferActivationValue);
		}

		public void DrawGiftActivation()
		{
			GiftActivationView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width - 40, 30));
			GiftActivationView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

			GiftActivationLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
			{
				MinimumFontSize = 12f,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Black", 14f),
				Lines = 1,
				Text = "GEACTIVEERD"
			};

			ContentView.AddSubview(GiftActivationView);
			GiftActivationView.AddSubviews(GiftActivationLabel);
		}

		public void DrawGiftUsed()
		{
			GiftUsedView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width - 40, 30));
			GiftUsedView.BackgroundColor = UIColor.FromRGB(0, 0, 0);

			GiftUsedLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
			{
				MinimumFontSize = 12f,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Black", 14f),
				Lines = 1,
				Text = "ACTIE INGEWISSELD"
			};

			//ContentView.AddSubview(GiftUsedView);
			//GiftUsedView.AddSubviews(GiftUsedLabel);
		}

		public static bool IsNullOrEmpty(JToken token)
		{
			return (token == null) ||
				   (token.Type == JTokenType.Array && !token.HasValues) ||
				   (token.Type == JTokenType.Object && !token.HasValues) ||
				   (token.Type == JTokenType.String && token.ToString() == string.Empty) ||
				   (token.Type == JTokenType.Null);
		}

	}
}

