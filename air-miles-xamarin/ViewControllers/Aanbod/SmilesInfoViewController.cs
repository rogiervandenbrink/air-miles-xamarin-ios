﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using Google.Analytics;
using CoreGraphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class SmilesInfoViewController : UICollectionViewController
    {
        // Network status
		NetworkStatus Internet;

		public Promotie Promotie { get; set; }
		public static Gift Gift;
		public static GiftsOffer Offer;
		public static GiftsOfferGift OfferGift;
		public static PersonalizedPromotion PersonalizedPromotion;

		public bool RemovePriceView { get; set; }
		public bool RemovePropositionView { get; set; }

		public bool PopToPromoties { get; set; }

		public bool InitActivated { get; set; }

		public UIViewController Parent { get; set; }
		private static ButtonSegue SegueForButton { get; set; }

		UIView GiftActivationView;

		bool IsActivated;
		public bool FromPromoties { get; set; }

		double ImageHeight;

		public static TransactiesOverlay TransactiesOverlay;

		// Overlays
		LoadingOverlay LoadingView;
		NetworkOverlay NetworkView;
		MaintainanceOverlay MaintainanceView;

		public Promotie[] Promoties;

		public List<GiftsOfferPartner> giftsOfferPartner;
		public List<Promotie> GiftsPromoties;
		static SmilesInfoViewController instance;

		List<string> giftPartners;
		List<string> giftProducts;

		public bool PersonalGifts { get; set; }
		public bool PersonalGiftsDetails { get; set; }

		public static bool RedrawGifts;
		public static bool Activated;
		public bool DoRefreshPromoties { get; set;}

		//refreshlayout
		UIRefreshControl RefreshControl;

		public SmilesInfoViewController(IntPtr handle) : base (handle)
        {
		}

		public SmilesInfoViewController()
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if (!FromPromoties)
			{
				if (NavigationController != null)
				{
                    HideBackButton();
					NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
					UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
					{
					PoppingViewControllers();

					}), true);
				}
			}
			else
			{
				NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
					UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
					{
						NavigationController.PopViewController(true);
						

					}), true);
			}

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				CollectionView.AddSubview(RefreshControl);

			}

			// Personolized promotion
			if (Promotie.PromotionType == PromotieType.Gift || Promotie.PromotionType == PromotieType.Offer)
			{
				// Set current gift, offer and calculate the offergift
				Gift = JsonConvert.DeserializeObject<Gift>(Promotie.GiftJson);
				Offer = JsonConvert.DeserializeObject<GiftsOffer>(Promotie.OfferJson);

				Console.WriteLine("Promotie Gift is "+JsonConvert.SerializeObject(Gift));
				Console.WriteLine("Promotie shell info "+JsonConvert.SerializeObject(Promotie));

				try
				{


				foreach (GiftsOfferGift gift in Offer.Gift)
					{
						Console.WriteLine("ProductID "+gift.ProductID);

						if (Gift.ProductId!=null)
						{
							if (gift.ProductID == Gift.ProductId)
							{
								OfferGift = gift;
							}
						}

					}

					}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Shell smiles", "Exception: "+ ex}
                    });
					Console.WriteLine("exception promotype gift || Offer " + ex);

				}
				if (!InitActivated)
				{
					// Set the content
					GetGift(Gift, Offer);
				}
			}

			// Personolized promotion
			if (Promotie.PromotionType == PromotieType.PersonalizedPromotion)
			{
				// Set current personal promotion
				PersonalizedPromotion = JsonConvert.DeserializeObject<PersonalizedPromotion>(Promotie.PersonalizedPromotionJson);
			}
			
		}

		public void PoppingViewControllers()
		{
			foreach (UIViewController view in NavigationController.ViewControllers)
			{
				if (view.GetType() == typeof(PromotiesViewController))
				{
					NavigationController.PopToViewController(view, false);
				}
			}

		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			FromPromoties = false;
			DoRefreshPromoties = true;
			if (InitActivated)
			{
				giftPartners = new List<string>();
				giftProducts = new List<string>();
				giftProducts.Clear();
				giftPartners.Clear();
				GetPersonalGiftsFromPartner();
				InitActivated = false;
			}

			SetAnalytics();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			Console.WriteLine("poptopromoties is "+PopToPromoties);
			if (PopToPromoties && NavigationController!=null)
			{
				//NavigationController.PopViewController(false);
			}
		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Miles&Me aanbod: details");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void GetGift(Gift gift, GiftsOffer offer)
		{
			GiftsOfferGift offerGift = null;
			if (Promotie.OfferGiftJson != null)
			{
				offerGift = JsonConvert.DeserializeObject<GiftsOfferGift>(Promotie.OfferGiftJson);
			}
			Console.WriteLine("deserialize promotie smilesinfo "+ JsonConvert.SerializeObject(Promotie.OfferGiftJson));
            Console.WriteLine("get gift "+ JsonConvert.SerializeObject(gift)+" offer "+JsonConvert.SerializeObject(offer)+" offergift "+JsonConvert.SerializeObject(offerGift));
            CollectionView.DataSource = new SmilesCollectionViewSource(this, GetGiftRows(gift, offer, offerGift));
			CollectionView.Delegate = new SmilesFlowDelegate(this, GetGiftRows(gift, offer, offerGift), true, false);
			CollectionView.ReloadData();
		}

        private void HideBackButton()
        {
            NavigationItem.SetHidesBackButton(true, false);
        }

		private Promotie[] GetGiftRows(Gift gift, GiftsOffer offer, GiftsOfferGift offerGift)
		{
            // Set page title

            if (gift != null) 
            { 
                Title = gift.ProductTileTitle;

                List<Promotie> Rows = new List<Promotie>();


				DateTime dateOfferValidTo = DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", null);

				Console.WriteLine("de datum is " + dateOfferValidTo.ToString());

				//Console.WriteLine("offernr in smilesinfo " + offer.OfferNr);
				//Console.WriteLine("offer " + JsonConvert.SerializeObject(offer));
				//Console.WriteLine("gift smilesinfo " + JsonConvert.SerializeObject(gift));
				//Console.WriteLine("offerGift " + JsonConvert.SerializeObject(offerGift));
				//Console.WriteLine("smilesinfo gift opendetails " + JsonConvert.SerializeObject(gift.OpenDetails));
				Console.WriteLine("smilesinfo Promotie " + JsonConvert.SerializeObject(Promotie));
				//Console.WriteLine("smilesinfo Promotie.GeneralConditionsValue " + JsonConvert.SerializeObject(Promotie.GeneralConditionsValue));
				// Offer is activated
				if (offer.Activated == "true" && offerGift != null)
				{
					Console.WriteLine("Offer is activated");

					//Button.Hidden = true;

					// ACTIVATED & GIFT IS CHOSEN
					//offer.OfferUsed == "true"
					if (offerGift.GiftChosen == "true" && offer.OfferUsed == "true")
					{
						Console.WriteLine("Gift is chosen & used");

						//Button.Hidden = true;
						//DrawGiftUsed();

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = offer.OfferNr,
							ItemNr = offerGift.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(offerGift),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = gift.UsedCouponTitle,
							PrintButtonTitle = gift.PrintButtonTitle,
							GiftValidUntil = gift.GiftValidUntil,
							CancelButtonMobile = gift.CancelButtonMobile,
							ProductTileTitle = gift.ProductTileTitle,
							PreviewMessageActivated = gift.PreviewMessageActivated,
							ChangeGift = gift.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = true,
							MilesOnly = gift.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = gift.OptimizedLogoUrl,
							PeriodValidUntil = gift.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = gift.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = gift.Id,
							Type = gift.Type,
							Image = gift.Image
						});

						Console.WriteLine("shell gift 0");
						//return Rows.ToArray();
					}
					// ACTIVATED & GIFT IS NOT CHOSEN
					//offer.OfferUsed == "false"
					else if (offerGift.GiftChosen == "true" && offer.OfferUsed == "false")
					{
						Console.WriteLine("Gift is chosen");

						//Button.Hidden = true;
						//DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = offer.OfferNr,
							ItemNr = offerGift.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(offerGift),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = gift.UsedCouponTitle,
							PrintButtonTitle = gift.PrintButtonTitle,
							GiftValidUntil = gift.GiftValidUntil,
							CancelButtonMobile = gift.CancelButtonMobile,
							ProductTileTitle = gift.ProductTileTitle,
							PreviewMessageActivated = gift.PreviewMessageActivated,
							ChangeGift = gift.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = false,
							MilesOnly = gift.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = gift.OptimizedLogoUrl,
							PeriodValidUntil = gift.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = gift.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = gift.Id,
							Type = gift.Type,
							Image = gift.Image
						});
						Console.WriteLine("shell gift 1");

						//return Rows.ToArray();

					}
					// NOT ACTIVATED & GIFT IS CHOSEN
					else if (offerGift.GiftChosen == "false" && offer.OfferUsed == "true")
					{
						Console.WriteLine("Gift is not chosen");

						//Button.Hidden = true;
						//DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = offer.OfferNr,
							ItemNr = offerGift.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(offerGift),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = gift.UsedCouponTitle,
							PrintButtonTitle = gift.PrintButtonTitle,
							GiftValidUntil = gift.GiftValidUntil,
							CancelButtonMobile = gift.CancelButtonMobile,
							ProductTileTitle = gift.ProductTileTitle,
							PreviewMessageActivated = gift.PreviewMessageActivated,
							ChangeGift = gift.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = true,
							MilesOnly = gift.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = gift.OptimizedLogoUrl,
							PeriodValidUntil = gift.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = gift.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = gift.Id,
							Type = gift.Type,
							Image = gift.Image
						});

						Console.WriteLine("shell gift 1");

						//return Rows.ToArray();

					}

					else if (offerGift.GiftChosen == "false" && offer.OfferUsed == "false")
					{
						Console.WriteLine("Gift is not chosen");
						//offer.OfferUsed == "false"

						//Button.Hidden = false;
						//Button.SetTitle(gift.ConfirmButton.ToUpper(), UIControlState.Normal);
						//DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = offer.OfferNr,
							ItemNr = offerGift.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(offerGift),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = gift.UsedCouponTitle,
							PrintButtonTitle = gift.PrintButtonTitle,
							GiftValidUntil = gift.GiftValidUntil,
							CancelButtonMobile = gift.CancelButtonMobile,
							ProductTileTitle = gift.ProductTileTitle,
							PreviewMessageActivated = gift.PreviewMessageActivated,
							ChangeGift = gift.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = false,
							MilesOnly = gift.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = gift.OptimizedLogoUrl,
							PeriodValidUntil = gift.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = gift.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = gift.Id,
							Type = gift.Type,
							Image = gift.Image
						});

						Console.WriteLine("shell gift 2");
					}

					Console.WriteLine("shellinfo rows to array " + JsonConvert.SerializeObject(Rows.ToArray()));

					return Rows.ToArray();
				}

				// NOT ACTIVATED
				else
				{
					Console.WriteLine("Offer is not activated");
                    Console.WriteLine("rowszz "+Rows.Count);
                    SegueForButton = ButtonSegue.ActivationSegue;

					//Button.Hidden = false;
					Rows.Add(new Promotie()
					{
						PromotionType = PromotieType.Gift,
						Title = Promotie.Title,
						RectangleImage = Promotie.RectangleImage,
						ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
						ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
						CancelButton = Promotie.CancelButton,
						ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
						GiftJson = JsonConvert.SerializeObject(gift),
						OfferJson = Promotie.OfferJson,
						OfferNr = offer.OfferNr,
						ItemNr = offerGift.ItemNr,
						OfferGiftJson = JsonConvert.SerializeObject(offerGift),
						ConditionsLabel = Promotie.ConditionsLabel,
						CheckConditionsLabel = Promotie.CheckConditionsLabel,
						PaymentTitle = Promotie.PaymentTitle,
						//Keywords = gift.Keywords,
						PaymentImage = Promotie.PaymentImage,
						PaymentMethod = Promotie.PaymentMethod,
						ProductType = Promotie.ProductType,
						EndDate = Promotie.EndDate,
						ProductId = Promotie.ProductId,
						LinkPartner = Promotie.LinkPartner,
						Conditions = Promotie.Conditions,
						ButtonTitle = Promotie.ButtonTitle,
						DeliverTime = Promotie.DeliverTime,
						StartDate = Promotie.StartDate,
						Logo = Promotie.PartnerImageUrl,
						PaymentColor = Promotie.PaymentColor,
						StoreLocator = Promotie.StoreLocator,
						OpenCoupon = Promotie.OpenCoupon,
						UsedCouponTitle = gift.UsedCouponTitle,
						PrintButtonTitle = gift.PrintButtonTitle,
						GiftValidUntil = gift.GiftValidUntil,
						CancelButtonMobile = gift.CancelButtonMobile,
						ProductTileTitle = gift.ProductTileTitle,
						PreviewMessageActivated = gift.PreviewMessageActivated,
						ChangeGift = gift.ChangeGift,
						UsedOfferText = Promotie.UsedOfferText,
						UsedOfferTitle = Promotie.UsedOfferTitle,
						OpenDetails = Promotie.OpenDetails,
						ConfirmButton = Promotie.ConfirmButton,
						OfferUsed = Promotie.OfferUsed,
						MilesOnly = gift.MilesOnly,
						PropositionValue = Promotie.PropositionValue,
						PropositionLabel = Promotie.PropositionLabel,
						PriceValue = Promotie.PriceValue,
						PriceLabel = Promotie.PriceLabel,
						LogoUrl = Promotie.LogoUrl,
						OptimizedLogoUrl = gift.OptimizedLogoUrl,
						PeriodValidUntil = gift.PeriodValidUntil,
						TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
						GeneralConditionsValue = Promotie.GeneralConditionsValue,
						TicketImageUrl = Promotie.TicketImageUrl,
						productSubType = Promotie.productSubType,
						ConditionsValue = Promotie.ConditionsValue,
						VoucherType = Promotie.VoucherType,
						ShowLogo = Promotie.ShowLogo,
						PartnerName = Promotie.PartnerName,
						PartnerImageUrl = Promotie.PartnerImageUrl,
						TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
						OrderedByText = Promotie.OrderedByText,
						PricePerTicketText = Promotie.PricePerTicketText,
						AddressText = Promotie.AddressText,
						ValidityText = Promotie.ValidityText,
						ConditionsText = Promotie.ConditionsText,
						ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
						LogoImageUrl = Promotie.LogoImageUrl,
						ShortDescription = gift.ShortDescription,
						Summary = Promotie.Summary,
						Category = Promotie.Category,
						Validity = Promotie.Validity,
						Address = Promotie.Address,
						Id = gift.Id,
						Type = gift.Type,
						Image = gift.Image
					});

					Console.WriteLine("shell gift 3");
					Console.WriteLine("shellinfo rows to array " + JsonConvert.SerializeObject(Rows.ToArray()));
					return Rows.ToArray();
				}
				Console.WriteLine("shellinfo rows to array "+JsonConvert.SerializeObject(Rows.ToArray()));
				return Rows.ToArray();

			}
            if (offer != null)
            {
                List<Promotie> Rows = new List<Promotie>();


                DateTime dateOfferValidTo = DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", null);

                Console.WriteLine("de datum is " + dateOfferValidTo.ToString());

                //Console.WriteLine("offernr in smilesinfo " + offer.OfferNr);
                //Console.WriteLine("offer " + JsonConvert.SerializeObject(offer));
                //Console.WriteLine("gift smilesinfo " + JsonConvert.SerializeObject(gift));
                //Console.WriteLine("offerGift " + JsonConvert.SerializeObject(offerGift));
                //Console.WriteLine("smilesinfo gift opendetails " + JsonConvert.SerializeObject(gift.OpenDetails));
                Console.WriteLine("smilesinfo Promotie " + JsonConvert.SerializeObject(Promotie));
                //Console.WriteLine("smilesinfo Promotie.GeneralConditionsValue " + JsonConvert.SerializeObject(Promotie.GeneralConditionsValue));
                // Offer is activated
                if (offer.Activated == "true" && offerGift != null)
                {
                    Console.WriteLine("Offer is activated");

                    //Button.Hidden = true;

                    // ACTIVATED & GIFT IS CHOSEN
                    //offer.OfferUsed == "true"
                    if (offerGift.GiftChosen == "true" && offer.OfferUsed == "true")
                    {
                        Console.WriteLine("Gift is chosen & used");

						//Button.Hidden = true;
						//DrawGiftUsed();

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = Promotie.OfferNr,
							ItemNr = Promotie.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(Promotie),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							//Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = Promotie.UsedCouponTitle,
							PrintButtonTitle = Promotie.PrintButtonTitle,
							GiftValidUntil = Promotie.GiftValidUntil,
							CancelButtonMobile = Promotie.CancelButtonMobile,
							ProductTileTitle = Promotie.ProductTileTitle,
							PreviewMessageActivated = Promotie.PreviewMessageActivated,
							ChangeGift = Promotie.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = Promotie.OfferUsed,
							MilesOnly = Promotie.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = Promotie.OptimizedLogoUrl,
							PeriodValidUntil = Promotie.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = Promotie.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = Promotie.Id,
							Type = Promotie.Type,
							Image = Promotie.Image
						});

                        Console.WriteLine("shell gift 0");
                        //return Rows.ToArray();
                    }
                    // ACTIVATED & GIFT IS NOT CHOSEN
                    //offer.OfferUsed == "false"
                    else if (offerGift.GiftChosen == "true" && offer.OfferUsed == "false")
                    {
                        Console.WriteLine("Gift is chosen");

						//Button.Hidden = true;
						//DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = Promotie.OfferNr,
							ItemNr = Promotie.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(Promotie),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							//Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = Promotie.UsedCouponTitle,
							PrintButtonTitle = Promotie.PrintButtonTitle,
							GiftValidUntil = Promotie.GiftValidUntil,
							CancelButtonMobile = Promotie.CancelButtonMobile,
							ProductTileTitle = Promotie.ProductTileTitle,
							PreviewMessageActivated = Promotie.PreviewMessageActivated,
							ChangeGift = Promotie.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = Promotie.OfferUsed,
							MilesOnly = Promotie.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = Promotie.OptimizedLogoUrl,
							PeriodValidUntil = Promotie.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = Promotie.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = Promotie.Id,
							Type = Promotie.Type,
							Image = Promotie.Image
						});
                        Console.WriteLine("shell gift 1");

                        //return Rows.ToArray();

                    }
                    // NOT ACTIVATED & GIFT IS CHOSEN
                    else if (offerGift.GiftChosen == "false" && offer.OfferUsed == "true")
                    {
                        Console.WriteLine("Gift is not chosen");

						//Button.Hidden = true;
						//DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = Promotie.OfferNr,
							ItemNr = Promotie.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(Promotie),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							//Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = Promotie.UsedCouponTitle,
							PrintButtonTitle = Promotie.PrintButtonTitle,
							GiftValidUntil = Promotie.GiftValidUntil,
							CancelButtonMobile = Promotie.CancelButtonMobile,
							ProductTileTitle = Promotie.ProductTileTitle,
							PreviewMessageActivated = Promotie.PreviewMessageActivated,
							ChangeGift = Promotie.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = Promotie.OfferUsed,
							MilesOnly = Promotie.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = Promotie.OptimizedLogoUrl,
							PeriodValidUntil = Promotie.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = Promotie.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = Promotie.Id,
							Type = Promotie.Type,
							Image = Promotie.Image
						});

                        Console.WriteLine("shell gift 1");

                        //return Rows.ToArray();

                    }

                    else if (offerGift.GiftChosen == "false" && offer.OfferUsed == "false")
                    {
                        Console.WriteLine("Gift is not chosen");
						//offer.OfferUsed == "false"

						//Button.Hidden = false;
						//Button.SetTitle(gift.ConfirmButton.ToUpper(), UIControlState.Normal);
						//DrawGiftActivation(Offer.OfferValidTo, Offer.Activated);

						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = Promotie.Title,
							RectangleImage = Promotie.RectangleImage,
							ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
							ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
							CancelButton = Promotie.CancelButton,
							ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
							GiftJson = JsonConvert.SerializeObject(gift),
							OfferJson = Promotie.OfferJson,
							OfferNr = Promotie.OfferNr,
							ItemNr = Promotie.ItemNr,
							OfferGiftJson = JsonConvert.SerializeObject(Promotie),
							ConditionsLabel = Promotie.ConditionsLabel,
							CheckConditionsLabel = Promotie.CheckConditionsLabel,
							PaymentTitle = Promotie.PaymentTitle,
							//Keywords = gift.Keywords,
							PaymentImage = Promotie.PaymentImage,
							PaymentMethod = Promotie.PaymentMethod,
							ProductType = Promotie.ProductType,
							EndDate = Promotie.EndDate,
							ProductId = Promotie.ProductId,
							LinkPartner = Promotie.LinkPartner,
							Conditions = Promotie.Conditions,
							ButtonTitle = Promotie.ButtonTitle,
							DeliverTime = Promotie.DeliverTime,
							StartDate = Promotie.StartDate,
							Logo = Promotie.PartnerImageUrl,
							PaymentColor = Promotie.PaymentColor,
							StoreLocator = Promotie.StoreLocator,
							OpenCoupon = Promotie.OpenCoupon,
							UsedCouponTitle = Promotie.UsedCouponTitle,
							PrintButtonTitle = Promotie.PrintButtonTitle,
							GiftValidUntil = Promotie.GiftValidUntil,
							CancelButtonMobile = Promotie.CancelButtonMobile,
							ProductTileTitle = Promotie.ProductTileTitle,
							PreviewMessageActivated = Promotie.PreviewMessageActivated,
							ChangeGift = Promotie.ChangeGift,
							UsedOfferText = Promotie.UsedOfferText,
							UsedOfferTitle = Promotie.UsedOfferTitle,
							OpenDetails = Promotie.OpenDetails,
							ConfirmButton = Promotie.ConfirmButton,
							OfferUsed = Promotie.OfferUsed,
							MilesOnly = Promotie.MilesOnly,
							PropositionValue = Promotie.PropositionValue,
							PropositionLabel = Promotie.PropositionLabel,
							PriceValue = Promotie.PriceValue,
							PriceLabel = Promotie.PriceLabel,
							LogoUrl = Promotie.LogoUrl,
							OptimizedLogoUrl = Promotie.OptimizedLogoUrl,
							PeriodValidUntil = Promotie.PeriodValidUntil,
							TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
							GeneralConditionsValue = Promotie.GeneralConditionsValue,
							TicketImageUrl = Promotie.TicketImageUrl,
							productSubType = Promotie.productSubType,
							ConditionsValue = Promotie.ConditionsValue,
							VoucherType = Promotie.VoucherType,
							ShowLogo = Promotie.ShowLogo,
							PartnerName = Promotie.PartnerName,
							PartnerImageUrl = Promotie.PartnerImageUrl,
							TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
							OrderedByText = Promotie.OrderedByText,
							PricePerTicketText = Promotie.PricePerTicketText,
							AddressText = Promotie.AddressText,
							ValidityText = Promotie.ValidityText,
							ConditionsText = Promotie.ConditionsText,
							ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
							LogoImageUrl = Promotie.LogoImageUrl,
							ShortDescription = Promotie.ShortDescription,
							Summary = Promotie.Summary,
							Category = Promotie.Category,
							Validity = Promotie.Validity,
							Address = Promotie.Address,
							Id = Promotie.Id,
							Type = Promotie.Type,
							Image = Promotie.Image
						});

                        Console.WriteLine("shell gift 2");
                    }

                    Console.WriteLine("shellinfo rows to array " + JsonConvert.SerializeObject(Rows.ToArray()));

                    return Rows.ToArray();
                }

                // NOT ACTIVATED
                else
                {
                    Console.WriteLine("Offer is not activated");
                    Console.WriteLine("rowszz " + Rows.Count);
                    SegueForButton = ButtonSegue.ActivationSegue;

                    //Button.Hidden = false;
                    Rows.Add(new Promotie()
                    {
                        PromotionType = PromotieType.Gift,
                        Title = Promotie.Title,
                        RectangleImage = Promotie.RectangleImage,
                        ProductText = AMMain.RemoveHTMLTags(Promotie.ProductText),
                        ActivatedMessage = AMMain.RemoveHTMLTags(Promotie.ActivatedMessage),
                        CancelButton = Promotie.CancelButton,
                        ActivatedMessageTitle = Promotie.ActivatedMessageTitle,
                        GiftJson = JsonConvert.SerializeObject(gift),
                        OfferJson = Promotie.OfferJson,
                        OfferNr = Promotie.OfferNr,
                        ItemNr = Promotie.ItemNr,
                        OfferGiftJson = JsonConvert.SerializeObject(Promotie),
                        ConditionsLabel = Promotie.ConditionsLabel,
                        CheckConditionsLabel = Promotie.CheckConditionsLabel,
                        PaymentTitle = Promotie.PaymentTitle,
                        //Keywords = gift.Keywords,
                        PaymentImage = Promotie.PaymentImage,
                        PaymentMethod = Promotie.PaymentMethod,
                        ProductType = Promotie.ProductType,
                        EndDate = Promotie.EndDate,
                        ProductId = Promotie.ProductId,
                        LinkPartner = Promotie.LinkPartner,
                        Conditions = Promotie.Conditions,
                        ButtonTitle = Promotie.ButtonTitle,
                        DeliverTime = Promotie.DeliverTime,
                        StartDate = Promotie.StartDate,
                        Logo = Promotie.PartnerImageUrl,
                        PaymentColor = Promotie.PaymentColor,
                        StoreLocator = Promotie.StoreLocator,
                        OpenCoupon = Promotie.OpenCoupon,
                        UsedCouponTitle = Promotie.UsedCouponTitle,
                        PrintButtonTitle = Promotie.PrintButtonTitle,
                        GiftValidUntil = Promotie.GiftValidUntil,
                        CancelButtonMobile = Promotie.CancelButtonMobile,
                        ProductTileTitle = Promotie.ProductTileTitle,
                        PreviewMessageActivated = Promotie.PreviewMessageActivated,
                        ChangeGift = Promotie.ChangeGift,
                        UsedOfferText = Promotie.UsedOfferText,
                        UsedOfferTitle = Promotie.UsedOfferTitle,
                        OpenDetails = Promotie.OpenDetails,
                        ConfirmButton = Promotie.ConfirmButton,
                        OfferUsed = Promotie.OfferUsed,
                        MilesOnly = Promotie.MilesOnly,
                        PropositionValue = Promotie.PropositionValue,
                        PropositionLabel = Promotie.PropositionLabel,
                        PriceValue = Promotie.PriceValue,
                        PriceLabel = Promotie.PriceLabel,
                        LogoUrl = Promotie.LogoUrl,
                        OptimizedLogoUrl = Promotie.OptimizedLogoUrl,
                        PeriodValidUntil = Promotie.PeriodValidUntil,
                        TicketSecondaryImageUrl = Promotie.TicketSecondaryImageUrl,
                        GeneralConditionsValue = Promotie.GeneralConditionsValue,
                        TicketImageUrl = Promotie.TicketImageUrl,
                        productSubType = Promotie.productSubType,
                        ConditionsValue = Promotie.ConditionsValue,
                        VoucherType = Promotie.VoucherType,
                        ShowLogo = Promotie.ShowLogo,
                        PartnerName = Promotie.PartnerName,
                        PartnerImageUrl = Promotie.PartnerImageUrl,
                        TicketDetailsHeaderText = Promotie.TicketDetailsHeaderText,
                        OrderedByText = Promotie.OrderedByText,
                        PricePerTicketText = Promotie.PricePerTicketText,
                        AddressText = Promotie.AddressText,
                        ValidityText = Promotie.ValidityText,
                        ConditionsText = Promotie.ConditionsText,
                        ShoppingBagsImageUrl = Promotie.ShoppingBagsImageUrl,
                        LogoImageUrl = Promotie.LogoImageUrl,
                        ShortDescription = Promotie.ShortDescription,
                        Summary = Promotie.Summary,
                        Category = Promotie.Category,
                        Validity = Promotie.Validity,
                        Address = Promotie.Address,
                        Id = Promotie.Id,
                        Type = Promotie.Type,
                        Image = Promotie.Image
                    });

                    Console.WriteLine("shell gift 3");
                    Console.WriteLine("shellinfo rows to array " + JsonConvert.SerializeObject(Rows.ToArray()));

                }
                return Rows.ToArray();
            }
			else {
				
			}
			return null;
		}

		public void DrawGiftActivation(string date, string activated)
		{
			if (!string.IsNullOrEmpty(date) || !string.IsNullOrWhiteSpace(date))
			{
				DateTime To = DateTime.ParseExact(date.Replace("/", "-"), "dd-MM-yyyy HH:mm:ss", null);
				DateTime Now = DateTime.Now;

				int d;

				if (To < Now)
					d = 0;
				else
					d = (int)Math.Ceiling((To - Now).TotalDays);

				d++;

				string DaysBetween = "Geactiveerd: Nog ";
				if (d == 1)
					DaysBetween += (d + " dag");
				else
					DaysBetween += (d + " dagen");
				DaysBetween += " geldig";

				GiftActivationView = new UIView(new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, 30));
				GiftActivationView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

				if (activated == "true")
				{
					GiftActivationView.Hidden = false;
				}
				else {
					GiftActivationView.Hidden = true;
				}

				UILabel GiftActivationLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
				{
					MinimumFontSize = 12f,
					TextAlignment = UITextAlignment.Center,
					TextColor = UIColor.White,
					Font = UIFont.FromName("Avenir-Black", 14f),
					Lines = 1,
					Text = DaysBetween.ToUpper()
				};

				GiftActivationView.AddSubviews(GiftActivationLabel);
				View.Add(GiftActivationView);
			}
		}

		public void DrawGiftUsed()
		{
			GiftActivationView = new UIView(new CGRect(0, 64, UIScreen.MainScreen.Bounds.Width, 30));
			GiftActivationView.BackgroundColor = UIColor.FromRGB(0, 0, 0);

			UILabel GiftUsedLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
			{
				MinimumFontSize = 12f,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Black", 14f),
				Lines = 1,
				Text = "ACTIE INGEWISSELD"
			};
			GiftActivationView.Hidden = false;
			GiftActivationView.AddSubviews(GiftUsedLabel);
			View.Add(GiftActivationView);
		}

		public void DrawGiftsInCollectionView()
		{
			//Console.WriteLine("promotie giftsoffer gift length smilesinfo "+Promotie.GiftsOffer.Gift.Length);
			// default promotions

			Console.WriteLine("promotie vanuit promotieviewc "+JsonConvert.SerializeObject(Promotie));
			CollectionView.DataSource = new SmilesCollectionViewSource(this, GetCollectionViewItems(Promotie));
			CollectionView.Delegate = new SmilesFlowDelegate(this, GetCollectionViewItems(Promotie), true, false);
			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}

		}

		private class SmilesFlowDelegate : UICollectionViewDelegateFlowLayout
		{
			NetworkStatus Internet;

			// Overlays
			LoadingOverlay LoadingView;
			MaintainanceOverlay MaintainanceView;
			NetworkOverlay NetworkView;

			UIViewController Controller;
			Promotie[] Rows;
			UIStringAttributes attr;

			bool Header, Footer;
			bool UsedOffer;
			nfloat TitleHeight = 100;
			nfloat TextHeight = 0;

			public SmilesFlowDelegate(UIViewController parent, Promotie[] items, bool header, bool footer) : base()
			{
				Controller = parent;
				Rows = items;
				Header = header;
				Footer = footer;
			}

			public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
			{
				bool giftActivated = true;
				if (!string.IsNullOrEmpty(Rows[indexPath.Row].OfferGiftJson))
				{
					UsedOffer = JToken.Parse(Rows[indexPath.Row].OfferJson).Value<bool?>("OfferUsed") ?? false;
				}
				if (!string.IsNullOrEmpty(Rows[indexPath.Row].OfferGiftJson))
				{
					giftActivated = JToken.Parse(Rows[indexPath.Row].OfferGiftJson).Value<bool?>("GiftChosen") ?? false;
				}
				string text = "";
				attr = new UIStringAttributes() { Font = UIFont.FromName("Avenir-Book", 14f) };

				//if (giftActivated && Rows[indexPath.Row].ActivatedMessage != null)
				//{
				//	text = AMMain.RemoveHTMLTags(Rows[indexPath.Row].ActivatedMessage);

				//}
				if (Rows[indexPath.Row].OfferUsed && !string.IsNullOrEmpty(Rows[indexPath.Row].UsedOfferText))
				{
					text = AMMain.RemoveHTMLTags(Rows[indexPath.Row].UsedOfferText);
				}
				else if (Rows[indexPath.Row].ProductText != null)
				{
					text = AMMain.RemoveHTMLTags(Rows[indexPath.Row].ProductText);

				}
				if (!string.IsNullOrEmpty(text))
				{
					TextHeight = new NSString(text).GetSizeUsingAttributes(attr).Height;
					Console.WriteLine("text height "+TextHeight);
				}

				if (Rows[indexPath.Row].OfferUsed)
				{
					TitleHeight = 50;
				}

				nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
				nfloat ScreenPadding = 20;

				double HeightAspectRatio = 0.9;
				//double HeightAspectRatio = 1.05;
				Console.WriteLine("item height = " + (ScreenWidth - (ScreenPadding * 2)), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TextHeight + TitleHeight);
				return new CGSize(ScreenWidth - (ScreenPadding * 2), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TextHeight + TitleHeight);

			}

			public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.FromRGB(250, 250, 250);
			}

			public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
			{


			}

			public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
			{
				return false;
			}

			public override CGSize GetReferenceSizeForFooter(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				if (Footer)
				{
					return new CGSize(UIScreen.MainScreen.Bounds.Width, 0);
				}

				return new CGSize(0, 0);
			}

			public override CGSize GetReferenceSizeForHeader(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				UIDevice thisDevice = UIDevice.CurrentDevice;
				if (thisDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
				{
					if (Header)
					{
						if (UsedOffer)
						{
							return new CGSize(UIScreen.MainScreen.Bounds.Width, 220);
						}
						else
						{
							
							return new CGSize(UIScreen.MainScreen.Bounds.Width, 250);
						}

					}
				}else
				{
					if (Header)
					{
						if (UsedOffer)
						{
							return new CGSize(UIScreen.MainScreen.Bounds.Width, 150);
						}
						else
						{
							if (UIScreen.MainScreen.Bounds.Width > 320)
							{
								return new CGSize(UIScreen.MainScreen.Bounds.Width, 180);
							}
							else
							{
								return new CGSize(UIScreen.MainScreen.Bounds.Width, 160);
							}
						}

					}
				}

				return new CGSize(0, 0);
			}
		}

		public static async void RedirectToGiftDetails(UINavigationController NavCtrl, Promotie Promotie, bool NotActivated)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			Console.WriteLine("Promotie cheese " + JsonConvert.SerializeObject(Promotie));

			LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts);
			NavCtrl.VisibleViewController.Add(LoadingView);

			List<string> ProductIds = new List<string>();

			for (int i = 0; i < Promotie.GiftsOffer.Gift.Length; i++)
			{
				GiftsOfferGift gift = Promotie.GiftsOffer.Gift[i];

				ProductIds.Add(gift.ProductID);
			}

			string JoinedProductIds = string.Join(",", ProductIds);

			string giftshell = await GiftsService.GetMilesGiftsByIds(JoinedProductIds);
			Console.WriteLine("response shellgiftsbyid " + giftshell);
			GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(JoinedProductIds);
			List<Promotie> Rows = new List<Promotie>();

			if (GiftRoot.Gifts == null)
			{
				AMAlerts.DefaultAlert(NavCtrl.VisibleViewController, AMStrings.ErrorNoGiftsTitle, AMStrings.ErrorNoGiftsMessage);
			}
			else
			{
				// of the offer has more than 1 gifts
				if (GiftRoot.Gifts.Length > 1)
				{
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						GiftsOfferGift[] gifts = Promotie.GiftsOffer.Gift;
						GiftsOfferGift selectedGift = null;
						Gift product = GiftRoot.Gifts[i];

						foreach (GiftsOfferGift gift in gifts)
						{
							if (gift.ProductID == GiftRoot.Gifts[i].ProductId)
							{
								selectedGift = gift;
							}
						}

						Console.WriteLine("Set product " + product.PartnerImageUrl + "  json " + JsonConvert.SerializeObject(selectedGift));
						Console.WriteLine("shell title " + product.ProductTileTitle + " producttext " + product.ProductText + " buttonconfirm " + product.ConfirmButton);
						Console.WriteLine("giftroot to string "+product.ToString());
						Console.WriteLine("shellinfo storelocator "+product.StoreLocator);

						// Add gift (the promotion is part of an offer)
						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = product.ProductTileTitle,
							Image = product.RectangleImage,
							Logo = product.PartnerImageUrl,
							ShowLogo = product.ShowLogo,
							ProductText = product.ProductText,
							ConfirmButton = product.ConfirmButton,
							CancelButton = product.CancelButton,

							GiftJson = JsonConvert.SerializeObject(product),
							OfferJson = Promotie.OfferJson,
							OfferGiftJson = JsonConvert.SerializeObject(selectedGift),
							NotActivated = NotActivated,

							ConditionsLabel = product.ConditionsLabel,
							CheckConditionsLabel = product.CheckConditionsLabel,
							PaymentTitle = product.PaymentTitle,
							Keywords = product.Keywords,
							PaymentImage = product.PaymentImage,
							PaymentMethod = product.PaymentMethod,
							ProductType = product.ProductType,
							EndDate = product.EndDate,
							ProductId = product.ProductId,
							LinkPartner = product.LinkPartner,
							Conditions = product.Conditions,
							ButtonTitle = product.ButtonTitle,
							DeliverTime = product.DeliverTime,
							StartDate = product.StartDate,
							PaymentColor = product.PaymentColor,
							RectangleImage = product.RectangleImage,
							StoreLocator = product.StoreLocator,
							OpenCoupon = product.OpenCoupon,
							OfferNr = product.OfferNr,
							ItemNr = product.ItemNr,
							ActivatedMessage = product.ActivatedMessage,
							UsedCouponTitle = product.UsedCouponTitle,
							PrintButtonTitle = product.PrintButtonTitle,
							GiftValidUntil = product.GiftValidUntil,
							CancelButtonMobile = product.CancelButtonMobile,
							ActivatedMessageTitle = product.ActivatedMessageTitle,
							ProductTileTitle = product.ProductTileTitle,
							PreviewMessageActivated = product.PreviewMessageActivated,
							ChangeGift = product.ChangeGift,
							UsedOfferText = product.UsedOfferText,
							UsedOfferTitle = product.UsedOfferTitle,
							OpenDetails = product.OpenDetails,
							MilesOnly = product.MilesOnly,
							PropositionValue = product.PropositionValue,
							PropositionLabel = product.PropositionLabel,
							PriceValue = product.PriceValue,
							PriceLabel = product.PriceLabel,
							LogoUrl = product.LogoUrl,
							OptimizedLogoUrl = product.OptimizedLogoUrl,
							PeriodValidUntil = product.PeriodValidUntil,
							TicketSecondaryImageUrl = product.TicketSecondaryImageUrl,
							GeneralConditionsValue = product.GeneralConditionsValue,
							TicketImageUrl = product.TicketImageUrl,
							productSubType = product.ProductSubType,
							ConditionsValue = product.ConditionsValue,
							VoucherType = product.VoucherType,
							PartnerName = product.PartnerName,
							PartnerImageUrl = product.PartnerImageUrl,
							TicketDetailsHeaderText = product.TicketDetailsHeaderText,
							OrderedByText = product.OrderedByText,
							PricePerTicketText = product.PricePerTicketText,
							AddressText = product.AddressText,
							ValidityText = product.ValidityText,
							ConditionsText = product.ConditionsText,
							ShoppingBagsImageUrl = product.ShoppingBagsImageUrl,
							LogoImageUrl = product.LogoImageUrl,
							ShortDescription = product.ShortDescription,
							Summary = product.Summary,
							Category = product.Category,
							Validity = product.Validity,
							Address = product.Address,
							Id = product.Id,
							Type = product.Type
						});
					}

					PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;

					NavCtrl.PopViewController(false);

					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = "Wijzig";
					Redirect.Promoties = Rows.ToArray();

					NavCtrl.PushViewController(Redirect, false);
				}
			}

			LoadingView.Hide();
		}

		public static async void RedirectToGiftDetailsString(UINavigationController NavCtrl, Promotie Promotie, bool NotActivated)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts);
			NavCtrl.VisibleViewController.Add(LoadingView);

			//Console.WriteLine("promotie detailstring " + promotie);
			List<string> ProductIds = new List<string>();

			for (int i = 0; i < Promotie.GiftsOffer.Gift.Length; i++)
			{
				GiftsOfferGift gift = Promotie.GiftsOffer.Gift[i];

				ProductIds.Add(gift.ProductID);
			}

			string JoinedProductIds = string.Join(",", ProductIds);
			if (string.IsNullOrEmpty(JoinedProductIds))
			{
				JoinedProductIds = Promotie.ProductIds;
			}
			Console.WriteLine("joinedproductids " + JoinedProductIds);

			GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(JoinedProductIds);
			List<Promotie> Rows = new List<Promotie>();

			if (GiftRoot.Gifts == null)
			{
				AMAlerts.DefaultAlert(NavCtrl.VisibleViewController, AMStrings.ErrorNoGiftsTitle, AMStrings.ErrorNoGiftsMessage);
			}
			else
			{
				Console.WriteLine("giftroot length " + GiftRoot.Gifts.Length);
				// of the offer has more than 1 gifts
				if (GiftRoot.Gifts.Length > 1)
				{
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						GiftsOfferGift[] gifts = Promotie.GiftsOffer.Gift;
						GiftsOfferGift selectedGift = null;
						Gift product = GiftRoot.Gifts[i];

						foreach (GiftsOfferGift gift in gifts)
						{
							if (gift.ProductID == GiftRoot.Gifts[i].ProductId)
							{
								selectedGift = gift;
							}
						}

						Console.WriteLine("Set product " + product.PartnerImageUrl + "  json " + JsonConvert.SerializeObject(selectedGift));
						Console.WriteLine("shellinfo storelocator " + product.StoreLocator);

						// Add gift (the promotion is part of an offer)
						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							ConditionsLabel = product.ConditionsLabel,
							CheckConditionsLabel = product.CheckConditionsLabel,
							PaymentTitle = product.PaymentTitle,
							Title = AMMain.RemoveHTMLTags(product.ProductTileTitle),
							Keywords = product.Keywords,
							PaymentImage = product.PaymentImage,
							PaymentMethod = product.PaymentMethod,
							ProductType = product.ProductType,
							EndDate = product.EndDate,
							ProductId = product.ProductId,
							LinkPartner = product.LinkPartner,
							Conditions = product.Conditions,
							ProductText = product.ProductText,
							ButtonTitle = product.ButtonTitle,
							DeliverTime = product.DeliverTime,
							StartDate = product.StartDate,
							Logo = product.PartnerImageUrl,
							PaymentColor = product.PaymentColor,
							RectangleImage = product.RectangleImage,
							StoreLocator = product.StoreLocator,
							OpenCoupon = product.OpenCoupon,
							OfferNr = product.OfferNr,
							ItemNr = product.ItemNr,
							ActivatedMessage = product.ActivatedMessage,
							UsedCouponTitle = product.UsedCouponTitle,
							PrintButtonTitle = product.PrintButtonTitle,
							GiftValidUntil = product.GiftValidUntil,
							CancelButton = product.CancelButton,
							CancelButtonMobile = product.CancelButtonMobile,
							ActivatedMessageTitle = product.ActivatedMessageTitle,
							ProductTileTitle = product.ProductTileTitle,
							PreviewMessageActivated = product.PreviewMessageActivated,
							ChangeGift = product.ChangeGift,
							UsedOfferText = product.UsedOfferText,
							UsedOfferTitle = product.UsedOfferTitle,
							OpenDetails = product.OpenDetails,
							ConfirmButton = product.ConfirmButton,
							MilesOnly = product.MilesOnly,
							PropositionValue = product.PropositionValue,
							PropositionLabel = product.PropositionLabel,
							PriceValue = product.PriceValue,
							PriceLabel = product.PriceLabel,
							LogoUrl = product.LogoUrl,
							OptimizedLogoUrl = product.OptimizedLogoUrl,
							PeriodValidUntil = product.PeriodValidUntil,
							TicketSecondaryImageUrl = product.TicketSecondaryImageUrl,
							GeneralConditionsValue = product.GeneralConditionsValue,
							TicketImageUrl = product.TicketImageUrl,
							productSubType = product.ProductSubType,
							ConditionsValue = product.ConditionsValue,
							VoucherType = product.VoucherType,
							ShowLogo = product.ShowLogo,
							PartnerName = product.PartnerName,
							PartnerImageUrl = product.PartnerImageUrl,
							TicketDetailsHeaderText = product.TicketDetailsHeaderText,
							OrderedByText = product.OrderedByText,
							PricePerTicketText = product.PricePerTicketText,
							AddressText = product.AddressText,
							ValidityText = product.ValidityText,
							ConditionsText = product.ConditionsText,
							ShoppingBagsImageUrl = product.ShoppingBagsImageUrl,
							LogoImageUrl = product.LogoImageUrl,
							ShortDescription = product.ShortDescription,
							Summary = product.Summary,
							Category = product.Category,
							Validity = product.Validity,
							Address = product.Address,
							Id = product.Id,
							Type = product.Type,
							Image = product.Image,
							GiftJson = JsonConvert.SerializeObject(product),
							OfferJson = Promotie.OfferJson,
							OfferGiftJson = JsonConvert.SerializeObject(selectedGift)
						});
					}

					PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;

					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = Promotie.Title;
					Redirect.Promoties = Rows.ToArray();

					NavCtrl.PushViewController(Redirect, NotActivated);
				}
			}
			LoadingView.Hide();
		}

		private Promotie[] GetCollectionViewItems(Promotie promotie)
		{
			Console.WriteLine("promotie. smilesinfovc " + promotie);
			// Define lists for the Row
			List<Promotie> Rows = new List<Promotie>();

			string productTileTitle = "";
			string rectangleImage = "";
			string logo = "";
			bool showLogo = false;
			string productText = "";
			string cancelButton = "";
			string confirmButton = "";
			string conditionsButton = "";
			string generalConditions = "";
			string conditionsTitle = "";
			string activatedMessage = "";
			string activatedMessageTitle = "";
			string giftValidUntil = "";
			string chooseGifts = "";
			string giftJson = "";
			string offerJson = "";
			string offerGiftJson = "";
			string productIds = "";
			string promotieJson = JsonConvert.SerializeObject(promotie);
			string previewActivatedMessage = "";

			if (promotie.GiftJson != null)
			{
				productTileTitle = JToken.Parse(promotie.GiftJson).Value<string>("productTileTitle");
				rectangleImage = JToken.Parse(promotie.GiftJson).Value<string>("rectangleImage");
				logo = JToken.Parse(promotie.GiftJson).Value<string>("logo");
				showLogo = JToken.Parse(promotie.GiftJson).Value<bool>("showLogo");
				productText = JToken.Parse(promotie.GiftJson).Value<string>("productText");
				cancelButton = JToken.Parse(promotie.GiftJson).Value<string>("cancelButton");
				confirmButton = JToken.Parse(promotie.GiftJson).Value<string>("confirmButton");
				conditionsButton = JToken.Parse(promotie.GiftJson).Value<string>("checkConditionsLabel");
				generalConditions = JToken.Parse(promotie.GiftJson).Value<string>("generalConditionsValue");
				conditionsTitle = JToken.Parse(promotie.GiftJson).Value<string>("conditions");
				activatedMessage = AMMain.RemoveHTMLTags(JToken.Parse(promotie.GiftJson).Value<string>("activatedMessage"));
				activatedMessageTitle = AMMain.RemoveHTMLTags(JToken.Parse(promotie.GiftJson).Value<string>("activatedMessageTitle"));
				giftValidUntil = JToken.Parse(promotie.GiftJson).Value<string>("GiftValidUntil");
				chooseGifts = JToken.Parse(promotie.GiftJson).Value<string>("chooseGifts");
				productIds = JToken.Parse(promotie.GiftJson).Value<string>("ProductIds");
				giftJson = promotie.GiftJson;
				offerJson = promotie.OfferJson;
				offerGiftJson = promotie.OfferGiftJson;
				previewActivatedMessage = JToken.Parse(promotie.GiftJson).Value<string>("previewMessageActivated");
				Console.WriteLine("promotie.giftjson " + JsonConvert.SerializeObject(promotie.GiftJson));
				Console.WriteLine("promotie json smilesinfovc "+promotieJson);
				//}

				// Add the Table Row
				Rows.Add(new Promotie()
				{
					Title = productTileTitle,
					ProductTileTitle = productTileTitle,
					RectangleImage = rectangleImage,
					Logo = logo,
					ShowLogo = showLogo,
					ProductText = AMMain.RemoveHTMLTags(productText),
					PersonalizedPromotionJson = promotieJson,
					CancelButton = cancelButton,
					ConfirmButton = confirmButton,
					ConditionsButton = conditionsButton,
					GeneralConditions = generalConditions,
					ConditionsTitle = AMMain.RemoveHTMLTags(conditionsTitle),
					ActivatedMessage = AMMain.RemoveHTMLTags(activatedMessage),
					ActivatedMessageTitle = AMMain.RemoveHTMLTags(activatedMessageTitle),
					GiftValidUntil = giftValidUntil,
					ChooseGifts = chooseGifts,
					OfferJson = offerJson,
					OfferGiftJson = offerGiftJson,
					ProductIds = productIds,
					GiftJson = giftJson,
					PreviewMessageActivated = previewActivatedMessage

				});
			} 			else 			{ 				productTileTitle = promotie.ProductTileTitle; 				rectangleImage = promotie.RectangleImage; 				logo = promotie.Logo; 				showLogo = promotie.ShowLogo; 				productText = promotie.ProductText; 				cancelButton = promotie.CancelButton; 				confirmButton = promotie.ConfirmButton; 				conditionsButton = promotie.ConditionsButton; 				generalConditions = promotie.GeneralConditions; 				conditionsTitle = promotie.ConditionsTitle; 				activatedMessage = AMMain.RemoveHTMLTags(promotie.ActivatedMessage); 				activatedMessageTitle = AMMain.RemoveHTMLTags(promotie.ActivatedMessageTitle); 				giftValidUntil = promotie.GiftValidUntil; 				chooseGifts = promotie.ChooseGifts; 				giftJson = promotie.GiftJson; 				offerJson = promotie.OfferJson; 				offerGiftJson = promotie.OfferGiftJson;
				productIds = promotie.ProductIds;
				previewActivatedMessage = promotie.PreviewMessageActivated; 				Console.WriteLine("promotie.giftjson " + JsonConvert.SerializeObject(promotie.GiftJson));

				Rows.Add(new Promotie()
				{
					Title = productTileTitle,
					ProductTileTitle = productTileTitle,
					RectangleImage = rectangleImage,
					Logo = logo,
					ShowLogo = showLogo,
					ProductText = AMMain.RemoveHTMLTags(productText),
					CancelButton = cancelButton,
					ConfirmButton = confirmButton,
					ConditionsButton = conditionsButton,
					GeneralConditions = generalConditions,
					PersonalizedPromotionJson = promotieJson,
					ConditionsTitle = AMMain.RemoveHTMLTags(conditionsTitle),
					ActivatedMessage = AMMain.RemoveHTMLTags(activatedMessage),
					ActivatedMessageTitle = AMMain.RemoveHTMLTags(activatedMessageTitle),
					GiftValidUntil = giftValidUntil,
					ChooseGifts = chooseGifts,
					OfferJson = offerJson,
					OfferGiftJson = offerGiftJson,
					ProductIds = productIds,
					GiftJson = giftJson,
					PreviewMessageActivated = previewActivatedMessage
  			} );
			}

			// Return the Rows as an arra
			return Rows.ToArray();

		}

		public void SetPromotions(Promotie[] promoties)
		{
			Promoties = promoties;
		}


		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		void Refresh()
		{

			RedrawGifts = false;

			CollectionView.ReloadData();
			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
		}

		public void GetPersonalGiftsFromPartner()
		{

			// personal offers from locals storage
			string JsonGift = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);

			Console.WriteLine("jsongift " + JsonGift);
			if (!string.IsNullOrEmpty(JsonGift))
			{
				GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(JsonGift);
				Console.WriteLine("Gifts convert " + JsonConvert.SerializeObject(Gifts.Response));
				Console.WriteLine("gifts response length " + Gifts.Response.Length);
				if (Gifts.Response.Length > 0)
				{
					for (int i = 0; i < Gifts.Response.Length; i++)
					{
						// Rempve tab badge
						AMMain.ClearAanbodTabBadge();

						bool productNotChosen = true;
						GiftsOffer offer = Gifts.Response[i].Offer[0];

						// if gift is activated
						if (offer.Activated == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);
									}
									// change bool
									productNotChosen = false;
								}
							}

						}
						// if gift is activated
						else if (offer.OfferUsed == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);
									}
									// change bool
									productNotChosen = false;
								}
							}

						}
						if (/*productNotChosen &&*/ !giftPartners.Contains(offer.Partner))
						{
							Console.WriteLine("add " + offer.Partner + " to list");

							// add the partnername to the list
							giftPartners.Add(offer.Partner);
						}
					}
				}

				// personal promotions from local storage
				string JsonPersonalizedPromotions = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions);

				if (!string.IsNullOrEmpty(JsonPersonalizedPromotions))
				{
					Console.WriteLine("personalized promotions " + JsonPersonalizedPromotions);
					MendixPersonalizedPromotionRoot PersonalizedPromotions = JsonConvert.DeserializeObject<MendixPersonalizedPromotionRoot>(JsonPersonalizedPromotions);
					Console.WriteLine("PersonalizedPromotionRoot " + JsonConvert.SerializeObject(PersonalizedPromotions));
					Console.WriteLine("personalizedpromotions errorcode " + PersonalizedPromotions.Errors[0].Code);
					if (PersonalizedPromotions.Errors[0].Code != "E00" && PersonalizedPromotions.Response.Length == 0)
					{
						// view WITHOUT personalized gifts
						GeneratePersonalGiftView(Gifts.Response, null);
					}
					else
					{
						// view WITH personalized gifts
						GeneratePersonalGiftView(Gifts.Response, PersonalizedPromotions);
					}
				}
			}
		}

		public async void GeneratePersonalGiftView(GiftsResponse[] Offers, MendixPersonalizedPromotionRoot PersonalizedPromotionsRoot)
		{
			SetLoader(AMStrings.LoadingGifts);
			if (GiftsPromoties != null)
			{
				GiftsPromoties.Clear();
			}
			if (GiftsPromoties == null)
			{
				GiftsPromoties = new List<Promotie>();
			}

			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Persoonlijk aanbod");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			// create a dictionary where the hippo-returns are stored. based on their partnername
			var GiftOfferPartnerDictionary = new Dictionary<string, object>();

			//var GiftsDictionary = new Dictionary<string, GiftRoot>();

			string productIds = null;

			// create another dictionay where the counters for the images are stored
			var CounterDictionary = new Dictionary<string, int>();

			Console.WriteLine("found partners " + giftPartners.Count.ToString());
			Console.WriteLine("giftpartners " + JsonConvert.SerializeObject(giftPartners));
			Console.WriteLine("giftproducts " + JsonConvert.SerializeObject(giftProducts));

			bool offerUsed = false;

			if (giftPartners.Count > 0)
			{
				// Set partner gifts
				foreach (string partner in giftPartners)
				{
					GiftsOfferPartner PartnerGifts = await GiftsService.GetGiftsFromPartner(partner);

					if (PartnerGifts != null)
					{
						// save the data in the dictionary
						GiftOfferPartnerDictionary.Add(partner, PartnerGifts);
						// set counter at 0
						CounterDictionary.Add(partner, 0);
					}
					else
					{
						Console.WriteLine($"no partner git found in {partner}");
					}
				}
			}

			if (giftProducts.Count > 0)
			{
				productIds = string.Join(",", giftProducts);
				GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(productIds);

				Console.WriteLine($"productIds: {productIds}");

				if (GiftRoot.Gifts == null)
				{
					Console.WriteLine("geen data");
				}
				else
				{
					Console.WriteLine("Giftroot.Gifts.Lenght " + GiftRoot.Gifts.Length);
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						Gift product = GiftRoot.Gifts[i];
						GiftOfferPartnerDictionary.Add(product.ProductId, product);
					}
				}
			}

			// again loop over all offers and create the offer-objects with the hippo data attached
			Console.WriteLine("Offers.Length " + Offers.Length);
			for (int i = 0; i < Offers.Length; i++)
			{
				string partnerName = Offers[i].Offer[0].Partner;

				bool showLogo = false;
				string img = "";
				string logo = "";
				string title = "";
				string giftJson = "";
				string offerGiftJson = "";
				string usedOfferTitle = "";
				string usedOfferText = "";
				string RectangleImage = "";
				string Partner = "";
				string ProductValidUntil = "";
				string productTileTitle = "";
				string ProductId = "";
				string endDate = "";
				string registrationDate = "";
				string priceTitle = "";
				string priceDetail = "";
				string propositionTitle = "";
				string propositionDetail = "";
				string url = "";
				string activatedMessage = "";
				string chooseGifts = "";
				string giftValidTo = "";
				string productText = "";
				string conditionsLabel = "";
				string checkConditionsLabel = "";
				string paymentTitle = "";
				string paymentImage = "";
				string paymentMethod = "";
				string productType = "";
				string linkPartner = "";
				string conditions = "";
				string buttonTitle = "";
				string deliverTime = "";
				string startDate = "";
				string paymentColor = "";
				string storeLocator = "";
				string openCoupon = "";
				string usedCouponTitle = "";
				string printButtonTitle = "";
				string giftValidUntil = "";
				string cancelButton = "";
				string cancelButtonMobile = "";
				string activatedMessageTitle = "";
				string previewMessageActivated = "";
				string changeGift = "";
				string openDetails = "";
				string confirmButton = "";
				bool milesOnly = false;
				string propositionValue = "";
				string propositionLabel = "";
				string priceValue = "";
				string priceLabel = "";
				string logoUrl = "";
				string optimizedLogoUrl = "";
				string periodValidUntil = "";
				string ticketSecondaryImageUrl = "";
				string generalConditionsValue = "";
				string ticketImageUrl = "";
				string productSubType = "";
				string conditionsValue = "";
				string voucherType = "";
				string partnerImageUrl = "";
				string ticketDetailsHeaderText = "";
				string orderedByText = "";
				string pricePerTicketText = "";
				string addressText = "";
				string validityText = "";
				string conditionsText = "";
				string shoppingBagsImageUrl = "";
				string logoImageUrl = "";
				string shortDescription = "";
				string summary = "";
				string category = "";
				string validity = "";
				string address = "";
				string id = "";
				string type = "";
				string itemNr = "";
				string offerNr = "";

				//offer to add
				GiftsOffer thisOffer = Offers[i].Offer[0];
				string offerJson = JsonConvert.SerializeObject(thisOffer);

				AMMain.SetSeenGifts(thisOffer.OfferNr);

				Gift productDetails = null;


				//if an offer is activated find the selected gift
				//if (thisOffer.Activated == "true")
				//{
				Console.WriteLine("activated offers " + thisOffer.Gift.Length);
				Console.WriteLine("offerjson string " + offerJson);
				Console.WriteLine("offerjson proddet " + JsonConvert.SerializeObject(thisOffer));
				for (int z = 0; z < thisOffer.Gift.Length; z++)
				{
					GiftsOfferGift Gift = thisOffer.Gift[z];
					Console.WriteLine("");
					if (Gift.GiftChosen == "true")
					{
						if (GiftOfferPartnerDictionary.ContainsKey(Gift.ProductID))
						{
							Console.WriteLine("product Id offer: " + Gift.ProductID);
							productDetails = (Gift)GiftOfferPartnerDictionary[Gift.ProductID];

							Console.WriteLine("Gift usedOfferTitle " + productDetails.UsedOfferTitle);
							Console.WriteLine("giftjson proddet " + JsonConvert.SerializeObject(productDetails));
							Console.WriteLine("offergiftjson proddet " + JsonConvert.SerializeObject(Gift));

							// set the image and logo
							img = productDetails.Image;
							logo = productDetails.PartnerImageUrl;
							showLogo = productDetails.ShowLogo;
							RectangleImage = productDetails.RectangleImage;
							usedOfferTitle = productDetails.UsedCouponTitle;
							productTileTitle = productDetails.ProductTileTitle;
							usedOfferText = productDetails.UsedOfferText;
							Partner = productDetails.PartnerName;
							ProductId = Gift.ProductID;
							ProductValidUntil = productDetails.GiftValidUntil;
							endDate = productDetails.EndDate;
							registrationDate = productDetails.RegistrationDate;
							priceTitle = productDetails.PriceTitle;
							priceDetail = productDetails.PriceDetail;
							propositionTitle = productDetails.PropositionTitle;
							propositionDetail = productDetails.PropositionDetail;
							url = productDetails.Url;
							activatedMessage = productDetails.ActivatedMessage;
							chooseGifts = productDetails.ChooseGifts;
							giftValidTo = productDetails.GiftValidUntil;
							productText = productDetails.ProductText;
							conditionsLabel = productDetails.ConditionsLabel;
							checkConditionsLabel = productDetails.CheckConditionsLabel;
							paymentTitle = productDetails.PaymentTitle;
							paymentImage = productDetails.PaymentImage;
							paymentMethod = productDetails.PaymentMethod;
							productType = productDetails.ProductType;
							linkPartner = productDetails.LinkPartner;
							conditions = productDetails.Conditions;
							buttonTitle = productDetails.ButtonTitle;
							deliverTime = productDetails.DeliverTime;
							startDate = productDetails.StartDate;
							paymentColor = productDetails.PaymentColor;
							storeLocator = productDetails.StoreLocator;
							openCoupon = productDetails.OpenCoupon;
							usedCouponTitle = productDetails.UsedCouponTitle;
							printButtonTitle = productDetails.PrintButtonTitle;
							giftValidUntil = productDetails.GiftValidUntil;
							cancelButton = productDetails.CancelButton;
							cancelButtonMobile = productDetails.CancelButtonMobile;
							activatedMessageTitle = productDetails.ActivatedMessageTitle;
							productTileTitle = productDetails.ProductTileTitle;
							previewMessageActivated = productDetails.PreviewMessageActivated;
							changeGift = productDetails.ChangeGift;
							openDetails = productDetails.OpenDetails;
							confirmButton = productDetails.ConfirmButton;
							milesOnly = productDetails.MilesOnly;
							propositionValue = productDetails.PropositionValue;
							propositionLabel = productDetails.PropositionLabel;
							priceValue = productDetails.PriceValue;
							priceLabel = productDetails.PriceLabel;
							logoUrl = productDetails.LogoUrl;
							optimizedLogoUrl = productDetails.OptimizedLogoUrl;
							periodValidUntil = productDetails.PeriodValidUntil;
							ticketSecondaryImageUrl = productDetails.TicketSecondaryImageUrl;
							generalConditionsValue = productDetails.GeneralConditionsValue;
							ticketImageUrl = productDetails.TicketImageUrl;
							productSubType = productDetails.ProductSubType;
							conditionsValue = productDetails.ConditionsValue;
							voucherType = productDetails.VoucherType;
							partnerImageUrl = productDetails.PartnerImageUrl;
							ticketDetailsHeaderText = productDetails.TicketDetailsHeaderText;
							orderedByText = productDetails.OrderedByText;
							pricePerTicketText = productDetails.PricePerTicketText;
							addressText = productDetails.AddressText;
							validityText = productDetails.ValidityText;
							conditionsText = productDetails.ConditionsText;
							shoppingBagsImageUrl = productDetails.ShoppingBagsImageUrl;
							logoImageUrl = productDetails.LogoImageUrl;
							shortDescription = productDetails.ShortDescription;
							summary = productDetails.Summary;
							category = productDetails.Category;
							validity = productDetails.Validity;
							address = productDetails.Address;
							id = productDetails.Id;
							type = productDetails.Type;
							itemNr = productDetails.ItemNr;
							offerNr = productDetails.OfferNr;

							// set the json for the offer objects
							if (!string.IsNullOrEmpty(productDetails.PeriodValidUntil))
							{
								ProductValidUntil = productDetails.PeriodValidUntil;
							}
							giftJson = JsonConvert.SerializeObject(productDetails);
							offerGiftJson = JsonConvert.SerializeObject(Gift);

							// if offer is used
							if (thisOffer.OfferUsed == "true")
							{
								title = productDetails.UsedOfferTitle;

							}
							if (thisOffer.Activated == "true")
							{
								title = productDetails.ActivatedMessageTitle;
							}
							else
							{
								title = productDetails.ActivatedMessageTitle;
							}

						}
					}

				}
				//}
				// if offer is used
				if (Gift.OfferUsed)
				{
					offerUsed = true;
					title = productDetails.UsedCouponTitle;

				}
				else
				{
					offerUsed = false;
				}

				// if the img is not set with a chosen product
				if (img == "")
				{

					Console.WriteLine("thisoffer " + JsonConvert.SerializeObject(thisOffer));
					Console.WriteLine("product id giftsoffer " + thisOffer.Gift[i].ProductID);
					//hippo data to add to the offer, from the dictionary
					GiftsOfferPartner partnerDetails = (GiftsOfferPartner)GiftOfferPartnerDictionary[partnerName];

					if (partnerDetails != null)
					{
						string[] images = partnerDetails.PreviewImages;
						int counter = CounterDictionary[partnerName];
						int max = images.Length - 1;
						img = images[counter];
						Console.WriteLine("counter images gift " + counter);

						if (counter >= max)
							CounterDictionary[partnerName] = 0;
						else
							CounterDictionary[partnerName] = counter + 1;
						title = partnerDetails.PreviewTitle;
						logo = partnerDetails.PartnerProgramLogo;
						showLogo = true;
					}
				}

				if (GiftsPromoties == null)
				{
					GiftsPromoties = new List<Promotie>();
				}
				Console.WriteLine("SmilesInfo Promotie.Title "+Promotie.Title);
				Console.WriteLine("Deze row title "+AMMain.RemoveHTMLTags(productTileTitle));


				if (Promotie.ProductId == thisOffer.Gift[i].ProductID)
				{
					Console.WriteLine("produtc Id lalala " + thisOffer.Gift[i].ProductID);
					Console.WriteLine("generalconditionsvalue " + generalConditionsValue);
					Console.WriteLine("smilesinfo offergiftjson " + JsonConvert.SerializeObject(offerGiftJson));
					// Add offer
					GiftsPromoties.Add(new Promotie()
					{
						PromotionType = PromotieType.Offer,
						Title = AMMain.RemoveHTMLTags(title),
						Id = thisOffer.Gift[i].ProductID,
						Image = img,
						RectangleImage = RectangleImage,
						Logo = logo,
						ShowLogo = showLogo,
						GiftsOffer = thisOffer,
						ProductIds = productIds,
						GiftJson = giftJson,
						OfferJson = offerJson,
						OfferGiftJson = offerGiftJson,
						UsedOfferTitle = usedOfferTitle,
						UsedOfferText = usedOfferText,
						OfferUsed = offerUsed,
						PartnerName = Partner,
						ItemNr = itemNr,
						OfferNr = offerNr,
						EndDate = endDate,
						RegistrationDate = registrationDate,
						PriceTitle = AMMain.RemoveHTMLTags(priceTitle),
						PriceDetail = AMMain.RemoveHTMLTags(priceDetail),
						PropositionTitle = AMMain.RemoveHTMLTags(propositionTitle),
						PropositionDetail = AMMain.RemoveHTMLTags(propositionDetail),
						Url = url,
						ActivatedMessage = AMMain.RemoveHTMLTags(activatedMessage),
						ChooseGifts = chooseGifts,
						GiftValidTo = giftValidTo,
						ProductText = productText,
						ConditionsLabel = conditionsLabel,
						CheckConditionsLabel = checkConditionsLabel,
						PaymentTitle = paymentTitle,
						PaymentImage = paymentImage,
						PaymentMethod = paymentMethod,
						ProductType = productType,
						ProductId = thisOffer.Gift[i].ProductID,
						LinkPartner = linkPartner,
						Conditions = conditions,
						ButtonTitle = buttonTitle,
						DeliverTime = deliverTime,
						StartDate = startDate,
						PaymentColor = paymentColor,
						StoreLocator = storeLocator,
						OpenCoupon = openCoupon,
						UsedCouponTitle = usedCouponTitle,
						PrintButtonTitle = printButtonTitle,
						GiftValidUntil = giftValidUntil,
						CancelButton = cancelButton,
						CancelButtonMobile = cancelButtonMobile,
						ActivatedMessageTitle = activatedMessageTitle,
						ProductTileTitle = productTileTitle,
						PreviewMessageActivated = previewMessageActivated,
						ChangeGift = changeGift,
						OpenDetails = openDetails,
						ConfirmButton = confirmButton,
						MilesOnly = milesOnly,
						PropositionValue = propositionValue,
						PropositionLabel = propositionLabel,
						PriceValue = priceValue,
						PriceLabel = priceLabel,
						LogoUrl = logoUrl,
						OptimizedLogoUrl = optimizedLogoUrl,
						PeriodValidUntil = periodValidUntil,
						TicketSecondaryImageUrl = ticketSecondaryImageUrl,
						GeneralConditionsValue = generalConditionsValue,
						TicketImageUrl = ticketImageUrl,
						productSubType = productSubType,
						ConditionsValue = conditionsValue,
						VoucherType = voucherType,
						PartnerImageUrl = partnerImageUrl,
						TicketDetailsHeaderText = ticketDetailsHeaderText,
						OrderedByText = orderedByText,
						PricePerTicketText = pricePerTicketText,
						AddressText = addressText,
						ValidityText = validityText,
						ConditionsText = conditionsText,
						ShoppingBagsImageUrl = shoppingBagsImageUrl,
						LogoImageUrl = logoImageUrl,
						ShortDescription = shortDescription,
						Summary = summary,
						Category = category,
						Validity = validity,
						Address = address,
						Type = type,
						//Segue = "PromotieDetailSegue",
					});
				}

			}

			if (GiftsPromoties == null || GiftsPromoties.Count == 0)
			{
				LoadingView.Hide();
				SetBackupView("Op dit moment zijn er geen persoonlijke acties beschikbaar. Houd deze pagina goed in de gaten!");
			}
			else
			{
				Console.WriteLine("giftspromoties to array " + JsonConvert.SerializeObject(GiftsPromoties.ToArray()));
				LoadingView.Hide();


				CollectionView.Source = new SmilesCollectionViewSource(this, GiftsPromoties.ToArray());
				CollectionView.Delegate = new SmilesFlowDelegate(this, GiftsPromoties.ToArray(), true, false);
				//CollectionView.RegisterClassForSupplementaryView(typeof(PromotionCollectionViewHeader), UICollectionElementKindSection.Header, "PersonalPromotionsCollectionViewHeader");
			}


		}

		public void SetBackupView(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			TransactiesOverlay = new TransactiesOverlay(bounds, text);
			View.Add(TransactiesOverlay);
		}
	}
}