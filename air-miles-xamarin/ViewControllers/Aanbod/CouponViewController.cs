﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using Air_Miles_Xamarin;
using System.Text.RegularExpressions;
using Google.Analytics;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using System.Collections.Generic;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
	public partial class CouponViewController : UIViewController
	{
		UIImageView Border, Barcode;
		UILabel ErrorLabel, CouponNr, CouponTitle;

		PersonalizedPromotion Promotion;

		public CouponViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = "Coupon";

			NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
			UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
			{
				NavigationController.PopViewController(true);
			}), true);

			nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
			nfloat HalfWidth = ScreenWidth / 2;
			nfloat HalfHeight = UIScreen.MainScreen.Bounds.Height / 2;

			Border = new UIImageView(new CGRect(0, HalfHeight - 4, ScreenWidth, 8));
			Border.Layer.MasksToBounds = true;
			Border.Image = UIImage.FromFile("Bullets.png");

			View.AddSubview(Border);

			BarcodeView.TranslatesAutoresizingMaskIntoConstraints = true;
			BarcodeView.Frame = new CGRect(0, 0, ScreenWidth, HalfHeight);

			Barcode = new UIImageView(new CGRect(40, (HalfHeight / 2) - 50, ScreenWidth - 80, 100));

			ErrorLabel = new UILabel(new CGRect(0, (HalfHeight / 2) - 30, ScreenWidth, 100));
			ErrorLabel.Font = UIFont.FromName("Avenir-book", 14f);
			ErrorLabel.Text = "Helaas, kan de barcode niet genereren.";
			ErrorLabel.TextColor = UIColor.FromRGB(242, 65, 65);
			ErrorLabel.TextAlignment = UITextAlignment.Center;

			CouponNr = new UILabel(new CGRect(0, (HalfHeight / 2) + 50, ScreenWidth, 50));
			CouponNr.Font = UIFont.FromName("Avenir-book", 18f);
			CouponNr.Text = Promotion.EanCompound.EanCode;
			CouponNr.TextColor = UIColor.FromRGB(51, 51, 51);
			CouponNr.TextAlignment = UITextAlignment.Center;

			BarcodeView.AddSubviews(Barcode, ErrorLabel, CouponNr);

			TitleView.TranslatesAutoresizingMaskIntoConstraints = true;
			TitleView.Frame = new CGRect(0, HalfHeight, ScreenWidth, HalfHeight);

			CouponTitle = new UILabel(new CGRect(20, 0, ScreenWidth - 40, HalfHeight - 50));
			CouponTitle.Lines = 0;
			CouponTitle.Font = UIFont.FromName("Avenir-Heavy", 35f);
			CouponTitle.Text = Promotion.Title;
			CouponTitle.TextColor = UIColor.FromRGB(51, 51, 51);
			CouponTitle.TextAlignment = UITextAlignment.Center;

			TitleView.AddSubviews(CouponTitle);

			string barcodeNumber = Promotion.EanCompound.EanCode;

			// Create the barcode
			ZXing.BarcodeFormat barcodeFormat = new ZXing.BarcodeFormat();
			bool dontTry = false;

			try
			{

			if (barcodeNumber.Length == 13)
				barcodeFormat = ZXing.BarcodeFormat.EAN_13;
			else
				barcodeFormat = ZXing.BarcodeFormat.CODE_128;


					Console.WriteLine("generate barcode");
					var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
					{
						Format = barcodeFormat,
						Options = new ZXing.Common.EncodingOptions
						{
							Width = 300,
							Height = 100,
							Margin = 0
						}
					};

					var barcode = BarcodeWriter.Write(barcodeNumber);
					Barcode.Image = BarcodeWriter.Write(barcodeNumber);
					ErrorLabel.Hidden = true;
					Barcode.Hidden = false;
					CouponNr.Font = UIFont.FromName("Avenir-book", 18f);
				}
				catch (Exception e)
				{
                Crashes.TrackError(e);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Coupon", "Exception: "+ e}
                });
					Console.WriteLine("unable to generate barcode");
				//ErrorLabel.Text = "Helaas, kan de barcode niet genereren.";
				//ErrorLabel.Hidden = false;
					CouponNr.Font = UIFont.FromName("Avenir-book", 28f);
					Barcode.Hidden = true;
					Console.WriteLine(e);
				}

			CouponNr.Text = barcodeNumber;
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			// Google analaytics
			var GAItabkeuze = DictionaryBuilder.CreateEvent("Overig aanbod: details", "Bekijk voucher", "", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
			Gai.SharedInstance.Dispatch();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		public void SetPromotion(PersonalizedPromotion promotion)
		{
			Promotion = promotion;
		}

		private void SetBarcode()
		{
			// Create the barcode
			var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
			{
				Format = ZXing.BarcodeFormat.EAN_13,
				Options = new ZXing.Common.EncodingOptions
				{
					Width = 300,
					Height = 70,
					Margin = 0
				}
			};

			Barcode.Image = BarcodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
		}
    }
}