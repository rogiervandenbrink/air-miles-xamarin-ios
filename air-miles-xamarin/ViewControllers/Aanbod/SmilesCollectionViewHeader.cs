﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public partial class SmilesCollectionViewHeader : UICollectionReusableView
	{
		UILabel Title;
		UILabel RedText;
		UIImageView HeaderImageView;
		UIViewController Controller;
		bool CellIsUpdated;

		public string Text
		{
			get
			{
				return Title.Text;
			}
			set
			{
				Title.Text = value;
				SetNeedsDisplay();
			}
		}

		public SmilesCollectionViewHeader(IntPtr handle) : base(handle)
		{
		}

		public void SetImage(string image)
		{
			Console.WriteLine("set footer image to " + image);
			if (!string.IsNullOrEmpty(image))
			{
				FromUrl(image);
				/*Image.SetImage(

				url: new NSUrl(image),
				placeholder: UIImage.FromBundle("Airmiles_Placeholder")
			);*/
			}
		}

		static UIImage FromUrl(string uri)
		{
			using (var url = new NSUrl(uri))
			using (var data = NSData.FromUrl(url))
				return UIImage.LoadFromData(data);
		}

        public void UpdateHeader(Promotie item, UIViewController parent, string image, string heading)
        {

            BackgroundColor = UIColor.White;
            SmilesHeaderView.BackgroundColor = UIColor.White;
            Controller = parent;

            if (Redline != null)
            {
                Redline.Frame = new CGRect(0, this.Frame.Height - 3, UIScreen.MainScreen.Bounds.Width, 3);
            }
            if (RedLine != null)
            {
                RedLine.Frame = new CGRect(0, this.Frame.Height - 3, UIScreen.MainScreen.Bounds.Width, 3);
            }
            Console.WriteLine("item.giftjsonnnn " + JsonConvert.SerializeObject(item));
            string Valid = "";
            if (!string.IsNullOrEmpty(item.GiftValidUntil))
            {
                Valid = item.GiftValidUntil;
            }
            if (item.GiftJson != "null")
            {
                Valid = JToken.Parse(item.GiftJson).Value<string>("giftValidUntil");
            }

            string validUntil = JToken.Parse(item.OfferJson).Value<string>("OfferValidTo");
            DateTime dateOfferValidTo = DateTime.ParseExact(validUntil, "dd-MM-yyyy HH:mm:ss", null);
            string redtext = "";
            //if (!string.IsNullOrEmpty(item.GiftValidUntil)) {
              redtext = Valid + " " + dateOfferValidTo.ToString("D", AMMain.Cultureinfo);
            //}

			bool giftActivated = JToken.Parse(item.OfferGiftJson).Value<bool?>("GiftChosen") ?? false;

			string activatedmessagetitle = AMMain.RemoveHTMLTags(item.ActivatedMessageTitle);
            string usedOfferTitle = "";
            //if (item.GiftJson != "null")
            if (item.GiftJson != null)
            {
                usedOfferTitle = JToken.Parse(item.GiftJson).Value<string>("usedOfferTitle").ToUpper();
            }
			bool UsedOffer = JToken.Parse(item.OfferJson).Value<bool?>("OfferUsed") ?? false;

			//string activatedmessagetitle = JToken.Parse(item.GiftJson).Value<string>("activatedMessageTitle");
			Console.WriteLine("item.activatedmesstitlen Header " + item.ActivatedMessageTitle);
			Console.WriteLine("item header " + JsonConvert.SerializeObject(item));
			//Console.WriteLine("item header usedoffertitle " + JToken.Parse(item.GiftJson).Value<string>("usedOfferTitle"));
			Console.WriteLine("item header used " + UsedOffer);
			Console.WriteLine("smiles header activated " + giftActivated);
			//UIScreen.MainScreen.Bounds.Width * 0.15217

			if (!string.IsNullOrEmpty(image))
			{
				HeaderImageView = new UIImageView()
				{
					Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Width * 0.16),
					Image = FromUrl(image),
					BackgroundColor = UIColor.White,
					ContentMode = UIViewContentMode.ScaleAspectFit,

				};
				AddSubview(HeaderImageView);
			}

			if (Controller.GetType() == typeof(SmilesDetailViewController))
			{
				Console.WriteLine("item header smilesdetail " + JsonConvert.SerializeObject(item));
				//MilesmeImageView.Image = FromUrl(image);
				KIESJOUWLabel.Hidden = true;
				TANKBIJLabel.Hidden = true;

				UIDevice thisDevice = UIDevice.CurrentDevice;
				if (thisDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
				{
					if (UsedOffer && !string.IsNullOrEmpty(usedOfferTitle))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							Title = new UILabel()
							{
								Frame = new CGRect(120, (UIScreen.MainScreen.Bounds.Width * 0.1) + 30, UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 24f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (usedOfferTitle).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
						else
						{
							Title = new UILabel()
							{
								Frame = new CGRect(120, (UIScreen.MainScreen.Bounds.Width * 0.1) + 30, UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 20f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (usedOfferTitle).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}

					}
					else if (!string.IsNullOrEmpty(heading))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 30, UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 24f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (heading).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
						else
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 30, UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 20f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (heading).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
					}

					if (!string.IsNullOrEmpty(redtext))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							RedText = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 80, UIScreen.MainScreen.Bounds.Width - 40, 50),
								Font = UIFont.FromName("Avenir", 16f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Lines = 2,
								Text = redtext.ToUpper(),
								TextColor = UIColor.FromRGB(255, 0, 0)
							};
							if (!UsedOffer || string.IsNullOrEmpty(usedOfferTitle))
							{
								AddSubview(RedText);
							}
						}
						else
						{
							RedText = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 80, UIScreen.MainScreen.Bounds.Width - 40, 50),
								Font = UIFont.FromName("Avenir", 16f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Lines = 2,
								Text = redtext.ToUpper(),
								TextColor = UIColor.FromRGB(255, 0, 0)
							};
							if (!UsedOffer || string.IsNullOrEmpty(usedOfferTitle))
							{
								AddSubview(RedText);
							}
						}
					}

				}
				else
				{

				if (UsedOffer && !string.IsNullOrEmpty(usedOfferTitle))
				{
					if (UIScreen.MainScreen.Bounds.Width > 320)
					{
						Title = new UILabel()
						{
							Frame = new CGRect(120, (UIScreen.MainScreen.Bounds.Width * 0.1), UIScreen.MainScreen.Bounds.Width - 40, 80),
							Font = UIFont.FromName("Avenir-Heavy", 24f),
							TextAlignment = UITextAlignment.Left,
							AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
							MinimumFontSize = 10f,
							Text = (usedOfferTitle).ToUpper(),
							LineBreakMode = UILineBreakMode.WordWrap,
							Lines = 2,
							TextColor = UIColor.FromRGB(87, 87, 87)
						};
						AddSubview(Title);
					}
					else
					{
						Title = new UILabel()
						{
							Frame = new CGRect(120, (UIScreen.MainScreen.Bounds.Width * 0.1), UIScreen.MainScreen.Bounds.Width - 40, 80),
							Font = UIFont.FromName("Avenir-Heavy", 20f),
							TextAlignment = UITextAlignment.Left,
							AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
							MinimumFontSize = 10f,
							Text = (usedOfferTitle).ToUpper(),
							LineBreakMode = UILineBreakMode.WordWrap,
							Lines = 2,
							TextColor = UIColor.FromRGB(87, 87, 87)
						};
						AddSubview(Title);
					}

				}
				else if (!string.IsNullOrEmpty(heading))
				{
					if (UIScreen.MainScreen.Bounds.Width > 320)
					{
						Title = new UILabel()
						{
							Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 15, UIScreen.MainScreen.Bounds.Width - 40, 80),
							Font = UIFont.FromName("Avenir-Heavy", 24f),
							TextAlignment = UITextAlignment.Left,
							AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
							MinimumFontSize = 10f,
							Text = (heading).ToUpper(),
							LineBreakMode = UILineBreakMode.WordWrap,
							Lines = 2,
							TextColor = UIColor.FromRGB(87, 87, 87)
						};
						AddSubview(Title);
					}
					else
					{
						Title = new UILabel()
						{
							Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10), UIScreen.MainScreen.Bounds.Width - 40, 80),
							Font = UIFont.FromName("Avenir-Heavy", 20f),
							TextAlignment = UITextAlignment.Left,
							AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
							MinimumFontSize = 10f,
							Text = (heading).ToUpper(),
							LineBreakMode = UILineBreakMode.WordWrap,
							Lines = 2,
							TextColor = UIColor.FromRGB(87, 87, 87)
						};
						AddSubview(Title);
					}
				}

				if (!string.IsNullOrEmpty(redtext))
				{
					if (UIScreen.MainScreen.Bounds.Width > 320)
					{
						RedText = new UILabel()
						{
							Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 80, UIScreen.MainScreen.Bounds.Width - 40, 50),
							Font = UIFont.FromName("Avenir", 16f),
							TextAlignment = UITextAlignment.Left,
							AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
							MinimumFontSize = 10f,
							Lines = 2,
							Text = redtext.ToUpper(),
							TextColor = UIColor.FromRGB(255, 0, 0)
						};
						if (!UsedOffer || string.IsNullOrEmpty(usedOfferTitle))
						{
							AddSubview(RedText);
						}
					}
					else
					{
						RedText = new UILabel()
						{
							Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.10) + 50, UIScreen.MainScreen.Bounds.Width - 40, 50),
							Font = UIFont.FromName("Avenir", 16f),
							TextAlignment = UITextAlignment.Left,
							AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
							MinimumFontSize = 10f,
							Lines = 2,
							Text = redtext.ToUpper(),
							TextColor = UIColor.FromRGB(255, 0, 0)
						};
						if (!UsedOffer || string.IsNullOrEmpty(usedOfferTitle))
						{
							AddSubview(RedText);
						}
					}
					}
				}
				}

				if (Controller.GetType() == typeof(SmilesInfoViewController))
				{

					TitleInfoLabel.Hidden = true;
					SecondTitleInfoLabel.Hidden = true;

					if (UsedOffer && !string.IsNullOrEmpty(usedOfferTitle))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217), UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 24f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Lines = 2,
								Text = (usedOfferTitle).ToUpper(),
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
						else
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217), UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 20f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Lines = 2,
								Text = (usedOfferTitle).ToUpper(),
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
					}
					else if (/*giftActivated &&*/ !string.IsNullOrEmpty(activatedmessagetitle))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217), UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 24f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (activatedmessagetitle).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
						else
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217), UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 20f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (activatedmessagetitle).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
					}
					else if (!string.IsNullOrEmpty(item.ProductTileTitle))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217), UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 24f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (item.ProductTileTitle).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
						else
						{
							Title = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217), UIScreen.MainScreen.Bounds.Width - 40, 80),
								Font = UIFont.FromName("Avenir-Heavy", 20f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Text = (item.ProductTileTitle).ToUpper(),
								LineBreakMode = UILineBreakMode.WordWrap,
								Lines = 2,
								TextColor = UIColor.FromRGB(87, 87, 87)
							};
							AddSubview(Title);
						}
					}

					if (!string.IsNullOrEmpty(redtext))
					{
						if (UIScreen.MainScreen.Bounds.Width > 320)
						{
							RedText = new UILabel()
							{
								Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217) + 70, UIScreen.MainScreen.Bounds.Width - 40, 50),
								Font = UIFont.FromName("Avenir", 16f),
								TextAlignment = UITextAlignment.Left,
								AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't f
								MinimumFontSize = 10f,
								Lines = 2,
								Text = redtext.ToUpper(),
								TextColor = UIColor.FromRGB(255, 0, 0)
							};
							if (!UsedOffer || string.IsNullOrEmpty(usedOfferTitle))
							{
								AddSubview(RedText);
							}
						}
							else
							{

								RedText = new UILabel()
								{
									Frame = new CGRect(20, (UIScreen.MainScreen.Bounds.Width * 0.15217) + 60, UIScreen.MainScreen.Bounds.Width - 40, 50),
									Font = UIFont.FromName("Avenir", 16f),
									TextAlignment = UITextAlignment.Left,
									AdjustsFontSizeToFitWidth = true, // gets smaller if it doesn't 
									MinimumFontSize = 10f,
									Lines = 2,
									Text = redtext.ToUpper(),
									TextColor = UIColor.FromRGB(255, 0, 0)
								};
								if (!UsedOffer || string.IsNullOrEmpty(usedOfferTitle))
								{
									AddSubview(RedText);
								}
							}
						}
					}


			}

	}
}
		