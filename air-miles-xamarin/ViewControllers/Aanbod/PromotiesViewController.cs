﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using System.Net;
using Google.Analytics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Json;
using Xamarin;
using System.Threading;
using Air_Miles_Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class PromotiesViewController : UICollectionViewController
    {
		// Network status
		NetworkStatus Internet;

		public static TransactiesOverlay TransactiesOverlay;

		// Overlays
		LoadingOverlay LoadingView;
		NetworkOverlay NetworkView;
		MaintainanceOverlay MaintainanceView;

		public Promotie[] Promoties;

		public string AanbodCategory;

		AanbodPromoties AanbodPromoties;

		public List<GiftsOfferPartner> giftsOfferPartner;
		public List<Promotie> GiftsPromoties;

		List<string> giftPartners;
		List<string> giftProducts;

		public bool PersonalGifts { get; set; }
		public bool PersonalGiftsDetails { get; set; }

		public bool FromAanbod { get; set; }

		public bool ShellSmiles { get; set;}

		public bool RedrawGifts;

		bool findable;

		bool firstload;

		//refreshlayout
		UIRefreshControl RefreshControl;
		UICollectionViewLayout flowLayout;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();


		public PromotiesViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Console.WriteLine("promotiesviewcontroller");

			DrawGiftsInCollectionView();

			NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
			UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
			{
                if (NavigationController != null)
                {
                    NavigationController.PopViewController(true);
                }
			}), true);

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				CollectionView.AddSubview(RefreshControl);
			}
			firstload = true;
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			SetAnalytics();
			//if (LoadingView != null)
			//{
			//	LoadingView.Hide();
			//}

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;

			GetShellPartnerGifts();

			CollectionView.ReloadData();

			Console.WriteLine("bool shellsmiles is "+ShellSmiles);

			// if user is not logged in go back to root
			if(!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin)){
				if (PersonalGifts == true || PersonalGiftsDetails == true)
				{
					NavigationController.PopToRootViewController(true);
				}
			}

			Console.WriteLine("redraw this view " + RedrawGifts);
			if (RedrawGifts == true)
			{
				Console.WriteLine("redrawing view");
				DrawGiftsInCollectionView();
			}
			SetAnalytics();
			Console.WriteLine("firstload is: "+firstload);
			if (!firstload)
			{
				Refresh();
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			firstload = false;
			base.ViewWillDisappear(animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();

		}

		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanbod categorie: " + Title);
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		private void DrawGiftsInCollectionView()
		{
			if (PersonalGifts && !PersonalGiftsDetails)
			{
				Console.WriteLine("loading offer promotions");
				// special offers
				giftPartners = new List<string>();
				giftProducts = new List<string>();
				giftPartners.Clear();
				giftProducts.Clear();
				GetPersonalGiftsFromPartner();

			}
			else if (!PersonalGifts && PersonalGiftsDetails)
			{
				Console.WriteLine("loading gift promotions");

				Title = "Maak je keuze";

				// not activated promotions (gifts only)
				CollectionView.Source = new PromotieCollectionViewSource(this, GetCollectionViewItems(PromotieType.Gift));
				CollectionView.Delegate = new PromotieFlowDelegate(this, GetCollectionViewItems(PromotieType.Gift), true, true);
				//CollectionView.RegisterClassForSupplementaryView(typeof(PromotionCollectionViewHeader), UICollectionElementKindSection.Header, "PersonalPromotionsCollectionViewHeader");

				if (RefreshControl != null)
				{
					RefreshControl.EndRefreshing();
				}
			}
			else
			{
				Console.WriteLine("loading marketing promotions");
				// default promotions
				CollectionView.DataSource = new PromotieCollectionViewSource(this, GetCollectionViewItems(PromotieType.MarketingPromotion));
				CollectionView.Delegate = new PromotieFlowDelegate(this, GetCollectionViewItems(PromotieType.MarketingPromotion), true, false);
				if (RefreshControl != null)
				{
					RefreshControl.EndRefreshing();
				}
			}

		}

		private class PromotieFlowDelegate : UICollectionViewDelegateFlowLayout
		{
			NetworkStatus Internet;

			UIViewController Controller;
			Promotie[] Rows;
			UIStringAttributes attr;
			nfloat TitleHeight = 150;

			bool Header, Footer;

			public PromotieFlowDelegate(UIViewController parent, Promotie[] items, bool header, bool footer) : base()
			{
				Controller = parent;
				Rows = items;
				Header = header;
				Footer = footer;
			}

			public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
			{
				//Console.WriteLine("enddate flowlayout " + Rows[indexPath.Row].EndDate);

				string title = Rows[indexPath.Row].Title;
				attr = new UIStringAttributes() { Font = UIFont.FromName("Avenir-Black", 24f) };
				nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
				nfloat ScreenPadding = 20;
				if (!string.IsNullOrEmpty(title))
				{
					TitleHeight = new NSString(title).GetSizeUsingAttributes(attr).Height;
				}
				double HeightAspectRatio = 1.0135;
				Console.WriteLine("title height " + TitleHeight);
				try
				{
					if (Rows[indexPath.Row].PromotionType == PromotieType.Offer)
					{
						if (Rows[indexPath.Row].GiftsOffer.Partner == "SHLL")
						{
							return new CGSize(ScreenWidth - (ScreenPadding * 2), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TitleHeight + 150);
						}

						else
						{
							return new CGSize(ScreenWidth - (ScreenPadding * 2), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TitleHeight + 150);
						}
					}
			
					}
				catch (Exception ex)
				{
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Promoties", "Exception: "+ ex}
                    });
					return new CGSize(ScreenWidth - (ScreenPadding * 2), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TitleHeight + 100);
				}

				return new CGSize(ScreenWidth - (ScreenPadding * 2), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TitleHeight + 100);
			}

			public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.FromRGB(250, 250, 250);
			}

			public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
			{
				// Analytics Screenname
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanbod promotie: " + Rows[indexPath.Row].Title);
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

				try
				{


				Internet = Reachability.InternetConnectionStatus();

					if (Internet != NetworkStatus.NotReachable)
					{
							// Marketing promotions
							if (Rows[indexPath.Row].PromotionType == PromotieType.MarketingPromotion)
							{
                                NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
								UIApplication.SharedApplication.OpenUrl(new NSUrl(Rows[indexPath.Row].Url + "?utm_campaign=aanbod&utm_medium=ios&utm_source=app"));
								Console.WriteLine("promotie marketing");
                                // Google analaytics
                                var GAIopenWebsite = DictionaryBuilder.CreateEvent("Aanbod", "Open website - aanbod", Rows[indexPath.Row].Title + " - " + Rows[indexPath.Row].Url, null).Build();
                                Gai.SharedInstance.DefaultTracker.Send(GAIopenWebsite);
                                Gai.SharedInstance.Dispatch();
                                
							}
							// Personalized promotions
							if (Rows[indexPath.Row].PromotionType == PromotieType.PersonalizedPromotion)
							{

								GiftViewController Redirect = App.Storyboard.InstantiateViewController("GiftViewController") as GiftViewController;

								var cell = collectionView.CellForItem(indexPath) as PromotionCollectionViewCell;

								Redirect.Parent = Controller;
								Redirect.Promotie = Rows[indexPath.Row];
								Redirect.RemovePriceView = cell.RemovePriceView;
								Redirect.RemovePropositionView = cell.RemovePropositionView;
								Controller.NavigationController.PushViewController(Redirect, true);
							}
							// Gift promotions
							if (Rows[indexPath.Row].PromotionType == PromotieType.Gift)
							{
								GiftViewController Redirect = App.Storyboard.InstantiateViewController("GiftViewController") as GiftViewController;

								Redirect.Parent = Controller;
								Redirect.Promotie = Rows[indexPath.Row];

								Controller.NavigationController.PushViewController(Redirect, true);
							}
							// Offer promotions
							if (Rows[indexPath.Row].PromotionType == PromotieType.Offer)
							{
								//hier checken shell smiles if PromotieType.Offer.Partner == shell

								// Google analaytics
								var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanbod", "Speciaal voor jou", "Ingelogd", null).Build();
								Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
								Gai.SharedInstance.Dispatch();
								

							if (Rows[indexPath.Row].GiftsOffer.Activated != null)
							{
								if (Rows[indexPath.Row].GiftsOffer.Activated == "true")
								{
									Console.WriteLine("activated true");
									if (Rows[indexPath.Row].GiftsOffer.Partner == "SHLL")
									{

										if (Rows[indexPath.Row].GiftsOffer.OfferUsed != null && Rows[indexPath.Row].GiftsOffer.OfferUsed == "true")
										{
											//if (Rows[indexPath.Row].GiftsOffer.OfferUsed == "true")
											//{
											SmilesInfoViewController RedirectSHLLS = App.Storyboard.InstantiateViewController("SmilesInfoViewController") as SmilesInfoViewController;

											var cell = collectionView.CellForItem(indexPath) as SmilesCollectionViewCell;
											RedirectSHLLS.Parent = Controller;
											RedirectSHLLS.FromPromoties = true;
											RedirectSHLLS.Promotie = Rows[indexPath.Row];
											Console.WriteLine("gebruikte SHLL promotie " + JsonConvert.SerializeObject(Rows[indexPath.Row]));
											Controller.NavigationController.PushViewController(RedirectSHLLS, true);

											//}
										}
										else
										{
											SmilesInfoViewController RedirectSHLL = App.Storyboard.InstantiateViewController("SmilesInfoViewController") as SmilesInfoViewController;
											RedirectSHLL.Parent = Controller;
											RedirectSHLL.FromPromoties = true;
											RedirectSHLL.Promotie = Rows[indexPath.Row];
											Console.WriteLine("geactiveerde SHLL promotie " + JsonConvert.SerializeObject(Rows[indexPath.Row]));
											Controller.NavigationController.PushViewController(RedirectSHLL, true);
										}
									}
									else
									{

										GiftViewController Redirect = App.Storyboard.InstantiateViewController("GiftViewController") as GiftViewController;

										Redirect.InitActivated = true;
										Redirect.Parent = Controller;
										Redirect.Promotie = Rows[indexPath.Row];
										Console.WriteLine("geactiveerde promotie " + JsonConvert.SerializeObject(Rows[indexPath.Row]));
										Controller.NavigationController.PushViewController(Redirect, true);

									}

									return;
								}
							
								//if (Rows[indexPath.Row].GiftsOffer.OfferUsed != null)
								//{
								//	if (Rows[indexPath.Row].GiftsOffer.OfferUsed == "true" && Rows[indexPath.Row].GiftsOffer.Activated == "false")
								//	{
								//		Console.WriteLine("used true");
								//		if (Rows[indexPath.Row].GiftsOffer.Partner == "SHLL")
								//		{
								//			SmilesInfoViewController RedirectSHLLS = App.Storyboard.InstantiateViewController("SmilesInfoViewController") as SmilesInfoViewController;

								//			RedirectSHLLS.Parent = Controller;
								//			RedirectSHLLS.Promotie = Rows[indexPath.Row];
								//			Console.WriteLine("gebruikte SHLL promotie " + JsonConvert.SerializeObject(Rows[indexPath.Row]));
								//			Controller.NavigationController.PushViewController(RedirectSHLLS, true);

								//			//SmilesDetailViewController Redirect = App.Storyboard.InstantiateViewController("SmilesDetailViewController") as SmilesDetailViewController;
								//			//SmilesDetailViewController.RedirectToGiftDetails(Controller.NavigationController, Rows[indexPath.Row], true);
								//			Console.WriteLine("ongeactiveerd en gebruikte info meegestuurd vanuit promotionviewcontroller " + JsonConvert.SerializeObject(Rows[indexPath.Row]));

								//			string CurrentPromotie = JsonConvert.SerializeObject(Rows[indexPath.Row]);
								//			NSUserDefaults.StandardUserDefaults.SetString(CurrentPromotie, AMLocalStorage.CurrentPromotie);
								//			Console.WriteLine("current promotie not activated " + CurrentPromotie);
								//		}
								//		else
								//		{
								//			RedirectToGiftDetails(Controller.NavigationController, Rows[indexPath.Row], true);
								//		}
								//		return;
								//	}
								//}
									if (Rows[indexPath.Row].GiftsOffer.Activated == "false")
									{
										Console.WriteLine("activated false "+ JsonConvert.SerializeObject(Rows[indexPath.Row]));

										if (Rows[indexPath.Row].GiftsOffer.Partner == "SHLL")
										{
										if (Rows[indexPath.Row].GiftsOffer.OfferUsed != null)
										{
											if (Rows[indexPath.Row].GiftsOffer.OfferUsed == "true")
											{
												SmilesInfoViewController RedirectSHLLS = App.Storyboard.InstantiateViewController("SmilesInfoViewController") as SmilesInfoViewController;

												var cell = collectionView.CellForItem(indexPath) as SmilesCollectionViewCell;

												RedirectSHLLS.Parent = Controller;
												RedirectSHLLS.FromPromoties = true;
												RedirectSHLLS.Promotie = Rows[indexPath.Row];
												Console.WriteLine("gebruikte SHLL promotie " + JsonConvert.SerializeObject(Rows[indexPath.Row]));
												Controller.NavigationController.PushViewController(RedirectSHLLS, true);

											}
											else
											{
												SmilesDetailViewController Redirect = App.Storyboard.InstantiateViewController("SmilesDetailViewController") as SmilesDetailViewController;
												Redirect.FromPromoties = true;
												SmilesDetailViewController.RedirectToGiftDetails(Controller.NavigationController, Rows[indexPath.Row], true);
												Console.WriteLine("ongeactiveerd info meegestuurd vanuit promotionviewcontroller " + JsonConvert.SerializeObject(Rows[indexPath.Row]));

												string CurrentPromotie = JsonConvert.SerializeObject(Rows[indexPath.Row]);
												NSUserDefaults.StandardUserDefaults.SetString(CurrentPromotie, AMLocalStorage.CurrentPromotie);
												Console.WriteLine("current promotie not activated " + CurrentPromotie);
											}
										}
										else
										{
											SmilesDetailViewController Redirect = App.Storyboard.InstantiateViewController("SmilesDetailViewController") as SmilesDetailViewController;
											Redirect.FromPromoties = true; 											SmilesDetailViewController.RedirectToGiftDetails(Controller.NavigationController, Rows[indexPath.Row], true); 											Console.WriteLine("ongeactiveerd info meegestuurd vanuit promotionviewcontroller " + JsonConvert.SerializeObject(Rows[indexPath.Row]));  											string CurrentPromotie = JsonConvert.SerializeObject(Rows[indexPath.Row]); 											NSUserDefaults.StandardUserDefaults.SetString(CurrentPromotie, AMLocalStorage.CurrentPromotie); 											Console.WriteLine("current promotie not activated " + CurrentPromotie);
										}
											
										}
										else
										{
											RedirectToGiftDetails(Controller.NavigationController, Rows[indexPath.Row], true);
										}
										
										return;
									}


										//if (Rows[indexPath.Row].GiftsOffer.OfferUsed == "false")
										//{
										//	Console.WriteLine("activated false");
										//	RedirectToGiftDetails(Controller.NavigationController, Rows[indexPath.Row], true);

										//	return;
										//}
									}
								
								
								else {
									Console.WriteLine("no giftoffer");
									AMAlerts.DefaultAlert(Controller, "Geen aanbiedingen", "Er zijn momenteel geen aabiedingen beschikbaar.");
								}
							}

							//else if (Rows[indexPath.Row].Url != null)
							//{
							//	UIApplication.SharedApplication.OpenUrl(new NSUrl(Rows[indexPath.Row].Url));
							//}

						

				}
				else {
					AMAlerts.DefaultAlert(Controller, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
				
					//if (Rows[indexPath.Row].Url != null)
					//{
					//	UIApplication.SharedApplication.OpenUrl(new NSUrl(Rows[indexPath.Row].Url));
					//}
					}
				catch (Exception ex)
				{
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Promoties", "Exception: "+ ex}
                    });
					Console.WriteLine("exception openen promoties "+ ex);
					Console.WriteLine("url promotie "+Rows[indexPath.Row].Url);
					//AMAlerts.DefaultAlert(Controller, "Geen aanbiedingen", "Er zijn momenteel geen aabiedingen beschikbaar.");

				}
				//if (Rows[indexPath.Row].Url != null)
				//{
				//	UIApplication.SharedApplication.OpenUrl(new NSUrl(Rows[indexPath.Row].Url));
				//}
				var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Aanbod", "Speciaal voor jou", "Ingelogd nee", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
				Gai.SharedInstance.Dispatch();

			}

			public override CGSize GetReferenceSizeForFooter(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				if (Footer)
				{
					return new CGSize(UIScreen.MainScreen.Bounds.Width, 100);
				}

				return new CGSize(0, 0);
			}

			public override CGSize GetReferenceSizeForHeader(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				if (Header)
				{
					return new CGSize(UIScreen.MainScreen.Bounds.Width, 40);
				}

				return new CGSize(0, 0);
			}
		}

		public static void SetShellSmiles(UIViewController Controller)
		{
			PromotiesViewController ctrl = Controller as PromotiesViewController;
			ctrl.ShellSmiles = true;
		}

		private static async void RedirectToGiftDetails(UINavigationController NavCtrl, Promotie Promotie, bool NotActivated)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts);
			NavCtrl.VisibleViewController.Add(LoadingView);

			List<string> ProductIds = new List<string>();

			for (int i = 0; i < Promotie.GiftsOffer.Gift.Length; i++)
			{
				GiftsOfferGift gift = Promotie.GiftsOffer.Gift[i];

				ProductIds.Add(gift.ProductID);
			}

			string JoinedProductIds = string.Join(",", ProductIds);
			if (string.IsNullOrEmpty(JoinedProductIds))
			{
				JoinedProductIds = Promotie.ProductIds;
			}
			Console.WriteLine("joinedproductids "+JoinedProductIds);

			GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(JoinedProductIds);
			List<Promotie> Rows = new List<Promotie>();

			if (GiftRoot.Gifts == null)
			{
				AMAlerts.DefaultAlert(NavCtrl.VisibleViewController, AMStrings.ErrorNoGiftsTitle, AMStrings.ErrorNoGiftsMessage);
			}
			else
			{
				Console.WriteLine("giftroot length "+GiftRoot.Gifts.Length);
				// of the offer has more than 1 gifts
				if (GiftRoot.Gifts.Length > 1)
				{
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						GiftsOfferGift[] gifts = Promotie.GiftsOffer.Gift;
						GiftsOfferGift selectedGift = null;
						Gift product = GiftRoot.Gifts[i];

						foreach (GiftsOfferGift gift in gifts)
						{
							if (gift.ProductID == GiftRoot.Gifts[i].ProductId)
							{
								selectedGift = gift;
							}
						}
						Console.WriteLine("product promotie "+ JsonConvert.SerializeObject(product));
						Console.WriteLine("Set product " + product.PartnerImageUrl + "  json " + JsonConvert.SerializeObject(selectedGift));

						// Add gift (the promotion is part of an offer)
						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							ConditionsLabel = product.ConditionsLabel,
							CheckConditionsLabel = product.CheckConditionsLabel,
							PaymentTitle = product.PaymentTitle,
							Title = AMMain.RemoveHTMLTags(product.ProductTileTitle),
							Keywords = product.Keywords,
							PaymentImage = product.PaymentImage,
							PaymentMethod = product.PaymentMethod,
							ProductType = product.ProductType,
							EndDate = product.EndDate,
							ProductId = product.ProductId,
							LinkPartner = product.LinkPartner,
							Conditions = product.Conditions,
							ProductText = product.ProductText,
							ButtonTitle = product.ButtonTitle,
							DeliverTime = product.DeliverTime,
							StartDate = product.StartDate,
							Logo = product.PartnerImageUrl,
							PaymentColor = product.PaymentColor,
							RectangleImage = product.RectangleImage,
							StoreLocator = product.StoreLocator,
							OpenCoupon = product.OpenCoupon,
							OfferNr = product.OfferNr,
							ItemNr = product.ItemNr,
							ActivatedMessage = product.ActivatedMessage,
							UsedCouponTitle = product.UsedCouponTitle,
							PrintButtonTitle = product.PrintButtonTitle,
							GiftValidUntil = product.GiftValidUntil,
							CancelButton = product.CancelButton,
							CancelButtonMobile = product.CancelButtonMobile,
							ActivatedMessageTitle = product.ActivatedMessageTitle,
							ProductTileTitle = product.ProductTileTitle,
							PreviewMessageActivated = product.PreviewMessageActivated,
							ChangeGift = product.ChangeGift,
							UsedOfferText = product.UsedOfferText,
							UsedOfferTitle = product.UsedOfferTitle,
							OpenDetails = product.OpenDetails,
							ConfirmButton = product.ConfirmButton,
							MilesOnly = product.MilesOnly,
							PropositionValue = product.PropositionValue,
							PropositionLabel = product.PropositionLabel,
							PriceValue = product.PriceValue,
							PriceLabel = product.PriceLabel,
							LogoUrl = product.LogoUrl,
							OptimizedLogoUrl = product.OptimizedLogoUrl,
							PeriodValidUntil = product.PeriodValidUntil,
							TicketSecondaryImageUrl = product.TicketSecondaryImageUrl,
							GeneralConditionsValue = product.GeneralConditionsValue,
							TicketImageUrl = product.TicketImageUrl,
							productSubType = product.ProductSubType,
							ConditionsValue = product.ConditionsValue,
							VoucherType = product.VoucherType,
							ShowLogo = product.ShowLogo,
							PartnerName = product.PartnerName,
							PartnerImageUrl = product.PartnerImageUrl,
							TicketDetailsHeaderText = product.TicketDetailsHeaderText,
							OrderedByText = product.OrderedByText,
							PricePerTicketText = product.PricePerTicketText,
							AddressText = product.AddressText,
							ValidityText = product.ValidityText,
							ConditionsText = product.ConditionsText,
							ShoppingBagsImageUrl = product.ShoppingBagsImageUrl,
							LogoImageUrl = product.LogoImageUrl,
							ShortDescription = product.ShortDescription,
							Summary = product.Summary,
							Category = product.Category,
							Validity = product.Validity,
							Address = product.Address,
							Id = product.Id,
							Type = product.Type,
							Image = product.Image,
							GiftJson = JsonConvert.SerializeObject(product),
							OfferJson = Promotie.OfferJson,
							OfferGiftJson = JsonConvert.SerializeObject(selectedGift)
						});
					}

					PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;

					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = Promotie.Title;
					Redirect.Promoties = Rows.ToArray();

					NavCtrl.PushViewController(Redirect, NotActivated);
				}
			}
			LoadingView.Hide();
		}

		private Promotie[] GetCollectionViewItems(PromotieType type)
		{

			// Define lists for the Row
			List<Promotie> Rows = new List<Promotie>();
			Rows.Clear();

			if (Promoties != null)
			{
				for (int i = 0; i < Promoties.Length; i++)
				{
					// if Rows don't exist, create them
					if (Rows == null)
					{
						Rows = new List<Promotie>();
					}

					if (Promoties[i].Keywords != null)
					{
						//JsonArray keywords = (JsonArray)Promoties[i].Keywords[0];
						Console.WriteLine("keywords != null");

						foreach (string key in Promoties[i].Keywords)
						{
							if (key == "onvindbaar")
							{
								findable = true;
								Console.WriteLine("key is onvindbaar "+ Promoties[i].Title);
							}
							else
							{
								Console.WriteLine("key != onvindbaar"+ Promoties[i].Title);
								findable = false;
							}
						}

						if (!string.IsNullOrEmpty(Promoties[i].Image) && !findable)
						{
							Console.WriteLine("url promotie "+Promoties[i].Url);
							Console.WriteLine("pronoties (get collectionviewitems) "+Promoties);
							Console.WriteLine("endatecampaign promotie "+Promoties[i].EndDateCampaign);
							Console.WriteLine("Promotie bleh "+JsonConvert.SerializeObject(Promoties[i]));

							AMMain.SetSeenGifts(Promoties[i].ProductIds);
							// Add the Table Row
							Rows.Add(new Promotie()
							{
								Title = AMMain.RemoveHTMLTags(AMMain.ReplaceBreakpoints(Promoties[i].Title.Replace("<br>", " "))),
								Image = Promoties[i].Image,
								EndDate = Promoties[i].EndDate,
								RegistrationDate = Promoties[i].RegistrationDate,
								PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
								Logo = Promoties[i].Logo,
								PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
								PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
								PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
								Url = Promoties[i].Url,
								RectangleImage = Promoties[i].RectangleImage,
								ActivatedMessage = AMMain.RemoveHTMLTags(Promoties[i].ActivatedMessage),
								ChooseGifts = Promoties[i].ChooseGifts,
								GiftValidTo = Promoties[i].GiftValidTo,
								ProductText = Promoties[i].ProductText,
								GiftJson = Promoties[i].GiftJson,
								OfferJson = Promoties[i].OfferJson,
								OfferGiftJson = Promoties[i].OfferGiftJson,
								OfferNr = Promoties[i].OfferNr,
								ItemNr = Promoties[i].ItemNr,
								PromotionType = type,
                                Used = Promoties[i].Used,
								ConditionsLabel = Promoties[i].ConditionsLabel,
								CheckConditionsLabel = Promoties[i].CheckConditionsLabel,
								PaymentTitle = Promoties[i].PaymentTitle,
								Keywords = Promoties[i].Keywords,
								PaymentImage = Promoties[i].PaymentImage,
								PaymentMethod = Promoties[i].PaymentMethod,
								ProductType = Promoties[i].ProductType,
								ProductId = Promoties[i].ProductId,
								LinkPartner = Promoties[i].LinkPartner,
								Conditions = Promoties[i].Conditions,
								ButtonTitle = Promoties[i].ButtonTitle,
								DeliverTime = Promoties[i].DeliverTime,
								StartDate = Promoties[i].StartDate,
								PaymentColor = Promoties[i].PaymentColor,
								StoreLocator = Promoties[i].StoreLocator,
								OpenCoupon = Promoties[i].OpenCoupon,
								UsedCouponTitle = Promoties[i].UsedCouponTitle,
								PrintButtonTitle = Promoties[i].PrintButtonTitle,
								GiftValidUntil = Promoties[i].GiftValidUntil,
								CancelButton = Promoties[i].CancelButton,
								CancelButtonMobile = Promoties[i].CancelButtonMobile,
								ActivatedMessageTitle = Promoties[i].ActivatedMessageTitle,
								ProductTileTitle = Promoties[i].ProductTileTitle,
								PreviewMessageActivated = Promoties[i].PreviewMessageActivated,
								ChangeGift = Promoties[i].ChangeGift,
								UsedOfferText = Promoties[i].UsedOfferText,
								UsedOfferTitle = Promoties[i].UsedOfferTitle,
								OpenDetails = Promoties[i].OpenDetails,
								ConfirmButton = Promoties[i].ConfirmButton,
								MilesOnly = Promoties[i].MilesOnly,
								PropositionValue = Promoties[i].PropositionValue,
								PropositionLabel = Promoties[i].PropositionLabel,
								PriceValue = Promoties[i].PriceValue,
								PriceLabel = Promoties[i].PriceLabel,
								LogoUrl = Promoties[i].LogoUrl,
								OptimizedLogoUrl = Promoties[i].OptimizedLogoUrl,
								PeriodValidUntil = Promoties[i].PeriodValidUntil,
								TicketSecondaryImageUrl = Promoties[i].TicketSecondaryImageUrl,
								GeneralConditionsValue = Promoties[i].GeneralConditionsValue,
								TicketImageUrl = Promoties[i].TicketImageUrl,
								productSubType = Promoties[i].productSubType,
								ConditionsValue = Promoties[i].ConditionsValue,
								VoucherType = Promoties[i].VoucherType,
								ShowLogo = Promoties[i].ShowLogo,
								PartnerName = Promoties[i].PartnerName,
								PartnerImageUrl = Promoties[i].PartnerImageUrl,
								TicketDetailsHeaderText = Promoties[i].TicketDetailsHeaderText,
								OrderedByText = Promoties[i].OrderedByText,
								PricePerTicketText = Promoties[i].PricePerTicketText,
								AddressText = Promoties[i].AddressText,
								ValidityText = Promoties[i].ValidityText,
								ConditionsText = Promoties[i].ConditionsText,
								ShoppingBagsImageUrl = Promoties[i].ShoppingBagsImageUrl,
								LogoImageUrl = Promoties[i].LogoImageUrl,
								ShortDescription = Promoties[i].ShortDescription,
								Summary = Promoties[i].Summary,
								Category = Promoties[i].Category,
								Validity = Promoties[i].Validity,
								Address = Promoties[i].Address,
								Id = Promoties[i].Id,
								Type = Promoties[i].Type,
								//Segue = "PromotieDetailSegue",
							});
							findable = false;
						}
					}else
					{
						if (!string.IsNullOrEmpty(Promoties[i].Image))
						{
							Console.WriteLine("url promotie " + Promoties[i].Url);
							Console.WriteLine("endatecampaign promotie " + Promoties[i].EndDateCampaign);
							Console.WriteLine("Promotie bla " + JsonConvert.SerializeObject(Promoties[i]));

							AMMain.SetSeenGifts(Promoties[i].ProductIds);
							// Add the Table Row
							Rows.Add(new Promotie()
							{
								Title = AMMain.RemoveHTMLTags(AMMain.ReplaceBreakpoints(Promoties[i].Title.Replace("<br>", " "))),
								Image = Promoties[i].Image,
								EndDate = Promoties[i].EndDate,
								RegistrationDate = Promoties[i].RegistrationDate,
								PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
								Logo = Promoties[i].Logo,
								PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
								PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
								PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
								Url = Promoties[i].Url,
								RectangleImage = Promoties[i].RectangleImage,
								ActivatedMessage = AMMain.RemoveHTMLTags(Promoties[i].ActivatedMessage),
								ChooseGifts = Promoties[i].ChooseGifts,
								GiftValidTo = Promoties[i].GiftValidTo,
								ProductText = Promoties[i].ProductText,
								GiftJson = Promoties[i].GiftJson,
								OfferJson = Promoties[i].OfferJson,
								OfferGiftJson = Promoties[i].OfferGiftJson,
								OfferNr = Promoties[i].OfferNr,
								ItemNr = Promoties[i].ItemNr,
								PromotionType = type,
                                Used = Promoties[i].Used,
								ConditionsLabel = Promoties[i].ConditionsLabel,
								CheckConditionsLabel = Promoties[i].CheckConditionsLabel,
								PaymentTitle = Promoties[i].PaymentTitle,
								Keywords = Promoties[i].Keywords,
								PaymentImage = Promoties[i].PaymentImage,
								PaymentMethod = Promoties[i].PaymentMethod,
								ProductType = Promoties[i].ProductType,
								ProductId = Promoties[i].ProductId,
								LinkPartner = Promoties[i].LinkPartner,
								Conditions = Promoties[i].Conditions,
								ButtonTitle = Promoties[i].ButtonTitle,
								DeliverTime = Promoties[i].DeliverTime,
								StartDate = Promoties[i].StartDate,
								PaymentColor = Promoties[i].PaymentColor,
								StoreLocator = Promoties[i].StoreLocator,
								OpenCoupon = Promoties[i].OpenCoupon,
								UsedCouponTitle = Promoties[i].UsedCouponTitle,
								PrintButtonTitle = Promoties[i].PrintButtonTitle,
								GiftValidUntil = Promoties[i].GiftValidUntil,
								CancelButton = Promoties[i].CancelButton,
								CancelButtonMobile = Promoties[i].CancelButtonMobile,
								ActivatedMessageTitle = Promoties[i].ActivatedMessageTitle,
								ProductTileTitle = Promoties[i].ProductTileTitle,
								PreviewMessageActivated = Promoties[i].PreviewMessageActivated,
								ChangeGift = Promoties[i].ChangeGift,
								UsedOfferText = Promoties[i].UsedOfferText,
								UsedOfferTitle = Promoties[i].UsedOfferTitle,
								OpenDetails = Promoties[i].OpenDetails,
								ConfirmButton = Promoties[i].ConfirmButton,
								MilesOnly = Promoties[i].MilesOnly,
								PropositionValue = Promoties[i].PropositionValue,
								PropositionLabel = Promoties[i].PropositionLabel,
								PriceValue = Promoties[i].PriceValue,
								PriceLabel = Promoties[i].PriceLabel,
								LogoUrl = Promoties[i].LogoUrl,
								OptimizedLogoUrl = Promoties[i].OptimizedLogoUrl,
								PeriodValidUntil = Promoties[i].PeriodValidUntil,
								TicketSecondaryImageUrl = Promoties[i].TicketSecondaryImageUrl,
								GeneralConditionsValue = Promoties[i].GeneralConditionsValue,
								TicketImageUrl = Promoties[i].TicketImageUrl,
								productSubType = Promoties[i].productSubType,
								ConditionsValue = Promoties[i].ConditionsValue,
								VoucherType = Promoties[i].VoucherType,
								ShowLogo = Promoties[i].ShowLogo,
								PartnerName = Promoties[i].PartnerName,
								PartnerImageUrl = Promoties[i].PartnerImageUrl,
								TicketDetailsHeaderText = Promoties[i].TicketDetailsHeaderText,
								OrderedByText = Promoties[i].OrderedByText,
								PricePerTicketText = Promoties[i].PricePerTicketText,
								AddressText = Promoties[i].AddressText,
								ValidityText = Promoties[i].ValidityText,
								ConditionsText = Promoties[i].ConditionsText,
								ShoppingBagsImageUrl = Promoties[i].ShoppingBagsImageUrl,
								LogoImageUrl = Promoties[i].LogoImageUrl,
								ShortDescription = Promoties[i].ShortDescription,
								Summary = Promoties[i].Summary,
								Category = Promoties[i].Category,
								Validity = Promoties[i].Validity,
								Address = Promoties[i].Address,
								Id = Promoties[i].Id,
								Type = Promoties[i].Type,
								//Segue = "PromotieDetailSegue",
							});
						}
					}
				}

				// Return the Rows as an arra
				return Rows.ToArray();

			}

			return Rows.ToArray();
		}

		private Promotie[] ReloadCollectionViewItems(Promotie[] Promoties)
		{
			
			// Define lists for the Row
			List<Promotie> Rows = new List<Promotie>();
			Rows.Clear();

			if (Promoties != null)
			{
				for (int i = 0; i < Promoties.Length; i++)
				{
					// if Rows don't exist, create them
					if (Rows == null)
					{
						Rows = new List<Promotie>();
					}

					if (Promoties[i].Keywords != null)
					{
						string giftJson = "";
						string offerJson = "";
						string offerGiftJson = "";

						giftJson = Promoties[i].GiftJson;
						offerGiftJson = Promoties[i].OfferGiftJson;

						offerJson = Promoties[i].OfferJson;

						//JsonArray keywords = (JsonArray)Promoties[i].Keywords[0];
						Console.WriteLine("keywords != null");
						Console.WriteLine("product id promotie "+Promoties[i].Id);


						foreach (string key in Promoties[i].Keywords)
						{
							if (key == "onvindbaar")
							{
								findable = true;
								Console.WriteLine("key is onvindbaar " + Promoties[i].Title);
							}
							else
							{
								Console.WriteLine("key != onvindbaar" + Promoties[i].Title);
								findable = false;
							}
						}

						if (!string.IsNullOrEmpty(Promoties[i].Image) && !findable)
						{
							Console.WriteLine("logo promoties "+Promoties[i].Logo);

							// Add the Table Row
							Rows.Add(new Promotie()
							{
								Title = AMMain.RemoveHTMLTags(AMMain.ReplaceBreakpoints(Promoties[i].Title.Replace("<br>", " "))),
								Image = Promoties[i].Image,
								EndDate = Promoties[i].EndDate,
								RegistrationDate = Promoties[i].RegistrationDate,
								PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
								Logo = Promoties[i].Logo,
								PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
								PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
								PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
								Url = Promoties[i].Url,
								RectangleImage = Promoties[i].RectangleImage,
								ActivatedMessage = AMMain.RemoveHTMLTags(Promoties[i].ActivatedMessage),
								ChooseGifts = Promoties[i].ChooseGifts,
								GiftValidTo = Promoties[i].GiftValidTo,
								ProductText = Promoties[i].ProductText,
								GiftJson = Promoties[i].GiftJson,
								OfferJson = Promoties[i].OfferJson,
								OfferGiftJson = Promoties[i].OfferGiftJson,
								OfferNr = Promoties[i].OfferNr,
								ItemNr = Promoties[i].ItemNr,
                                Used = Promoties[i].Used,
								PromotionType = PromotieType.Gift,
								ConditionsLabel = Promoties[i].ConditionsLabel,
								CheckConditionsLabel = Promoties[i].CheckConditionsLabel,
								PaymentTitle = Promoties[i].PaymentTitle,
								Keywords = Promoties[i].Keywords,
								PaymentImage = Promoties[i].PaymentImage,
								PaymentMethod = Promoties[i].PaymentMethod,
								ProductType = Promoties[i].ProductType,
								ProductId = Promoties[i].ProductId,
								LinkPartner = Promoties[i].LinkPartner,
								Conditions = Promoties[i].Conditions,
								ButtonTitle = Promoties[i].ButtonTitle,
								DeliverTime = Promoties[i].DeliverTime,
								StartDate = Promoties[i].StartDate,
								PaymentColor = Promoties[i].PaymentColor,
								StoreLocator = Promoties[i].StoreLocator,
								OpenCoupon = Promoties[i].OpenCoupon,
								UsedCouponTitle = Promoties[i].UsedCouponTitle,
								PrintButtonTitle = Promoties[i].PrintButtonTitle,
								GiftValidUntil = Promoties[i].GiftValidUntil,
								CancelButton = Promoties[i].CancelButton,
								CancelButtonMobile = Promoties[i].CancelButtonMobile,
								ActivatedMessageTitle = Promoties[i].ActivatedMessageTitle,
								ProductTileTitle = Promoties[i].ProductTileTitle,
								PreviewMessageActivated = Promoties[i].PreviewMessageActivated,
								ChangeGift = Promoties[i].ChangeGift,
								UsedOfferText = Promoties[i].UsedOfferText,
								UsedOfferTitle = Promoties[i].UsedOfferTitle,
								OpenDetails = Promoties[i].OpenDetails,
								ConfirmButton = Promoties[i].ConfirmButton,
								MilesOnly = Promoties[i].MilesOnly,
								PropositionValue = Promoties[i].PropositionValue,
								PropositionLabel = Promoties[i].PropositionLabel,
								PriceValue = Promoties[i].PriceValue,
								PriceLabel = Promoties[i].PriceLabel,
								LogoUrl = Promoties[i].LogoUrl,
								OptimizedLogoUrl = Promoties[i].OptimizedLogoUrl,
								PeriodValidUntil = Promoties[i].PeriodValidUntil,
								TicketSecondaryImageUrl = Promoties[i].TicketSecondaryImageUrl,
								GeneralConditionsValue = Promoties[i].GeneralConditionsValue,
								TicketImageUrl = Promoties[i].TicketImageUrl,
								productSubType = Promoties[i].productSubType,
								ConditionsValue = Promoties[i].ConditionsValue,
								VoucherType = Promoties[i].VoucherType,
								ShowLogo = Promoties[i].ShowLogo,
								PartnerName = Promoties[i].PartnerName,
								PartnerImageUrl = Promoties[i].PartnerImageUrl,
								TicketDetailsHeaderText = Promoties[i].TicketDetailsHeaderText,
								OrderedByText = Promoties[i].OrderedByText,
								PricePerTicketText = Promoties[i].PricePerTicketText,
								AddressText = Promoties[i].AddressText,
								ValidityText = Promoties[i].ValidityText,
								ConditionsText = Promoties[i].ConditionsText,
								ShoppingBagsImageUrl = Promoties[i].ShoppingBagsImageUrl,
								LogoImageUrl = Promoties[i].LogoImageUrl,
								ShortDescription = Promoties[i].ShortDescription,
								Summary = Promoties[i].Summary,
								Category = Promoties[i].Category,
								Validity = Promoties[i].Validity,
								Address = Promoties[i].Address,
								Id = Promoties[i].Id,
								Type = Promoties[i].Type,
								//Segue = "PromotieDetailSegue",
							});
							findable = false;
						}
					}
					else
					{
						if (!string.IsNullOrEmpty(Promoties[i].Image))
						{
							// Add the Table Row
							Rows.Add(new Promotie()
							{
								Title = AMMain.RemoveHTMLTags(AMMain.ReplaceBreakpoints(Promoties[i].Title.Replace("<br>", " "))),
								Image = Promoties[i].Image,
								EndDate = Promoties[i].EndDate,
								RegistrationDate = Promoties[i].RegistrationDate,
								PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
								Logo = Promoties[i].Logo,
								PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
								PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
								PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
								Url = Promoties[i].Url,
								RectangleImage = Promoties[i].RectangleImage,
								ActivatedMessage = AMMain.RemoveHTMLTags(Promoties[i].ActivatedMessage),
								ChooseGifts = Promoties[i].ChooseGifts,
								GiftValidTo = Promoties[i].GiftValidTo,
								ProductText = Promoties[i].ProductText,
								GiftJson = Promoties[i].GiftJson,
								OfferJson = Promoties[i].OfferJson,
                                Used = Promoties[i].Used,
								OfferGiftJson = Promoties[i].OfferGiftJson,
								OfferNr = Promoties[i].OfferNr,
								ItemNr = Promoties[i].ItemNr,
								PromotionType = PromotieType.Gift,
								ConditionsLabel = Promoties[i].ConditionsLabel,
								CheckConditionsLabel = Promoties[i].CheckConditionsLabel,
								PaymentTitle = Promoties[i].PaymentTitle,
								Keywords = Promoties[i].Keywords,
								PaymentImage = Promoties[i].PaymentImage,
								PaymentMethod = Promoties[i].PaymentMethod,
								ProductType = Promoties[i].ProductType,
								ProductId = Promoties[i].ProductId,
								LinkPartner = Promoties[i].LinkPartner,
								Conditions = Promoties[i].Conditions,
								ButtonTitle = Promoties[i].ButtonTitle,
								DeliverTime = Promoties[i].DeliverTime,
								StartDate = Promoties[i].StartDate,
								PaymentColor = Promoties[i].PaymentColor,
								StoreLocator = Promoties[i].StoreLocator,
								OpenCoupon = Promoties[i].OpenCoupon,
								UsedCouponTitle = Promoties[i].UsedCouponTitle,
								PrintButtonTitle = Promoties[i].PrintButtonTitle,
								GiftValidUntil = Promoties[i].GiftValidUntil,
								CancelButton = Promoties[i].CancelButton,
								CancelButtonMobile = Promoties[i].CancelButtonMobile,
								ActivatedMessageTitle = Promoties[i].ActivatedMessageTitle,
								ProductTileTitle = Promoties[i].ProductTileTitle,
								PreviewMessageActivated = Promoties[i].PreviewMessageActivated,
								ChangeGift = Promoties[i].ChangeGift,
								UsedOfferText = Promoties[i].UsedOfferText,
								UsedOfferTitle = Promoties[i].UsedOfferTitle,
								OpenDetails = Promoties[i].OpenDetails,
								ConfirmButton = Promoties[i].ConfirmButton,
								MilesOnly = Promoties[i].MilesOnly,
								PropositionValue = Promoties[i].PropositionValue,
								PropositionLabel = Promoties[i].PropositionLabel,
								PriceValue = Promoties[i].PriceValue,
								PriceLabel = Promoties[i].PriceLabel,
								LogoUrl = Promoties[i].LogoUrl,
								OptimizedLogoUrl = Promoties[i].OptimizedLogoUrl,
								PeriodValidUntil = Promoties[i].PeriodValidUntil,
								TicketSecondaryImageUrl = Promoties[i].TicketSecondaryImageUrl,
								GeneralConditionsValue = Promoties[i].GeneralConditionsValue,
								TicketImageUrl = Promoties[i].TicketImageUrl,
								productSubType = Promoties[i].productSubType,
								ConditionsValue = Promoties[i].ConditionsValue,
								VoucherType = Promoties[i].VoucherType,
								ShowLogo = Promoties[i].ShowLogo,
								PartnerName = Promoties[i].PartnerName,
								PartnerImageUrl = Promoties[i].PartnerImageUrl,
								TicketDetailsHeaderText = Promoties[i].TicketDetailsHeaderText,
								OrderedByText = Promoties[i].OrderedByText,
								PricePerTicketText = Promoties[i].PricePerTicketText,
								AddressText = Promoties[i].AddressText,
								ValidityText = Promoties[i].ValidityText,
								ConditionsText = Promoties[i].ConditionsText,
								ShoppingBagsImageUrl = Promoties[i].ShoppingBagsImageUrl,
								LogoImageUrl = Promoties[i].LogoImageUrl,
								ShortDescription = Promoties[i].ShortDescription,
								Summary = Promoties[i].Summary,
								Category = Promoties[i].Category,
								Validity = Promoties[i].Validity,
								Address = Promoties[i].Address,
								Id = Promoties[i].Id,
								Type = Promoties[i].Type,
								//Segue = "PromotieDetailSegue",
							});
						}
					}
				}
			

				// Return the Rows as an arra
				return Rows.ToArray();

			}

			return Rows.ToArray();

		}


		public void SetPromotions(Promotie[] promoties)
		{
			Promoties = promoties;
		}

		public void GetPersonalGiftsFromPartner()
		{

			// personal offers from locals storage
			string JsonGift = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);

			Console.WriteLine("jsongift "+JsonGift);
			if (!string.IsNullOrEmpty(JsonGift))
			{
				GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(JsonGift);
				Console.WriteLine("Gifts convert "+JsonConvert.SerializeObject(Gifts.Response));
				Console.WriteLine("gifts response length "+ Gifts.Response.Length);
				if (Gifts.Response.Length > 0)
				{
					for (int i = 0; i < Gifts.Response.Length; i++)
					{
						// Rempve tab badge
						AMMain.ClearAanbodTabBadge();

						bool productNotChosen = true;
						GiftsOffer offer = Gifts.Response[i].Offer[0];

						// if gift is activated
						if (offer.Activated == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);

                                    }
									// change bool
									productNotChosen = false;
								}
							}

						}
						// if gift is activated
						else if (offer.OfferUsed == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);
									}
									// change bool
									productNotChosen = false;
								}
							}

						}
						if (/*productNotChosen &&*/ !giftPartners.Contains(offer.Partner))
						{
							Console.WriteLine("add " + offer.Partner + " to list");

							// add the partnername to the list
							giftPartners.Add(offer.Partner);
						}
					}
				}

				// personal promotions from local storage
				string JsonPersonalizedPromotions = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions);

				if (!string.IsNullOrEmpty(JsonPersonalizedPromotions))
				{
					Console.WriteLine("personalized promotions "+JsonPersonalizedPromotions);
					MendixPersonalizedPromotionRoot PersonalizedPromotions = JsonConvert.DeserializeObject<MendixPersonalizedPromotionRoot>(JsonPersonalizedPromotions);
					Console.WriteLine("PersonalizedPromotionRoot "+ JsonConvert.SerializeObject(PersonalizedPromotions));
					Console.WriteLine("personalizedpromotions errorcode "+PersonalizedPromotions.Errors[0].Code);
					if (PersonalizedPromotions.Errors[0].Code != "E00" && PersonalizedPromotions.Response.Length == 0)
					{
						// view WITHOUT personalized gifts
						GeneratePersonalGiftView(Gifts.Response, null);
					}
					else {
						// view WITH personalized gifts
						GeneratePersonalGiftView(Gifts.Response, PersonalizedPromotions);
					}
				}
			}
		}

		public async void GeneratePersonalGiftView(GiftsResponse[] Offers, MendixPersonalizedPromotionRoot PersonalizedPromotionsRoot)
		{
			SetLoader(AMStrings.LoadingGifts);
			if (GiftsPromoties != null)
			{
				GiftsPromoties.Clear();
			}
			if (GiftsPromoties == null)
			{
				GiftsPromoties = new List<Promotie>();
			}

			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Persoonlijk aanbod");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			// create a dictionary where the hippo-returns are stored. based on their partnername
			var GiftOfferPartnerDictionary = new Dictionary<string, object>();

			//var GiftsDictionary = new Dictionary<string, GiftRoot>();

			string productIds = null;

			// create another dictionay where the counters for the images are stored
			var CounterDictionary = new Dictionary<string, int>();

			Console.WriteLine("found partners " + giftPartners.Count.ToString());
			Console.WriteLine("giftpartners "+ JsonConvert.SerializeObject(giftPartners));
			Console.WriteLine("giftproducts "+ JsonConvert.SerializeObject(giftProducts));

			bool offerUsed = false;

			if (giftPartners.Count > 0)
			{
				// Set partner gifts
				foreach (string partner in giftPartners)
				{
                    if (partner != null)
                    {
                        try
                        {
                            GiftsOfferPartner PartnerGifts = await GiftsService.GetGiftsFromPartner(partner);

                            if (PartnerGifts != null)
                            {
                                // save the data in the dictionary
                                Console.WriteLine("save partner in dictionary " + partner);
                                GiftOfferPartnerDictionary.Add(partner, PartnerGifts);
                                // set counter at 0
                                CounterDictionary.Add(partner, 0);
                            }

                            else
                            {
                                Console.WriteLine($"no partner git found in {partner}");
                            }
                        }
                        catch (Exception ex)
                        {
                            Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                                {"Promoties", "Exception: "+ ex}
                            });
                            Console.WriteLine("Exception promotions "+ex);
                        }
                    }
				}
			}

			if (giftProducts.Count > 0)
			{
				productIds = string.Join(",", giftProducts);
				GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(productIds);

				Console.WriteLine($"productIds: {productIds}");
                Console.WriteLine("GiftRoot "+JsonConvert.SerializeObject(GiftRoot));
                if (GiftRoot.Gifts == null)
				{
					Console.WriteLine("geen data");
				}
				else
				{
						Console.WriteLine("Giftroot.Gifts.Lenght "+ GiftRoot.Gifts.Length);
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						Gift product = GiftRoot.Gifts[i];
						GiftOfferPartnerDictionary.Add(product.ProductId, product);
					}
				}
			}

			// again loop over all offers and create the offer-objects with the hippo data attached
				Console.WriteLine("Offers.Length " + Offers.Length);
			for (int i = 0; i < Offers.Length; i++)
			{
				string partnerName = Offers[i].Offer[0].Partner;
                bool showLogo = false;
				string img = "";
				string logo = "";
				string title = "";
				string giftJson = "";
				string offerGiftJson = "";
				string usedOfferTitle = "";
				string usedOfferText = "";
				string RectangleImage = "";
				string Partner = "";
				string ProductValidUntil = "";
				string ProductId = "";
				string endDate = "";
				string registrationDate = "";
				string priceTitle = "";
				string priceDetail = "";
				string propositionTitle = "";
				string propositionDetail = "";
				string url = "";
				string activatedMessage = "";
				string chooseGifts = "";
				string giftValidTo = "";
				string productText = "";
				string conditionsLabel = "";
				string checkConditionsLabel = "";
				string paymentTitle = "";
				string paymentImage = "";
				string paymentMethod = "";
				string productType = "";
				string linkPartner = "";
				string conditions = "";
				string buttonTitle = "";
				string deliverTime = "";
				string startDate = "";
				string paymentColor = "";
				string storeLocator = "";
				string openCoupon = "";
				string usedCouponTitle = "";
				string printButtonTitle = "";
				string giftValidUntil = "";
				string cancelButton = "";
				string cancelButtonMobile = "";
				string activatedMessageTitle = "";
				string productTileTitle = "";
				string previewMessageActivated = "";
				string changeGift = "";
				string openDetails = "";
				string confirmButton = "";
				bool milesOnly = false;
				string propositionValue = "";
				string propositionLabel = "";
				string priceValue = "";
				string priceLabel = "";
				string logoUrl = "";
				string optimizedLogoUrl = "";
				string periodValidUntil = "";
				string ticketSecondaryImageUrl = "";
				string generalConditionsValue = "";
				string ticketImageUrl = "";
				string productSubType = "";
				string conditionsValue = "";
				string voucherType = "";
				string partnerImageUrl = "";
				string ticketDetailsHeaderText = "";
				string orderedByText = "";
				string pricePerTicketText = "";
				string addressText = "";
				string validityText = "";
				string conditionsText = "";
				string shoppingBagsImageUrl = "";
				string logoImageUrl = "";
				string shortDescription = "";
				string summary = "";
				string category = "";
				string validity = "";
				string address = "";
				string id = "";
				string type = "";
				string itemNr = "";
				string offerNr = "";
					
				//offer to add
				GiftsOffer thisOffer = Offers[i].Offer[0];
				string offerJson = JsonConvert.SerializeObject(thisOffer);

				AMMain.SetSeenGifts(thisOffer.OfferNr);

				Gift productDetails = null;


				//if an offer is activated find the selected gift
				//if (thisOffer.Activated == "true")
				//{
				Console.WriteLine("activated offers "+thisOffer.Gift.Length);
				Console.WriteLine("offerjson proddet "+ JsonConvert.SerializeObject(thisOffer));
					for (int z = 0; z < thisOffer.Gift.Length; z++)
					{
						GiftsOfferGift Gift = thisOffer.Gift[z];
						Console.WriteLine("");
						if (Gift.GiftChosen == "true")
						{
							if (GiftOfferPartnerDictionary.ContainsKey(Gift.ProductID))
							{
								Console.WriteLine("product Id offer: "+Gift.ProductID);
								productDetails = (Gift)GiftOfferPartnerDictionary[Gift.ProductID];

								Console.WriteLine("Gift usedOfferTitle "+productDetails.UsedOfferTitle);
								Console.WriteLine("giftjson proddet "+JsonConvert.SerializeObject(productDetails));
								Console.WriteLine("offergiftjson proddet "+JsonConvert.SerializeObject(Gift));

								// set the image and logo
								img = productDetails.Image;
								logo = productDetails.PartnerImageUrl;
								showLogo = productDetails.ShowLogo;
								RectangleImage = productDetails.RectangleImage;
								usedOfferTitle = productDetails.UsedCouponTitle;
								usedOfferText = productDetails.UsedOfferText;
								Partner = productDetails.PartnerName;
								ProductId = Gift.ProductID;
								ProductValidUntil = productDetails.GiftValidUntil;
								endDate = productDetails.EndDate;
								registrationDate = productDetails.RegistrationDate;
								priceTitle = productDetails.PriceTitle;
								priceDetail = productDetails.PriceDetail;
								propositionTitle = productDetails.PropositionTitle;
								propositionDetail = productDetails.PropositionDetail;
								url = productDetails.Url;
								activatedMessage = productDetails.ActivatedMessage;
								chooseGifts = productDetails.ChooseGifts;
								giftValidTo = productDetails.GiftValidUntil;
								productText = productDetails.ProductText;
								conditionsLabel = productDetails.ConditionsLabel;
								checkConditionsLabel = productDetails.CheckConditionsLabel;
								paymentTitle = productDetails.PaymentTitle;
								paymentImage = productDetails.PaymentImage;
								paymentMethod = productDetails.PaymentMethod;
								productType = productDetails.ProductType;
								linkPartner = productDetails.LinkPartner;
								conditions = productDetails.Conditions;
								buttonTitle = productDetails.ButtonTitle;
								deliverTime = productDetails.DeliverTime;
								startDate = productDetails.StartDate;
								paymentColor = productDetails.PaymentColor;
								storeLocator = productDetails.StoreLocator;
								openCoupon = productDetails.OpenCoupon;
								usedCouponTitle = productDetails.UsedCouponTitle;
								printButtonTitle = productDetails.PrintButtonTitle;
								giftValidUntil = productDetails.GiftValidUntil;
								cancelButton = productDetails.CancelButton;
								cancelButtonMobile = productDetails.CancelButtonMobile;
								activatedMessageTitle = productDetails.ActivatedMessageTitle;
								productTileTitle = productDetails.ProductTileTitle;
								previewMessageActivated = productDetails.PreviewMessageActivated;
								changeGift = productDetails.ChangeGift;
								openDetails = productDetails.OpenDetails;
								confirmButton = productDetails.ConfirmButton;
								milesOnly = productDetails.MilesOnly;
								propositionValue = productDetails.PropositionValue;
								propositionLabel = productDetails.PropositionLabel;
								priceValue = productDetails.PriceValue;
								priceLabel = productDetails.PriceLabel;
								logoUrl = productDetails.LogoUrl;
								optimizedLogoUrl = productDetails.OptimizedLogoUrl;
								periodValidUntil = productDetails.PeriodValidUntil;
								ticketSecondaryImageUrl = productDetails.TicketSecondaryImageUrl;
								generalConditionsValue = productDetails.GeneralConditionsValue;
								ticketImageUrl = productDetails.TicketImageUrl;
								productSubType = productDetails.ProductSubType;
								conditionsValue = productDetails.ConditionsValue;
								voucherType = productDetails.VoucherType;
								partnerImageUrl = productDetails.PartnerImageUrl;
								ticketDetailsHeaderText = productDetails.TicketDetailsHeaderText;
								orderedByText = productDetails.OrderedByText;
								pricePerTicketText = productDetails.PricePerTicketText;
								addressText = productDetails.AddressText;
								validityText = productDetails.ValidityText;
								conditionsText = productDetails.ConditionsText;
								shoppingBagsImageUrl = productDetails.ShoppingBagsImageUrl;
								logoImageUrl = productDetails.LogoImageUrl;
								shortDescription = productDetails.ShortDescription;
								summary = productDetails.Summary;
								category = productDetails.Category;
								validity = productDetails.Validity;
								address = productDetails.Address;
								id = productDetails.Id;
								type = productDetails.Type;
								itemNr = productDetails.ItemNr;
								offerNr = productDetails.OfferNr;

								// set the json for the offer objects
							if (!string.IsNullOrEmpty(productDetails.PeriodValidUntil))
								{
									ProductValidUntil = productDetails.PeriodValidUntil;
								}
								giftJson = JsonConvert.SerializeObject(productDetails);
								offerGiftJson = JsonConvert.SerializeObject(Gift);

								// if offer is used
								if (thisOffer.OfferUsed == "true")
								{
									title = productDetails.UsedOfferTitle;

								}
								if (thisOffer.Activated == "true")
								{
									title = productDetails.ActivatedMessageTitle;
								}
								else
								{
									title = productDetails.ActivatedMessageTitle;
								}

							}
						}

					}
                //}
                // if offer is used
                if (thisOffer.OfferUsed == "true")
                {
                    offerUsed = true;
                    if (productDetails != null && productDetails.UsedCouponTitle != null) 
                    { 
                        title = productDetails.UsedCouponTitle;
                    }

				}
				else
				{
					offerUsed = false;
				}

				// if the img is not set with a chosen product
				if (img == "")
				{

					Console.WriteLine("thisoffer "+ JsonConvert.SerializeObject(thisOffer));
					//Console.WriteLine("product id giftsoffer "+ thisOffer.Gift[i].ProductID);
					//hippo data to add to the offer, from the dictionary
					Console.WriteLine("GiftOfferPartnerDictionary " + JsonConvert.SerializeObject(GiftOfferPartnerDictionary));

					if (partnerName != null && GiftOfferPartnerDictionary != null && GiftOfferPartnerDictionary.ContainsKey(partnerName))
                    {
                        try
                        {
                            GiftsOfferPartner partnerDetails = (GiftsOfferPartner)GiftOfferPartnerDictionary[partnerName];

                            if (partnerDetails != null)
                            {
                                string[] images = partnerDetails.PreviewImages;
                                int counter = CounterDictionary[partnerName];
                                int max = images.Length - 1;
                                img = images[counter];
                                Console.WriteLine("counter images gift " + counter);

                                if (counter >= max)
                                    CounterDictionary[partnerName] = 0;
                                else
                                    CounterDictionary[partnerName] = counter + 1;
                                title = partnerDetails.PreviewTitle;
                                logo = partnerDetails.PartnerProgramLogo;
                                showLogo = true;
                            }
						}
						catch (Exception ex)
						{
                            Console.WriteLine("exception GiftOfferPartnerDictionary "+ex);
                        }
                    }
				}

				if (GiftsPromoties == null)
				{
					GiftsPromoties = new List<Promotie>();
				}

                if (thisOffer.Gift.Length > 0)
                {
                    Console.WriteLine("produtc Id lalala " + thisOffer.Gift[0].ProductID);
                    Console.WriteLine("generalconditionsvalue " + generalConditionsValue);
                    // Add offer
                    GiftsPromoties.Add(new Promotie()
                    {
                        PromotionType = PromotieType.Offer,
                        Title = AMMain.RemoveHTMLTags(title),
                        Id = thisOffer.Gift[0].ProductID,
                        Image = img,
                        RectangleImage = RectangleImage,
                        Logo = logo,
                        ShowLogo = showLogo,
                        GiftsOffer = thisOffer,
                        ProductIds = productIds,
                        GiftJson = giftJson,
                        OfferJson = offerJson,
                        OfferGiftJson = offerGiftJson,
                        UsedOfferTitle = usedOfferTitle,
                        UsedOfferText = usedOfferText,
                        OfferUsed = offerUsed,
                        PartnerName = Partner,
                        ItemNr = itemNr,
                        OfferNr = offerNr,
                        EndDate = endDate,
                        RegistrationDate = registrationDate,
                        PriceTitle = AMMain.RemoveHTMLTags(priceTitle),
                        PriceDetail = AMMain.RemoveHTMLTags(priceDetail),
                        PropositionTitle = AMMain.RemoveHTMLTags(propositionTitle),
                        PropositionDetail = AMMain.RemoveHTMLTags(propositionDetail),
                        Url = url,
                        ActivatedMessage = AMMain.RemoveHTMLTags(activatedMessage),
                        ChooseGifts = chooseGifts,
                        GiftValidTo = giftValidTo,
                        ProductText = productText,
                        ConditionsLabel = conditionsLabel,
                        CheckConditionsLabel = checkConditionsLabel,
                        PaymentTitle = paymentTitle,
                        PaymentImage = paymentImage,
                        PaymentMethod = paymentMethod,
                        ProductType = productType,
                        ProductId = thisOffer.Gift[0].ProductID,
                        LinkPartner = linkPartner,
                        Conditions = conditions,
                        ButtonTitle = buttonTitle,
                        DeliverTime = deliverTime,
                        StartDate = startDate,
                        PaymentColor = paymentColor,
                        StoreLocator = storeLocator,
                        OpenCoupon = openCoupon,
                        UsedCouponTitle = usedCouponTitle,
                        PrintButtonTitle = printButtonTitle,
                        GiftValidUntil = giftValidUntil,
                        CancelButton = cancelButton,
                        CancelButtonMobile = cancelButtonMobile,
                        ActivatedMessageTitle = activatedMessageTitle,
                        ProductTileTitle = productTileTitle,
                        PreviewMessageActivated = previewMessageActivated,
                        ChangeGift = changeGift,
                        OpenDetails = openDetails,
                        ConfirmButton = confirmButton,
                        MilesOnly = milesOnly,
                        PropositionValue = propositionValue,
                        PropositionLabel = propositionLabel,
                        PriceValue = priceValue,
                        PriceLabel = priceLabel,
                        LogoUrl = logoUrl,
                        OptimizedLogoUrl = optimizedLogoUrl,
                        PeriodValidUntil = periodValidUntil,
                        TicketSecondaryImageUrl = ticketSecondaryImageUrl,
                        GeneralConditionsValue = generalConditionsValue,
                        TicketImageUrl = ticketImageUrl,
                        productSubType = productSubType,
                        ConditionsValue = conditionsValue,
                        VoucherType = voucherType,
                        PartnerImageUrl = partnerImageUrl,
                        TicketDetailsHeaderText = ticketDetailsHeaderText,
                        OrderedByText = orderedByText,
                        PricePerTicketText = pricePerTicketText,
                        AddressText = addressText,
                        ValidityText = validityText,
                        ConditionsText = conditionsText,
                        ShoppingBagsImageUrl = shoppingBagsImageUrl,
                        LogoImageUrl = logoImageUrl,
                        ShortDescription = shortDescription,
                        Summary = summary,
                        Category = category,
                        Validity = validity,
                        Address = address,
                        Type = type,
                        //Segue = "PromotieDetailSegue",
                    });
                }

			}
			/// <summary>
			/// PERSONALIZED PROMOTIONS
			/// </summary>
			/// 

			if (PersonalizedPromotionsRoot != null)
			{
				if (PersonalizedPromotionsRoot.Errors[0].Code == "E00" && PersonalizedPromotionsRoot.Response.Length > 0)
				{
					List<string> giftPersonalizedProductsIds = new List<string>();

					// create a dictionary where the hippo-returns are stored. based on their partnername
					var PersonalizedPromotionDictionary = new Dictionary<string, object>();


					Console.WriteLine("personlizedpromotions.Response.Length is "+PersonalizedPromotionsRoot.Response.Length);
					Console.WriteLine("personlizedpromotions.Response is " +JsonConvert.SerializeObject(PersonalizedPromotionsRoot.Response));

					for (int i = 0; i < PersonalizedPromotionsRoot.Response.Length; i++)
					{
						string productId = "";

						// check if type is campain
						if (!string.IsNullOrEmpty(PersonalizedPromotionsRoot.Response[i].CampaignID))
						{
							productId = PersonalizedPromotionsRoot.Response[i].CampaignID;
							giftPersonalizedProductsIds.Add(productId);
							PersonalizedPromotionDictionary.Add(productId, PersonalizedPromotionsRoot.Response[i]); 
						}
						// check if type is product
						else if (!string.IsNullOrEmpty(PersonalizedPromotionsRoot.Response[i].ProductID))
						{
							productId = PersonalizedPromotionsRoot.Response[i].ProductID;
							giftPersonalizedProductsIds.Add(productId);
							PersonalizedPromotionDictionary.Add(productId, PersonalizedPromotionsRoot.Response[i]);
						}
						AMMain.SetSeenGifts(productId);
					}

					string JoinedProductIds = string.Join(",", giftPersonalizedProductsIds);
					Console.WriteLine("JoinedProductIds "+JoinedProductIds);
					Console.WriteLine("PersonalizedPromotionDictionary "+JsonConvert.SerializeObject(PersonalizedPromotionDictionary));

					try
					{

					PersonalizedPromotionsService PersonalizedPromotionsService = new PersonalizedPromotionsService();
					PersonalizedPromotionsRoot PersonalPromotions = await PersonalizedPromotionsService.GetPersonalizedPromotionsByIds(JoinedProductIds);

					if (PersonalPromotions != null && PersonalPromotions.Products.Length > 0)
					{
							Console.WriteLine("PersonalPromotions.Products.Length "+PersonalPromotions.Products.Length);
							Console.WriteLine("personalizedpromotions " + JsonConvert.SerializeObject(PersonalPromotions));
						for (int i = 0; i < PersonalPromotions.Products.Length; i++)
						{
							PersonalizedPromotion item = PersonalPromotions.Products[i];

							string Activation = "false";
							string ProductId = "";

							// if personalized promotion is set
							if (item.ActivateCompound != null)
							{
								Activation = item.ActivateCompound.Activated.ToString().ToLower();
								Console.WriteLine("Activation is set to " + Activation);
							}

							// check if type is campain or product
							if (!string.IsNullOrEmpty(item.CampaignId))
							{
								ProductId = item.CampaignId;
							}
							else if (!string.IsNullOrEmpty(item.ProductId))
							{
								ProductId = item.ProductId;
							}
						
							AMMain.SetSeenGifts(ProductId);

							Console.WriteLine("personalizedpromotion json "+JsonConvert.SerializeObject(item));
							Console.WriteLine("title from list :" + JsonConvert.SerializeObject(item.Title));
							Console.WriteLine("product id personal prom "+ProductId);

							MendixPersonalizedPromotionsResponse MendixPersonalPromotion = (MendixPersonalizedPromotionsResponse)PersonalizedPromotionDictionary[ProductId];
								Console.WriteLine("MendixPersonalPromotion.Used " + MendixPersonalPromotion.Used);

							GiftsPromoties.Add(new Promotie()
							{
								PromotionType = PromotieType.PersonalizedPromotion,
								Title = item.Title,
								Id = ProductId,
								Image = item.Image,
								Logo = item.LogoUrl,
								GiftsOffer = null,
								GiftActivated = Activation,
								GiftJson = "{}",
								OfferJson = "{}",
								OfferGiftJson = "{}",
								EnrolledMember = MendixPersonalPromotion.EnrolledMember,
								EndDateCampaign = MendixPersonalPromotion.EndDateCampaign,
								PersonalizedPromotionJson = JsonConvert.SerializeObject(item),
								MilesOnly = item.MilesOnly,
								PriceTitle = item.PropositionValue,
								PriceDetail = item.PropositionLabel,
								PropositionTitle = item.PriceValue,
								PropositionDetail = item.PriceLabel,
								RectangleImage = item.RectangleImage,
								ShowLogo = item.ShowLogo,
                                Used = MendixPersonalPromotion.Used,
								UsedOfferTitle = item.UsedOfferTitle,
								UsedOfferText = item.UsedOfferText,
								OfferUsed = item.OfferUsed,
								PartnerName = item.PartnerName,
								ItemNr = item.ItemNr,
								OfferNr = item.OfferNr,
								EndDate = MendixPersonalPromotion.EndDateCampaign,
								RegistrationDate = MendixPersonalPromotion.RegistrationDateTo,
								Url = item.Url,
								ActivatedMessage = AMMain.RemoveHTMLTags(item.ActivatedMessage),
								ChooseGifts = item.ChooseGifts,
								GiftValidTo = item.GiftValidUntil,
								ProductText = item.ProductText,
								ConditionsLabel = item.ConditionsLabel,
								CheckConditionsLabel = item.CheckConditionsLabel,
								PaymentTitle = item.PaymentTitle,
								PaymentImage = item.PaymentImage,
								PaymentMethod = item.PaymentMethod,
								ProductType = item.ProductType,
								ProductId = item.ProductId,
								LinkPartner = item.LinkPartner,
								Conditions = item.Conditions,
								ButtonTitle = item.ButtonTitle,
								DeliverTime = item.DeliverTime,
								StartDate = item.StartDate,
								PaymentColor = item.PaymentColor,
								StoreLocator = item.StoreLocator,
								OpenCoupon = item.OpenCoupon,
								UsedCouponTitle = item.UsedCouponTitle,
								PrintButtonTitle = item.PrintButtonTitle,
								GiftValidUntil = item.GiftValidUntil,
								CancelButton = item.CancelButton,
								CancelButtonMobile = item.CancelButtonMobile,
								ActivatedMessageTitle = item.ActivatedMessageTitle,
								ProductTileTitle = item.ProductTileTitle,
								PreviewMessageActivated = item.PreviewMessageActivated,
								ChangeGift = item.ChangeGift,
								OpenDetails = item.OpenDetails,
								ConfirmButton = item.ConfirmButton,
								PropositionValue = item.PropositionValue,
								PropositionLabel = item.PropositionLabel,
								PriceValue = item.PriceValue,
								PriceLabel = item.PriceLabel,
								LogoUrl = item.LogoUrl,
								OptimizedLogoUrl = item.OptimizedLogoUrl,
								PeriodValidUntil = item.PeriodValidUntil,
								TicketSecondaryImageUrl = item.TicketSecondaryImageUrl,
								GeneralConditionsValue = item.GeneralConditionsValue,
								TicketImageUrl = item.TicketImageUrl,
								ConditionsValue = item.ConditionsValue,
								VoucherType = item.VoucherType,
								PartnerImageUrl = item.PartnerImageUrl,
								TicketDetailsHeaderText = item.TicketDetailsHeaderText,
								OrderedByText = item.OrderedByText,
								PricePerTicketText = item.PricePerTicketText,
								AddressText = item.AddressText,
								ValidityText = item.ValidityText,
								ConditionsText = item.ConditionsText,
								ShoppingBagsImageUrl = item.ShoppingBagsImageUrl,
								LogoImageUrl = item.LogoImageUrl,
								ShortDescription = item.ShortDescription,
								Summary = item.Summary,
								Category = item.Category,
								Validity = item.Validity,
								Address = item.Address,
								Type = item.Type,
							});

						}
					}
						}
					catch (Exception ex)
					{
                        Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                            {"Promoties", "Exception: "+ ex}
                        });
						Console.WriteLine("exception personalized promotions "+ex);
						if (LoadingView != null)
						{
							LoadingView.Hide();
						}
					}
				}

			}
			if(GiftsPromoties== null || GiftsPromoties.Count==0)			
			{
				if (LoadingView != null)
				{
					LoadingView.Hide();

				}
				SetBackupView("Op dit moment zijn er geen persoonlijke acties beschikbaar. Houd deze pagina goed in de gaten!");
			}
			else
			{
				Console.WriteLine("giftspromoties to array " + JsonConvert.SerializeObject(GiftsPromoties.ToArray()));
				if (LoadingView != null)
				{
					LoadingView.Hide();
					LoadingView.RemoveFromSuperview();
				}


                CollectionView.Source = new PromotieCollectionViewSource(this, GiftsPromoties.ToArray());
                CollectionView.Source = new PromotieCollectionViewSource(this, GiftsPromoties.ToArray());
                CollectionView.Source = new PromotieCollectionViewSource(this, GiftsPromoties.ToArray());
				CollectionView.Source = new PromotieCollectionViewSource(this, GiftsPromoties.ToArray());
				CollectionView.Delegate = new PromotieFlowDelegate(this, GiftsPromoties.ToArray(), true, false);
				//CollectionView.RegisterClassForSupplementaryView(typeof(PromotionCollectionViewHeader), UICollectionElementKindSection.Header, "PersonalPromotionsCollectionViewHeader");
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
				LoadingView.RemoveFromSuperview();
			}

		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		void Refresh()
		{
			//RedrawGifts = true;
			Console.WriteLine("refreshing man");
			DrawGiftsInCollectionView();
			CollectionView.ReloadData();
			var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Aanbod categorie", "Pull to refresh", "Pull to refresh", null).Build();
			Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
			Gai.SharedInstance.Dispatch();

			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		public void SetBackupView(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			TransactiesOverlay = new TransactiesOverlay(bounds, text);
			View.Add(TransactiesOverlay);
		}


		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			View.Add(MaintainanceView);
		}

		public static async void GetShellPartnerGifts()
		{
			GiftsOfferPartner PartnerGifts = await GiftsService.GetGiftsFromPartner("SHLL");
		}
	  	
    }
}