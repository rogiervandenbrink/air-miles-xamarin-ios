﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class PromotiesViewController : UICollectionViewController
    {
		static readonly Promotie[] Categorien = new[] {
			new Promotie 
				Title = "Uitgelicht",
			},
			new Promotie 
				Title = "Shop & Tank",
			},
		};

        public PromotiesViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			CollectionView.Source = new DefaultCollectionViewSource(this, GetTableItems());
			//CollectionView.Delegate = new AanbodFlowDelegate(this, GetTableItems());
		}

		private TableRow[] GetCollectionViewItems()
		{

			// Define lists for the Row
			List<TableRow> Rows = new List<TableRow>();

			for (int i = 0; i < Categorien.Length; i++)
			{
				// if Rows don't exist, create them
				if (Rows == null)
				{
					Rows = new List<TableRow>();
				}

				// Add the Table Row
				Rows.Add(new TableRow()
				{
					Index = i,
					Title = Categorien[i].Title,
					Detail = "Aanbod laden...",
					Icon = GetRowIcon(Categorien[i].Icon),
					Segue = "AanbodDetailSegue",
					CellIdentifier = CellIdentifier.IconDetailCollectionViewCell
				});
			}

			// Return the Rows as an arra
			return Rows.ToArray();

		}
    }
}