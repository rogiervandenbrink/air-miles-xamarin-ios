﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using Google.Analytics;
using CoreGraphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading.Tasks;

namespace airmilesxamarin
{
    public partial class SmilesDetailViewController : UICollectionViewController
	{
		// Network status
		NetworkStatus Internet;

		public Promotie Promotie { get; set; }
		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;

		public Promotie[] Promoties;
		public static Gift Gift;
		public static GiftsOffer Offer;
		public static GiftsOfferGift OfferGift;
		public static PersonalizedPromotion PersonalizedPromotion;

		public List<GiftsOfferPartner> giftsOfferPartner;
		public List<Promotie> GiftsPromoties;

		List<string> giftPartners;
		List<string> giftProducts;

		public bool PersonalGifts { get; set; }
		public bool PersonalGiftsDetails { get; set; }

		public static bool RedrawGifts;
		public static bool Activated;

		bool findable;
		public bool FromPromoties { get; set; }

		//refreshlayout
		UIRefreshControl RefreshControl;

		public SmilesDetailViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Console.WriteLine("smilesdetailvc");
			DrawGiftsInCollectionView();
			//this.AutomaticallyAdjustsScrollViewInsets = false;

			if (!FromPromoties)
			{
                //if (NavigationController != null)
                HideBackButton();
				NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
				UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
				{
					//NavigationController.PopViewController(true);
					PoppingViewControllers();

				}), true);
				//}
			}
			else
			{
				NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
					UIImage.FromBundle("IconBack.png"), UIBarButtonItemStyle.Plain, (sender, args) =>
					{
						NavigationController.PopViewController(true);


					}), true);
			}
				

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				CollectionView.AddSubview(RefreshControl);

			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			FromPromoties = false;
			SetAnalytics();
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		public void PoppingViewControllers()
		{
			foreach (UIViewController view in NavigationController.ViewControllers)
			{
				if (view.GetType() == typeof(PromotiesViewController))
				{
					NavigationController.PopToViewController(view, false);
				}
			}

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			SetLoader(AMStrings.LoadingGifts);
			// if user is not logged in go back to root
			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				if (PersonalGifts == true || PersonalGiftsDetails == true)
				{
					NavigationController.PopToRootViewController(true);
				}
			}

			Console.WriteLine("redraw this view " + RedrawGifts);
			Console.WriteLine("Promotie vanuit promotiesvc " + JsonConvert.SerializeObject(Promoties));

			if (RedrawGifts == true)
			{
				Console.WriteLine("redrawing view");
				DrawGiftsInCollectionView();
			}
			SetAnalytics();

			//Gift = JsonConvert.DeserializeObject<Gift>(Promotie.GiftJson);
			//Offer = JsonConvert.DeserializeObject<GiftsOffer>(Promotie.OfferJson);

			//foreach (GiftsOfferGift gift in Offer.Gift)
			//{
			//	if (gift.ProductID == Gift.ProductId)
			//	{
			//		OfferGift = gift;
			//	}
			//}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
            if (LoadingView != null)
            {
                LoadingView.Hide();
            }
		}


		void SetAnalytics()
		{
			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Miles&Me aanbod: keuzes");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public void DrawGiftsInCollectionView()
		{
			if (PersonalGifts && !PersonalGiftsDetails)
			{
				Console.WriteLine("loading offer promotions");
				// special offers
				giftPartners = new List<string>();
				giftProducts = new List<string>();
				GetPersonalGiftsFromPartner();

			}
			else if (!PersonalGifts && PersonalGiftsDetails)
			{
				Console.WriteLine("loading gift promotions");

				Title = "Maak je keuze";
				// not activated promotions (gifts only)
				CollectionView.DataSource = new SmilesCollectionViewSource(this, GetCollectionViewItems(PromotieType.Gift));
				CollectionView.Delegate = new SmilesFlowDelegate(this, GetCollectionViewItems(PromotieType.Gift), true, true);
				//CollectionView.RegisterClassForSupplementaryView(typeof(SmilesCollectionViewHeader), UICollectionElementKindSection.Header, "SmilesCollectionViewHeader");

				if (RefreshControl != null)
				{
					RefreshControl.EndRefreshing();
				}
			}
			else
			{
				Console.WriteLine("loading marketing promotions");
				// default promotions
				CollectionView.Source = new PromotieCollectionViewSource(this, GetCollectionViewItems(PromotieType.MarketingPromotion));
				CollectionView.Delegate = new SmilesFlowDelegate(this, GetCollectionViewItems(PromotieType.MarketingPromotion), true, false);
				if (RefreshControl != null)
				{
					RefreshControl.EndRefreshing();
				}
			}

		}

        private void HideBackButton()
        {
            NavigationItem.SetHidesBackButton(true, false);
        }

		private class SmilesFlowDelegate : UICollectionViewDelegateFlowLayout
		{
			NetworkStatus Internet;

			// Overlays
			LoadingOverlay LoadingView;
			MaintainanceOverlay MaintainanceView;
			NetworkOverlay NetworkView;

			UIViewController Controller;
			Promotie[] Rows;

			bool Header, Footer;

			public SmilesFlowDelegate(UIViewController parent, Promotie[] items, bool header, bool footer) : base()
			{
				Controller = parent;
				Rows = items;
				Header = header;
				Footer = footer;
			}

			public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
			{
				nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
				nfloat ScreenPadding = 20;
				nfloat TitleHeight = 140;

				double HeightAspectRatio = 1.2;

				return new CGSize(ScreenWidth - (ScreenPadding * 2), ((ScreenWidth - (ScreenPadding * 2)) * HeightAspectRatio) + TitleHeight);
			}

			public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.FromRGB(250, 250, 250);
			}

			public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
			{


			}

			public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
			{
				return false;
			}

			public override CGSize GetReferenceSizeForFooter(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				if (Footer)
				{
					return new CGSize(UIScreen.MainScreen.Bounds.Width, 0);
				}

				return new CGSize(0, 0);
			}

			public override CGSize GetReferenceSizeForHeader(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
			{
				if (Header)
				{
					if (UIScreen.MainScreen.Bounds.Width > 320)
					{
						return new CGSize(UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Width * 0.15217 + 110);
						//return new CGSize(UIScreen.MainScreen.Bounds.Width, 180);
					}else
					{
						return new CGSize(UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Width * 0.10 + 110);
						//return new CGSize(UIScreen.MainScreen.Bounds.Width, 160);
					}
				}

				return new CGSize(0, 0);
			}
		}

		public static async void RedirectToGiftDetails(UINavigationController NavCtrl, Promotie Promotie, bool NotActivated)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			//string promotie = JsonConvert.SerializeObject(Promotie);
			//string productIDS = JToken.Parse(Promotie.GiftsOffer.Gift[i]).Value<string>("ProductId");

			Console.WriteLine("promotie uit redirecttogiftdetails " + Promotie);
			//Console.WriteLine("productids shell " + productIDS);

			LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts);
			NavCtrl.VisibleViewController.Add(LoadingView);
			string productIDS = "";
			//if (string.IsNullOrEmpty(productIDS))
			//{

			List<string> ProductIds = new List<string>();

			for (int i = 0; i < Promotie.GiftsOffer.Gift.Length; i++)
			{
				GiftsOfferGift gift = Promotie.GiftsOffer.Gift[i];

				ProductIds.Add(gift.ProductID);
			}

			productIDS = string.Join(",", ProductIds);
			//}
			//Console.WriteLine("joined productids shell "+JoinedProductIds)
			string responseEdit = "";
			string response = "";
			string title = "";
			string rectangleImage = "";
			string logo = "";
			bool showLogo = false;
			string productText = "";
			string confirmButton = "";
			string cancelButton = "";
			string giftJson = "";
			string offerJson = "";
			string offerGiftJson = "";
			bool notActivated;
			string activatedMessageTitle = "";
			string activatedMessage = "";
			string validity = "";
			string productTileTitle = "";
			string itemNr = "";
			string conditionsValue = "";
			string conditionsLabel = "";
			string openDetails = "";


			GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(productIDS);
			List<Promotie> Rows = new List<Promotie>();

			if (GiftRoot.Gifts != null)
			{
				response = JsonConvert.SerializeObject(GiftRoot);
				responseEdit = response.Replace("[]", "");


			}

			if (GiftRoot.Gifts == null || string.IsNullOrEmpty(responseEdit))
			{
				AMAlerts.DefaultAlert(NavCtrl.VisibleViewController, AMStrings.ErrorNoGiftsTitle, AMStrings.ErrorNoGiftsMessage);
			}
			else
			{
				Console.WriteLine("response gifts shell " + JsonConvert.SerializeObject(GiftRoot));
				JObject ShellGiftsObject = JObject.Parse(response);
				Console.WriteLine("response shellgiftsobject " + response);
				// of the offer has more than 1 gifts
				if (GiftRoot.Gifts.Length > 1)
				{
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						GiftsOfferGift[] gifts = Promotie.GiftsOffer.Gift;
						GiftsOfferGift selectedGift = null;
						Gift product = GiftRoot.Gifts[i];

						foreach (GiftsOfferGift gift in gifts)
						{
							if (gift.ProductID == GiftRoot.Gifts[i].ProductId)
							{
								selectedGift = gift;
							}
						}

						Console.WriteLine("Offerjson smilesdetail redirecttogiftdetails " + Promotie.OfferJson);
						Console.WriteLine("OfferGiftjson smilesdetail redirecttogiftdetails " + JsonConvert.SerializeObject(selectedGift));
						Console.WriteLine("Set product " + product.PartnerImageUrl + "  json " + JsonConvert.SerializeObject(selectedGift));
						Console.WriteLine("shell title " + product.ProductTileTitle + " producttext " + product.ProductText + " buttonconfirm " + product.ConfirmButton);
						Console.WriteLine("shell used title " + (string)ShellGiftsObject["productId"][i]["usedOfferTitle"]);
						Console.WriteLine("shell used? " + Promotie.OfferUsed);
						Console.WriteLine("shelldetailvc Promotie " + JsonConvert.SerializeObject(Promotie));
						Console.WriteLine("shelldetail selectedgift " + JsonConvert.SerializeObject(selectedGift));


						// Add gift (the promotion is part of an offer)
						Rows.Add(new Promotie()
						{
							PromotionType = PromotieType.Gift,
							Title = (string)ShellGiftsObject["productId"][i]["productTileTitle"],
							Image = (string)ShellGiftsObject["productId"][i]["rectangleImage"],
							Logo = (string)ShellGiftsObject["productId"][i]["logo"],
							ShowLogo = (bool)ShellGiftsObject["productId"][i]["showLogo"],
							ProductText = (string)ShellGiftsObject["productId"][i]["productText"],
							ConfirmButton = (string)ShellGiftsObject["productId"][i]["ConfirmButton"],
							CancelButton = (string)ShellGiftsObject["productId"][i]["CancelButton"],
							GiftJson = JsonConvert.SerializeObject(product),
							OfferJson = Promotie.OfferJson,
							OfferGiftJson = JsonConvert.SerializeObject(Promotie.GiftsOffer),
							NotActivated = NotActivated,
							ItemNr = selectedGift.ItemNr,
							RectangleImage = (string)ShellGiftsObject["productId"][i]["rectangleImage"],
							ActivatedMessageTitle = (string)ShellGiftsObject["productId"][i]["activatedMessageTitle"],
							ActivatedMessage = (string)ShellGiftsObject["productId"][i]["activatedMessage"],
							Validity = (string)ShellGiftsObject["productId"][i]["validity"],
							ProductTileTitle = (string)ShellGiftsObject["productId"][i]["productTileTitle"],
							UsedOfferTitle = (string)ShellGiftsObject["productId"][i]["usedOfferTitle"],
							UsedOfferText = (string)ShellGiftsObject["productId"][i]["usedOfferText"],
							OfferUsed = Promotie.OfferUsed,
							ConditionsValue = (string)ShellGiftsObject["productId"][i]["conditionsValue"],
							ConditionsLabel = (string)ShellGiftsObject["productId"][i]["conditionsLabel"],
							OpenDetails = (string)ShellGiftsObject["productId"][i]["openDetails"],
						});
					}

					SmilesDetailViewController Redirect = App.Storyboard.InstantiateViewController("SmilesDetailViewController") as SmilesDetailViewController;

					//NavCtrl.PopViewController(false);
					Console.WriteLine("promotie.Title " + Promotie.Title);
					Console.WriteLine("redirect.Promities " + JsonConvert.SerializeObject(Rows.ToArray()));
					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = Promotie.Title;
					Redirect.Promoties = Rows.ToArray();
					Redirect.DrawGiftsInCollectionView();
					NavCtrl.PushViewController(Redirect, false);
				}
				if (GiftRoot.Gifts.Length == 1)
				{
					GiftsOfferGift[] gifts = Promotie.GiftsOffer.Gift;
					GiftsOfferGift selectedGift = null;
					Gift product = GiftRoot.Gifts[0];
					// Add gift (the promotion is part of an offer)
                    foreach (GiftsOfferGift gift in gifts)
                    {
                        if (gift.ProductID == GiftRoot.Gifts[0].ProductId)
                        {
                            selectedGift = gift;
                        }
                    }

					Console.WriteLine("shell used title " + (string)ShellGiftsObject["productId"][0]["usedOfferTitle"]);
					Console.WriteLine("shell used? " + Promotie.OfferUsed);
					Console.WriteLine("shell detail Promotie "+JsonConvert.SerializeObject(Promotie));
					Console.WriteLine("shelldetail selectedgift "+JsonConvert.SerializeObject(selectedGift));
					Console.WriteLine("response shellgiftsobject " + response);
                    Console.WriteLine("shell promotie "+ JsonConvert.SerializeObject(Promotie));

                    Rows.Add(new Promotie()
					{
						PromotionType = PromotieType.Gift,
						Title = (string)ShellGiftsObject["productId"][0]["productTileTitle"],
						Image = (string)ShellGiftsObject["productId"][0]["rectangleImage"],
						Logo = (string)ShellGiftsObject["productId"][0]["logo"],
						ShowLogo = (bool)ShellGiftsObject["productId"][0]["showLogo"],
						ProductText = (string)ShellGiftsObject["productId"][0]["productText"],
						ConfirmButton = (string)ShellGiftsObject["productId"][0]["ConfirmButton"],
						CancelButton = (string)ShellGiftsObject["productId"][0]["CancelButton"],
						GiftJson = JsonConvert.SerializeObject(product),
						OfferJson = Promotie.OfferJson,
						ItemNr = selectedGift.ItemNr,
						OfferGiftJson = JsonConvert.SerializeObject(Promotie.GiftsOffer),
						NotActivated = NotActivated,
						RectangleImage = (string)ShellGiftsObject["productId"][0]["rectangleImage"],
						ActivatedMessageTitle = (string)ShellGiftsObject["productId"][0]["activatedMessageTitle"],
						ActivatedMessage = (string)ShellGiftsObject["productId"][0]["activatedMessage"],
						Validity = (string)ShellGiftsObject["productId"][0]["validity"],
						ProductTileTitle = (string)ShellGiftsObject["productId"][0]["productTileTitle"],
						UsedOfferTitle = (string)ShellGiftsObject["productId"][0]["usedOfferTitle"],
						UsedOfferText = (string)ShellGiftsObject["productId"][0]["usedOfferText"],
						OfferUsed = Promotie.OfferUsed,
						ConditionsValue = (string)ShellGiftsObject["productId"][0]["conditionsValue"],
						ConditionsLabel = (string)ShellGiftsObject["productId"][0]["conditionsLabel"],
						OpenDetails = (string)ShellGiftsObject["productId"][0]["openDetails"],
					});

					SmilesDetailViewController Redirect = App.Storyboard.InstantiateViewController("SmilesDetailViewController") as SmilesDetailViewController;

					//NavCtrl.PopViewController(false);
					Console.WriteLine("promotie.Title " + Promotie.Title);
					Console.WriteLine("redirect.Promities " + JsonConvert.SerializeObject(Rows.ToArray()));
					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = Promotie.Title;
					Redirect.Promoties = Rows.ToArray();
					Redirect.DrawGiftsInCollectionView();
					NavCtrl.PushViewController(Redirect, false);
				}

			}

			LoadingView.Hide();
		}

		public static async void RedirectToGiftDetailsString(UINavigationController NavCtrl, string promotie, bool NotActivated) 		{ 			CGRect bounds = UIScreen.MainScreen.Bounds;  			LoadingOverlay LoadingView = new LoadingOverlay(bounds, AMStrings.LoadingGifts); 			NavCtrl.VisibleViewController.Add(LoadingView);

			// personal offers from locals storage
			string JsonGift = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts); 			Console.WriteLine("promotie detailstring jsongift "+JsonGift);
			Console.WriteLine("promotie detailstring " + promotie);

			var GiftOfferDictionary = new Dictionary<string, object>();

			if (!string.IsNullOrEmpty(JsonGift))
			{
				GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(JsonGift);
				Console.WriteLine("Gifts convert " + JsonConvert.SerializeObject(Gifts.Response));
				Console.WriteLine("gifts response length " + Gifts.Response.Length);
				if (Gifts.Response.Length > 0)
				{
					for (int i = 0; i < Gifts.Response.Length; i++)
					{
						
						GiftsOffer offer = Gifts.Response[i].Offer[0];

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];
								string identifier = offer.OfferNr + " " + gift.ItemNr; 
								if (!GiftOfferDictionary.ContainsKey(identifier))
								{

									GiftOfferDictionary.Add(identifier, gift);
								}

							}


					}
				}
			}
 			string responseEdit = ""; 			string response = ""; 			string title = ""; 			string rectangleImage = ""; 			string logo = ""; 			bool showLogo = false; 			string productText = ""; 			string confirmButton = ""; 			string cancelButton = ""; 			string giftJson = ""; 			string offerJson = ""; 			string offerGiftJson = ""; 			bool notActivated; 			string activatedMessageTitle = ""; 			string activatedMessage = ""; 			string validity = ""; 			string productTileTitle = ""; 			string offernr = ""; 			string itemnr = "";
			string checkConditionsLabel = "";
			string usedCouponTitle = "";
			string printButtonTitle = "";
			string giftValidUntil = "";
			string cancelButtonMobile = "";
			string previewMessageActivated = "";
			string changeGift = "";
			string usedOfferText = "";
			string usedOfferTitle = "";
			string openDetails = "";
			string logoUrl = "";
			string optimizedLogoUrl = "";
			string periodValidUntil = "";
			string generalConditionsValue = "";
			string conditionsText = "";
			string conditionsValue = "";


			//GiftsDictionary.Add(JToken.Parse(promotie).Value<string>("OfferNr"), 
 			GiftsOffer GiftPromotie = JsonConvert.DeserializeObject<GiftsOffer>(promotie); 			Console.WriteLine("giftpromotie smilesdetail " + JsonConvert.SerializeObject(GiftPromotie)); 			List<string> ProductIds = new List<string>();  			for (int i = 0; i < GiftPromotie.Gift.Length; i++) 			{ 				GiftsOfferGift gift = GiftPromotie.Gift[i];  				ProductIds.Add(gift.ProductID); 			}  			string productIDS = string.Join(",", ProductIds);  			GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(productIDS); 			List<Promotie> Rows = new List<Promotie>();  			if (GiftRoot.Gifts != null) 			{ 				response = JsonConvert.SerializeObject(GiftRoot); 				responseEdit = response.Replace("[]", "");  			}  			if (GiftRoot.Gifts == null || string.IsNullOrEmpty(responseEdit)) 			{ 				AMAlerts.DefaultAlert(NavCtrl.VisibleViewController, AMStrings.ErrorNoGiftsTitle, AMStrings.ErrorNoGiftsMessage); 			} 			else 			{
				GiftsOfferGift productDetails = null; 				Console.WriteLine("response gifts shell " + JsonConvert.SerializeObject(GiftRoot)); 				JObject ShellGiftsObject = JObject.Parse(response); 				// of the offer has more than 1 gifts 				if (GiftRoot.Gifts.Length > 1) 				{ 					for (int i = 0; i < GiftRoot.Gifts.Length; i++) 					{ 						GiftsOfferGift[] gifts = GiftPromotie.Gift; 						GiftsOfferGift selectedGift = null; 						Gift product = GiftRoot.Gifts[i];  						foreach (GiftsOfferGift gift in gifts) 						{ 							if (gift.ProductID == GiftRoot.Gifts[i].ProductId) 							{ 								selectedGift = gift; 							} 						}
						bool activated = false; 						if (GiftOfferDictionary.ContainsKey((JToken.Parse(promotie).Value<string>("OfferNr"))+" "+ selectedGift.ItemNr))
						{
							Console.WriteLine("product Id offer: " + (string)ShellGiftsObject["productId"][i]["productId"]);
							productDetails = (GiftsOfferGift)GiftOfferDictionary[((JToken.Parse(promotie).Value<string>("OfferNr")) + " " + selectedGift.ItemNr)];
							if (productDetails.GiftChosen == "true")
							{
								activated = true;
							}
							else
							{
								activated = false;
							}
							Console.WriteLine("productDetils detailvc "+JsonConvert.SerializeObject(productDetails));
						} 						Console.WriteLine("detailvc title "+(string)ShellGiftsObject["productId"][i]["productTileTitle"]+ " activated "+activated); 						Console.WriteLine("Set product " + product.PartnerImageUrl + "  json " + JsonConvert.SerializeObject(selectedGift));
						Console.WriteLine("ItemNr smilesdetailvc "+ JsonConvert.SerializeObject(selectedGift.ItemNr)); 						Console.WriteLine("shell title " + product.ProductTileTitle + " producttext " + product.ProductText + " buttonconfirm " + product.ConfirmButton); 						Console.WriteLine("shell giftstring " + promotie);
						Console.WriteLine("product offer used "+product.OfferUsed);
						Console.WriteLine("shellgiftsobject "+ JsonConvert.SerializeObject(ShellGiftsObject)); 						Console.WriteLine("shell product. "+ JsonConvert.SerializeObject(product)); 						Console.WriteLine("offernr " + JToken.Parse(promotie).Value<string>("OfferNr")); 						Console.WriteLine("shellgiftsobject " + JsonConvert.SerializeObject(ShellGiftsObject)); 						Console.WriteLine("offergiftjson smilesdetail " + JsonConvert.SerializeObject(selectedGift)); 						Console.WriteLine("selectedgift giftchosen " + JsonConvert.SerializeObject(selectedGift.GiftChosen));
						Console.WriteLine("shell previewmessageactivated " + JsonConvert.SerializeObject(product.PreviewMessageActivated));
						Console.WriteLine("shellobject previewmessageactivated "+(string)ShellGiftsObject["productId"][i]["previewMessageActivated"]); 						// Add gift (the promotion is part of an offer) 						Rows.Add(new Promotie() 						{ 							PromotionType = PromotieType.Gift, 							Title = (string)ShellGiftsObject["productId"][i]["productTileTitle"], 							Image = (string)ShellGiftsObject["productId"][i]["rectangleImage"], 							Logo = (string)ShellGiftsObject["productId"][i]["logo"], 							ShowLogo = (bool)ShellGiftsObject["productId"][i]["showLogo"], 							ProductText = (string)ShellGiftsObject["productId"][i]["productText"], 							ConfirmButton = (string)ShellGiftsObject["productId"][i]["ConfirmButton"], 							CancelButton = (string)ShellGiftsObject["productId"][i]["CancelButton"], 							GiftJson = JsonConvert.SerializeObject(product), 							OfferJson = promotie, 							GiftActivated = productDetails.GiftChosen, 							OfferGiftJson = JsonConvert.SerializeObject(selectedGift), 							NotActivated = NotActivated, 							OfferNr = JToken.Parse(promotie).Value<string>("OfferNr"), 							ItemNr = selectedGift.ItemNr, 							RectangleImage = (string)ShellGiftsObject["productId"][i]["rectangleImage"], 							ActivatedMessageTitle = AMMain.RemoveHTMLTags((string)ShellGiftsObject["productId"][i]["activatedMessageTitle"]), 							ActivatedMessage = AMMain.RemoveHTMLTags((string)ShellGiftsObject["productId"][i]["activatedMessage"]), 							Validity = (string)ShellGiftsObject["productId"][i]["validity"], 							ProductTileTitle = (string)ShellGiftsObject["productId"][i]["productTileTitle"],
							ConditionsLabel = (string)ShellGiftsObject["productId"][i]["conditionsLabel"],
							UsedCouponTitle = (string)ShellGiftsObject["productId"][i]["usedCouponTitle"],
							PrintButtonTitle = (string)ShellGiftsObject["productId"][i]["printButtonTitle"],
							GiftValidUntil = (string)ShellGiftsObject["productId"][i]["giftValidUntil"],
							CancelButtonMobile = (string)ShellGiftsObject["productId"][i]["cancelButtonMobile"],
							PreviewMessageActivated = (string)ShellGiftsObject["productId"][i]["previewMessageActivated"],
							ChangeGift = (string)ShellGiftsObject["productId"][i]["changeGift"],
							UsedOfferText = (string)ShellGiftsObject["productId"][i]["usedOfferText"],
							UsedOfferTitle = (string)ShellGiftsObject["productId"][i]["usedOfferTitle"],
							OpenDetails = (string)ShellGiftsObject["productId"][i]["openDetails"],
							LogoUrl = (string)ShellGiftsObject["productId"][i]["logoUrl"],
							PeriodValidUntil = (string)ShellGiftsObject["productId"][i]["periodValidUntil"],
							GeneralConditionsValue = (string)ShellGiftsObject["productId"][i]["generalConditionsValue"],
							ConditionsText = (string)ShellGiftsObject["productId"][i]["conditionsText"],
							ConditionsValue = (string)ShellGiftsObject["productId"][i]["conditionsValue"],
							OfferUsed = product.OfferUsed,

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																															}); 					} 
					Console.WriteLine("smiles detailvc rows to array "+JsonConvert.SerializeObject(Rows.ToArray())); 					SmilesDetailViewController Redirect = App.Storyboard.InstantiateViewController("SmilesDetailViewController") as SmilesDetailViewController;

					NavCtrl.PopViewController(false);
					Redirect.PersonalGifts = false;
					Redirect.PersonalGiftsDetails = true;
					Redirect.Title = GiftPromotie.ProductTileTitle;
					Redirect.Promoties = Rows.ToArray();
					Redirect.DrawGiftsInCollectionView();
					NavCtrl.PushViewController(Redirect, false); 				} 			}  			LoadingView.Hide(); 		}


		private Promotie[] GetCollectionViewItems(PromotieType type)
		{

			string promotie = JsonConvert.SerializeObject(Promoties);
			Console.WriteLine("promotie vanuit smilesinfo " + promotie);
			var Counter = 0;
			// Define lists for the Row
			List<Promotie> Rows = new List<Promotie>();

			if (Promoties != null)
			{
				for (int i = 0; i < Promoties.Length; i++)
				{
					// if Rows don't exist, create them
					if (Rows == null)
					{
						Rows = new List<Promotie>();
					}

					if (Promoties[i].Keywords != null)
					{
						//JsonArray keywords = (JsonArray)Promoties[i].Keywords[0];
						Console.WriteLine("keywords != null");

						foreach (string key in Promoties[i].Keywords)
						{
							if (key == "onvindbaar")
							{
								findable = true;
								Console.WriteLine("key is onvindbaar " + Promoties[i].Title);
							}
							else
							{
								Console.WriteLine("key != onvindbaar" + Promoties[i].Title);
								findable = false;
							}
						}

						if (!string.IsNullOrEmpty(Promoties[i].Image) && !findable)
						{
							Counter++;
							string KeuzeCounter = "Keuze " + Counter + " van " + (Promoties.Length);
							string offerGiftJson = "";

							GiftsOffer giftsOffer = JsonConvert.DeserializeObject<GiftsOffer>(Promoties[i].OfferJson);
							if (giftsOffer.Gift.Length > 0)
							{
								offerGiftJson = JsonConvert.SerializeObject(giftsOffer.Gift[0]);
								Console.WriteLine("itemnr detailvc " + Promoties[i].ItemNr);
								Console.WriteLine("promotie detailvc cheese " + JsonConvert.SerializeObject(Promoties[i]));
								Console.WriteLine("Offer Used Promoties " + Promoties[i].OfferUsed);
								//Console.WriteLine("ofergiftjson promotie? " + JsonConvert.SerializeObject(giftsOffer.Gift[0]));
								// Add the Table Row
								Rows.Add(new Promotie()
								{
									EndDate = Promoties[i].EndDate,
									RegistrationDate = Promoties[i].RegistrationDate,
									PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
									Logo = Promoties[i].Logo,
									PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
									PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
									PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
									MilesOnly = Promoties[i].MilesOnly,
									Url = Promoties[i].Url,
									OfferUsed = Promoties[i].OfferUsed,
									//Segue = "PromotieDetailSegue"
									Title = Promoties[i].ProductTileTitle,
									Image = Promoties[i].RectangleImage,
									OfferJson = Promoties[i].OfferJson,
									GiftJson = Promoties[i].GiftJson,
									OfferGiftJson = Promoties[i].OfferGiftJson,
									ProductIds = Promoties[i].ProductIds,
									OfferNr = Promoties[i].OfferNr,
									ItemNr = Promoties[i].ItemNr,
									GiftActivated = Promoties[i].GiftActivated,
									OpenDetails = Promoties[i].OpenDetails,
									ConditionsLabel = Promoties[i].ConditionsLabel,
									ConditionsValue = Promoties[i].ConditionsValue,
									PreviewMessageActivated = Promoties[i].PreviewMessageActivated,
									ActivatedMessageTitle = Promoties[i].ActivatedMessageTitle,
									//Logo = Logo,
									//ShowLogo = ShowLogo,
									ProductText = AMMain.RemoveHTMLTags(Promoties[i].ProductText),
									ConfirmButton = Promoties[i].ConfirmButton,
									CancelButton = Promoties[i].CancelButton,
									ActivatedMessage = Promoties[i].ActivatedMessage,
									ConditionsButton = Promoties[i].ConditionsButton,
									GeneralConditions = Promoties[i].ConditionsText,
									ConditionsTitle = Promoties[i].ConditionsTitle,
									GeneralConditionsValue = Promoties[i].GeneralConditionsValue,
									ChoiceCounter = KeuzeCounter,
								});
								findable = false;
							}
						}
					}
					else
					{
						//if (!string.IsNullOrEmpty(Promoties[i].Image))
						//{
						Counter++;
						string KeuzeCounter = "Keuze " + Counter + " van " + (Promoties.Length);

						string offerGiftJson = "";

						GiftsOffer giftsOffer = JsonConvert.DeserializeObject<GiftsOffer>(Promoties[i].OfferJson);
						if (giftsOffer.Gift.Length > 0)
						{
							offerGiftJson = JsonConvert.SerializeObject(giftsOffer.Gift[0]); ;

							Console.WriteLine("OfferJson " + JsonConvert.SerializeObject(Promoties[i].OfferJson));
							Console.WriteLine("itemnr detailvc " + Promoties[i].ItemNr);
							Console.WriteLine("promotie detailvc cheese " + JsonConvert.SerializeObject(Promoties[i]));
							Console.WriteLine("Offer Used Promoties "+Promoties[i].OfferUsed);
							// Add the Table Ro
							Rows.Add(new Promotie()
							{
								EndDate = Promoties[i].EndDate,
								RegistrationDate = Promoties[i].RegistrationDate,
								PriceTitle = AMMain.RemoveHTMLTags(Promoties[i].PriceTitle),
								Logo = Promoties[i].Logo,
								PriceDetail = AMMain.RemoveHTMLTags(Promoties[i].PriceDetail),
								PropositionTitle = AMMain.RemoveHTMLTags(Promoties[i].PropositionTitle),
								PropositionDetail = AMMain.RemoveHTMLTags(Promoties[i].PropositionDetail),
								MilesOnly = Promoties[i].MilesOnly,
								Url = Promoties[i].Url,
								OfferUsed = Promoties[i].OfferUsed,
								ActivatedMessageTitle = Promoties[i].ActivatedMessageTitle,
								//Segue = "PromotieDetailSegue"
								Title = Promoties[i].ProductTileTitle,
								Image = Promoties[i].Image,
								RectangleImage = Promoties[i].RectangleImage,
								OfferJson = Promoties[i].OfferJson,
								GiftJson = Promoties[i].GiftJson,
								OfferGiftJson = Promoties[i].OfferGiftJson,
								OfferNr = Promoties[i].OfferNr,
								ItemNr = Promoties[i].ItemNr,
								GiftActivated = Promoties[i].GiftActivated,
								OpenDetails = Promoties[i].OpenDetails,
								ConditionsLabel = Promoties[i].ConditionsLabel,
								ConditionsValue = Promoties[i].ConditionsValue,
								PreviewMessageActivated = Promoties[i].PreviewMessageActivated,
								//Logo = Logo,
								//ShowLogo = ShowLogo
								ProductText = AMMain.RemoveHTMLTags(Promoties[i].ProductText),
								ConfirmButton = Promoties[i].ConfirmButton,
								CancelButton = Promoties[i].CancelButton,
								ProductIds = Promoties[i].ProductIds,
								ActivatedMessage = Promoties[i].ActivatedMessage,
								ConditionsButton = Promoties[i].ConditionsButton,
								GeneralConditions = Promoties[i].ConditionsText,
								ConditionsTitle = Promoties[i].ConditionsTitle,
								GeneralConditionsValue = Promoties[i].GeneralConditionsValue,
								ChoiceCounter = KeuzeCounter
							});
						}
					}
				}

				// Return the Rows as an arra
				return Rows.ToArray();
			}

			return Rows.ToArray();

		}
		/*

			if (!string.IsNullOrEmpty(SmilesGifts))
			{
				for (int i = 0; i < count; i++)
				{
					// if Rows don't exist, create them
					if (Rows == null)
					{
						Rows = new List<Promotie>();
					}

					string RectangleImage = (string)SmilesGiftsOnject["productId"][i]["rectangleImage"];

					if (!string.IsNullOrEmpty(RectangleImage))
					{
						//test
						Counter++;
						string ProductTileTitle = (string)SmilesGiftsOnject["productId"][i]["productTileTitle"];
						string ProductText = (string)SmilesGiftsOnject["productId"][i]["productText"];
						string ConfirmButton = (string)SmilesGiftsOnject["productId"][i]["confirmButton"];
						string CancelButton = (string)SmilesGiftsOnject["productId"][i]["cancelButtonMobile"];
						string Logo = (string)SmilesGiftsOnject["productId"][i]["logoUrl"];
						string ConditionsButton = (string)SmilesGiftsOnject["productId"][i]["checkConditionsLabel"];
						string ConditionsText = (string)SmilesGiftsOnject["productId"][i]["generalConditionsValue"];
						string ConditionsTitle = (string)SmilesGiftsOnject["productId"][i]["conditionsText"];
						string ActivatedMessage = (string)SmilesGiftsOnject["productId"][i]["activatedMessage"];
						string KeuzeCounter = "Keuze " + Counter + " van " + (count-1);
						bool ShowLogo = (bool)SmilesGiftsOnject["productId"][i]["showLogo"];

						Console.WriteLine("producttiletitle " + ProductTileTitle);


							// Add the Table Row
							Rows.Add(new Promotie()
							{
								Title = ProductTileTitle,
								Image = RectangleImage,
								//Logo = Logo,
								//ShowLogo = ShowLogo,
								ProductText = AMMain.RemoveHTMLTags(ProductText),
								ConfirmButton = ConfirmButton,
								CancelButton = CancelButton,
								ActivatedMessage = ActivatedMessage,
								ConditionsButton = ConditionsButton,
								GeneralConditions = ConditionsText,
								ConditionsTitle = ConditionsTitle,
								ChoiceCounter = KeuzeCounter
							});
					}
				}

				// Return the Rows as an arra
				return Rows.ToArray();

			}

			return Rows.ToArray();

		}
		*/

		public void SetPromotions(Promotie[] promoties)
		{
			Promoties = promoties;
		}

		public void GetPersonalGiftsFromPartner()
		{

			// personal offers from locals storage
			string JsonGift = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);

			Console.WriteLine("jsongift " + JsonGift);
			if (!string.IsNullOrEmpty(JsonGift))
			{
				GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(JsonGift);
				Console.WriteLine("Gifts convert " + JsonConvert.SerializeObject(Gifts.Response));
				Console.WriteLine("gifts response length " + Gifts.Response.Length);
				if (Promoties.Length > 0)
				{
					for (int i = 0; i < Promoties.Length; i++)
					{
						
						bool productNotChosen = true;
						GiftsOffer offer = Gifts.Response[i].Offer[0];

						// if gift is activated
						if (offer.Activated == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);
									}
									// change bool
									productNotChosen = false;
								}
							}

						}
						// if gift is activated
						else if (offer.OfferUsed == "true")
						{

							for (int j = 0; j < offer.Gift.Length; j++)
							{
								GiftsOfferGift gift = offer.Gift[j];

								if (gift.GiftChosen == "true")
								{
									Console.WriteLine("activated product, add " + gift.ProductID + " to list");

									if (!giftProducts.Contains(gift.ProductID))
									{
										//gift is chosen, add productid to the list
										giftProducts.Add(gift.ProductID);
									}
									// change bool
									productNotChosen = false;
								}
							}

						}
						if (productNotChosen && !giftPartners.Contains(offer.Partner))
						{
							Console.WriteLine("add " + offer.Partner + " to list");

							// add the partnername to the list
							giftPartners.Add(offer.Partner);
						}
					}
				}
				GeneratePersonalGiftView(Gifts.Response, null);
			}
		}

		public async void GeneratePersonalGiftView(GiftsResponse[] Offers, MendixPersonalizedPromotionRoot PersonalizedPromotionsRoot)
		{
			SetLoader(AMStrings.LoadingGifts);
			if (GiftsPromoties != null)
			{
				GiftsPromoties.Clear();
			}
			if (GiftsPromoties == null)
			{
				GiftsPromoties = new List<Promotie>();
			}

			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Persoonlijk aanbod");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			// create a dictionary where the hippo-returns are stored. based on their partnername
			var GiftOfferPartnerDictionary = new Dictionary<string, object>();

			//var GiftsDictionary = new Dictionary<string, GiftRoot>();

			string productIds = null;

			// create another dictionay where the counters for the images are stored
			var CounterDictionary = new Dictionary<string, int>();

			Console.WriteLine("found partners " + giftPartners.Count.ToString());
			Console.WriteLine("giftpartners " + JsonConvert.SerializeObject(giftPartners));
			Console.WriteLine("giftproducts " + JsonConvert.SerializeObject(giftProducts));

			bool offerUsed = false;

			if (giftPartners.Count > 0)
			{
				// Set partner gifts
				foreach (string partner in giftPartners)
				{
					GiftsOfferPartner PartnerGifts = await GiftsService.GetGiftsFromPartner(partner);

					if (PartnerGifts != null)
					{
						// save the data in the dictionary
						GiftOfferPartnerDictionary.Add(partner, PartnerGifts);
						// set counter at 0
						CounterDictionary.Add(partner, 0);
					}
					else
					{
						Console.WriteLine($"no partner git found in {partner}");
					}
				}
			}

			if (giftProducts.Count > 0)
			{
				productIds = string.Join(",", giftProducts);
				GiftRoot GiftRoot = await GiftsService.GetGiftsByIds(productIds);

				Console.WriteLine($"productIds: {productIds}");

				if (GiftRoot.Gifts == null)
				{
					Console.WriteLine("geen data");
				}
				else
				{
					Console.WriteLine("Giftroot.Gifts.Lenght " + GiftRoot.Gifts.Length);
					for (int i = 0; i < GiftRoot.Gifts.Length; i++)
					{
						Gift product = GiftRoot.Gifts[i];
						GiftOfferPartnerDictionary.Add(product.ProductId, product);
					}
				}
			}

			// again loop over all offers and create the offer-objects with the hippo data attached
			Console.WriteLine("Offers.Length " + Offers.Length);
			for (int i = 0; i < Offers.Length; i++)
			{
				string partnerName = Offers[i].Offer[0].Partner;

				bool showLogo = false;
				string img = "";
				string logo = "";
				string title = "";
				string giftJson = "";
				string offerGiftJson = "";
				string usedOfferTitle = "";
				string usedOfferText = "";
				string RectangleImage = "";
				string Partner = "";
				string ProductValidUntil = "";
				string ProductId = "";
				string endDate = "";
				string registrationDate = "";
				string priceTitle = "";
				string priceDetail = "";
				string propositionTitle = "";
				string propositionDetail = "";
				string url = "";
				string activatedMessage = "";
				string chooseGifts = "";
				string giftValidTo = "";
				string productText = "";
				string conditionsLabel = "";
				string checkConditionsLabel = "";
				string paymentTitle = "";
				string paymentImage = "";
				string paymentMethod = "";
				string productType = "";
				string linkPartner = "";
				string conditions = "";
				string buttonTitle = "";
				string deliverTime = "";
				string startDate = "";
				string paymentColor = "";
				string storeLocator = "";
				string openCoupon = "";
				string usedCouponTitle = "";
				string printButtonTitle = "";
				string giftValidUntil = "";
				string cancelButton = "";
				string cancelButtonMobile = "";
				string activatedMessageTitle = "";
				string productTileTitle = "";
				string previewMessageActivated = "";
				string changeGift = "";
				string openDetails = "";
				string confirmButton = "";
				bool milesOnly = false;
				string propositionValue = "";
				string propositionLabel = "";
				string priceValue = "";
				string priceLabel = "";
				string logoUrl = "";
				string optimizedLogoUrl = "";
				string periodValidUntil = "";
				string ticketSecondaryImageUrl = "";
				string generalConditionsValue = "";
				string ticketImageUrl = "";
				string productSubType = "";
				string conditionsValue = "";
				string voucherType = "";
				string partnerImageUrl = "";
				string ticketDetailsHeaderText = "";
				string orderedByText = "";
				string pricePerTicketText = "";
				string addressText = "";
				string validityText = "";
				string conditionsText = "";
				string shoppingBagsImageUrl = "";
				string logoImageUrl = "";
				string shortDescription = "";
				string summary = "";
				string category = "";
				string validity = "";
				string address = "";
				string id = "";
				string type = "";
				string itemNr = "";
				string offerNr = "";

				//offer to add
				GiftsOffer thisOffer = Offers[i].Offer[0];
				string offerJson = JsonConvert.SerializeObject(thisOffer);

				AMMain.SetSeenGifts(thisOffer.OfferNr);

				Gift productDetails = null;


				//if an offer is activated find the selected gift
				//if (thisOffer.Activated == "true")
				//{
				Console.WriteLine("activated offers " + thisOffer.Gift.Length);
				Console.WriteLine("offerjson proddet " + JsonConvert.SerializeObject(thisOffer));
				for (int z = 0; z < thisOffer.Gift.Length; z++)
				{
					GiftsOfferGift Gift = thisOffer.Gift[z];
					Console.WriteLine("");
					if (Gift.GiftChosen == "true")
					{
						if (GiftOfferPartnerDictionary.ContainsKey(Gift.ProductID))
						{
							Console.WriteLine("product Id offer: " + Gift.ProductID);
							productDetails = (Gift)GiftOfferPartnerDictionary[Gift.ProductID];

							Console.WriteLine("Gift usedOfferTitle " + productDetails.UsedOfferTitle);
							Console.WriteLine("giftjson proddet " + JsonConvert.SerializeObject(productDetails));
							Console.WriteLine("offergiftjson proddet " + JsonConvert.SerializeObject(Gift));

							// set the image and logo
							img = productDetails.Image;
							logo = productDetails.PartnerImageUrl;
							showLogo = productDetails.ShowLogo;
							RectangleImage = productDetails.RectangleImage;
							usedOfferTitle = productDetails.UsedCouponTitle;
							usedOfferText = productDetails.UsedOfferText;
							Partner = productDetails.PartnerName;
							ProductId = Gift.ProductID;
							ProductValidUntil = productDetails.GiftValidUntil;
							endDate = productDetails.EndDate;
							registrationDate = productDetails.RegistrationDate;
							priceTitle = productDetails.PriceTitle;
							priceDetail = productDetails.PriceDetail;
							propositionTitle = productDetails.PropositionTitle;
							propositionDetail = productDetails.PropositionDetail;
							url = productDetails.Url;
							activatedMessage = productDetails.ActivatedMessage;
							chooseGifts = productDetails.ChooseGifts;
							giftValidTo = productDetails.GiftValidUntil;
							productText = productDetails.ProductText;
							conditionsLabel = productDetails.ConditionsLabel;
							checkConditionsLabel = productDetails.CheckConditionsLabel;
							paymentTitle = productDetails.PaymentTitle;
							paymentImage = productDetails.PaymentImage;
							paymentMethod = productDetails.PaymentMethod;
							productType = productDetails.ProductType;
							linkPartner = productDetails.LinkPartner;
							conditions = productDetails.Conditions;
							buttonTitle = productDetails.ButtonTitle;
							deliverTime = productDetails.DeliverTime;
							startDate = productDetails.StartDate;
							paymentColor = productDetails.PaymentColor;
							storeLocator = productDetails.StoreLocator;
							openCoupon = productDetails.OpenCoupon;
							usedCouponTitle = productDetails.UsedCouponTitle;
							printButtonTitle = productDetails.PrintButtonTitle;
							giftValidUntil = productDetails.GiftValidUntil;
							cancelButton = productDetails.CancelButton;
							cancelButtonMobile = productDetails.CancelButtonMobile;
							activatedMessageTitle = productDetails.ActivatedMessageTitle;
							productTileTitle = productDetails.ProductTileTitle;
							previewMessageActivated = productDetails.PreviewMessageActivated;
							changeGift = productDetails.ChangeGift;
							openDetails = productDetails.OpenDetails;
							confirmButton = productDetails.ConfirmButton;
							milesOnly = productDetails.MilesOnly;
							propositionValue = productDetails.PropositionValue;
							propositionLabel = productDetails.PropositionLabel;
							priceValue = productDetails.PriceValue;
							priceLabel = productDetails.PriceLabel;
							logoUrl = productDetails.LogoUrl;
							optimizedLogoUrl = productDetails.OptimizedLogoUrl;
							periodValidUntil = productDetails.PeriodValidUntil;
							ticketSecondaryImageUrl = productDetails.TicketSecondaryImageUrl;
							generalConditionsValue = productDetails.GeneralConditionsValue;
							ticketImageUrl = productDetails.TicketImageUrl;
							productSubType = productDetails.ProductSubType;
							conditionsValue = productDetails.ConditionsValue;
							voucherType = productDetails.VoucherType;
							partnerImageUrl = productDetails.PartnerImageUrl;
							ticketDetailsHeaderText = productDetails.TicketDetailsHeaderText;
							orderedByText = productDetails.OrderedByText;
							pricePerTicketText = productDetails.PricePerTicketText;
							addressText = productDetails.AddressText;
							validityText = productDetails.ValidityText;
							conditionsText = productDetails.ConditionsText;
							shoppingBagsImageUrl = productDetails.ShoppingBagsImageUrl;
							logoImageUrl = productDetails.LogoImageUrl;
							shortDescription = productDetails.ShortDescription;
							summary = productDetails.Summary;
							category = productDetails.Category;
							validity = productDetails.Validity;
							address = productDetails.Address;
							id = productDetails.Id;
							type = productDetails.Type;
							itemNr = productDetails.ItemNr;
							offerNr = productDetails.OfferNr;

							// set the json for the offer objects
							if (!string.IsNullOrEmpty(productDetails.PeriodValidUntil))
							{
								ProductValidUntil = productDetails.PeriodValidUntil;
							}
							giftJson = JsonConvert.SerializeObject(productDetails);
							offerGiftJson = JsonConvert.SerializeObject(Gift);

							// if offer is used
							if (thisOffer.OfferUsed == "true")
							{
								title = productDetails.UsedOfferTitle;

							}
							if (thisOffer.Activated == "true")
							{
								title = productDetails.ActivatedMessageTitle;
							}
							else
							{
								title = productDetails.ActivatedMessageTitle;
							}

						}
					}

				}
				//}
				// if offer is used
				if (thisOffer.OfferUsed == "true")
				{
					offerUsed = true;
					title = productDetails.UsedCouponTitle;

				}
				else
				{
					offerUsed = false;
				}

				// if the img is not set with a chosen product
				if (img == "")
				{

					//Console.WriteLine("thisoffer "+ JsonConvert.SerializeObject(thisOffer));
					//Console.WriteLine("product id giftsoffer "+ thisOffer.Gift[i].ProductID);
					//hippo data to add to the offer, from the dictionary
					GiftsOfferPartner partnerDetails = (GiftsOfferPartner)GiftOfferPartnerDictionary[partnerName];

					if (partnerDetails != null)
					{
						string[] images = partnerDetails.PreviewImages;
						int counter = CounterDictionary[partnerName];
						int max = images.Length - 1;
						img = images[counter];
						Console.WriteLine("counter images gift " + counter);

						if (counter >= max)
							CounterDictionary[partnerName] = 0;
						else
							CounterDictionary[partnerName] = counter + 1;
						title = partnerDetails.PreviewTitle;
						logo = partnerDetails.PartnerProgramLogo;
						showLogo = true;
					}
				}

				if (GiftsPromoties == null)
				{
					GiftsPromoties = new List<Promotie>();
				}

				Console.WriteLine("produtc Id lalala " + thisOffer.Gift[0].ProductID);
				Console.WriteLine("generalconditionsvalue " + generalConditionsValue);
				// Add offer
				GiftsPromoties.Add(new Promotie()
				{
					PromotionType = PromotieType.Offer,
					Title = AMMain.RemoveHTMLTags(title),
					Id = thisOffer.Gift[0].ProductID,
					Image = img,
					RectangleImage = RectangleImage,
					Logo = logo,
					ShowLogo = showLogo,
					GiftsOffer = thisOffer,
					ProductIds = productIds,
					GiftJson = giftJson,
					OfferJson = offerJson,
					OfferGiftJson = offerGiftJson,
					UsedOfferTitle = usedOfferTitle,
					UsedOfferText = usedOfferText,
					OfferUsed = offerUsed,
					PartnerName = Partner,
					ItemNr = itemNr,
					OfferNr = offerNr,
					EndDate = endDate,
					RegistrationDate = registrationDate,
					PriceTitle = AMMain.RemoveHTMLTags(priceTitle),
					PriceDetail = AMMain.RemoveHTMLTags(priceDetail),
					PropositionTitle = AMMain.RemoveHTMLTags(propositionTitle),
					PropositionDetail = AMMain.RemoveHTMLTags(propositionDetail),
					Url = url,
					ActivatedMessage = AMMain.RemoveHTMLTags(activatedMessage),
					ChooseGifts = chooseGifts,
					GiftValidTo = giftValidTo,
					ProductText = productText,
					ConditionsLabel = conditionsLabel,
					CheckConditionsLabel = checkConditionsLabel,
					PaymentTitle = paymentTitle,
					PaymentImage = paymentImage,
					PaymentMethod = paymentMethod,
					ProductType = productType,
					ProductId = thisOffer.Gift[0].ProductID,
					LinkPartner = linkPartner,
					Conditions = conditions,
					ButtonTitle = buttonTitle,
					DeliverTime = deliverTime,
					StartDate = startDate,
					PaymentColor = paymentColor,
					StoreLocator = storeLocator,
					OpenCoupon = openCoupon,
					UsedCouponTitle = usedCouponTitle,
					PrintButtonTitle = printButtonTitle,
					GiftValidUntil = giftValidUntil,
					CancelButton = cancelButton,
					CancelButtonMobile = cancelButtonMobile,
					ActivatedMessageTitle = activatedMessageTitle,
					ProductTileTitle = productTileTitle,
					PreviewMessageActivated = previewMessageActivated,
					ChangeGift = changeGift,
					OpenDetails = openDetails,
					ConfirmButton = confirmButton,
					MilesOnly = milesOnly,
					PropositionValue = propositionValue,
					PropositionLabel = propositionLabel,
					PriceValue = priceValue,
					PriceLabel = priceLabel,
					LogoUrl = logoUrl,
					OptimizedLogoUrl = optimizedLogoUrl,
					PeriodValidUntil = periodValidUntil,
					TicketSecondaryImageUrl = ticketSecondaryImageUrl,
					GeneralConditionsValue = generalConditionsValue,
					TicketImageUrl = ticketImageUrl,
					productSubType = productSubType,
					ConditionsValue = conditionsValue,
					VoucherType = voucherType,
					PartnerImageUrl = partnerImageUrl,
					TicketDetailsHeaderText = ticketDetailsHeaderText,
					OrderedByText = orderedByText,
					PricePerTicketText = pricePerTicketText,
					AddressText = addressText,
					ValidityText = validityText,
					ConditionsText = conditionsText,
					ShoppingBagsImageUrl = shoppingBagsImageUrl,
					LogoImageUrl = logoImageUrl,
					ShortDescription = shortDescription,
					Summary = summary,
					Category = category,
					Validity = validity,
					Address = address,
					Type = type,
					//Segue = "PromotieDetailSegue",
				});

			}


			if (GiftsPromoties == null || GiftsPromoties.Count == 0)
			{
				if (LoadingView != null)
				{
					LoadingView.Hide();

				}
			}
			else
			{
				Console.WriteLine("giftspromoties to array " + JsonConvert.SerializeObject(GiftsPromoties.ToArray()));
				if (LoadingView != null)
				{
					LoadingView.Hide();
					LoadingView.RemoveFromSuperview();
				}


				CollectionView.Source = new SmilesCollectionViewSource(this, GiftsPromoties.ToArray());
				CollectionView.Delegate = new SmilesFlowDelegate(this, GiftsPromoties.ToArray(), true, false);
				//CollectionView.RegisterClassForSupplementaryView(typeof(PromotionCollectionViewHeader), UICollectionElementKindSection.Header, "PersonalPromotionsCollectionViewHeader");
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
				LoadingView.RemoveFromSuperview();
			}

		}

		private void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}

		void Refresh()
		{
			RedrawGifts = false;
			DrawGiftsInCollectionView();
			CollectionView.ReloadData();
			if (RefreshControl != null)
			{
				RefreshControl.EndRefreshing();
			}
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

			// do first a control on the Identifier for your segue
			var navctlr = segue.DestinationViewController as SmilesInfoViewController;

			if (segue.Identifier == "SmilesInfoSegue")
			{

				NSIndexPath rowPath = sender as NSIndexPath;

				var itemPath = rowPath.Row;


			}
		}

	}
}