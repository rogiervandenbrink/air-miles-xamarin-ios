﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using Google.Analytics;
using Newtonsoft.Json;
using System.Threading;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    partial class AanbodViewController : UICollectionViewController
    {
        // Network status
        NetworkStatus Internet;

        readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

        //refreshlayout
        UIRefreshControl RefreshControl;
        bool IsRefresh;
        bool ViewLoaded;

        public static bool FinishedLoading { get; set;}
        // Overlays
        public static LoadingOverlay LoadingView { get; set;}
        MaintainanceOverlay MaintainanceView;
        NetworkOverlay NetworkView;

        Aanbod[] CurrentAanbodCategorien;

        AanbodPromoties AanbodPromoties;


        bool ViewLoad;

        public ObservableCollection<TableRow> AanbodObCol;

        public AanbodViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            ViewLoaded = true;
            Internet = Reachability.InternetConnectionStatus();
            Reachability.ReachabilityChanged += InternetChanged;
            CurrentAanbodCategorien = null;

            if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
            {
                // UIRefreshControl iOS6
                RefreshControl = new UIRefreshControl();
                RefreshControl.TintColor = UIColor.FromRGB(0, 125, 195);
                RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
            }
            else {
                // old style refresh button
                NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Refresh), false);
                NavigationItem.RightBarButtonItem.Clicked += (sender, e) => { Refresh(); };
            }
            CollectionView.Frame = UIScreen.MainScreen.Bounds;
            //RetrieveAanbod();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            SetAnalytics();
            CollectionView.UserInteractionEnabled = true;
            Console.WriteLine("new session is "+NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSession));
            //AnimateCells();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            CurrentAanbodCategorien = null;

            // cancel the webservice via token
            if (cancelToken.Token.CanBeCanceled)
                cancelToken.Cancel();

        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            App.ViewCtrl = this;

            App.TabBar.ViewControllers[2].TabBarItem.Enabled = true;
            //AMMain.GetGiftsForBadge(this);
            Internet = Reachability.InternetConnectionStatus();

            // If internet
            if (Internet != NetworkStatus.NotReachable)
            {

            }
            else
            {
                AMAlerts.DefaultAlert(this, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
            }
            CurrentAanbodCategorien = null;

            FinishedLoading = false;
            Console.WriteLine("newsession aanbod "+NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSessionAanbod));
            if (AMMain.DoWebService(AMLocalStorage.AanbodService) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSession) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSessionAanbod))
            {
                RetrieveAanbod();
                NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.NewSession);
                NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.NewSessionAanbod);
            }
            else {

                string localAanbod = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonAanbod);

                if (!string.IsNullOrEmpty(localAanbod))
                {
                    AanbodCategorien CurrentAanbod = JsonConvert.DeserializeObject<AanbodCategorien>(localAanbod);

                    Console.WriteLine("Setting aanbod + ");
                    //for (int i = 0; i < CurrentAanbod.Categorien.Length; i++)
                    //{
                    CollectionView.DataSource = new DefaultCollectionViewSource(this, GetCollectionViewItems(CurrentAanbod.Categorien));
                    CollectionView.Delegate = new AanbodFlowDelegate(this, GetCollectionViewItems(CurrentAanbod.Categorien));
                    CollectionView.UserInteractionEnabled = true;
                    //}
                    //AanbodPromoties = JsonConvert.DeserializeObject<AanbodPromoties>(localPromoties);
                    //CollectionView.UserInteractionEnabled = true;
                    if (LoadingView != null)
                    {
                        LoadingView.Hide();
                    }
                }
                else
                {
                    if (LoadingView != null)
                    {
                        LoadingView.Hide();
                    }
                    RetrieveAanbod();
                }
                if (LoadingView != null)
                {
                    LoadingView.Hide();
                }
            }
            //PrepareCellsForAnimation();
        }

        public bool HasPersonalOffer()
        {
            if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
            {
                string JsonGift = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);
                string JsonPersonalizedPromotions = NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions);

                Console.WriteLine("gifts localstorage: "+ JsonGift);
                Console.WriteLine("personalized promotions "+ JsonPersonalizedPromotions);
                if (!string.IsNullOrEmpty(JsonGift))
                {
                    GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(JsonGift);

                    if (Gifts.Response.Length > 0)
                    {
                        Console.WriteLine("User has " + Gifts.Response.Length + " gifts");

                        return true;
                    }
                    Console.WriteLine("User has no gifts");
                }
                if (!string.IsNullOrEmpty(JsonPersonalizedPromotions))
                {
                    MendixPersonalizedPromotionRoot PersonalizedPromotions = JsonConvert.DeserializeObject<MendixPersonalizedPromotionRoot>(JsonPersonalizedPromotions);

                    if (PersonalizedPromotions.Response.Length > 0)
                    {
                        Console.WriteLine("User has " + PersonalizedPromotions.Response.Length + " personalized promotions");

                        return true;
                    }
                    Console.WriteLine("User has no personalized promotions");
                }
            }
            else {
                return false;
            }

            return false;
            //RetrieveAanbod();
        }

        void SetAnalytics()
        {
            // Analytics Screenname
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aanbod");
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            try
            {
                if (segue.Identifier == "AanbodDetailSegue")
                { // set in Storyboard
                    var navctlr = segue.DestinationViewController as PromotiesViewController;
                    if (navctlr != null)
                    {
                        var source = CollectionView.DataSource as DefaultCollectionViewSource;
                        var cell = sender as IconDetailCollectionViewCell;
                        var id = CollectionView.IndexPathForCell(cell).Row;
                        var indexPath = source.GetItem(id);

                        Console.WriteLine("aanbod indexpath " + indexPath);


                        navctlr.Title = indexPath.Title;
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Aanbod overzicht", "Exception: "+ ex}
                });
                Console.WriteLine("error prepare for segue Aanbod: " + ex);
            }
        }

        public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
        {
            var source = CollectionView.DataSource as DefaultCollectionViewSource;
            var cell = sender as IconDetailCollectionViewCell;
            var id = CollectionView.IndexPathForCell(cell).Row;
            var indexPath = source.GetItem(id);

            return true;
        }


        void InternetChanged(object sender, EventArgs ea)
        {
            Internet = Reachability.InternetConnectionStatus();

            if (Internet != NetworkStatus.NotReachable)
            {
                if (NetworkView != null)
                {
                    NetworkView.RemoveFromSuperview();
                }

                if (CurrentAanbodCategorien == null)
                {
                    RetrieveAanbod();
                }
            }
        }

        private string GetRowIcon(string icon)
        {

            string Icon = "";

            if (icon == "star")
            {
                Icon = "IconStar";
            }
            if (icon == "airplane")
            {
                Icon = "IconPlane";
            }
            if (icon == "desktop-screen")
            {
                Icon = "IconScreen";
            }
            if (icon == "drinkglass")
            {
                Icon = "IconDrink";
            }
            if (icon == "giftbox")
            {
                Icon = "IconGift";
            }
            if (icon == "heart")
            {
                Icon = "IconHart";
            }
            if (icon == "shoppingbag")
            {
                Icon = "IconBag";
            }

            return Icon;

        }

        public async void RetrieveAanbod()
        {
            CurrentAanbodCategorien = null;
            if (NetworkView != null)
            {
                NetworkView.Hide();
            }
            SetLoader(AMStrings.LoadingGifts);
            try
            {
                Internet = Reachability.InternetConnectionStatus();

                if (Internet != NetworkStatus.NotReachable)
                {
                    
                    //CollectionView.UserInteractionEnabled = false;

                    try
                    {
                        AanbodService AanbodService = new AanbodService();
                        AanbodCategorien CurrentAanbod = await AanbodService.GetAanbod();

                        if (CurrentAanbod.Categorien != null)
                        {
                            CurrentAanbodCategorien = CurrentAanbod.Categorien;
                            Console.WriteLine("Setting aanbod "+JsonConvert.SerializeObject(CurrentAanbodCategorien));

                            string service = AMLocalStorage.AanbodService;
                            NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);

                            CollectionView.DataSource = new DefaultCollectionViewSource(this, GetCollectionViewItems(CurrentAanbod.Categorien));
                            CollectionView.Delegate = new AanbodFlowDelegate(this, GetCollectionViewItems(CurrentAanbod.Categorien));
                            CollectionView.AlwaysBounceVertical = true;
                            CollectionView.AddSubview(RefreshControl);
                            //GetPromotions(CurrentAanbodCategorien);
                            CollectionView.ReloadData();


                        }
                        else {
                            if (LoadingView != null)
                            {
                                LoadingView.Hide();
                            }
                            //SetMaintanaceView();
                        }
                        }
                    catch (Exception ex)
                    {
                        //SetMaintanaceView();
                        AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
                        if (LoadingView != null)
                        {
                            LoadingView.Hide();
                        }
                        if (AMMain.CanShowError())
                        {
                            AMAlerts.DefaultAlert(this, AMStrings.ErrorLoginTitle, AMStrings.ErrorLoginNoConnection);
                        }
                        RefreshControl.EndRefreshing();
                    }
                    if (LoadingView != null)
                    {
                        LoadingView.Hide();
                    }

                    RefreshControl.EndRefreshing();

                } 
                else {

                    string localAanbod = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonAanbod);

                    if (!string.IsNullOrEmpty(localAanbod))
                    {
                        AanbodCategorien CurrentAanbod = JsonConvert.DeserializeObject<AanbodCategorien>(localAanbod);

                        Console.WriteLine("Setting aanbod + ");
                        //for (int i = 0; i < CurrentAanbod.Categorien.Length; i++)
                        //{
                            CollectionView.DataSource = new DefaultCollectionViewSource(this, GetCollectionViewItems(CurrentAanbod.Categorien));
                            CollectionView.Delegate = new AanbodFlowDelegate(this, GetCollectionViewItems(CurrentAanbod.Categorien));
                            CollectionView.UserInteractionEnabled = true;
                        //}
                        //AanbodPromoties = JsonConvert.DeserializeObject<AanbodPromoties>(localPromoties);
                        //CollectionView.UserInteractionEnabled = true;
                        if (LoadingView!=null)
                        {
                            LoadingView.Hide();
                        }
                    }
                    else {
                        if (LoadingView != null)
                        {
                            LoadingView.Hide();
                        }
                        AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.InternetNoConnectionMessage);
                        //SetNetworkView();
                    }

                }
                }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Aanbod overzicht", "Exception: "+ ex}
                });

                if (LoadingView != null)
                {
                    LoadingView.Hide();
                }
                AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
                RefreshControl.EndRefreshing();
                IsRefresh = false;
            } 
            if (LoadingView != null)
            {
                LoadingView.Hide();
            }
        }

        private async void UpdateAanbod()
        {
            Internet = Reachability.InternetConnectionStatus();

            // Set header if user has personal offers
            if (HasPersonalOffer())
            {
                Console.WriteLine("Show personal promotion header");
                //CollectionView.RegisterClassForSupplementaryView(typeof(PersonalPromotionsCollectionViewHeader), UICollectionElementKindSection.Header, "PersonalPromotionsCollectionViewHeader");
            }
            //CollectionView.ReloadData();

            if (Internet != NetworkStatus.NotReachable)
            {
                try
                {
                    AanbodService AanbodService = new AanbodService();
                    AanbodCategorien CurrentAanbod = await AanbodService.GetAanbod();
                    Console.WriteLine("current anbod "+CurrentAanbod);

                    if (CurrentAanbod != null)
                    {
                        if (CurrentAanbod.Categorien != null)
                        {
                            //GetPromotions(CurrentAanbod.Categorien);
                        }
                        //GetPromotions(CurrentAanbod.Categorien);

                    }
                    }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                        {"Aanbod overzicht", "Exception: "+ ex}
                    });
                    RefreshControl.EndRefreshing();
                    IsRefresh = false;
                }

            }
            RefreshControl.EndRefreshing();
            IsRefresh = false;
        }


        private async Task<AanbodPromoties> GetPromotions(string detail, string title)
        {
            try
            {
                if (detail == "uitgelicht")
                {
                    detail = "starred";
                }
                else
                {
                    detail = detail;
                }
                Console.WriteLine("categorie promotie " + detail);

                Internet = Reachability.InternetConnectionStatus();

                if (Internet != NetworkStatus.NotReachable)
                {
                    PromotiesService PromotiesService = new PromotiesService();
                    AanbodPromoties = await PromotiesService.GetAanbod(detail);


                    if (AanbodPromoties.Promoties != null)
                    {
                        Console.WriteLine("Updated promotions: " + AanbodPromoties.Promoties.Length);
                        PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;
                        Redirect.Title = title;
                        //Redirect.PersonalGifts = false;
                        //Redirect.PersonalGiftsDetails = false;
                        Redirect.SetPromotions(AanbodPromoties.Promoties);
                        Console.WriteLine("Promotiessss "+JsonConvert.SerializeObject(AanbodPromoties.Promoties));
                        NavigationController.PushViewController(Redirect, true);
                    }

                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Aanbod overzicht", "Exception: "+ ex}
                });
                Console.WriteLine("exception aanbod: "+ex);
            }

            return AanbodPromoties;
        }

        private void SetLoader(string text)
        {
            CGRect bounds = UIScreen.MainScreen.Bounds;

            LoadingView = new LoadingOverlay(bounds, text);
            View.Add(LoadingView);
        }

        private void SetNetworkView()
        {
            CGRect bounds = UIScreen.MainScreen.Bounds;

            NetworkView = new NetworkOverlay(bounds);
            View.Add(NetworkView);
        }

        private void SetMaintanaceView()
        {
            CGRect bounds = UIScreen.MainScreen.Bounds;

            MaintainanceView = new MaintainanceOverlay(bounds);
            View.Add(MaintainanceView);
        }


        private class AanbodFlowDelegate : UICollectionViewDelegateFlowLayout
        {
            AanbodViewController Controller;
            TableRow[] Rows;

            public AanbodFlowDelegate(AanbodViewController parent, TableRow[] items) : base() {
                Controller = parent;
                Rows = items;
            }

            public override CGSize GetSizeForItem (UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
            {
                nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
                nfloat ScreenHeight = UIScreen.MainScreen.Bounds.Height;
                Console.WriteLine("device orientation is "+ UIDevice.CurrentDevice.Orientation);
                if (UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.LandscapeRight || UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    return new CGSize(ScreenHeight / 2, (ScreenHeight / 2) * 0.85);
                }
                else if (UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.FaceUp || UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.Portrait)
                {
                    return new CGSize(ScreenWidth / 2, (ScreenWidth / 2) * 0.85);
                }
                else
                {
                    return new CGSize(ScreenWidth / 2, (ScreenWidth / 2) * 0.85);
                }
            }

            public override void ItemHighlighted (UICollectionView collectionView, NSIndexPath indexPath)
            {
                var Cell = collectionView.CellForItem (indexPath);
                Cell.ContentView.BackgroundColor = UIColor.FromRGB(217, 236, 246);
            }

            public override void ItemUnhighlighted (UICollectionView collectionView, NSIndexPath indexPath)
            {
                var Cell = collectionView.CellForItem (indexPath);
                Cell.ContentView.BackgroundColor = UIColor.White;
            }

            public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
            {
                var cell = (IconDetailCollectionViewCell)collectionView.CellForItem(indexPath);

                if (Rows[indexPath.Row].Id == "speciaal-voor-jou")
                {
                    //UIApplication.SharedApplication.OpenUrl(new NSUrl(Rows[indexPath.Row].Url));

                    /*AanbodDetailViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as LoginViewController;
                    Redirect.Routing = "InstellingenViewController";
                    Controller.NavigationController.PushViewController(Redirect, false);*/
                    if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
                    {
                        if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts)))
                        {
                            Console.WriteLine("personal offers " + NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts));
                            GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts));

                            MendixPersonalizedPromotionRoot PersonalizedPromotions = JsonConvert.DeserializeObject<MendixPersonalizedPromotionRoot>(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions));


                            if (Gifts.Response != null && Gifts.Errors[0].Code == "E00" || PersonalizedPromotions.Errors[0].Code == "E00" && PersonalizedPromotions.Response != null)
                            {
                                //if (Gifts.Response.Length > 0 || PersonalizedPromotions.Response.Length > 0)
                                //{
                                    NetworkStatus Internet = Reachability.InternetConnectionStatus();

                                    if (Internet != NetworkStatus.NotReachable)
                                    {
                                        PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;
                                        Redirect.Title = "Speciaal voor jou!";
                                        Redirect.PersonalGifts = true;
                                        NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.UnseenGifts);
                                        NSUserDefaults.StandardUserDefaults.Synchronize();
                                        AMMain.ClearAanbodTabBadge();
                                        App.ViewCtrl.NavigationController.PushViewController(Redirect, true);

                                        // Google analaytics
                                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanbod", "Speciaal voor jou", "Ingelogd: Ja", null).Build();
                                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                                    Gai.SharedInstance.Dispatch();
                                    }
                                    else {
                                        AMAlerts.DefaultAlert(App.ViewCtrl, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
                                    }
                                //}
                            }
                        }
                    } else
                    {
                        // Google analaytics
                        var GAItabkeuze = DictionaryBuilder.CreateEvent("Aanbod", "Speciaal voor jou", "Ingelogd: Nee", null).Build();
                        Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                        Gai.SharedInstance.Dispatch();

                        LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
                        Redirect.Routing = "AanbodViewController";
                        App.ViewCtrl.NavigationController.PushViewController(Redirect, false);
                        //Controller.PerformSegue(Rows[indexPath.Row].Segue, cell);
                    }
                }
                else {
                    Controller.GetPromotions(Rows[indexPath.Row].Id, Rows[indexPath.Row].Title);
                    //Controller.PerformSegue(Rows[indexPath.Row].Segue, cell);

                }
                collectionView.UserInteractionEnabled = false;
            }

            public override CGSize GetReferenceSizeForHeader(UICollectionView collectionView, UICollectionViewLayout layout, nint section)
            {
                //if (Controller.HasPersonalOffer())
                //{
                //    return new CGSize(UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Width);
                //}
                //else {
                    return new CGSize(0, 0);
                //}
            }
        }

        private TableRow[] GetCollectionViewItems(Aanbod[] Aanbod)
        {

            // Define lists for the Rows
            List<TableRow> Rows = new List<TableRow>();
            Rows.Clear();
            string sizePromotions = "";

            for (int i = 0; i < Aanbod.Length; i++)
            {
                // if Rows don't exist, create them
                if (Rows == null)
                {
                    Rows = new List<TableRow>();
                }
                Console.WriteLine("svgIcon "+Aanbod[i].SvgIcon);
                Console.WriteLine("icon" +Aanbod[i].Icon);
                Console.WriteLine("number " + i);
                if (Aanbod[i].SizePromotions == "1")
                {
                    sizePromotions = Aanbod[i].SizePromotions + " aanbieding";
                }else
                {
                    sizePromotions = Aanbod[i].SizePromotions + " aanbiedingen";
                }

                Rows.Add(new TableRow()
                {
                    Index = i,
                    Id = Aanbod[i].Id,
                    Title = AMMain.RemoveHTMLTags(Aanbod[i].Title),
                    Detail = sizePromotions,
                    Icon = Aanbod[i].SvgIcon,
                    Segue = "AanbodDetailSegue",
                    Url = Aanbod[i].Url,
                    CellIdentifier = CellIdentifier.IconDetailCollectionViewCell
                });

            }

            // Return the Rows as an array
            Console.WriteLine("row array " + JsonConvert.SerializeObject(Rows.ToArray()));
            return Rows.ToArray();


        }

        public AanbodPromoties GetPromoties()
        {
            return AanbodPromoties;
        }

        void Refresh()
        {
            var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Aanbod", "Pull to refresh", "Pull to refresh", null).Build();
            Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
            Gai.SharedInstance.Dispatch();

            NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.AanbodRefresh);
            IsRefresh = true;
            //UpdateAanbod();
            //AMMain.GetGiftsForBadge(this);
            CollectionView.DataSource = null;
            RetrieveAanbod();
        }

        public void PrepareCellsForAnimation()
        {
            for (int z = 0; z < CollectionView.NumberOfSections(); z++)
            {
                for (int i = 0; i < CollectionView.NumberOfItemsInSection(z); i++)
                {
                    IconDetailCollectionViewCell Cell = CollectionView.CellForItem(NSIndexPath.FromItemSection(i, z)) as IconDetailCollectionViewCell;            //Cell.Alpha = 0f;
                    Console.WriteLine("cell numer " + 1);
                    Console.WriteLine("cell " + Cell);
                    if (Cell != null)
                    {
                        Cell.Alpha = 0.0f;
                    }
                }
            }
        }

        public void AnimateCells()
        {
            for (int z = 0; z < CollectionView.NumberOfSections(); z++)
            {
                for (int i = 0; i < CollectionView.NumberOfItemsInSection(z); i++)
                {
                    IconDetailCollectionViewCell Cell = CollectionView.CellForItem(NSIndexPath.FromItemSection(i, z)) as IconDetailCollectionViewCell;            //Cell.Alpha = 0f;
                    if (Cell != null)
                    {
                        Cell.Alpha = 1.0f;
                        AMStyle.Fade(Cell, true, (z * 0.4) + (i * 0.1));
                               
                    }
                }
            }
        }
    }
}
