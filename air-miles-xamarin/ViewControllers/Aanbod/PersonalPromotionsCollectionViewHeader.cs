﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
    public partial class PersonalPromotionsCollectionViewHeader : UICollectionReusableView
    {
		NetworkStatus Internet;
		UIImageView Image;
		UIButton Button;
		UIView Border;

		public PersonalPromotionsCollectionViewHeader(IntPtr handle) : base(handle)
		{
			Button = new UIButton();
			Button.Frame = new CGRect((UIScreen.MainScreen.Bounds.Width / 2) - 150, UIScreen.MainScreen.Bounds.Width - 80, 300, 40);
			Button.SetTitle("BEKIJK SPECIAAL VOOR JOU!", UIControlState.Normal);
			Button.Font = UIFont.FromName("Avenir-Black", 12f);
			Button.BackgroundColor = UIColor.FromRGB(224, 0, 129);
			AMStyle.RoundedButton(Button);

			Image = new UIImageView();
			Image.Frame = new CGRect(0,0, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Width * 0.6);
			Image.Image = UIImage.FromFile("PersonalPromotions.png");

			Border = new UIImageView();
			Border.Frame = new CGRect(0, UIScreen.MainScreen.Bounds.Width - 1, UIScreen.MainScreen.Bounds.Width, 1);
			Border.BackgroundColor = UIColor.FromRGB(238, 238, 238);

			BackgroundColor = UIColor.White;
			AddSubviews(Image, Border, Button);

			Button.TouchUpInside += delegate
			{
				Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;
					Redirect.Title = "Speciaal voor jou!";
					Redirect.PersonalGifts = true;
					App.ViewCtrl.NavigationController.PushViewController(Redirect, true);
				}
				else {
					AMAlerts.DefaultAlert(App.ViewCtrl, AMStrings.InternetNoConnectionTitle, AMStrings.InternetNoConnectionMessage);
				}
			};
 
		}
    }
}