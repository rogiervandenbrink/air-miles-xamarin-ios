﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using SDWebImage;
using System.Threading.Tasks;
using System.Net.Http;

namespace airmilesxamarin
{
    public partial class PromotionCollectionViewFooter : UICollectionReusableView
    {
		UIImageView Image;

		[Export("initWithFrame:")]
		public PromotionCollectionViewFooter(CGRect frame) : base(frame)
		{
			Image = new UIImageView();
			Image.Frame = new CGRect((UIScreen.MainScreen.Bounds.Width / 2) - 35, 5, 70, 70);

			AddSubview(Image);
		}

		public PromotionCollectionViewFooter(IntPtr p) : base(p)
		{
		}

		public void SetImage(string image)
		{
			Console.WriteLine("set footer image to " + image);
			if (!string.IsNullOrEmpty(image))
			{
				FromUrl(image);
				/*Image.SetImage(

				url: new NSUrl(image),
				placeholder: UIImage.FromBundle("Airmiles_Placeholder")
			);*/
			}
		}

		static UIImage FromUrl(string uri)
		{
			using (var url = new NSUrl(uri))
			using (var data = NSData.FromUrl(url))
				return UIImage.LoadFromData(data);
		}
    }
}

