﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using LocalAuthentication;
using Google.Analytics;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
	partial class TransactiesViewController : UITableViewController
	{
		// Network status
		NetworkStatus Internet;

		//refreshlayout
		UIRefreshControl RefreshControl;

		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;
		TransactiesOverlay TransactiesOverlay;

		public Boolean hitBottom = false;
		public Boolean SpinnerOn = false;
		public Boolean SectionIdOneUp = false;
		public Boolean FirstUpdateTransacties = false;
		public Boolean FirstRetrieveTransacties = false;
		public bool loggedIn;

		Transactie[] firstTransactiesBatch;

		LAContext Context = new LAContext();
		NSError Error;

		public TransactiesViewController(IntPtr handle) : base(handle)
		{
		}

		public TransactiesViewController()
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Internet = Reachability.InternetConnectionStatus();
			Reachability.ReachabilityChanged += InternetChanged;

			// Tableview header
			Initialize();

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(0, 125, 195);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
			}

			//UpdateTransacties(0, 20);
			//RetrieveTransacties();

			//firstTransactiesBatch = null;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			App.LeftOnSecure = NavigationController;
			Console.WriteLine("NAV CTRL SECURE = " + App.LeftOnSecure);

			hitBottom = false;

			Console.WriteLine(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session));

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false)
			{
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

				// Set the routing is user is succesfully loggedin
				Redirect.Routing = "TransactiesViewController";
				NavigationController.PushViewController(Redirect, false);

				RemoveFromParentViewController();


			}
			else {
				firstTransactiesBatch = null;

				UpdateTransacties(0, 20, true);

				AMMain.SetExpiredSession(NavigationController, "TransactiesViewController");
			}

			// Set tab item title
			if (NavigationController != null)
			{
				//NavigationController.TabBarItem.Title = "Transacties";
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			firstTransactiesBatch = null;

			// Analytics Screenname
			Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Transacties");
			Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			firstTransactiesBatch = null;

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == false)
			{
				RemoveFromParentViewController();
			}

			if (App.LeftOnSecure == NavigationController)
			{
				App.LeftOnSecure = null;
			}

			/*firstTransactiesBatch = null;

			if (App.LeftOnSecure == NavigationController)
			{
				App.LeftOnSecure = null;
			}
			RemoveFromParentViewController();*/
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			firstTransactiesBatch = null;

			if (LoadingView != null)
			{
				TableView.ScrollEnabled = true;
				LoadingView.Hide();
			}

			if (MaintainanceView != null)
			{
				TableView.ScrollEnabled = true;
				MaintainanceView.Hide();
			}
		}

		void InternetChanged(object sender, EventArgs ea)
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable)
			{
				if (NetworkView != null)
				{
					NetworkView.RemoveFromSuperview();
				}

			}
			else
			{
				SetNetworkView();
			}
		}

		private void Initialize()
		{
			SetHeader();

			UserAvatar.Layer.CornerRadius = UserAvatar.Bounds.Height / 2;
			UserAvatar.Layer.MasksToBounds = true;

			HideBackButton();

			UserAvatar.Layer.CornerRadius = UserAvatar.Layer.Bounds.Height / 2;
			TableView.RowHeight = 60f;

			UILabel BackupText = new UILabel();
			BackupText.Text = "Geen transacties";
			BackupText.Font = UIFont.FromName("Avenir-Black", 14f);
			BackupText.TextColor = UIColor.FromRGB(0, 125, 195);
			BackupText.Frame = new CGRect((UIScreen.MainScreen.Bounds.Width / 2) - (BackupText.Bounds.Width / 2), UIScreen.MainScreen.Bounds.Height / 2, BackupText.Bounds.Width, BackupText.Bounds.Height);
		}

		private void SetHeader()
		{
			Console.WriteLine("aanhef "+NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender));
			// Set image
			UserAvatar.TranslatesAutoresizingMaskIntoConstraints = true;
			if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar)))
			{
				UserAvatar.Image = UIImage.LoadFromData(NSData.FromArray(Convert.FromBase64String(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.Avatar))));
			}
			else {
				UserAvatar.Image = UIImage.FromBundle("Airmiles_Placeholder.png");
			}

			// Set title
			if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName) != null)
			{
				UserTitle.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName);
			}
			else {
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender) == "heer" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "male")
				{
					UserTitle.Text = "Dhr.";
				}
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender) == "mevrouw" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserGender).ToLower() == "female")
				{
					UserTitle.Text = "Mevr.";
				}
				else
				{
					UserTitle.Text = "";
				}
			}

			if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName) != null)
			{
				string prefix;
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix)))
				{
					prefix = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix) + " ";
				}
				else {
					prefix = "";
				}
				UserLastname.Text = prefix + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
			}

			if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance) != null)
			{
				UserBalance.Text = AMMain.SetBalanceFormat(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Balance));
			}
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}

		private TableSection[] GetTableItems(Transactie[] transacties)
		{
			// Define lists for the Sections and Rows
			List<TableSection> Sections = new List<TableSection>();
			List<TableRow>[] Rows = new List<TableRow>[transacties.Length];

			// Section id starts with 0
			int SectionId = 0;

			for (int i = 0; i < transacties.Length; i++)
			{
				if (i > 0)
				{
					if (AMMain.DateToTime(transacties[i].Date).ToString("yyyy-MM-dd") != AMMain.DateToTime(transacties[i - 1].Date).ToString("yyyy-MM-dd"))
					{
						SectionId++;
					}
				}

				// if Section Rows don't exist, create them
				if (Rows[SectionId] == null)
				{
					Rows[SectionId] = new List<TableRow>();
				}

				// Add the Table Rows
				Rows[SectionId].Add(new TableRow()
				{
					Title = transacties[i].Organisation,
					Detail = transacties[i].Title,
					Subdetail = SharedMain.SetBalanceFormat(transacties[i].Points),
					Hiddeninfo = AMMain.DateToTime(transacties[i].Date).ToString("D", AMMain.Cultureinfo),
					CellIdentifier = CellIdentifier.DetailAmountTableViewCell
				});
			}

			if (SectionId > 0)
			{
				for (int i = 0; i < SectionId+1; i++)
				{
					Console.WriteLine("sections added" + Rows[i].Count);
					Sections.Add(new TableSection()
					{
						Title = Rows[i][0].Hiddeninfo,
						Items = Rows[i].ToArray()
					});
				}
			}
			else {
				if (Rows.Length > 0)
				{
					Sections.Add(new TableSection()
					{
						Title = Rows[0][0].Hiddeninfo,
						Items = Rows[0].ToArray()
					});
				}
			}
			// Return the Sections as Rows as an array
			return Sections.ToArray();
		}

		public async void UpdateTransacties(int LowId, int HighId, bool loading)
		{
			if (NetworkView != null)
			{
				NetworkView.Hide();
			}
			if (TransactiesOverlay != null)
			{
				TransactiesOverlay.Hide();
			}
			//InitLogin();
			if (loading)
			{
				Console.WriteLine("ADD LOADING");

				TableView.ScrollEnabled = false;
				SetLoader(AMStrings.LoadingTransacties);
			}
			TableView.Hidden = false;
			try
			{

			

			TransactiesService TransactiesService = new TransactiesService();
			TransactiesRoot CurrentTransacties = await TransactiesService.GetTransacties(LowId, HighId);

			//if (!string.IsNullOrEmpty(CurrentTransacties.TokenApp.TokenApp))
			//{
			//	AMMain.UpdateTokenApp(CurrentTransacties.TokenApp.TokenApp);
			//}

			if (loading && LoadingView != null)
			{
				Console.WriteLine("REMOVE LOADING");
				TableView.ScrollEnabled = true;
				LoadingView.Hide();
			}
			Console.WriteLine("transacties error: "+ CurrentTransacties.Error[0].Code);
			// if there is a response
			if (CurrentTransacties.Error != null)
			{
				if (CurrentTransacties.Error[0].Code == "E00")
				{
					SetHeader();
					Console.WriteLine("updating transacties");

					if (firstTransactiesBatch != null)
					{
						Transactie[] combined = new Transactie[firstTransactiesBatch.Length + CurrentTransacties.Response[0].Transacties.ToArray().Length];
						Array.Copy(firstTransactiesBatch, combined, firstTransactiesBatch.Length);
						Array.Copy(CurrentTransacties.Response[0].Transacties.ToArray(), 0, combined, firstTransactiesBatch.Length, CurrentTransacties.Response[0].Transacties.ToArray().Length);
						TableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems(combined));
						TableView.ReloadData();
						TableView.AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin;
							firstTransactiesBatch = combined;
						hitBottom = false;
						TableView.AddSubview(RefreshControl);
						RefreshControl.EndRefreshing();
						if (SpinnerOn)
						{
							TableView.TableFooterView = null;
						}
					}
					else {
						TableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems(CurrentTransacties.Response[0].Transacties.ToArray()));
						firstTransactiesBatch = CurrentTransacties.Response[0].Transacties.ToArray();
						TableView.ReloadData();
						TableView.SetContentOffset(new CGPoint(0, -64), true);
						TableView.AddSubview(RefreshControl);
						RefreshControl.EndRefreshing();
							if (CurrentTransacties.Response[0].Transacties.Length == 0)
							{
								SetBackupView("Geen transacties");
							}
					}
				}
				else {
					if (CurrentTransacties.Error[0].Code == "E23")
					{
						Console.WriteLine("E23 ");

						// Logout
						LocalStorage.Logout(TabBarController);

						// Redirect to login page
						LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
						Redirect.Routing = "TransactiesViewController";
						NavigationController.ViewControllers = new[] { Redirect };

						AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentTransacties.Error[0].Message);
					}
				}
			}
			else {
				SetMaintanaceView();
			}
				}
			catch (Exception ex)
			{
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				SetMaintanaceView();
				Console.WriteLine("exception transactions "+ex);
			}
		}

		public void SetScrollOffset(UIScrollView scrollView, nfloat currentOffset)
		{

			nfloat maximumOffset = (scrollView.ContentSize.Height) - scrollView.Frame.Size.Height;

			if (maximumOffset - currentOffset <= 100)
			{

				if (!hitBottom)
				{
					Internet = Reachability.InternetConnectionStatus();

					if (Internet != NetworkStatus.NotReachable)
					{
						hitBottom = true;
						SpinnerOn = true;
						if (firstTransactiesBatch != null)
						{
							UpdateTransacties(firstTransactiesBatch.Length+1, firstTransactiesBatch.Length + 20, false);
						}
						else {
							UpdateTransacties(0, 20, true);
						}

						//Console.WriteLine("hit bottom");
						UIActivityIndicatorView Spinner = new UIActivityIndicatorView();
						Spinner.StartAnimating();
						Spinner.Color = UIColor.FromRGB(0, 125, 195);
						Spinner.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 44);
						TableView.TableFooterView = Spinner;

					}
					else
					{
						new UIAlertView("Geen internetverbinding", "Helaas! Je hebt momenteel geen verbinding met het internet. Probeer het bij goed bereik nogmaals.", null, "OK", null).Show();
						hitBottom = true;
						Console.WriteLine("geen internet");
					}
				}
			}
		}

		void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		void SetBackupView(string text)
		{
			//CGRect bounds = UIScreen.MainScreen.Bounds;
			CGRect bounds = new CGRect(0, HeaderView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, (UIScreen.MainScreen.Bounds.Height - HeaderView.Bounds.Height));
			TransactiesOverlay = new TransactiesOverlay(bounds, text);
			View.Add(TransactiesOverlay);
		}

		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			TableView.ScrollEnabled = false;
			View.Add(MaintainanceView);
		}

		void Refresh()
		{
			UpdateTransacties(firstTransactiesBatch.Length + 1, firstTransactiesBatch.Length + 20, false);
		}


		private void SetNetworkView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			NetworkView = new NetworkOverlay(bounds);
			View.Add(NetworkView);
		}
	}
}
