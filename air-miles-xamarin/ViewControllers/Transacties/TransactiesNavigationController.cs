﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class TransactiesNavigationController : UINavigationController
    {
        public TransactiesNavigationController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			RedirectToDetailPage(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin));
		}

		protected void RedirectToDetailPage(bool login)
		{
			Console.WriteLine("redirect to detail");
			if (VisibleViewController != null)
			{
				Console.WriteLine("no view controller");

				if (login == true && this.VisibleViewController.GetType() == typeof(LoginViewController))
				{
					// redirect to transacties page
					VisibleViewController.RemoveFromParentViewController();

					TransactieAankoopViewController Redirect = App.Storyboard.InstantiateViewController("TransactieAankoopViewController") as TransactieAankoopViewController;
					Console.WriteLine("login: true, visible controller: true");
					Title = "Mijn";
					PushViewController(Redirect, false);
				}
				if (login == false && this.VisibleViewController.GetType() != typeof(LoginViewController))
				{
					// redirect to login page
					LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
					Console.WriteLine("login: false, visible controller: true");
					Title = "Login";
					Redirect.Routing = "TransactieAankoopViewController";
					PushViewController(Redirect, false);
				}
			}
			else {

				Console.WriteLine("yes view controller");

				if (App.LeftOnSecure != null)
				{
					if (App.LeftOnSecure.GetType() != typeof(TransactiesNavigationController))
					{
						if (login == true)
						{
							// redirect to transactie page
							TransactieAankoopViewController Redirect = App.Storyboard.InstantiateViewController("TransactieAankoopViewController") as TransactieAankoopViewController;
							Console.WriteLine("long: true, visible controller: false");
							Title = "Mijn";
							PushViewController(Redirect, false);
						}

						if (login == false)
						{
							// redirect to login page
							LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
							Console.WriteLine("long: false, visible controller: false");
							Title = "Mijn";
							Redirect.Routing = "TransactieAankoopViewController";
							PushViewController(Redirect, false);
						}
					}
				}
				else {
					if (login == true)
					{
						// redirect to transactie page
						TransactieAankoopViewController Redirect = App.Storyboard.InstantiateViewController("TransactieAankoopViewController") as TransactieAankoopViewController;
						Console.WriteLine("long: true, visible controller: false");
						Title = "Transacties";
						PushViewController(Redirect, false);
					}

					if (login == false)
					{
						// redirect to login page
						LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
						Console.WriteLine("long: false, visible controller: false");
						Title = "Login";
						Redirect.Routing = "TransactieAankoopViewController";
						PushViewController(Redirect, false);
					}
				}
			}
		}
	}
}