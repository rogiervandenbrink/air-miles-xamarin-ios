﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("TransactieAankoopViewController")]
    partial class TransactieAankoopViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView AankopenTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView TransactiesTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AankopenTableView != null) {
                AankopenTableView.Dispose ();
                AankopenTableView = null;
            }

            if (TransactiesTableView != null) {
                TransactiesTableView.Dispose ();
                TransactiesTableView = null;
            }
        }
    }
}