﻿using Foundation;
using System;
using UIKit;
using LocalAuthentication;
using CoreGraphics;
using Google.Analytics;
using System.Collections.Generic;
using Air_Miles_Xamarin;
using Xamarin;
using System.Threading;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace airmilesxamarin
{
    public partial class TransactieAankoopViewController : UIViewController
    {
		// Network status
		NetworkStatus Internet;

		// Overlays
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		NetworkOverlay NetworkView;
		TransactiesOverlay TransactiesOverlay;

		readonly CancellationTokenSource cancelToken = new CancellationTokenSource();

		//refreshlayout
		UIRefreshControl RefreshControl;

		UITableViewCell cell;

		public bool hitBottom = false;
		public bool SpinnerOn = false;
		public bool SectionIdOneUp = false;
		public bool FirstUpdateTransacties = false;
		public bool FirstRetrieveTransacties = false;
		public bool FirstRetrieveAankopen = false;
		public int IndexVoucher { get; set;}
		public int VoucherLength;

		airmilesxamarin.Transactie[] firstTransactiesBatch;
        public airmilesxamarin.Aankoop[] firstAankopenBatch;
        public airmilesxamarin.Aankoop[] VoucherBatch;

		AankopenRoot AankopenRoot;

		// Segue is defined in storyboard
		string VoucherSegue = "VoucherSegue";

		LAContext Context = new LAContext();
		NSError Error;

		UIView SelectedBorder;
		nfloat SelectedBorderX;

		public NSIndexPath SelectedRowAankopen { get; set;}

		public bool Aankopen { get; set; }

        public TransactieAankoopViewController (IntPtr handle) : base (handle)
        {
        }

		public TransactieAankoopViewController()
		{ 
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Console.WriteLine("saved username " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username));

			nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
			nfloat SelectedBorderWidth = ScreenWidth / 2;

			if (Aankopen == true)
			{
				//SelectedBorderX = SelectedBorderWidth;
				TransactiesTableView.Hidden = true;
				AankopenTableView.Hidden = false;
			}
			else {
				//SelectedBorderX = 0;
				AankopenTableView.Hidden = true;
				TransactiesTableView.Hidden = false;
			}

			Internet = Reachability.InternetConnectionStatus();
			Reachability.ReachabilityChanged += InternetChanged;

			// Tableview header
			Initialize();

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			hitBottom = false;

			firstTransactiesBatch = null;

			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(0, 125, 195);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
			}

			App.LeftOnSecure = NavigationController;

			Console.WriteLine(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session));

			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;

				// Set the routing is user is succesfully loggedin
				Redirect.Routing = "TransactieAankoopViewController";
				NavigationController.PushViewController(Redirect, false);

				RemoveFromParentViewController();


			}
			else {
				if (Aankopen)
				{
					this.Title = "Aankopen";
					UpdateAankopen();
                    SetState(true);
				}
				else
				{
					this.Title = "Transacties";
					UpdateTransacties(0, 20, true);
                    SetState(false);
				}
				AMMain.SetExpiredSession(NavigationController, "TransactieAankoopViewController");
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			if (Aankopen)
			{
				//AnimateCells(AankopenTableView);
			}else
			{
				//AnimateCells(TransactiesTableView);
			}

		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			App.LeftOnSecure = null;

			if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
			{
				RemoveFromParentViewController();


			}

			firstTransactiesBatch = null;

			Console.WriteLine("batch is null");
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);

			// cancel the webservice via token
			if (cancelToken.Token.CanBeCanceled)
				cancelToken.Cancel();

			firstTransactiesBatch = null;


			if (MaintainanceView != null)
			{
				TransactiesTableView.ScrollEnabled = true;
				MaintainanceView.Hide();
			}
		}

		void SetState(bool aankopen)
		{
			if (aankopen)
			{
				TransactiesTableView.Hidden = true;
				AankopenTableView.Hidden = false;

				// Analytics Screenname
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Aankopen");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			}
			if (!aankopen)
			{
				TransactiesTableView.Hidden = false;
				AankopenTableView.Hidden = true;

				// Analytics Screenname
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Transacties");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

			}
		}

		void SetAnalytics()
		{
			if (Aankopen == true)
			{
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Transacties: aankopen");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
			}
			else {
				Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Transacties: transacties");
				Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());
			}
		}



		void InternetChanged(object sender, EventArgs ea)
		{
			Internet = Reachability.InternetConnectionStatus();

			if (Internet != NetworkStatus.NotReachable)
			{
				if (NetworkView != null)
				{
					NetworkView.RemoveFromSuperview();
				}

				//RetrieveAanbod();
			}
		}

		private void Initialize()
		{
			//HideBackButton();

			//UserAvatar.Layer.CornerRadius = UserAvatar.Layer.Bounds.Height / 2;
			TransactiesTableView.RowHeight = 60f;
			AankopenTableView.RowHeight = 60f;
		}

		private void HideBackButton()
		{
			NavigationItem.SetHidesBackButton(true, false);
		}


		private TableSection[] GetTableItems(Transactie[] transacties)
		{

			// Define lists for the Sections and Rows
			List<TableSection> Sections = new List<TableSection>();
			List<TableRow>[] Rows = new List<TableRow>[transacties.Length];

			// Section id starts with 0
			int SectionId = 0;

			for (int i = 0; i < transacties.Length; i++)
			{
				if (i > 0)
				{
					if (AMMain.DateToTime(transacties[i].Date).ToString("yyyy-MM-dd") != AMMain.DateToTime(transacties[i - 1].Date).ToString("yyyy-MM-dd"))
					{
						SectionId++;
					}
				}

				// if Section Rows don't exist, create them
				if (Rows[SectionId] == null)
				{
					Rows[SectionId] = new List<TableRow>();
				}
				string title = "";
                string detail = "";
				Console.WriteLine("transacties description "+ transacties[i].Title + " " +transacties[i].Initials + " " + transacties[i].Prefix + " " + transacties[i].LastNameMember);
				if (transacties[i].Title == "Overboeking")
				{
					if (!string.IsNullOrEmpty(transacties[i].Prefix))
					{
						title = transacties[i].Initials + " " + transacties[i].Prefix + " " + transacties[i].LastNameMember;
					}
					else
					{
						title = transacties[i].Initials + " " + transacties[i].LastNameMember;
					}
				}
				else if (transacties[i].Title == "Samen Sparen Donatie")
				{
					if (!string.IsNullOrEmpty(transacties[i].NameAccount))
					{
						title = transacties[i].NameAccount;
					}
					else
					{
						title = "";
					}
				}
				else if (!string.IsNullOrEmpty(transacties[i].NameCharity))
				{
					title = transacties[i].NameCharity;
				}
				else if (!string.IsNullOrEmpty(transacties[i].Organisation))
				{
					title = transacties[i].Organisation;
				}
                if (!string.IsNullOrEmpty(transacties[i].Title)) {
                    detail = transacties[i].Title;
                }

				// Add the Table Rows
				Rows[SectionId].Add(new TableRow()
				{
					Title = title,
					Detail = detail,
					Subdetail = SharedMain.SetBalanceFormat(transacties[i].Points),
					Hiddeninfo = SharedDatetime.ConvertFromUnixTimestamp(transacties[i].Date).ToString("D", AMMain.Cultureinfo),
					CellIdentifier = CellIdentifier.DetailAmountTableViewCell
				});
					
			}

			if (SectionId > 0)
			{
				for (int i = 0; i < SectionId+1; i++)
				{
					Console.WriteLine("sections added" + Rows[i].Count);
					Sections.Add(new TableSection()
					{
						Title = FirstCharToUpper(Rows[i][0].Hiddeninfo),
						Items = Rows[i].ToArray()
					});
				}
			}
			else {
				if (Rows.Length > 0)
				{
					Sections.Add(new TableSection()
					{
						Title = FirstCharToUpper(Rows[0][0].Hiddeninfo),
						Items = Rows[0].ToArray()
					});
				}
			}

			// Return the Sections as Rows as an array
			return Sections.ToArray();
		}

		public async void UpdateTransacties(int LowId, int HighId, bool loading)
		{
			//InitLogin();
			if (NetworkView != null)
			{
				NetworkView.Hide();
			}
			if (TransactiesOverlay != null)
			{
				TransactiesOverlay.Hide();
			}

			if (loading)
			{
				Console.WriteLine("ADD LOADING");

				TransactiesTableView.ScrollEnabled = false;
				SetLoader(AMStrings.LoadingTransacties);
			}

			try
			{

				Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					TransactiesService TransactiesService = new TransactiesService();
					TransactiesRoot CurrentTransacties = await TransactiesService.GetTransacties(LowId, HighId);

					//if (!string.IsNullOrEmpty(CurrentTransacties.TokenApp.TokenApp))
					//{
					//	AMMain.UpdateTokenApp(CurrentTransacties.TokenApp.TokenApp);
					//}

					if (loading && LoadingView != null)
					{
						Console.WriteLine("REMOVE LOADING");
						TransactiesTableView.ScrollEnabled = true;
						LoadingView.Hide();
					}
					Console.WriteLine("transacties error: " + CurrentTransacties.Error[0].Code);
					// if there is a response
					if (CurrentTransacties.Error != null)
					{
						if (CurrentTransacties.Error[0].Code == "E00")
						{
							Console.WriteLine("updating transacties");

							if (firstTransactiesBatch != null)
							{
								Transactie[] combined = new Transactie[firstTransactiesBatch.Length + CurrentTransacties.Response[0].Transacties.Length];
								Array.Copy(firstTransactiesBatch, combined, firstTransactiesBatch.Length);
								Array.Copy(CurrentTransacties.Response[0].Transacties, 0, combined, firstTransactiesBatch.Length, CurrentTransacties.Response[0].Transacties.Length);
								TransactiesTableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems(combined));
								NSUserDefaults.StandardUserDefaults.SetString(JsonConvert.SerializeObject(combined), AMLocalStorage.SavedTransacties);
								TransactiesTableView.ReloadData();
								firstTransactiesBatch = combined;
								hitBottom = false;
								TransactiesTableView.AddSubview(RefreshControl);
								RefreshControl.EndRefreshing();
								if (SpinnerOn)
								{
									TransactiesTableView.TableFooterView = null;
								}
							}
							else
							{
								TransactiesTableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems(CurrentTransacties.Response[0].Transacties));
								firstTransactiesBatch = CurrentTransacties.Response[0].Transacties;
								NSUserDefaults.StandardUserDefaults.SetString(JsonConvert.SerializeObject(firstTransactiesBatch), AMLocalStorage.SavedTransacties);
								TransactiesTableView.ReloadData();
								TransactiesTableView.SetContentOffset(new CGPoint(0, -64), true);
								TransactiesTableView.AddSubview(RefreshControl);
								RefreshControl.EndRefreshing();
								if (CurrentTransacties.Response[0].Transacties.Length == 0)
								{
									SetBackupView("Je hebt nog geen transacties.");
								}
								LoadingView.Hide();
							}
						}
						else
						{
							if (CurrentTransacties.Error[0].Code == "E23")
							{
								Console.WriteLine("E23 ");

								// Logout
								LocalStorage.Logout(TabBarController);

								// Redirect to login page
								LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
								Redirect.Routing = "MijnViewController";
								NavigationController.PushViewController(Redirect, false);
								//NavigationController.ViewControllers = new[] { Redirect };

								AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentTransacties.Error[0].Message);
							}
						}
					}
					else
					{
						SetMaintanaceView();
					}
				}else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.InternetNoConnectionMessage);
					if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SavedTransacties)))
					{
						TransactiesTableView.Source = new DetailButtonSectionHeaderTableViewSource(this, GetTableItems(JsonConvert.DeserializeObject<Transactie[]>(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SavedTransacties))));
						TransactiesTableView.ReloadData();
						hitBottom = false;
						TransactiesTableView.AddSubview(RefreshControl);
						RefreshControl.EndRefreshing();
						if (SpinnerOn)
						{
							TransactiesTableView.TableFooterView = null;
						}
					}
					else
					{
						SetBackupView(AMStrings.InternetNoConnectionTitle);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("exception transacties "+ex);
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Transacties", "Exception: "+ ex}
                });
				SetMaintanaceView();
				if (NetworkView != null)
				{
					NetworkView.Hide();
				}
				if (TransactiesOverlay != null)
				{
					TransactiesOverlay.Hide();
				}
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		//begin aankopen inladen

		private TableSection[] GetTableItemsAankopen(Aankoop[] aankopen)
		{

			// Define lists for the Sections and Row
			List<TableSection> Sections = new List<TableSection>();
			List<TableRow>[] Rows = new List<TableRow>[aankopen.Length];

			string totalstring = "";
			// Section id starts with 
			int SectionId = 0;

			for (int i = 0; i < aankopen.Length; i++)
			{
				if (i > 0)
				{
					if (AMMain.DateToTime(aankopen[i].Date).ToString("yyyy-MM-dd") != AMMain.DateToTime(aankopen[i - 1].Date).ToString("yyyy-MM-dd"))
					{
						SectionId++;
					}
				}

				// if Section Rows don't exist, create the
				if (Rows[SectionId] == null)
				{
					Rows[SectionId] = new List<TableRow>();
				}
				if (aankopen[i].TotalString.Contains(" en ") && aankopen[i].TotalString.Contains(",00"))
				{
					string SubString = aankopen[i].TotalString.Replace(" en ", " + ");
					totalstring = SubString.Replace(",00", ",-");
				}
				if (aankopen[i].TotalString.Contains(" en ") && !aankopen[i].TotalString.Contains(",00"))
				{
					totalstring = aankopen[i].TotalString.Replace(" en ", " + ");
				}
				else
				{
					totalstring = aankopen[i].TotalString;
				}
				// Add the Table Row
				Rows[SectionId].Add(new TableRow()
				{
					Index = i,
					Title = aankopen[i].Title,
					Detail = totalstring,
					Description = aankopen[i].Description,
					Hiddeninfo = SharedDatetime.ConvertFromUnixTimestamp(aankopen[i].Date).ToString("D", AMMain.Cultureinfo),
					Segue = "VoucherSegue",
					CellIdentifier = CellIdentifier.DetailButtonTableViewCell
				});
			}

			if (SectionId > 0)
			{
				for (int i = 0; i < SectionId+1; i++)
				{
					Console.WriteLine("sections added" + Rows[i].Count);
					Sections.Add(new TableSection()
					{
						Title = "Geldig t/m "+ FirstCharAlsoLower(Rows[i][0].Hiddeninfo),
						Items = Rows[i].ToArray()
					});
				}
			}
			else {
				if (Rows.Length > 0)
				{
					Sections.Add(new TableSection()
					{
						Title = "Geldig t/m " + FirstCharAlsoLower(Rows[0][0].Hiddeninfo),
						Items = Rows[0].ToArray()
					});
				}
			}

			// Return the Sections as Rows as an arra
			return Sections.ToArray();
		}


		public async void UpdateAankopen()
		{
			SetLoader(AMStrings.LoadingAankopen);
			try
			{
				Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					AankopenService AankopenService = new AankopenService();
					AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();
					Console.WriteLine("transacties error: " + CurrentAankopen.Error[0].Code);
					// if there is a respons
					if (CurrentAankopen.Error != null)
					{
						if (CurrentAankopen.Error[0].Code == "E00")
						{
							Console.WriteLine("updating aankopen");

							AankopenTableView.Source = new AankoopSectionHeaderTableViewSource(this, GetTableItemsAankopen(CurrentAankopen.Aankopen));
							firstAankopenBatch = CurrentAankopen.Aankopen;
							NSUserDefaults.StandardUserDefaults.SetString(JsonConvert.SerializeObject(firstAankopenBatch), AMLocalStorage.SavedAankopen);
							Console.WriteLine("current aankopen " + JsonConvert.SerializeObject(CurrentAankopen));
							AankopenTableView.ReloadData();
							AankopenTableView.SetContentOffset(new CGPoint(0, -64), true);
							AankopenTableView.AddSubview(RefreshControl);
							if (CurrentAankopen.Aankopen.Length == 0)
							{
								SetBackupView("Je hebt nog geen aankopen.");
							}
							LoadingView.Hide();
							RefreshControl.EndRefreshing();
						}
						else
						{
							if (CurrentAankopen.Error[0].Code == "E23")
							{
								Console.WriteLine("E23 ");

								// Logou
								LocalStorage.Logout(TabBarController);

								// Redirect to login pag
								LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
								Redirect.Routing = "TransactieAankoopViewController";
								NavigationController.PushViewController(Redirect, false);
								//NavigationController.ViewControllers = new[] { Redirect };

								AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentAankopen.Error[0].Message);
							}
						}
					}
					else
					{
						SetMaintanaceView();
					}
				} else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.InternetNoConnectionMessage);
					if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SavedAankopen)))
					{
						AankopenTableView.Source = new AankoopSectionHeaderTableViewSource(this, GetTableItemsAankopen(JsonConvert.DeserializeObject<Aankoop[]>(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SavedAankopen))));
						AankopenTableView.ReloadData();
						AankopenTableView.SetContentOffset(new CGPoint(0, -64), true);
						AankopenTableView.AddSubview(RefreshControl);
					}
					else
					{
						SetBackupView(AMStrings.InternetNoConnectionTitle);
					}
				}
			}
			catch (Exception ex)
			{
				SetMaintanaceView();
                Crashes.TrackError(ex);
                    Analytics.TrackEvent("Exception", new Dictionary<string, string> {
                    {"Aankopen", "Exception: "+ ex}
                });
				if (NetworkView != null)
				{
					NetworkView.Hide();
				}
				if (TransactiesOverlay != null)
				{
					TransactiesOverlay.Hide();
				}
				if (LoadingView != null)
				{
					LoadingView.Hide();
				}
			}
			if (LoadingView != null)
			{
				LoadingView.Hide();
			}
		}

		//eind aankopen inladen

		//aankopen link naar voucher viewcontroller


		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			var navctlr = segue.DestinationViewController as Voucherviewcontroller;

			if (segue.Identifier == "VoucherSeguer")
			{
				Console.WriteLine("sender = "+ sender);
                NSIndexPath senderPath = sender as NSIndexPath;
                Console.WriteLine("senderpath "+senderPath);


                    nint rowNumber = 0;
                    for (int i = 0; i < senderPath.Section; i++)
                    {
                        rowNumber += AankopenTableView.NumberOfRowsInSection(i);
                    }
                    rowNumber += senderPath.Row;

                    Console.WriteLine("Index in transactie :" + rowNumber);
                    navctlr.SelectedVoucher = IndexVoucher;

                if (firstAankopenBatch != null)
                {
                    navctlr.LoadedVouchers = firstAankopenBatch;
                }
                else if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SavedAankopen)))
                {
                    navctlr.LoadedVouchers = JsonConvert.DeserializeObject<Aankoop[]>(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SavedAankopen));
                }
                

			}
		}

		// eind aankopen link naar voucher viewcontroller


		public void SetScrollOffset(UIScrollView scrollView, nfloat currentOffset)
		{

			nfloat maximumOffset = (scrollView.ContentSize.Height) - scrollView.Frame.Size.Height;

			if (maximumOffset - currentOffset <= 100)
			{

				if (!hitBottom)
				{
					Internet = Reachability.InternetConnectionStatus();

					if (Internet != NetworkStatus.NotReachable)
					{
						if (!Aankopen)
						{
							hitBottom = true;
							SpinnerOn = true;
							if (firstTransactiesBatch != null)
							{
								UpdateTransacties(firstTransactiesBatch.Length + 1, firstTransactiesBatch.Length + 20, false);
							}
							else {
								UpdateTransacties(0, 20, true);
							}

							//Console.WriteLine("hit bottom");
							UIActivityIndicatorView Spinner = new UIActivityIndicatorView();
							Spinner.StartAnimating();
							Spinner.Color = UIColor.FromRGB(0, 125, 195);
							Spinner.Frame = new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 44);
							TransactiesTableView.TableFooterView = Spinner;
						}

					}
					else
					{
						new UIAlertView("Geen internetverbinding", "Helaas! Je hebt momenteel geen verbinding met het internet. Probeer het bij goed bereik nogmaals.", null, "OK", null).Show();
						hitBottom = true;
						Console.WriteLine("geen internet");
					}
				}
			}
		}

		void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			//CGRect overlay = new CGRect(0, NavigationController.NavigationBar.Bounds.Height+UIApplication.SharedApplication.StatusBarFrame.Height + ButtonTabView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - (NavigationController.NavigationBar.Bounds.Height + ButtonTabView.Bounds.Height+ UIApplication.SharedApplication.StatusBarFrame.Height));
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		void SetBackupView(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;
			TransactiesOverlay = new TransactiesOverlay(bounds, text);
			View.Add(TransactiesOverlay);
		}

		protected void ShowTouchId()
		{

			if (NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID))
			{
				//Lets double check the device supports Touch ID
				if (Context.CanEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, out Error))
				{
					var replyHandler = new LAContextReplyHandler((success, error) =>
						{
							InvokeOnMainThread(() =>
								{
									if (success)
									{
										UpdateTransacties(firstTransactiesBatch.Length + 1, firstTransactiesBatch.Length + 20, true);
									}
									else
									{
									}
								});
						});
					Context.EvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, "Logging in with Touch ID", replyHandler);
				}
				else
				{
					var alert = new UIAlertView("Error", "TouchID not available", null, "BOOO!", null);
					alert.Show();
				}
			}
		}

		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			TransactiesTableView.ScrollEnabled = false;
			View.Add(MaintainanceView);
		}

		void Refresh()
		{
			if(!Aankopen)
			{
				firstTransactiesBatch = null;
				//if (firstTransactiesBatch != null)
				//{
				//	UpdateTransacties(firstTransactiesBatch.Length + 1, firstTransactiesBatch.Length + 20, false);
				//}
				//else
				//{
					UpdateTransacties(0, 20, false);
					var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Transacties", "Pull to refresh", "Pull to refresh", null).Build();
					Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
					Gai.SharedInstance.Dispatch();

				//}
			}
			else
			{
				UpdateAankopen();
				var GAItabkeuze1 = DictionaryBuilder.CreateEvent("Aankopen", "Pull to refresh", "Pull to refresh", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze1);
				Gai.SharedInstance.Dispatch();
			}

		}

		public void PrepareCellsForAnimation(UITableView TableView)
		{
			for (int z = 0; z < TableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < TableView.NumberOfRowsInSection(z); i++)
				{
					DetailAmountTableViewCell Cell = TableView.CellAt(NSIndexPath.FromItemSection(i, z)) as DetailAmountTableViewCell;
					Console.WriteLine("cell numer " + 1);
					Console.WriteLine("cell " + Cell);
					if (Cell != null)
					{
						Cell.Alpha = 0.0f;
					}
				}
			}
		}

		public void AnimateCells(UITableView TableView)
		{
			int count = 0;
			for (int z = 0; z < TableView.NumberOfSections(); z++)
			{
				for (int i = 0; i < TableView.NumberOfRowsInSection(z); i++)
				{
					DetailAmountTableViewCell Cell = TableView.CellAt(NSIndexPath.FromItemSection(i, z)) as DetailAmountTableViewCell;            //Cell.Alpha = 0f;
					if (Cell != null)
					{
						count++;
						Cell.Alpha = 1.0f;
						AMStyle.SlideHorizontaly(Cell, true, count * 0.1, true);
					}
				}
			}
		}

		public string FirstCharToUpper(string str)
		{
			if (str == null)
				return null;

			if (str.Length > 1)
				return char.ToUpper(str[0]) + str.Substring(1);

			return str.ToUpper();
		}

		public string FirstCharAlsoLower(string str)
		{
			if (str == null)
				return null;

			if (str.Length > 1)
				return char.ToLower(str[0]) + str.Substring(1);

			return str.ToUpper();
		}
	}
}
