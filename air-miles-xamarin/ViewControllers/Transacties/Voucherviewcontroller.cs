﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using CoreAnimation;
using Google.Analytics;
using PassKit;
using System.IO;

namespace airmilesxamarin
{
    public partial class Voucherviewcontroller : UIViewController
    {
        // Network status
		NetworkStatus Internet;

		Aankoop CurrentVoucher;
        airmilesxamarin.Aankoop[] firstAankopenBatch;
		LoadingOverlay LoadingView;
		MaintainanceOverlay MaintainanceView;
		public nint SelectedVoucher { get; set;}
		public int AmountVouchers { get; set; }
		public int PositionVoucher;
		public Aankoop[] LoadedVouchers { get; set;}


		public Voucherviewcontroller (IntPtr handle) : base (handle)
        {
        }

		public Voucherviewcontroller()
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (CurrentVoucher != null)
            {
                Title = CurrentVoucher.Title + " voucher";
            }
            // Analytics Screenname
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, "Vouchers");
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());

            //RetrieveAankopen();
            Console.WriteLine("loaded vouhers : " + LoadedVouchers.Length);
            SetVoucherItems(LoadedVouchers);
            VouchersCollectionView.PagingEnabled = true;
            var linelayout = new LineLayout();
            VouchersCollectionView.CollectionViewLayout = linelayout;
            VouchersCollectionView.SetNeedsLayout();
            VouchersCollectionView.SetNeedsDisplay();
        }

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Console.WriteLine("index in voucher" + SelectedVoucher);

		}

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            NSIndexPath indexPath = NSIndexPath.FromItemSection(SelectedVoucher, 0);
            VouchersCollectionView.ScrollEnabled = true;
            CGRect rect = VouchersCollectionView.GetLayoutAttributesForItem(indexPath).Frame;
            VouchersCollectionView.ScrollRectToVisible(rect, false);
        }

		public void SetCurrentVoucher(Aankoop item)
		{
			CurrentVoucher = item;
		}

		public void SetVoucherItems(Aankoop[] aankopen)
		{
			VouchersCollectionView.DataSource = new VoucherCollectionViewSource(this, GetCollectionViewItems(aankopen));
			VouchersCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(aankopen));
			VouchersCollectionView.ReloadData();
			VouchersCollectionView.CollectionViewLayout.InvalidateLayout();
			VouchersCollectionView.CollectionViewLayout.PrepareLayout();

			Console.WriteLine("current aankopen aantal 1: " + aankopen.Length);
		}


		private VoucherTableRow[] GetCollectionViewItems(Aankoop[] aankopen)
		{

			// Define lists for the Row
			List<VoucherTableRow> Rows = new List<VoucherTableRow>();
			AmountVouchers = aankopen.Length;

			if (aankopen != null)
			{
				Console.WriteLine("aantal aankopen" + aankopen.Length);
				for (int i = 0; i < aankopen.Length; i++)
				{
					// if Rows don't exist, create them
					if (Rows == null)
					{
						Rows = new List<VoucherTableRow>();
					}
					// Add the Table Row
					Rows.Add(new VoucherTableRow()
					{
						Index = i,
						Description = aankopen[i].Description,
						Barcode = aankopen[i].VoucherNumber,
						BarcodeType = aankopen[i].BarcodeType,
                        PartnerName = aankopen[i].Title,
                        ValidityDate = aankopen[i].ValidityDate,
						CellIdentifier = CellIdentifier.VoucherCollectionViewCell
					});

				}
				Console.WriteLine("Rows aankopen" + Rows.ToArray());
				// Return the Rows as an arra
				return Rows.ToArray();

			}

			return Rows.ToArray();
		}



		public async void RetrieveAankopen()
		{
			//InitLogin();
			SetLoader(AMStrings.LoadingVoucher);
			try
			{
				Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					AankopenService AankopenService = new AankopenService();

					if (firstAankopenBatch != null)
					{
						AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();


						if (CurrentAankopen.Error != null)
						{
							// if there is a response
							if (CurrentAankopen.Error[0].Code == "E00")
							{
								VouchersCollectionView.DataSource = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
								VouchersCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
								VouchersCollectionView.ReloadData();
								VouchersCollectionView.CollectionViewLayout.InvalidateLayout();
								VouchersCollectionView.CollectionViewLayout.PrepareLayout();
                                NSIndexPath indexPath = NSIndexPath.FromItemSection(SelectedVoucher, 0);
                                VouchersCollectionView.ScrollEnabled = true;
                                CGRect rect = VouchersCollectionView.GetLayoutAttributesForItem(indexPath).Frame;
                                VouchersCollectionView.ScrollRectToVisible(rect, false);
								Console.WriteLine("current aankopen aantal 1: " + CurrentAankopen.Aankopen.Length);



							}
							else
							{

								if (CurrentAankopen.Error[0].Code == "E23")
								{
									Console.WriteLine("E23 ");

									// Logout
									LocalStorage.Logout(TabBarController);

									// Redirect to login page
									LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
									Redirect.Routing = "TransactieAankoopViewController";
									NavigationController.PushViewController(Redirect, false);
									//NavigationController.ViewControllers = new[] { Redirect };

									AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentAankopen.Error[0].Message);
								}
							}
						}
						else
						{
							Console.WriteLine("error");
							SetMaintanaceView();
						}

						LoadingView.Hide();
					}
			
				else {
						try
						{

						
						AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();


					if (CurrentAankopen.Error != null)
					{
						// if there is a respons
						if (CurrentAankopen.Error[0].Code == "E00")
						{
							VouchersCollectionView.DataSource = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
							VouchersCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
							VouchersCollectionView.ReloadData();
							VouchersCollectionView.CollectionViewLayout.InvalidateLayout();
							VouchersCollectionView.CollectionViewLayout.PrepareLayout();
                            NSIndexPath indexPath = NSIndexPath.FromItemSection(SelectedVoucher, 0);
                            VouchersCollectionView.ScrollEnabled = true;
                            CGRect rect = VouchersCollectionView.GetLayoutAttributesForItem(indexPath).Frame;
                            VouchersCollectionView.ScrollRectToVisible(rect, false);
							Console.WriteLine("current aankopen aantal 2: " + CurrentAankopen.Aankopen.Length);

						}
						else {

							if (CurrentAankopen.Error[0].Code == "E23")
							{
								Console.WriteLine("E23 ");

								// Logou
								LocalStorage.Logout(TabBarController);

								// Redirect to login pag
								LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
								Redirect.Routing = "TransactieAankoopViewController";
								NavigationController.PushViewController(Redirect, false);
								//NavigationController.ViewControllers = new[] { Redirect };

								AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, CurrentAankopen.Error[0].Message);
							}
						}
						}
						else {
							Console.WriteLine("error");
							SetMaintanaceView();
							}
							}
						catch (Exception ex)
						{
							SetMaintanaceView();
						}

					LoadingView.Hide();

				}


				}
				else
				{
				AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				}
			}
			catch (Exception ex)
			{
				SetMaintanaceView();
			}
		}

		public async void UpdateAankopen()
		{
			//InitLogin();

			SetLoader(AMStrings.LoadingTransacties);
			try
			{
				Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					AankopenService AankopenService = new AankopenService();
					AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();

					// if there is a respons
					if (CurrentAankopen.Error != null)
					{
						if (CurrentAankopen.Error[0].Code == "E00")
						{
							Console.WriteLine("updating transacties");

							if (firstAankopenBatch != null)
							{
								Aankoop[] combined = new Aankoop[firstAankopenBatch.Length + CurrentAankopen.Aankopen.Length];
								Array.Copy(firstAankopenBatch, combined, firstAankopenBatch.Length);
								Array.Copy(CurrentAankopen.Aankopen, 0, combined, firstAankopenBatch.Length, CurrentAankopen.Aankopen.Length);

								VouchersCollectionView.DataSource = new VoucherCollectionViewSource(this, GetCollectionViewItems(combined));
								VouchersCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(combined));
								VouchersCollectionView.ReloadData();
								firstAankopenBatch = combined;

							}
							else
							{

								VouchersCollectionView.DataSource = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
								VouchersCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(CurrentAankopen.Aankopen));

								firstAankopenBatch = CurrentAankopen.Aankopen;
								VouchersCollectionView.ReloadData();
							}
						}
					}
					else
					{
						SetMaintanaceView();
					}
				}else
				{
					AMAlerts.DefaultAlert(this, AMStrings.ErrorTitle, AMStrings.ErrorNoConnectionMessage);
				}
			}
			catch (Exception ex)
			{
				SetMaintanaceView();
			}
			LoadingView.Hide();
		}


		private void SetMaintanaceView()
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			MaintainanceView = new MaintainanceOverlay(bounds);
			VouchersCollectionView.ScrollEnabled = false;
			View.Add(MaintainanceView);
		}

		void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			//CGRect overlay = new CGRect(0, NavigationController.NavigationBar.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height + ButtonTabView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - (NavigationController.NavigationBar.Bounds.Height + ButtonTabView.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height));
			LoadingView = new LoadingOverlay(bounds, text);
			View.Add(LoadingView);
		}

		public class VoucherFlowDelegate : UICollectionViewDelegateFlowLayout
		{
			UIViewController Controller;
			VoucherTableRow[] Rows;

			public VoucherFlowDelegate(UIViewController parent, VoucherTableRow[] items) : base()
			{
				Controller = parent;
				Rows = items;
			}

			public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
			{
				nfloat ScreenWidth = collectionView.Bounds.Width;

				nfloat ScreenHeight = collectionView.Bounds.Height - 120;


				return new CGSize(ScreenWidth , ScreenHeight);
			}

			public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
			{
				
			}

			public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
			{
				return false;
			}

			public override void WillDisplayCell(UICollectionView collectionView, UICollectionViewCell cell, NSIndexPath indexPath)
			{
				cell.LayoutSubviews();
			}

		}

		public class LineLayout : UICollectionViewFlowLayout
		{
			public static nfloat ITEM_SIZE = UIScreen.MainScreen.Bounds.Width;
			public static nfloat ACTIVE_DISTANCE = 200;
			public const float ZOOM_FACTOR = 1.0f;

			public LineLayout()
			{
				ItemSize = new CGSize(ITEM_SIZE, ITEM_SIZE);
				ScrollDirection = UICollectionViewScrollDirection.Horizontal;
				SectionInset = new UIEdgeInsets(0, 0, 0, 0);
				MinimumLineSpacing = 0.0f;
			}

			public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
			{
				return true;
			}

			public override void PrepareLayout()
			{
				base.PrepareLayout();
			}

			public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect)
			{
				var array = base.LayoutAttributesForElementsInRect(rect);
				var visibleRect = new CGRect(CollectionView.ContentOffset, CollectionView.Bounds.Size);

				foreach (var attributes in array)
				{
					if (attributes.Frame.IntersectsWith(rect))
					{
						float distance = (float)(visibleRect.GetMidX() - attributes.Center.X);
						float normalizedDistance = distance / (float)ACTIVE_DISTANCE;
						if (Math.Abs(distance) < ACTIVE_DISTANCE)
						{
							float zoom = 0 + ZOOM_FACTOR * (1 - Math.Abs(normalizedDistance));
							attributes.Transform3D = CATransform3D.MakeScale(zoom, zoom, 1.0f);
							attributes.ZIndex = 1;
						}
					}
				}
				return array;
			}

			public override CGPoint TargetContentOffset(CGPoint proposedContentOffset, CGPoint scrollingVelocity)
			{
				float offSetAdjustment = float.MaxValue;
				float horizontalCenter = (float)(proposedContentOffset.X + (this.CollectionView.Bounds.Size.Width / 2.0));
				CGRect targetRect = new CGRect(proposedContentOffset.X, 0.0f, this.CollectionView.Bounds.Size.Width, this.CollectionView.Bounds.Size.Height);
				var array = base.LayoutAttributesForElementsInRect(targetRect);
				foreach (var layoutAttributes in array)
				{
					float itemHorizontalCenter = (float)layoutAttributes.Center.X;
					if (Math.Abs(itemHorizontalCenter - horizontalCenter) < Math.Abs(offSetAdjustment))
					{
						offSetAdjustment = itemHorizontalCenter - horizontalCenter;
					}
				}
				return new CGPoint(proposedContentOffset.X + offSetAdjustment, proposedContentOffset.Y);
			}

		}

		void OpenPassbookViewController()
		{
			if (PKPassLibrary.IsAvailable)
			{
				var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Documents folder
				var libRary = Path.Combine(documentsPath, "..", "Library");
				var newFilePath = Path.Combine(libRary, "ExamplePass.pkpass");
				var builtInPassPath = Path.Combine(System.Environment.CurrentDirectory, "Lollipop2.pkpass");
				if (!System.IO.File.Exists(newFilePath))
					System.IO.File.Copy(builtInPassPath, newFilePath);

				NSData nsdata;
				using (FileStream oStream = File.Open(newFilePath, FileMode.Open))
				{
					nsdata = NSData.FromStream(oStream);
				}

				var err = new NSError(new NSString("42"), -42);
				var newPass = new PKPass(nsdata, out err);

				var pkapvc = new PKAddPassesViewController(newPass);
				NavigationController.PresentModalViewController(pkapvc, true);
			}
		}

	}

}
