﻿using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	public class SectionHeaderProfile : UIView
	{
		UILabel Title;
		CGRect TitleFrame;

		public SectionHeaderProfile (string text, nint section)
		{
			if (section == 0)
			{
				TitleFrame = new CGRect(15, 25, 300, 30);
			}
			else {
				TitleFrame = new CGRect(15, 0, 300, 35);
			}
			Title = new UILabel (TitleFrame);
			Title.Text = text.ToUpper();
			Title.Font = UIFont.FromName ("Avenir-Black", 12f);
			Title.TextColor = UIColor.FromRGB(0,125,195);

	
			AddSubview (Title);
		}
	} 
}

