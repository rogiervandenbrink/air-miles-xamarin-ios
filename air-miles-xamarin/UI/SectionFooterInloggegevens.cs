﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using Google.Analytics;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
	public class SectionFooterInloggegevens : UIView
	{
		UIButton Button;
		UILabel Paragraph;

		public SectionFooterInloggegevens()
		{
			UserInteractionEnabled = true;

			Paragraph = new UILabel(new CGRect(15, 10, 280, 60));
			Paragraph.Text = "Het is niet mogelijk om je inlogggegevens aan te passen via de app. Wil je deze toch wijzigen, ";

			Button = new UIButton(new CGRect(245, 33, 60, 30));
			Button.Font = UIFont.FromName("Avenir-Light", 12f);
			Button.SetTitle("klik hier.", UIControlState.Normal);
			Button.SetTitleColor(UIColor.FromRGB(196, 0, 112), UIControlState.Normal);

			UIStringAttributes stringAttributes = new UIStringAttributes
			{
				Font = UIFont.FromName("Avenir-Light", 12f),
				ForegroundColor = UIColor.FromRGB(135, 180, 200),
				ParagraphStyle = new NSMutableParagraphStyle() { LineSpacing = 1.0f }
			};

			var AttributedText = new NSMutableAttributedString(Paragraph.Text);
			AttributedText.AddAttributes(stringAttributes, new NSRange(0, Paragraph.Text.Length));

			Paragraph.AttributedText = AttributedText;
			Paragraph.Lines = 3;

			AddSubviews(Paragraph, Button);

			Button.TouchUpInside += delegate
			{
                UIApplication.SharedApplication.OpenUrl(new NSUrl(SharedWebUrls.URLMijnprofiel));
                //AMMain.OpenURLInApp(this, AMClient.URLMijnprofiel);

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Login", "Open website", "Inloggegevens vergeten", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			};
		}
	}
}