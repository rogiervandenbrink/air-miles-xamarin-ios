﻿using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	public class SectionHeaderDetailBelow : UIView
	{
		UILabel Title;
		CGRect TitleFrame;
		UILabel Detail;
		CGRect DetailFrame;

		public SectionHeaderDetailBelow(string text, string detail, nint section)
		{
			if (section == 0)
			{
				TitleFrame = new CGRect(15, 12, 300, 30);
			}
			else {
				TitleFrame = new CGRect(15, -16, 300, 30);
			}
			Title = new UILabel(TitleFrame);
			Title.Text = text.ToUpper();
			Title.Font = UIFont.FromName("Avenir-Black", 11f);
			Title.TextColor = UIColor.FromRGB(0, 125, 195);
			if (section == 0)
			{
				DetailFrame = new CGRect(15, 47, 300, 30);
			}
			else {
				DetailFrame = new CGRect(15, 7, 300, 50);
			}
			Detail = new UILabel(DetailFrame);
			Detail.Text = detail;
			Detail.Font = UIFont.FromName("Avenir-Book", 11f);
			Detail.TextColor = UIColor.FromRGB(0, 125, 195);
			Detail.LineBreakMode = UILineBreakMode.WordWrap;
			Detail.Lines = 0;
			AddSubview(Title);
			AddSubview(Detail);
		}
	} 
}

