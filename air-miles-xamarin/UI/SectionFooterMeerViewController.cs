﻿using System;
using Air_Miles_Xamarin;
using CoreGraphics;
using Foundation;
using UIKit;

namespace airmilesxamarin.UI
{
	public class SectionFooterMeerViewController : UIView
	{
		UILabel Paragraph;
        string version_accp = "Air Miles versie " + NSBundle.MainBundle.InfoDictionary[new NSString("CFBundleVersion")].ToString() + " acceptatie";
		string version_prod = "Air Miles versie " + NSBundle.MainBundle.InfoDictionary[new NSString("CFBundleVersion")].ToString();

		public SectionFooterMeerViewController()
		{
			UserInteractionEnabled = true;

            Paragraph = new UILabel(new CGRect(15, 0, (UIScreen.MainScreen.Bounds.Width - 30), 30));


			string version = version_accp;

			if (!SharedWebUrls.ISACCP)
			{
				version = version_prod;
			}

			Paragraph.Text = version;
            if (SharedWebUrls.ISACCP)
            {
				UIStringAttributes stringAttributes = new UIStringAttributes
				{
					Font = UIFont.FromName("Avenir-Light", 12f),
					ForegroundColor = UIColor.FromRGB(221, 15, 150),
					ParagraphStyle = new NSMutableParagraphStyle() { LineSpacing = 1.0f, Alignment = UITextAlignment.Center }
				};

				var AttributedText = new NSMutableAttributedString(Paragraph.Text);
				AttributedText.AddAttributes(stringAttributes, new NSRange(0, Paragraph.Text.Length));

				Paragraph.AttributedText = AttributedText;

            }else
            {
				UIStringAttributes stringAttributes = new UIStringAttributes
				{
					Font = UIFont.FromName("Avenir-Light", 12f),
					ForegroundColor = UIColor.FromRGB(135, 180, 200),
					ParagraphStyle = new NSMutableParagraphStyle() { LineSpacing = 1.0f, Alignment = UITextAlignment.Center }
				};

				var AttributedText = new NSMutableAttributedString(Paragraph.Text);
				AttributedText.AddAttributes(stringAttributes, new NSRange(0, Paragraph.Text.Length));

				Paragraph.AttributedText = AttributedText;
            }


			AddSubviews(Paragraph);

			
		}
	}
}