﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace airmilesxamarin
{
	public class SectionHeaderAankoopDetail : UIView
	{
		UILabel Title;
		UILabel Detail;

		public SectionHeaderAankoopDetail(string title, string detail, nint section)
		{
			if (section == 0)
			{
				Title = new UILabel(new CGRect(15, 0, 300, 35))
				{
					Text = title,
					Font = UIFont.FromName("Avenir-Heavy", 10f),
					TextColor = UIColor.FromRGB(0, 125, 195)
				};
				Detail = new UILabel(new CGRect(68, 0, 300, 35))
				{
					Text = detail,
					Font = UIFont.FromName("Avenir-Book", 10f),
					TextColor = UIColor.FromRGB(0, 0, 0)
				};

				BackgroundColor = UIColor.FromRGB(244, 249, 252);

				AddSubview(Title);
				AddSubview(Detail);
			}
			else
			{
				Title = new UILabel(new CGRect(15, 0, 300, 28))
				{
					Text = title,
					Font = UIFont.FromName("Avenir-Heavy", 10f),
					TextColor = UIColor.FromRGB(0, 125, 195)
				};
				Detail = new UILabel(new CGRect(68, 0, 300, 28))
				{
					Text = detail,
					Font = UIFont.FromName("Avenir-Book", 10f),
					TextColor = UIColor.FromRGB(0, 0, 0)
				};

				BackgroundColor = UIColor.FromRGB(244, 249, 252);

				AddSubview(Title);
				AddSubview(Detail);
			}
		}

	} 
}

