﻿using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	public class SectionHeaderMore : UIView
	{
		UILabel Title;
		CGRect TitleFrame;

		public SectionHeaderMore (string text, nint section)
		{
			if (section == 0)
			{
				TitleFrame = new CGRect(15, -13, 300, 30);
			}
			else {
				TitleFrame = new CGRect(15, -13, 300, 30);
			}
			Title = new UILabel(TitleFrame);
			Title.Text = text.ToUpper();
			Title.Font = UIFont.FromName("Avenir-Black", 11f);
			Title.TextColor = UIColor.FromRGB(0, 125, 195);

			AddSubview(Title);
		}
	} 
}

