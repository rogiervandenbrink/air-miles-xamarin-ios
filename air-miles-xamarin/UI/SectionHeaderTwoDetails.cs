﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace airmilesxamarin
{
	public class SectionHeaderTwoDetails : UIView 	{ 		UILabel Title; 		CGRect TitleFrame; 		UILabel ErrorOne;  		public SectionHeaderTwoDetails(string title, string errorone, nint section) 		{  			if (section == 0) 			{ 				TitleFrame = new CGRect(15, 25, 300, 30); 			} 			else { 				TitleFrame = new CGRect(15, 7, 300, 30); 			} 			Title = new UILabel(TitleFrame); 			Title.Text = title.ToUpper(); 			Title.Font = UIFont.FromName("Avenir-Black", 12f); 			Title.TextColor = UIColor.FromRGB(0, 125, 195);


			ErrorOne = new UILabel(new CGRect(15, 0, 270, 110)) 			{ 				Text = errorone, 				Font = UIFont.FromName("Avenir-Book", 12f), 				TextColor = UIColor.FromRGB(255, 0, 0), 				LineBreakMode = UILineBreakMode.WordWrap, 				Lines = 0
			};  			AddSubview(Title); 			AddSubview(ErrorOne);

		}  	}
}

