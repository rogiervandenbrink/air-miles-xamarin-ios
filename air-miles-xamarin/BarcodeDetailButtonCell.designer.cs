﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("BarcodeDetailButtonCell")]
    partial class BarcodeDetailButtonCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Barcode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Description { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EANLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton PDFButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Barcode != null) {
                Barcode.Dispose ();
                Barcode = null;
            }

            if (Description != null) {
                Description.Dispose ();
                Description = null;
            }

            if (EANLabel != null) {
                EANLabel.Dispose ();
                EANLabel = null;
            }

            if (PDFButton != null) {
                PDFButton.Dispose ();
                PDFButton = null;
            }
        }
    }
}