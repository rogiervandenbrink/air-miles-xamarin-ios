﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class LinkCardOverviewViewController : UITableViewController
    {
        public LinkCardOverviewViewController (IntPtr handle) : base (handle)
        {
        }

		UIRefreshControl RefreshControl;

		public override void ViewDidLoad()
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
			{
				// UIRefreshControl iOS6
				RefreshControl = new UIRefreshControl();
				RefreshControl.TintColor = UIColor.FromRGB(55, 145, 205);
				RefreshControl.ValueChanged += (sender, e) => { Refresh(); };
				TableView.AddSubview(RefreshControl);
			}
			base.ViewDidLoad();

		}

		public override void ViewWillAppear(bool animated)
		{
			TableView.Source = new DefaultSectionHeaderTableViewSource(this, LinkCardTableItems.Overview());
			base.ViewWillAppear(animated);
		}

		public override bool ShouldPerformSegue(string segueIdentifier, NSObject sender)
		{
			if (segueIdentifier == "SamenSparenSegue")
			{
				SamenSparenSegue();

				return false;
			}
			if (segueIdentifier == "MastercardSegue")
			{
				MastercardSegue();

				return false;
			}
			if (segueIdentifier == "AHSegue")
			{
				AHSegue();

				return false;
			}
			if (segueIdentifier == "EtosSegue")
			{
				EtosSegue();

				return false;
			}
			if (segueIdentifier == "PraxisSegue")
			{
				PraxisSegue();

				return false;
			}

			this.PerformSegue(segueIdentifier, sender);
			return true;
		}

		void Refresh()
		{
			//do refresh
			RefreshControl.EndRefreshing();
		}

		void SamenSparenSegue()
		{
			KaartKoppelenViewController Redirect = App.Storyboard.InstantiateViewController("KaartKoppelenViewController") as KaartKoppelenViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		void MastercardSegue()
		{
			MastercardViewController Redirect = App.Storyboard.InstantiateViewController("MastercardViewController") as MastercardViewController;
			NavigationController.PushViewController(Redirect, false);
		}

		void AHSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(AMClient.URLAHLinkCard));
		}

		void EtosSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(AMClient.URLEtosLinkCard));
		}

		void PraxisSegue()
		{
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.ExternalLink);
			UIApplication.SharedApplication.OpenUrl(new NSUrl(AMClient.URLPraxisLinkCard));
		}
    }
}