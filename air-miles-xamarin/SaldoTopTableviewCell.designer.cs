﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("SaldoTopTableviewCell")]
    partial class SaldoTopTableviewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BalanceTableLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CollectorsTableLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel DatetimeTableLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BalanceTableLabel != null) {
                BalanceTableLabel.Dispose ();
                BalanceTableLabel = null;
            }

            if (CollectorsTableLabel != null) {
                CollectorsTableLabel.Dispose ();
                CollectorsTableLabel = null;
            }

            if (DatetimeTableLabel != null) {
                DatetimeTableLabel.Dispose ();
                DatetimeTableLabel = null;
            }
        }
    }
}