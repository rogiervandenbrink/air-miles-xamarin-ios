﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;

namespace airmilesxamarin
{
	public class KaartService
	{
		public KaartService()
		{
		}

		public class KaartRequest
		{
		}

		public async Task<KaartRoot> GetKaartDataV2(string kaartnummer, CancellationTokenSource cancelToken)
		{

			string response = "";
			string deviceid = UIDevice.CurrentDevice.IdentifierForVendor.AsString();

			response = await SharedWebServices.GetBalance(kaartnummer, deviceid, true);

			Console.WriteLine("kaart response " + response);
			return JsonConvert.DeserializeObject<KaartRoot>(response);
		}

		private static string GetTimestamp(DateTime value)
		{
			return value.ToString("yyyyMMddhhmmss");
		}
	}
}

