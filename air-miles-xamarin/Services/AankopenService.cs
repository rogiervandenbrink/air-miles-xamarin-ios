﻿using System;
using System.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class AankopenService
	{
		public AankopenService()
		{
		}
		public class AankopenRequest
		{
		}

		public async Task<AankopenRoot> GetAankopen()
		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";
            response = await SharedWebServices.GetPurchases(MemberId, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId));

			Console.WriteLine("response aankopen: " + response);
			return JsonConvert.DeserializeObject<AankopenRoot>(response);
						
		}
	
	}
}

