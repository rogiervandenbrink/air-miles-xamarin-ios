﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class TransferService
	{
		public TransferService()
		{
		}

		public async Task<TransferRoot> Transfer(string receiverCardNr, string amount, CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			
			string response = "";
            response = await SharedWebServices.Transfer(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), receiverCardNr, amount, cancellationtoken);

			Console.WriteLine("response transferservice "+response);

			return JsonConvert.DeserializeObject<TransferRoot>(response);

		}
	}
}

