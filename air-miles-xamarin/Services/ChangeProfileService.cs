﻿using System;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;

namespace airmilesxamarin
{
	public class ChangeProfileService
	{
		public ChangeProfileService()
		{
		}

		public class ChangeProfileRequest
		{

		}

		public async Task<GegevensRoot> ChangeProfile(string memberid, string saluation, string firstname, string lastname, string prefix, long birthdate, string email, string emailconfirm, string street, string housenumber, string housenumbersupplement, string city, string postalcode, string phone, string mobile, string country, string initials,bool newsletter, bool profiling)
		{
			//todo housenumber to int

			JsonCreator jc = new JsonCreator();
			jc.AddString("salutation", saluation);
			jc.AddString("firstname", firstname);
			jc.AddString("lastname", lastname);
			jc.AddString("prefix", prefix);
			jc.AddLong("birthdate", birthdate);
			jc.AddString("email", email);
			jc.AddString("emailconfirm", emailconfirm);
			jc.AddString("street", street);
			//jc.AddString("housenumber", housenumber);
			int houseNumberInt;
			bool intParsed = int.TryParse(housenumber, out houseNumberInt);
			if (intParsed)
			{
				jc.AddInt("housenumber", houseNumberInt);
			}else
			{
				jc.AddInt("housenumber", 0);
			}
			jc.AddString("housenumbersupplement", housenumbersupplement);
			jc.AddString("city", city);
			jc.AddString("postalcode", postalcode);
			jc.AddString("telephoneprivate", phone);
			jc.AddString("telephonemobile", mobile);
			jc.AddString("Country", country);
			jc.AddString("initials", initials);
			jc.AddBoolean("optin", newsletter);
            jc.AddBoolean("Profiling", profiling);

			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string Memberid = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

			string response = "";
            response = await SharedWebServices.ChangeProfile(jc, Memberid, tokenapp, Helpers.DeviceId.DeviceIdString(Memberid));
			System.Console.WriteLine("response changeprofile " + response);

			return JsonConvert.DeserializeObject<GegevensRoot>(response);
		}


	}
}
