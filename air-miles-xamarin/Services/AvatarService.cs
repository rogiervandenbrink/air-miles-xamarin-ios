﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class AvatarService
	{
		public AvatarService()
		{
		}

		public class AvatarRequest
		{
			[JsonProperty(PropertyName = "zmid2")]
			public string ZMemberID { get; set; }

			[JsonProperty(PropertyName = "mobilekey")]
			public string Mobilekey { get; set; }

			[JsonProperty(PropertyName = "avatar")]
			public string Avatar { get; set; }

			[JsonProperty(PropertyName = "filename")]
			public string Filename { get; set; }
		}


		public async Task<AvatarRoot> GetAvatar(string tokenapp)
		{

			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

			string response = "";
            response = await SharedWebServices.GetAvatar(MemberId, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId));

			return JsonConvert.DeserializeObject<AvatarRoot>(response);

		}


		public async Task<AvatarRoot> SaveAvatar(string avatar, string filename)
		{

			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";
            response = await SharedWebServices.SaveAvatar(MemberId, avatar, filename, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId));

			return JsonConvert.DeserializeObject<AvatarRoot>(response);

		}
	}
}

