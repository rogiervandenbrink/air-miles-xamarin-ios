﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class SignoutService
	{
		public SignoutService()
		{
		}

		public async Task<SignoutRoot> Signout(CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			
			string response = "";
            response = await SharedWebServices.Unsubscribe(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), cancellationtoken);

			Console.WriteLine("response signout service "+response);

			return JsonConvert.DeserializeObject<SignoutRoot>(response);

		}
	}
}

