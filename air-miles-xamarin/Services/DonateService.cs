﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class DonateService
	{
		public DonateService()
		{
		}

		public async Task<DonationRoot> Donate(string charityName, string amount, CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

			string response = "";
            response = await SharedWebServices.Donate(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), charityName, amount, cancellationtoken);

			Console.WriteLine("response donateservice "+response);

			return JsonConvert.DeserializeObject<DonationRoot>(response);

		}

		public async Task<CharityRoot> GetCharityNames(CancellationTokenSource cancellationtoken)
		{
			string response = "";
			response = await SharedWebServices.GetCharityNames(cancellationtoken);

			Console.WriteLine("response charitynames service " + response);

			return JsonConvert.DeserializeObject<CharityRoot>(response);

		}
	}
}

