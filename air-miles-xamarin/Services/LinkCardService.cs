﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class LinkCardService
	{
		public LinkCardService()
		{
		}

		public async Task<LinkCardsRoot> LinkCardsInfo(CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

			string response = "";
            response = await SharedWebServices.LinkCardsGetInfo(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), cancellationtoken);

			Console.WriteLine("response linkcardsinfo service "+response);

			return JsonConvert.DeserializeObject<LinkCardsRoot>(response);

		}


		public async Task<LinkCardsRoot> LinkCards(string cardnumberToInvite, bool unlinkCard, CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

			string response = "";
            response = await SharedWebServices.LinkCards(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), cardnumberToInvite, unlinkCard, cancellationtoken);

			Console.WriteLine("response Linkcards " + response);

			return JsonConvert.DeserializeObject<LinkCardsRoot>(response);

		}

		public async Task<LinkCardsRoot> LinkCardsAction(bool accept, CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
            string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

			string response = "";
            response = await SharedWebServices.LinkCardsAction(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), accept, cancellationtoken);

			Console.WriteLine("response Linkcards action " + response);

			return JsonConvert.DeserializeObject<LinkCardsRoot>(response);

		}
	}
}

