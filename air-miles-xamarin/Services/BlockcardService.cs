﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class BlockcardService
	{
		public BlockcardService()
		{
		}

		public async Task<BlockcardRoot> BlockCard(CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string fullname = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFullName);
			string cardnumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);
			string zipcode = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPostalCode);
			string housenumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
			string housenumbersupplement = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumberSupplement);
			string street = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserStreet);
			string city = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCity);
			string country = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserCountry);

			string response = "";
            response = await SharedWebServices.BlockCard(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), fullname, cardnumber, zipcode, housenumber, housenumbersupplement, city, country, street, cancellationtoken);

			Console.WriteLine("response blockcardservice "+response);

			return JsonConvert.DeserializeObject<BlockcardRoot>(response);

		}
	}
}

