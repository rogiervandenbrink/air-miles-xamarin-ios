﻿using System;
using System.IO;
using System.Json;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;
using Xamarin;

namespace airmilesxamarin
{
	public class TransactiesService
	{
		public TransactiesService()
		{
		}

		public class TransactiesRequest
		{
		}

		public async Task<TransactiesRoot> GetTransacties(int LowId, int HighId)
		{

			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			
			string response = "";
            response = await SharedWebServices.GetTransactions(MemberId, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId), LowId, HighId);
			Console.WriteLine("response transacties "+response);
			return JsonConvert.DeserializeObject<TransactiesRoot>(response);

		}

	}
}

