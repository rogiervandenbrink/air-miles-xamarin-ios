﻿using System;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class PromotiesService
	{
		public PromotiesService()
		{
		}

		public async Task<AanbodPromoties> GetAanbod(string Detail)
		{
			string response = "";

			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HipppAanbodUrl + Detail);

			// set in localstorage
			NSUserDefaults.StandardUserDefaults.SetString(response, AMLocalStorage.JsonPromotie + "-" + Detail);

			System.Console.WriteLine("response promties "+response);
			return JsonConvert.DeserializeObject<AanbodPromoties>(response);
					
		}
	}
}

