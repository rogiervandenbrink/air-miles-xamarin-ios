﻿using System;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;

namespace airmilesxamarin
{
	public class PostcodeCheckerService
	{
		public PostcodeCheckerService()
		{
		}

		public class PostcodeCheckerRequest
		{
		}

		public async Task<PostcodeCheckerDataRoot> GetPostcodeCheckerData(string postcode, string housenumber)
		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			
			string response = "";
            response = await SharedWebServices.GetAddress(MemberId, tokenapp, postcode, housenumber, Helpers.DeviceId.DeviceIdString(MemberId));

			return JsonConvert.DeserializeObject<PostcodeCheckerDataRoot>(response);
					

		}
	}
}
