﻿using System;
using System.IO;
using System.Json;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;
using Xamarin;

namespace airmilesxamarin
{
	public class WalletService
	{
		public WalletService()
		{
		}

		public async Task<Byte[]> addWallet(string passType,string barcodeMessage, string barcodeFormat, string barcodeMessageEncoding, string latitude, string longitude, string altitude, string relevanttext, string organizationName, string description, string logoText, string foregroundColor, string backgroundColor, string primaryKey, string primaryLabel, string primaryValue, string primaryCurrencyCode, string auxiliaryKey, string auxiliaryLabel, string auxiliaryValue, bool isRelative, string dateStyle, CancellationTokenSource token = null)

		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
            string email = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail);
            string username = "";
            string prefix = "";
            string lastname = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserLastName);
            if(!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName)))
            {
                username = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserFirstName)+ " ";
            }else
            {
				if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation)))
				{
					if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation) == "heer" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation).ToLower() == "male")
					{
						username = "Dhr. ";
					}
					if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation) == "mevrouw" || NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserSalutation).ToLower() == "female")
					{
						username = "Mevr. ";
					}
					else
					{
						username = "";
					}
				}
            }
            if(!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix))){
                prefix = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserPrefix) + " ";
            }else{
                prefix = "";
            }
            string fullname = username + prefix + lastname;
			int formatVersion = 1;
            string passTypeIdentifier = "pass.com.airmiles.test";
            string serialNumber = barcodeMessage;
            string teamIdentifier = "KX96J9T27N";
            string webServiceURL = SharedWebUrls.Mendix;
            string authenticationToken = SharedWebUrls.BalanceCheckerSecret;
            int maxdistance = 500;

			//var response = "";
            var response = await SharedWebServices.addVoucherWallet(MemberId, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId), email, fullname, formatVersion, passType, passTypeIdentifier, serialNumber, teamIdentifier, webServiceURL, authenticationToken, barcodeMessage, barcodeFormat, barcodeMessageEncoding, latitude, longitude, altitude, relevanttext, organizationName, description, logoText, foregroundColor, backgroundColor, primaryKey, primaryLabel, primaryValue, primaryCurrencyCode, auxiliaryKey, auxiliaryLabel, auxiliaryValue, isRelative, dateStyle, maxdistance, token);

			Console.WriteLine("response wallet "+response);
            return response;

		}

	}
}

