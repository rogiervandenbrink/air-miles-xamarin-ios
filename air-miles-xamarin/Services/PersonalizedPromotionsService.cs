﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;

namespace airmilesxamarin
{
	public class PersonalizedPromotionsService
	{
		public PersonalizedPromotionsService()
		{
		}

		public class PersonalizedPromotionRequest
		{
		}

		public class ActivatePersonalizedPromotionRequest
		{
		}


		public async Task<MendixPersonalizedPromotionRoot> GetPersonalizedPromotions()
		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";
            response = await SharedWebServices.GetPersonalPromotions(MemberId, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId));
			Console.WriteLine("response personalized promotions: " + response);
			// set in localstorage
			NSUserDefaults.StandardUserDefaults.SetString(response, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions);

			return JsonConvert.DeserializeObject<MendixPersonalizedPromotionRoot>(response);

		}

		public static async Task<PersonalizedPromotionsRoot> GetPersonalizedPromotionsByIds(string productId)
		{
			Console.WriteLine("Start loading partner gift from " + productId);

			string response = "";
			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HippoProductsUrl + "?productId=" + productId + "&key=" + SharedWebUrls.HippoKey);
			Console.WriteLine("response personalizedpromotionsbyid : " + response);
			// set in localstorage
			NSUserDefaults.StandardUserDefaults.SetString(response, AMLocalStorage.JsonHippoGifts + "-" + productId);

			return JsonConvert.DeserializeObject<PersonalizedPromotionsRoot>(response);

		}

		//activate personalized promotions

		public static async Task<ActivatedPromotionRoot> ActivatePersonalizedPromotion(string campaignId)
		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";
            response = await SharedWebServices.ActivateCampaign(MemberId, tokenapp, campaignId, Helpers.DeviceId.DeviceIdString(MemberId));

			// set in localstorage
			NSUserDefaults.StandardUserDefaults.SetString(response, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);

			return JsonConvert.DeserializeObject<ActivatedPromotionRoot>(response);

		}
	}
}

