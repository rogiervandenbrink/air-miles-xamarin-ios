﻿using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class AccountSignUpService
	{
		public AccountSignUpService()
		{
		}
		public class AccountSignUpRequest
		{
		}

		public async Task<LightAccountRoot> CreateAccount(string username, string password, string passwordconfirm, string birthdate, string email)
		{
			string deviceid = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
			string cardnumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber);

			string response = "";
			response = await SharedWebServices.SignUp(deviceid, username, password, passwordconfirm, cardnumber, email, birthdate);
			System.Console.WriteLine("response create account "+response);
			return JsonConvert.DeserializeObject<LightAccountRoot>(response);

		}

		public async Task<LightAccountRoot> CheckIfBirthdateIsNeeded(string cardnumber)
		{
			string deviceid = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();

			string response = "";
			response = await SharedWebServices.SignUp_CheckCardNumber(deviceid, cardnumber);
			System.Console.WriteLine("response check if birthdate is needed " + response);
			return JsonConvert.DeserializeObject<LightAccountRoot>(response);

		}

		public async Task<LightAccountRoot> CheckBirthdate(string cardnumber, string birthdate)
		{
			string deviceid = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();

			string response = "";
			response = await SharedWebServices.SignUp_CheckBirthdate(deviceid, cardnumber, birthdate);
			System.Console.WriteLine("response check birthdate " + response);
			return JsonConvert.DeserializeObject<LightAccountRoot>(response);

		}

		public async Task<LightAccountRoot> CheckEmail(string cardnumber, string email)
		{
			string deviceid = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();

			string response = "";
			response = await SharedWebServices.SignUp_CheckEmail(deviceid, cardnumber, email);
			System.Console.WriteLine("response check email " + response);
			return JsonConvert.DeserializeObject<LightAccountRoot>(response);

		}
	}
}