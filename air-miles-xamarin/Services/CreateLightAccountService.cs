﻿using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class CreateLightAccountService
	{
		public CreateLightAccountService()
		{
		}
		public class CreateLightAccountRequest
		{
		}

		public async Task<LightAccountRoot> CreateLightAccount(string email, bool optin)
		{
			string partner = "ZAPP";
			string deviceid = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();

			string response = "";
			response = await SharedWebServices.CreateLightAccount(email, optin, deviceid, partner);

			return JsonConvert.DeserializeObject<LightAccountRoot>(response);

		}
	}
}