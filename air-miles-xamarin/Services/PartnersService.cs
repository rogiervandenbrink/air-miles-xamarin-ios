﻿using System;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class PartnersService
	{
		public PartnersService()
		{
		}

		public async Task<PartnersRoot> GetPartners()
		{
			string response = "";

			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HipppPartnersUrl);

			// set in localstorage
			NSUserDefaults.StandardUserDefaults.SetString(response, AMLocalStorage.JsonPartners);

			return JsonConvert.DeserializeObject<PartnersRoot>(response);
					
		}

	}
}

