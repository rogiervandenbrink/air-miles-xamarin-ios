﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;

namespace airmilesxamarin
{
	public class LoginService
	{
		
		public LoginService()
		{
		}

		public class LoginRequest
		{
		}

		public async Task<UserRoot> Login(string username, string password)
		{

			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string deviceid = UIDevice.CurrentDevice.IdentifierForVendor.AsString();

			string OSType = "iOS";
			string AppVersion = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AppVersion);
			string DeviceType = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.DeviceType);
			string OSVersion = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.OSVersion);

			string response = "";
			response = await SharedWebServices.GetLogin(username, password, deviceid, DeviceType, OSType, OSVersion, AppVersion);
			Console.WriteLine("response inloggen " + response);
			return JsonConvert.DeserializeObject<UserRoot>(response);
						
		}

		public async Task<UserRoot> LoginToken(CancellationTokenSource cancelToken)
		{
        
            string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);			
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";
			response = await SharedWebServices.GetLogin(tokenapp, Helpers.DeviceId.DeviceIdString(MemberId), MemberId, cancelToken);
			Console.WriteLine("response inloggen " + response);
			return JsonConvert.DeserializeObject<UserRoot>(response);

		}
	}
}


