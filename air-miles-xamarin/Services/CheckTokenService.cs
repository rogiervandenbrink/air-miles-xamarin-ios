﻿using System;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using UIKit;

namespace airmilesxamarin
{
	public class CheckTokenService
	{
		public CheckTokenService()
		{
		}

		public class CheckTokenRequest
		{
		}

		public async Task<TokenRoot> GetTokenData()
		{
			string response = "";
			string TokenApp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

            response = await SharedWebServices.CheckToken(TokenApp, Helpers.DeviceId.DeviceIdString(ZMemberID), ZMemberID);

			System.Console.WriteLine("response checktoken: " + response);
			return JsonConvert.DeserializeObject<TokenRoot>(response);
					
		}
	}
}

