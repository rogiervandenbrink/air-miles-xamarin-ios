﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Foundation;
using Air_Miles_Xamarin;
using UIKit;

namespace airmilesxamarin
{
	public class GiftsService
	{
		public GiftsService()
		{
		}

		public class GiftRequest
		{
		}

		public class ActivateGiftRequest
		{
		}

		public async Task<GiftsRoot> GetGiftData()
		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";

            response = await SharedWebServices.GetPersonalOffers(MemberId, tokenapp, Helpers.DeviceId.DeviceIdString(MemberId));

			Console.WriteLine("response giftdata: " + response);

			NSUserDefaults.StandardUserDefaults.SetString(response, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);
			return JsonConvert.DeserializeObject<GiftsRoot>(response);

		}


		public static async Task<GiftsOfferPartner> GetGiftsFromPartner(string partner)
		{
			Console.WriteLine("Start loading partner gifts from " + partner);

			string response = "";

			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HipppGiftsUrl + partner);

            if (!string.IsNullOrEmpty(response))
            {
                // set in localstorage
                NSUserDefaults.StandardUserDefaults.SetString(response, AMLocalStorage.JsonHippoGifts + "-" + partner);
            }

			return JsonConvert.DeserializeObject<GiftsOfferPartner>(response);
		}

		public static async Task<GiftRoot> GetGiftsByIds(string productId)
		{
			Console.WriteLine("Start loading partner gift from " + productId);

			string response = "";

			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HipppGiftsUrl + "gifts?productId=" + productId);

			// set in localstorage
			NSUserDefaults.StandardUserDefaults.SetString(response, AMLocalStorage.JsonGift + "-" + productId);
			Console.WriteLine("respnse getgiftsbyid " + response);
			return JsonConvert.DeserializeObject<GiftRoot>(response);

		}

		public static async Task<string> GetMilesGiftsByIds(string productId)
		{
			Console.WriteLine("Start loading partner gift from " + productId);

			string response = "";

			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HipppGiftsUrl + "gifts?productId=" + productId);

			//// set in localstorage
			//NSUserDefaults.StandardUserDefaults.SetString(response, AMLocalStorage.JsonShellGifts);

			return response;

		}

		public static async Task<ActivatedGiftRoot> ActivateGift(string OfferNr, string ItemNr)
		{
			string MemberId = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);

			string response = "";

            response = await SharedWebServices.ActivateGift(MemberId, tokenapp, OfferNr, ItemNr, Helpers.DeviceId.DeviceIdString(MemberId));
			Console.WriteLine("response ActivateGift " + response);
			return JsonConvert.DeserializeObject<ActivatedGiftRoot>(response);

		}
	}
}

