﻿using System;
using System.Json;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;
using Xamarin;

namespace airmilesxamarin
{
	public class MastercardService
	{
		public MastercardService()
		{
		}

		public async Task<GingerKeyRoot> GetGingerKey(CancellationTokenSource cancellationtoken)
		{

			string response = "";
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

            response = await SharedWebServices.RetrieveKeyGP(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), cancellationtoken);

			Console.WriteLine("response Retrieve Key GP service " + response);

			return JsonConvert.DeserializeObject<GingerKeyRoot>(response);

		}

		public async Task<MastercardRoot> PostMastercardNumber(string SetupToken, string CardNumber, CancellationTokenSource cancellationtoken)
		{
			
			string response = "";
			response = await SharedWebServices.GingerPanToken(SetupToken, CardNumber, cancellationtoken);

			Console.WriteLine("response mastercard service "+response);

			return JsonConvert.DeserializeObject<MastercardRoot>(response);

		}

		public async Task<StoreTokenRoot> StoreToken(string truncatedPan, string panToken, CancellationTokenSource cancellationtoken)
		{

			string response = "";
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);

            response = await SharedWebServices.StoreToken(truncatedPan, panToken, ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), cancellationtoken);

			Console.WriteLine("response storetoken service " + response);

			return JsonConvert.DeserializeObject<StoreTokenRoot>(response);

		}

	}
}

