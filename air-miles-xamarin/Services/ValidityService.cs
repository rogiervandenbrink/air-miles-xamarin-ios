﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class ValidityService
	{
		public ValidityService()
		{
		}

		public async Task<ValidityRoot> GetExperitaionAirMiles(CancellationTokenSource cancellationtoken)
		{
			string tokenapp = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SecureTokenApp);
			string ZMemberID = KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			
			string response = "";
            response = await SharedWebServices.Expiration(ZMemberID, tokenapp, Helpers.DeviceId.DeviceIdString(ZMemberID), cancellationtoken);

			Console.WriteLine("response validity service "+response);

			return JsonConvert.DeserializeObject<ValidityRoot>(response);

		}
	}
}

