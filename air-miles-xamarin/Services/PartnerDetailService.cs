﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Xamarin;
using Air_Miles_Xamarin;
using Foundation;

namespace airmilesxamarin
{
	public class PartnerDetailService
	{

		public PartnerDetailService()
		{
		}

		public async Task<PartnerDetail> GetPartnerDetails(string id)
		{
			string response = "";
			response = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Hippo + AMClient.HipppPartnersUrl /*+ "vtwo/"*/ + id);

			Console.WriteLine("response partner detail: "+response);

			var partnersJsonArray = response;
			var partnersJsonObject = partnersJsonArray;

			
				if (partnersJsonArray[0] == '{')
				{
					var rootobject = JsonConvert.DeserializeObject<PartnerDetail>(partnersJsonObject);

					return rootobject;
				}
				else {
					return JsonConvert.DeserializeObject<PartnerDetail>("{}");
				}
				

		}

	}
}


