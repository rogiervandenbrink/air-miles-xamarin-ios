using System;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using HorizontalSwipe.Controls;
using CoreAnimation;

namespace HorizontalSwipe.Steps
{
    public class DiscoverStep : UIViewController, IMultiStepProcessStep
    {

        UIImageView CircleView;
		public override void LoadView()
		{
			View = new UIView();
			View.BackgroundColor = UIColor.FromRGB(0, 125, 195);

			CircleView = new UIImageView();
			CircleView.BackgroundColor = UIColor.FromRGB(0, 132, 212);
			CALayer profileImageCircle = CircleView.Layer;
			profileImageCircle.CornerRadius = (UIScreen.MainScreen.Bounds.Width / 4.0f);
			profileImageCircle.MasksToBounds = true;
			var image = new UIImageView(UIImage.FromBundle("Airmiles_Placeholder@3x.png"));
			var headingText = new UILabel
			{
				Text = "Connect",
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Book", 30f),
				TextAlignment = UITextAlignment.Center
			};

			View.Add(CircleView);
			View.Add(image);
			View.Add(headingText);

			View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();
			View.AddConstraints(

				CircleView.WithSameCenterX(View),
				CircleView.AtTopOf(View, (UIScreen.MainScreen.Bounds.Height / 2.0f) - 125),
				CircleView.Height().EqualTo((UIScreen.MainScreen.Bounds.Width / 2.0f)),
				CircleView.Width().EqualTo((UIScreen.MainScreen.Bounds.Width / 2.0f)),

				image.WithSameCenterX(View),
				image.AtTopOf(View, (UIScreen.MainScreen.Bounds.Height / 2.0f) - 100),
				image.Height().EqualTo((UIScreen.MainScreen.Bounds.Width / 2.0f) - 50),
				image.Width().EqualTo((UIScreen.MainScreen.Bounds.Width / 2.0f) - 50),

				headingText.Below(CircleView, 25),
				headingText.WithSameCenterX(View)
			);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            StepActivated?.Invoke(this, new MultiStepProcessStepEventArgs { Index = StepIndex });
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            StepDeactivated?.Invoke(this, new MultiStepProcessStepEventArgs { Index = StepIndex });
        }

        public int StepIndex { get; set; }
        public event EventHandler<MultiStepProcessStepEventArgs> StepActivated;
        public event EventHandler<MultiStepProcessStepEventArgs> StepDeactivated;
    }

}