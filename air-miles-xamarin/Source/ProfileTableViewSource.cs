﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace airmilesxamarin
{
	public class ProfileTableViewSource : UITableViewSource {

		UIViewController Controller;
		TableSection[] Sections;

		public ProfileTableViewSource (UIViewController parent)
		{
			Controller = parent;

		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 5;

		}

		public override string TitleForHeader (UITableView tableView, nint section)
		{
			return Sections[section].Title;

		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return Sections[section].Items.Length;

		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			
			if (Sections[indexPath.Section].Items[indexPath.Row].Segue != null)
			{
				Controller.ShouldPerformSegue(Sections[indexPath.Section].Items[indexPath.Row].Segue, this);
				Console.WriteLine("indexpath " + Sections[indexPath.Section].Items[indexPath.Row]);
			}

			tableView.DeselectRow(indexPath, true);

		}

		public override UIView GetViewForHeader (UITableView tableView, nint section)
		{
			SectionHeaderDefault Header = new SectionHeaderDefault(Sections[section].Title, section);
			return Header;

		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			if (section == 0)
			{
				return 50;
			}
			else {
				return 25;
			}

		}

		public override nfloat GetHeightForFooter(UITableView tableView, nint section)
		{
			if (!string.IsNullOrEmpty(Sections[section].FooterId))
			{
				if (Sections[section].FooterId == "InloggegevensAccount")
				{
					return 100;
				}

				return 0;
			}

			return 0;
		}

		public override UIView GetViewForFooter(UITableView tableView, nint section)
		{
			if (!string.IsNullOrEmpty(Sections[section].FooterId))
			{
				if (Sections[section].FooterId == "InloggegevensAccount")
				{
					SectionFooterInloggegevens Footer = new SectionFooterInloggegevens();
					return Footer;
				}

				return null;
			}

			return null;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			TableRow Item = Sections[indexPath.Section].Items[indexPath.Row];
			string CellIdentifier = Item.CellIdentifier.ToString();

				return 50;
		}


		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			TableRow Item = Sections [indexPath.Section].Items [indexPath.Row];
			String CellIdentifier = Item.CellIdentifier.ToString ();

			if (CellIdentifier == "DetailButtonTableViewCell") {
				DetailButtonTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as DetailButtonTableViewCell;
				Cell.UpdateCell(Item, Controller, indexPath);

				return Cell;
			}

			if (CellIdentifier == "DisclosureIndicatorTableViewCell") {
				DisclosureIndicatorTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as DisclosureIndicatorTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if(CellIdentifier == "IconDetailRightTableViewCell" || CellIdentifier == "IconDetailRightTableViewCellDisabled"){
				IconDetailRightTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as IconDetailRightTableViewCell;
				Cell.UpdateCell(Item, Controller, CellIdentifier);

				return Cell;
			}

			if (CellIdentifier == "IconTextRightTableViewCell" || CellIdentifier == "IconTextRightTableViewCellDisabled")
			{
				IconTextRightTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconTextRightTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == "GreyIconDetailRightTableViewCell" || CellIdentifier == "GreyIconDetailRightTableViewCell")
			{
				GreyIconDetailRightTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as GreyIconDetailRightTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == "StaticIconSwitchTableViewCell" || CellIdentifier == "StaticIconSwitchTableViewCellDisabled")
			{
				StaticIconSwitchTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as StaticIconSwitchTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == "DatePickerTableViewCell" || CellIdentifier == "DatePickerTableViewCellDisabled")
			{
				DatePickerTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as DatePickerTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == "GenderPickersTableViewCell" || CellIdentifier == "GenderPickersTableViewCellDisabled")
			{
				GenderPickersTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as GenderPickersTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == "CountryPickerTableViewCell" || CellIdentifier == "CountryPickerTableViewCellDisabled")
			{
				CountryPickerTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as CountryPickerTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == "IconSubdetailRightTableViewCell")
			{
				IconSubdetailRightTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconSubdetailRightTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if (CellIdentifier == "IconDisclosureIndicatorTableViewCell") {
				IconDisclosureIndicatorTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as IconDisclosureIndicatorTableViewCell;
				Cell.UpdateCell (Item);

				return Cell;
			}

			if (CellIdentifier == "IconDisclosureIndicatorTableViewCellDisabled")
			{
				IconDisclosureIndicatorTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconDisclosureIndicatorTableViewCell;
				Cell.UpdateCell(Item);
				Cell.DisableIndicator();

				return Cell;
			}

			if(CellIdentifier == "IconSwitchTableViewCell"){
				IconSwitchTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as IconSwitchTableViewCell;
				Cell.UpdateCell(Item, Controller, tableView);

				return Cell;
			}


			return null;
		}


		public bool SetOpslaan(UIViewController controller, bool boolean)
		{
			if (boolean) {
				return true;
			}
			return false;
		}

	}
}

