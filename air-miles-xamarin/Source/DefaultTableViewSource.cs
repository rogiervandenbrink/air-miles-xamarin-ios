﻿using System;
using UIKit;
using Foundation;

namespace airmilesxamarin
{
	public class DefaultTableViewSource : UITableViewSource
	{

		UIViewController Controller;
		TableRow[] Rows;

		public DefaultTableViewSource(UIViewController parent, TableRow[] items)
		{
			Controller = parent;
			Rows = items;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return Rows.Length;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			if (Rows[indexPath.Row].Segue != null)
			{
				Console.WriteLine("has segue " + Rows[indexPath.Row].Segue);
				Controller.PerformSegue(Rows[indexPath.Row].Segue, this);
			}
			else {
				Console.WriteLine("no segue");
				tableView.DeselectRow(indexPath, true);
			}
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 50;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			TableRow Item = Rows[indexPath.Row];
			String CellIdentifier = Item.CellIdentifier.ToString();

			if (CellIdentifier == "DetailButtonTableViewCell")
			{
				DetailButtonTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as DetailButtonTableViewCell;
				Cell.UpdateCell(Item, Controller, indexPath);

				return Cell;
			}

			if (CellIdentifier == "DisclosureIndicatorTableViewCell")
			{
				DisclosureIndicatorTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as DisclosureIndicatorTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if (CellIdentifier == "IconDetailRightTableViewCell" || CellIdentifier == "IconDetailRightTableViewCellDisabled")
			{
				IconDetailRightTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconDetailRightTableViewCell;
				Cell.UpdateCell(Item, Controller, CellIdentifier);

				return Cell;
			}

			if (CellIdentifier == "IconDisclosureIndicatorTableViewCell")
			{
				IconDisclosureIndicatorTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconDisclosureIndicatorTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if (CellIdentifier == "IconTableViewCell")
			{
				IconTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if (CellIdentifier == "IconSwitchTableViewCell")
			{
				IconSwitchTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconSwitchTableViewCell;
				Cell.UpdateCell(Item, Controller, tableView);

				return Cell;
			}

			return null;
		}

		public int GetRealIndex(int row)
		{
			return Rows[row].Index;
		}

	}
}

