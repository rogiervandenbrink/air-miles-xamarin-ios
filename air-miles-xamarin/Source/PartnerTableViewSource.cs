﻿using System;
using UIKit;
using Foundation;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;

namespace airmilesxamarin
{
	public class PartnerTableViewSource : UITableViewSource
	{

		UIViewController Controller;
		TableRow[] Rows;

		Dictionary<string, List<TableRow>> indexedTableItems;
		string[] keys;
		int incerement;

		public PartnerTableViewSource(UIViewController parent, TableRow[] items)
		{
			Controller = parent;
			Rows = items;

			indexedTableItems = new Dictionary<string, List<TableRow>>();

			foreach (var item in items)
			{
				item.SectionIndex = incerement++;

				if (indexedTableItems.ContainsKey(item.Title[0].ToString().ToUpper()))
				{
					indexedTableItems[item.Title[0].ToString().ToUpper()].Add(item);
				}
				else {
					indexedTableItems.Add(item.Title[0].ToString().ToUpper(), new List<TableRow>() { item });
				}
			}
			keys = indexedTableItems.Keys.ToArray();
		}

		public override nint NumberOfSections(UITableView tableView)
		{
			return keys.Length;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return indexedTableItems[keys[section]].Count;
		}

		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			return GetSectionHeader(keys[section]);
		}

		public override string[] SectionIndexTitles(UITableView tableView)
		{
			return keys;
		}

		public override string TitleForHeader(UITableView tableView, nint section)
		{
			return keys[section];
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			if (Rows[indexPath.Row].Segue != null)
			{
				Console.WriteLine("has segue " + Rows[indexPath.Row].Segue);
				Controller.PerformSegue(Rows[indexPath.Row].Segue, this);
			}
			else {
				Console.WriteLine("no segue");
				tableView.DeselectRow(indexPath, true);
			}
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 50;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			TableRow Item = indexedTableItems[keys[indexPath.Section]][indexPath.Row];
			String CellIdentifier = Item.CellIdentifier.ToString();

			if (CellIdentifier == "IconTableViewCell")
			{
				IconTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as IconTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			return null;
		}

		public static UIView GetSectionHeader(string caption)
		{
			UIView view = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width, 20));
			view.BackgroundColor = UIColor.FromRGB(244, 244, 244);

			UILabel label = new UILabel();
			label.BackgroundColor = UIColor.Clear;
			label.Font = UIFont.FromName("Avenir-Black", 17f);
			label.Frame = new CGRect(15, 2, 290, 20);
			label.TextColor = UIColor.FromRGB(200, 200, 200);
			label.Text = caption.ToUpper();

			view.AddSubview(label);

			return view;
		}

		public int GetRealIndex(TableRow[] items, int section, int row)
		{
			indexedTableItems = new Dictionary<string, List<TableRow>>();

			foreach (var item in items)
			{
				item.SectionIndex = incerement++;

				if (indexedTableItems.ContainsKey(item.Title[0].ToString().ToUpper()))
				{
					indexedTableItems[item.Title[0].ToString().ToUpper()].Add(item);
				}
				else {
					indexedTableItems.Add(item.Title[0].ToString().ToUpper(), new List<TableRow>() { item });
				}
			}
			keys = indexedTableItems.Keys.ToArray();

			return indexedTableItems[keys[section]][row].Index;
		}

	}
}

