﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace airmilesxamarin
{
	public class GiftTableViewSource : UITableViewSource
	{

		GiftViewController Controller;
		GiftRow[] Rows;

		public GiftTableViewSource(GiftViewController parent, GiftRow[] items)
		{
			Controller = parent;
			Rows = items;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return Rows.Length;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			GiftRow Item = Rows[indexPath.Row];
			String CellIdentifier = Item.Type.ToString();

			if (CellIdentifier == GiftRowType.GiftIntroCell.ToString() || CellIdentifier == GiftRowType.GiftSmallIntroCell.ToString() || CellIdentifier == GiftRowType.GiftPrintSmallIntroCell.ToString())
			{
				GiftIntroTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as GiftIntroTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if (CellIdentifier == GiftRowType.GiftRowCell.ToString())
			{
				GiftRowTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as GiftRowTableViewCell;
				Cell.UpdateCell(Item);

				return Cell;
			}

			if (CellIdentifier == GiftRowType.GiftFooterCell.ToString())
			{
				GiftFooterTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as GiftFooterTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			if (CellIdentifier == GiftRowType.GiftFooterEmptyCell.ToString())
			{
				GiftFooterTableViewCell Cell = tableView.DequeueReusableCell(CellIdentifier) as GiftFooterTableViewCell;
				Cell.UpdateCell(Item, Controller);

				return Cell;
			}

			return null;
		}

		public override void Scrolled(UIScrollView scrollView)
		{
			Controller.SetScrollOffset(scrollView.ContentOffset.Y);
		}

	}
}

