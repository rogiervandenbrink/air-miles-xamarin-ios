﻿using System;
using Foundation;
using UIKit;

namespace airmilesxamarin
{
	public class VoucherCollectionViewSource : UICollectionViewSource
	{
		UIViewController Controller;
		VoucherTableRow[] Rows;


		public VoucherCollectionViewSource(UIViewController parent, VoucherTableRow[] items)
		{
			Controller = parent;
			Rows = items;
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return Rows.Length;
		}

		public override bool ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
		{
			return false;
		}


		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			VoucherTableRow Item = Rows[indexPath.Row];
			nfloat width = collectionView.Bounds.Width;

			VoucherCollectionViewCell Cell = (VoucherCollectionViewCell)collectionView.DequeueReusableCell("VoucherCollectionViewCell", indexPath);

			Cell.UpdateCell(Item, Controller,indexPath, width);
			Cell.UpdateArrows(Item, Controller, indexPath);
			Cell.UpdateBarcode(Item);
			//Cell.LayoutSubviews(Item, Controller, indexPath);
			Cell.parent = Controller;

			return Cell;
		}
	}
}