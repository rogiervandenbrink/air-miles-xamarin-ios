﻿using System;
using UIKit;
using Foundation;

namespace airmilesxamarin
{
	public class PartnerDetailTableViewSource : UITableViewSource
	{

		PartnerDetailViewController Controller;
		Partner Partner;

		public PartnerDetailTableViewSource(PartnerDetailViewController parent, Partner partner)
		{
			Controller = parent;
			Partner = partner;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return 1;
		}

		public override void Scrolled(UIScrollView scrollView)
		{
			Controller.SetScrollOffset(scrollView.ContentOffset.Y);
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			PartnerDetailCell Cell = tableView.DequeueReusableCell("PartnerDetailCell") as PartnerDetailCell;
			if (Cell != null)
			{
				Cell.UpdateCell(Controller, Partner);
			}

			return Cell;
		}

	}
}

