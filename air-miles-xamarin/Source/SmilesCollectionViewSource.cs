﻿using System;
using CoreGraphics;
using System.Collections.Generic;

using Foundation;
using UIKit;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace airmilesxamarin
{
	public class SmilesCollectionViewSource : UICollectionViewSource
	{
		UIViewController Controller;
		Promotie[] Rows;

		static NSString SmilesCollectionViewHeaderId = new NSString("SmilesCollectionViewHeader");
		static NSString PromotionCollectionViewFooterId = new NSString("PromotionCollectionViewFooter");

		public SmilesCollectionViewSource(UIViewController parent, Promotie[] items)
		{
			Controller = parent;
			Rows = items;

			Console.WriteLine("Rows smiles source "+JsonConvert.SerializeObject(Rows));
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return Rows.Length;
		}

		public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
		{

			if (elementKind == "UICollectionElementKindSectionHeader")
			{
				
				SmilesCollectionViewHeader headerView = (SmilesCollectionViewHeader)collectionView.DequeueReusableSupplementaryView(elementKind, SmilesCollectionViewHeaderId, indexPath);
				Console.WriteLine("header item "+JsonConvert.SerializeObject(Rows[indexPath.Row]));
				string JsonShellPartner = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonHippoGifts + "-SHLL");
				Console.WriteLine("Jsonshellpartner "+JsonShellPartner);
				//Console.WriteLine("Jsonshellgifts "+JsonShellGifts);
				string headerImage = JToken.Parse(JsonShellPartner).Value<string>("partnerProgramLogo");

				string heading = JToken.Parse(JsonShellPartner).Value<string>("chooseGifts");
				try{
					headerView.BackgroundColor = UIColor.White;
					headerView.UpdateHeader(Rows[indexPath.Row], Controller, headerImage, heading);
				}
				catch (Exception ex)
			{
                Console.WriteLine("exception headerview "+ex);
                    Controller.NavigationController.PopViewController(false);
				AMAlerts.DefaultAlert(Controller, "Geen aanbiedingen", "Er zijn momenteel geen aabiedingen beschikbaar.");
			}

				return headerView;
			}
			else if (elementKind == "UICollectionElementKindSectionFooter")
			{
				
				PromotionCollectionViewFooter footerView = (PromotionCollectionViewFooter)collectionView.DequeueReusableSupplementaryView(elementKind, PromotionCollectionViewFooterId, indexPath);
				//footerView.SetImage(Rows[indexPath.Row].Logo);
				//Console.WriteLine("footer img " + Rows[indexPath.Row] + " " + Rows[indexPath.Row].Logo);
				return footerView;
			}
			else {
				return new UICollectionReusableView();
			}
		}

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			Promotie Item = Rows[indexPath.Row];

			SmilesCollectionViewCell Cell = (SmilesCollectionViewCell)collectionView.DequeueReusableCell("SmilesCollectionViewCell", indexPath);
			Cell.Layer.ShouldRasterize = true;
			Cell.Layer.RasterizationScale = UIScreen.MainScreen.Scale;
			Cell.StyleCell();
			Cell.UpdateCell(Item, collectionView, Controller);

			return Cell;
		}
	}
}