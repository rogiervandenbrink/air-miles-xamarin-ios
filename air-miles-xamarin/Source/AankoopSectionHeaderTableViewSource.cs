﻿using System;
using UIKit;
using Foundation;

namespace airmilesxamarin
{
	public class AankoopSectionHeaderTableViewSource : UITableViewSource {

		UIViewController Controller;
		TableSection[] Sections;

		public AankoopSectionHeaderTableViewSource (UIViewController parent, TableSection[] items)
		{
			Controller = parent;
			Sections = items;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			Console.WriteLine("sections " + Sections.Length);

			return Sections.Length;
		}

		public override string TitleForHeader (UITableView tableView, nint section)
		{
			
			return Sections[section].Title;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			Console.WriteLine("rows " + Sections[section].Items.Length);

			return Sections[section].Items.Length;
		}

		public override UIView GetViewForHeader (UITableView tableView, nint section)
		{
			SectionHeaderAankoopDetail Header = new SectionHeaderAankoopDetail(Sections[section].Title, Sections[section].Detail, section);
			return Header;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			TableRow Item = Sections [indexPath.Section].Items [indexPath.Row];
			String CellIdentifier = Item.CellIdentifier.ToString ();

			if(CellIdentifier == "DetailButtonTableViewCell"){
				DetailButtonTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as DetailButtonTableViewCell;
				Cell.UpdateCell(Item, Controller, indexPath);


				return Cell;
			}

			if(CellIdentifier == "DetailAmountTableViewCell"){
				DetailAmountTableViewCell Cell = tableView.DequeueReusableCell (CellIdentifier) as DetailAmountTableViewCell;
				Cell.UpdateCell (Item);

				return Cell;
			}

			return null;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{

			if (Controller.GetType() == typeof(TransactieAankoopViewController))
			{
				TransactieAankoopViewController navcltr = Controller as TransactieAankoopViewController;
				navcltr.SelectedRowAankopen = indexPath;


			}

			tableView.DeselectRow(indexPath, true);
		}



		public override void Scrolled(UIScrollView scrollView)
		{
			if (Controller.GetType() == typeof(TransactiesViewController))
			{
				TransactiesViewController navcltr = Controller as TransactiesViewController;
				navcltr.SetScrollOffset(scrollView, scrollView.ContentOffset.Y);


			}

			if (Controller.GetType() == typeof(TransactieAankoopViewController))
			{
				TransactieAankoopViewController navcltr = Controller as TransactieAankoopViewController;
				navcltr.SetScrollOffset(scrollView, scrollView.ContentOffset.Y);


			}



		}

	}
}

