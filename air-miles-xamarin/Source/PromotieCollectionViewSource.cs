﻿using System;
using CoreGraphics;
using System.Collections.Generic;

using Foundation;
using UIKit;
using Newtonsoft.Json.Linq;

namespace airmilesxamarin
{
	public class PromotieCollectionViewSource : UICollectionViewSource
	{
		UIViewController Controller;
		Promotie[] Rows;

		static NSString PromotionCollectionViewHeaderId = new NSString("PromotionCollectionViewHeader");
		static NSString PromotionCollectionViewFooterId = new NSString("PromotionCollectionViewFooter");

		public PromotieCollectionViewSource(UIViewController parent, Promotie[] items)
		{
			Controller = parent;
			Rows = items;
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return Rows.Length;
		}

		public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
		{
			if (elementKind == "UICollectionElementKindSectionHeader")
			{
				
				PromotionCollectionViewHeader headerView = (PromotionCollectionViewHeader)collectionView.DequeueReusableSupplementaryView(elementKind, PromotionCollectionViewHeaderId, indexPath);
				if (Rows.Length == 1)
				{
					headerView.Text = "1 aanbieding";
				}
				else
				{
					headerView.Text = Rows.Length + " aanbiedingen";
				}
					
				return headerView;

			}

			else if (elementKind == "UICollectionElementKindSectionFooter")
			{
				
				PromotionCollectionViewFooter footerView = (PromotionCollectionViewFooter)collectionView.DequeueReusableSupplementaryView(elementKind, PromotionCollectionViewFooterId, indexPath);
				footerView.SetImage(Rows[indexPath.Row].Logo);
				Console.WriteLine("footer img " + Rows[indexPath.Row] + " " + Rows[indexPath.Row].Logo);
				return footerView;
			}
			else {
				return new UICollectionReusableView();
			}
		}

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			Promotie Item = Rows[indexPath.Row];

			PromotionCollectionViewCell Cell = (PromotionCollectionViewCell)collectionView.DequeueReusableCell("PromotionCollectionViewCell", indexPath);
			Cell.StyleCell();
			Cell.UpdateCell(Item);

			return Cell;
		}
	}
}