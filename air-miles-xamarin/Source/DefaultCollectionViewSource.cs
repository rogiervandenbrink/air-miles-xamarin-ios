﻿using System;
using CoreGraphics;
using System.Collections.Generic;

using Foundation;
using UIKit;

namespace airmilesxamarin
{
	public class DefaultCollectionViewSource : UICollectionViewSource
	{
		UIViewController Controller;
		TableRow[] Rows;

		public DefaultCollectionViewSource (UIViewController parent, TableRow[] items)
		{
			Controller = parent;
			Rows = items;
		}

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			return Rows.Length;
		}

		public TableRow GetItem(int id)
		{
			return Rows[id];
		}

		public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
		{
			PersonalPromotionsCollectionViewHeader headerView = (PersonalPromotionsCollectionViewHeader)collectionView.DequeueReusableSupplementaryView(elementKind, "PersonalPromotionsCollectionViewHeader", indexPath);
			return headerView;
		}

		public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
		{
			TableRow Item = Rows [indexPath.Row];
			string CellIdentifier = Item.CellIdentifier.ToString ();

			if (CellIdentifier == "IconDetailCollectionViewCell") {
				IconDetailCollectionViewCell Cell = (IconDetailCollectionViewCell)collectionView.DequeueReusableCell (CellIdentifier, indexPath);
				Cell.UpdateCell(Item, indexPath.Row, Rows.Length, Controller);
				//Cell.GetAmountPromotions(Item, collectionView, Controller);

				return Cell;
			}

			if (CellIdentifier == "PromotionCollectionViewCell")
			{
				PromotionCollectionViewCell Cell = (PromotionCollectionViewCell)collectionView.DequeueReusableCell(CellIdentifier, indexPath);
				//Cell.UpdateCell(Item);
				//Cell.AddBorder(indexPath.Row, Rows.Length);

				return Cell;
			}

			return null;
		}
	}
}