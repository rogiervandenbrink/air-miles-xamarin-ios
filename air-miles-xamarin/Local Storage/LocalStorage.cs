﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace airmilesxamarin
{
	public class LocalStorage
	{

		public static void LocalLogin(User user, ResponseToken token, string username)
		{
			var HasLoginKey = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin);
			List<string> bounceIndicators = new List<string>();

            if (!string.IsNullOrEmpty(username))
			{
				NSUserDefaults.StandardUserDefaults.SetString(username, AMLocalStorage.Username);
			}
            if (!string.IsNullOrEmpty(user.Initials))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.Initials, AMLocalStorage.UserInitials);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserInitials);
            }
            if (!string.IsNullOrEmpty(user.FirstName))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.FirstName, AMLocalStorage.UserFirstName);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserFirstName);
            }
            if (!string.IsNullOrEmpty(user.Prefix))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.Prefix, AMLocalStorage.UserPrefix);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserPrefix);
            }
            if (!string.IsNullOrEmpty(user.LastName))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.LastName, AMLocalStorage.UserLastName);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserLastName);
            }
            if (!string.IsNullOrEmpty(user.FullName))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.FullName, AMLocalStorage.UserFullName);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserFullName);
            }
            if (!string.IsNullOrEmpty(user.Email))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.Email, AMLocalStorage.UserEmail);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserEmail);
            }
            if (!string.IsNullOrEmpty(user.Salutation ))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.Salutation, AMLocalStorage.UserSalutation);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserSalutation);
            }
			
			if (!string.IsNullOrEmpty(user.BirthDate))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.BirthDate, AMLocalStorage.UserBirthday);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserBirthday);
            }
			if (!string.IsNullOrEmpty(user.EmailBounceReason))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.EmailBounceReason, AMLocalStorage.EmailBounceReason);
				if (!string.IsNullOrEmpty(user.EmailBounceReason))
				{
					bounceIndicators.Add(user.EmailBounceReason);
				}
			}
			else if (string.IsNullOrEmpty(user.EmailBounceReason))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.EmailBounceReason);
				}
			}
            if (!string.IsNullOrEmpty(user.HouseNumber))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.HouseNumber, AMLocalStorage.UserHouseNumber);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserHouseNumber);
            }
            if (!string.IsNullOrEmpty(user.TelephoneNrPrivate))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.TelephoneNrPrivate, AMLocalStorage.UserTelephoneNrPrivate);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserPostalCode);
            }
            if (!string.IsNullOrEmpty(user.PostalCode))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.PostalCode, AMLocalStorage.UserPostalCode);
			}
            else{
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserPostalCode);
            }
            if (!string.IsNullOrEmpty(user.UserStatus))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.UserStatus, AMLocalStorage.UserStatus);
			}
            else
            {
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserStatus);
            }
            if (!string.IsNullOrEmpty(user.RefreshToken))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.RefreshToken, AMLocalStorage.UserRefreshToken);
			}
            if (!string.IsNullOrEmpty(user.ExternalCardNumber))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.ExternalCardNumber, AMLocalStorage.ExternalCardNumber);
			}
            if (!string.IsNullOrEmpty(user.PopUpMessage))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.PopUpMessage, AMLocalStorage.PopUpMessage);
			}
			if (!string.IsNullOrEmpty(user.MobilePhoneBounceReason))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.MobilePhoneBounceReason, AMLocalStorage.MobilePhoneBounceReason);
				if (!string.IsNullOrEmpty(user.MobilePhoneBounceReason))
				{
					bounceIndicators.Add(user.MobilePhoneBounceReason);
				}
			}
			else if (string.IsNullOrEmpty(user.MobilePhoneBounceReason))
			{
				if(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.MobilePhoneBounceReason);
				}
			}
            if (!string.IsNullOrEmpty(user.Street))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.Street, AMLocalStorage.UserStreet);
			}
            else
            {
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserStreet);
            }
            if (!string.IsNullOrEmpty(user.UserNameIR025))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.UserNameIR025, AMLocalStorage.UserNameIR025);
			}
			if (!string.IsNullOrEmpty(user.TelephoneNrMobile))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.TelephoneNrMobile, AMLocalStorage.UserTelephoneNrMobile);
			}
            else
            {
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserTelephoneNrMobile);
            }
            if (!string.IsNullOrEmpty(user.PrivatePhoneBounceReason))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.PrivatePhoneBounceReason, AMLocalStorage.PrivatePhoneBounceReason);
				if (!string.IsNullOrEmpty(user.PrivatePhoneBounceReason))
				{
					bounceIndicators.Add(user.PrivatePhoneBounceReason);
				}
			}
			else if (string.IsNullOrEmpty(user.PrivatePhoneBounceReason))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.PrivatePhoneBounceReason);
				}
			}
            if (!string.IsNullOrEmpty(user.City))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.City, AMLocalStorage.UserCity);
			}else
            {
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserCity);
            }
            if (!string.IsNullOrEmpty(user.Country))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.Country, AMLocalStorage.UserCountry);
			}
            else
            {
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserCountry);
            }
            if (!string.IsNullOrEmpty(user.HouseNumberSupplement))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.HouseNumberSupplement, AMLocalStorage.UserHouseNumberSupplement);
			}
            else
            {
                NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.UserHouseNumberSupplement);
            }
            if (!string.IsNullOrEmpty(user.AddressBounceReason))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.AddressBounceReason, AMLocalStorage.AddressBounceReason);
				if (!string.IsNullOrEmpty(user.AddressBounceReason))
				{
					//bounceIndicators.Add(user.AddressBounceReason);
				}
			}
			else if (string.IsNullOrEmpty(user.AddressBounceReason))
			{
				if (NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason) != null)
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.AddressBounceReason);
				}
			}
            if (!string.IsNullOrEmpty(user.CardNumber))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.CardNumber, AMLocalStorage.CardNumber);
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasCard);
				NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
				shared.SetString(user.CardNumber, AMLocalStorage.CardNumber);
				shared.Synchronize();

			}
            if (user.MobileLoginInfo_LinkCardsMessage != null)
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.MobileLoginInfo_LinkCardsMessage.Message, AMLocalStorage.LinkcardsMessage);
			}
            if (!string.IsNullOrEmpty(user.CCL4))
			{
				NSUserDefaults.StandardUserDefaults.SetString(user.CCL4, AMLocalStorage.TruncatedPan);	
                Console.WriteLine("truncated pan "+ user.CCL4);
            }
			if (string.IsNullOrEmpty(user.CCL4))
			{
				NSUserDefaults.StandardUserDefaults.SetString(string.Empty, AMLocalStorage.TruncatedPan);
			}
			NSUserDefaults.StandardUserDefaults.SetBool(user.AddressBouncePresent, AMLocalStorage.AddressBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(user.OptIn, AMLocalStorage.OptIn);
			NSUserDefaults.StandardUserDefaults.SetBool(user.EmailBouncePresent, AMLocalStorage.EmailBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(user.PrivateNrBouncePresent, AMLocalStorage.PrivateNrBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(user.MobileNrBouncePresent, AMLocalStorage.MobileNrBouncePresent);
			NSUserDefaults.StandardUserDefaults.SetBool(user.ShowPopUp, AMLocalStorage.UserShowPopUp);

            //profiling
            NSUserDefaults.StandardUserDefaults.SetBool(user.Profiling, AMLocalStorage.Profiling);
            Console.WriteLine("profiling bool is "+user.Profiling);

			NSUserDefaults.StandardUserDefaults.SetInt(bounceIndicators.Count, AMLocalStorage.AmountBounceIndicators);
			Console.WriteLine("count bounceindicators "+bounceIndicators.Count);
			Console.WriteLine("address bounce reason "+user.AddressBounceReason);
			Console.WriteLine("address bounce reason localstorage" + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason));
			Console.WriteLine("email bounce reason " + user.EmailBounceReason);
			Console.WriteLine("email bounce reason localstorage" + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason));

			Console.WriteLine("token app : " + token.TokenApp);
			if (!string.IsNullOrEmpty(token.TokenApp))
			{
				NSUserDefaults.StandardUserDefaults.SetString(token.TokenApp, AMLocalStorage.SecureTokenApp);
				//KeychainHelpers.SetPasswordForUsername(username, token.TokenApp, AMLocalStorage.SecureTokenApp, Security.SecAccessible.WhenUnlockedThisDeviceOnly, true);
			}

			NSObject version = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
			string AppVersion = version.ToString();
			string OSversion = UIDevice.CurrentDevice.SystemVersion;
			string DeviceType = UIDevice.CurrentDevice.Model;
			string deviceid = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
			Console.WriteLine("app version " + AppVersion + " OSVersion " + OSversion + " Devicetype " + DeviceType);
			Console.WriteLine("username " + username + " zmemberid " + user.ZMemberID);
			KeychainHelpers.SetPasswordForUsername(username, user.ZMemberID, AMLocalStorage.SecureZMemberId, Security.SecAccessible.WhenUnlockedThisDeviceOnly, true);
			//if (string.IsNullOrEmpty(KeychainHelpers.GetDeviceId(AMLocalStorage.SecureDeviceId, true)))
			//{
			KeychainHelpers.SetDeviceId(deviceid, user.ZMemberID, Security.SecAccessible.WhenUnlockedThisDeviceOnly, true);
			//}
			NSUserDefaults.StandardUserDefaults.SetBool(user.DigitalCard, AMLocalStorage.DigitalAccount);

			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.Session);
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasLogin);
            ResetPinAttempts(); 
			NSUserDefaults.StandardUserDefaults.SetString(AppVersion, AMLocalStorage.AppVersion);
			NSUserDefaults.StandardUserDefaults.SetString(OSversion, AMLocalStorage.OSVersion);
			NSUserDefaults.StandardUserDefaults.SetString(DeviceType, AMLocalStorage.DeviceType);
			NSUserDefaults.StandardUserDefaults.SetString(deviceid, AMLocalStorage.DeviceID);

            NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public static void SaveLightAccount(string cardnumber, string email)
		{
			if (!string.IsNullOrEmpty(cardnumber))
			{
				NSUserDefaults.StandardUserDefaults.SetString(cardnumber, AMLocalStorage.CardNumber);
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasCard);
				//NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasLogin);
			}
			if (!string.IsNullOrEmpty(email))
			{
				NSUserDefaults.StandardUserDefaults.SetString(email, AMLocalStorage.UserEmail);
			}
		}

		public static void SaveDigitalAccount(string cardnumber, string email, string username)
		{
			
				NSUserDefaults.StandardUserDefaults.SetString(cardnumber, AMLocalStorage.CardNumber);
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasCard);
				NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasLogin);
				NSUserDefaults.StandardUserDefaults.SetString(username, AMLocalStorage.Username);

			if (!string.IsNullOrEmpty(email))
			{
				NSUserDefaults.StandardUserDefaults.SetString(email, AMLocalStorage.UserEmail);
			}
		}

		public static void Logout(UITabBarController tabbar)
		{
			AMMain.ClearAanbodTabBadge();
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSession);
			NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.HasLogin);
			NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.Session);

			KeychainHelpers.DeletePasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureMemberId, true);
			KeychainHelpers.DeletePasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true);
			KeychainHelpers.DeletePasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureTokenApp, true);

			NSUserDefaults.StandardUserDefaults.RemoveObject(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts);
			NSUserDefaults.StandardUserDefaults.RemoveObject(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.JsonPromotie + "-" + "vtwo/speciaal-voor-jou");
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.JsonAanbod);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.SeenGifts);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UnseenGifts);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.SecureTokenApp);

			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserFirstName);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserEmail);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserPrefix);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserLastName);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserGender);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserInitials);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserBirthday);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserBirthdayFloat);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserStreet);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserPostalCode);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserCity);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserCountry);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserHouseNumber);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserHouseNumberSupplement);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserTelephoneNrPrivate);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserTelephoneNrMobile);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.Avatar);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.EmailBouncePresent);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.AddressBounceReason);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.AddressBouncePresent);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserEmailBounceReason);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserCity);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserStreet);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserCountry);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserPostalCode);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserHouseNumber);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserHouseNumberSupplement);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserFullName);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserTelephoneNrMobile);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserTelephoneNrPrivate);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.UserShowPopUp);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.AanbodService);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.KaartService);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.PromotieService);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.PartnerService);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.PartnerDetailService);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.LinkcardsMessage);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.TruncatedPan);
            NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.Profiling);

			NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public static void DeleteTouchPin()
		{
			KeychainHelpers.DeletePasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true);

			NSUserDefaults.StandardUserDefaults.RemoveObject(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID);
			NSUserDefaults.StandardUserDefaults.RemoveObject(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID);

			NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public static void DeletePin()
		{
			KeychainHelpers.DeletePasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true);
			NSUserDefaults.StandardUserDefaults.RemoveObject(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID);

			NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public static void SetCard(KaartData kaart, string kaartnummer)
		{
			NSUserDefaults.StandardUserDefaults.SetString(kaartnummer, AMLocalStorage.CardNumber);

			NSUserDefaults.StandardUserDefaults.SetInt(kaart.Collectors, AMLocalStorage.Collectors);
			NSUserDefaults.StandardUserDefaults.SetString(kaart.Balance.ToString(), AMLocalStorage.Balance);
			NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString("D", AMMain.Cultureinfo), AMLocalStorage.BalanceDate);
			NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.HasCard);
            NSUserDefaults.StandardUserDefaults.SetBool(kaart.Redirect, AMLocalStorage.RedirectToDigitalAccount);

            //validity
            NSUserDefaults.StandardUserDefaults.SetInt(0, AMLocalStorage.ExpirationDate);
            NSUserDefaults.StandardUserDefaults.SetString("", AMLocalStorage.ExpirationAmount);
            NSUserDefaults.StandardUserDefaults.Synchronize();

            try
            {
                if (!string.IsNullOrEmpty(kaart.KaartValidity[0].NumberOfAirmiles))
                {
                    NSUserDefaults.StandardUserDefaults.SetInt((nint)kaart.KaartValidity[0].ExpirationDatePerMonth, AMLocalStorage.ExpirationDate);
                    NSUserDefaults.StandardUserDefaults.SetString(kaart.KaartValidity[0].NumberOfAirmiles, AMLocalStorage.ExpirationAmount);
                    NSUserDefaults.StandardUserDefaults.Synchronize();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception validity " + ex);
            }
		}

		public static void RemoveCard()
		{
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.Collectors);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.Balance);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.BalanceDate);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.HasCard);
			NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.CardNumber);
            NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.ExpirationDate);
            NSUserDefaults.StandardUserDefaults.RemoveObject(AMLocalStorage.ExpirationAmount);

            NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
            shared.RemoveObject(AMLocalStorage.CardNumber);
            shared.Synchronize();

            NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public static void IncreareWrongPinAttempt()
		{
			nint CurrentPinAttempts = NSUserDefaults.StandardUserDefaults.IntForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);
			CurrentPinAttempts++;
			Console.WriteLine("Current Pin Attempts: " + CurrentPinAttempts);

			NSUserDefaults.StandardUserDefaults.SetInt(CurrentPinAttempts, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);
			NSUserDefaults.StandardUserDefaults.Synchronize();
		}

		public static void ResetPinAttempts()
		{
			NSUserDefaults.StandardUserDefaults.RemoveObject(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.PinAttempts);
			NSUserDefaults.StandardUserDefaults.Synchronize();
		}
	}
}

