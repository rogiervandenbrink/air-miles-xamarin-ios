﻿using Foundation;
using System;
using UIKit;
using NotificationCenter;
using CoreGraphics;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
    public partial class WidgetViewController : UIViewController, INCWidgetProviding
    {
        public WidgetViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Tell widget it can be expanded
			ExtensionContext.SetWidgetLargestAvailableDisplayMode(NCWidgetDisplayMode.Expanded);

			// Get the maximum size
			var maxSize = ExtensionContext.GetWidgetMaximumSize(NCWidgetDisplayMode.Expanded);
		}

		[Export("widgetPerformUpdateWithCompletionHandler:")]
		public void WidgetPerformUpdate(Action<NCUpdateResult> completionHandler)
		{

			var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
			{
				Format = ZXing.BarcodeFormat.EAN_13,
				Options = new ZXing.Common.EncodingOptions
				{
					Width = 300,
					Height = 70,
					Margin = 0
				}
			};
			// Take action based on the display mode
			switch (ExtensionContext.GetWidgetActiveDisplayMode())
			{
				case NCWidgetDisplayMode.Compact:
					CardImageview.Image = BarcodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
					break;
				case NCWidgetDisplayMode.Expanded:
					EANLabel.Text = " " + SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));

					var EANStyle = new NSAttributedString(EANLabel.Text,
						new UIStringAttributes()
						{
							KerningAdjustment = 15
						});

					EANLabel.AttributedText = EANStyle;
					break;
			}

			// Report results
			// If an error is encoutered, use NCUpdateResultFailed
			// If there's no update required, use NCUpdateResultNoData
			// If there's an update, use NCUpdateResultNewData
			completionHandler(NCUpdateResult.NewData);
		}

		[Export("widgetActiveDisplayModeDidChange:withMaximumSize:")]
		public void WidgetActiveDisplayModeDidChange(NCWidgetDisplayMode activeDisplayMode, CGSize maxSize)
		{
			var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
			{
				Format = ZXing.BarcodeFormat.EAN_13,
				Options = new ZXing.Common.EncodingOptions
				{
					Width = 300,
					Height = 70,
					Margin = 0
				}
			};
			// Take action based on the display mode
			switch (activeDisplayMode)
			{
				case NCWidgetDisplayMode.Compact:
					PreferredContentSize = maxSize;
					CardImageview.Image = BarcodeWriter.Write(SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber)));
					break;
				case NCWidgetDisplayMode.Expanded:
					PreferredContentSize = new CGSize(0, 200);
					EANLabel.Text = " " + SharedMain.GenerateEan(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.CardNumber));

					var EANStyle = new NSAttributedString(EANLabel.Text,
						new UIStringAttributes()
						{
							KerningAdjustment = 15
						});

					EANLabel.AttributedText = EANStyle;
					break;
			}
		}

	}
}