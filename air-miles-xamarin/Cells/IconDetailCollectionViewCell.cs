﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreAnimation;
using CoreGraphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SushiHangover.SVGKit;

namespace airmilesxamarin
{
	partial class IconDetailCollectionViewCell : UICollectionViewCell
	{
		AanbodPromoties AanbodPromoties;
		UIViewController Controller;
		SVGKFastImageView imageView;

		// Network status
		NetworkStatus Internet;

		bool findable;

		bool CellIsNotUpdated = false;
		nfloat CellWidth = UIScreen.MainScreen.Bounds.Width / 2;
		nfloat CellHeight = (nfloat)((UIScreen.MainScreen.Bounds.Width / 2) * 0.85);

		public IconDetailCollectionViewCell (IntPtr handle) : base (handle)
		{
		}

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
        }

		public void UpdateCell(TableRow item, int cell, int total, UIViewController controller)
		{
			AddBorder(cell, total);
			//UserInteractionEnabled = false;
			//Detail.Hidden = false;

			if (imageView != null)
			{
				imageView.Hidden = false;
			}
			if (Title != null)
			{
				Title.Hidden = false;
			}

			if (item.Title != null) {
				
				Title.Text = item.Title;
					
			}

			//imageView.Image = null;

			if (item.Detail != null /*&& CellIsNotUpdated == true*/)
			{
				
				if (item.Title == "Speciaal voor jou!")
				{
					GetAmountPromotions(item);
				}
				else
				{
					Detail.Text = item.Detail;
					Detail.Hidden = false;
				}

					
			}
			UpdateDetailLabel(item);
			UserInteractionEnabled = true;

			imageView.Frame = new CGRect(((UIScreen.MainScreen.Bounds.Width / 2) - 30) / 2, (((UIScreen.MainScreen.Bounds.Width / 2) - 40) / 2) - 30, 30, 30);
			Add(imageView);
				
			
		}


		public async void GetAmountPromotions(TableRow item) 		{
			int count = 0;
			int countunfindable = 0;

				
			Detail.Hidden = true;

			if(NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
			{
			Internet = Reachability.InternetConnectionStatus();

				if (Internet != NetworkStatus.NotReachable)
				{
					if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts)))
					{
						Detail.Hidden = false;
						Console.WriteLine("aantal speciaal voor jou " + NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts));
						GiftsRoot Gifts = JsonConvert.DeserializeObject<GiftsRoot>(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonGifts));

						if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions)))
						{
							MendixPersonalizedPromotionRoot PersonalizedPromotions = JsonConvert.DeserializeObject<MendixPersonalizedPromotionRoot>(NSUserDefaults.StandardUserDefaults.StringForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.JsonPersonalizedPromotions));

							if (Gifts.Response != null && Gifts.Errors[0].Code == "E00" || PersonalizedPromotions.Errors[0].Code == "E00" && PersonalizedPromotions.Response.Length > 0)
							{
								count = Gifts.Response.Length + PersonalizedPromotions.Response.Length;
								Console.WriteLine("count 4 " + count);
							}
						}
					}
					else
					{
						if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.NewSession) || AMMain.DoWebService(AMLocalStorage.PromotieService) || NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AanbodRefresh))
						{
							try
							{
								PromotiesService PromotiesService = new PromotiesService();
								AanbodPromoties = await PromotiesService.GetAanbod("vtwo/speciaal-voor-jou");

								if (AanbodPromoties.Promoties != null)
								{
									string service = AMLocalStorage.PromotieService;
									NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.DateTimeLastCall + service);
									for (int i = 0; i < AanbodPromoties.Promoties.Length; i++)
									{
										if (AanbodPromoties.Promoties[i].Keywords != null)
										{

											foreach (string key in AanbodPromoties.Promoties[i].Keywords)
											{
												if (key == "onvindbaar" && !string.IsNullOrEmpty(AanbodPromoties.Promoties[i].Image))
												{
													countunfindable++;
													Console.WriteLine("key is onvindbaar " + countunfindable);
												}
											}

										}

										if (!string.IsNullOrEmpty(AanbodPromoties.Promoties[i].Image))
										{
											count++;

										}


									}


								}
								NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.AanbodRefresh);
							}
							catch (Exception ex)
							{

							}
							//NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.NewSession);
						}
						else
						{

							string localPromoties = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonPromotie + "-" + "vtwo/speciaal-voor-jou");

							if (!string.IsNullOrEmpty(localPromoties))
							{
								AanbodPromoties = JsonConvert.DeserializeObject<AanbodPromoties>(localPromoties);

								JObject itemContainer = JObject.Parse(localPromoties);
								JEnumerable<JToken> itemList = itemContainer.SelectToken("Promotions").Children<JToken>();
								foreach (var items in itemList)
								{
									if (items.HasValues && !string.IsNullOrEmpty(items.Value<string>("previewImage")))
									{
										count++;
									}
								}
								//Console.WriteLine("aantal aanbiedingen "+ count);
								Console.WriteLine("LocalStorage update");
								CellIsNotUpdated = false;

								if (count == 1)
								{
									Detail.Hidden = false;
									Detail.Text = "1 aanbieding";
								}
								else
								{
									Detail.Hidden = false;
									Detail.Text = count + " aanbiedingen";
								}
							}

						}
					}
					Console.WriteLine("aantal aanbiedingen " + count);
					int totalcount = count /*- countunfindable*/;
					if (totalcount == 1)
					{
						Detail.Hidden = false;
						Detail.Text = "1 aanbieding";
					}
					else
					{
						Detail.Hidden = false;
						Detail.Text = count + " aanbiedingen";
					}

				}
				else
				{

					string localPromoties = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonPromotie + "-" + "vtwo/speciaal-voor-jou");

					if (!string.IsNullOrEmpty(localPromoties))
					{
						AanbodPromoties = JsonConvert.DeserializeObject<AanbodPromoties>(localPromoties);

						JObject itemContainer = JObject.Parse(localPromoties);
						JEnumerable<JToken> itemList = itemContainer.SelectToken("Promotions").Children<JToken>();
						foreach (var items in itemList)
						{
							if (items.HasValues && !string.IsNullOrEmpty(items.Value<string>("previewImage")))
							{
								count++;
							}
						}
						//Console.WriteLine("aantal aanbiedingen "+ count);
						Console.WriteLine("LocalStorage update");
						CellIsNotUpdated = false;

						if (count == 1)
						{
							Detail.Hidden = false;
							Detail.Text = "1 aanbieding";
						}
						else
						{
							Detail.Hidden = false;
							Detail.Text = count + " aanbiedingen";
						}
					}

				}
			}else
			{
				Detail.Hidden = true;
			} 		}


		public AanbodPromoties GetPromoties()
		{
			return AanbodPromoties;
		}

		private void UpdateDetailLabel(TableRow item) //, string text, UICollectionView collectionView, UIViewController controller){
		{
			bool greyColor = false;

				try
				{
					SetColorIcons(item, false);

				}
				catch (Exception ex)
				{

				}
				Title.TextColor = UIColor.FromRGB(19, 133, 195);
				Detail.TextColor = UIColor.FromRGB(19, 133, 195);

				Title.Hidden = false;

		}

		public void SetColorIcons(TableRow item, bool greyColor)
		{
			if (item.Icon != null)
			{
				try
				{
					string color = "#007dc3"; //007dc3
				  if (greyColor)
				  {
						color = "#c9c9c9";
					}
					string svgString = item.Icon;
					svgString = svgString.Replace("</style>", ".st0 { fill:" + color + ";} </style>");
					svgString = svgString.Replace("<path ", "<path class=\"st0\" ");
					NSData dataString = NSData.FromString(svgString);
					SVGKImage image = new SVGKImage(dataString);
					if (imageView != null)
					{
						imageView.RemoveFromSuperview();
					}
					imageView = new SVGKFastImageView(image);
                    imageView.Hidden = false;
                    //device size was here


				}
				catch (Exception ex)
				{
					Console.WriteLine("exception svg " + ex);
					Image.Image = UIImage.FromBundle(item.Icon);

				}

			}
		}

		public void AddBorder(int cell, int total)
		{
			int evenOdd;

			// Check to see if total number of cells are even or odd
			if (total % 2 == 0) {
				evenOdd = 2;
			} else {
				evenOdd = 1;
			}

			// Check to see if cell item is even to set the border
			if (cell % 2 == 0) {

				// Check to see if cell item it the first
				if (cell == 0) {
					AddFirstBorderRight ();
					AddBorderBottomLeft ();
				} else {
					if (cell == (total - evenOdd)) {
						AddLastBorderRight ();
					} else {
						AddBorderRight ();
						AddBorderBottomLeft ();
					}
				}
			} else {
				if(cell != (total - 1)){
					AddBorderBottomRight ();
				}
			}
		}

		private void AddFirstBorderRight(){

			CAGradientLayer Border = new CAGradientLayer();
			Border.Colors = new CGColor[] {
				UIColor.White.CGColor,
				UIColor.FromRGB(238,238,238).CGColor
			};

			Border.Frame = new CGRect(CellWidth - 1, 0, 1, CellHeight);

			ContentView.Layer.AddSublayer(Border);
		}

		private void AddLastBorderRight(){

			CAGradientLayer Border = new CAGradientLayer();
			Border.Colors = new CGColor[] {
				UIColor.FromRGB(238,238,238).CGColor,
				UIColor.White.CGColor
			};

			Border.Frame = new CGRect(CellWidth - 1, 0, 1, CellHeight);

			ContentView.Layer.AddSublayer(Border);
		}

		private void AddBorderRight(){
			CALayer Border = new CALayer();
			Border.BackgroundColor = UIColor.FromRGB (238, 238, 238).CGColor;

			Border.Frame = new CGRect(CellWidth - 1, 0, 1, ContentView.Frame.Height);

			ContentView.Layer.AddSublayer(Border);
		}

		private void AddBorderBottomLeft(){

			CAGradientLayer Border = new CAGradientLayer();
			Border.Colors = new CGColor[] {
				UIColor.White.CGColor,
				UIColor.FromRGB(238,238,238).CGColor
			};

			Border.Frame = new CGRect(0, CellHeight - 1, CellWidth, 1);
			Border.StartPoint = new CGPoint (0,0);
			Border.EndPoint = new CGPoint (1,0);

			ContentView.Layer.AddSublayer(Border);
		}
			
		private void AddBorderBottomRight(){

			CAGradientLayer Border = new CAGradientLayer();
			Border.Colors = new CGColor[] {
				UIColor.FromRGB(238,238,238).CGColor,
				UIColor.White.CGColor
			};

			Border.Frame = new CGRect(0, CellHeight - 1, CellWidth, 1);
			Border.StartPoint = new CGPoint (0,0);
			Border.EndPoint = new CGPoint (1,0);

			ContentView.Layer.AddSublayer(Border);
		}

	}
}
