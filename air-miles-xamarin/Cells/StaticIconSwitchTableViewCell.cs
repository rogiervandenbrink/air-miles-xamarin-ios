﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    partial class StaticIconSwitchTableViewCell : UITableViewCell
	{
		UIViewController Controller;

		public StaticIconSwitchTableViewCell(IntPtr handle) : base(handle)
		{
		}

		public void UpdateCell(TableRow item, UIViewController parent)
		{
			// Set the parent controller
			Controller = parent;

			if (item.Icon != null)
			{
				Image.Image = UIImage.FromBundle(item.Icon);
			}

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}

			Switch.SetState(item.Switch, false);


		}

	}
}