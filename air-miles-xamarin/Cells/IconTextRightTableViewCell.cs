﻿using Foundation;
using System;
using UIKit;
using System.Text.RegularExpressions;
using CoreGraphics;
using Air_Miles_Xamarin;
using System.Collections.Generic;

namespace airmilesxamarin
{
    public partial class IconTextRightTableViewCell : UITableViewCell
    {
		
		UIViewController Controller;

        public IconTextRightTableViewCell (IntPtr handle) : base (handle)
        {
        }


		public void UpdateCell(TableRow item, UIViewController parent)
		{


			Controller = parent;
			AMMain.AddDoneButton(InputText);

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
			else
			{
				Title.Text = "";
			}

			if (item.InputText != null)
			{
				InputText.Text = item.InputText;
			}
			else {
				InputText.Text = "";
			}

			InputText.EditingDidBegin += delegate {
				InputText.TextColor = UIColor.Blue;

			}; ;

			InputText.EditingDidEnd +=delegate {
				InputText.TextColor = UIColor.FromRGB(217,217,217 );
				InputText.ResignFirstResponder();
			};

			if (item.Title == "Email")
			{
				InputText.KeyboardType = UIKeyboardType.EmailAddress;
			}
			if (item.Title == "Huisnummer")
			{
				InputText.KeyboardType = UIKeyboardType.NumberPad;
			}
			if (item.Title == "Telefoon")
			{
				InputText.KeyboardType = UIKeyboardType.NumberPad;
			}
			if (item.Title == "Mobiel")
			{
				InputText.KeyboardType = UIKeyboardType.NumberPad;
			}

			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				if (navctrl.Opslaan)
				{
					
					if (item.Title == "Voornaam")
					{
						InputText.EditingDidEnd += delegate {
							
							Console.WriteLine("ingevulde voornaam: "+ InputText.Text);
							if (InputText.Text.Length > 0 || InputText.Text.Length == 0)
							{

								Gegevens.Voornaam = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}
						};
					}
					if (item.Title == "Voorletters")
					{
						InputText.EditingChanged += delegate
						{
							if (InputText.Text.Length > 0 || InputText.Text.Length == 0)
							{

								navctrl.Invalid = false;
							}
							else
							{
								navctrl.Invalid = true;
							}
						};
						InputText.EditingDidEnd += delegate
						{
							if (InputText.Text.Length > 0)
							{

								Gegevens.Initialen = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}
						};
					}
					if (item.Title == "Tussenvoegsel")
					{
						InputText.EditingDidEnd += delegate
						{
							Gegevens.Tussenvoegsel = InputText.Text;

						};
					}
					if (item.Title == "Achternaam")
					{
						InputText.EditingDidEnd += delegate
						{

							if (InputText.Text.Length > 0)
							{

								Gegevens.Achternaam = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}
						};
					}
					//if (item.Title == "Geboortedatum")
					//{
					//	//InputText.ShouldBeginEditing += InputText_ShouldBeginEditing;
					//	InputText.EditingDidBegin += (sender, e) => ShowDatePicker(InputText);
					//	//InputText.EditingDidEnd += (sender, e) => DismissDatePicker(InputText);

					//}
					if (item.Title == "E-mail")
					{
						InputText.EditingChanged += delegate
						{
							if (Regex.Match(InputText.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
							{
							 

								navctrl.Invalid = false;
							}
							else
							{
								
								navctrl.Invalid = true;
							}

						};
						InputText.EditingDidEnd += delegate
						{
							if (Regex.Match(InputText.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
							{

								Gegevens.Email = InputText.Text; ;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}

						};
					}
					if (item.Title == "Telefoon")
					{
						InputText.EditingChanged += delegate
						{
							if (InputText.Text.Length == 10 || InputText.Text.Length == 0)
							{
								navctrl.Invalid = false;
							}
							else
							{
								navctrl.Invalid = true;
							}

						};
						InputText.EditingDidEnd += delegate
						{
							if (InputText.Text.Length == 10 || InputText.Text.Length == 0)
							{

								Gegevens.Telefoon = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}

						};
					}
					if (item.Title == "Mobiel")
					{
						InputText.EditingChanged += delegate
						{
							if (InputText.Text.Length == 10 || InputText.Text.Length == 0)
							{
								navctrl.Invalid = false;
							}
							else
							{
								navctrl.Invalid = true;
							}

						};
						InputText.EditingDidEnd += delegate
						{
							if (InputText.Text.Length == 10 || InputText.Text.Length == 0)
							{

								Gegevens.Mobiel = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}

						};
					}
					if (item.Title == "Straat")
					{
						InputText.EditingChanged += delegate
						{
							if (InputText.Text.Length > 0)
							{
								navctrl.Invalid = false;
							}
							else
							{
								navctrl.Invalid = true;
							}

						};
						InputText.EditingDidEnd += delegate
						{
							if (InputText.Text.Length > 0)
							{

								Gegevens.Straat = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}

						};
					}
					if (item.Title == "Huisnummer")
					{
						InputText.EditingChanged += delegate
						{
							if (InputText.Text.Length > 0)
							{

								Gegevens.Huisnummer = InputText.Text;
								//int houseNumber = Convert.ToInt32(InputText.Text);
								Address.HouseNumber = Gegevens.Huisnummer;
								PostcodeCheckerHuisnummer(InputText.Text);
								navctrl.Invalid = false;
							}
							else
							{
								navctrl.Invalid = true;
							}

						};
						InputText.EditingDidEnd += delegate
						{
							if (InputText.Text.Length > 0)
							{

								Gegevens.Huisnummer = InputText.Text;
								//int houseNumber = Convert.ToInt32(InputText.Text);
								Address.HouseNumber = Gegevens.Huisnummer;
								ValidCell();
								PostcodeCheckerHuisnummer(InputText.Text);
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}

						};
					}
					if (item.Title == "Huisnummer toevoeging")
					{
						InputText.EditingDidEnd += delegate
						{
							
							Gegevens.HuisnummerToevoeging = InputText.Text;

						};
					}
					if (item.Title == "Plaats")
					{
						Console.WriteLine("item title plaats" + item.Title);
						InputText.EditingChanged += delegate
						{
							if (InputText.Text.Length > 0)
							{

								navctrl.Invalid = false;
							}
							else
							{
								navctrl.Invalid = true;
							}
						};
						InputText.EditingDidEnd += delegate
						{
							if (InputText.Text.Length > 0)
							{

								Gegevens.Plaats = InputText.Text;
								ValidCell();
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}
						};
					}

					if (item.Title == "Postcode")
					{
						InputText.EditingChanged += delegate
						{
							if (Regex.Match(InputText.Text, "^[1-9][\\d]{3}\\s?(?!(sa|sd|ss|SA|SD|SS))([a-eghj-opr-tv-xzA-EGHJ-OPR-TV-XZ]{2})?$").Success)
							{

								navctrl.Invalid = false;
							}
							else
							{
								
								navctrl.Invalid = true;
							}
						};
						InputText.EditingDidEnd += delegate
						{
							if (Regex.Match(InputText.Text, "^[1-9][\\d]{3}\\s?(?!(sa|sd|ss|SA|SD|SS))([a-eghj-opr-tv-xzA-EGHJ-OPR-TV-XZ]{2})?$").Success)
							{
								CheckZip(InputText);
								Gegevens.Postcode = InputText.Text;
								Address.Postcode = InputText.Text;
								ValidCell();
								PostcodeCheckerPostcode(InputText.Text);
								navctrl.Invalid = false;
							}
							else
							{
								ErrorCell("!");
								navctrl.Invalid = true;
							}
						};
					}
					//if (item.Title == "Land")
					//{
					//	//InputText.ShouldBeginEditing += InputLand_ShouldBeginEditing;
					//	InputText.EditingDidBegin += (sender, e) => ShowCountryPicker(InputText);
					//	//InputText.EditingDidEnd += (sender, e) => DismissLandPicker(InputText);

					//}
					//if (item.Title == "Aanhef")
					//{
					//	Console.WriteLine("item title aanhef" + item.Title);
					//	Title.Text = item.Title;
					//	InputText.EditingDidBegin += (sender, e) => ShowAanhefPicker(InputText);
					//	//InputText.EditingDidEnd += (sender, e) => DismissAanhefPicker(InputText);
					//}

				}
				if (!navctrl.Opslaan)
				{
					InputText.ResignFirstResponder();
				}
			}
		}

		public void ErrorCell(string icon)
		{
			Title.Frame = new CGRect(50, Title.Frame.Y, Title.Frame.Width, Title.Frame.Height);
			Title.TranslatesAutoresizingMaskIntoConstraints = true;
			Title.TextColor = UIColor.FromRGB(242, 42, 94);
			Icon.Image = UIImage.FromBundle("warning_icon_air");
			Icon.TintColor = UIColor.FromRGB(242, 42, 94);
			Icon.Hidden = false;
			InputText.TextColor = UIColor.FromRGB(242, 42, 94);

			Console.WriteLine("error cell");
		}

		public void ValidCell()
		{
			Title.Frame = new CGRect(15, Title.Frame.Y, Title.Frame.Width, Title.Frame.Height);
			Title.TranslatesAutoresizingMaskIntoConstraints = true;
			Title.TextColor = UIColor.Black;
			Icon.Hidden = true;
			InputText.TextColor = UIColor.FromRGB(217, 217, 217);
			Console.WriteLine("valid cell");
		}

		static void CheckZip(UITextField zip)
		{
			string replace = zip.Text.ToUpper();
			Regex rgx = new Regex("[^0-9A-Z]");
			replace = rgx.Replace(replace, "");
			if (replace.Length > 6)
				replace = replace.Substring(0, 6);
			if (replace.Length >= 6)
				replace = replace.Substring(0, 4) + " " + replace.Substring(4, replace.Length - 4);
			zip.Text = replace;
		}

		public async void PostcodeCheckerPostcode(string postcode)
		{
			GegevensViewController navctrl = Controller as GegevensViewController;
			if (!string.IsNullOrEmpty(Address.HouseNumber))
			{
				try
				{
					PostcodeCheckerService PostcodeService = new PostcodeCheckerService();
					PostcodeCheckerDataRoot CurrentAddress = await PostcodeService.GetPostcodeCheckerData(postcode, Address.HouseNumber);

					if (CurrentAddress.Errors[0] != null && CurrentAddress.Errors[0].Code == "E00")
					{

						Address.Street = CurrentAddress.Response.Street;
						Address.City = CurrentAddress.Response.City;

						Gegevens.Straat = CurrentAddress.Response.Street;
						Gegevens.Plaats = CurrentAddress.Response.City;

						Console.WriteLine("postcode checker postcode huisnummer address " + Address.HouseNumber);
						navctrl.RefreshTableAanpassenItems();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("exception postcodeservice "+ ex);
				}
			}
			else if (!String.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber)))
			{
				try
				{
					string houseNumber = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber);
					PostcodeCheckerService PostcodeService = new PostcodeCheckerService();
					PostcodeCheckerDataRoot CurrentAddress = await PostcodeService.GetPostcodeCheckerData(postcode, houseNumber);

					if (CurrentAddress.Errors[0] != null && CurrentAddress.Errors[0].Code == "E00")
					{
						Address.Street = CurrentAddress.Response.Street;
						Address.City = CurrentAddress.Response.City;

						Gegevens.Straat = CurrentAddress.Response.Street;
						Gegevens.Plaats = CurrentAddress.Response.City;

						Console.WriteLine("postcode checker postcode huisnummer storage " + NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserHouseNumber));
						navctrl.RefreshTableAanpassenItems();
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("exception postcodeservice " + ex);
				}

			}



		}

		public async void PostcodeCheckerHuisnummer(string huisnummer)
		{
			GegevensViewController navctrl = Controller as GegevensViewController;

			if (Address.Postcode != null)
			{
				try {
					PostcodeCheckerService PostcodeService = new PostcodeCheckerService();
					PostcodeCheckerDataRoot CurrentAddress = await PostcodeService.GetPostcodeCheckerData(Gegevens.Postcode, huisnummer);

					if (CurrentAddress.Errors[0] != null && CurrentAddress.Errors[0].Code == "E00")
					{
						Console.WriteLine("response postcode "+ CurrentAddress.Response.ToString());
						Address.Street = CurrentAddress.Response.Street;
						Address.City = CurrentAddress.Response.City;

						if (!string.IsNullOrEmpty(CurrentAddress.Response.Street))
						{
							NSUserDefaults.StandardUserDefaults.SetString(CurrentAddress.Response.Street, AMLocalStorage.UserStreet);
						}
						if (!string.IsNullOrEmpty(CurrentAddress.Response.City))
						{
							NSUserDefaults.StandardUserDefaults.SetString(CurrentAddress.Response.City, AMLocalStorage.UserCity);
						}

						navctrl.RefreshTableAanpassenItems();

						Console.WriteLine("address gegevens " + Address.Street + " " + Address.City);
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("exception postcodeservice " + ex);
				}
			}
				
		}

		private void ShowAanhefPicker(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;

				var customAanhefList = new List<string>();
				customAanhefList.Add("Dhr.");
				customAanhefList.Add("Mvr.");

				var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Selecteer uw aanhef", navctrl)
				{
					HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
					HeaderTextColor = UIColor.White,
					TransitioningDelegate = new ModalPickerTransitionDelegate(),
					ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPicker.PickerView.Model = new CustomPickerModel(customAanhefList);

				//On an item is selected, update our label with the selected item.
				modalPicker.OnModalPickerDismissed += (s, ea) =>
				{
					var index = modalPicker.PickerView.SelectedRowInComponent(0);
					textField.Text = customAanhefList[(int)index];
					Gegevens.Aanhef = customAanhefList[(int)index];
				};

				navctrl.PresentViewController(modalPicker, true, null);
			}
		}

		private void ShowCountryPicker(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;

				var customLandList = new List<string>();
				customLandList.Add("Nederland");
				customLandList.Add("België");
				customLandList.Add("Duitsland");
				customLandList.Add("Frankrijk");
				customLandList.Add("Luxemburg");

				var modalPickerLand = new ModalPickerViewController(ModalPickerType.Country, "Selecteer uw land", navctrl)
				{
					HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
					HeaderTextColor = UIColor.White,
					TransitioningDelegate = new ModalPickerTransitionDelegate(),
					ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPickerLand.PickerView.Model = new CountryPickerModel(customLandList);

				//On an item is selected, update our label with the selected item
				modalPickerLand.OnModalPickerDismissed += (s, ea) =>
				{
					var index = modalPickerLand.PickerView.SelectedRowInComponent(0);
					textField.Text = customLandList[(int)index];
					Gegevens.Land = customLandList[(int)index];
				};

				navctrl.PresentViewController(modalPickerLand, true, null);
			}
		}

		private void ShowDatePicker(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Selecteer de datum", navctrl)
				{
					HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
					HeaderTextColor = UIColor.White,
					TransitioningDelegate = new ModalPickerTransitionDelegate(),
					ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPicker.DatePicker.Mode = UIDatePickerMode.Date;

				modalPicker.OnModalPickerDismissed += (s, ea) =>
				{
					var dateFormatter = new NSDateFormatter()
					{
						DateFormat = "dd MMMM yyyy"
					};

					if (modalPicker.DatePicker.Date != null)
					{
						DateTime date = DateTime.ParseExact(dateFormatter.ToString(modalPicker.DatePicker.Date), "dd MMMM yyyy", null);
						double epochDate = SharedDatetime.ConvertToUnixTimestamp(date);
						Console.WriteLine("epoch is " + epochDate);

						textField.Text = dateFormatter.ToString(modalPicker.DatePicker.Date);
						Gegevens.Geboortedatum = (nint)epochDate;
					}
					Console.WriteLine("gegevens geboortedatum " + Gegevens.Geboortedatum);
				};

				navctrl.PresentViewController(modalPicker, true, null);
			}
		}

		void DismissDatePicker(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Selecteer de datum", navctrl);
					modalPicker.DismissViewController(true, null);

			}
		}

		void DismissAanhefPicker(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Selecteer uw aanhef", navctrl);
				modalPicker.DismissViewController(true, null);

			}
		}

		void DismissLandPicker(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				var modalPicker = new ModalPickerViewController(ModalPickerType.Country, "Selecteer uw land", navctrl);
				modalPicker.DismissViewController(true, null);

			}
		}

	}
}