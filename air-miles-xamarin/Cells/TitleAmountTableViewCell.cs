﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
    public partial class TitleAmountTableViewCell : UITableViewCell
    {

		UIViewController Controller;

        public TitleAmountTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public TitleAmountTableViewCell(UIViewController parent)
		{
			Controller = parent;
		}

		public void UpdateCell(TableRow item)
		{
			Amount.TranslatesAutoresizingMaskIntoConstraints = true;

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}

			if (item.Detail != null)
			{
				Amount.Text = item.Detail;
				Amount.Frame = new CGRect(UIScreen.MainScreen.Bounds.Width - 165, 10, 150, 20);
				Console.WriteLine("item.Detail is: "+item.Detail);

			}
		}
    }
}