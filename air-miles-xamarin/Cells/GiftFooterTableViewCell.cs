﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class GiftFooterTableViewCell : UITableViewCell
    {
		GiftViewController Controller;
		bool updated;

        public GiftFooterTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(GiftRow item, GiftViewController parent)
		{
			if (updated == false)
			{
				Controller = parent;

				if (Button != null)
				{
					AMStyle.RoundedButton(Button);

					Button.TouchUpInside += (sender, e) =>
					{
						if (item.SegueButton == ButtonSegue.ActivationSegue)
						{
							parent.ActivateOffer(parent);
						}
						else if (item.SegueButton == ButtonSegue.EanCompoundSegue)
						{
							parent.PerformSegue(ButtonSegue.EanCompoundSegue.ToString(), this);
						}
					};

					if (item.Title != null)
					{
						Button.SetTitle(item.Title.ToUpper(), UIControlState.Normal);
					}
				}

				if (item.Description != null)
				{
					OverviewButton.SetTitle(item.Description.ToUpper(), UIControlState.Normal);

					OverviewButton.TouchUpInside += (sender, e) =>
					{
						if (parent.InitActivated == true)
						{
							parent.RedirectToGiftDetails(parent.NavigationController, true);
						}
						else {
							Controller.NavigationController.PopViewController(true);
						}
					};
				}
				else {
					OverviewButton.Hidden = true;
				}

				updated = true;
			}
		}
    }
}