﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	partial class DetailButtonTableViewCell : UITableViewCell
	{
		UIViewController Controller;
		bool CellIsUpdated = false;
		int Index = 0;

		public DetailButtonTableViewCell (IntPtr handle) : base (handle)
		{
		}

		public DetailButtonTableViewCell (UIViewController parent)
		{
			Controller = parent;
		}

		public void UpdateCell(TableRow item, UIViewController parent, NSIndexPath indexPath)
		{

				Controller = parent;
				


				if (item.Title != null)
				{
				Title.Text = item.Title;
				Index = item.Index;

				Console.WriteLine("cell inderx " + item.Index);
				}
				

				if (item.Detail != null)
				{
					Detail.Text = item.Detail;

				}

				Button.Layer.CornerRadius = Button.Layer.Bounds.Height / 2;

				if (!CellIsUpdated)
				{
					//UIView.Animate(0.2, 0, UIViewAnimationOptions.Autoreverse | UIViewAnimationOptions.CurveEaseOut, () =>
					//   {
					//	   Button.Transform = CGAffineTransform.MakeScale(1.1f, 1.1f);
					//   }, null);

					Button.TouchUpInside += (sender, e) =>
					{
						if (Controller.GetType() == typeof(TransactieAankoopViewController))
						{

							TransactieAankoopViewController navctrl = Controller as TransactieAankoopViewController;
							
							navctrl.IndexVoucher = Index;
							Console.WriteLine("Index in cell :" + Index);
							Controller.PerformSegue("VoucherSeguer", indexPath);

						}

						Console.WriteLine("button voucher geklikt");
					};
					CellIsUpdated = true;
				}


		}
	}
}
