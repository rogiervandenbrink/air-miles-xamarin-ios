using Foundation;
using System;
using UIKit;
using Air_Miles_Xamarin;

namespace airmilesxamarin
{
    public partial class DatePickerTableViewCell : UITableViewCell
    {

		UIViewController Controller;

        public DatePickerTableViewCell (IntPtr handle) : base (handle)
        {
        }


		public void UpdateCell(TableRow item, UIViewController parent)
		{

			Controller = parent;

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
			else
			{
				Title.Text = "";
			}

			if (item.InputText != null)
			{
				InputText.Text = item.InputText;
			}
			else {
				InputText.Text = "";
			}

			InputText.EditingDidBegin += delegate
			{
				InputText.TextColor = UIColor.Blue;

			}; ;

			InputText.EditingDidEnd += delegate
			{
				InputText.TextColor = UIColor.FromRGB(217, 217, 217);
			};

			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				if (navctrl.Opslaan)
				{
					//InputText.EditingDidBegin += (sender, e) => ShowDatePicker(InputText);
					InputText.ShouldBeginEditing += OnTextFieldShouldBeginEditing;
				}
			}

		}

		bool OnTextFieldShouldBeginEditing(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Selecteer de datum", navctrl)
				{
					HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
					HeaderTextColor = UIColor.White,
					TransitioningDelegate = new ModalPickerTransitionDelegate(),
					ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPicker.DatePicker.Mode = UIDatePickerMode.Date;

				modalPicker.OnModalPickerDismissed += (s, ea) =>
				{
					var dateFormatter = new NSDateFormatter()
					{
						DateFormat = "dd MMMM yyyy"
					};

					if (modalPicker.DatePicker.Date != null)
					{
						DateTime date = DateTime.ParseExact(dateFormatter.ToString(modalPicker.DatePicker.Date), "dd MMMM yyyy", null);
						double epochDate = SharedDatetime.ConvertToUnixTimestamp(date);
						Console.WriteLine("epoch is " + epochDate);

						textField.Text = dateFormatter.ToString(modalPicker.DatePicker.Date);
						Gegevens.Geboortedatum = (nint)epochDate;

					}
					Console.WriteLine("gegevens geboortedatum " + Gegevens.Geboortedatum);
				};

				navctrl.PresentViewController(modalPicker, true, null);
			}
			return false;
		}
    }
}