﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

namespace airmilesxamarin
{
    public partial class GenderPickersTableViewCell : UITableViewCell
    {
		UIViewController Controller;

        public GenderPickersTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(TableRow item, UIViewController parent)
		{

			Controller = parent;

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
			else
			{
				Title.Text = "";
			}

			if (item.InputText != null)
			{
				InputText.Text = item.InputText;
			}
			else {
				InputText.Text = "";
			}

			InputText.EditingDidBegin += delegate
			{
				InputText.TextColor = UIColor.Blue;

			}; ;

			InputText.EditingDidEnd += delegate
			{
				InputText.TextColor = UIColor.FromRGB(217, 217, 217);
			};

			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				if (navctrl.Opslaan)
				{
					//InputText.EditingDidBegin += (sender, e) => ShowAanhefPicker(InputText);
					InputText.ShouldBeginEditing += OnGenderPickerShouldBeginEditing;
				}
			}

		}

		bool OnGenderPickerShouldBeginEditing(UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;

				var customAanhefList = new List<string>();
				customAanhefList.Add("Heer");
				customAanhefList.Add("Mevrouw");

				var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Selecteer uw aanhef", navctrl)
				{
					HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
					HeaderTextColor = UIColor.White,
					TransitioningDelegate = new ModalPickerTransitionDelegate(),
					ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPicker.PickerView.Model = new CustomPickerModel(customAanhefList);

				//On an item is selected, update our label with the selected item.
				modalPicker.OnModalPickerDismissed += (s, ea) =>
				{
					var index = modalPicker.PickerView.SelectedRowInComponent(0);
					textField.Text = customAanhefList[(int)index];
					Gegevens.Aanhef = customAanhefList[(int)index];
				};

				navctrl.PresentViewController(modalPicker, true, null);
			}
			return false;
		}
    }
}