﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using MappBinding;
using Newtonsoft.Json;
using airmilesxamarin.Helpers;

namespace airmilesxamarin
{
	partial class IconSwitchTableViewCell : UITableViewCell
	{
		UIViewController Controller;

		public IconSwitchTableViewCell (IntPtr handle) : base (handle)
		{
		}

        public void UpdateCell(TableRow item, UIViewController parent, UITableView tableView)
        {
            // Set the parent controller
            Controller = parent;

            if (item.Icon != null)
            {
                Image.Image = UIImage.FromBundle(item.Icon);
            }

            if (item.Title != null)
            {
                Title.Text = item.Title;
            }

			if (item.Title == "E-mail nieuwsbrief")
			{
				Switch.On = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Newsletter);

				Switch.ValueChanged += delegate {
					NewsletterValueChanged(Switch.On);
				};
			}
			if (item.Title == "Notificaties")
			{

                //Switch.On = NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Notifications);
				Switch.ValueChanged += delegate
				{
					AppNotificationsValueChanged(Switch.On);
				
				};
			}

			// Touch ID Toggle
			if (item.Title == "Touch ID")
			{
				Switch.On = NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID);

				Switch.ValueChanged += delegate
				{
					Console.WriteLine("switch change touch id");
					TouchIdValueChanged(Switch.On);
				};
			}

			// Pincode Toggle
			if (item.Title == "Pincode")
			{
				Switch.On = NSUserDefaults.StandardUserDefaults.BoolForKey(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID);

				Switch.ValueChanged += delegate
				{
					Console.WriteLine("switch change pincode");
					PinValueChanged(Switch.On, tableView);

				};
			}
		}

		private void TouchIdValueChanged(bool value)
		{
			// Set touch id to switch state
			NSUserDefaults.StandardUserDefaults.SetBool(value, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertTouchID);

			Console.WriteLine(Switch.On.ToString());
		}

		private void PinValueChanged(bool value, UITableView tableView)
		{
			Console.WriteLine("change pin to " + value);

			// If user enables pincode
			if (Switch.On == true)
			{
				// Check if user already has a valid Pincode
				if (string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecurePincode, true)))
				{
					SetPinTouchViewController Redirect = App.Storyboard.InstantiateViewController("SetPinTouchViewController") as SetPinTouchViewController;
					Controller.PresentModalViewController(Redirect, false);
					// Show controller to set pincode
					//PincodeNavigationViewController PinIdModal = App.Storyboard.InstantiateViewController("PincodeNavigationViewController") as PincodeNavigationViewController;
					//NSUserDefaults.StandardUserDefaults.SetBool(true, "InstellingenViewcontroller");
					//Controller.PresentModalViewController(PinIdModal, true);
				}
				// If user has a valid pincode, enable pincode
				else {
					NSUserDefaults.StandardUserDefaults.SetBool(value, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID);

				}
			}
			// If user disables pincode
			else {
				// Disable pincode
				NSUserDefaults.StandardUserDefaults.SetBool(value, NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username) + "-" + AMLocalStorage.AlertPinID);

			}

			if (Controller.GetType() == typeof(InstellingenViewController))
			{
				//InstellingenViewController controller = Controller as InstellingenViewController;
						
			}

		}

		private void NewsletterValueChanged(bool value)
		{
			// Set touch id to switch state
			Gegevens.Newsletter = value;
			Console.WriteLine("email switch is "+Switch.On.ToString());
		}

		private void AppNotificationsValueChanged(bool value)
		{
            
			/*NSUserDefaults.StandardUserDefaults.SetBool(value, AMLocalStorage.Notifications);
            NSUserDefaults.StandardUserDefaults.Synchronize();
            bool IsDisabled = !NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Notifications);

            Appoxee.Shared().DisablePushNotifications(IsDisabled, (NSError appoxeeError, NSObject data) =>
            {
                if (appoxeeError == null)
                {
                    // operation successful
                    Console.WriteLine("notification settings changed to disabled " + IsDisabled);
                }
            });*/
		}
    }
}
