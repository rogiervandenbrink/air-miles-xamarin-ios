﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
    [Register ("VoucherCollectionViewCell")]
    partial class VoucherCollectionViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel BackArrow { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Barcode { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView BorderImage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Description { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel EANLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ErrorLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ForwardArrow { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView VoucherBottom { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton WalletButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BackArrow != null) {
                BackArrow.Dispose ();
                BackArrow = null;
            }

            if (Barcode != null) {
                Barcode.Dispose ();
                Barcode = null;
            }

            if (BorderImage != null) {
                BorderImage.Dispose ();
                BorderImage = null;
            }

            if (Description != null) {
                Description.Dispose ();
                Description = null;
            }

            if (EANLabel != null) {
                EANLabel.Dispose ();
                EANLabel = null;
            }

            if (ErrorLabel != null) {
                ErrorLabel.Dispose ();
                ErrorLabel = null;
            }

            if (ForwardArrow != null) {
                ForwardArrow.Dispose ();
                ForwardArrow = null;
            }

            if (VoucherBottom != null) {
                VoucherBottom.Dispose ();
                VoucherBottom = null;
            }

            if (WalletButton != null) {
                WalletButton.Dispose ();
                WalletButton = null;
            }
        }
    }
}