﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace airmilesxamarin
{
	partial class DisclosureIndicatorTableViewCell : UITableViewCell
	{
		public DisclosureIndicatorTableViewCell (IntPtr handle) : base (handle)
		{
		}

		public void UpdateCell(TableRow item)
		{
			if(item.Title != null){
				Title.Text = item.Title;
			}

			DisclosureIndicator.Image = UIImage.FromBundle ("DisclosureIndicator");
		}
	}
}
