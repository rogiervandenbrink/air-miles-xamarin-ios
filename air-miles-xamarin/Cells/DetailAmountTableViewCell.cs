﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	partial class DetailAmountTableViewCell : UITableViewCell
	{
		UIViewController Controller;

		public DetailAmountTableViewCell (IntPtr handle) : base (handle)
		{
		}

		public DetailAmountTableViewCell (UIViewController parent)
		{
			Controller = parent;
		}

		public void UpdateCell(TableRow item)
		{
			Detail.TranslatesAutoresizingMaskIntoConstraints = true;

			if (item.Title != null)
			{
				Title.Hidden = false;
				Title.Text = item.Title;
				Detail.Frame = new CGRect(15, 32, UIScreen.MainScreen.Bounds.Width - 120, 20);
			}
			else {
				Title.Hidden = true;
				Detail.Frame = new CGRect(15, 20, UIScreen.MainScreen.Bounds.Width - 120, 20);
			}

			if (item.Detail != null) {
				Detail.Text =  item.Detail;
			}

			if (item.Subdetail != null) {
				if (item.Subdetail == "0")
				{
					Amount.TextColor = UIColor.FromRGB(255, 0, 0);
					Amount.Text = item.Subdetail;
				}
				if (item.Subdetail != "0" && item.Subdetail.Substring (0, 1) != "-") {
					Amount.TextColor = UIColor.FromRGB(139,183,51);
					Amount.Text = "+" + item.Subdetail;
				}
				if (item.Subdetail.Substring (0, 1) == "-") {
					Amount.TextColor = UIColor.FromRGB(239,106,10);
					Amount.Text = item.Subdetail;
				}
			}
		}
	}
}
