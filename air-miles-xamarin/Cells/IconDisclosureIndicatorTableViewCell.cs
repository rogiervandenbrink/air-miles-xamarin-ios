﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace airmilesxamarin
{
	partial class IconDisclosureIndicatorTableViewCell : UITableViewCell
	{

		public IconDisclosureIndicatorTableViewCell (IntPtr handle) : base (handle)
		{
		}

		UILabel accesoryBadge;
		CGRect BadgeFrame;
		UILabel ErrorOne;

		public void UpdateCell(TableRow item)
		{

			if (item.Icon != null) {

				//StartAnimateIcon(Image);
				if (item.CellIdentifier == CellIdentifier.IconDisclosureIndicatorTableViewCellDisabled)
				{
					Console.WriteLine("cell diasbled");
					Image.Image = UIImage.FromBundle(item.Icon).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
					Image.TintColor = UIColor.FromRGB(199, 199, 199);
					Image.ContentMode = UIViewContentMode.ScaleAspectFit;
				}
				if (item.CellIdentifier == CellIdentifier.IconDisclosureIndicatorBlueTableViewCell)
				{
					Console.WriteLine("cell blue");
					Image.Image = UIImage.FromBundle(item.Icon);
					//Image.TintColor = UIColor.FromRGB(199, 199, 199);
					Image.ContentMode = UIViewContentMode.ScaleAspectFit;
				}
				else {
					Console.WriteLine("cell enabled");
					Image.Image = UIImage.FromBundle(item.Icon).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
					Image.TintColor = UIColor.FromRGB(1, 125, 195);
					Image.ContentMode = UIViewContentMode.ScaleAspectFit;
				}
			}

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
			if (item.CellIdentifier == CellIdentifier.IconDisclosureIndicatorBlueTableViewCell)
			{
				Title.TextColor = UIColor.White;
				Title.Font = UIFont.FromName("Avenir-Black", 18f);
				DisclosureIndicator.Image = UIImage.FromBundle("DisclosureIndicator").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				DisclosureIndicator.TintColor = UIColor.White;
			}
			else
			{
				DisclosureIndicator.Image = UIImage.FromBundle("DisclosureIndicator");
			}


			if (item.Title == "Speciaal voor jou!")
			{
				if (accesoryBadge != null)
				{
					accesoryBadge.Hidden = true;
					accesoryBadge.RemoveFromSuperview();
				}
				if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UnseenGifts) > 0)
				{
					if (NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UnseenGifts) > 9)
					{
						BadgeFrame = new CGRect(40, 5, 25, 20);
						accesoryBadge = new UILabel(BadgeFrame);
						string amount = NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UnseenGifts).ToString();
						accesoryBadge.Text = amount;
						accesoryBadge.TextColor = UIColor.White;
						accesoryBadge.Font = UIFont.FromName("avenir-book", 14);
						accesoryBadge.TextAlignment = UITextAlignment.Center;
						accesoryBadge.BackgroundColor = UIColor.Red;
						accesoryBadge.Layer.CornerRadius = BadgeFrame.Size.Height / 2;
						accesoryBadge.ClipsToBounds = true;
						accesoryBadge.Hidden = false;
						AddSubview(accesoryBadge);
					}else
					{
						BadgeFrame = new CGRect(40, 5, 20, 20);
						accesoryBadge = new UILabel(BadgeFrame);
						string amount = NSUserDefaults.StandardUserDefaults.IntForKey(AMLocalStorage.UnseenGifts).ToString();
						accesoryBadge.Text = amount;
						accesoryBadge.TextColor = UIColor.White;
						accesoryBadge.Font = UIFont.FromName("avenir-book", 14);
						accesoryBadge.TextAlignment = UITextAlignment.Center;
						accesoryBadge.BackgroundColor = UIColor.Red;
						accesoryBadge.Layer.CornerRadius = BadgeFrame.Size.Height / 2;
						accesoryBadge.ClipsToBounds = true;
						accesoryBadge.Hidden = false;
						AddSubview(accesoryBadge);
					}

				}
				else
				{
					if (accesoryBadge != null)
					{
						accesoryBadge.Hidden = true;
						accesoryBadge.RemoveFromSuperview();
					}
				}
			}



		}

		public UIImage ResizeUIImage(UIImage sourceImage, float widthToScale, float heightToScale)
		{
			Console.WriteLine("image wordt geresized");
			var sourceSize = sourceImage.Size;
			var maxResizeFactor = Math.Max(widthToScale / sourceSize.Width, heightToScale / sourceSize.Height);
			Console.WriteLine("image resizefactor "+ maxResizeFactor);
			if (maxResizeFactor < 1) return sourceImage;
			var width = maxResizeFactor * sourceSize.Width;
			var height = maxResizeFactor * sourceSize.Height;
			UIGraphics.BeginImageContext(new CGSize(width, height));
			sourceImage.Draw(new CGRect(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		public void DisableIndicator()
		{
			DisclosureIndicator.Alpha = 0.25f;
		}

		void StartAnimateIcon(UIImageView Image)
		{
			// AnimateNotify ( duration, delay, options, animations, completion )
			UIView.AnimateNotify(0.25, 0, UIViewAnimationOptions.CurveLinear, () =>
		   {

			   CGAffineTransform transform = CGAffineTransform.MakeScale(0.75f, 0.75f);
			   Image.Transform = transform;
		   },
			(finished) =>
			{

				AnimateIcon(Image);
			});
		}

		void AnimateIcon(UIImageView Image)
		{
			// AnimateNotify ( duration, delay, spring damping, spring velocity, options, animations, completion )
			UIView.AnimateNotify(0.25, 0, 1, 50, 0, () =>
		   {
			   CGAffineTransform transform = CGAffineTransform.MakeScale(1f, 1f);
			   Image.Transform = transform;
		   }, null);

		}
	}
}