﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class GiftRowTableViewCell : UITableViewCell
    {
        public GiftRowTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(GiftRow item)
		{
			if (item.Title != null)
			{
				Title.Text = item.Title;
			}

			if (item.Description != null)
			{
				Description.Text = item.Description;
			}
		}
    }
}