﻿using Foundation;
using System;
using UIKit;
using Air_Miles_Xamarin;
using CoreGraphics;
using Google.Analytics;
using System.IO;

namespace airmilesxamarin
{
    public partial class VoucherCollectionViewCell : UICollectionViewCell
    {
		public UIViewController parent { get; set;}
		public bool IsUpdated = false;
        public bool ArrowIsUpdated = false;
        LoadingOverlay LoadingView;
        string BarcodeFormat = "";

        VoucherTableRow Item;

        public VoucherCollectionViewCell (IntPtr handle) : base (handle)
        {
        }

		public override void LayoutSubviews()
		{
			nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;

			Description.Frame = new CGRect(20, 220, UIScreen.MainScreen.Bounds.Width - 40, parent.View.Bounds.Height / 3);

			Barcode.Frame = new CGRect((ScreenWidth - Barcode.Frame.Width) / 2, 60, Barcode.Frame.Width, 120);

			EANLabel.Frame = new CGRect((ScreenWidth - EANLabel.Frame.Width) / 2, 50 + 130, EANLabel.Frame.Width, 70);


			BorderImage.Frame = new CGRect(0, 247, ScreenWidth, 5);
			BorderImage.Image = UIImage.FromBundle("Bullets");

			BackArrow.Frame = new CGRect(15, 10, 42, 42);
			BackArrow.TextColor = UIColor.FromRGB(217, 217, 217);

			ForwardArrow.Frame = new CGRect((ScreenWidth - 25), 10, 42, 42);
			ForwardArrow.TextColor = UIColor.FromRGB(217, 217, 217);

			VoucherBottom.Frame = new CGRect(0, 250, ScreenWidth, UIScreen.MainScreen.Bounds.Height - 250);
			VoucherBottom.BackgroundColor = UIColor.FromRGB(247, 247, 247);

			//ErrorLabel.Text = "Helaas, kan geen barcode genereren.";
			ErrorLabel.Frame = new CGRect(30, 60, (ScreenWidth - 60), 120);
			ErrorLabel.Font = UIFont.FromName("Avenir", 50f);
			ErrorLabel.AdjustsFontSizeToFitWidth = true;
			ErrorLabel.TextAlignment = UITextAlignment.Center;
			ErrorLabel.TextColor = UIColor.Black;

			WalletButton.Frame = new CGRect((ScreenWidth - 138) / 2, 10, 138, 44);
			WalletButton.SetImage(UIImage.FromBundle("IconWallet"), UIControlState.Normal);

			base.LayoutSubviews();
		}

        void WalletButtonClicked(){

            if (!string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.UserEmail)))
            {
                ShowPopupWallet(Item);
            }else
            {
                ShowWalletAlertMessage();
            }
        }

		public void LayoutSubviews(VoucherTableRow item, UIViewController parent, NSIndexPath rowPath)
		{

			nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
			Description.Frame = new CGRect(20, 220, UIScreen.MainScreen.Bounds.Width - 40, parent.View.Bounds.Height / 3);

			//ButtonPdf.Frame = new CGRect((UIScreen.MainScreen.Bounds.Width - 200) / 2, 380, 200, 40);
			//ButtonPdf.Layer.CornerRadius = ButtonPdf.Layer.Bounds.Height / 2;
			//ButtonPdf.Hidden = true;

			Barcode.Frame = new CGRect((parent.View.Frame.Width - Barcode.Frame.Width) / 2, 60, Barcode.Frame.Width, 120);

			EANLabel.Frame = new CGRect((parent.View.Frame.Width - EANLabel.Frame.Width) / 2, 50 + 130, EANLabel.Frame.Width, 70);


			BorderImage.Frame = new CGRect(0, 247, ScreenWidth, 5);
			BorderImage.Image = UIImage.FromBundle("Bullets");

			BackArrow.Frame = new CGRect(15, 10, 42, 42);
			BackArrow.TextColor = UIColor.FromRGB(217, 217, 217);

			ForwardArrow.Frame = new CGRect((ScreenWidth - 25), 10, 42, 42);
			ForwardArrow.TextColor = UIColor.FromRGB(217, 217, 217);

			//VoucherTop.Frame = new CGRect(0, 0, parent.View.Frame.Width, 250);
			//VoucherTop.BackgroundColor = UIColor.White;
			//SendSubviewToBack(VoucherTop);

			VoucherBottom.Frame = new CGRect(0, 250, ScreenWidth, UIScreen.MainScreen.Bounds.Height - 250);
			VoucherBottom.BackgroundColor = UIColor.FromRGB(247, 247, 247);

			//ErrorLabel.Text = "Helaas, kan geen barcode genereren.";
			ErrorLabel.Frame = new CGRect(30, 60, (ScreenWidth - 60), 120);
			ErrorLabel.Font = UIFont.FromName("Avenir", 50f);
			ErrorLabel.AdjustsFontSizeToFitWidth = true;
			ErrorLabel.TextAlignment = UITextAlignment.Center;
			ErrorLabel.TextColor = UIColor.Black;
		}

		public void UpdateArrows(VoucherTableRow item, UIViewController parent, NSIndexPath rowPath)
		{
			Voucherviewcontroller navctrlr = parent as Voucherviewcontroller;

            //if (!ArrowIsUpdated)
            //{

			int Position = item.Index;
			int LastVoucher = navctrlr.AmountVouchers - 1;
			Console.WriteLine("collection view position "+ Position + " amount of vouchers "+ LastVoucher);
			if (Position == LastVoucher)
			{
				BackArrow.Hidden = false;
				ForwardArrow.Alpha = 0;
				Console.WriteLine("last voucher reached");
			}
            if (item.Index == 0)
            {
                BackArrow.Hidden = true;
                ForwardArrow.Hidden = false;
            }
            else
            {
                BackArrow.Hidden = false;
                ForwardArrow.Hidden = false;
            }

		}


		public void UpdateCell(VoucherTableRow item, UIViewController parent, NSIndexPath rowPath, nfloat width)
		{

			//if(!IsUpdated){
			Voucherviewcontroller navctrlr = parent as Voucherviewcontroller;
			
            Item = item;

			Console.WriteLine("width collectionview "+width);
            Console.WriteLine("voucher item "+item);
            ErrorLabel.Frame = new CGRect(30, 60, (width - 60), 120);
			
				if (item.Description != null)
				{
					Description.Text = item.Description;
					Description.Font = UIFont.FromName("Avenir-Heavy", 30f);
					Description.TextAlignment = UITextAlignment.Center;
					Description.Lines = 3;

				}

				if (item.Barcode != null)
				{

					EANLabel.Text = item.Barcode;
					EANLabel.Font = UIFont.FromName("Avenir", 28f);
					EANLabel.AdjustsFontSizeToFitWidth = true;
					ErrorLabel.Text = item.Barcode;
				}
			if (!IsUpdated)
			{
				WalletButton.TouchUpInside += delegate
				{
					WalletButtonClicked();
			    };
				IsUpdated = true;
			}

		}

		public void UpdateBarcode(VoucherTableRow item)
		{
			SetBarcode(item.Barcode, item.BarcodeType);
		}

		private void SetBarcodeTest(string barcode, string format)
		{
		}

		private void SetBarcode(string barcode, string format)
		{
			// Create the barcode

			try
			{
				ZXing.BarcodeFormat barcodeFormat = new ZXing.BarcodeFormat();
                Console.WriteLine("format barcode "+format);
                switch (format)
				{
					case "EAN13":
						barcodeFormat = ZXing.BarcodeFormat.EAN_13;
                        BarcodeFormat = "EAN13";
						break;
					case "EAN8":
						barcodeFormat = ZXing.BarcodeFormat.EAN_8;
						break;
					case "CODE128":
						barcodeFormat = ZXing.BarcodeFormat.CODE_128;
                        BarcodeFormat = "CODE128";
						break;
					case "ALL1D":
						barcodeFormat = ZXing.BarcodeFormat.All_1D;
						break;
					case "AZTEC":
						barcodeFormat = ZXing.BarcodeFormat.AZTEC;
						break;
					case "CODABAR":
						barcodeFormat = ZXing.BarcodeFormat.CODABAR;
						break;
					case "CODE39":
						barcodeFormat = ZXing.BarcodeFormat.CODE_39;
						break;
					case "CODE93":
						barcodeFormat = ZXing.BarcodeFormat.CODE_93;
						break;
					case "DATAMATRIX":
						barcodeFormat = ZXing.BarcodeFormat.DATA_MATRIX;
						break;
					case "IMB":
						barcodeFormat = ZXing.BarcodeFormat.IMB;
						break;
					case "ITF":
						barcodeFormat = ZXing.BarcodeFormat.ITF;
						break;
					case "MAXICODE":
						barcodeFormat = ZXing.BarcodeFormat.MAXICODE;
						break;
					case "MSI":
						barcodeFormat = ZXing.BarcodeFormat.MSI;
						break;
					case "PDF417":
						barcodeFormat = ZXing.BarcodeFormat.PDF_417;
						break;
					case "PLESSEY":
						barcodeFormat = ZXing.BarcodeFormat.PLESSEY;
						break;
					case "QRCODE":
						barcodeFormat = ZXing.BarcodeFormat.QR_CODE;
						break;
					case "RSS14":
						barcodeFormat = ZXing.BarcodeFormat.RSS_14;
						break;
					case "RSSEXPANDED":
						barcodeFormat = ZXing.BarcodeFormat.RSS_EXPANDED;
						break;
					case "UPCA":
						barcodeFormat = ZXing.BarcodeFormat.UPC_A;
						break;
					case "UPCE":
						barcodeFormat = ZXing.BarcodeFormat.UPC_E;
						break;
					case "UPCEANEXTENSION":
						barcodeFormat = ZXing.BarcodeFormat.UPC_EAN_EXTENSION;
						break;
					default:
						break;
				}

				var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
				{

					Format = barcodeFormat,
					Options = new ZXing.Common.EncodingOptions
					{
						Width = 300,
						Height = 120,
						Margin = 0
					}
				};
                Barcode.Image = BarcodeWriter.Write(barcode);
				Barcode.Hidden = false;
				EANLabel.Hidden = false;
				ErrorLabel.Hidden = true;


			}
			catch (Exception ex)
			{

				Console.WriteLine("foutmelding barcode "+ ex);
				Barcode.Hidden = true;
				EANLabel.Hidden = true;
				ErrorLabel.Hidden = false;
                BarcodeFormat = "gift_actioncode";
			}

		}

		private void ShowPopupWallet(VoucherTableRow item)
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpWalletTitle;
			alert.AddButton("OK");
			alert.AddButton("Annuleren");
			alert.Message = "Wil je de voucher van "+item.Description+" aan je iOS Wallet toevoegen?";
			alert.AlertViewStyle = UIAlertViewStyle.Default;
            alert.Clicked += (object sender, UIButtonEventArgs e) => {
                ButtonWalletClicked(sender, e, item);
            };
			alert.Show();

		}

		void ButtonWalletClicked(object sender, UIButtonEventArgs e, VoucherTableRow item)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK buttonn
				AddToWallet(item);
				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Vouchers", "Toevoegen aan Wallet", "Accepteren", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else
			{

				// Cancel button

				// Analytics event
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Vouchers", "Toevoegen aan Wallet", "Weigeren", null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
		}

		async void AddToWallet(VoucherTableRow item)
		{
			SetLoader(AMStrings.LoadingWalletVoucher);

			WalletService walletService = new WalletService();
			string passType = "storeCard";
			string barcodeEncodingMessage = "iso-8859-1";
			string foregroundColor = "rgb(0, 125, 195)";
			string backgroundColor = "rgb(255, 255, 255)";
			string primaryKey = "voucher";
			string auxiliaryKey = "geldigheid";
			string dateStyle = "PKDateStyleShort";
            string latitude = AMMain.getLatitudePartner(item.PartnerName);
            string longitude = AMMain.getLongitudePartner(item.PartnerName);
			var resp = await walletService.addWallet(passType, item.Barcode, BarcodeFormat, barcodeEncodingMessage, latitude, longitude, "0", null, item.PartnerName, item.Description, item.PartnerName, foregroundColor, backgroundColor, primaryKey, "Beschrijving", item.Description, null, auxiliaryKey, auxiliaryKey, item.ValidityDate, false, dateStyle);
			string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			string localFilename = item.Barcode + ".pkpass";
			string localPath = Path.Combine(documentsPath, localFilename);
			if (resp != null)
			{
				try
				{
					File.WriteAllBytes(localPath, resp);
					Console.WriteLine("pass " + File.ReadAllBytes(localPath).Length);
					Console.WriteLine("pass filepath " + localPath);
					if (LoadingView != null)
					{
						LoadingView.RemoveFromSuperview();
					}
					AMMain.OpenPassbookViewController(parent.NavigationController, localPath, localFilename);
				}
				catch (Exception ex)
				{
					Console.WriteLine("exception add to wallet " + ex);
					if (LoadingView != null)
					{
						LoadingView.RemoveFromSuperview();
					}
				}
			}
			if (LoadingView != null)
			{
				LoadingView.RemoveFromSuperview();
			}
		}

		private void ShowWalletAlertMessage()
		{

			UIAlertView alert = new UIAlertView();
			alert.Title = AMStrings.PopUpWalletTitle;
			alert.AddButton("Naar gegevens");
			alert.AddButton("Annuleren");
			alert.Message = AMStrings.PopUpWalletVoucherAlertMessage;
			alert.AlertViewStyle = UIAlertViewStyle.Default;
			alert.Clicked += ButtonPopUpClicked;
			alert.Show();
		}

		void ButtonPopUpClicked(object sender, UIButtonEventArgs e)
		{
			UIAlertView parent_alert = (UIAlertView)sender;

			if (e.ButtonIndex == 0)
			{
				// OK button
				GegevensSegue();

			}
			else
			{

				// Cancel button

			}
		}

		void GegevensSegue()
		{
            if(parent.NavigationController as UINavigationController != null)
            {
                var NavController = parent.NavigationController as UINavigationController;
                NavController.PopToRootViewController(true);
            }	
		}

		void SetLoader(string text)
		{
			CGRect bounds = UIScreen.MainScreen.Bounds;

			//CGRect overlay = new CGRect(0, NavigationController.NavigationBar.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height + ButtonTabView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - (NavigationController.NavigationBar.Bounds.Height + ButtonTabView.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height));
			LoadingView = new LoadingOverlay(bounds, text);
			Add(LoadingView);
		}
	}
}