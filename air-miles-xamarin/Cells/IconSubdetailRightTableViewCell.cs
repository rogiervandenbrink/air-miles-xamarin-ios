﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class IconSubdetailRightTableViewCell : UITableViewCell
    {
        public IconSubdetailRightTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(TableRow item)
		{
			if (item.Icon != null)
			{
				Image.Image = UIImage.FromBundle(item.Icon);
			}

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}

			if (item.Detail != null)
			{
				Detail.Text = item.Detail;
			}

			if (item.Subdetail != null)
			{
				Subdetail.Text = item.Subdetail;
			}
		}
    }
}