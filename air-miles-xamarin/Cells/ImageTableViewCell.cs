﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class ImageTableViewCell : UITableViewCell
    {
        public ImageTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(TableRow item)
		{
			if (item.Icon != null)
			{
				//Image.Image = UIImage.FromBundle(item.Icon);
			}

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
		}
    }
}