using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

namespace airmilesxamarin
{
    public partial class CountryPickerTableViewCell : UITableViewCell
    {
        UIViewController Controller;

		public CountryPickerTableViewCell (IntPtr handle) : base (handle)
        {
        }


		public void UpdateCell(TableRow item, UIViewController parent)
		{

			Controller = parent;

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
			else
			{
				Title.Text = "";
			}

			if (item.InputText != null)
			{
				InputText.Text = item.InputText;
			}
			else {
				InputText.Text = "";
			}

			InputText.EditingDidBegin += delegate
			{
				InputText.TextColor = UIColor.Blue;

			}; ;

			InputText.EditingDidEnd += delegate
			{
				InputText.TextColor = UIColor.FromRGB(217, 217, 217);
			};

			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;
				if (navctrl.Opslaan)
				{
					//InputText.EditingDidBegin += (sender, e) => ShowCountryPicker(InputText);
					InputText.ShouldBeginEditing += OnCountryPickerShouldBeginEditing;
				}
			}

		}

		bool OnCountryPickerShouldBeginEditing (UITextField textField)
		{
			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;

				var customLandList = new List<string>();
				customLandList.Add("Nederland");
				customLandList.Add("België");
				customLandList.Add("Duitsland");
				customLandList.Add("Frankrijk");
				customLandList.Add("Luxemburg");

				var modalPickerLand = new ModalPickerViewController(ModalPickerType.Country, "Selecteer uw land", navctrl)
				{
					HeaderBackgroundColor = UIColor.FromRGB(48, 122, 187),
					HeaderTextColor = UIColor.White,
					TransitioningDelegate = new ModalPickerTransitionDelegate(),
					ModalPresentationStyle = UIModalPresentationStyle.Custom
				};

				modalPickerLand.PickerView.Model = new CountryPickerModel(customLandList);

				//On an item is selected, update our label with the selected item
				modalPickerLand.OnModalPickerDismissed += (s, ea) =>
				{
					var index = modalPickerLand.PickerView.SelectedRowInComponent(0);
					textField.Text = customLandList[(int)index];
					Gegevens.Land = customLandList[(int)index];
					if (customLandList[(int)index] == "Nederland")
					{
						navctrl.IsNotNederland = false;
						navctrl.land = "Nederland";
						navctrl.RefreshTableAanpassenItems();
					}
					else
					{
						navctrl.IsNotNederland = true;
						navctrl.land = customLandList[(int)index];
						navctrl.RefreshTableAanpassenItems();
					}
				};

				navctrl.PresentViewController(modalPickerLand, true, null);
			}
			return false;
		}
    }
}