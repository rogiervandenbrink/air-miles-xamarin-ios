﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class GreyIconDetailRightTableViewCell : UITableViewCell
    {
        UIViewController Controller;

		public GreyIconDetailRightTableViewCell(IntPtr handle) : base (handle)
		{
		}

		public void UpdateCell(TableRow item, UIViewController parent)
		{

			Controller = parent;

			if (item.Icon != null)
			{
				Icon.Image = UIImage.FromBundle("warning_icon");

			}

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}
			else
			{
				Title.Text = "";
			}

			if (item.Detail != null)
			{
				Detail.Text = item.Detail;
				Detail.TextColor = UIColor.Black;
			}
		}
    }
}