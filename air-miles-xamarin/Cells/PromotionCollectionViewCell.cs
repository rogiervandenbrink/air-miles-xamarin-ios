﻿using Foundation;
using System;
using UIKit;
using CoreGraphics;
using SDWebImage;
using System.Globalization;
using Newtonsoft.Json;
using Air_Miles_Xamarin;
using Newtonsoft.Json.Linq;
using System.Drawing;
using Google.Analytics;

namespace airmilesxamarin
{
    public partial class PromotionCollectionViewCell : UICollectionViewCell
    {

		UIView OfferActivationView, GiftActivationView, GiftUsedView;
		UILabel OfferActivationLabel, GiftActivationLabel, GiftUsedLabel;
		UILabel OfferActivationValue;
		UIImageView shellLogo;

		public bool RemovePriceView, RemovePropositionView;

		public PromotionCollectionViewCell(IntPtr handle) : base(handle)
		{
			DrawGiftActivation();
			DrawOfferActivation();
			DrawGiftUsed();
		}

		//public override CGSize SizeThatFits(CGSize size)
		//{
		//	Console.WriteLine("height text cell "+ResizeHeigthWithText(Title));
		//	CGSize sizeCell = new CGSize(UIScreen.MainScreen.Bounds.Width-40, ResizeHeigthWithText(Title)); 
		//	return sizeCell;	
		//}

		public void UpdateCell(Promotie item)
		{
			if (shellLogo != null)
			{
				shellLogo.RemoveFromSuperview();
			}
			RemovePriceView = true;
			RemovePropositionView = true;

			Title.Font = UIFont.FromName("Avenir-Black", 18f);
			Title.Lines = 0;
			Title.LineBreakMode = UILineBreakMode.WordWrap;
			Title.AdjustsFontSizeToFitWidth = true;
			Title.MinimumFontSize = 6f;


			if (item.Title != null)
			{
				Title.Text = AMMain.ReplaceBreakpoints(item.Title);

			}
			else {
				Title.Text = "";
			}

			if (item.Logo != null && item.ShowLogo == true)
			{
				Console.WriteLine("set logo" + item.Logo);
				Logo.SetImage(
					url: new NSUrl(item.Logo),
					placeholder: UIImage.FromBundle("")
				);
			}
			if (item.Image != null)
			{
				Image.SetImage(
					url: new NSUrl(item.Image),
					placeholder: UIImage.FromBundle("Airmiles_Placeholder_Promotion.png")
				);
				Image.Layer.MasksToBounds = true;
			}

			if (item.Logo != null)
			{
				Logo.SetImage(
					url: new NSUrl(item.Logo)
				);
				Logo.Layer.MasksToBounds = true;
				Logo.ContentMode = UIViewContentMode.ScaleAspectFit;
			}

			if (item.PriceTitle != null)
			{
				PriceTitle.Text = item.PriceTitle;
				if (item.PriceTitle != "")
				{
					PriceTitle.Text = AMMain.RemoveHTMLTags(item.PriceTitle);
					PriceTitle.AdjustsFontSizeToFitWidth = true;
					RemovePriceView = false;
				}
			}
			else if (item.PropositionValue != null)
			{
				if (item.PropositionValue != "")
				{
					PriceTitle.Text = AMMain.RemoveHTMLTags(item.PropositionValue);
					RemovePriceView = false;
				}
				else
				{
					PropositionTitle.Text = "";
				}
			}
			else {
				PriceTitle.Text = "";
			}

			if (item.PriceDetail != null)
			{
				if (item.PriceDetail != "")
				{
					PriceDetail.Text = AMMain.RemoveHTMLTags(item.PriceDetail);
					PriceDetail.AdjustsFontSizeToFitWidth = true;
					if (item.PriceDetail.Length >9)
					{
						PriceDetail.Lines = 2;
						PriceDetail.LineBreakMode = UILineBreakMode.WordWrap;
					}
					RemovePriceView = false;
				}
			}
			else if (item.PropositionLabel != null)
			{
				if (item.PropositionLabel != "")
				{
					PriceDetail.Text = item.PropositionLabel;
					PriceDetail.AdjustsFontSizeToFitWidth = true;
					if (item.PropositionLabel.Length > 9)
					{
						PriceDetail.Lines = 2;
						PriceDetail.LineBreakMode = UILineBreakMode.WordWrap;
					}
					RemovePriceView = false;
				}
				else
				{
					PriceDetail.Text = "";
				}
			}
			else {
				PriceDetail.Text = "";
			}

			if (item.MilesOnly == true)
			{
				PropositionTitle.Text = "MILES";
				PropositionDetail.Text = "ONLY";
				PropositionView.BackgroundColor = UIColor.FromRGB(221, 15, 150);

				RemovePropositionView = false;
			}
			else {
				if (item.PropositionTitle != null)
				{
					if (item.PropositionTitle != "")
					{
						PropositionTitle.Text = AMMain.RemoveHTMLTags(item.PropositionTitle);
						RemovePropositionView = false;
					}

				}
				else if (item.PriceValue != null)
				{
					if (item.PriceValue != "")
					{
						PropositionTitle.Text = AMMain.RemoveHTMLTags(item.PriceValue);
						RemovePropositionView = false;
					}
				}
				else
				{
					PropositionTitle.Text = "";
				}

				if (item.PropositionDetail != null)
				{
					if (item.PropositionDetail != "")
					{
						PropositionDetail.Text = item.PropositionDetail;
						RemovePropositionView = false;
					}
				}
				else if (item.PriceLabel != null)
				{
					if (item.PriceLabel != "")
					{
						PropositionDetail.Text = AMMain.RemoveHTMLTags(item.PriceLabel);
						RemovePropositionView = false;
					}
				}
				else
				{
					PropositionDetail.Text = "";
				}
				PropositionView.BackgroundColor = UIColor.FromRGB(0, 163, 255);
			}

			if (RemovePriceView == true)
			{
				PriceView.Hidden = true;
			}
			else {
				PriceView.Hidden = false;
			}

			if (RemovePropositionView == true)
			{
				PropositionView.Hidden = true;
			}
			else {
				PropositionView.Hidden = false;
			}

			if (!string.IsNullOrEmpty(item.RegistrationDate) && !string.IsNullOrEmpty(item.EndDate) && item.RegistrationDate != item.EndDate)
			{
				try
				{
				//string date = item.EndDate;
				long dateTime = Convert.ToInt64(item.EndDate);
				DateTime enddate = SharedDatetime.ConvertFromUnixTimestamp(dateTime);
				CultureInfo arSA = AMMain.Cultureinfo;
				//DateTime oDate = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm:ss", arSA);
					string dateString = "Te activeren tot " + enddate.ToString("dd\\/MM\\/yyyy", arSA).Replace("/", "-");


				OfferActivationValue.Text = dateString;

				OfferActivationView.Hidden = false;
					}
				catch (Exception ex)
				{

				}
			}
			else {
				OfferActivationView.Hidden = true;
			}
			Console.WriteLine("promotiontype "+item.PromotionType);
			if (item.PromotionType == PromotieType.MarketingPromotion)
			{
				GiftActivationView.Hidden = true;

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Persoonlijk aanbod", "Bekijk overige details", item.Title, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			else if (item.PromotionType == PromotieType.Offer)
			{
				UpdateOffer(item.GiftsOffer);
			}
			if (item.PromotionType == PromotieType.PersonalizedPromotion)
			{
				PersonalizedPromotion personalizedPromotion = JsonConvert.DeserializeObject<PersonalizedPromotion>(item.PersonalizedPromotionJson); 
				Console.WriteLine("item.PersonalizedPromotion "+item.PersonalizedPromotionJson);
				UpdatePersonalizedPromotion(personalizedPromotion, item);

				// Google analaytics
				var GAItabkeuze = DictionaryBuilder.CreateEvent("Persoonlijk aanbod", "Bekijk Air Miles details", item.Title, null).Build();
				Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
				Gai.SharedInstance.Dispatch();
			}
			if (item.PromotionType == PromotieType.Gift)
			{
				GiftsOffer offer = JsonConvert.DeserializeObject<GiftsOffer>(item.OfferJson);
				GiftsOfferGift gift = JsonConvert.DeserializeObject<GiftsOfferGift>(item.OfferGiftJson);
				UpdateGift(offer, gift, item);
			}
		}

		public void StyleCell()
		{
			// Cell styling
			Layer.CornerRadius = 4;
			Layer.MasksToBounds = false;
			Layer.ShadowColor = new CGColor(0, 0, 0);
			Layer.ShadowRadius = 1;
			Layer.ShadowOpacity = 0.1f;
			Layer.ShadowOffset = new CGSize(0, 1);

			// ContentView styling
			ContentView.Layer.CornerRadius = 4;
			ContentView.Layer.MasksToBounds = true;

			// PriceView styling
			PriceView.Frame = new CGRect(PriceView.Frame.X, PriceView.Frame.Y, 90, 90);
			PriceView.Layer.CornerRadius = PriceView.Bounds.Height / 2;

			// PropositionView styling
			PropositionView.Layer.CornerRadius = PropositionView.Bounds.Height / 2;
		}

		public void UpdateOffer(GiftsOffer offer)
		{
			Console.WriteLine("offer "+JsonConvert.SerializeObject(offer));
			if (offer != null)
			{
				if (offer.Partner == "SHLL")
				{
					string JsonShellPartner = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.JsonHippoGifts + "-SHLL");
					if (!string.IsNullOrEmpty(JsonShellPartner))
					{
						string headerImage = JToken.Parse(JsonShellPartner).Value<string>("partnerProgramLogo");
						shellLogo = new UIImageView(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Width * 0.1521739));
						shellLogo.Image = FromUrl(headerImage);
						AddSubview(shellLogo);
						Image.Frame = new CGRect(0, (GiftActivationView.Bounds.Width * 0.1521739), this.Frame.Width, this.Frame.Width);
						Image.TranslatesAutoresizingMaskIntoConstraints = true;
						Image.Layer.MasksToBounds = true;

						Title.Frame = new CGRect(Title.Frame.X, Image.Frame.Height + shellLogo.Frame.Height + 5, this.Frame.Width - 30, this.Frame.Height - (Image.Frame.Height + shellLogo.Frame.Height + 20));
						Title.TranslatesAutoresizingMaskIntoConstraints = true;

					}
					else
					{
						Image.Frame = new CGRect(0, 0, this.Frame.Width, this.Frame.Width);
						Image.TranslatesAutoresizingMaskIntoConstraints = true;
						Image.Layer.MasksToBounds = true;

						Title.Frame = new CGRect(Title.Frame.X, Image.Frame.Height + 5, this.Frame.Width - 30, this.Frame.Height - (Image.Frame.Height + 20));
						Title.TranslatesAutoresizingMaskIntoConstraints = true;

					}
                    GiftUsedView.Hidden = true;
					GiftActivationView.Hidden = true;
					Logo.Hidden = true;
				}
				if (offer.Partner != "SHLL")
				{
					if (shellLogo != null)
					{
						shellLogo.RemoveFromSuperview();
					}
				}
				else if (offer.Activated == "true" && offer.OfferUsed == "false")
				{
					SetActivationDate(DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", null));

					GiftActivationView.Hidden = false;
                    OfferActivationView.Hidden = true;
                    GiftUsedView.Hidden = true;

				}
				else {
					GiftActivationView.Hidden = true;
                    GiftUsedView.Hidden = true;
				}

				if (/*offer.Activated == "true" &&*/ offer.OfferUsed == "true")
				{
					//ContentView.Alpha = 0.3f;
					GiftActivationView.Hidden = true;
                    GiftUsedView.Hidden = true;
				}
				else {
					ContentView.Alpha = 1f;
				}
			}
			else {
				ContentView.Alpha = 1f;
				GiftActivationView.Hidden = true;
                GiftUsedView.Hidden = true;

			}
		}

		public void SetActivationDate(DateTime To)
		{
			Console.WriteLine("enddate "+To.ToString("F"));

			DateTime Now = DateTime.Now;
			Console.WriteLine("now "+Now);
			Console.WriteLine("to - now milliseconds "+ (int)Math.Ceiling((To - Now).TotalMinutes));

			string DaysBetween = "";
			string HoursBetween = "";
			string MinutesBetween = "";

			int d;
			int h;
			int m;

			if (To < Now)
			{
				d = 0;
			}
			else if ((int)Math.Ceiling((To - Now).TotalMinutes)<3600 && (int)Math.Ceiling((To - Now).TotalMinutes)>0)
			{
				if ((int)Math.Ceiling((To - Now).TotalMinutes) < 60)
				{
					m = (int)Math.Ceiling((To - Now).TotalMinutes);
					MinutesBetween = "Geactiveerd: Nog ";
					if (m == 1)
						MinutesBetween += (m + " minuut");
					else
						MinutesBetween += (m + " minuten");
					MinutesBetween += " geldig";

					GiftActivationLabel.Text = MinutesBetween.ToUpper();
				}
				if ((int)Math.Ceiling((To - Now).TotalMinutes) > 60)
				{
					h = (int)Math.Ceiling((To - Now).TotalHours);
					HoursBetween = "Geactiveerd: Nog ";
					if (h == 1)
						HoursBetween += (h + " uur");
					else
						HoursBetween += (h + " uur");
					HoursBetween += " geldig";

					GiftActivationLabel.Text = HoursBetween.ToUpper();
				}
			}
			else
			{
				d = (int)Math.Ceiling((To - Now).TotalDays);
				DaysBetween = "Geactiveerd: Nog ";
				if (d == 1)
					DaysBetween += (d + " dag");
				else
					DaysBetween += (d + " dagen");
				DaysBetween += " geldig";

				GiftActivationLabel.Text = DaysBetween.ToUpper();
			}

			// increase the day with 1 since day is valid t/m
			//d++;


		}

		public void UpdatePersonalizedPromotion(PersonalizedPromotion personalizedPromotion, Promotie promotie)
		{
			Console.WriteLine("personalizedPromotion EnrollerdMember "+promotie.EnrolledMember);
			Console.WriteLine("personalizedPromotion EndDateCampaign " + promotie.EndDateCampaign);
            if (shellLogo != null)
			{
				shellLogo.RemoveFromSuperview();
			}
			if (personalizedPromotion.ActivateCompound != null)
			{
				Console.WriteLine("personalizedPromotion ActivateCompound & EnrollerdMember " + promotie.EnrolledMember);
                //essent
                if (!string.IsNullOrEmpty(promotie.Used))
                {
                    GiftActivationView.Hidden = true;
                    GiftUsedView.Hidden = false;
                    OfferActivationView.Hidden = true;
                }
                if (promotie.EnrolledMember && !string.IsNullOrEmpty(promotie.EndDateCampaign))
				{
					Console.WriteLine("geactiveerd "+ personalizedPromotion.Title);
					Console.WriteLine("personalizedPromotion EndDateCampaign " + promotie.EndDateCampaign);
                    Console.WriteLine("personalizedPromotion Registration DateCampaign " + promotie.RegistrationDate);
					try
					{
						long date = Convert.ToInt64(promotie.RegistrationDate);
						DateTime enDate = SharedDatetime.ConvertFromUnixTimestamp(date);
						SetActivationDate(enDate);
					}
					catch (FormatException e)
					{
						Console.WriteLine(e.Message);
					}
					//SetActivationDate(DateTime.ParseExact(promotie.EndDateCampaign, "dd/MM/yyyy HH:mm:ss", AMMain.Cultureinfo));
					GiftActivationView.Hidden = false;
                    OfferActivationView.Hidden = true;
				}
				else {
					GiftActivationView.Hidden = true;
                    GiftUsedView.Hidden = true;
				}
			}

			else {
				GiftActivationView.Hidden = true;
                GiftUsedView.Hidden = true;
			}

			ContentView.Alpha = 1f;

		}

		public void UpdateGift(GiftsOffer offer, GiftsOfferGift gift, Promotie promotie)
		{
			// set gift
			if (gift != null)
			{
				if (shellLogo != null)
				{
					shellLogo.RemoveFromSuperview();
				}
				if (promotie.EnrolledMember && !string.IsNullOrEmpty(promotie.EndDateCampaign) || offer.Activated == "true" && offer.OfferUsed == "false" && gift.GiftChosen == "true")
				{
					DateTime To = DateTime.ParseExact(offer.OfferValidTo, "dd-MM-yyyy HH:mm:ss", AMMain.Cultureinfo);
					DateTime Now = DateTime.Now;

					int d;

					if (To < Now)
						d = 0;
					else
						d = (int)Math.Ceiling((To - Now).TotalDays);

					string DaysBetween = "Geactiveerd: Nog ";
					if (d == 1)
						DaysBetween += (d + " dag");
					else
						DaysBetween += (d + " dagen");
					DaysBetween += " geldig";

					GiftActivationLabel.Text = DaysBetween.ToUpper();
					GiftActivationView.Hidden = false;
                    OfferActivationView.Hidden = true;
                    GiftUsedView.Hidden = true;

				}
				else {
					GiftActivationView.Hidden = true;
                    GiftUsedView.Hidden = true;

				}
			}
			else {
				GiftActivationView.Hidden = true;
                GiftUsedView.Hidden = true;

			}
		}

		public void DrawOfferActivation()
		{
			OfferActivationView = new UIView(new CGRect(-10, (ContentView.Bounds.Width / 2) - 35, 260, 70));
			OfferActivationView.BackgroundColor = UIColor.FromRGBA(239, 106, 10, 200);
			OfferActivationView.Layer.CornerRadius = 5f;

			OfferActivationLabel = new UILabel(new CGRect(20, 5, UIScreen.MainScreen.Bounds.Width - 60, 35))
			{
				MinimumFontSize = 22f,
				TextAlignment = UITextAlignment.Left,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Heavy", 24f),
				Lines = 1,
				Text = "Let op!"
			};

			OfferActivationValue = new UILabel(new CGRect(20, 34, UIScreen.MainScreen.Bounds.Width - 60, 35))
			{
				MinimumFontSize = 14f,
				TextAlignment = UITextAlignment.Left,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Medium", 16f),
				Lines = 1,
				Text = "Te activeren tot "
			};

			ContentView.AddSubview(OfferActivationView);
			OfferActivationView.AddSubviews(OfferActivationLabel, OfferActivationValue);
		}

		public void DrawGiftActivation()
		{
			GiftActivationView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width - 40, 30));
			GiftActivationView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

			GiftActivationLabel = new UILabel(new CGRect(0, 0, GiftActivationView.Bounds.Width, GiftActivationView.Bounds.Height))
			{
				MinimumFontSize = 12f,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White,
				Font = UIFont.FromName("Avenir-Black", 14f),
				Lines = 1,
				Text = "GEACTIVEERD"
			};

			ContentView.AddSubview(GiftActivationView);
			GiftActivationView.AddSubviews(GiftActivationLabel);

		}

		public void DrawGiftUsed()
		{
            GiftUsedView = new UIView(new CGRect(0, 0, UIScreen.MainScreen.Bounds.Width - 40, 30));
            GiftUsedView.BackgroundColor = UIColor.FromRGB(239, 106, 10);

            GiftUsedLabel = new UILabel(new CGRect(0, 0, GiftUsedView.Bounds.Width, GiftUsedView.Bounds.Height))
            {
                MinimumFontSize = 12f,
                TextAlignment = UITextAlignment.Center,
                TextColor = UIColor.White,
                Font = UIFont.FromName("Avenir-Black", 14f),
                Lines = 1,
                Text = "ONTVANGEN"
            };

            ContentView.AddSubview(GiftUsedView);
            GiftUsedView.AddSubviews(GiftUsedLabel);
            GiftUsedView.Hidden = true;
		}

		public void SetImage(string image)
		{
			Console.WriteLine("set footer image to " + image);
			if (!string.IsNullOrEmpty(image))
			{
				FromUrl(image);
			}
		}

		static UIImage FromUrl(string uri)
		{
			using (var url = new NSUrl(uri))
			using (var data = NSData.FromUrl(url))
				return UIImage.LoadFromData(data);
		}

		int ResizeHeigthWithText(UILabel label, int maxHeight = 900)
		{
			int width = (int)label.Frame.Width;
			var size = ((NSString)label.Text).StringSize(label.Font, new CGSize(width, maxHeight), //new Size(width, 100),
			UILineBreakMode.WordWrap);
			var labelFrame = label.Frame;
			return (int)size.Height;
			//labelFrame.Size = new CGSize(width, size.Height);
			//label.Frame = labelFrame;
		}

	}
}
 