﻿using Foundation;
using System;
using UIKit;
using SDWebImage;

namespace airmilesxamarin
{
    public partial class IconTableViewCell : UITableViewCell
    {
        public IconTableViewCell (IntPtr handle) : base (handle)
        {
        }

		public void UpdateCell(TableRow item)
		{
			if (item.Icon != null)
			{
				if (item.Icon.Contains("http"))
				{
					Image.SetImage(
						url: new NSUrl(item.Icon),
						placeholder: UIImage.FromBundle("placeholder.png")
					);
				}
				else {
					Image.Image = UIImage.FromBundle(item.Icon);
				}
			}

			if (item.Title != null)
			{
				Title.Text = item.Title;
			}


		}
    }
}