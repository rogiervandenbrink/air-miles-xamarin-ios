﻿using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Text.RegularExpressions;
using CoreGraphics;

namespace airmilesxamarin
{
	partial class IconDetailRightTableViewCell : UITableViewCell
	{

		UIViewController Controller;

		public IconDetailRightTableViewCell (IntPtr handle) : base (handle)
		{
		}

		public void UpdateCell(TableRow item, UIViewController parent, string CellIdentifier)
		{

			Controller = parent;

			if (item.Icon != null) {
				Icon.Image = UIImage.FromBundle("warning_icon");

			}

			if (item.Title != null)
			{
				Title.Text = item.Title;

			}
			else
			{
				Title.Text = "";
			}

			if (CellIdentifier == "IconDetailRightTableViewCell")
			{
				UserInteractionEnabled = true;

				if (item.Icon != null)
				{
					Image.Image = UIImage.FromBundle(item.Icon);
				}

				if (item.Title != null)
				{
					Title.Text = item.Title;
				}

				if (item.Detail != null)
				{
					Detail.Text = item.Detail;
				}
			}
				if (CellIdentifier == "IconDetailRightTableViewCellDisabled")
				{
					UserInteractionEnabled = false;
					if (item.Icon != null)
					{
						Image.Image = UIImage.FromBundle(item.Icon);
					}

					if (item.Detail != null)
					{
						Detail.Text = item.Detail;
					}
					else {
						Detail.Text = "";
				}
			}



			if (Controller.GetType() == typeof(GegevensViewController))
			{
				GegevensViewController navctrl = Controller as GegevensViewController;


				/*if (item.Title == "Aanhef")
				{

					if (Detail.Text.Length == 0)
					{
						ErrorCell();
						Detail.Text = "Aanhef onjuist";

					}
					else
					{
						ValidCell();
					}

				}
				if (item.Title == "Voornaam")
				{

					if (Detail.Text.Length == 0)
					{
						ErrorCell();
						Detail.Text = "Voornaam onjuist";

					}
					else
					{
						ValidCell();
					}

				}
				if (item.Title == "Voorletters")
				{

					if (Detail.Text.Length == 0)
					{
						Detail.Text = "Voorletters ontbreken";
						ErrorCell();

					}
					else
					{
						ValidCell();
					}
				}

				if (item.Title == "Achternaam")
				{
					if (Detail.Text.Length == 0)
					{
						Detail.Text = "Achternaam onjuist";
						ErrorCell();

					}
					else
					{
						ValidCell();
					}
				}

				if (item.Title == "Geboortedatum")
				{
					if (Detail.Text.Length == 0)
					{
						Detail.Text = "Datum onjuist";
						ErrorCell();

					}
					else
					{
						ValidCell();
					}
				}*/

				if (item.Title == "E-mail")
				{
					//if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.EmailBouncePresent))
					//{
					//	Detail.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.EmailBounceReason);
					//	ErrorCell();

					//}
					//else
					//{
						ValidCell(item);
					//}


				}
				if (item.Title == "Telefoon")
				{

					//if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.PrivateNrBouncePresent))
					//{
					//	Detail.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.PrivatePhoneBounceReason);
					//	ErrorCell();
					//}
					//else
					//{
						ValidCell(item);
					//}


				}
				if (item.Title == "Mobiel")
				{
					//if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.MobileNrBouncePresent))
					//{
					//	Detail.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.MobilePhoneBounceReason);
					//	ErrorCell();

					//}
					//else
					//{
						ValidCell(item);
					//}
				}
				if (item.Title == "Straat")
				{
					//if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.AddressBouncePresent))
					//{
					//	Detail.Text = NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.AddressBounceReason);
					//	ErrorCell();

					//}
					//else
					//{
						ValidCell(item);
					//}
				}
				/*if (item.Title == "Huisnummer")
				{
					if (Detail.Text.Length == 0)
					{
						Detail.Text = "Nummer onjuist";
						ErrorCell();

					}
					else
					{
						ValidCell();
					}
				}

				if (item.Title == "Plaats")
				{
					if (Detail.Text.Length == 10)
					{
						Detail.Text = "Plaats onjuist";
						ErrorCell();

					}
					else
					{
						ValidCell();
					}
				}
				if (item.Title == "Postcode")
				{

					if (Regex.Match(Detail.Text, "^[1-9][\\d]{3}\\s?(?!(sa|sd|ss|SA|SD|SS))([a-eghj-opr-tv-xzA-EGHJ-OPR-TV-XZ]{2})?$").Success)
					{

						ValidCell();
					}
					else
					{
						Detail.Text = "Postcode onjuist";
						ErrorCell();
					}

				}*/

			}
		}

		public void ErrorCell()
		{
			Title.Frame = new CGRect(50, Title.Frame.Y, Title.Frame.Width, Title.Frame.Height);
			Title.TranslatesAutoresizingMaskIntoConstraints = true;
			Title.TextColor = UIColor.FromRGB(242, 42, 94);
			Icon.Image = UIImage.FromBundle("warning_icon_air");
			Icon.TintColor = UIColor.FromRGB(242, 42, 94);
			Icon.Hidden = false;
			Detail.TextColor = UIColor.FromRGB(242, 42, 94);

			Console.WriteLine("error cell");
		}

		public void ValidCell(TableRow item)
		{
			Title.Frame = new CGRect(15, Title.Frame.Y, Title.Frame.Width, Title.Frame.Height);
			Title.TranslatesAutoresizingMaskIntoConstraints = true;
			Title.TextColor = UIColor.Black;
			Icon.Hidden = true;
			Detail.TextColor = UIColor.FromRGB(217, 217, 217);
			Console.WriteLine("valid cell");
				if (item.Title != null)
				{
					Title.Text = item.Title;
					Title.TextColor = UIColor.LightGray;
				}

				if (item.Detail != null)
				{
					Detail.Text = item.Detail;
				}	


		}
	}
}
