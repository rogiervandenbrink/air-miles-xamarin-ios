﻿using System;
using Foundation;
using Google.Analytics;
using UIKit;
using Xamarin;
using MappBinding;
using Air_Miles_Xamarin;
using airmilesxamarin.Helpers;
using System.Linq;
using MessageUI;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using NotificationCenter;
using UserNotifications;

namespace airmilesxamarin
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("App")]
    public class App : UIApplicationDelegate
    {
        // class-level declarations
        public static UILocalNotification Notification = new UILocalNotification();
        public static UIStoryboard Storyboard = UIStoryboard.FromName("Main", null);

        public static UINavigationController NavController;

        public static UIViewController ViewCtrl { get; set; }
        public static UINavigationController LeftOnSecure { get; set; }
        public static UITabBarController TabBar { get; set; }

        AppoxeeNotificationDelegate Appoxeedelegate;

        public override UIWindow Window
        {
            get;
            set;
        }

        const string AllowTrackingKey = "AllowTracking";
        public ITracker Tracker;

        public static readonly string TrackingId_accp = "UA-60201328-7"; // accp
        public static readonly string TrackingId_prod = "UA-60201328-6"; //producti

        public static readonly string Mapp_Key_Dev = "59c0cc9ba51335.88582385";
        public static readonly string Mapp_Key_Prod = "59c0ca41d857b8.26112853";
        public static readonly string Mapp_Secret_Key_Dev = "59c0cc9ba51549.90814929";
        public static readonly string Mapp_Secret_Key_Prod = "59c0ca41d85893.08748681";
        public static readonly string Mapp_Key_Test = "59c1124745d544.20738241";

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method

            //appoxee
#if DEBUG
            if (!SharedWebUrls.ISACCP)
            {
                Appoxee.Shared().EngageWithLaunchOptions(launchOptions, Appoxeedelegate, Mapp_Key_Dev);
            }
#elif (!DEBUG)
            if(!SharedWebUrls.ISACCP){
                Appoxee.Shared().EngageWithLaunchOptions(launchOptions, Appoxeedelegate, Mapp_Key_Prod);
            }
#endif

            //user notifications center for push notifications in foreground
            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) => {
                // Handle approval
            });

            UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

            if (string.IsNullOrEmpty(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SessionDateTimeClose)))             {                 NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.AddSeconds(-11).ToString(), AMLocalStorage.SessionDateTimeClose);             }              NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.Session);              if (App.LeftOnSecure != null)             {                 Console.WriteLine("App had left on secure page = " + App.LeftOnSecure.GetType().ToString());             }

            var TabBarItemTextAttributesNormal = new UITextAttributes()
            {
                Font = UIFont.FromName("Avenir-Book", 11),
                TextShadowColor = UIColor.FromRGB(200, 200, 200),
                TextColor = UIColor.Gray
            };
            var TabBarItemTextAttributesSelected = new UITextAttributes()
            {
                Font = UIFont.FromName("Avenir-Book", 11),
                TextShadowColor = UIColor.FromRGB(200, 200, 200),
                TextColor = UIColor.FromRGB(0,125,195)
            };
            var NavBarButtonTextAttributes = new UITextAttributes()
            {
                Font = UIFont.FromName("Avenir-Heavy", 17),
                TextShadowColor = UIColor.Gray,
                TextColor = UIColor.White
            };

            // Set proporties
            UITabBarItem.Appearance.SetTitleTextAttributes(TabBarItemTextAttributesNormal, UIControlState.Normal);
            UITabBarItem.Appearance.SetTitleTextAttributes(TabBarItemTextAttributesSelected, UIControlState.Selected);
            UITabBar.Appearance.SelectedImageTintColor = UIColor.FromRGB(0, 125, 195);

            UIBarButtonItem.AppearanceWhenContainedIn(typeof(UINavigationBar)).SetTitleTextAttributes(NavBarButtonTextAttributes, UIControlState.Normal);

            // We use NSUserDefaults to store a bool value if we are tracking the user or not 
            var optionsDict = NSDictionary.FromObjectAndKey(new NSString("YES"), new NSString(AllowTrackingKey));
            NSUserDefaults.StandardUserDefaults.RegisterDefaults(optionsDict);

            // User must be able to opt out of tracking
            Gai.SharedInstance.OptOut = !NSUserDefaults.StandardUserDefaults.BoolForKey(AllowTrackingKey);

            // Initialize Google Analytics with a 5-second dispatch interval (Use a higher value when in production). There is a
            // tradeoff between battery usage and timely dispatch.
            Gai.SharedInstance.DispatchInterval = 5;
            Gai.SharedInstance.TrackUncaughtExceptions = true;

            string TrackingId = TrackingId_accp;

            if(!SharedWebUrls.ISACCP){
                TrackingId = TrackingId_prod;
            }
            Tracker = Gai.SharedInstance.GetTracker("Air Miles", TrackingId);

            AMMain.GetGiftsForBadge(null);

            //app center initialization
            AppCenter.Start("7c39498c-e545-4daa-9048-c8e315c05a8d", typeof(Analytics), typeof(Crashes));

            return true;
        }

        public override void OnResignActivation (UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground (UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.

            SessionClose();
        }

        public override void WillEnterForeground (UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
            NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.NewSessionAanbod);

            //redirect to SaldoTableViewController?
            if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.FirstKaartAanvragen))
            {
                if (!NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.ExternalLink))
                {
                    if (AMMain.RedirectToCard())
                    {
                        Console.WriteLine("redirect to kaart");
                        TabController Redirect = Storyboard.InstantiateViewController("TabController") as TabController;
                        Window.RootViewController = Redirect;
                    }
                }else
                {
                    NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.ExternalLink);
                }
            }

            //check session
            //session = 5 min = 300 sec
            int SessionSeconds = 300; 
            try
            {
                if (Convert.ToDateTime(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.SessionDateTimeClose)) < DateTime.Now.AddSeconds(-SessionSeconds))                 {                     NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.Session);                 }                 else                 {                     if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))                     {                         NSUserDefaults.StandardUserDefaults.SetBool(true, AMLocalStorage.Session);                     }                 }
            }
            catch (Exception ex)
            {
                Console.WriteLine("set session exception " + ex);
                NSUserDefaults.StandardUserDefaults.SetBool(false, AMLocalStorage.Session);
            }

            if (App.LeftOnSecure != null)
            {
                Console.WriteLine("App had left on secure page = " + App.LeftOnSecure.GetType().ToString());
            }

            //routing
            string Routing = "";
            if (App.LeftOnSecure != null)
            {
                Console.WriteLine("show expired session");
                if (App.LeftOnSecure.GetType() == typeof(TransactiesNavigationController))
                {
                    Routing = "Dismiss";
                }
                if (App.LeftOnSecure.GetType() == typeof(MijnNavigationViewController))
                {
                    Routing = "Dismiss";
                }
                else if (App.LeftOnSecure.GetType() == typeof(MeerNavigationViewController))
                {
                    Routing = "Dismiss";
                }
                AMMain.SetExpiredSession(App.LeftOnSecure, Routing);
            }

        }

        public override void OnActivated (UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.

            //only use push if connected to prod
            if (!SharedWebUrls.ISACCP)
            {
                if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)))
                {
                    if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin))
                    {
                        Appoxee.Shared().SetDeviceAlias(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true), (NSError arg0, NSObject arg1) =>
                        {
                            Console.WriteLine("Appoxee device alias set");
                        });
                    }
                    else
                    {
                        Appoxee.Shared().RemoveDeviceAliasWithCompletionHandler((arg0, arg1) =>
                        {
                            Console.WriteLine("Appoxee device alias removed");
                        });
                    }
                }
            }
        }

        public override void WillTerminate (UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.

            SessionClose();
        }

        private void SessionClose()
        {
            if (NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.HasLogin) == true)
            {
                Console.WriteLine("Closing app at " + DateTime.Now.ToString());

                NSUserDefaults.StandardUserDefaults.SetString(DateTime.Now.ToString(), AMLocalStorage.SessionDateTimeClose);
            }
        }

        private async void GetShellPartnerGifts()
        {
            GiftsOfferPartner PartnerGifts = await GiftsService.GetGiftsFromPartner("SHLL");
        }

        //push notification funcs
        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Appoxee.Shared().DidRegisterForRemoteNotificationsWithDeviceToken(deviceToken);
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            Appoxee.Shared().ReceivedRemoteNotification(userInfo);
        }

        public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
        {
            Appoxee.Shared().DidRegisterUserNotificationSettings(notificationSettings);
        }

        //deep linking
        public override bool OpenUrl(UIApplication app, NSUrl url, string sourceApp, NSObject annotation)
        {
            try
            {
                if (url != null)
                {
                    var rurl = new Rivets.AppLinkUrl(url.ToString());
                    //do stuff with url
                    string UrlString = url.ToString();
                    string Parameter = rurl.InputUrl.AbsolutePath;
                    string id = string.Empty;
                    string title = "Aanbod";
                    if (rurl.InputQueryParameters.ContainsKey("cat"))
                        id = rurl.InputQueryParameters["cat"];
                    if (rurl.InputQueryParameters.ContainsKey("title"))
                        title = rurl.InputQueryParameters["title"];

                    // Analytics event
                    var GAItabkeuze = DictionaryBuilder.CreateEvent("Main", "Open deepling", Parameter, null).Build();
                    Gai.SharedInstance.DefaultTracker.Send(GAItabkeuze);
                    Gai.SharedInstance.Dispatch();

                    switch (Parameter)
                    {
                        case "/persoonlijkaanbod":
                            LinktoPersonalPromotions();
                            break;
                        case "/aanbod":
                            LinktoAanbodCategories(id, title);
                            break;
                        case "/aankopen":
                            LinktoTransactions(true);
                            break;
                        case "/transacties":
                            LinktoTransactions(false);
                            break;
                        case "/geldigheid":
                            LinktoValidity();
                            break;
                        case "/gegevens":
                            LinktoProfile();
                            break;
                        case "/mastercard":
                            LinktoMastercard();
                            break;
                        default:
                            if (NavController != null)
                            {
                                NavController.PopToRootViewController(true);
                            }
                            break;
                    }
                }
                if (NavController != null)
                {
                    NavController.PopToRootViewController(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception deeplink "+ex);
                if (NavController != null)
                {
                    NavController.PopToRootViewController(true);
                }
            }

            return true;
        }

        void LinktoPersonalPromotions()
        {
            UINavigationController NavCtrl = TabBar.ViewControllers[1] as UINavigationController;
            if (!string.IsNullOrEmpty(KeychainHelpers.GetPasswordForUsername(NSUserDefaults.StandardUserDefaults.StringForKey(AMLocalStorage.Username), AMLocalStorage.SecureZMemberId, true)) && NSUserDefaults.StandardUserDefaults.BoolForKey(AMLocalStorage.Session))
            {
                PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;

                Redirect.Title = "Speciaal voor jou!";
                Redirect.PersonalGifts = true;
                NavCtrl.PushViewController(Redirect, true);
            }
            else
            {
                LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController;
                Redirect.Routing = "AanbodViewController";
                NavCtrl.PushViewController(Redirect, true);
            }
            TabBar.SelectedIndex = 1;
        }

        async void LinktoAanbodCategories(string category, string title)
        {
            UINavigationController NavCtrl = TabBar.ViewControllers[1] as UINavigationController;

            PromotiesService PromotiesService = new PromotiesService();
            AanbodPromoties AanbodPromoties = await PromotiesService.GetAanbod(category);
            if (AanbodPromoties.Promoties != null)
            {
                Console.WriteLine("Updated promotions: " + AanbodPromoties.Promoties.Length);
                PromotiesViewController Redirect = App.Storyboard.InstantiateViewController("AanbodDetailViewController") as PromotiesViewController;
                Redirect.Title = title;

                Redirect.SetPromotions(AanbodPromoties.Promoties);
                NavCtrl.PushViewController(Redirect, true);
            }

            TabBar.SelectedIndex = 1;

        }

        void LinktoTransactions(bool Purchases)
        {
            UINavigationController NavCtrl = TabBar.ViewControllers[2] as UINavigationController;

            TransactieAankoopViewController Redirect = App.Storyboard.InstantiateViewController("TransactieAankoopViewController") as TransactieAankoopViewController;
            Redirect.Aankopen = Purchases;
            NavCtrl.PushViewController(Redirect, true);

            TabBar.SelectedIndex = 2;
        }

        void LinktoValidity()
        {
            UINavigationController NavCtrl = TabBar.ViewControllers[2] as UINavigationController;

            ValidityViewController Redirect = App.Storyboard.InstantiateViewController("ValidityViewController") as ValidityViewController;
            Redirect.Title = "Geldigheid";
            NavCtrl.PushViewController(Redirect, true);

            TabBar.SelectedIndex = 2;
        }

        void LinktoProfile()
        {
            UINavigationController NavCtrl = TabBar.ViewControllers[2] as UINavigationController;

            ProfileViewController Redirect = App.Storyboard.InstantiateViewController("ProfileViewController") as ProfileViewController;
            Redirect.Title = "Gegevens";
            NavCtrl.PushViewController(Redirect, true);

            TabBar.SelectedIndex = 2;
        }

        void LinktoMastercard()
        {
            UINavigationController NavCtrl = TabBar.ViewControllers[2] as UINavigationController;

            MastercardViewController Redirect = App.Storyboard.InstantiateViewController("MastercardViewController") as MastercardViewController;
            Redirect.Title = "Mastercard";
            NavCtrl.PushViewController(Redirect, true);

            TabBar.SelectedIndex = 2;
        }

    }
}


