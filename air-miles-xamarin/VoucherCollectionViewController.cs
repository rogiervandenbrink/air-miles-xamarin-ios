﻿using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;

namespace airmilesxamarin
{
    
    public partial class VoucherCollectionViewController : UICollectionViewController
    {

		Aankoop CurrentVoucher; 		Aankoop[] firstAankopenBatch; 		LoadingOverlay LoadingView; 		MaintainanceOverlay MaintainanceView;
		LineLayout LineLayout;

        public VoucherCollectionViewController (UICollectionViewFlowLayout layout) : base (layout)
        {
        }

		public override void ViewDidLoad () 		{ 			base.ViewDidLoad ();
 			if (CurrentVoucher != null) 			{ 				Title = CurrentVoucher.Title + " voucher"; 			}  			RetrieveAankopen();

			VoucherCollectionView.DecelerationRate = UIScrollView.DecelerationRateFast;
 		}  		public void SetCurrentVoucher (Aankoop item) { 			CurrentVoucher = item; 		}
 		public class VoucherFlowDelegate : UICollectionViewDelegateFlowLayout
		{
			UIViewController Controller;
			VoucherTableRow[] Rows;


			public VoucherFlowDelegate(UIViewController parent, VoucherTableRow[] items) : base()
			{
				Controller = parent;
				Rows = items;
			}

			public override CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
			{
				nfloat ScreenWidth = UIScreen.MainScreen.Bounds.Width;
				nfloat ScreenHeight = UIScreen.MainScreen.Bounds.Height;
				return new CGSize(ScreenWidth, ScreenHeight);
			}

			public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.FromRGB(217, 236, 246);
			}

			public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
			{
				var Cell = collectionView.CellForItem(indexPath);
				Cell.ContentView.BackgroundColor = UIColor.White;
			}

			public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
			{
				//Controller.PerformSegue(Rows[indexPath.Row].Segue, this);
			}
		}
 		private VoucherTableRow[] GetCollectionViewItems(Aankoop[] aankopen) 		{  			// Define lists for the Row 			List<VoucherTableRow> Rows = new List<VoucherTableRow>();  			if (aankopen != null) 			{
				Console.WriteLine("aantal aankopen" + aankopen.Length); 				for (int i = 0; i < aankopen.Length; i++) 				{ 					// if Rows don't exist, create them 					if (Rows == null) 					{ 						Rows = new List<VoucherTableRow>(); 					} 						// Add the Table Row 					Rows.Add(new VoucherTableRow() 						{ 							Description = aankopen[i].TotalString, 							Barcode = aankopen[i].VoucherNumber, 							BarcodeType = aankopen[i].BarcodeType, 							CellIdentifier = CellIdentifier.VoucherCollectionViewCell 							//Segue = "PromotieDetailSegue", 						} );  				} 				Console.WriteLine("Rows aankopen" + Rows.ToArray()); 				// Return the Rows as an arra 				return Rows.ToArray();
 			}  			return Rows.ToArray(); 		}    		public async void RetrieveAankopen() 		{ 			//InitLogin();  			SetLoader(AMStrings.LoadingAankopen);  			AankopenService AankopenService = new AankopenService();  			if (firstAankopenBatch != null) 			{                 AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();   				if (CurrentAankopen.Error != null) 				{ 					// if there is a response 					if (CurrentAankopen.Error[0].Code == "E00") 					{ 						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
						//VoucherCollectionView.Delegate = new VoucherFlowDelegate (this, GetCollectionViewItems(CurrentAankopen.Aankopen)); 						Console.WriteLine("current aankopen aantal 1: "+CurrentAankopen.Aankopen.Length);  						CollectionView.ReloadData();  					} 					else {  						if (CurrentAankopen.Error[0].Code == "E23") 						{ 							Console.WriteLine("E23 Sessie verlopen");  							// Logout                             LocalStorage.Logout(TabBarController);  							// Redirect to login page 							LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController; 							Redirect.Routing = "VoucherCollectionViewController"; 							NavigationController.ViewControllers = new[] { Redirect } ;  							AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, AMStrings.ErrorSessionExpiredMessage); 						} 					} 				} 				else { 					Console.WriteLine("error"); 					SetMaintanaceView(); 				}  				LoadingView.Hide(); 			}  			else { 				AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();   				if (CurrentAankopen.Error != null) 				{ 					// if there is a respons 					if (CurrentAankopen.Error[0].Code == "E00") 					{
						 						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen)); 						//VoucherCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
						Console.WriteLine("current aankopen aantal 2: " + CurrentAankopen.Aankopen.Length); 						VoucherCollectionView.ReloadData(); 					} 					else {  						if (CurrentAankopen.Error[0].Code == "E23") 						{ 							Console.WriteLine("E23 Sessie verlopen");  							// Logou                             LocalStorage.Logout(TabBarController);  							// Redirect to login pag 							LoginViewController Redirect = App.Storyboard.InstantiateViewController("LoginViewController") as LoginViewController; 							Redirect.Routing = "VoucherCollectionViewController"; 							NavigationController.ViewControllers = new[] { Redirect } ;  							AMAlerts.DefaultAlert(Redirect, AMStrings.ErrorSessionExpiredTitle, AMStrings.ErrorSessionExpiredMessage); 						} 					} 				} 				else { 					Console.WriteLine("error"); 					SetMaintanaceView(); 				}  				LoadingView.Hide();  			} 		}  		public async void UpdateAankopen(int LowId, int HighId) 		{ 			//InitLogin();  			AankopenService AankopenService = new AankopenService(); 			AankopenRoot CurrentAankopen = await AankopenService.GetAankopen();  			// if there is a respons 			if (CurrentAankopen.Error != null) 			{ 				if (CurrentAankopen.Error[0].Code == "E00") 				{ 					Console.WriteLine("updating transacties");  					if (firstAankopenBatch != null) 					{ 						Aankoop[] combined = new Aankoop[firstAankopenBatch.Length + CurrentAankopen.Aankopen.Length]; 						Array.Copy(firstAankopenBatch, combined, firstAankopenBatch.Length); 						Array.Copy(CurrentAankopen.Aankopen, 0, combined, firstAankopenBatch.Length, CurrentAankopen.Aankopen.Length); 						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(combined));
						//VoucherCollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(combined)); 						VoucherCollectionView.ReloadData(); 						firstAankopenBatch = combined;  					} 					else { 						VoucherCollectionView.Source = new VoucherCollectionViewSource(this, GetCollectionViewItems(CurrentAankopen.Aankopen));
						//CollectionView.Delegate = new VoucherFlowDelegate(this, GetCollectionViewItems(CurrentAankopen.Aankopen)); 						firstAankopenBatch = CurrentAankopen.Aankopen; 						VoucherCollectionView.ReloadData(); 					} 				} 			} 			else { 				SetMaintanaceView(); 			} 		}   		private void SetMaintanaceView() 		{ 			CGRect bounds = UIScreen.MainScreen.Bounds;  			MaintainanceView = new MaintainanceOverlay(bounds); 			CollectionView.ScrollEnabled = false; 			View.Add(MaintainanceView); 		}  		void SetLoader(string text) 		{ 			CGRect bounds = UIScreen.MainScreen.Bounds;  			//CGRect overlay = new CGRect(0, NavigationController.NavigationBar.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height + ButtonTabView.Bounds.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - (NavigationController.NavigationBar.Bounds.Height + ButtonTabView.Bounds.Height + UIApplication.SharedApplication.StatusBarFrame.Height)); 			LoadingView = new LoadingOverlay(bounds, text); 			View.Add(LoadingView); 		}  	} }  