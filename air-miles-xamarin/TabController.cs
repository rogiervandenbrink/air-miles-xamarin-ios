﻿using Foundation;
using System;
using UIKit;

namespace airmilesxamarin
{
    public partial class TabController : UITabBarController
    {
        public TabController (IntPtr handle) : base (handle)
		{
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			App.TabBar = this;
		}

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            this.TabBar.InvalidateIntrinsicContentSize();
        }
    }
}