﻿using System;
using Foundation;

namespace Air_Miles_Xamarin
{
	public class SharedDatetime
	{
		public SharedDatetime()
		{
		}

		public static DateTime ConvertFromUnixTimestamp(long timestamp)
		{
			DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return epoch.AddMilliseconds(timestamp);
		}

		public static DateTime ConvertFromUnixTimestamp(double timestamp)
		{
			DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			return origin.AddSeconds(timestamp);
		}

		public static long ConvertToUnixTimestampLong(DateTime date)
		{
			return (long)ConvertToUnixTimestamp(date);
		}

		public static double ConvertToUnixTimestamp(DateTime date)
		{
			DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan diff = date.ToUniversalTime() - origin;
			return Math.Floor(diff.TotalMilliseconds);
		}


	}
}

