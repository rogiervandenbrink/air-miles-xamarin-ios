﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Air_Miles_Xamarin;
using Foundation;

namespace Air_Miles_Xamarin
{
	public static class SharedWebServices
	{
		// latest exception from webrequest
		static Exception _ErrorException;
		public static Exception ErrorException
		{
			get { return _ErrorException; }
			set { _ErrorException = value; }
		}
		static CancellationTokenSource _LastCancelToken;
		public static CancellationTokenSource LastCancelToken
		{
			get { return _LastCancelToken; }
			set { _LastCancelToken = value; }
		}

		public static async Task<string> GingerPanToken(string setuptoken, string pan, CancellationTokenSource token = null)
		{
			Console.WriteLine("GET GINGERPAYMENTS TOKEN");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("setuptoken", setuptoken);
			jc.AddString("pan", pan);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.GingerPan, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetLogin(string username, string password, string deviceId, string deviceType, string osType, string osVersion, string currentAppVersion, CancellationTokenSource token = null)
		{
			Console.WriteLine("LOGIN");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("userid", username);
			jc.AddString("password", password);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("deviceid", deviceId);
			jc.AddString("appversionsap", currentAppVersion);
			jc.AddString("devicetype", deviceType);
			jc.AddString("ostype", osType);
			jc.AddString("osversion", osVersion);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Login, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetLogin(string tokenApp, string deviceId, string ZMID2, CancellationTokenSource token = null)
		{
			Console.WriteLine("LOGIN");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("zmid2", ZMID2);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Login, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> CheckToken(string tokenApp, string deviceId, string ZMID2, CancellationTokenSource token = null)
		{
			Console.WriteLine("CHECK TOKEN");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("deviceid", deviceId);
			jc.AddString("zmid2", ZMID2);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.CheckToken, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetTransactions(string zmId2, string tokenApp, string deviceId, int lowId = -1, int highId = -1, CancellationTokenSource token = null)
		{
			Console.WriteLine("TRANSACTIONS");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("deviceid", deviceId);

			if (lowId >= 0)
				jc.AddInt("lowid", lowId);
			if (highId >= 0)
				jc.AddInt("highid", highId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Transactions, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetBalance(string cardNumber, string deviceId, bool DayRangeOn = false, CancellationTokenSource token = null)
		{
			Console.WriteLine("BALANCE");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("cardnumber", cardNumber);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("deviceid", deviceId);
            jc.AddBoolean("DayRangeOn", DayRangeOn);

			Console.WriteLine("post: " + jc.ToString());

			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Balance, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetAvatar(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine($"AVATAR - {zmId2}");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("deviceid", deviceId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.GetAvatar, jc.ToString(), token);
			return resp;
		}

		///
		/// FOR RELEASE 2.1
		/// 

		public static async Task<string> GetPurchases(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("PURCHASES");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.MyPurchases, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetPersonalOffers(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("PERSONAL OFFERS");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.PersonalOffers, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetPersonalPromotions(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("PERSONAL PROMOTIONS");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.PersonalPromotions, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetGiftsAndPromotions(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("GETBOTH");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.GiftsAndPromotions, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> ActivateGift(string zmId2, string tokenApp, string offerNr, string itemNr, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("ACTIVATE GIFT");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("offernr", offerNr);
			jc.AddString("itemnr", itemNr);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.ActivateGift, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> ActivateCampaign(string zmId2, string tokenApp, string campaignId, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("ACTIVATE CAMPAIGN");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("campaignid", campaignId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.ActivateCampaign, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> SaveAvatar(string zmId2, string base64, string name, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("SAVE AVATAR");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("filename", name);
			jc.AddString("avatar", base64);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.SaveAvatar, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> ChangeProfile(JsonCreator profileJson, string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			string resp = "";

			profileJson.AddString("mobilekey", SharedWebUrls.MobileKey);
			profileJson.AddString("zmid2", zmId2);
			profileJson.AddString("tokenapp", tokenApp);
			profileJson.AddString("deviceid", deviceId);

			Console.WriteLine("post: " + profileJson.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.ChangeProfile, profileJson.ToString(), token);
			return resp;
		}

		public static async Task<string> CreateLightAccount(string email, bool optin, string deviceId, string partner, CancellationTokenSource token = null)
		{
			Console.WriteLine($"APPLY FOR CARD");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("deviceid", deviceId);
			jc.AddString("email", email);
			jc.AddString("partner", partner);
			jc.AddBoolean("optin", optin);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.CreateAccount, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetAddress(string zmId2, string tokenApp, string zipCode, string houseNr, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine($"GET ADDRESS");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("postcode", zipCode);
			jc.AddString("housenumber", houseNr);
			jc.AddString("country", "NL");

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.ZipCode, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> SignUp(string deviceId, string userName, string password, string passwordConfirm, string cardNr, string email, string birthdate, CancellationTokenSource token = null)
		{
			Console.WriteLine($"SIGNUP");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("username", userName);
			jc.AddString("password", password);
			jc.AddString("passwordconfirm", passwordConfirm);
			jc.AddString("cardnumber", cardNr);
			jc.AddString("email", email);
			if (!string.IsNullOrEmpty(birthdate))
				jc.AddString("birthdate", birthdate);
			jc.AddString("deviceid", deviceId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.AccountSignup, jc.ToString(), token);
			return resp;
		}

		///
		/// FOR RELEASE 2.2
		/// 

		public static async Task<string> SignUp_CheckCardNumber(string deviceId, string cardNr, CancellationTokenSource token = null)
		{
			Console.WriteLine($"SIGNUP - CHECK FOR BIRTHDATE?");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("username", "");
			jc.AddString("password", "");
			jc.AddString("passwordconfirm", "");
			jc.AddString("cardnumber", cardNr);
			jc.AddString("email", "");
			jc.AddString("deviceid", deviceId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.AccountSignup, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> SignUp_CheckBirthdate(string deviceId, string cardNr, string birthdate, CancellationTokenSource token = null)
		{
			Console.WriteLine($"SIGNUP - POST BIRTHDATE");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("username", "");
			jc.AddString("password", "");
			jc.AddString("passwordconfirm", "");
			jc.AddString("cardnumber", cardNr);
			jc.AddString("email", "");
			jc.AddString("birthdate", birthdate);
			jc.AddString("deviceid", deviceId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.AccountSignup, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> SignUp_CheckEmail(string deviceId, string cardNr, string email, CancellationTokenSource token = null)
		{
			Console.WriteLine($"SIGNUP - POST EMAIL");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("username", "");
			jc.AddString("password", "");
			jc.AddString("passwordconfirm", "");
			jc.AddString("cardnumber", cardNr);
			jc.AddString("email", email);
			jc.AddString("deviceid", deviceId);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.AccountSignup, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> Transfer(string zmId2, string tokenApp, string deviceId, string receiverCardNr, string amount, CancellationTokenSource token = null)
		{
			Console.WriteLine("TRANSFER MILES");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("CardNumberToTransfer", receiverCardNr);
			jc.AddString("AmountAirmilesToTransfer", amount);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Transfer, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> Donate(string zmId2, string tokenApp, string deviceId, string charityName, string amount, CancellationTokenSource token = null)
		{
			Console.WriteLine("DONATE MILES");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("CharityName", charityName);
			jc.AddString("AmountAirmilesToDonate", amount);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Donate, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> BlockCard(string zmId2, string tokenApp, string deviceId, string fullName, string cardNumber, string zipCode, string houseNr, string houseNrSup, string city, string country, string street, CancellationTokenSource token = null)
		{
			Console.WriteLine("BLOCK CARD");
			string resp = "";

			int sendHousenr;
			bool intParsed = int.TryParse(houseNr, out sendHousenr);
			if (!intParsed)
				sendHousenr = 0;

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("FullName", fullName);
			jc.AddString("CardNumber", cardNumber);
			jc.AddString("PostalCode", zipCode);
			jc.AddInt("HouseNumber", sendHousenr);
			jc.AddString("HouseNumberSuppliment", houseNrSup);
			jc.AddString("City", city);
			jc.AddString("Country", country);
			jc.AddString("Street", street);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.BlockCard, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> ReplaceCard(string zmId2, string tokenApp, string deviceId, string fullName, string cardNumber, string zipCode, string houseNr, string houseNrSup, string city, string country, string street, CancellationTokenSource token = null)
		{
			Console.WriteLine("REPLACE CARD");
			string resp = "";

			int sendHousenr;
			bool intParsed = int.TryParse(houseNr, out sendHousenr);
			if (!intParsed)
				sendHousenr = 0;

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);
			jc.AddString("FullName", fullName);
			jc.AddString("CardNumber", cardNumber);
			jc.AddString("PostalCode", zipCode);
			jc.AddInt("HouseNumber", sendHousenr);
			jc.AddString("HouseNumberSuppliment", houseNrSup);
			jc.AddString("City", city);
			jc.AddString("Country", country);
			jc.AddString("Street", street);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.ReplaceCard, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> Unsubscribe(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("UNSUBSCRIBE");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Unsubscribe, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> GetCharityNames(CancellationTokenSource token = null)
		{
			Console.WriteLine("CHARITIES");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Charities, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> Expiration(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("EXPIRATION AIRMILES");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.Expiration, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> LinkCards(string zmId2, string tokenApp, string deviceId, string cardNr, bool unLinkCard, CancellationTokenSource token = null)
		{
			Console.WriteLine("LINKCARDS " + unLinkCard);
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("cardnumbertoinvite", cardNr);
			jc.AddBoolean("unlinkcard", unLinkCard);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.LinkCards, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> LinkCardsAction(string zmId2, string tokenApp, string deviceId, bool accept, CancellationTokenSource token = null)
		{
			Console.WriteLine("LINKCARDS ACTION");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddBoolean("accept", accept);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.LinkCardsAction, jc.ToString(), token);
			return resp;
		}

		public static async Task<string> LinkCardsGetInfo(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("LINKCARDS GET INFO");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.LinkCardsGetInfo, jc.ToString(), token);
			return resp;
		}

        public static async Task<Byte[]> addVoucherWallet(string zmId2, string tokenApp, string deviceId, string email, string fullname, int formatVersion, string passType, string passTypeIdentifier, string serialNumber, string teamIdentifier, string webServiceURL, string authenticationToken, string barcodeMessage, string barcodeFormat, string barcodeMessageEncoding, string latitude, string longitude, string altitude, string relevanttext, string organizationName, string description, string logoText, string foregroundColor, string backgroundColor, string primaryKey, string primaryLabel, string primaryValue, string primaryCurrencyCode, string auxiliaryKey, string auxiliaryLabel, string auxiliaryValue, bool isRelative, string dateStyle, int maxdistance, CancellationTokenSource token = null)
        {
            Console.WriteLine("ADD TO WALLET");
            //string resp = "";

            JsonCreator jc = new JsonCreator();
            jc.AddString("zmid2", zmId2);
            jc.AddString("tokenapp", tokenApp);
            jc.AddString("deviceid", deviceId);
            jc.AddString("mobilekey", SharedWebUrls.MobileKey);
            jc.AddString("email", email);
            jc.AddString("fullname", fullname);
            jc.AddInt("formatVersion", formatVersion);
            jc.AddString("passTypeIdentifier", passTypeIdentifier);
            jc.AddString("serialNumber", serialNumber);
            jc.AddString("teamIdentifier", teamIdentifier);
            jc.AddString("webServiceURL", webServiceURL);
            jc.AddString("authenticationToken", authenticationToken);
            //barcode
            JsonCreator jayc = new JsonCreator();
            jayc.AddString("message", barcodeMessage);
            jayc.AddString("format", barcodeFormat);
            jayc.AddString("messageEncoding", barcodeMessageEncoding);
            JsonCreator[] barcode = new JsonCreator[]{
                jayc
            };

            jc.AddJsonArray("barcode", barcode);
            //locations
            JsonCreator jayz = new JsonCreator();
            jayz.AddString("longitude", longitude);
            jayz.AddString("latitude", latitude);
            jayz.AddString("altitude", altitude);
            jayz.AddString("relevantText", relevanttext);
            jayz.AddInt("maxDistance", maxdistance);
            JsonCreator[] locations = new JsonCreator[] {
                jayz
            };

            jc.AddJsonArray("locations", locations);

            jc.AddString("organizationName", organizationName);
            jc.AddString("description", description);
            jc.AddString("logoText", logoText);
            jc.AddString("foregroundColor", foregroundColor);
            jc.AddString("backgroundColor", backgroundColor);
            // coupon / storeCard
            JsonCreator passtype = new JsonCreator();
            //primaryfields
            JsonCreator secondaryFields = new JsonCreator();
            secondaryFields.AddString("key", primaryKey);
            secondaryFields.AddString("label", primaryLabel);
            secondaryFields.AddString("value", primaryValue);
            secondaryFields.AddString("currencyCode", primaryCurrencyCode);
            JsonCreator[] secondaryArray = new JsonCreator[]{
                secondaryFields
            };
            passtype.AddJsonArray("secondaryFields", secondaryArray);
            //auxiliaryfields
            if (!string.IsNullOrEmpty(auxiliaryKey)) {
	            JsonCreator auxiliaryFields = new JsonCreator();
	            auxiliaryFields.AddString("key", auxiliaryKey);
	            auxiliaryFields.AddString("label", auxiliaryLabel);
	            auxiliaryFields.AddString("value", auxiliaryValue);
	            auxiliaryFields.AddBoolean("isRelative", isRelative);
	            auxiliaryFields.AddString("dateStyle", dateStyle);
	            JsonCreator[] auxiliaryArray = new JsonCreator[]{
	                auxiliaryFields
	            };
	            passtype.AddJsonArray("auxiliaryFields", auxiliaryArray);
            }
            jc.AddJsonValue(passType, passtype);

            Console.WriteLine("post wallet: " + jc.ToString());
            var resp = await SharedSecurity.GetInstance().WebRequestWallet(SharedWebUrls.AddWallet, jc.ToString(), token);
            return resp;
		}

		public static async Task<string> RetrieveKeyGP(string zmId2, string tokenApp, string deviceId, CancellationTokenSource token = null)
		{
			Console.WriteLine("RETRIEVE KEY GP");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.RetrieveKeyGP, jc.ToString(), token);

			Console.WriteLine("response retrievekeygp service " + resp);
			return resp;
		}

		public static async Task<string> StoreToken(string truncatedPan, string token, string zmId2, string tokenApp, string deviceId, CancellationTokenSource cancelToken = null)
		{
			Console.WriteLine("STORE TOKEN MASTERCARD");
			string resp = "";

			JsonCreator jc = new JsonCreator();
			jc.AddString("zmid2", zmId2);
			jc.AddString("tokenapp", tokenApp);
			jc.AddString("deviceid", deviceId);
			jc.AddString("Token", token);
			jc.AddString("Truncated_PAN", truncatedPan);
			jc.AddString("mobilekey", SharedWebUrls.MobileKey);

			Console.WriteLine("post: " + jc.ToString());
			resp = await SharedSecurity.GetInstance().SecureWebRequest(SharedWebUrls.StoreTokenMasterCard, jc.ToString(), cancelToken);

			Console.WriteLine("response storetoken mc service " + resp);
			return resp;
		}
	}
}