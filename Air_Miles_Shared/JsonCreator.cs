﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Net.Http;
using System.Text;

namespace Air_Miles_Xamarin {
	/// <summary>
	/// Creating Json Objects in an easier way.
	/// </summary>
	public class JsonCreator {
		List<object> jsoncontent = new List<object>();

		/// <summary>
		/// Create a JsonCreator object.
		/// </summary>
		public JsonCreator() {
		}

		/// <summary>
		/// Add String to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="content">Content.</param>
		public void AddString(string name, string content){
			jsoncontent.Add("\"" + name + "\": \"" + content + "\"");
		}

		/// <summary>
		/// Add int to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="content">Content.</param>
		public void AddInt(string name, int content){
			jsoncontent.Add("\"" + name + "\": " + content);
		}

		/// <summary>
		/// Add long to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="content">Content.</param>
		public void AddLong(string name, long content){
			jsoncontent.Add("\"" + name + "\": " + content);
		}

		/// <summary>
		/// Add Boolean to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="content">If set to <c>true</c> content.</param>
		public void AddBoolean(string name, bool content){
			jsoncontent.Add("\"" + name + "\": " + content.ToString().ToLower());
		}


		/// <summary>
		/// Add another JsonCreator to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="jc">Jc.</param>
		public void AddJsonValue(string name, JsonCreator jc){
			jsoncontent.Add("\"" + name + "\": " + jc.ToString());
		}

		/// <summary>
		/// Add an array to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="jc">Jc.</param>
		public void AddJsonArray(string name, JsonCreator[] jc){
			String content = "";
			for (int i = 0; i < jc.Length; i++) {
				content += jc [i].ToString () + ",";
			}
			content = content.Remove (content.Length - 1);

			jsoncontent.Add("\"" + name + "\": [" + content + "]");
		}

		/// <summary>
		/// Adds an array to JsonCreator.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="jc">Jc.</param>
		public void AddStringAsArray(string name, string array){
			/*String content = "";
			for (int i = 0; i < array.Length; i++) {
				content += array [i].ToString () + ",";
			}
			content = content.Remove (content.Length - 1);

			jsoncontent.Add("\"" + name + "\": [" + content + "]");*/
			jsoncontent.Add("\"" + name + "\": " + array);
		}

		/// <summary>
		/// Print JsonCreator to a String.
		/// </summary>
		/// <returns>To be added.</returns>
		/// <remarks>To be added.</remarks>
		public override string ToString(){
			string jsonString = "{";
			for(int i = 0; i < jsoncontent.Count; i++){
				jsonString += jsoncontent[i] + ", ";
			}
			jsonString = jsonString.Remove(jsonString.Length - 2, 2);
			jsonString += "}";

			return jsonString;
		}

		/// <summary>
		/// Print JsonCreator to a HttpContent.
		/// </summary>
		/// <returns>The http content.</returns>
		public HttpContent ToHttpContent(){
			return new StringContent(ToString(), Encoding.UTF8);
		}

		/// <summary>
		/// Print JsonCreator to a JsonValue.
		/// </summary>
		/// <returns>The json value.</returns>
		public JsonValue ToJsonValue(){
			return JsonValue.Parse(ToString());
		}

		/// <summary>
		/// Cast a jsonString to a HttpContent.
		/// </summary>
		/// <returns>The to http content.</returns>
		/// <param name="jsonString">Json string.</param>
		public HttpContent StringToHttpContent(string jsonString){
			return new StringContent(jsonString, Encoding.UTF8);
		}

		/// <summary>
		/// Add an extra String value to your jsonString.
		/// </summary>
		/// <returns>The string to json string.</returns>
		/// <param name="jsonString">Json string.</param>
		/// <param name="nameToAdd">Name to add.</param>
		/// <param name="valueToAdd">Value to add.</param>
		public String AddStringToJsonString(string jsonString, string nameToAdd, string valueToAdd){
			String returnString = jsonString.Trim();
			return (returnString.Remove(jsonString.Length - 1) + ", " + "\""+ nameToAdd +"\": \"" + valueToAdd + "\"}");
		}
	}
}