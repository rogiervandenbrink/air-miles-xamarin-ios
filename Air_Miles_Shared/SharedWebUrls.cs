
﻿namespace Air_Miles_Xamarin
{
	public static class SharedWebUrls
	{
        // IS ACCEPTANCE USED         public static readonly bool ISACCP = true;         public static readonly bool ISTEST = true;         // MENDIX URL         static readonly string Mendix_accp = "https://acceptatie.airmiles.nl/mendix/rest/";         static readonly string Mendix_test = "https://test.airmiles.nl/mendix/rest/";         //static readonly string Mendix_accp = "https://airmilesnl-accp.mendixcloud.com/rest/";         static readonly string Mendix_prod = "https://www.airmiles.nl/mendix/rest/";         public static string Mendix         {             get             {                 if (ISTEST) return Mendix_test;                 if (ISACCP) return Mendix_accp;                 return Mendix_prod;             }         }          //IOS WALLET         static readonly string Mendix_iOS_Wallet_accp = "https://airmilesapi-accp.mendixcloud.com/rest/ioswallet";         static readonly string Mendix_iOS_Wallet_prod = "https://airmilesapi.mendixcloud.com/rest/ioswallet";         public static string IosWallet      {           get             {               return ISACCP ? Mendix_iOS_Wallet_accp : Mendix_iOS_Wallet_prod;            }       }       // HIPPO URL        static readonly string Hippo_accp = "https://acceptatie.airmiles.nl/rest/v2_20/";       static readonly string Hippo_prod = "https://www.airmiles.nl/rest/v2_20/";          public static string Hippo      {           get             {               return ISACCP ? Hippo_accp : Hippo_prod;            }       }           // NODE URL         static readonly string Node_Mendix_accp = "https://appservices-accp.airmiles.nl/rest/";         static readonly string Node_Mendix_prod = "https://appservices.airmiles.nl/rest/";         public static string Node_Mendix         {             get             {                 if (ISTEST) return Mendix_test;                 if (ISACCP) return Node_Mendix_accp;                 return Node_Mendix_prod;             }         }
		// HIPPO KEY
		static readonly string HippoKey_accp = "VSlt3owsjydVtB6L0lw1AvfTnIsWaGwYy50JTkPEHa6Ps06EU231QKsHmaRM";
		static readonly string HippoKey_prod = "41yKJLk2CSQgrxIXoj3kGbqXp03189XU6B8aPzaatzfHXH3xuRTcdu1lkipc";

		public static string HippoKey
		{
			get
			{
				return ISACCP ? HippoKey_accp : HippoKey_prod;
			}
		}


		// MOBILE KEY
		static readonly string MobileKey_accp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		static readonly string MobileKey_prod = "PlDvVfbG332fK03k43Bb4eYKe1ztC3u2";

		public static string MobileKey
		{
			get
			{
				return ISACCP ? MobileKey_accp : MobileKey_prod;
			}
		}

        //BALANCECHECKERSECRET
		static readonly string BalanceCheckerSecret_accp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		static readonly string BalanceCheckerSecret_prod = "VP82n43n44V6AZppPgcLE6BiXA4GDp5f";

		public static string BalanceCheckerSecret
		{
			get
			{
				return ISACCP ? BalanceCheckerSecret_accp : BalanceCheckerSecret_prod;
			}
		}

		//AIR MILES WEBSITE
		static readonly string AirmilesSite_accp = "https://acceptatie.airmiles.nl/";
		static readonly string AirmilesSite_prod = "https://www.airmiles.nl/";

		public static string AirmilesSite
		{
			get
			{
				return ISACCP ? AirmilesSite_accp : AirmilesSite_prod;
			}
		}


		// MENDIX PUB KEY
		// (airmiles.nl key)
		//static readonly string Mendix_key_accp = "3082010A0282010100B339E9F7755200580EC1AD2B4A9C26EB734239F22C4D85B03BCFC630E00FCCE3C3E3E0884C5CB6AA2DF70F6ECFA57DBEFED08E7A728FA90F1733319463D96081434E02D96CEAFF328EDE87DF3CD6A31763A7092FF7C257DA96926FFCF69A4440AD56E9A2EFEB6C45D34187FA5FB190F41D3710BBC6F797D5275AD0B90AE3743A01918020711EB6168CEF27760EB44EA2EDB5972F81FB8696B1CE5C00C5020C82F09244AA391E3FD907E2FCC40BFF7F074E0F91BF2995C395117685F5A9A071F1190D9D1521665DA13305C05B64EABC1638663E25DDB7733DCC41909AC9B6380EE278FEAD85D0CE1382D8E6563BA3407088EF2910A45482BCEA63D2EC540CE90F02030101";
		// (mendixcloud.com key)
		static readonly string Mendix_key_accp = "3082020A0282020100C364243A3D3C5C37609E07C4A38222E0A88F2E7DD8C8F59FA216F4F0E20A1154DBEB6D5AACC7A93E8A6564152262ADFD7BB28A7220D9128D9979C97C51577374BC1EB7C447EDF07728B0B84EA384059C6EAF94CF8A6361665DDA52AEB655458C69DBBD14B127C0FC1F0A0FA3B713E19A6AA320BE4CBECA1B900DB24ECDA5E64D7FBA8DE298E09DE351410AA90EF057EF0501BC4C005DD5AEB801BEE800C648A581DFF61AB3AEF0754299EFC2E4609C1FFC278781D2428E128657258CF689977BBFD2E9106C6E8405184969DD7AC028A909BB08B40AFF058EE52C41D354C78C3903408EC21ABDA21484FDDCE5178A61D0D08AB8AAD606049ABF4E61EE7EE9C0E0197AE73B98C61E682E44C28340AB6F8BFF21AB33F9FAA7A5EF7C2A1AA28F497F5588FC7D35383F743B2898229C491E74F254A3E104087637D7FF21A5B2178D48F012165A9CB0C7B93905E1E4A6B47DB3D08B2B0FBD8011FA9E03539F5B922843CA75C0F553A667A83A28B685F4A9EA243D1BC02B7501170E19D088DF3157F5A3D71C93FF726BD37A38B1FFC4857293EBABB5C3A57FA90A5B3265E07242E54E96B0EE13DB4E6D1EFA44A1BD695DAD053A82B80BFD0CDBA3341E2BB9EE8A6EACCFA45066A943078B3C36E8926180E8760071C7DFB649264DEB6CAD90EC713405BD4925EC50B403769F90AEF67025CE1D04263883E5F30EC6641FD74CB0E8E06A830203010001";
		static readonly string Mendix_key_prod = "3082010A0282010100B9B96D42727C324707B9D6D331C1CE9791684C6C583C49F5D76F51425BC0EFE6FC9C6644E81A89A0C4D57DBA701C4F9F34288D1898B4AE180C9C338EE9C7E9E5D113AD3A0D8C31677462EEA2C3C8A9A0FB93009372C723A7F7EE9FF80B5DB2EA39BB861D03FCFCF52151115C359DE2DFE0FD5E2414681CB5592AA83D42D320E0CB2F9019AF4495F8F6EC9D6C353CF74AD0F14249524029A464CDACC2172E04F9AA5E2290CD0914F749AE8062A13EB058F81D49C9A8B50E1FFF7549EE6041DB3A49238C970CA2C524EA3C47E58E1F8498A5BF6FF605C0C20315653607BEEACF1F04D850D7A8136E3E7321720FC60C57AA0CC5C052B5D22CCA878E357AF43939130203010001";

		public static string PUB_KEY
		{
			get
			{
				return ISACCP ? Mendix_key_accp : Mendix_key_prod;
			}
		}


		// NODE PUB KEY
		static readonly string Node_key_accp = "3082010A0282010100A51EF64BA78D4519F4D57D21D1D3F2983A65D3ED520931A1EFC301EAE8EBDEDA5CB8837A18DFA5DA29E5E53B56717A2DCF25E51970EA0CF73E750E0366109E84DDA3CCD4808986C976B5D608AC92536F6367F595A93E38663A991A569D66FBD40A9C3922067A82F9CA45717142B5BAE9662CC7F0D928D44ED4DBB0B3E6E287F65681CD0764547B328E06DB84E113AC7EB7D070B95FE68347983B5C63FCAC11F608389ED8FD16689D5A8F31F10FE58C9D0BAA7B65FEF7E72D5EC43E368F76FFE326DE5B6CF66630F3C1D6A916DF6ACB99866638B07C597D7BE1C37AC851165F0CD024564DB237660BB1636392BF0D8F62940C37017DDA55B7A6A848456DCA89BD0203010001";
		static readonly string Node_key_prod = "3082010A0282010100DD7EFA15781A93856F3DB7414981E0313582774FB06753BE3E24FC87C8B326A740EB47BB65509F31B6FE14F9209B076D08E0741ECC147FB3BC3EE45FA9D101AC3181FA0D6D26359A92C21C298459EFB5E2B70693AD96C4D589D807C9397F6566558C9D452553EEED654779B40EA0CA1729AA0FA5EC5FCF02BD5991364C89FD0044C7B816796F1312B4305CD1E6597582109D68A4279F9234DB0A4339B0A478C0AD24F24A3ED51502919E092510E93B31F71DFFB26AB4AA4BBACBFA5EB025BFB2283C414AB73BE33A6E0A5D82F403546FD807EE116E408009FDB5805C7492DBB5D6B40CFCEEEDB8E8433E7A0052A83D6505E25DCC5C6FA11483D20A9D10FAAE7F0203010001";

		public static string PUB_KEY_NODE
		{
			get
			{
				return ISACCP ? Node_key_accp : Node_key_prod;
			}
		}

		// GINGERPAYMENTS
		public static readonly string GINGER_PUB_KEY = "3082010A0282010100BBF07C588B1CA80861AD5930A9FEE14E3787F4F69A9F08A310451D94BFF308FFB3F13CCA5CC8F68B23C6DE66A0DF70922AB91168074D90E63FD821534C285026906FFA1C39803D1AC860293B88AE078FE81BD8E45DA0FC1A2F26564B49EBDF0D46650CA1AE15AFEC278C0F531A06585A83822BAF3DBA6C36AEB29A5374C2E95FED0484C84BC9760FC023522CA44E32E839CD3064AF6509045F77E3163690681B56A9496868B3C65988390116F1E09BC7DD1A3EF5A96143D980E928218976785A1B9CC0E5B4A1EC6049467AF4492C5C70B62E57FAA8B87E5506C4A1A074A172DA9F23E9A49CAE52E0319F87773AD8643E5945E0C9B40039B74223A18AB0D06C110203010001";
		public static readonly string GingerPan = "https://tokens.gingerpayments.com/?json=1";

        //PUSH SDK KEYS
        public static readonly string MAPP_KEY_ANDROID = "59c0cdd744dbe4.19640751";
        public static readonly string MAPP_KEY_IOS = "59c0ca41d857b8.26112853";

		// APP URLS
		public static readonly string Balance = Node_Mendix + "balancecheckerv2";
		public static readonly string Login = Mendix + "loginservice";
		public static readonly string Transactions = Mendix + "transactionservice";
		public static readonly string GetAvatar = Mendix + "getavatar";
		public static readonly string SaveAvatar = Mendix + "saveavatar";
		public static readonly string PersonalOffers = Mendix + "readoffergifts";
		public static readonly string ActivateGift = Mendix + "activateoffergift";
		public static readonly string ChangeProfile = Mendix + "changeprofile";
		public static readonly string MyPurchases = Mendix + "mypurchases";
		public static readonly string PersonalPromotions = Mendix + "personalizedpromotions";
		public static readonly string GiftsAndPromotions = Mendix + "giftsandpromotions";
		public static readonly string CreateAccount = Mendix + "createlightaccount";
		public static readonly string ActivateCampaign = Mendix + "activatepersonalizedpromotions";
		public static readonly string ZipCode = Mendix + "postcodechecker";
		public static readonly string AccountSignup = Node_Mendix + "accountsignup";
		public static readonly string CheckToken = Mendix + "checktoken";
		public static readonly string Transfer = Mendix + "transferairmiles";
		public static readonly string Donate = Mendix + "donateairmiles";
		public static readonly string BlockCard = Mendix + "blockcard";
		public static readonly string ReplaceCard = Mendix + "replacecard";
		public static readonly string Unsubscribe = Mendix + "unsubscribe";
		public static readonly string Charities = Mendix + "getcharitynames";
		public static readonly string Expiration = Mendix + "expirationairmiles";
		public static readonly string LinkCards = Mendix + "linkcards";
		public static readonly string LinkCardsAction = Mendix + "linkcardsaction";
		public static readonly string LinkCardsGetInfo = Mendix + "linkcardsgetinfo";
		public static readonly string RetrieveKeyGP = Mendix + "retrievekeygp";
		public static readonly string StoreTokenMasterCard = Mendix + "storetoken";
        public static readonly string AddWallet = IosWallet;

		// SITE URLS
		public static readonly string URLAlgemeneVoorwaarden = AirmilesSite + "algemene-voorwaarden";
		public static readonly string URLPrivacyCookieBeleid = AirmilesSite + "privacy-en-cookiebeleid";
		public static readonly string URLAHLinkCard = "https://www.ah.nl/airmiles";
		public static readonly string URLEtosLinkCard = "http://www.etos.nl/over-etos/ditismijnetos";
		public static readonly string URLPraxisLinkCard = "https://www.praxis.nl/service/mijnpraxis";
		public static readonly string URLloginReset = AirmilesSite + "login-reset";
		public static readonly string URLloginNewAccount = AirmilesSite + "online-account-aanmaken";
		public static readonly string URLContactForm = AirmilesSite + "klantenservice/contact";
		public static readonly string URLVeelgesteldeVragen = AirmilesSite + "klantenservice";
		public static readonly string URLChatForm = AirmilesSite + "chat";
		public static readonly string URLMijnprofiel = AirmilesSite + "mijn/profiel";

		public static readonly string URLSamenSparen = AirmilesSite + "mijn/kaart-koppelen/met-andere-spaarders";
		public static readonly string URLKaartBlokkeren = AirmilesSite + "mijn/kaart-blokkeren";
        public static readonly string URLReplaceCard = AirmilesSite + "mijn/kaart-vervangen";
	}
}