﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Air_Miles_Xamarin
{
	public class SharedMain
	{
		public SharedMain()
		{
		}

		/// <summary>
		/// Generates a ean from a cardnumber
		/// </summary>
		/// <returns>The ean.</returns>
		/// <param name="cardNumber">Card number.</param>
		public static string GenerateEan(string cardNumber)
		{
			int even = 0;
			int oneven = 0;

			string result = "260" + cardNumber;

			for (int i = 0; i < result.Length; i++)
			{
				if (i % 2 == 0)
				{
					oneven += Int32.Parse(result[i].ToString());
				}
				else {
					even += Int32.Parse(result[i].ToString());
				}
			}
			int controleGetal;
			int total = ((even * 3) + oneven) % 10;

			if (total != 0)
			{
				controleGetal = (10 - total);
			}
			else {
				controleGetal = 0;
			}

			return result + controleGetal;
		}


		/// <summary>
		/// Sets the balance format.
		/// </summary>
		/// <returns>The balance format with a dot after every 3 characters (e.g. 1.000).</returns>
		/// <param name="balance">Balance.</param>
		public static string SetBalanceFormat(string balance)
		{
			string minus = "";

			// remove minus 
			if(balance.Substring(0, 1) == "-")
				minus = "-";
			
			balance = balance.Replace("-", "");

			if (balance != null)
			{
				if (balance.Length > 3)
				{
					int balanceLength = balance.Length;
					int numberOfDots = Convert.ToInt16(Math.Floor((balanceLength - 1) / 3.0));
					if (numberOfDots == 0)
					{
						return balance;
					}

					string outputString = "";
					int startPos = 0;

					for (int i = 0; i < numberOfDots + 1; i++)
					{
						int length = (i == 0 ? balanceLength % 3 : 3);
						if (length == 0)
						{
							length = 3;
						}
						outputString += balance.Substring(startPos, length) + ".";
						startPos = startPos + length;
					}
					outputString = outputString.Remove(outputString.Length - 1);

					return minus + outputString;
				}
				return minus + balance;
			}

			return "0";
		}

		public static string FixPartnerLogoUrl(string url)
		{
			if (!string.IsNullOrEmpty(url))
			{
				if (!url.Contains("partnerlogo"))
				{
					List<string> splitUrl = url.Split('/').ToList();
					string name = splitUrl.ElementAt(splitUrl.Count - 1);
					url = url + "/" + name + "/airmiles:partnerlogo";
				}
			}
			return url;
		}

		public static string GetTimeUntilString(DateTime endDate)
		{
			string between = "Geactiveerd: Nog ";

			int daysBetween = (int)Math.Ceiling((endDate - DateTime.Now).TotalDays);
			if (daysBetween == 0)
			{
				int hoursBetween = (int)Math.Ceiling((endDate - DateTime.Now).TotalHours);

				if (hoursBetween == 0)
				{
					int minutesBetween = (int)Math.Ceiling((endDate - DateTime.Now).TotalMinutes);

					if (minutesBetween == 0)
					{
						int secondsBetween = (int)Math.Ceiling((endDate - DateTime.Now).TotalSeconds);
						between += (secondsBetween + " seconden");
					}
					else if (minutesBetween == 1)
					{
						between += (minutesBetween + " minuut");
					}
					else
					{
						between += (minutesBetween + " minuten");
					}
				}
				else
				{
					between += (hoursBetween + " uur");
				}
			}
			else if (daysBetween == 1)
			{
				between += (daysBetween + " dag");
			}
			else
			{
				between += (daysBetween + " dagen");
			}

			between += " geldig";

			return between;
		}

		public static string DecodeFromBase64(string EncodedString)
		{
			byte[] data = Convert.FromBase64String(EncodedString);
			string decodedString = Encoding.UTF8.GetString(data);

			return decodedString;
		}

        public static bool PersonalMastercard(string Mastercardnumber)
        {
            string FirstCharacters = Mastercardnumber.Substring(0, 6);
            List<string> PersonalNumbers = new List<string>();
            PersonalNumbers.Add("524886");
            PersonalNumbers.Add("516097");
            PersonalNumbers.Add("542114");
            PersonalNumbers.Add("535611");
            PersonalNumbers.Add("510029");
            PersonalNumbers.Add("528700");
            PersonalNumbers.Add("534126");
            PersonalNumbers.Add("540876");
            PersonalNumbers.Add("548133");
            PersonalNumbers.Add("510008");
            PersonalNumbers.Add("512494");
            PersonalNumbers.Add("512668");
            PersonalNumbers.Add("516091");
            PersonalNumbers.Add("522078");
            PersonalNumbers.Add("535321");
            PersonalNumbers.Add("542223");
            PersonalNumbers.Add("520953");
            PersonalNumbers.Add("557949");
            PersonalNumbers.Add("530401");
            PersonalNumbers.Add("545179");
            PersonalNumbers.Add("537465");
            PersonalNumbers.Add("548398");
            PersonalNumbers.Add("535612");
            PersonalNumbers.Add("510801");
            PersonalNumbers.Add("535120");
            PersonalNumbers.Add("514472");

            bool IsPersonal = PersonalNumbers.Contains(FirstCharacters);

            return IsPersonal;
        }
	}
}

