using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Air_Miles_Xamarin
{
	public class SharedSecurity
	{
		static SharedSecurity instance;
		TimeSpan timeout;
		HttpClient httpClient;
        static readonly string Mendix_key_accp = "3082020A0282020100C364243A3D3C5C37609E07C4A38222E0A88F2E7DD8C8F59FA216F4F0E20A1154DBEB6D5AACC7A93E8A6564152262ADFD7BB28A7220D9128D9979C97C51577374BC1EB7C447EDF07728B0B84EA384059C6EAF94CF8A6361665DDA52AEB655458C69DBBD14B127C0FC1F0A0FA3B713E19A6AA320BE4CBECA1B900DB24ECDA5E64D7FBA8DE298E09DE351410AA90EF057EF0501BC4C005DD5AEB801BEE800C648A581DFF61AB3AEF0754299EFC2E4609C1FFC278781D2428E128657258CF689977BBFD2E9106C6E8405184969DD7AC028A909BB08B40AFF058EE52C41D354C78C3903408EC21ABDA21484FDDCE5178A61D0D08AB8AAD606049ABF4E61EE7EE9C0E0197AE73B98C61E682E44C28340AB6F8BFF21AB33F9FAA7A5EF7C2A1AA28F497F5588FC7D35383F743B2898229C491E74F254A3E104087637D7FF21A5B2178D48F012165A9CB0C7B93905E1E4A6B47DB3D08B2B0FBD8011FA9E03539F5B922843CA75C0F553A667A83A28B685F4A9EA243D1BC02B7501170E19D088DF3157F5A3D71C93FF726BD37A38B1FFC4857293EBABB5C3A57FA90A5B3265E07242E54E96B0EE13DB4E6D1EFA44A1BD695DAD053A82B80BFD0CDBA3341E2BB9EE8A6EACCFA45066A943078B3C36E8926180E8760071C7DFB649264DEB6CAD90EC713405BD4925EC50B403769F90AEF67025CE1D04263883E5F30EC6641FD74CB0E8E06A830203010001";

		public static SharedSecurity GetInstance()
		{
			if (instance == null)
			{
				instance = new SharedSecurity();
				instance.Initialize();
			}
			return instance;
		}

		void Initialize()
		{
			ServicePointManager.ServerCertificateValidationCallback += ServicePointManager_ServerCertificateValidationCallback;
			ServicePointManager.DefaultConnectionLimit = 10;

			timeout = new TimeSpan(0, 0, 30);
		}

		bool ServicePointManager_ServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
		{
			HttpWebRequest url = (HttpWebRequest)sender;

            if (url.RequestUri.ToString().Contains("xaapi.xamarin.com") || url.RequestUri.ToString().Contains("acceptatie.airmiles.nl") /*|| url.RequestUri.ToString().Contains("airmilesnl-accp.mendixcloud.com") || url.RequestUri.ToString().Contains("https://airmilesapi.mendixcloud.com/rest/ioswallet") */)
			{
				return true;
			}
            if (url.RequestUri.ToString().Contains("https://airmilesapi.mendixcloud.com/rest/ioswallet"))
            {
                return TestCertificateWallet(certificate);   
            }

			return TestCertificate(certificate);
		}

		bool TestCertificate(System.Security.Cryptography.X509Certificates.X509Certificate certificate)
		{
			Console.WriteLine("test connection");
			if (certificate == null)
				return false;

			try
			{
				string pk = certificate.GetPublicKeyString();
				if (pk.Equals(SharedWebUrls.PUB_KEY) || pk.Equals(SharedWebUrls.PUB_KEY_NODE) || pk.Equals(SharedWebUrls.GINGER_PUB_KEY))
				{
					Console.WriteLine("secure connection");
					return true;
				}
                return true;
				Console.WriteLine("key");
				Console.WriteLine(pk);
			}
			catch (Exception e)
			{
				Console.WriteLine("bad connection");
				Console.WriteLine(e);
			}
			return false;
		}

        bool TestCertificateWallet(System.Security.Cryptography.X509Certificates.X509Certificate certificate)
        {
            Console.WriteLine("test connection");
            if (certificate == null)
                return false;

            try
            {
                string pk = certificate.GetPublicKeyString();
                if (pk.Equals(SharedWebUrls.PUB_KEY) || pk.Equals(Mendix_key_accp) || pk.Equals(SharedWebUrls.PUB_KEY_NODE) || pk.Equals(SharedWebUrls.GINGER_PUB_KEY))
                {
                    Console.WriteLine("secure connection");
                    return true;
                }

                Console.WriteLine("key");
                Console.WriteLine(pk);
            }
            catch (Exception e)
            {
                Console.WriteLine("bad connection");
                Console.WriteLine(e);
            }
            return false;
        }
		async Task<string> _SecureWebRequest(string url, string jsonInput, string authToken, string wallet, CancellationTokenSource token)
		{
			Console.WriteLine("Url: " + url);
			Uri uri = new Uri(url);
			ServicePoint am = ServicePointManager.FindServicePoint(uri);

			try
			{
				using (httpClient = new HttpClient())
				{
					httpClient.Timeout = timeout;
					HttpResponseMessage response;

					if (!string.IsNullOrEmpty(authToken))
					{
						Console.WriteLine("add header: " + authToken);
						httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authToken);
					}

					Console.WriteLine("headers: " + httpClient.DefaultRequestHeaders.ToString());

					if (string.IsNullOrEmpty(jsonInput))
					{
						if (token != null)
							response = await httpClient.GetAsync(uri, token.Token);
						else
							response = await httpClient.GetAsync(uri);
					}
					else
					{
						httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

						if (token != null)
							response = await httpClient.PostAsync(uri, new StringContent(jsonInput), token.Token);
						else
							response = await httpClient.PostAsync(uri, new StringContent(jsonInput));

					}

					response.EnsureSuccessStatusCode();

					string content = await response.Content.ReadAsStringAsync();
					return content;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("webrequest exception: " + e);
				SharedWebServices.LastCancelToken = token;
				SharedWebServices.ErrorException = e;
				return null;
			}
		}

		public async Task<string> SecureWebRequest(string url, string jsonInput, CancellationTokenSource token = null)
		{
			return await _SecureWebRequest(url, jsonInput, null, null, token);
		}

		public async Task<string> SecureWebRequest(string url, CancellationTokenSource token = null)
		{
			return await _SecureWebRequest(url, null, null, null, token);
		}

		public async Task<string> SecureWebRequestAuth(string url, string authToken, CancellationTokenSource token = null)
		{
			return await _SecureWebRequest(url, null, authToken, null, token);
		}

		public async Task<string> SecureWebRequestWallet(string url, string wallet, CancellationTokenSource token = null)
		{
			return await _SecureWebRequest(url, null, null, wallet, token);
		}

		public async Task<Byte[]> WebRequestWallet(string url, string jsonInput, CancellationTokenSource token)
		{
			Console.WriteLine("Url: " + url);
			Uri uri = new Uri(url);
			HttpClient httpclient;
			TimeSpan timeOut = new TimeSpan(0, 0, 20);
			ServicePoint am = ServicePointManager.FindServicePoint(uri);

			try
			{
				using (httpclient = new HttpClient())
				{

					httpclient.Timeout = timeOut;
					HttpResponseMessage response;

					Console.WriteLine("jsoninput headerwallet : " + jsonInput);
					httpclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "aW9zd2FsbGV0OldlbGtvbTAxIQ==");
					httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
					httpclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
					if (token != null)
						response = await httpclient.PostAsync(uri, new StringContent(jsonInput), token.Token);
					else
						response = await httpclient.PostAsync(uri, new StringContent(jsonInput));
					var stream = await response.Content.ReadAsByteArrayAsync();
					response.EnsureSuccessStatusCode();
                    return stream;

				}
			}
			catch (Exception e)
			{
				Console.WriteLine("webrequest exception: " + e);
				SharedWebServices.LastCancelToken = token;
				SharedWebServices.ErrorException = e;
				return null;
			}
		}
	}


	public static class SharedSha
	{
		/// <summary>
		/// Shas the hash.
		/// </summary>
		/// <returns>encrypted string with sha.</returns>
		/// <param name="hash">Hashstring.</param>
		public static string ShaEncryption(string hash)
		{
			SHA1 sh1 = SHA1.Create();
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] bytes = Encoding.UTF8.GetBytes(hash);
			byte[] result = sh1.ComputeHash(bytes);
			return GetHashedString(result);
		}

		/// <summary>
		/// Returns a string from bytes array
		/// </summary>
		/// <returns>string.</returns>
		/// <param name="result">hash.</param>
		public static string GetHashedString(byte[] result)
		{
			var sb = new StringBuilder();
			foreach (byte b in result)
			{
				var hex = b.ToString("x2");
				sb.Append(hex);
			}
			return sb.ToString();
		}
	}
}