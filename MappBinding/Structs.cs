﻿using System;
using ObjCRuntime;

namespace MappBinding
{
    [Native]
public enum APXPushNotificationActionButtonTodo : long
{
    Set = 1,
    Remove,
    Increment,
    OpenURLScheme,
    OpenWebSite,
    OpenAppStore,
    OpenViewController
}

[Native]
public enum APXPushNotificationActionButtonType : long
{
    Custom = 1,
    Tag
}

[Native]
public enum APXInterfaceServiceOperation : long
{
    One = 1,
    Two,
    Three
}
}
