using System;
using ObjCRuntime;

[Native]
public enum APXPushNotificationActionButtonTodo : nint
{
	Set = 1,
	Remove,
	Increment,
	OpenURLScheme,
	OpenWebSite,
	OpenAppStore,
	OpenViewController
}

[Native]
public enum APXPushNotificationActionButtonType : nint
{
	Custom = 1,
	Tag
}

[Native]
public enum APXInterfaceServiceOperation : nint
{
	One = 1,
	Two,
	Three
}
