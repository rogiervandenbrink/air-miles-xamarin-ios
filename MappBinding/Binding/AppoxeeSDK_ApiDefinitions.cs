using System;
using AppoxeeSDK;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;
using UserNotifications;

// @interface APXPushNotificationActionButtonAction : NSObject
[BaseType (typeof(NSObject))]
interface APXPushNotificationActionButtonAction
{
	// @property (nonatomic, strong) NSString * name;
	[Export ("name", ArgumentSemantic.Strong)]
	string Name { get; set; }

	// @property (nonatomic) APXPushNotificationActionButtonTodo todo;
	[Export ("todo", ArgumentSemantic.Assign)]
	APXPushNotificationActionButtonTodo Todo { get; set; }

	// @property (nonatomic) APXPushNotificationActionButtonType type;
	[Export ("type", ArgumentSemantic.Assign)]
	APXPushNotificationActionButtonType Type { get; set; }

	// @property (nonatomic, strong) NSString * value;
	[Export ("value", ArgumentSemantic.Strong)]
	string Value { get; set; }

	// -(id)initWithKeyedValues:(NSDictionary *)keyedValues;
	[Export ("initWithKeyedValues:")]
	IntPtr Constructor (NSDictionary keyedValues);
}

// @interface APXPushNotificationActionButton : NSObject
[BaseType (typeof(NSObject))]
interface APXPushNotificationActionButton
{
	// @property (nonatomic, strong) APXPushNotificationActionButtonAction * foregroundActionButtonAction;
	[Export ("foregroundActionButtonAction", ArgumentSemantic.Strong)]
	APXPushNotificationActionButtonAction ForegroundActionButtonAction { get; set; }

	// @property (nonatomic, strong) NSArray * backgroundActions;
	[Export ("backgroundActions", ArgumentSemantic.Strong)]
	[Verify (StronglyTypedNSArray)]
	NSObject[] BackgroundActions { get; set; }

	// -(id)initWithKeyedValues:(NSDictionary *)keyedValues;
	[Export ("initWithKeyedValues:")]
	IntPtr Constructor (NSDictionary keyedValues);
}

// @interface APXPushNotificationAction : NSObject
[BaseType (typeof(NSObject))]
interface APXPushNotificationAction
{
	// @property (readonly, getter = isAppoxeeCategory, nonatomic) BOOL appoxeeCategory;
	[Export ("appoxeeCategory")]
	bool AppoxeeCategory { [Bind ("isAppoxeeCategory")] get; }

	// @property (readonly, nonatomic, strong) NSString * categoryName;
	[Export ("categoryName", ArgumentSemantic.Strong)]
	string CategoryName { get; }

	// @property (readonly, nonatomic, strong) NSArray<APXPushNotificationActionButton *> * actionButtons;
	[Export ("actionButtons", ArgumentSemantic.Strong)]
	APXPushNotificationActionButton[] ActionButtons { get; }

	// -(id)initWithKeyedValues:(NSDictionary *)keyedValues;
	[Export ("initWithKeyedValues:")]
	IntPtr Constructor (NSDictionary keyedValues);
}

// @interface APXPushNotification : NSObject
[BaseType (typeof(NSObject))]
interface APXPushNotification
{
	// @property (readonly, nonatomic, strong) NSString * _Nullable alert;
	[NullAllowed, Export ("alert", ArgumentSemantic.Strong)]
	string Alert { get; }

	// @property (readonly, nonatomic, strong) NSString * _Nullable title;
	[NullAllowed, Export ("title", ArgumentSemantic.Strong)]
	string Title { get; }

	// @property (readonly, nonatomic, strong) NSString * _Nullable subtitle;
	[NullAllowed, Export ("subtitle", ArgumentSemantic.Strong)]
	string Subtitle { get; }

	// @property (readonly, nonatomic, strong) NSString * _Nullable body;
	[NullAllowed, Export ("body", ArgumentSemantic.Strong)]
	string Body { get; }

	// @property (readonly, nonatomic) NSInteger badge;
	[Export ("badge")]
	nint Badge { get; }

	// @property (readonly, nonatomic) NSInteger uniqueID;
	[Export ("uniqueID")]
	nint UniqueID { get; }

	// @property (readonly, nonatomic, strong) NSDictionary * _Nullable extraFields;
	[NullAllowed, Export ("extraFields", ArgumentSemantic.Strong)]
	NSDictionary ExtraFields { get; }

	// @property (readonly, nonatomic) BOOL didLaunchApp;
	[Export ("didLaunchApp")]
	bool DidLaunchApp { get; }

	// @property (readonly, nonatomic) BOOL isRich;
	[Export ("isRich")]
	bool IsRich { get; }

	// @property (readonly, nonatomic) BOOL isSilent;
	[Export ("isSilent")]
	bool IsSilent { get; }

	// @property (readonly, nonatomic) BOOL isTriggerUpdate;
	[Export ("isTriggerUpdate")]
	bool IsTriggerUpdate { get; }

	// @property (readonly, nonatomic, strong) APXPushNotificationAction * _Nullable pushAction;
	[NullAllowed, Export ("pushAction", ArgumentSemantic.Strong)]
	APXPushNotificationAction PushAction { get; }

	// +(instancetype _Nullable)notificationWithKeyedValues:(NSDictionary * _Nullable)keyedValues;
	[Static]
	[Export ("notificationWithKeyedValues:")]
	[return: NullAllowed]
	APXPushNotification NotificationWithKeyedValues ([NullAllowed] NSDictionary keyedValues);

	// +(instancetype _Nullable)notificationWithNotification:(UNNotification * _Nullable)notification;
	[Static]
	[Export ("notificationWithNotification:")]
	[return: NullAllowed]
	APXPushNotification NotificationWithNotification ([NullAllowed] UNNotification notification);

	// +(instancetype _Nullable)notificationWithNotificationResponse:(UNNotificationResponse * _Nullable)notificationResponse;
	[Static]
	[Export ("notificationWithNotificationResponse:")]
	[return: NullAllowed]
	APXPushNotification NotificationWithNotificationResponse ([NullAllowed] UNNotificationResponse notificationResponse);
}

// @interface APXRichMessage : NSObject <NSCoding>
[BaseType (typeof(NSObject))]
interface APXRichMessage : INSCoding
{
	// @property (readonly, nonatomic) NSInteger uniqueID;
	[Export ("uniqueID")]
	nint UniqueID { get; }

	// @property (readonly, nonatomic) NSDate * postDateUTC;
	[Export ("postDateUTC")]
	NSDate PostDateUTC { get; }

	// @property (readonly, nonatomic, strong) NSDate * postDate;
	[Export ("postDate", ArgumentSemantic.Strong)]
	NSDate PostDate { get; }

	// @property (readonly, nonatomic, strong) NSString * title;
	[Export ("title", ArgumentSemantic.Strong)]
	string Title { get; }

	// @property (readonly, nonatomic, strong) NSString * content;
	[Export ("content", ArgumentSemantic.Strong)]
	string Content { get; }

	// @property (readonly, nonatomic) BOOL isRead;
	[Export ("isRead")]
	bool IsRead { get; }

	// @property (readonly, nonatomic, strong) NSString * messageLink;
	[Export ("messageLink", ArgumentSemantic.Strong)]
	string MessageLink { get; }

	// -(id)initWithKeyedValues:(NSDictionary *)keyedValues;
	[Export ("initWithKeyedValues:")]
	IntPtr Constructor (NSDictionary keyedValues);

	// -(id)initWithKeyedValues:(NSDictionary *)keyedValues andMarkAsRead:(BOOL)isRead;
	[Export ("initWithKeyedValues:andMarkAsRead:")]
	IntPtr Constructor (NSDictionary keyedValues, bool isRead);
}

// @interface APXClientDevice : NSObject
[BaseType (typeof(NSObject))]
interface APXClientDevice
{
	// @property (nonatomic, strong) NSString * sdkVersion;
	[Export ("sdkVersion", ArgumentSemantic.Strong)]
	string SdkVersion { get; set; }

	// @property (nonatomic, strong) NSString * locale;
	[Export ("locale", ArgumentSemantic.Strong)]
	string Locale { get; set; }

	// @property (nonatomic, strong) NSString * timeZone;
	[Export ("timeZone", ArgumentSemantic.Strong)]
	string TimeZone { get; set; }

	// @property (nonatomic, strong) NSString * pushToken;
	[Export ("pushToken", ArgumentSemantic.Strong)]
	string PushToken { get; set; }

	// @property (nonatomic, strong) NSString * udid;
	[Export ("udid", ArgumentSemantic.Strong)]
	string Udid { get; set; }

	// @property (nonatomic, strong) NSString * udidHashed;
	[Export ("udidHashed", ArgumentSemantic.Strong)]
	string UdidHashed { get; set; }

	// @property (nonatomic, strong) NSString * osName;
	[Export ("osName", ArgumentSemantic.Strong)]
	string OsName { get; set; }

	// @property (nonatomic, strong) NSString * osVersion;
	[Export ("osVersion", ArgumentSemantic.Strong)]
	string OsVersion { get; set; }

	// @property (nonatomic, strong) NSString * hardwearType;
	[Export ("hardwearType", ArgumentSemantic.Strong)]
	string HardwearType { get; set; }

	// @property (nonatomic, strong) NSString * applicationID;
	[Export ("applicationID", ArgumentSemantic.Strong)]
	string ApplicationID { get; set; }

	// @property (getter = isInboxEnabled, nonatomic) BOOL inboxEnabled;
	[Export ("inboxEnabled")]
	bool InboxEnabled { [Bind ("isInboxEnabled")] get; set; }

	// @property (getter = isPushEnabled, nonatomic) BOOL pushEnabled;
	[Export ("pushEnabled")]
	bool PushEnabled { [Bind ("isPushEnabled")] get; set; }
}

// typedef void (^AppoxeeCompletionHandler)(NSError * _Nullable, id _Nullable);
delegate void AppoxeeCompletionHandler ([NullAllowed] NSError arg0, [NullAllowed] NSObject arg1);

// @protocol AppoxeeNotificationDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface AppoxeeNotificationDelegate
{
	// @optional -(void)appoxee:(Appoxee * _Nonnull)appoxee handledRemoteNotification:(APXPushNotification * _Nonnull)pushNotification andIdentifer:(NSString * _Nonnull)actionIdentifier;
	[Export ("appoxee:handledRemoteNotification:andIdentifer:")]
	void HandledRemoteNotification (Appoxee appoxee, APXPushNotification pushNotification, string actionIdentifier);

	// @optional -(void)appoxee:(Appoxee * _Nonnull)appoxee handledRichContent:(APXRichMessage * _Nonnull)richMessage didLaunchApp:(BOOL)didLaunch;
	[Export ("appoxee:handledRichContent:didLaunchApp:")]
	void HandledRichContent (Appoxee appoxee, APXRichMessage richMessage, bool didLaunch);
}

// @interface Appoxee : NSObject
[BaseType (typeof(NSObject))]
interface Appoxee
{
	// @property (nonatomic) NSInteger logLevel;
	[Export ("logLevel")]
	nint LogLevel { get; set; }

	// @property (nonatomic) BOOL postponeNotificationRequest;
	[Export ("postponeNotificationRequest")]
	bool PostponeNotificationRequest { get; set; }

	// @property (nonatomic) BOOL showNotificationsOnForeground;
	[Export ("showNotificationsOnForeground")]
	bool ShowNotificationsOnForeground { get; set; }

	// @property (readonly, nonatomic) BOOL isReady;
	[Export ("isReady")]
	bool IsReady { get; }

	[Wrap ("WeakDelegate")]
	[NullAllowed]
	AppoxeeNotificationDelegate Delegate { get; set; }

	// @property (nonatomic, weak) id<AppoxeeNotificationDelegate> _Nullable delegate;
	[NullAllowed, Export ("delegate", ArgumentSemantic.Weak)]
	NSObject WeakDelegate { get; set; }

	// @property (readonly, nonatomic, strong) NSString * _Nullable dmcUserID;
	[NullAllowed, Export ("dmcUserID", ArgumentSemantic.Strong)]
	string DmcUserID { get; }

	// @property (readonly, nonatomic, strong) NSString * _Nullable dmcAlias;
	[NullAllowed, Export ("dmcAlias", ArgumentSemantic.Strong)]
	string DmcAlias { get; }

	// +(instancetype _Nullable)shared;
	[Static]
	[Export ("shared")]
	[return: NullAllowed]
	Appoxee Shared ();

	// +(NSString * _Nonnull)sdkVersion;
	[Static]
	[Export ("sdkVersion")]
	[Verify (MethodToProperty)]
	string SdkVersion { get; }

	// -(void)engageAndAutoIntegrateWithLaunchOptions:(NSDictionary * _Nullable)launchOptions andDelegate:(id<AppoxeeNotificationDelegate> _Nullable)delegate;
	[Export ("engageAndAutoIntegrateWithLaunchOptions:andDelegate:")]
	void EngageAndAutoIntegrateWithLaunchOptions ([NullAllowed] NSDictionary launchOptions, [NullAllowed] AppoxeeNotificationDelegate @delegate);

	// -(void)engageWithLaunchOptions:(NSDictionary * _Nullable)launchOptions andDelegate:(id<AppoxeeNotificationDelegate> _Nullable)delegate andSDKID:(NSString * _Nonnull)sdkID;
	[Export ("engageWithLaunchOptions:andDelegate:andSDKID:")]
	void EngageWithLaunchOptions ([NullAllowed] NSDictionary launchOptions, [NullAllowed] AppoxeeNotificationDelegate @delegate, string sdkID);

	// -(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData * _Nullable)token;
	[Export ("didRegisterForRemoteNotificationsWithDeviceToken:")]
	void DidRegisterForRemoteNotificationsWithDeviceToken ([NullAllowed] NSData token);

	// -(void)didRegisterUserNotificationSettings:(NSObject * _Nullable)notificationSettings;
	[Export ("didRegisterUserNotificationSettings:")]
	void DidRegisterUserNotificationSettings ([NullAllowed] NSObject notificationSettings);

	// -(void)receivedRemoteNotification:(NSDictionary * _Nullable)userInfo;
	[Export ("receivedRemoteNotification:")]
	void ReceivedRemoteNotification ([NullAllowed] NSDictionary userInfo);

	// -(BOOL)handleActionWithIdentifier:(NSString * _Nullable)identifier forRemoteNotification:(NSDictionary * _Nullable)userInfo completionHandler:(void (^ _Nonnull)())completionHandler;
	[Export ("handleActionWithIdentifier:forRemoteNotification:completionHandler:")]
	bool HandleActionWithIdentifier ([NullAllowed] string identifier, [NullAllowed] NSDictionary userInfo, Action completionHandler);

	// -(void)didReceiveRemoteNotification:(NSDictionary * _Nullable)userInfo fetchCompletionHandler:(void (^ _Nullable)(UIBackgroundFetchResult))completionHandler andNotifyCompletionWithBlock:(AppoxeeCompletionHandler _Nullable)completionBlock;
	[Export ("didReceiveRemoteNotification:fetchCompletionHandler:andNotifyCompletionWithBlock:")]
	void DidReceiveRemoteNotification ([NullAllowed] NSDictionary userInfo, [NullAllowed] Action<UIBackgroundFetchResult> completionHandler, [NullAllowed] AppoxeeCompletionHandler completionBlock);

	// -(void)performFetchWithCompletionHandler:(void (^ _Nullable)(UIBackgroundFetchResult))fetchHandler andNotifyCompletionWithBlock:(AppoxeeCompletionHandler _Nullable)completionBlock;
	[Export ("performFetchWithCompletionHandler:andNotifyCompletionWithBlock:")]
	void PerformFetchWithCompletionHandler ([NullAllowed] Action<UIBackgroundFetchResult> fetchHandler, [NullAllowed] AppoxeeCompletionHandler completionBlock);

	// -(void)userNotificationCenter:(UNUserNotificationCenter * _Nonnull)center didReceiveNotificationResponse:(UNNotificationResponse * _Nonnull)response withAppoxeeCompletionHandler:(void (^ _Nullable)())appoxeeCompletionHandler __attribute__((availability(ios, introduced=10.0)));
	[iOS (10,0)]
	[Export ("userNotificationCenter:didReceiveNotificationResponse:withAppoxeeCompletionHandler:")]
	void UserNotificationCenter (UNUserNotificationCenter center, UNNotificationResponse response, [NullAllowed] Action appoxeeCompletionHandler);

	// -(void)disablePushNotifications:(BOOL)isDisabled withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("disablePushNotifications:withCompletionHandler:")]
	void DisablePushNotifications (bool isDisabled, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)isPushEnabled:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("isPushEnabled:")]
	void IsPushEnabled ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)disableInbox:(BOOL)isDisabled withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("disableInbox:withCompletionHandler:")]
	void DisableInbox (bool isDisabled, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)isInboxEnabled:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("isInboxEnabled:")]
	void IsInboxEnabled ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)setDeviceAlias:(NSString * _Nullable)alias withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("setDeviceAlias:withCompletionHandler:")]
	void SetDeviceAlias ([NullAllowed] string alias, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)removeDeviceAliasWithCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("removeDeviceAliasWithCompletionHandler:")]
	void RemoveDeviceAliasWithCompletionHandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)getDeviceAliasWithCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("getDeviceAliasWithCompletionHandler:")]
	void GetDeviceAliasWithCompletionHandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)clearAliasCacheWithCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("clearAliasCacheWithCompletionHandler:")]
	void ClearAliasCacheWithCompletionHandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)fetchDeviceTags:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("fetchDeviceTags:")]
	void FetchDeviceTags ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)fetchApplicationTags:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("fetchApplicationTags:")]
	void FetchApplicationTags ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)addTagsToDevice:(NSArray<NSString *> * _Nullable)tagsToAdd withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("addTagsToDevice:withCompletionHandler:")]
	void AddTagsToDevice ([NullAllowed] string[] tagsToAdd, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)removeTagsFromDevice:(NSArray<NSString *> * _Nullable)tagsToRemove withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("removeTagsFromDevice:withCompletionHandler:")]
	void RemoveTagsFromDevice ([NullAllowed] string[] tagsToRemove, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)addTagsToDevice:(NSArray<NSString *> * _Nullable)tagsToAdd andRemove:(NSArray<NSString *> * _Nullable)tagsToRemove withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("addTagsToDevice:andRemove:withCompletionHandler:")]
	void AddTagsToDevice ([NullAllowed] string[] tagsToAdd, [NullAllowed] string[] tagsToRemove, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)clearTagsCacheWithCompletionhandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("clearTagsCacheWithCompletionhandler:")]
	void ClearTagsCacheWithCompletionhandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)setDateValue:(NSDate * _Nonnull)date forKey:(NSString * _Nonnull)key withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("setDateValue:forKey:withCompletionHandler:")]
	void SetDateValue (NSDate date, string key, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)setNumberValue:(NSNumber * _Nonnull)number forKey:(NSString * _Nonnull)key withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("setNumberValue:forKey:withCompletionHandler:")]
	void SetNumberValue (NSNumber number, string key, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)incrementNumericKey:(NSString * _Nonnull)key byNumericValue:(NSNumber * _Nonnull)number withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("incrementNumericKey:byNumericValue:withCompletionHandler:")]
	void IncrementNumericKey (string key, NSNumber number, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)setStringValue:(NSString * _Nonnull)string forKey:(NSString * _Nonnull)key withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("setStringValue:forKey:withCompletionHandler:")]
	void SetStringValue (string @string, string key, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)fetchCustomFieldByKey:(NSString * _Nullable)key withCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("fetchCustomFieldByKey:withCompletionHandler:")]
	void FetchCustomFieldByKey ([NullAllowed] string key, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)clearCustomFieldsCacheWithCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("clearCustomFieldsCacheWithCompletionHandler:")]
	void ClearCustomFieldsCacheWithCompletionHandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(APXClientDevice * _Nonnull)deviceInfo;
	[Export ("deviceInfo")]
	[Verify (MethodToProperty)]
	APXClientDevice DeviceInfo { get; }

	// -(void)deviceInformationwithCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("deviceInformationwithCompletionHandler:")]
	void DeviceInformationwithCompletionHandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)getRichMessagesWithHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("getRichMessagesWithHandler:")]
	void GetRichMessagesWithHandler ([NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)deleteRichMessage:(APXRichMessage * _Nullable)richMessage withHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("deleteRichMessage:withHandler:")]
	void DeleteRichMessage ([NullAllowed] APXRichMessage richMessage, [NullAllowed] AppoxeeCompletionHandler handler);

	// -(void)refreshInboxWithCompletionHandler:(AppoxeeCompletionHandler _Nullable)handler;
	[Export ("refreshInboxWithCompletionHandler:")]
	void RefreshInboxWithCompletionHandler ([NullAllowed] AppoxeeCompletionHandler handler);
}

// @protocol AppoxeeDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface AppoxeeDelegate
{
	// @required -(NSString * _Nonnull)AppoxeeDelegateAppSDKID __attribute__((deprecated("using engageWithLaunchOptions:andDelegate:andSDKID: does not require implementing this method")));
	[Abstract]
	[Export ("AppoxeeDelegateAppSDKID")]
	[Verify (MethodToProperty)]
	string AppoxeeDelegateAppSDKID { get; }

	// @required -(NSString * _Nonnull)AppoxeeDelegateAppSecret __attribute__((deprecated("using engageWithLaunchOptions:andDelegate:andSDKID: does not require implementing this method")));
	[Abstract]
	[Export ("AppoxeeDelegateAppSecret")]
	[Verify (MethodToProperty)]
	string AppoxeeDelegateAppSecret { get; }

	// @optional -(void)appDidOpenFromPushNotification:(NSDictionary * _Nonnull)userInfo __attribute__((deprecated("Use appoxeeManager:handledRemoteNotification:andIdentifer: instead.")));
	[Export ("appDidOpenFromPushNotification:")]
	void AppDidOpenFromPushNotification (NSDictionary userInfo);

	// @optional -(void)AppoxeeNeedsToUpdateBadge:(int)badgeNum hasNumberChanged:(BOOL)hasNumberChanged __attribute__((deprecated("Use appoxeeManager:handledRemoteNotification:andIdentifer: instead.")));
	[Export ("AppoxeeNeedsToUpdateBadge:hasNumberChanged:")]
	void AppoxeeNeedsToUpdateBadge (int badgeNum, bool hasNumberChanged);

	// @optional -(void)appoxeeInteractivePushNotificationPressedWithCategory:(NSString * _Nonnull)category andPayload:(NSDictionary * _Nonnull)pushPayload __attribute__((deprecated("No calls will be forwarded to this method. Use appoxeeManager:handledRemoteNotification:andIdentifer: instead.")));
	[Export ("appoxeeInteractivePushNotificationPressedWithCategory:andPayload:")]
	void AppoxeeInteractivePushNotificationPressedWithCategory (string category, NSDictionary pushPayload);

	// @optional -(void)appoxeeInteractivePushButton1PressedWithCategory:(NSString * _Nonnull)category andPayload:(NSDictionary * _Nonnull)pushPayload __attribute__((deprecated("No calls will be forwarded to this method. Use appoxeeManager:handledRemoteNotification:andIdentifer: instead.")));
	[Export ("appoxeeInteractivePushButton1PressedWithCategory:andPayload:")]
	void AppoxeeInteractivePushButton1PressedWithCategory (string category, NSDictionary pushPayload);

	// @optional -(void)appoxeeInteractivePushButton2PressedWithCategory:(NSString * _Nonnull)category andPayload:(NSDictionary * _Nonnull)pushPayload __attribute__((deprecated("No calls will be forwarded to this method. Use appoxeeManager:handledRemoteNotification:andIdentifer: instead.")));
	[Export ("appoxeeInteractivePushButton2PressedWithCategory:andPayload:")]
	void AppoxeeInteractivePushButton2PressedWithCategory (string category, NSDictionary pushPayload);

	// @optional -(void)AppoxeeDelegateReciveAppoxeeClosed __attribute__((deprecated("No calls will be forwarded to this method.")));
	[Export ("AppoxeeDelegateReciveAppoxeeClosed")]
	void AppoxeeDelegateReciveAppoxeeClosed ();

	// @optional -(void)AppoxeeDelegateReciveAppoxeeRequestFocus __attribute__((deprecated("No calls will be forwarded to this method.")));
	[Export ("AppoxeeDelegateReciveAppoxeeRequestFocus")]
	void AppoxeeDelegateReciveAppoxeeRequestFocus ();

	// @optional -(BOOL)shouldAppoxeeRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation __attribute__((deprecated("No calls will be forwarded to this method.")));
	[Export ("shouldAppoxeeRotateToInterfaceOrientation:")]
	bool ShouldAppoxeeRotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation);
}

// @interface AppoxeeManager : NSObject
[BaseType (typeof(NSObject))]
interface AppoxeeManager
{
	// +(instancetype _Nullable)sharedManager;
	[Static]
	[Export ("sharedManager")]
	[return: NullAllowed]
	AppoxeeManager SharedManager ();

	// -(void)initManagerWithDelegate:(id<AppoxeeDelegate> _Nullable)delegate andOptions:(NSDictionary * _Nullable)options __attribute__((deprecated("use engageWithLaunchOptions:andDelegate:andSDKID: instead.")));
	[Export ("initManagerWithDelegate:andOptions:")]
	void InitManagerWithDelegate ([NullAllowed] AppoxeeDelegate @delegate, [NullAllowed] NSDictionary options);

	// -(void)managerParseLaunchOptions:(NSDictionary * _Nullable)launchOptions __attribute__((deprecated("use engageWithLaunchOptions:andDelegate:andSDKID: instead.")));
	[Export ("managerParseLaunchOptions:")]
	void ManagerParseLaunchOptions ([NullAllowed] NSDictionary launchOptions);

	// -(BOOL)didReceiveRemoteNotification:(NSDictionary * _Nullable)userInfo __attribute__((deprecated("use receivedRemoteNotification: instead.")));
	[Export ("didReceiveRemoteNotification:")]
	bool DidReceiveRemoteNotification ([NullAllowed] NSDictionary userInfo);

	// -(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData * _Nonnull)token __attribute__((deprecated("use didRegisterForRemoteNotificationsWithDeviceToken: instead.")));
	[Export ("didRegisterForRemoteNotificationsWithDeviceToken:")]
	void DidRegisterForRemoteNotificationsWithDeviceToken (NSData token);

	// -(BOOL)handleActionWithIdentifier:(NSString * _Nullable)identifier forRemoteNotification:(NSDictionary * _Nonnull)userInfo completionHandler:(void (^ _Nullable)())completionHandler __attribute__((deprecated("use handleActionWithIdentifier:forRemoteNotification:completionHandler: instead.")));
	[Export ("handleActionWithIdentifier:forRemoteNotification:completionHandler:")]
	bool HandleActionWithIdentifier ([NullAllowed] string identifier, NSDictionary userInfo, [NullAllowed] Action completionHandler);

	// -(void)optOutPush:(BOOL)flag __attribute__((deprecated("use disablePushNotifications:withCompletionHandler: instead.")));
	[Export ("optOutPush:")]
	void OptOutPush (bool flag);

	// -(BOOL)isPushEnabled __attribute__((deprecated("use isPushEnabled: instead.")));
	[Export ("isPushEnabled")]
	[Verify (MethodToProperty)]
	bool IsPushEnabled { get; }

	// -(void)optOutInbox:(BOOL)flag __attribute__((deprecated("use disableInbox:withCompletionHandler: instead.")));
	[Export ("optOutInbox:")]
	void OptOutInbox (bool flag);

	// -(BOOL)isInboxEnabled __attribute__((deprecated("use isInboxEnabled: instead.")));
	[Export ("isInboxEnabled")]
	[Verify (MethodToProperty)]
	bool IsInboxEnabled { get; }

	// -(BOOL)setDeviceAlias:(NSString * _Nullable)alias __attribute__((deprecated("use setDeviceAlias:withCompletionHandler instead.")));
	[Export ("setDeviceAlias:")]
	bool SetDeviceAlias ([NullAllowed] string alias);

	// -(BOOL)removeDeviceAlias __attribute__((deprecated("use removeDeviceAliasWithCompletionHandler: instead.")));
	[Export ("removeDeviceAlias")]
	[Verify (MethodToProperty)]
	bool RemoveDeviceAlias { get; }

	// -(NSString * _Nullable)getDeviceAlias __attribute__((deprecated("use getDeviceAliasWithCompletionHandler: instead.")));
	[NullAllowed, Export ("getDeviceAlias")]
	[Verify (MethodToProperty)]
	string DeviceAlias { get; }

	// -(void)clearAliasCache __attribute__((deprecated("use clearAliasCacheWithCompletionHandler: instead.")));
	[Export ("clearAliasCache")]
	void ClearAliasCache ();

	// -(NSArray * _Nullable)getDeviceTags __attribute__((deprecated("use fetchDeviceTags: instead.")));
	[NullAllowed, Export ("getDeviceTags")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] DeviceTags { get; }

	// -(NSArray * _Nullable)getTagList __attribute__((deprecated("use fetchApplicationTags: instead.")));
	[NullAllowed, Export ("getTagList")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] TagList { get; }

	// -(BOOL)addTagsToDevice:(NSArray * _Nullable)tagsToAdd andRemove:(NSArray * _Nullable)tagsToRemove __attribute__((deprecated("use addTagsToDevice:andRemove:withCompletionHandler: instead.")));
	[Export ("addTagsToDevice:andRemove:")]
	[Verify (StronglyTypedNSArray), Verify (StronglyTypedNSArray)]
	bool AddTagsToDevice ([NullAllowed] NSObject[] tagsToAdd, [NullAllowed] NSObject[] tagsToRemove);

	// -(BOOL)addTagsToDevice:(NSArray * _Nullable)tags __attribute__((deprecated("use addTagsToDevice:withCompletionHandler: instead.")));
	[Export ("addTagsToDevice:")]
	[Verify (StronglyTypedNSArray)]
	bool AddTagsToDevice ([NullAllowed] NSObject[] tags);

	// -(BOOL)removeTagsFromDevice:(NSArray * _Nullable)tags __attribute__((deprecated("use removeTagsFromDevice:withCompletionHandler: instead.")));
	[Export ("removeTagsFromDevice:")]
	[Verify (StronglyTypedNSArray)]
	bool RemoveTagsFromDevice ([NullAllowed] NSObject[] tags);

	// -(void)clearTagsCache __attribute__((deprecated("use clearTagsCacheWithCompletionhandler: instead.")));
	[Export ("clearTagsCache")]
	void ClearTagsCache ();

	// -(BOOL)setDateFields:(NSMutableDictionary * _Nullable)datesDict __attribute__((deprecated("use setDateField:withCompletionHandler: instead.")));
	[Export ("setDateFields:")]
	bool SetDateFields ([NullAllowed] NSMutableDictionary datesDict);

	// -(BOOL)setNumericFields:(NSMutableDictionary * _Nullable)numericValuesDict __attribute__((deprecated("use setNumericField:withCompletionHandler: instead.")));
	[Export ("setNumericFields:")]
	bool SetNumericFields ([NullAllowed] NSMutableDictionary numericValuesDict);

	// -(BOOL)incNumericFields:(NSMutableDictionary * _Nullable)numericValuesDict __attribute__((deprecated("use incrementNumericField:withCompletionHandler: instead.")));
	[Export ("incNumericFields:")]
	bool IncNumericFields ([NullAllowed] NSMutableDictionary numericValuesDict);

	// -(BOOL)setStringFields:(NSMutableDictionary * _Nullable)stringValuesDict __attribute__((deprecated("use setStringField:withCompletionHandler: instead.")));
	[Export ("setStringFields:")]
	bool SetStringFields ([NullAllowed] NSMutableDictionary stringValuesDict);

	// -(NSString * _Nullable)getCustomFieldByKey:(NSString * _Nullable)key __attribute__((deprecated("use fetchCustomFieldByKey:withCompletionHandler: instead.")));
	[Export ("getCustomFieldByKey:")]
	[return: NullAllowed]
	string GetCustomFieldByKey ([NullAllowed] string key);

	// -(NSString * _Nullable)getDeviceOsName __attribute__((deprecated("use deviceInformationwithCompletionHandler: instead.")));
	[NullAllowed, Export ("getDeviceOsName")]
	[Verify (MethodToProperty)]
	string DeviceOsName { get; }

	// -(NSString * _Nullable)getDeviceOsNumber __attribute__((deprecated("use deviceInformationwithCompletionHandler: instead.")));
	[NullAllowed, Export ("getDeviceOsNumber")]
	[Verify (MethodToProperty)]
	string DeviceOsNumber { get; }

	// -(NSString * _Nullable)getHardwareType __attribute__((deprecated("use deviceInformationwithCompletionHandler: instead.")));
	[NullAllowed, Export ("getHardwareType")]
	[Verify (MethodToProperty)]
	string HardwareType { get; }

	// -(NSString * _Nullable)getDeviceCountry __attribute__((deprecated("use deviceInformationwithCompletionHandler: instead.")));
	[NullAllowed, Export ("getDeviceCountry")]
	[Verify (MethodToProperty)]
	string DeviceCountry { get; }

	// -(NSString * _Nullable)getApplicationID __attribute__((deprecated("use deviceInformationwithCompletionHandler: instead.")));
	[NullAllowed, Export ("getApplicationID")]
	[Verify (MethodToProperty)]
	string ApplicationID { get; }

	// -(NSString * _Nullable)getDeviceUniqueID __attribute__((deprecated("use deviceInformationwithCompletionHandler: instead.")));
	[NullAllowed, Export ("getDeviceUniqueID")]
	[Verify (MethodToProperty)]
	string DeviceUniqueID { get; }

	// -(void)deleteAppoxeeMessage:(id _Nullable)appoxeeMessage __attribute__((deprecated("use deleteRichMessage:withHandler: instead.")));
	[Export ("deleteAppoxeeMessage:")]
	void DeleteAppoxeeMessage ([NullAllowed] NSObject appoxeeMessage);

	// -(NSArray * _Nullable)getInboxMessages __attribute__((deprecated("use getRichMessagesWithHandler: or refreshDataWithCompletionHandler: instead.")));
	[NullAllowed, Export ("getInboxMessages")]
	[Verify (MethodToProperty), Verify (StronglyTypedNSArray)]
	NSObject[] InboxMessages { get; }

	// -(void)disableFeedbackAndMoreApps:(BOOL)shouldDisable __attribute__((deprecated("")));
	[Export ("disableFeedbackAndMoreApps:")]
	void DisableFeedbackAndMoreApps (bool shouldDisable);

	// -(NSString * _Nullable)getServerV3URL __attribute__((deprecated("")));
	[NullAllowed, Export ("getServerV3URL")]
	[Verify (MethodToProperty)]
	string ServerV3URL { get; }

	// -(NSString * _Nullable)getGateKeeperURL __attribute__((deprecated("")));
	[NullAllowed, Export ("getGateKeeperURL")]
	[Verify (MethodToProperty)]
	string GateKeeperURL { get; }

	// -(NSString * _Nullable)getServerV2URL __attribute__((deprecated("")));
	[NullAllowed, Export ("getServerV2URL")]
	[Verify (MethodToProperty)]
	string ServerV2URL { get; }

	// -(NSString * _Nullable)getMoreAppsURL __attribute__((deprecated("")));
	[NullAllowed, Export ("getMoreAppsURL")]
	[Verify (MethodToProperty)]
	string MoreAppsURL { get; }

	// -(NSString * _Nullable)getFeedbackURL __attribute__((deprecated("")));
	[NullAllowed, Export ("getFeedbackURL")]
	[Verify (MethodToProperty)]
	string FeedbackURL { get; }

	// -(NSString * _Nullable)getDeviceDefURL __attribute__((deprecated("")));
	[NullAllowed, Export ("getDeviceDefURL")]
	[Verify (MethodToProperty)]
	string DeviceDefURL { get; }

	// -(NSString * _Nullable)getDeviceAdsURL __attribute__((deprecated("")));
	[NullAllowed, Export ("getDeviceAdsURL")]
	[Verify (MethodToProperty)]
	string DeviceAdsURL { get; }

	// -(NSArray * _Nullable)setAttributeWithDict:(NSMutableDictionary * _Nullable)param andKey:(NSString * _Nullable)keyParam __attribute__((deprecated("")));
	[Export ("setAttributeWithDict:andKey:")]
	[Verify (StronglyTypedNSArray)]
	[return: NullAllowed]
	NSObject[] SetAttributeWithDict ([NullAllowed] NSMutableDictionary param, [NullAllowed] string keyParam);

	// -(NSString * _Nullable)getAttribute:(NSString * _Nullable)param __attribute__((deprecated("")));
	[Export ("getAttribute:")]
	[return: NullAllowed]
	string GetAttribute ([NullAllowed] string param);

	// -(void)configureAppoxeeForLocale:(NSString * _Nullable)theLocale __attribute__((deprecated("")));
	[Export ("configureAppoxeeForLocale:")]
	void ConfigureAppoxeeForLocale ([NullAllowed] string theLocale);

	// -(void)optOutQuietTime __attribute__((deprecated("")));
	[Export ("optOutQuietTime")]
	void OptOutQuietTime ();

	// -(void)SplashScreen __attribute__((deprecated("")));
	[Export ("SplashScreen")]
	void SplashScreen ();

	// -(void)optOutBadge:(BOOL)flag __attribute__((deprecated("")));
	[Export ("optOutBadge:")]
	void OptOutBadge (bool flag);

	// -(void)optOutSound:(BOOL)flag __attribute__((deprecated("")));
	[Export ("optOutSound:")]
	void OptOutSound (bool flag);

	// -(UIRemoteNotificationType)getNotificationStatus __attribute__((deprecated("")));
	[Export ("getNotificationStatus")]
	[Verify (MethodToProperty)]
	UIRemoteNotificationType NotificationStatus { get; }

	// -(void)setQuietTime:(NSString * _Nullable)startTimeParam endTime:(NSString * _Nullable)endTimeParam __attribute__((deprecated("")));
	[Export ("setQuietTime:endTime:")]
	void SetQuietTime ([NullAllowed] string startTimeParam, [NullAllowed] string endTimeParam);

	// -(NSString * _Nullable)getSplashScreen __attribute__((deprecated("")));
	[NullAllowed, Export ("getSplashScreen")]
	[Verify (MethodToProperty)]
	string SplashScreen { get; }

	// -(NSString * _Nullable)getPoweredByImageLink __attribute__((deprecated("")));
	[NullAllowed, Export ("getPoweredByImageLink")]
	[Verify (MethodToProperty)]
	string PoweredByImageLink { get; }

	// -(NSString * _Nullable)getQuietTimeStart __attribute__((deprecated("")));
	[NullAllowed, Export ("getQuietTimeStart")]
	[Verify (MethodToProperty)]
	string QuietTimeStart { get; }

	// -(NSString * _Nullable)getQuietTimeEnd __attribute__((deprecated("")));
	[NullAllowed, Export ("getQuietTimeEnd")]
	[Verify (MethodToProperty)]
	string QuietTimeEnd { get; }

	// -(void)setSoundEnabled:(BOOL)enabled __attribute__((deprecated("")));
	[Export ("setSoundEnabled:")]
	void SetSoundEnabled (bool enabled);

	// -(void)setBadgeEnabled:(BOOL)enabled __attribute__((deprecated("")));
	[Export ("setBadgeEnabled:")]
	void SetBadgeEnabled (bool enabled);

	// -(BOOL)isCoppaShown __attribute__((deprecated("")));
	[Export ("isCoppaShown")]
	[Verify (MethodToProperty)]
	bool IsCoppaShown { get; }

	// -(BOOL)isSoundEnabled __attribute__((deprecated("")));
	[Export ("isSoundEnabled")]
	[Verify (MethodToProperty)]
	bool IsSoundEnabled { get; }

	// -(BOOL)isBadgeEnabled __attribute__((deprecated("")));
	[Export ("isBadgeEnabled")]
	[Verify (MethodToProperty)]
	bool IsBadgeEnabled { get; }

	// -(int)getDeviceActivations __attribute__((deprecated("")));
	[Export ("getDeviceActivations")]
	[Verify (MethodToProperty)]
	int DeviceActivations { get; }

	// -(NSDecimalNumber * _Nullable)getInAppPayment __attribute__((deprecated("")));
	[NullAllowed, Export ("getInAppPayment")]
	[Verify (MethodToProperty)]
	NSDecimalNumber InAppPayment { get; }

	// -(int)getNumProductsPurchased __attribute__((deprecated("")));
	[Export ("getNumProductsPurchased")]
	[Verify (MethodToProperty)]
	int NumProductsPurchased { get; }

	// -(BOOL)increaseInAppPayment:(NSDecimalNumber * _Nullable)payment andNumPurchased:(NSDecimalNumber * _Nullable)numPurchased __attribute__((deprecated("")));
	[Export ("increaseInAppPayment:andNumPurchased:")]
	bool IncreaseInAppPayment ([NullAllowed] NSDecimalNumber payment, [NullAllowed] NSDecimalNumber numPurchased);

	// -(BOOL)increaseNumProductPurchased:(NSDecimalNumber * _Nullable)payment __attribute__((deprecated("")));
	[Export ("increaseNumProductPurchased:")]
	bool IncreaseNumProductPurchased ([NullAllowed] NSDecimalNumber payment);

	// -(int)getInboxFlag __attribute__((deprecated("")));
	[Export ("getInboxFlag")]
	[Verify (MethodToProperty)]
	int InboxFlag { get; }

	// -(int)getCustomInboxFlag __attribute__((deprecated("")));
	[Export ("getCustomInboxFlag")]
	[Verify (MethodToProperty)]
	int CustomInboxFlag { get; }

	// -(int)getFeedbackFlag __attribute__((deprecated("")));
	[Export ("getFeedbackFlag")]
	[Verify (MethodToProperty)]
	int FeedbackFlag { get; }

	// -(int)getMoreAppsFlag __attribute__((deprecated("")));
	[Export ("getMoreAppsFlag")]
	[Verify (MethodToProperty)]
	int MoreAppsFlag { get; }

	// -(void)openAppoxeeMessage:(id _Nullable)appoxeeMessage __attribute__((deprecated("")));
	[Export ("openAppoxeeMessage:")]
	void OpenAppoxeeMessage ([NullAllowed] NSObject appoxeeMessage);

	// -(void)addBadgeToView:(UIView * _Nullable)badgeView badgeText:(NSString * _Nullable)badgeText badgeLocation:(CGPoint)badgeLocation __attribute__((deprecated("")));
	[Export ("addBadgeToView:badgeText:badgeLocation:")]
	void AddBadgeToView ([NullAllowed] UIView badgeView, [NullAllowed] string badgeText, CGPoint badgeLocation);

	// -(void)addBadgeToView:(UIView * _Nullable)badgeView badgeText:(NSString * _Nullable)badgeText badgeLocation:(CGPoint)badgeLocation shouldFlashBadge:(BOOL)shouldFlashBadge __attribute__((deprecated("")));
	[Export ("addBadgeToView:badgeText:badgeLocation:shouldFlashBadge:")]
	void AddBadgeToView ([NullAllowed] UIView badgeView, [NullAllowed] string badgeText, CGPoint badgeLocation, bool shouldFlashBadge);

	// -(void)addBadgeToView:(UIView * _Nullable)badgeView badgeText:(NSString * _Nullable)badgeText badgeLocation:(CGPoint)badgeLocation shouldFlashBadge:(BOOL)shouldFlashBadge withFontSize:(float)fontSize __attribute__((deprecated("")));
	[Export ("addBadgeToView:badgeText:badgeLocation:shouldFlashBadge:withFontSize:")]
	void AddBadgeToView ([NullAllowed] UIView badgeView, [NullAllowed] string badgeText, CGPoint badgeLocation, bool shouldFlashBadge, float fontSize);

	// -(void)show __attribute__((deprecated("")));
	[Export ("show")]
	void Show ();

	// -(BOOL)showMoreAppsViewController __attribute__((deprecated("")));
	[Export ("showMoreAppsViewController")]
	[Verify (MethodToProperty)]
	bool ShowMoreAppsViewController { get; }

	// -(BOOL)showFeedbackViewController __attribute__((deprecated("")));
	[Export ("showFeedbackViewController")]
	[Verify (MethodToProperty)]
	bool ShowFeedbackViewController { get; }

	// -(void)showMoreAppsOnInbox:(BOOL)show __attribute__((deprecated("")));
	[Export ("showMoreAppsOnInbox:")]
	void ShowMoreAppsOnInbox (bool show);

	// -(void)showFeedbackOnInbox:(BOOL)show __attribute__((deprecated("")));
	[Export ("showFeedbackOnInbox:")]
	void ShowFeedbackOnInbox (bool show);

	// -(UIViewController * _Nullable)getAppoxeeViewController __attribute__((deprecated("")));
	[NullAllowed, Export ("getAppoxeeViewController")]
	[Verify (MethodToProperty)]
	UIViewController AppoxeeViewController { get; }

	// -(UIViewController * _Nullable)getAppoxeeMoreAppsViewController __attribute__((deprecated("")));
	[NullAllowed, Export ("getAppoxeeMoreAppsViewController")]
	[Verify (MethodToProperty)]
	UIViewController AppoxeeMoreAppsViewController { get; }

	// -(UIViewController * _Nullable)getAppoxeeFeedbackViewController __attribute__((deprecated("")));
	[NullAllowed, Export ("getAppoxeeFeedbackViewController")]
	[Verify (MethodToProperty)]
	UIViewController AppoxeeFeedbackViewController { get; }

	// -(void)recalculateUnreadMessagesBadge __attribute__((deprecated("")));
	[Export ("recalculateUnreadMessagesBadge")]
	void RecalculateUnreadMessagesBadge ();

	// -(void)setUsingCustomInbox:(BOOL)isCustom __attribute__((deprecated("")));
	[Export ("setUsingCustomInbox:")]
	void SetUsingCustomInbox (bool isCustom);

	// -(BOOL)getUsingCustomInboxFlag __attribute__((deprecated("")));
	[Export ("getUsingCustomInboxFlag")]
	[Verify (MethodToProperty)]
	bool UsingCustomInboxFlag { get; }

	// -(void)showCustomInbox:(BOOL)show __attribute__((deprecated("")));
	[Export ("showCustomInbox:")]
	void ShowCustomInbox (bool show);

	// +(void)automaticallyClearNotifications:(BOOL)automatically __attribute__((deprecated("")));
	[Static]
	[Export ("automaticallyClearNotifications:")]
	void AutomaticallyClearNotifications (bool automatically);

	// +(BOOL)isNotificationsClearedAutomatically __attribute__((deprecated("")));
	[Static]
	[Export ("isNotificationsClearedAutomatically")]
	[Verify (MethodToProperty)]
	bool IsNotificationsClearedAutomatically { get; }
}

// typedef void (^APXInterfaceServiceCompletionBlock)(NSError * _Nullable, id _Nullable);
delegate void APXInterfaceServiceCompletionBlock ([NullAllowed] NSError arg0, [NullAllowed] NSObject arg1);

// typedef void (^APXInterfaceServiceFetchBlock)(UIBackgroundFetchResult);
delegate void APXInterfaceServiceFetchBlock (UIBackgroundFetchResult arg0);

// @protocol APXInterfaceServiceFetchDelegate <NSObject>
[Protocol, Model]
[BaseType (typeof(NSObject))]
interface APXInterfaceServiceFetchDelegate
{
	// @required -(void)interfaceService:(APXInterfaceService * _Nonnull)service performFetchWithCompletionHandler:(APXInterfaceServiceFetchBlock _Nullable)fetchBlock withCompletionBlock:(APXInterfaceServiceCompletionBlock _Nullable)completionBlock;
	[Abstract]
	[Export ("interfaceService:performFetchWithCompletionHandler:withCompletionBlock:")]
	void PerformFetchWithCompletionHandler (APXInterfaceService service, [NullAllowed] APXInterfaceServiceFetchBlock fetchBlock, [NullAllowed] APXInterfaceServiceCompletionBlock completionBlock);
}

// @interface APXInterfaceService : NSObject
[BaseType (typeof(NSObject))]
interface APXInterfaceService
{
	// +(instancetype _Nullable)shared;
	[Static]
	[Export ("shared")]
	[return: NullAllowed]
	APXInterfaceService Shared ();

	[Wrap ("WeakFetchDelegate")]
	[NullAllowed]
	APXInterfaceServiceFetchDelegate FetchDelegate { get; set; }

	// @property (nonatomic, weak) id<APXInterfaceServiceFetchDelegate> _Nullable fetchDelegate;
	[NullAllowed, Export ("fetchDelegate", ArgumentSemantic.Weak)]
	NSObject WeakFetchDelegate { get; set; }

	// @property (readonly, nonatomic, strong) NSString * _Nullable appID;
	[NullAllowed, Export ("appID", ArgumentSemantic.Strong)]
	string AppID { get; }

	// @property (readonly, nonatomic) BOOL isBackgroundFetchAvailable;
	[Export ("isBackgroundFetchAvailable")]
	bool IsBackgroundFetchAvailable { get; }

	// @property (readonly, nonatomic) BOOL isSilentPushAvailable;
	[Export ("isSilentPushAvailable")]
	bool IsSilentPushAvailable { get; }

	// -(void)performOperation:(APXInterfaceServiceOperation)operation withIdentifier:(NSString * _Nullable)identifier andData:(NSDictionary * _Nullable)args andCompletionBlock:(APXInterfaceServiceCompletionBlock _Nullable)completionBlock;
	[Export ("performOperation:withIdentifier:andData:andCompletionBlock:")]
	void PerformOperation (APXInterfaceServiceOperation operation, [NullAllowed] string identifier, [NullAllowed] NSDictionary args, [NullAllowed] APXInterfaceServiceCompletionBlock completionBlock);

	// -(void)performFetchWithCompletionHandler:(APXInterfaceServiceFetchBlock _Nullable)fetchBlock andCompletionBlock:(APXInterfaceServiceCompletionBlock _Nullable)completionBlock;
	[Export ("performFetchWithCompletionHandler:andCompletionBlock:")]
	void PerformFetchWithCompletionHandler ([NullAllowed] APXInterfaceServiceFetchBlock fetchBlock, [NullAllowed] APXInterfaceServiceCompletionBlock completionBlock);

	// +(NSTimeInterval)timestamp;
	[Static]
	[Export ("timestamp")]
	[Verify (MethodToProperty)]
	double Timestamp { get; }

	// +(NSString * _Nonnull)stringTimestamp;
	[Static]
	[Export ("stringTimestamp")]
	[Verify (MethodToProperty)]
	string StringTimestamp { get; }
}
