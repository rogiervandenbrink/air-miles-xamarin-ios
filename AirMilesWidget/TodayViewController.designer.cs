// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace AirMilesWidget
{
    [Register ("TodayViewController")]
    partial class TodayViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView BarcodeImageview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardnumberLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CardnumbertextLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BarcodeImageview != null) {
                BarcodeImageview.Dispose ();
                BarcodeImageview = null;
            }

            if (CardnumberLabel != null) {
                CardnumberLabel.Dispose ();
                CardnumberLabel = null;
            }

            if (CardnumbertextLabel != null) {
                CardnumbertextLabel.Dispose ();
                CardnumbertextLabel = null;
            }
        }
    }
}