﻿using System;

using NotificationCenter;
using Foundation;
using UIKit;
using CoreGraphics;

namespace AirMilesWidget
{
    public partial class TodayViewController : UIViewController, INCWidgetProviding
    {

        protected TodayViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            PreferredContentSize = new CGSize(PreferredContentSize.Width, 180f);
            //Console.WriteLine("screen width "+UIScreen.MainScreen.Bounds.Width);

            try
            {
                NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
                shared.Synchronize();
                string cardnumber = shared.StringForKey("CardNumber");

                if (!string.IsNullOrEmpty(cardnumber))
                {
                    UpdateTitle(cardnumber);
                    CardnumbertextLabel.Text = "Kaartnummer: " + cardnumber;
                }
                CardnumbertextLabel.Font = UIFont.FromName("Avenir-Book", 12f);
                CardnumbertextLabel.AdjustsFontSizeToFitWidth = true;
                CardnumberLabel.Font = UIFont.FromName("Avenir-Book", 12f);
                CardnumberLabel.AdjustsFontSizeToFitWidth = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception cardnumber widget " + ex);
            }
            ExtensionContext.SetWidgetLargestAvailableDisplayMode(NCWidgetDisplayMode.Expanded);

            // Get the maximum size
            var maxSize = ExtensionContext.GetWidgetMaximumSize(NCWidgetDisplayMode.Expanded);

            base.ViewDidLoad();

            // Do any additional setup after loading the view.
        }

        private void SetBarcode(string cardnumber)
        {
            try
            {
                // Create the barcode
                var BarcodeWriter = new ZXing.Mobile.BarcodeWriter
                {
                    Format = ZXing.BarcodeFormat.EAN_13,
                    Options = new ZXing.Common.EncodingOptions
                    {
                        Width = 290,
                        Height = 100,
                        Margin = 0
                    }
                };

                BarcodeImageview.Image = BarcodeWriter.Write(GenerateEan(cardnumber));
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception widget barcode " + ex);
            }
        }

        private void SetEanLabel(string cardnumber)
        {
            try
            {
                CardnumberLabel.Text = " " + GenerateEan(cardnumber);

                var EANStyle = new NSAttributedString(CardnumberLabel.Text,
                    new UIStringAttributes()
                    {
                        KerningAdjustment = 15
                    });

                CardnumberLabel.AttributedText = EANStyle;
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception ean label widget " + ex);
            }
        }

        public static string GenerateEan(string cardNumber)
        {
            int even = 0;
            int oneven = 0;

            string result = "260" + cardNumber;

            for (int i = 0; i < result.Length; i++)
            {
                if (i % 2 == 0)
                {
                    oneven += Int32.Parse(result[i].ToString());
                }
                else
                {
                    even += Int32.Parse(result[i].ToString());
                }
            }
            int controleGetal;
            int total = ((even * 3) + oneven) % 10;

            if (total != 0)
            {
                controleGetal = (10 - total);
            }
            else
            {
                controleGetal = 0;
            }

            return result + controleGetal;
        }

        [Export("widgetPerformUpdateWithCompletionHandler:")]
        public void WidgetPerformUpdate(Action<NCUpdateResult> completionHandler)
        {
            // Perform any setup necessary in order to update the view.

            // If an error is encoutered, use NCUpdateResultFailed
            // If there's no update required, use NCUpdateResultNoData
            // If there's an update, use NCUpdateResultNewData

            try
            {
                NSUserDefaults shared = new NSUserDefaults("group.nl.airmiles.app", NSUserDefaultsType.SuiteName);
                shared.Synchronize();
                string cardnumber = shared.StringForKey("CardNumber");
                if (!string.IsNullOrEmpty(cardnumber))
                {
                    UpdateTitle(cardnumber);
                    CardnumbertextLabel.Text = "Kaartnummer: " + cardnumber;
                }
                Console.WriteLine("shared " + shared);
                Console.WriteLine("cardnumber is " + cardnumber);
            }
            catch (Exception ex)
            {
                Console.WriteLine("exception cardnumber update widget " + ex);
            }
            // Take action based on the display mode
            switch (ExtensionContext.GetWidgetActiveDisplayMode())
            {
                case NCWidgetDisplayMode.Compact:
                    CardnumbertextLabel.Hidden = true;

                    break;
                case NCWidgetDisplayMode.Expanded:
                    CardnumbertextLabel.Hidden = false;
                    break;
            }

            completionHandler(NCUpdateResult.NewData);
        }

        void UpdateTitle(string cardnumber)
        {
            SetBarcode(cardnumber);
            SetEanLabel(cardnumber);
        }

        [Export("widgetActiveDisplayModeDidChange:withMaximumSize:")]
        public void WidgetActiveDisplayModeDidChange(NCWidgetDisplayMode activeDisplayMode, CGSize maxSize)
        {
            // Take action based on the display mode
            switch (activeDisplayMode)
            {
                case NCWidgetDisplayMode.Compact:
                    PreferredContentSize = new CGSize(maxSize.Width, 110);
                    CardnumbertextLabel.Hidden = true;
                    break;
                case NCWidgetDisplayMode.Expanded:
                    PreferredContentSize = new CGSize(maxSize.Width, 200);
                    CardnumbertextLabel.Hidden = false;
                    break;
            }
        }
    }
}
